<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
session_start();
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array('Email','Cookie','Session');
	public $language_name = array('ar'=>'Arbic','en'=>'English','de'=>'German','es'=>'Spanish','fr'=>'French','it'=>'Italian','zh'=>'Chinese','ru'=>'Russian','ja'=>'Japanese');
	public $months_ru = array(
                                    "Jan" => 'Ñ��?½�?²�?°Ñ€Ñ�',
                                    "Feb" => 'Ñ„�?µ�?²Ñ€�?°�?»Ñ�',
                                    "Mar" => '�?¼�?°Ñ€Ñ‚�?°',
                                    "Apr" => '�?°�?¿Ñ€�?µ�?»Ñ�',
                                    "May" => '�?¼�?°Ñ�',
                                    "Jun" => '�?¸ÑŽ�?½Ñ�',
                                    "Jul" => '�?¸ÑŽ�?»Ñ�',
                                    "Aug" => '�?°�?²�?³ÑƒÑ�Ñ‚�?°',
                                    "Sep" => 'Ñ��?µ�?½Ñ‚Ñ��?±Ñ€Ñ�',
                                    "Oct" => '�?¾�?ºÑ‚Ñ��?±Ñ€Ñ�',
                                    "Nov" => '�?½�?¾Ñ��?±Ñ€Ñ�',
                                    "Dec" => '�?´�?µ�?º�?°�?±Ñ€Ñ�'
                            );
        public $months_ja = array(
                                    "Jan" => 'ä¸€æœˆ',
                                    "Feb" => 'äºŒæœˆ',
                                    "Mar" => 'ä¸‰æœˆ',
                                    "Apr" => 'å››æœˆ',
                                    "May" => 'äº�?æœˆ',
                                    "Jun" => 'å…­æœˆ',
                                    "Jul" => 'ä¸ƒæœˆ',
                                    "Aug" => 'å…«æœˆ',
                                    "Sep" => 'ä¹�æœˆ',
                                    "Oct" => 'å��æœˆ',
                                    "Nov" => 'å��ä¸€æœˆ',
                                    "Dec" => 'å��äºŒæœˆ'
                                );
        public $months_ko = array(
                                    "Jan" => 'ì�¼ ì›�?',
                                    "Feb" => 'ì�´ ì›�?',
                                    "Mar" => 'ì‚¼ ìœŒ',
                                    "Apr" => 'ì‚¬ ì›�?',
                                    "May" => 'ì˜¤ ì›�?',
                                    "Jun" => 'ìœ  ì›�?',
                                    "Jul" => 'ì¹  ì›�?',
                                    "Aug" => 'íŒ�? ì›�?',
                                    "Sep" => 'êµ¬ ì›�?',
                                    "Oct" => 'ì‹œ ì›�?',
                                    "Nov" => 'ì‹­ ì�¼ ì›�?',
                                    "Dec" => 'ì‹­ ì�´ ìœŒ'
				);
	
        public $months_ar = array(
                                    "Jan" => "ÙŠÙ†Ø§ÙŠØ±",
                                    "Feb" => "Ù�Ø¨Ø±Ø§ÙŠØ±",
                                    "Mar" => "Ù…Ø§Ø±Ø³",
                                    "Apr" => "Ø£Ø¨Ø±ÙŠÙ„",
                                    "May" => "Ù…Ø§ÙŠÙˆ",
                                    "Jun" => "ÙŠÙˆÙ†ÙŠÙˆ",
                                    "Jul" => "ÙŠÙˆÙ„ÙŠÙˆ",
                                    "Aug" => "Ø£ØºØ³Ø·Ø³",
                                    "Sep" => "Ø³Ø¨ØªÙ…Ø¨Ø±",
                                    "Oct" => "Ø£ÙƒØªÙˆØ¨Ø±",
                                    "Nov" => "Ù†ÙˆÙ�Ù…Ø¨Ø±",
                                    "Dec" => "Ø¯ÙŠØ³Ù…Ø¨Ø±"
                                );
public $getCoursesindex;
	//*****THIS IS USED FOR NOTIFICATION  *************
	function beforeFilter()
	{
                $this->set('months_ru',$this->months_ru);
                $this->set('months_ja',$this->months_ja);
                $this->set('months_ko',$this->months_ko);
                $this->set('months_ar',$this->months_ar);


			// Codes added for SSL security
			/*$this->Security->validatePost=false;
			$this->Security->csrfCheck=false;
			$this->Security->csrfUseOnce=false;
			$sslnotallowed_url  = array('');
			$this->Security->blackHoleCallback = 'forceSSL';
			if(!in_array($this->params['action'],$sslnotallowed_url)){
			$this->Security->requireSecure('*');
			}*/
		$m_id=$this->Session->read("LocTrain.login");
		
		$visitor_id=$this->Session->read('LocTrain.visitor_id');
		// Start of new code to check whether the code is ajax or not.
		if(!$this->request->is('ajax')){
			$this->loadModel('Notification');
			//$this->Session->write('LocTrain.LangId',1);
			if(empty($m_id) && empty($visitor_id))
			{
					$visitor_id = str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ');
					$visitor_id=substr($visitor_id,0,8);
					$this->Session->write('LocTrain.visitor_id',$visitor_id);
			}
			$v=$this->Session->read('LocTrain.visitor_id');
			
	               	//$this->set('n_last', $this->Notification->find('first',array('conditions'=>array('OR'=>array(array('Notification.notification_sender_id'=>$v),array('Notification.notification_sender_id'=>$this->Session->read('LocTrain.id')))))));
	                $this->set('n_last', $this->Notification->find('first',array('conditions'=>array('OR'=>array(array('Notification.notification_sender_id'=>$v),array('Notification.notification_sender_id'=>$this->Session->read('LocTrain.id')))),'order'=>'Notification.id DESC')));
			
			$this->set('notification_total', $this->Notification->find('count',array('conditions'=>array('OR'=>array(array('Notification.notification_sender_id'=>$v),array('Notification.notification_sender_id'=>$this->Session->read('LocTrain.id')))))));
		}
		// End of new code
		
	}


function forceSSL() {
$this->redirect('https://stage.l10ntrain.com' . $this->here);
}
	//*****THIS IS USED FOR NOTIFICATION  *************
	
	
	function beforeRender()
	{ 
			/* SET TIME ZONE
			 *
			 * Determine the user's time zone based upon some geolocation information from the IP-API service.
			 * The default time zone will be UTC (the Olson TZ format is UTC as well).
			 * If the time zone information cannot be obtained, the default time zone will be used.
			 * Resolves: http://bugs.l10ntrain.com/view.php?id=490.
			 */

			// We will use the user's IP address instead of the server's IP address
	    $ip = $_SERVER['REMOTE_ADDR']; // NOI18N
              $query = '';
	   // $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip)); // NOI18N
			$tz = 'UTC'; // NOI18N

			if($query && $query['status'] == 'success') {
				if($query['timezone'])
				{
					$tz = $query['timezone']; // NOI18N
				}
			}
                        
			$this->Session->write('Timezone',$tz);

		//$this->Cookie->delete('LocTrain.locale'); die;
		/* -------------------- Check Multi-Langauge Function ------------------------- */
		// New code to check whether the request is ajax or nor
			if(!$this->request->is('ajax')){
				$this->notify();
			}
		// end of new code
			$action = $this->params['action'];			
			
			$this->loadModel('Language');
			$language = $this->Language->find('all',array('conditions'=>array('Language.status'=>'1'),'order'=>'Language.id ASC'));
			
			$this->set('language',$language);
			
			$language_code = array();
			
			foreach($language as $language)
			{
				$language_code[$language['Language']['lang_code']] = $language['Language']['locale'];
			}
			
			$lang = $this->Session->read('LocTrain.locale');
			$langId = $this->Session->read('LocTrain.LangId');
			
			if($this->Session->read('LocTrain.locale')=="ar")
			{
				if($this->Cookie->read('LocTrain.style_arabic'))
				{
					$css = $this->Cookie->read('LocTrain.style_arabic');
				}
				else
				{
					$css = 'style_arabic';
				}
			} else
			{ 
				if($this->Cookie->read('LocTrain.style'))
				{
					$css = $this->Cookie->read('LocTrain.style');
				}
				else
				{
					$css = 'style';
				}
			}
			 $this->loadModel('purchaseItem');
			 $this->loadModel('Course');
			$this->loadModel('CourseLesson');
                        $login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
			if($login_m_id){
                            $get_array=$this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));

		$encode_ids=array();
		if(!empty($get_array)){
			foreach($get_array as $row){
				$encode_ids[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
			}
		}
		$this->getCoursesindex=$encode_ids;$course_list=array();
				if(!empty($encode_ids)){
					foreach($encode_ids as $key=>$ids){
						$decode_id = @convert_uudecode(base64_decode($ids));
						if(strlen($key)>8)
						{ 
							$course=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'contain'=>array('Course'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id')))))));
						}else{
							$course = $this->Course->find('first',array('conditions'=>array('Course.id'=>$decode_id),'contain'=>array('CourseLesson','CoursePurchaseList'=>array('fields'=>array('mem_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));
						
						}
						if(!empty($course)){
							if($course['Course']['m_id'] != $this->Session->read('LocTrain.id')){
								$course['Course']['pro_id'] = base64_encode(convert_uuencode($key));
								$course_list[$key] = $course;	
							}else{
								$this->purchaseItem->delete(array('item_value' => $ids,'m_id'=>$login_m_id));
		 						$this->Cookie->delete('LocTrainPurchase.'.$key);
							}
						}else{
							$this->purchaseItem->delete(array('item_value' => $ids,'m_id'=>$login_m_id));
							$this->Cookie->delete('LocTrainPurchase.'.$key);
						}
						
						
					}
				}
				$cookPurchase=count($course_list);
		        }else{
					$this->getCoursesindex=$this->Cookie->read('LocTrainPurchase');
					$cookPurchase= count($this->Cookie->read('LocTrainPurchase'));
		        }
				$this->set('getCoursesindex',$this->getCoursesindex);



			$this->loadModel('Member');
			$this->Member->unbindModel(
        	array('hasMany' => array('MemberKeyword','MmeberAuthorKeyword','Subscription_course','MemberSubscription'),'belongsTo'=>array('CancelReason','Admin'))    );
                        $memberDetail =	$this->Member->find('first',array('conditions'=>array('Member.id'=>$login_m_id),'contain'=>array('MemberKeyword'=>array('fields'=>array('keyword')))));
            if(!empty($memberDetail['Member']['locale'])){
				$lang=$memberDetail['Member']['locale'];
            }
            if(!empty($language))
            {
                foreach($language as $lang_a)
                {
                    if(isset($lang_a['Language']) && $lang_a['Language']['locale']==  $memberDetail['Member']['locale']){
                            $langId=$lang_a['Language']['id'];
                    }
                }
            }
			if(in_array($lang,$language_code))
			{  
				$val = array_search($lang,$language_code);			
				$this->Cookie->write('LocTrain.locale',$lang,false,60*60*24*365);
				$this->Session->write('Config.language',$val);
				$this->Session->write('LocTrain.locale',$lang);
                                 $this->Session->write('LocTrain.LangId',$langId);				
			} 
			else 
			{
			/*------------country code-----------------*/
			           $lang_id ="";
				//$country_code = $this->lang();
				
				if(isset($country_code))
				{
					$arab_country = array('DZ','BH','EG','IQ','JO','KW','LB','LY','MA','AE','QA','SA','SY','TN','UE','YE','OM');
					
					$country = array('DE','FR','IT','JP','KO','PT','RU','ZH','ES','IN');
					
					
					if(in_array($country_code,$country))
					{

						if($country_code=="JP")
						{ 
                                                     
							$country_codes = "ja";
							$lang_country = $this->Language->find('first',array('conditions'=>array('Language.locale'=>$country_codes ),'contain'=>false,'fields'=>array('id','lang_code','locale')));
						
							$this->Cookie->write('LocTrain.locale',$lang_country ['Language']['locale'],false,60*60*24*365);
							$this->Session->write('Config.language',$lang_country ['Language']['lang_code']);
							$this->Session->write('LocTrain.locale',$lang_country ['Language']['locale']);
							$this->Session->write('LocTrain.LangId',$lang_country ['Language']['id']);


							
						}else
						{ 
							$country_code = strtolower($country_code);

							$lang_id = $this->Language->find('first',array('conditions'=>array('Language.locale'=>$country_code),'contain'=>false,'fields'=>array('id','lang_code')));
							
							if(!empty($lang_id))
							{
								$this->Cookie->write('LocTrain.locale',$country_code,false,60*60*24*365);
								$this->Session->write('Config.language',@$lang_Id['Language']['lang_code']);
								$this->Session->write('LocTrain.locale',@$lang_Id['Language']['locale']);
								$this->Session->write('LocTrain.LangId',@$lang_Id['Language']['id']);
							}else {
								
								$this->Cookie->write('LocTrain.locale','en',false,60*60*24*365);
								$this->Session->write('Config.language','en_US');
								$this->Session->write('LocTrain.locale','en');
								$this->Session->write('LocTrain.LangId','1');
								
							}
						}
					} else if(in_array($country_code,$arab_country))
					{
							$lang_id = $this->Language->find('first',array('conditions'=>array('Language.locale'=>'ar'),'contain'=>false,'fields'=>array('id','lang_code','locale')));
						
							$this->Cookie->write('LocTrain.locale','ar',false,60*60*24*365);
							$this->Session->write('Config.language',$lang_id['Language']['lang_code']);
							$this->Session->write('LocTrain.locale','ar');
							$this->Session->write('LocTrain.LangId',4);
					}else
					{
						$this->Cookie->write('LocTrain.locale','en',false,60*60*24*365);
						$this->Session->write('Config.language','en_US');
						$this->Session->write('LocTrain.locale','en');
						$this->Session->write('LocTrain.LangId','1');
					}
				}
				/*----------------------end country----------------*/
				else 
				{
					$lang = isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])?substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2):'en';	
					$lang_id = $this->Language->find('first',array('conditions'=>array('Language.locale'=>$lang),'contain'=>false,'fields'=>array('id','lang_code','locale')));
				
					if(in_array($lang,$language_code))
					{
						$val = array_search($lang,$language_code);
						$this->Cookie->write('LocTrain.locale',$lang,false,60*60*24*365);
						$this->Session->write('Config.language',$val);
						$this->Session->write('LocTrain.locale',$lang);	
						$this->Session->write('LocTrain.LangId',@$lang_Id['Language']['id']);
							//pr($val); die;
					} else
					{
						$this->Cookie->write('LocTrain.locale','en',false,60*60*24*365);
						$this->Session->write('Config.language','en_US');
						$this->Session->write('LocTrain.locale','en');
						$this->Session->write('LocTrain.LangId','1');
					}
				}
			}					
				//pr($_SERVER);die;
			$this->set(compact('action','cookPurchase'));
			$this->user_permissions();
			
			if($this->Session->check("LocTrain.timeout"))
			{
				$this->loadModel('LogoutTime');
				$lt = $this->LogoutTime->findById('1');
				
				$login_time = $this->Session->read("LocTrain.timeout");
				$now = time();
				$minutes = $lt['LogoutTime']['minutes'] * 60; //10 minutes
				
				$total_time = $login_time + $minutes;
			
				if($total_time < $now)
				{
					$this->Session->delete('LocTrain');
					$this->redirect(array('controller'=>'Home','action'=>'index'));
				}
				else 
				{
					$this->Session->write("LocTrain.timeout",$now);
				}
			}
			
			$m_role_id=1;
			$this->loadModel('Subscription');
			$subs_data=$this->Subscription->find('first');
			$subs_function=$subs_data['Subscription']['subscription_status'];
			if($this->Session->read('LocTrain.login')==1)
			{
				$this->loadModel('Member');
				$this->Member->unbindModel(
        array('hasMany' => array('MemberKeyword','MmeberAuthorKeyword','Subscription_course','MemberSubscription'),'belongsTo'=>array('CancelReason','Admin'))    );
				$condition=array('Member.id'=>$this->Session->read('LocTrain.id'));
				$m_info=$this->Member->find('first',array('conditions'=>$condition));
				
				$m_role_id=$m_info['Member']['role_id'];
			}
			$this->set(compact('css','m_role_id','subs_function')); 
			
								
			
				
	}
	function lang()
	{		
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
		$result  = "Unknown";
		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}

		$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));

		if($ip_data && $ip_data->geoplugin_countryCode != null)
		{
			$result = $ip_data->geoplugin_countryCode;
		}

		return $result;	
    }
	
	function notify()
	{

		$this->loadModel('Notification');
		if($this->Session->read('LocTrain.login')==1)
		{
			$mem_id =$this->Session->read('LocTrain.id');
			$conditions = array('Notification.mem_id !='=>$mem_id,'Notification.notification_sender_id'=>$mem_id);
		}
		else{
			$temp_id =$this->Session->read('LocTrain.visitor_id');
			$conditions = array('Notification.notification_sender_id'=>$temp_id);
		}	
		
		$notification=$this->Notification->find('all',array('conditions'=>$conditions,'limit'=>5,'order'=>'Notification.id DESC'));		//echo "<pre>";print_r($notification);echo "</pre>";
		//$order_ids=$this->Cookie->read('LocTrainPurchase');
		//echo "<pre>";print_r($order_ids);echo "</pre>";
		$countnotification=$this->Notification->find('count',array('conditions'=>$conditions,'order'=>'Notification.id DESC'));
		
		//print_r($countnotification);
		
		$this->set(compact('notification','countnotification'));
	}
	
	public function validEmail($email)
	{
		$pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,40})$";
		$ret="false";
		if (eregi($pattern, $email))
		{
			$ret="true";
		}
		return($ret);			
	}
	
	function send_email($to = null,$from = null, $subject = null, $data = null,$cc=NULL)
	{
		$email = new CakeEmail();
		// $email->config('smtp_amz');
		$email->template('common_template');
		$email->emailFormat('both');
		$email->viewVars(array('emailData' => $data));
		$email->to($to);	
		$email->cc($cc);
		$email->from(array($from=>'LocTrain'));
		$email->subject($subject);
		//print_r($email);
		//if($email->send()){echo 'sent';}else{echo 'No';}exit;
		return $email->send();
		
	}
	
	function user_permissions()
	{
		$all_permission = array();
		$EAP = array('update_profile','change_password','my_learning');
		$EOM = array('upgrade_membership');
		$PC = array('');
		$PS = array('');
		$CC = array('course','course_create','course_edit','lesson_add','lesson_edit');
		
		$all_pages = array('course','course_create','course_edit','lesson_add','lesson_edit','update_profile','change_password','upgrade_membership');
		
		if(in_array($this->params['action'],$all_pages)){
			if($this->Session->read('LocTrain.login') == '1'){
				$this->loadModel('Role');
				$permission = $this->Role->findById($this->Session->read('LocTrain.member_type'));
				//echo '<pre>';print_r($permission);echo '</pre>';
				if($permission['Role']['EAP'] == '1'){
					$all_permission = array_merge($all_permission,$EAP);
				}
				if($permission['Role']['EOM'] == '1'){
					$all_permission = array_merge($all_permission,$EOM);
				}
				if($permission['Role']['CC'] == '1'){
					$all_permission = array_merge($all_permission,$CC);
				}
				//echo '<pre>';print_r($all_permission);echo '</pre>';
//echo '<pre>';echo $this->params['action'];echo '</pre>';die();
				if(!in_array($this->params['action'],$all_permission)){
					//$this->Session->write('LocTrain.flashMsg',__('Permission Denied'));
					
					$login_m_id = $this->Session->read('LocTrain.id');
					$this->loadModel('Notification');
					$notification['Notification']['mem_id'] =  0;
					$notification['Notification']['notification_type'] = 'permission_denied';
					//$notification['Notification']['lesson_id'] = $lesson_deatils['CourseLesson']['id'];
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['notification_sender_id'] = $login_m_id;
					$this->Notification->create();
					$this->Notification->save($notification);
					
					
					
					$this->redirect(array('controller'=>'Home','action'=>'index'));
				}
			}
		}
	}

	
	function captchaImage()
	{	
	
		$captcha = str_shuffle('1234567890');					
		$randomnr = substr($captcha,0,2);
		$captcha = str_shuffle('abcdefghijklmnopqrstuvwxyz');					
		$randomnr .= substr($captcha,0,2);
		$captcha = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ');					
		$randomnr .= substr($captcha,0,2);
		$randomnr = str_shuffle($randomnr);
		
		$captcha_new = str_shuffle('1234567890');					
		$randomnr_new = substr($captcha,0,2);
		$captcha = str_shuffle('abcdefghijklmnopqrstuvwxyz');					
		$randomnr_new .= substr($captcha,0,2);
		$string  = $randomnr."  ".$randomnr_new;
		$this->Session->write('LocTrain.captchaCode', base64_encode($string));
		$im = imagecreatetruecolor(280, 80);
	 
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey = imagecolorallocate($im, 150, 150, 150);
		$black = imagecolorallocate($im, 0, 0, 0);
		$blue = imagecolorallocate($im, 48, 108, 181);
	 
		imagefilledrectangle($im, 0, 0, 279, 79, $blue);
		//path to font - this is just an example you can use any font you like:
		
		$font = './font/karate/VeraSe.ttf';
		
		imagettftext($im, 24, 5, 15, 53, $grey, $font, $string);
	 
		imagettftext($im, 25, 5, 15, 55, $white, $font, $string);
		
		//prevent caching on client side:
		header("Expires: Wed, 1 Jan 1997 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		//header ("Content-type: image/gif");
		
		imagegif($im,'./font/captcha.gif');
		//imagegif($im);
		imagedestroy($im);
	}

    function RandomStringGenerator($length = 10)
	{       
	  $string = "";
	  $pattern = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for($i=0; $i<$length; $i++)
		{
			$string .= $pattern{rand(0,61)};
		}
		return $string;
	}	
	function delete_lesson_files($previous_lesson_video, $current_lesson_video, $previous_lesson_ext, $current_lesson_ext)
	{
			$folder = $this->Session->read('LocTrain.id');
			$this->delete_lesson_file($current_lesson_video, $current_lesson_ext);
			if($current_lesson_video != $previous_lesson_video)
			{
				$this->delete_lesson_file($previous_lesson_video, $previous_lesson_ext);
			}

	}
	function delete_lesson_file($video, $ext)
	{
			$folder = $this->Session->read('LocTrain.id');
			switch(true)
			{
				case stristr(PHP_OS, 'WIN') :
					//echo "you run windows";
					$targetFolder = 'files/members/'.$folder."/";
					break;
				case stristr(PHP_OS, 'LINUX') :
					//echo "you run linux";
					$targetFolder = FILE_PATH.'app/webroot/files/members/'.$folder.'/';
					break;
			}
			
			$original_video		=	$video.'.'.$ext;
			$watermarked_video	=	$video.'_watermark.'.$ext;
			$preview_video		=	$video.'_preview.'.$ext;
			$thumbnail_video	=	$video.'_thumb.png';
			$small_thumb_video	=	$video.'_smallThumb.png';
			if(file_exists($targetFolder.$original_video))
			{
				@unlink($targetFolder.$original_video);
				@unlink($targetFolder.$watermarked_video);
				@unlink($targetFolder.$small_thumb_video);
				@unlink($targetFolder.$thumbnail_video);
				@unlink($targetFolder.$preview_video);
			}
			
	}
        
    function before_update_notification()
	{ 
		$this->loadModel('Notification');
		$v=$this->Session->read('LocTrain.visitor_id');
		$notify = $this->Notification->find('count',array('conditions'=>array('OR'=>array(array('Notification.notification_sender_id'=>$v),array('Notification.notification_sender_id'=>$this->Session->read('LocTrain.id'))))));
		if($notify)
		{	
			$this->Session->delete('before_update');
			$this->Session->write('before_update',$notify);
			//print_r($this->Session->read('before_update'));die; 
		}else{
			
			$this->Session->write('before_update','0');
		}		
	}
}
?>
