<?php
ob_start();
App::uses('Component', 'Controller');
// working all except purchase function
class CommonController extends AppController {
	var $name='Common';	
	var $components=array('Upload');
	var $helpers = array('Timezone','Utility');
	
	function importFiles($key,$bucket,$filepath){
		// uploading to AWS server 
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();
		$this->Aws->uploadFile($key,$bucket, $filepath);
	}
	function deleteFiles($keys,$bucket){
		// uploading to AWS server 
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();
		$this->Aws->deleteFiles($bucket, $keys);
		return json_encode($keys);
	}
	
	function url_exists($url) {
		if (!$fp = curl_init($url)) return false;
		return true;
	}
	function upload_video($lesson_id=NULL)
	{
		App::import('Helper', 'Utility'); 
		$this->Utility = new UtilityHelper(new View(null));
		$decode_id = "";
		ini_set('max_execution_time','14400');
		ini_set('max_input_time','14400');
		ini_set('max_file_uploads','800');
		ini_set('upload_max_filesize','30M');
		$verifyToken = md5('unique_salt' . $_POST['timestamp']);			
		$lesson['CourseLesson']['hours'] =0;
		$lesson['CourseLesson']['mins'] =0;
		$lesson['CourseLesson']['secs'] = 0;
		if (!empty($_FILES) && $_POST['token'] == $verifyToken)
		{
			$targetFolder = 'files/members/'.$_POST['id']."/";	
			if (!file_exists('files/members/'.$_POST['id'])) {
				mkdir('files/members/'.$_POST['id'], 0777, true);
		}
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$fileParts = pathinfo($_FILES['Filedata']['name']);	
			$ext = strtolower($fileParts['extension']);
			$file_name = str_replace(array(" ","-","(",')','.'),array("","_","_","",""),substr($fileParts['filename'],0,100)."_".time());
			$file_new_name = $file_name.".".$ext;			
		
			if($ext=='mp4' || $ext=='wmv' || $ext=='flv' || $ext=='avi' || $ext=='mov' || $ext=='swf' || $ext=='zip')
			{
				$img = $_FILES['Filedata'];	                            
                	$result = $this->Upload->upload($img, $targetFolder, $file_new_name, NULL, array('mov','mp4','wmv','flv','avi','swf','zip'));
					//upload vedio to aws server 
					if(!empty($result) && $ext !='zip'){
						$a=$this->importFiles($_POST['id']."/".$result,LESSON_VIDEOS_BUCKET, $targetFolder.$result);						
					}
				if($result && $ext !='zip')
				{
					
					$cmdThumb = '/opt/ffmpeg/bin/ffmpeg  -y -itsoffset -6  -i '.FILE_PATH.'app/webroot/files/members/'.$_POST['id']."/".$file_new_name.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 530X298 '.FILE_PATH.'app/webroot/files/members/'.$_POST['id']."/".$file_name.'_thumb.png 2>&1';	
					$cmdSmallThumb = '/opt/ffmpeg/bin/ffmpeg  -y -itsoffset -6  -i '.FILE_PATH.'app/webroot/files/members/'.$_POST['id']."/".$file_new_name.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 262X138 '.FILE_PATH.'app/webroot/files/members/'.$_POST['id']."/".$file_name.'_smallThumb.png 2>&1';
					$cmdCatalogue = '/opt/ffmpeg/bin/ffmpeg  -y -itsoffset -6  -i '.FILE_PATH.'app/webroot/files/members/'.$_POST['id']."/".$file_new_name.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 256X144 '.FILE_PATH.'app/webroot/files/members/'.$_POST['id']."/".$file_name.'_catalogueThumb.png 2>&1';
	
					exec($cmdSmallThumb);
					exec($cmdCatalogue);
					exec($cmdThumb,$thumbResponse, $thumbErr);
					if($thumbErr == 0){						
							$getDuration = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/members/'.$_POST['id']."/".$file_new_name.'  2>&1 | grep Duration';
							exec($getDuration, $getDurationResponse, $getDurationErr);							
							$preview_cmd = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/members/'.$_POST['id'].'/'.$file_new_name.' -ss 00:00:0.0 -t 00:01:0.0 -acodec copy -vcodec copy '.FILE_PATH.'app/webroot/files/members/'.$_POST['id'].'/'.$file_name.'_preview.'.$ext;							
							exec($preview_cmd, $preview_cmdResponse, $preview_cmdErr);
							if($getDurationErr == 0){
								$search = '/Duration: (.*?)[.]/';
								$duration = preg_match($search, $getDurationResponse[0], $matches, PREG_OFFSET_CAPTURE);							
								$duration = $matches[1][0];
								list($hours, $mins, $secs) = split('[:]', $duration);		
								$lesson['CourseLesson']['hours'] = $hours;
								$lesson['CourseLesson']['mins'] = $mins;
								$lesson['CourseLesson']['secs'] = $secs;
								$curr_lesson_duration = ($hours*60*60)+$mins*60+$secs;
							}							
								// After getting vedio thumnails and duration delete the files form Kuulum server
								$key_thumb=$_POST['id'].'/'.$file_name.'_thumb.png';
								$key_medium=$_POST['id'].'/'.$file_name.'_smallThumb.png';
								$key_catalogue=$_POST['id'].'/'.$file_name.'_catalogueThumb.png';
								$key=$_POST['id'].'/'.$file_name.'_preview.'.$ext;
								$this->importFiles($key, LESSON_VIDEOS_BUCKET,$targetFolder.$file_name.'_preview.'.$ext);
								$this->importFiles($key_thumb, LESSON_VIDEOS_BUCKET,$targetFolder.$file_name.'_thumb.png');
								$this->importFiles($key_medium, LESSON_VIDEOS_BUCKET, $targetFolder.$file_name.'_smallThumb.png');
								$this->importFiles($key_catalogue, LESSON_VIDEOS_BUCKET, $targetFolder.$file_name.'_catalogueThumb.png');
								@unlink('files/members/'.$_POST['id']."/".$result);
						
								@unlink($targetFolder.$file_name.'_preview.'.$ext);
								@unlink('files/members/'.$_POST['id']."/".$file_name.'_thumb.png');
								@unlink('files/members/'.$_POST['id']."/".$file_name.'_smallThumb.png');
								@unlink('files/members/'.$_POST['id']."/".$file_name.'_catalogueThumb.png');
								
							$this->loadModel('CourseLesson');
							$this->loadModel('Course');
							$this->loadModel('LessonWatermark');
                                                       
							if(isset($_POST['newid']) && !empty($_POST['newid'])) // Edit Case
							{	
                                                                $mem_id=$_POST['memid'];
								$decode_id = convert_uudecode(base64_decode($_POST['newid']));
								$lesson_id = convert_uudecode(base64_decode($_POST['newid']));
								$lesson['CourseLesson']['video'] =	$file_name;
								$lesson['CourseLesson']['extension'] =	$ext;
								if($ext=='zip'){
									$lesson['CourseLesson']['upload_zip_status']=1;
									}else{
										$lesson['CourseLesson']['upload_zip_status']=0;
										}
								$lesson['CourseLesson']['id'] = $lesson_id;
								$lesson['CourseLesson']['date_modified'] =	strtotime(date('Y-m-d H:i'));
							
								if($_POST['action'] !='1')
								{
									$prev_lesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$lesson_id)));
									$prev_lesson_duration =($prev_lesson['CourseLesson']['hours'])*60*60+($prev_lesson['CourseLesson']['mins'])*60 + $prev_lesson['CourseLesson']['secs'];
									$duration_of_course = $this->Course->field('duration_seconds',array('Course.id'=>$prev_lesson['CourseLesson']['course_id']));
									$new_duration = $duration_of_course - $prev_lesson_duration + $curr_lesson_duration;
									$this->Course->id=$prev_lesson['CourseLesson']['course_id'];
									$this->Course->saveField('duration_seconds',$new_duration);
								}
								$course_lesson=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'fields'=>array('hours','mins','secs')));
		
								$checkRecord = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'contain'=>false));		
								if($ext =='zip' && $checkRecord['CourseLesson']){
										
											//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUEs***********************
											$course_title = $this->Course->find('first',array('conditions'=>array('Course.id'=>$checkRecord['CourseLesson']['course_id']),'contain'=>false,'fields'=>array('title')));
											
											
											$lesson_title = $checkRecord['CourseLesson']['title'];
											$this->loadModel('Member');
											$mem_info = $this->Member->find('first',array('conditions'=>array('Member.id'=>$mem_id),'fields'=>array('given_name','email','locale'),'contain'=>false));
											$final_name = $mem_info['Member']['given_name'];
											$email = $mem_info['Member']['email'];
											$locale = $mem_info['Member']['locale']?$mem_info['Member']['locale']:'en';
											$course_title = $course_title['Course']['title'];
											$this->loadModel('EmailTemplate');
											$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>18,'locale'=>$locale),'fields'=>array('subject','description','email_from')));
											$emailData = $emailTemplate['EmailTemplate']['description'];
											$emailTo = $email;
											
											$emailData = str_replace(array('{final_name}','{lesson_title}','{course_title}'),array($final_name,$lesson_title,$course_title),$emailData);
											$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
											$emailSubject = $emailTemplate['EmailTemplate']['subject'];
											
											$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
											
											//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUES***********************	
											$this->loadModel('Notification');
											$this->before_update_notification();
											$notification['Notification']['mem_id'] =0;
											$notification['Notification']['notification_type'] = 'upload_progress';
											$notification['Notification']['lesson_id'] = $checkRecord['CourseLesson']['id'];			
											$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['notification_sender_id'] = $this->Session->read('LocTrain.id');
											$this->Notification->create();
											$this->Notification->save($notification);
									}
								if(!empty($checkRecord['CourseLesson']['video'])){

									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv",$mem_id."/".$checkRecord['CourseLesson']['video'].".flv",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",
										$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4",
										$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4",
										$mem_id."/".$checkRecord['CourseLesson']['video'].".swf",
										$mem_id."/".$checkRecord['CourseLesson']['video'].".swf");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}
	
									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4",$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$aa.= $this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}
									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].".wmv"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",$mem_id."/".$checkRecord['CourseLesson']['video'].".wmv",$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.wmv",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$aa.= $this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}
									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].".mov"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",$mem_id."/".$checkRecord['CourseLesson']['video'].".mov",$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mov",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}

									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}

									if($checkRecord['CourseLesson']['extension']=='zip')
									{
											$file_name1=$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'];
										// update the status delete_zip_status to 1 (only delete video from aws)				
										$this->CourseLesson->updateAll(array('CourseLesson.delete_zip_status'=>1,'CourseLesson.to_delete_video'=>"'".$file_name1."'"),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
										$this->loadModel('Notification');
													
										/*	if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video']))){
													$keys=array();
													$iter = new RecursiveDirectoryIterator('../webroot/files/members/'.$mem_id.'/'.$checkRecord['CourseLesson']['video']);
																foreach (new RecursiveIteratorIterator($iter) as $fileName => $fileInfo) {
																$info = new SplFileInfo($fileInfo);
																	if(!empty($info->getExtension()))
																	{
																		$extense=	explode('../webroot/files/members/',$fileInfo);
																		$keys[]=$extense['1'];
																	}
																}
													$keys[]=$mem_id.'/'.$checkRecord['CourseLesson']['video'].'/video.html';
													$keys[]=$mem_id.'/'.$checkRecord['CourseLesson']['video'];					
													$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
												}*/
										}
										

										if($checkRecord['CourseLesson']['extension']=='swf')
										{
											if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))){

												$keys=array($mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'],$mem_id."/".$checkRecord['CourseLesson']['video'].'_preview.'.$checkRecord['CourseLesson']['extension']);
												$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

											}		
										
										}
										if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv")){
				
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".flv");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.swf");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".swf");
										}
			
										if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png")){
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mov");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".mov");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.wmv");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".wmv");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.swf");
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".swf");
										}
										if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png")){
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										}
										
										if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png")){
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png");
										}
			
										if($checkRecord['CourseLesson']['extension']=='zip')
										{
											if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']))
											{ 
												$path = realpath('../../app/webroot/files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']);                          
													  exec('rm -r '.$path);
												if($checkRecord['CourseLesson']['image'] && file_exists('files/members/course_lesson/'.$checkRecord['CourseLesson']['image']))
												{
												
													
												}
												
											}
											
										}
										if($checkRecord['CourseLesson']['extension']=='swf')
										{
											if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))
											{ 
												unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']);
												if($checkRecord['CourseLesson']['image'] && file_exists('files/members/course_lesson/'.$checkRecord['CourseLesson']['image']))
												{
													
												}												
											}
										}
										if($checkRecord['CourseLesson']['extension']=='zip' || $checkRecord['CourseLesson']['extension']=='swf'){
											/*$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.image'=>NULL,'CourseLesson.extension'=>NULL),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));*/$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.extension'=>NULL),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
										}else {
											$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.extension'=>NULL),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
										}
										$this->loadModel('Notification');
										$this->before_update_notification();
										$notification['Notification']['mem_id'] = 0;
										$notification['Notification']['notification_type'] = 'video_is_being_deleted';
										$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
										$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
										$notification['Notification']['notification_sender_id'] = $this->Session->read('LocTrain.id');
										$this->Notification->create();
										$this->Notification->save($notification);
								}else{
									
								}
								if($this->CourseLesson->save($lesson)){								
									
								}
							}else{	  $mem_id=$_POST['memid'];
                                                                $lesson['CourseLesson']['video'] =	$file_name;
								$lesson['CourseLesson']['extension'] =	$ext;
								$lesson['CourseLesson']['date_added'] =	strtotime(date('Y-m-d H:i'));								
								
                                                                $lesson_title =$_POST['title'];
                                                                $lesson_description = $_POST['description'];
                                                               	$lesson_course_id=$_POST['course_id'];
  								$checkRecordLess = $this->CourseLesson->find('first',array('conditions'=>
                                                                    array('CourseLesson.title'=>$lesson_title,'CourseLesson.description'=>$lesson_description,'CourseLesson.course_id'=>$lesson_course_id),'contain'=>false));	
                                                                if(!empty($checkRecordLess['CourseLesson'])){
                                                                    $lesson_id =$checkRecordLess['CourseLesson']['id'];
                                                                    $lesson['CourseLesson']['id'] =$lesson_id;
                                                                    $this->CourseLesson->save($lesson);
                                                                }else{
                                                                    $this->CourseLesson->save($lesson);
                                                                    $lesson_id = $this->CourseLesson->getlastInsertId();
                                                                }
							}
							$resp = array('res'=>$result,'s'=>$fileParts['filename'],'url'=>$this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$file_name.".".$ext),'exe'=>$this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$file_name.".".$ext)),'extension'=>$ext,'status'=>'true','file'=>$file_name,'id'=>base64_encode(convert_uuencode($lesson_id)));
							echo json_encode($resp);die;
					}
				} else {
				
						$this->loadModel('CourseLesson');
							$this->loadModel('Course');
							$this->loadModel('LessonWatermark');
                                                        
							if(isset($_POST['newid']) && !empty($_POST['newid'])) // Edit Case
							{	$mem_id=$_POST['memid'];
								$decode_id = convert_uudecode(base64_decode($_POST['newid']));
								$lesson_id = convert_uudecode(base64_decode($_POST['newid']));
								$lesson['CourseLesson']['video'] =	$file_name;
								$lesson['CourseLesson']['extension'] =	$ext;
								if($ext=='zip'){
									$lesson['CourseLesson']['upload_zip_status']=1;
								}else{
									$lesson['CourseLesson']['upload_zip_status']=0;
								}
								$lesson['CourseLesson']['id'] = $lesson_id;
								$lesson['CourseLesson']['date_modified'] =	strtotime(date('Y-m-d H:i'));
							
								$course_lesson=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'fields'=>array('hours','mins','secs')));
		
								$checkRecord = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'contain'=>false));		
								if($ext =='zip' && $checkRecord['CourseLesson']){
										
											//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUEs***********************
											$course_title = $this->Course->find('first',array('conditions'=>array('Course.id'=>$checkRecord['CourseLesson']['course_id']),'contain'=>false,'fields'=>array('title')));
											
											
											$lesson_title = $checkRecord['CourseLesson']['title'];
											$this->loadModel('Member');
											$mem_info = $this->Member->find('first',array('conditions'=>array('Member.id'=>$mem_id),'fields'=>array('given_name','email','locale'),'contain'=>false));
											$final_name = $mem_info['Member']['given_name'];
											$email = $mem_info['Member']['email'];
											$locale = $mem_info['Member']['locale']?$mem_info['Member']['locale']:'en';
											$course_title = $course_title['Course']['title'];
											$this->loadModel('EmailTemplate');
											$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>18,'locale'=>$locale),'fields'=>array('subject','description','email_from')));
											$emailData = $emailTemplate['EmailTemplate']['description'];
											$emailTo = $email;
											
											$emailData = str_replace(array('{final_name}','{lesson_title}','{course_title}'),array($final_name,$lesson_title,$course_title),$emailData);
											$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
											$emailSubject = $emailTemplate['EmailTemplate']['subject'];
											
											$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
											
											//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUES***********************	
											$this->loadModel('Notification');
											$this->before_update_notification();
											$notification['Notification']['mem_id'] =0;
											$notification['Notification']['notification_type'] = 'upload_progress';
											$notification['Notification']['lesson_id'] = $checkRecord['CourseLesson']['id'];			
											$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['notification_sender_id'] = $this->Session->read('LocTrain.id');
											$this->Notification->create();
											$this->Notification->save($notification);
									}
								if(!empty($checkRecord['CourseLesson']['video'])){

									
									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv",$mem_id."/".$checkRecord['CourseLesson']['video'].".flv",$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png",
										$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4",
										$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4",
										$mem_id."/".$checkRecord['CourseLesson']['video'].".swf",
										$mem_id."/".$checkRecord['CourseLesson']['video'].".swf");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}
	
									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4",$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}
									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].".mov"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",$mem_id."/".$checkRecord['CourseLesson']['video'].".mov",$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mov",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

									}
									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].".wmv"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png",$mem_id."/".$checkRecord['CourseLesson']['video'].".wmv",$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.wmv",$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
									}

									if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png"))){

										$keys=array($mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
									}

									if($checkRecord['CourseLesson']['extension']=='zip')
									{
				
										$file_name1=$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'];
										// update the status delete_zip_status to 1 (only delete video from aws)				
										$this->CourseLesson->updateAll(array('CourseLesson.delete_zip_status'=>1,'CourseLesson.to_delete_video'=>"'".$file_name1."'"),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
													
										/*if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video']))){
											$keys=array();
											$iter = new RecursiveDirectoryIterator('../webroot/files/members/'.$mem_id.'/'.$checkRecord['CourseLesson']['video']);
											foreach (new RecursiveIteratorIterator($iter) as $fileName => $fileInfo) {
												$info = new SplFileInfo($fileInfo);
												if(!empty($info->getExtension()))
												{
													$extense=	explode('../webroot/files/members/',$fileInfo);
													$keys[]=$extense['1'];
												}
											}
											$keys[]=$mem_id.'/'.$checkRecord['CourseLesson']['video'].'/video.html';
											$keys[]=$mem_id.'/'.$checkRecord['CourseLesson']['video'];
											$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
										}*/
									}
									
									if($checkRecord['CourseLesson']['extension']=='swf')
									{

										if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))){

											$keys=array($mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'],$mem_id."/".$checkRecord['CourseLesson']['video'].'_preview.'.$checkRecord['CourseLesson']['extension']);
											$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
										}			
											
									}
									if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv")){
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.flv");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".flv");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_watermark.flv");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.swf");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".swf");
									}
										
									if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png")){
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_thumb.png");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".mp4");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mp4");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.mov");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".mov");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.wmv");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".wmv");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_preview.swf");
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].".swf");
									}
									
									if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png")){
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
									}
									
									if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png")){
										unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png");
									}
									
									if($checkRecord['CourseLesson']['extension']=='zip')
									{
										if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']))
										{ 
											$path = realpath('../../app/webroot/files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video']);                          
											exec('rm -r '.$path);
											if($checkRecord['CourseLesson']['image'] && file_exists('files/members/course_lesson/'.$checkRecord['CourseLesson']['image']))
											{						
											}
											
										}
										
									}
									if($checkRecord['CourseLesson']['extension']=='swf')
									{
										if(file_exists('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))
										{ 
											unlink('files/members/'.$mem_id."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']);
											if($checkRecord['CourseLesson']['image'] && file_exists('files/members/course_lesson/'.$checkRecord['CourseLesson']['image']))
											{
											}											
										}	
									}
									if($checkRecord['CourseLesson']['extension']=='zip' || $checkRecord['CourseLesson']['extension']=='swf'){
										/*$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.image'=>NULL,'CourseLesson.extension'=>NULL),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));*/$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.extension'=>NULL),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
									}else {
										$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
									}
								//$this->Course->id=$cource_decode_id;
								//$this->Course->saveField('duration_seconds', $new_duration );
										$this->loadModel('Notification');
										$this->before_update_notification();
										$notification['Notification']['mem_id'] =0;
										$notification['Notification']['notification_type'] = 'video_is_being_deleted';
										$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
										$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
										$notification['Notification']['notification_sender_id'] = $this->Session->read('LocTrain.id');
										$this->Notification->create();
										$this->Notification->save($notification);
								}else{
									
								}
		
								if($this->CourseLesson->save($lesson)){	
																
								}
							}else{
								  $mem_id=$_POST['memid'];
								$lesson['CourseLesson']['video'] =	$file_name;
								$lesson['CourseLesson']['extension'] =	$ext;
								$lesson['CourseLesson']['date_added'] =	strtotime(date('Y-m-d H:i'));								
								
                                                                $lesson_title =$_POST['title'];
                                                                $lesson_description = $_POST['description'];
								$lesson_course_id=$_POST['course_id'];
                                                                $checkRecordLess = $this->CourseLesson->find('first',array('conditions'=>
                                                                    array('CourseLesson.title'=>$lesson_title,'CourseLesson.description'=>$lesson_description,'CourseLesson.course_id'=>$lesson_course_id),'contain'=>false));		
                                                                if(!empty($checkRecordLess['CourseLesson'])){
                                                                    $lesson_id =$checkRecordLess['CourseLesson']['id'];
                                                                    $lesson['CourseLesson']['id'] =$lesson_id;
                                                                    $this->CourseLesson->save($lesson);
                                                                }else{
                                                                    $this->CourseLesson->save($lesson);
                                                                    $lesson_id = $this->CourseLesson->getlastInsertId();
                                                                }
							}
								
							$resp = array('res'=>$result,'s2'=>$fileParts['filename'],'extension'=>$ext,'status'=>'true','file'=>$file_name,'id'=>base64_encode(convert_uuencode($lesson_id)));
							echo json_encode($resp);die;
                }				
			} else {
				echo "false";
				$resp = array('status'=>'false');
				echo json_encode($resp);
			}
		}	
		die;
	}
	
	
	
	function paypal_ipn_update_purchase()
	{	
		// STEP 1: Read POST data
	
		// reading posted data from directly from $_POST causes serialization 
		// issues with array data in POST
		// reading raw POST data from input stream instead. 
		
		
		
		$str='';
		foreach($_POST as $key=>$din)
		{
			$str.=$key."  ".$din."\n\n";
		}
		
		
		/*$item_name ='Course+purchase&mc_currency=USD';
			$member_id =139;
			$payment_status ='Completed';
			$payment_amount =599.00;
			$payment_currency ='USD';
			$txn_id ='084897175V751193N';
			$receiver_email ='bc1%40lt.com';
			$payer_email ='akhleshkumar111%40gmail.com';
			$str_cource_ids ='less_item0-124-,less_item1-125-,less_item2-126-,less_item3-127-,item4-48-,less_item3-127-,item4-48-';
			$c_ids= explode(',',$str_cource_ids);
			$this->loadModel('CoursePurchase');
			$this->loadModel('CoursePurchaseList');
			$this->loadModel('CourseLesson');*/
			
		
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
	
		foreach ($raw_post_array as $keyval) {
		  $keyval = explode ('=', $keyval);
		  if (count($keyval) == 2)
			 $myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
		   $get_magic_quotes_exists = true;
		} 
		foreach ($myPost as $key => $value) {        
		   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
				$value = urlencode(stripslashes($value)); 
		   } else {
				$value = urlencode($value);
		   }
		   $req .= "&$key=$value";
		}
		
		 
		
                    // Set up request to PayPal
                    $ipn_post_data = $req;
                   
		$this->loadModel('Paypal');
		$info=$this->Paypal->find('first');
		$state=$info['Paypal']['paypal_status'];
		if($state)
		{
			 $p_url =SANDBOX_URL;
				
		}
		else
		{
			 $p_url =LIVE_URL;
		
		}
                    $request = curl_init();
                    curl_setopt_array($request, array
                    (
                        CURLOPT_URL => $p_url,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS =>$ipn_post_data ,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_SSL_VERIFYPEER => TRUE,
                        CURLOPT_CAINFO =>  dirname(__FILE__) . '/cacert.pem',
                    ));
                    
                    // Execute request and get response and status code
                    $response = curl_exec($request);
                    $status   = curl_getinfo($request, CURLINFO_HTTP_CODE);                   
                    // Close connection
                    curl_close($request);
                
		// STEP 3: Inspect IPN validation result and act accordingly
		
		/*if (strcmp ($res, "VERIFIED") == 0) {*/
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your Primary PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment
		 
			// assign posted variables to local variables
                    if($status == 200 && $response == 'VERIFIED')
                    {//Inspect IPN validation result is verified.Then Proceed...
                                       
			$item_name = $_POST['item_name'];
                       // $item_number=explode('-',$item_name);
			//$mem_commission = explode(',',$item_number[1]);
                  	$item_num_get=explode('*',$_POST['custom']);
			
                       
			
			$item_number_new=explode(',',$item_num_get[1]);
			
			//$item_number=explode('-',$item_num_get[1]);
			$mem_commission = explode(',',$item_number[1]);
			
			//$member_id = $mem_commission[0];
			//$commision_rat = $mem_commission[1];
            $member_id = $item_number_new[0];
			$commision_rat = $item_number_new[1];

			if(count($item_num_get)>2)
			{
				$discount=$item_num_get[2];
			}
			else
			{
				$discount=0;
			}


			$payment_status = $_POST['payment_status'];
			$payment_amount = $_POST['mc_gross'];
			$payment_currency = $_POST['mc_currency'];
			$txn_id = $_POST['txn_id'];
			$receiver_email = $_POST['receiver_email'];
			$payer_email = $_POST['payer_email']; 
			//$str_cource_ids = $_POST['custom'];
			$str_cource_ids = $item_num_get[0];
                      //  $str_cource_ids = $item_num_get[0];

			$c_ids= explode(',',$str_cource_ids);
			$this->loadModel('CoursePurchase');
			$this->loadModel('CoursePurchaseList');
			$this->loadModel('CourseLesson');
			$this->loadModel('Member');
			$this->loadModel('Course');
			$this->loadModel('Notification');
			$this->loadModel('Commission');
			$this->loadModel('Courses');
			$this->loadModel('Language');
			$this->loadModel('Defaultcommission');
			if(strtolower(trim($payment_status)) =='completed')
			{		
				/* foreach ($_POST as $key => $value){
				
					$emailtext .= $key . " = " .$value ."\n\n";
				}*/
				
				$getRecord = $this->CoursePurchase->find('count',array('conditions'=>array('CoursePurchase.txn_id'=>$txn_id)));
				
				if($getRecord == 0)
				{
					$deduct_commission = $payment_amount * $commision_rat/100;
					$deduct_commission = number_format($deduct_commission,'2','.','');
					
					$date=$this->CoursePurchase->create();
					$data['CoursePurchase']['m_id']= $member_id;
					$data['CoursePurchase']['payment_status']= $payment_status;
					$data['CoursePurchase']['payment_amount']= $payment_amount;
					$data['CoursePurchase']['commission_rate']= $commision_rat;
					$data['CoursePurchase']['deduct_commission']= $deduct_commission;
					$data['CoursePurchase']['discount']= $discount;
					$data['CoursePurchase']['txn_id'] = $txn_id;
					$data['CoursePurchase']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					try{					
						if($this->CoursePurchase->save($data))
						{	
							  $p_id=$this->CoursePurchase->getLastInsertID();
							  foreach($c_ids as $value)
							  {
									$v=explode('-',$value);
									$type=$v[0];
									$val=$v[1];
									
									
									$locale=$this->Member->find('all',array('conditions'=>array('Member.id'=>$member_id),'fields'=>array('locale')));
										
										$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$locale[0]['Member']['locale']),'fields'=>array('lang_code')));
										$language1=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$locale[0]['Member']['locale']),'fields'=>array('id')));
										
										
										
										$this->Session->write('LocTrain.locale',$locale[0]['Member']['locale']);
										$this->Session->write('Config.language',$language[0]['Language']['lang_code']);
										$this->Session->write('LocTrain.LangId',$language1[0]['Language']['id']);
								  //if the cource was previously in the my learning (type==1)
									if(strlen($type)<8)
									{
										$prev = $this->CoursePurchaseList->find('first',array('conditions'=>array('CoursePurchaseList.c_id'=>$val,'CoursePurchaseList.mem_id'=>$member_id, 'CoursePurchaseList.type'=>1)));
											  if(!empty($prev))
											  {
												 $this->CoursePurchaseList->delete($prev['CoursePurchaseList']['id']);
											  }
											  
											$memberid=$this->Courses->find('all',array('conditions'=>array('Courses.id'=>$val),'fields'=>array('m_id')));
										
											  $commissions=$this->Commission->find('all',array('conditions'=>array('author_name'=>$memberid[0]['Courses']['m_id']),'order'=>array('Commission.id'=>'desc')));
											  
											   $defaultcommissions=$this->Defaultcommission->find('all');
											   
											  
											  
											 
										 if(!empty($commissions))
											 {
								foreach($commissions as $commission)
		{
			
			
			if($commission['Commission']['courses']==1 && $commission['Commission']['duration']==1)
			
			
			{
				if($val==$commission['Commission']['particular-course'])
				{
				$commfound=	$commission['Commission']['commission'];
				break;
				}
				
				else
				{
					$commfound=$defaultcommissions[0]['Defaultcommission']['commission'];	
					}
			
			}
			elseif($commission['Commission']['courses']==0 && $commission['Commission']['duration']==1){
				
				$commfound=$commission['Commission']['commission'];
				break;
			
			}
				elseif($commission['Commission']['courses']==1 && $commission['Commission']['duration']==0)
			
			
			{
	if($val==$commission['Commission']['particular-course'])
	{					
	$paymentDate = date('Y-m-d');
	
	$contractDateBegin = date('Y-m-d', strtotime($commission['Commission']['date_from']));
	
	$contractDateEnd = date('Y-m-d', strtotime($commission['Commission']['date_to']));
	
	if((strtotime($paymentDate) >= strtotime($contractDateBegin)) && (strtotime($paymentDate) <= strtotime($contractDateEnd)))
	{
	$commfound=	$commission['Commission']['commission'];
	break;
	}
	else
	{
	$commfound=	$defaultcommissions[0]['Defaultcommission']['commission'];
	}				
	}
	else
	{
	$commfound=$defaultcommissions[0]['Defaultcommission']['commission'];	
	}
	}
			elseif($commission['Commission']['courses']==0 && $commission['Commission']['duration']==0){
				
	$paymentDate = date('Y-m-d');
	
	$contractDateBegin = date('Y-m-d', strtotime($commission['Commission']['date_from']));
	 
	$contractDateEnd = date('Y-m-d', strtotime($commission['Commission']['date_to']));
	
					
	if((strtotime($paymentDate) >= strtotime($contractDateBegin)) && (strtotime($paymentDate) <= strtotime($contractDateEnd)))
	{
	$commfound=	$commission['Commission']['commission'];
	break;
	}
	else
	{
		 $commfound=	$defaultcommissions[0]['Defaultcommission']['commission'];
		}
  
			
			}
			else
			{
			$commfound=$defaultcommissions[0]['Defaultcommission']['commission'];
			}
			}
			}	
			else
										 {
											 $commfound=$defaultcommissions[0]['Defaultcommission']['commission'];
											 }
										 
											//$purchasedprice=$this->Courses->find('first',array('conditions'=>array('Courses.id'=>$val)));
											 $purchasedprice = $this->Course->find('first',array('conditions'=>array('Course.id'=>$val),'contain'=>array('CourseLesson','CoursePurchaseList'=>array('fields'=>array('mem_id','lesson_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$member_id)))));
											//$discountper=($discount*100)/($discount+$payment_amount);
											 $minus_price=0;
											  if(!empty($purchasedprice['CoursePurchaseList'])){// If previously purchased a lesson of a course,then  minus the cost  have already paid.
												  
												  foreach($purchasedprice['CoursePurchaseList'] as $list){
													  if($list['lesson_id']){
															foreach($purchasedprice['CourseLesson'] as $k=>$les){
																if($les['id']==$list['lesson_id']){
																	$key=$k;																	
																	$minus_price+=$purchasedprice['CourseLesson'][$key]['price'];
																	}
																}
															
													  }
												  }
											  }
											 
											$coursediscount=($purchasedprice['Course']['price']-$minus_price)*$discount/100;
											 
											/*if($coursediscount<1)
											{
											$coursediscount=(string)$coursediscount;
											$coursediscount=substr($coursediscount,0,4);	
											}
											else
											{
											$coursediscount=round($coursediscount,2);
											}*/
											
											 $c_p_list['CoursePurchaseList']['purchase_id']=$p_id;
											 $c_p_list['CoursePurchaseList']['c_id']=$val;
											 $c_p_list['CoursePurchaseList']['lesson_id']=NULL;
											
											 $c_p_list['CoursePurchaseList']['purchasedprice']= $purchasedprice['Course']['price']-$minus_price;
											 $c_p_list['CoursePurchaseList']['discount']= $coursediscount;
											 
											 $c_p_list['CoursePurchaseList']['mem_id']=$member_id;
											 $c_p_list['CoursePurchaseList']['type']=0;
											 $c_p_list['CoursePurchaseList']['commission']= $commfound;
											 $c_p_list['CoursePurchaseList']['date_created']= strtotime(date('Y-m-d H:i:s'));
											
											 $this->CoursePurchaseList->create(); 
											 $this->CoursePurchaseList->save($c_p_list);
											  
											 //*** Notification for member *****
											
											
											$course_owner = $this->Course->find('first',array('conditions'=>array('Course.id'=>$val),'fields'=>array('id','m_id'),'contain'=>false));
											$course_owner_id = $course_owner['Course']['m_id'];
											
											$notification['Notification']['mem_id'] =  $member_id;
											$notification['Notification']['notification_type'] = 'course_purchased';
											$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['c_id'] = $val;
											$notification['Notification']['notification_sender_id'] = $course_owner_id;
											$this->Notification->create();
											$this->Notification->save($notification); 
											  
											 //***End Notification for member *****  
											  
											  
									}
									else
									{
										$course_lesson=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$val)));
										
										$prev = $this->CoursePurchaseList->find('first',array('conditions'=>array('CoursePurchaseList.c_id'=>$course_lesson['CourseLesson']['course_id'],'CoursePurchaseList.lesson_id'=>$val, 'CoursePurchaseList.type'=>1)));
										if(!empty($prev))
										{
											$this->CoursePurchaseList->delete($prev['CoursePurchaseList']['id']);
										}
											  
											  
											
												$memberid=$this->Courses->find('all',array('conditions'=>array('Courses.id'=>$course_lesson['CourseLesson']['course_id']),'fields'=>array('m_id')));
										
											  $commissions=$this->Commission->find('all',array('conditions'=>array('author_name'=>$memberid[0]['Courses']['m_id']),'order'=>array('Commission.id'=>'desc')));
											  
											   $defaultcommissions=$this->Defaultcommission->find('all');
										 if(!empty($commissions))
											 {
												 
												  
								foreach($commissions as $commission)
		{
			 
			
			if($commission['Commission']['courses']==1 && $commission['Commission']['duration']==1)
			
			
			{
				if($course_lesson['CourseLesson']['course_id']==$commission['Commission']['particular-course'])
				{
				$commfound=	$commission['Commission']['commission'];
				break;
				}
				else
				{
					$commfound=$defaultcommissions[0]['Defaultcommission']['commission'];
					}
			
			}
			elseif($commission['Commission']['courses']==0 && $commission['Commission']['duration']==1){
				
				$commfound=$commission['Commission']['commission'];
				break;
			
			}
				elseif($commission['Commission']['courses']==1 && $commission['Commission']['duration']==0)
			
			
			{
				
				
				
				if($course_lesson['CourseLesson']['course_id']==$commission['Commission']['particular-course'])
				{
				$paymentDate = date('Y-m-d');
	
	$contractDateBegin = date('Y-m-d', strtotime($commission['Commission']['date_from']));
	
	$contractDateEnd = date('Y-m-d', strtotime($commission['Commission']['date_to']));
	
	if((strtotime($paymentDate) >= strtotime($contractDateBegin)) && (strtotime($paymentDate) <= strtotime($contractDateEnd)))
	{
	 $commfound=	$commission['Commission']['commission'];
	break;
	}
	else
	{
		 $commfound=	$defaultcommissions[0]['Defaultcommission']['commission'];
		}
	
				
				}
				else
				{
					$commfound=$defaultcommissions[0]['Defaultcommission']['commission'];
					}
			
			}
			elseif($commission['Commission']['courses']==0 && $commission['Commission']['duration']==0){
				
			$paymentDate = date('Y-m-d');
	
	$contractDateBegin = date('Y-m-d', strtotime($commission['Commission']['date_from']));
	
	$contractDateEnd = date('Y-m-d', strtotime($commission['Commission']['date_to']));
	
	if((strtotime($paymentDate) >= strtotime($contractDateBegin)) && (strtotime($paymentDate) <= strtotime($contractDateEnd)))
	{
	$commfound=	$commission['Commission']['commission'];
	break;
	}
	else
	{
		 $commfound=	$defaultcommissions[0]['Defaultcommission']['commission'];
		}

			}
			
			else
			{
			$commfound=$defaultcommissions[0]['Defaultcommission']['commission'];
			}
			}
			}	 
										 else
										 {
											 
											
											 $commfound=$defaultcommissions[0]['Defaultcommission']['commission'];
											 }
											 
											  //$discountper=($discount*100)/($discount+$payment_amount);
											  $lessondiscount=$course_lesson['CourseLesson']['price']*$discount/100;
											
											/*	if($lessondiscount<1)
											{
											$lessondiscount=(string)$lessondiscount;
											$lessondiscount=substr($lessondiscount,0,4);	
											}
											else
											{
											$lessondiscount=round($lessondiscount,2);
											}*/
											  $c_p_list['CoursePurchaseList']['purchase_id']=$p_id;
											  $c_p_list['CoursePurchaseList']['c_id']=$course_lesson['CourseLesson']['course_id'];
											  $c_p_list['CoursePurchaseList']['lesson_id']=$val;
											  $c_p_list['CoursePurchaseList']['purchasedprice']= $course_lesson['CourseLesson']['price'];
											  $c_p_list['CoursePurchaseList']['mem_id']=$member_id;
											  $c_p_list['CoursePurchaseList']['commission']= $commfound;
											  $c_p_list['CoursePurchaseList']['discount']= $lessondiscount;
											  $c_p_list['CoursePurchaseList']['type']=0;
											  $c_p_list['CoursePurchaseList']['date_created']= strtotime(date('Y-m-d H:i:s'));
											  $this->CoursePurchaseList->create();
											  $this->CoursePurchaseList->save($c_p_list);
											  
										
											   //*** Notification for member *****
											
											$this->loadModel('Notification');
											$course_owner = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_lesson['CourseLesson']['course_id']),'fields'=>array('id','m_id'),'contain'=>false));
											$course_owner_id = $course_owner['Course']['m_id'];
											
											$course_owner_name = $this->Member->find('first',array('conditions'=>array('Member.id'=>$course_owner_id),'fields'=>array('given_name','id'),'contain'=>false));
											
											$notification['Notification']['mem_id'] =  $member_id;
											$notification['Notification']['notification_type'] = 'lesson_purchased';
											$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
											$notification['Notification']['c_id'] = $course_lesson['CourseLesson']['course_id'];
											$notification['Notification']['lesson_id'] = $val;
											$notification['Notification']['notification_sender_id'] = $course_owner_name['Member']['id'];
											$this->Notification->create();
											$this->Notification->save($notification); 
											  
											 //***End Notification for member *****  
											  
											  
									}
							  }  
						}						
					}catch(Exception $e){		
						
						$error = ($e->getMessage().'<pre>'.$e->getTraceAsString().'</pre>');
						//pr($error);
					}
				}
					
					
					$this->loadModel('EmailTemplate');
					$this->loadModel('Member');
					$mem_id=$member_id;
					$mem = $this->Member->find('first',array('conditions'=>array('Member.id'=>$mem_id),'contain'=>false,'fields'=>array('email','final_name','given_name','id','locale')));


					$email = $mem['Member']['email'];
					$final_name = $mem['Member']['final_name'];		
					$given_name = $mem['Member']['given_name'];
					$m_locale = $mem['Member']['locale']? $mem['Member']['locale']:'en';
					//$today_date = date('d-M-Y');

                    $date = date("Y-m-d H:i:s");
					$today_date = strtotime($date);
					
					if($m_locale == $this->Session->read('LocTrain.locale'))
					{
						App::import('Helper', 'Timezone'); 
					 $timezone = new TimezoneHelper(new View(null));
					$today_date=$timezone ->dateFormatAccoringLocaleLangNew($this->Session->read('Config.language'),$today_date);  
						//setlocale(LC_ALL, "it_IT",'italian');
						//$today_date = strftime('%d %B %Y',$today_date);
						
					}
					
										
					$grand_total = 0;
					$items = '';
					$lessonTitle='';
					$amount='';
					$items_aar=array();$i=0;
					foreach($c_ids as $value)
					{

						$v=explode('-',$value);							
						$type=$v[0];
						$val=$v[1];
						$lessonTitle='';

						$this->loadModel('CoursePurchaseList');
						
						$count = $this->CoursePurchaseList->find('count',array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_id,'CoursePurchaseList.lesson_id'=>$val,'CoursePurchaseList.type'=>0)));
						
						
						
						
						//pr($val);
						//pr($count);
						if($count > 0)
						{
							$info = $this->CoursePurchaseList->find('first',array('conditions'=>array('CoursePurchaseList.lesson_id'=>$val,'CoursePurchaseList.mem_id'=>$mem_id,'CoursePurchaseList.type'=>0),'contain'=>array('CourseLesson'=>array('title','price','description'),'Course'=>array('title','price','description'))));
							$lessonTitle = $info['CourseLesson']['title'];
							
							
							$lessonDescription =$info['CourseLesson']['description'];
							$amount =$info['CourseLesson']['price'];
							$grand_total = $grand_total + $amount;
						
						}else
						{
							$info = $this->CoursePurchaseList->find('first',array('conditions'=>array('CoursePurchaseList.c_id'=>$val,'CoursePurchaseList.mem_id'=>$mem_id,'CoursePurchaseList.type'=>0),'contain'=>array('CourseLesson','Course'=>array('title','price','description'))));
							$course = $this->Course->find('first',array('conditions'=>array('Course.id'=>$val),'contain'=>array('CourseLesson','CoursePurchaseList'=>array('fields'=>array('mem_id','lesson_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$mem_id,'CoursePurchaseList.lesson_id !='=>'')))));
							$minus_price=0;
							  if(!empty($course['CoursePurchaseList'])){// If previously purchased a lesson of a course,then  minus the cost  have already paid.
								  
								  foreach($course['CoursePurchaseList'] as $list){
									  if($list['lesson_id']){
										  foreach($course['CourseLesson'] as $k=>$les){
												if($les['id']==$list['lesson_id']){
													$key=$k;
													$minus_price+=$course['CourseLesson'][$key]['price'];													
												}
											  }
											
									  }
								  }
							  }
							
							if($m_locale == $this->Session->read('LocTrain.locale'))
							{	
								$lessonTitle=__('all_lessons');
							}
							$lessonDescription =$info['Course']['description'];
							$amount = $info['Course']['price']-$minus_price;
							$grand_total = $grand_total + $amount;
						
						}

						if($m_locale == $this->Session->read('LocTrain.locale'))
						{	
							$m_value=__('usd');
						}
						$items_aar[$i]['m_value']=$m_value;
						$items_aar[$i]['amount']=$amount;
						$items_aar[$i]['lessonTitle']=$lessonTitle;
						$items_aar[$i]['lessonDescription']=$lessonDescription;
						$items_aar[$i]['title']=$info['Course']['title'];
						$i++;
						
					/*if($m_locale == 'ar')
					{
						$items.='<tr>
								<td style="text-align:right;">'.$m_value.' '. $amount.'</td>
								<td style="text-align:right;">'.$lessonTitle.'</td>
								<td style="text-align:right;">'.$lessonDescription.'</td>
								<td style="text-align:right;">'.$info['Course']['title'].'</td>
								</tr>';
						
					}else
					{
					
						$items.='<tr>
							<td>'.$info['Course']['title'].'</td>
							<td>'.$lessonTitle.'</td>
							<td>'.$lessonDescription.'</td>
							<td style="text-align:right;">'.$m_value.' '. $amount.'</td>
							</tr>';	
					
					}*/
				}
$sort = array(); 

foreach($items_aar as $k=>$v){ 
     $sort['title'][$k] = $v['title'];
    $sort['lessonTitle'][$k] = $v['lessonTitle'];
} 

array_multisort($sort['title'], SORT_ASC,$items_aar); 

				foreach($items_aar as $val){
					
					if($m_locale == 'ar')
					{
						$items.='<tr>
								<td style="text-align:right;">'.$val['m_value'].' '. $val['amount'].'</td>
								<td style="text-align:right;">'.$val['lessonTitle'].'</td>
								<td style="text-align:right;">'.$val['lessonDescription'].'</td>
								<td style="text-align:right;">'.$val['title'].'</td>
								</tr>';
						
					}else
					{
					
						$items.='<tr>
							<td>'.$val['title'].'</td>
							<td>'.$val['lessonTitle'].'</td>
							<td>'.$val['lessonDescription'].'</td>
							<td style="text-align:right;">'.$val['m_value'].' '. $val['amount'].'</td>
							</tr>';	
					
					}
					
					
					}
					
					
					$discount=0;

					if($payment_amount < $grand_total)
					{
						$discount=0.0;
						$discount=$grand_total-$payment_amount;
						$grand_total=$payment_amount;
					}
$grand_total=sprintf(" %1\$.2f",$grand_total);
$grand_total = $m_value.' '.$grand_total;
$discount=sprintf(" %1\$.2f",$discount);
$discount='-'.$m_value.' '.$discount;

						
// --------------------Transaction Confirmation To Member-----------------							
						$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>10,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
						$emailData = $emailTemplate['EmailTemplate']['description'];					
						//$emailData = str_replace(array('{final_name}','{member_name}','{purchase_date}','{txn_id}','{items}','{amount}','{grand_total}','{discount}'),array($final_name,$given_name,$today_date,$txn_id,$items,$payment_amount,$grand_total,$discount),$emailData);		
						
						if($m_locale == $this->Session->read('LocTrain.locale'))
						{
						$m_course=__('course');
    $m_lesson=__('lesson');
    $m_description=__('description');
    $m_price=__('price');
    $m_discount=__('discount');
    $m_total=__('total');
						 
						}
						
						
						if($m_locale == 'ar')
						{						
						$m_course=__('course');
    $m_lesson=__('lesson');
    $m_description=__('description');
    $m_price=__('price');
    $m_discount=__('discount');
    $m_total=__('total');
							
							$tabel = '<table border="1" cellpadding="1" cellspacing="1" style="width:800px">
										<tbody>
											<tr>
												
											
												<td style="text-align:right;" width="15%"><strong>'.$m_course.'</strong></td>
												<td style="text-align:right;" width="15%"><strong>'.$m_lesson.'</strong></td>													
												<td style="text-align:right;" width="50%"><strong>'.$m_description.'</strong></td>
												<td style="text-align:right;" width="20%"><strong>'.$m_price.'</strong></td>
												
											</tr>
											<tr>
												<td colspan="3">{items}</td>
											</tr>
											<tr>
												<td style="border-top: 2px solid; text-align:right; border-bottom: 2px solid;">{discount}</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td style="text-align:right;"><strong>'.$m_discount.'</strong></td>
												
											</tr>
											<tr>
												<td style="text-align:right;">{grand_total}</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td style="text-align:right;"><strong>'.$m_total.'</strong></td>
												
											</tr>
										</tbody>
									</table>';
						}else{
						
						$tabel = '<table border="1" cellpadding="1" cellspacing="1" style="width:800px">
										<tbody>
											<tr>
												<td width="15%"><strong>'.$m_course.'</strong></td>
												<td width="15%"><strong>'.$m_lesson.'</strong></td>
												<td width="50%"><strong>'.$m_description.'</strong></td>
												<td style="text-align:right;" width="15%"><strong>'.$m_price.'</strong></td>
											</tr>
											<tr>
												<td colspan="3">{items}</td>
											</tr>
											<tr>
												<td><strong>'.$m_discount.'</strong></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td style="border-top: 2px solid; border-bottom: 2px solid;text-align:right;">{discount}</td>
											</tr>
											<tr>
												<td><strong>'.$m_total.'</strong></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td style="text-align:right;">{grand_total}</td>
											</tr>
										</tbody>
									</table>';
						
						
						
						}	
		
						$tabel = str_replace(array('{items}','{amount}','{grand_total}','{discount}'),array($items,$payment_amount,$grand_total,$discount),$tabel);
						$emailData = str_replace(array('{final_name}','{member_name}','{purchase_date}','{txn_id}','{tebel_items_content}'),array($final_name,$given_name,$today_date,$txn_id,$tabel),$emailData);		
						//pr($emailData);die;
						$emailTo = $email;
						$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
						$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
						
						$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);

						
						
			} 
		/*else if (strcmp ($res, "INVALID") == 0) {
		
			// log for manual investigation
		}*/
        }
		die;
	}
	function paypal_ipn_update_subscription()
	{	
		
		// STEP 1: Read POST data
	
		// reading posted data from directly from $_POST causes serialization 
		// issues with array data in POST
		// reading raw POST data from input stream instead. 
		$str='';
		
		
		foreach($_POST as $key=>$din)
		{
			$str.=$key." = ".$din."\n\n";
		}
		
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
	
		foreach ($raw_post_array as $keyval) {
		  $keyval = explode ('=', $keyval);
		  if (count($keyval) == 2)
			 $myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
		   $get_magic_quotes_exists = true;
		} 
		foreach ($myPost as $key => $value) {        
		   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
				$value = urlencode(stripslashes($value)); 
		   } else {
				$value = urlencode($value);
		   }
		   $req .= "&$key=$value";
		}
		
		
		/*$item_name = 'Localization Subscription';
		$item_number = 15;
		$payment_status = 'Completed';
		$payment_amount = 49.99;
		$payment_currency = 'USD';
		$txn_id = '9VU50552AJsdfsxdddfs4f0ss269';
		$receiver_email = 'bc1@lt.com';
		$payer_email = 'akhleshkumar111@gmail.com';
		$member_id = 'IywzLFgKYAo=';
		$member_id = 132;*/
		// STEP 2: Post IPN data back to paypal to validate
		/*$ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
		//$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));*/
		 
		// In wamp like environments that do not come bundled with root authority certificates,
		// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
		// of the certificate as shown below.
		// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		/*if( !($res = curl_exec($ch)) ) {
			// error_log("Got " . curl_error($ch) . " when processing IPN data");
			curl_close($ch);
			exit;
		}
		curl_close($ch);*/	 
		// STEP 3: Inspect IPN validation result and act accordingly
		/*if (strcmp ($res, "VERIFIED") == 0) {
			*/
			
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your Primary PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment
		 
			// assign posted variables to local variables
			$item_name = $_POST['item_name'];
			$item_number = $_POST['item_number'];
			$payment_status = $_POST['payment_status'];
			$payment_amount = $_POST['mc_gross'];
			$payment_currency = $_POST['mc_currency'];
			$txn_id = $_POST['txn_id'];
			$receiver_email = $_POST['receiver_email'];
			$payer_email = $_POST['payer_email'];
			$member_id = $_POST['custom'];
			
			if(strtolower($payment_status) == 'completed'){	
				
				$this->loadModel('MemberSubscription');
				$this->loadModel('Price');
				$date = date("Y-m-d H:i:s");
				$m_Id=convert_uudecode(base64_decode($member_id));
				
				
				$getRecord = $this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.txn_id'=>$txn_id)));
				
				$subscribed=$this->MemberSubscription->find('first',array('conditions'=>array('MemberSubscription.m_id'=>$m_Id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
				
				if($getRecord == 0)
				{
				
				
					if(empty($subscribed))
					{
						
						$subscription_months=$this->Price->find('first',array('conditions'=>array('Price.id'=>$item_number)));
						
						$pay_for=$subscription_months['Price']['name_en'];
						$subscription_months=$subscription_months['Price']['no_of_months'];
							
							$expiry_date = strtotime($date."+ ".$subscription_months." month");						
							$subscription['MemberSubscription']['date_expired'] = $expiry_date;
					
						$subscription['MemberSubscription']['m_id'] = $m_Id;
						$subscription['MemberSubscription']['type'] = $item_number;
						$subscription['MemberSubscription']['payment_status'] = $payment_status;
						$subscription['MemberSubscription']['payment_amount'] = $payment_amount;
						$subscription['MemberSubscription']['txn_id'] = $txn_id;
						$subscription['MemberSubscription']['date_added'] = strtotime(date('Y-m-d h:i:s'));
						$subscription['MemberSubscription']['status'] = '1';
						
						try{
							$this->MemberSubscription->save($subscription);	

							$this->loadModel('EmailTemplate');
							$this->loadModel('Member');
							
							$mem = $this->Member->find('first',array('conditions'=>array('Member.id'=>$m_Id),'contain'=>false,'fields'=>array('email','final_name','given_name','locale')));
							$email = $mem['Member']['email'];
							$final_name = $mem['Member']['final_name'];		
							$given_name = $mem['Member']['given_name'];
							$m_locale = $mem['Member']['locale'];
							
							$today_date = date('d-M-Y');
// --------------------Transaction Confirmation To Member-----------------							
						
							$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>9,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
							$emailData = $emailTemplate['EmailTemplate']['description'];						
							$emailData = str_replace(array('{final_name}','{subscription_title}','{txn_id}','{amount}','{to_date}','{purchase_date}','{from_date}','{grand_total}','{member_name}'),array($final_name,$pay_for,$txn_id,$payment_amount,date('d-M-Y',$expiry_date),$today_date,$today_date,$payment_amount,$given_name),$emailData);							
							
							
							
							$emailTo = $email;
							$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
							$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
							$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);

						}catch(Exception $e){				
							$error = ($e->getMessage().'<pre>'.$e->getTraceAsString().'</pre>');
							
						}
						
					}
					else
					{
					
						$subscription_months=$this->Price->find('first',array('conditions'=>array('Price.id'=>$item_number)));
						
						
						
						$pay_for=$subscription_months['Price']['name_en'];
						
						$subscription_months=$subscription_months['Price']['no_of_months'];
						
						$subscribed_expiry_timestamp = $subscribed['MemberSubscription']['date_expired'];
						$Id =$subscribed['MemberSubscription']['id'];
						
						$date = date('Y-m-d H:i:s', $subscribed_expiry_timestamp);
						$ending_date = date('Y-m-d H:i:s', strtotime($date."+ ".$subscription_months." month"));
						$expiry_date = strtotime($ending_date);
						
						try{
							$this->MemberSubscription->updateAll(array('MemberSubscription.date_expired'=>'"'.$expiry_date.'"', 'MemberSubscription.txn_id'=>'"'.$txn_id.'"'),array('MemberSubscription.id'=>$Id));
							
							$this->loadModel('EmailTemplate');
							$this->loadModel('Member');
							
							$mem = $this->Member->find('first',array('conditions'=>array('Member.id'=>$m_Id),'contain'=>false,'fields'=>array('email','final_name','given_name','locale')));
							
							$start = date('d-M-Y',$subscribed['MemberSubscription']['date_added']);
							
							$email = $mem['Member']['email'];
							$final_name = $mem['Member']['final_name'];
							$given_name = $mem['Member']['given_name'];
							$m_locale = $mem['Member']['locale'];
							$purchase_date = date('d-M-Y');
							
// --------------------Transaction Confirmation To Member-----------------							
							
							$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>9,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
							$emailData = $emailTemplate['EmailTemplate']['description'];						
							$emailData = str_replace(array('{final_name}','{subscription_title}','{txn_id}','{amount}','{to_date}','{purchase_date}','{from_date}','{grand_total}','{member_name}'),array($final_name,$pay_for,$txn_id,$payment_amount,date('d-M-Y',$expiry_date),$purchase_date,$start,$payment_amount,$given_name),$emailData);							
							
							$emailTo = $email;
							$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
							$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
							$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
							
						}catch(Exception $e){				
							$error = ($e->getMessage().'<pre>'.$e->getTraceAsString().'</pre>');
							
						}		
					}	
				}
			}
		/*} else if (strcmp ($res, "INVALID") == 0) {
			// log for manual investigation
		}*/
		die;
	}
	
	function my_cronjob()
	{
	
			$this->loadModel('Watermark');
			$this->loadModel('CourseLesson');
			$lesson = $this->Watermark->find('first',array('contain'=>array('CourseLesson'=>array('Course'=>array('Member')))));
			if(!empty($lesson) && !empty($lesson['CourseLesson']))
			{
				
				$record_id=$lesson['Watermark']['id'];
				$folder=$lesson['CourseLesson']['Course']['m_id'];
				$ext=$lesson['CourseLesson']['extension'];
				$video=$lesson['Watermark']['video'];
				$email=$lesson['CourseLesson']['Course']['Member']['email'];
				$m_locale=$lesson['CourseLesson']['Course']['Member']['locale'];
				$final_name =$lesson['CourseLesson']['Course']['Member']['final_name'];
				$lesson_title= $lesson['CourseLesson']['title'];
				$lesson_id= $lesson['CourseLesson']['id'];
				$course_title= $lesson['CourseLesson']['Course']['title'];
				if(file_exists(''.FILE_PATH.'app/webroot/files/members/'.$folder.'/'.$video.'.'.$ext))
				{
						//$cmdWatermark = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/members/'.$folder.'/'.$video.'.'.$ext.' -i '.FILE_PATH.'app/webroot/files/logo.png  -filter_complex overlay=10:10 '.FILE_PATH.'app/webroot/files/members/'.$folder.'/'.$video.'_watermark.'.$ext;
						
						//exec($cmdWatermark , $getwatermarkResponse, $gerwatermarkError);
						
						//if($gerwatermarkError == 0)
						//{
							//$preview_cmd = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/members/'.$folder.'//'.$video.'_watermark.'.$ext.' -ss 00:00:0.0 -t 00:01:0.0 -acodec copy -vcodec copy '.FILE_PATH.'app/webroot/files/members/'.$folder.'//'.$video.'_preview.'.$ext;
						
							//exec($preview_cmd, $getpreviewResponse, $gerpreviewError);
							//if($gerpreviewError == 0)
							//{
								
								
								try{
										$targetFolder = ''.FILE_PATH.'app/webroot/files/members/'.$folder.'/';
										$filename=$video.'.'.$ext;
										//unlink($targetFolder.$filename);
										$this->CourseLesson->id = $lesson_id;
										$this->CourseLesson->saveField('status',1); 
										$this->Watermark->delete($record_id);
										
										$this->loadModel('EmailTemplate');
											// --------------------Lesson available to view on the portal-----------------							
										$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>11,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));
										$emailData = $emailTemplate['EmailTemplate']['description'];
										$emailTo = $email;
										$emailData = str_replace(array('{final_name}','{lesson_title}','{course_title}'),array($final_name,$lesson_title,$course_title),$emailData);
										$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
										$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
										$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
								}
								catch(Exception $e)
									{				
										$error = ($e->getMessage().'<pre>'.$e->getTraceAsString().'</pre>');
								}	
								
							//}
					
							//}
				}
				else
				{
					$this->Watermark->delete($record_id);
				}
			}
			
			die;
	}
	
	function test()
	{
		echo FILE_PATH;die;
	}
	
	
	function subs_account_cancel()
	{	
		$currentDate = date('d-M-Y');
		$this->loadModel('MemberSubscription');
		$this->loadModel('Member');
		$this->loadModel('EmailTemplate');
		$record=$this->MemberSubscription->find('all',array('conditions'=>array('MemberSubscription.cancel_request'=>'1','MemberSubscription.status'=>'1'),'fields'=>array('m_id','date_expired')));
		
		foreach ($record as $record)
		{
			$m_id=$record['MemberSubscription']['m_id'];
			$exp_date=date('d-M-Y',$record['MemberSubscription']['date_expired']);
			$data=$this->Member->find('first',array('conditions'=>array('Member.id'=>$m_id),'fields'=>array('email','final_name','locale')));
			if(!empty($data))
			{
				$email=$data['Member']['email'];
				$name = $data['Member']['final_name'];
				$m_locale = $data['Member']['locale'];
			}
			$two_week_before=date('d-M-Y',strtotime($exp_date.'-14 days'));
			$one_week_before=date('d-M-Y',strtotime($exp_date.'-7 days'));
			$one_day_before=date('d-M-Y',strtotime($exp_date.'-1 days'));
			
			if($two_week_before==$currentDate || $one_week_before==$currentDate || $one_day_before==$currentDate)
			{
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>13,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
				$emailData = $emailTemplate['EmailTemplate']['description'];						
				$emailData = str_replace(array('{final_name}','{exp_date}'),array($name,$exp_date),$emailData);									
				$emailTo =$email;
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
			
			}
			if($exp_date==$currentDate)
			{
				$date = date('Y-m-d H:i:s');
				$this->loadModel('Member');
				$this->Member->updateAll(array('Member.status'=>2),array('Member.id'=>$m_id));
				$this->MemberSubscription->updateAll(array('MemberSubscription.cancel_request'=>'0'),array('MemberSubscription.m_id'=>$m_id));
				
			}
		}
	}
	
	
	function recursivefilecheck()
	{
	/*	
	$this->loadModel('CourseLesson');
	$this->loadModel('Course');
	$zipfiles=$this->CourseLesson->find('all',array('conditions'=>array('extension'=>'zip','CourseLesson.id'=>850)));
	$courseid=$zipfiles[0]['CourseLesson']['course_id'];
	$foldername=$zipfiles[0]['CourseLesson']['video'];
	$courselist=$this->Course->find('all',array('conditions'=>array('Course.id'=>$courseid)));
	$memberid=$courselist[0]['Course']['m_id'];
	$iter = new RecursiveDirectoryIterator('../webroot/files/members/'.$memberid.'/'.$foldername);
	foreach (new RecursiveIteratorIterator($iter) as $fileName => $fileInfo) {
	$info = new SplFileInfo($fileInfo);
	if(!empty($info->getExtension()))
	{
	App::import('Component', 'Aws');
	$this->Aws = new AwsComponent();
	$extense=	explode($memberid,$fileInfo);
	$extense1=explode("../",$fileInfo);
	$this->Aws->uploadFile($memberid.$extense[1],LESSON_VIDEOS_BUCKET,$_SERVER['DOCUMENT_ROOT'].'/kuulum/app/'.$extense1[1]);
	}
}*/
	ini_set('display_errors', 0);
	ini_set('max_execution_time', -1);
	ini_set('memory_limit', '2048M');
	$this->loadModel('Member');
	$ids=$this->Member->find('all',array('fields'=>'id'));
	foreach($ids as $key=>$id)
	{
	if(is_dir(realpath('../webroot/files/members/'.$id['Member']['id'])))
	{
	$iter1 = new RecursiveDirectoryIterator('../webroot/files/members/'.$id['Member']['id']);
	foreach (new RecursiveIteratorIterator($iter1) as $fileName1 => $fileInfo1) 
	{
	
	$info1 = new SplFileInfo($fileInfo1);
	$extensions = $info1->getExtension();
	if(!empty($extensions) && $extensions!='zip')
	{
	App::import('Component', 'Aws');
	$this->Aws = new AwsComponent();
	$extense1=	explode("/members/".$id['Member']['id'],$fileInfo1); 
	$extense2=explode("../",$fileInfo1);
	$this->Aws->uploadFile($id['Member']['id'].$extense1[1],LESSON_VIDEOS_BUCKET,$_SERVER['DOCUMENT_ROOT'].ROOT_FOLDER."app/".$extense2[1]);
	
	}
	}
	}
	}	
	exit;
	}
}
?>
