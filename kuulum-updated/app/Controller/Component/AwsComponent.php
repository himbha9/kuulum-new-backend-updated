<?php
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;

	App::uses('Component', 'Controller');

	class AwsComponent extends Component {
	
		/** Private Vars **/
		var $_client;
		
		/** Public Vars **/
		var $errors;
				
		function __construct(){

			// This method takes a reference to the controller which is loading it.
			// Perform controller initialization here.
			App::import('Vendor','aws/aws-autoloader');
			
			$this->_client = S3Client::factory(array(
			//'profile' => 'my_profile',
			'key'    => AWS_KEY,
			'secret' => AWS_SECRET,
			));	
		}	

		/** Function to call to delete object from bucket **/
		function deleteFile($bucket, $key){
			$result = $this->_client->deleteObject(array(
			    'Bucket' => $bucket,
			    'Key'    => $key
			));
		}

		/** Function to call to delete multiple objects from bucket **/
		function deleteFiles($bucket, $keys){
			// Delete objects from a bucket
			$keyNames = array();
			foreach($keys as $key){
				$keyNames[] = array('Key'=>$key);
			}
			$result = $this->_client->deleteObjects(array(
			    'Bucket'  => $bucket,
			    'Objects' => $keyNames
			));
		}

		/** Function to call to upload object to bucket **/
		function uploadFile($key, $bucket, $filePath){
			try {
				$result = $this->_client->putObject(array(
					'Bucket' => $bucket,
					'Key'    => $key,
					'SourceFile' => $filePath,
					'ACL'    => 'public-read',
					'CacheControl' => "max-age=315360000",
					'Expires' =>  gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
				));
				echo $result['ObjectURL'];
			} catch (S3Exception $e) {
				$this->errors = "There was an error uploading the file.";
				return $this->errors;
			}
		}

		function getFileURL($bucket, $key){
			$result = $this->_client->getObjectUrl($bucket, $key);
			return $result;
		}

		/** Function to call to list objects in bucket **/
		function listObjects($bucket,$prefix){
			try {
			    $return = array();
			    $result = $this->_client->listObjects(array('Bucket' => $bucket,'Prefix'=>$prefix));
				if(isset($result['Contents']) && !empty($result['Contents'])){
			    foreach ($result['Contents'] as $object) {
				$return[] = $object['Key'];
			    }}
				return $return;
			} catch (S3Exception $e) {
			    $this->errors = $e->getMessage();
			    return $this->errors;
			}
		}

		/** Function to call to list buckets **/
		function listBuckets(){
			$result = $this->_client->listBuckets();
			$return = array();
			foreach ($result['Buckets'] as $bucket) {
			   $return[] = $bucket['Name'];
			}
			return $return;
		}
	
		
		
				
	}
	
?>
