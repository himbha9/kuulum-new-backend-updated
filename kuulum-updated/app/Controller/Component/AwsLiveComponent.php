<?php
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;

	App::uses('Component', 'Controller');

	class AwsLiveComponent extends Component {
	
		/** Private Vars **/
		var $_client;
		
		/** Public Vars **/
		var $errors;
				
		function __construct(){

			// This method takes a reference to the controller which is loading it.
			// Perform controller initialization here.
			App::import('Vendor','aws/aws-autoloader');
			
			$this->_client = S3Client::factory(array(
			//'profile' => 'my_profile',
			'key'    => 'AKIAIHOQVTVAAZKVIEKQ',
			'secret' => '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',
			));	
		}	

		/** Function to call to delete object from bucket **/
		function deleteFile($bucket, $key){
			$result = $this->_client->deleteObject(array(
			    'Bucket' => $bucket,
			    'Key'    => $key
			));
		}

		/** Function to call to delete multiple objects from bucket **/
		function deleteFiles($bucket, $keys){
			// Delete objects from a bucket
			$keyNames = array();
			foreach($keys as $key){
				$keyNames[] = array('Key'=>$key);
			}
			$result = $this->_client->deleteObjects(array(
			    'Bucket'  => $bucket,
			    'Objects' => $keyNames
			));
		}

		/** Function to call to upload object to bucket **/
		function uploadFile($key, $bucket, $filePath){
			try {
				$result = $this->_client->putObject(array(
					'Bucket' => $bucket,
					'Key'    => $key,
					'SourceFile' => $filePath,
					'ACL'    => 'public-read',
				));
				echo $result['ObjectURL'];
			} catch (S3Exception $e) {
				$this->errors = "There was an error uploading the file.";
				return $this->errors;
			}
		}

		function getFileURL($bucket, $key){
			$result = $this->_client->getObjectUrl($bucket, $key);
			return $result;
		}

		/** Function to call to list objects in bucket **/
		function listObjects($bucket){
			try {
			    $return = array();
			    $result = $this->_client->listObjects(array('Bucket' => $bucket));
			    foreach ($result['Contents'] as $object) {
				$return[] = $object['Key'];
			    }
			} catch (S3Exception $e) {
			    $this->errors = $e->getMessage();
			    return $this->errors;
			}
		}

		/** Function to call to list buckets **/
		function listBuckets(){
			$result = $this->_client->listBuckets();
			$return = array();
			foreach ($result['Buckets'] as $bucket) {
			   $return[] = $bucket['Name'];
			}
			return $return;
		}
	
		
		
				
	}
	
?>
