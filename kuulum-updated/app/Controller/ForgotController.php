<?php
 ini_set('memory_limit', '1024M'); // or you could use 1G
ob_start();
class ForgotController extends AppController 
{ 
    var $name='Forgot';
	 var $helpers = array('Session','Html','Js','Form','Text','Paginator');
	 var $components=array('Session','Email','RequestHandler','Upload');      
     var $uses=array('Admin','Commission','EmailTemplate','CmsPage','Member','Course','News','CourseLesson','Interest','LessonPrice','CoursePurchase','CourseCategory','CourseSubcategory','Discount','Price','Reason','CoursePrice','RemarkReport','FreeVideo'); 
    
          function admin_forgot_password(){
              $this->loadModel('Admin');
           // $admin_info=$this->Admin->find('first');
		//$this->Set('admin_info',$admin_info);
		$this->layout="";
                if(!empty($this->request->data)){
                    if($this->data['forgotpass']['email']!="")
			{			
				$email = $this->data['forgotpass']['email'];
				
				$getAdminData = $this->Admin->find('first',array('conditions' => array('email'=>$email)));
                               
				if($getAdminData)
				{	
                                    $token  = md5(date("Y-m-d H:i:s"));
                                    $data['Admin']['id']=$getAdminData['Admin']['id'];
                                    $data['Admin']['token']=$token;
                                    $this->Admin->save($data);
                                    
                                    $emailTo = $getAdminData['Admin']['email'];
                                    $emailFrom = "noreply@l10ntrain.com";
                                    $emailSubject = "Reset Password";
                                    $emailData = "<a href='".HTTP_ROOT."admin/Forgot/resetpassword/".$token."'>Click here</a> to rest password : ";
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
                                	$this->Session->write('success','Reset password link sent on your email address. please check your inbox');
                                        $this->redirect(array('controller'=>'Forgot','action'=>'forgot_password','admin'=>true));
				}
			}
			$this->Session->write('error',"Enter valid email address");		
                    
                }
          }
          function validate_admin_forgot_password()
	{
              $this->loadModel('Admin');
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax())
		{
			$errors_msg = null;
			$errors=$this->validate_admin_forgot_password_ajax($this->data);
                        
                       
                        
			if ( is_array ( $this->data ) )
			{
				foreach ($this->data['forgotpass'] as $key => $value )
				{
					if( array_key_exists ( $key, $errors) )
					{
						foreach ( $errors [ $key ] as $k => $v )
						{
							$errors_msg .= "error|$key|$v";
						}	
					}
					else 
					{
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}

                function validate_admin_forgot_password_ajax($data)
	{		
                    $this->loadModel('Admin');
		$email = $data['forgotpass']['email'];
		//$password = $data['Admin']['password'];
		$checkexistUser=$this->Admin->find('count',array('conditions'=>array('email'=>$email)));	
		//echo $checkexistUser;
		if($checkexistUser==0)
		{
			$errors ['password'] [] = 'Your e-mail address have not been recognized. Please try again.'."\n";
		}	
               
		return $errors;			
	}
        
        function admin_resetpassword($token="")
        { 
            $this->layout="";   
            $checkexistUser=$this->Admin->find('count',array('conditions'=>array('token'=>$token)));	
            if($checkexistUser)
            {
                $User=$this->Admin->find('first',array('conditions'=>array('token'=>$token)));	
                $this->set(compact('User'));
                
            }  else {
               $this->Session->write('error',"Invalid Request for password reset.");	
               $this->redirect(array('controller'=>'Forgot','action'=>'forgot_password','admin'=>true));
            }
            
        }
        
        
         function admin_update_password()
        { 
             $token = $this->request->data['token'];
             if(strcmp($this->request->data['forgotpass']['password'],$this->request->data['forgotpass']['repassword'])==0)
             {
                 
                    $checkexistUser=$this->Admin->find('count',array('conditions'=>array('token'=>$token)));	
                    if($checkexistUser)
                    {
                                            $data['Admin']['id']=$this->request->data['id'];
                                            $data['Admin']['password']=md5($this->request->data['forgotpass']['password']);
                                            $data['Admin']['token']='';
                                            $this->Admin->save($data);

                        $this->Session->write('success',"Password reset successfully.");
                        $this->redirect(array('controller'=>'Forgot','action'=>'forgot_password','admin'=>true));

                    }  else {
                       $this->Session->write('error',"Invalid Request for password reset.");	
                       $this->redirect(array('controller'=>'Forgot','action'=>'forgot_password','admin'=>true));
                    }
            
                                   
                                   
             }  else {
                   $this->Session->write('error',"Password and Repassword should be same.");
                   $token = $this->request->data['token'];
                   $this->redirect(array('controller'=>'Forgot','action'=>'resetpassword/'.$token,'admin'=>true));
                   die;
             }
             
             
            /*
             $this->layout="";   
             
            $checkexistUser=$this->Admin->find('count',array('conditions'=>array('token'=>$token)));	
            if($checkexistUser)
            {
                $User=$this->Admin->find('first',array('conditions'=>array('token'=>$token)));	
            }  else {
               $this->Session->write('error',"Invalid Request for password reset.");	
               $this->redirect(array('controller'=>'Forgot','action'=>'forgot_password','admin'=>true));
            }
             * 
             */
            
        }
        
        
          
            
        
    
}
?>