<?php ob_start();

class HomeController extends AppController 
{
	var $name =	'Home';
	var $components = array('RequestHandler','Session','Cookie');
	var $helpers = array('Form','Js','Html','Text','Timezone','Utility');
	var $pageLimit = '12';
	function beforeFilter()
	{
		parent::beforeFilter(); 
		$this->disableCache();
		if($this->Session->read('LocTrain.login') == '1' && in_array($this->params['action'],array('login','register','reactivate')))
		{
		    $this->redirect(array('controller'=>'Home','action'=>'index'));
		}
	}
	
	function index()
	{	
		//Configure::write('debug',2);
	    	$this->layout =	'public';		
            $title_for_layout = 'Localization Training';
            $this->loadModel('Banners');
            $this->loadModel('Bannerdefaults');
            $locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
            $this->set('locale',$locale);
            $locale_id = $this->Session->read('LocTrain.LangId');		
         	$mem_login_id=$this->Session->read('LocTrain.id');
         	/** BANNER Functionality **/
            if($this->Session->read('LocTrain.locale')!='en')
            {
				$default=$this->Bannerdefaults->find('first',array('conditions'=>array('language'=>$this->Session->read('LocTrain.locale')),array('fields'=>array('default'))));
				
				if(isset($default['Bannerdefaults']) &&$default['Bannerdefaults']['default']==0)
				{
				$banners = $this->Banners->find('all',array('conditions'=>array('Banners.locale'=>'en','publish'=>1),'order'=>'Banners.order DESC'));
				}
				else
				{
				$banners = $this->Banners->find('all',array('conditions'=>array('Banners.locale'=>$this->Session->read('LocTrain.locale'),'publish'=>1),'order'=>'Banners.order DESC'));
				if(empty($banners))
				{
				$banners = $this->Banners->find('all',array('conditions'=>array('Banners.locale'=>'en','publish'=>1),'order'=>'Banners.order DESC'));	
				}
				}
			}
			else
			{
				$banners = $this->Banners->find('all',array('conditions'=>array('Banners.locale'=>$this->Session->read('LocTrain.locale'),'publish'=>1),'order'=>'Banners.order DESC'));
			} 
			/** BANNER Functionality **/

            $this->loadModel('Course');
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory'))

    );
          	$courses = $this->Course->find('all',array(
          				'conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),
          				'order'=>'Course.id DESC',
          				'contain'=>array(
	          								'CourseLesson'=>array(
	          									'fields'=>array('id','video','extension','image'),
	          									'order'=>'CourseLesson.order ASC'
	          								),
										'CourseRating'=>array(
	          									'fields'=>array('rating')
	          								),
	          								'CourseView'=>array(
	          									'order'=>'CourseView.no_of_views DESC'
	          								),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0))
          								),
          				'limit'=>8
          				));
		//print_r($courses);

		$this->set(compact('title_for_layout','courses','locale_id','language','banners'));


	}
	
	
	//-----------Change Language-----------------------	
	
	function change_language()
	{
		if(!empty($this->data))
		{
			//pr($this->data);die;
			$lang = $this->data['Language']['lang'];
			$langId = $this->data['Language']['id'];
			$language = array('en_US'=>'en','de_DE'=>'de','ar_AR'=>'ar','fr_FR'=>'fr','it_IT'=>'it','ja_JP'=>'ja','ko_KR'=>'ko','pt_PT'=>'pt','ru_RU'=>'ru','zh_ZH'=>'zh','es_ES'=>'es');
		
			if(!empty($lang) && in_array($lang,$language) && !empty($langId))
			{
				$val = array_search($lang,$language);
				$this->Cookie->write('LocTrain.locale',$lang,false,60*60*24*365);
				$this->Cookie->write('LocTrain.LangId',$langId,false,60*60*24*365);
				$this->Session->write('Config.language',$val);
				$this->Session->write('LocTrain.locale',$lang);
				$this->Session->write('LocTrain.LangId',$langId);
				
				if($this->Session->read('LocTrain.locale')=="ar")
				{ 
					$this->Cookie->write('LocTrain.style_arabic','style_arabic',false,60*60*24*365);
				} else {
				
					$this->Cookie->write('LocTrain.style','style',false,60*60*24*365);
				}
				// updating member locale
				$this->loadModel('Member');
				
				$login_m_id = $this->Session->read("LocTrain.id");
				if(!empty($login_m_id))
				{
				$this->request->data['Member']['id'] =	$login_m_id;
				$this->request->data['Member']['locale'] =$lang;
				
				$this->Member->save($this->data);
				}
				//pr($val); die;
			}
			$this->redirect($this->referer());
		}
	}
	
	//-----------REGISTRATION PROCESS-----------------------	
	function register($course_id='')
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$locale = $this->Session->read('LocTrain.locale');
		App::import('Vendor','recaptchalib');
                $publickey = CAPTCHA_PUBLICKEY;
                $this->set('publickey',$publickey);
                 $cookieVal = $this->Cookie->read('LocTrain.emailCookie');
                $this->set('cookieVal',$cookieVal);	
               /* if (isset($recaptcha_error)) {
                        echo '<strong>' . $recaptcha_error . '</strong><br/>';
                }*/
  $show_or_hide_login='No';
 	 	$url=explode('/',Router::url($this->referer(), true));
		if(in_array('view_course_details',$url) || in_array('search',$url)){
			$show_or_hide_login='Yes';		
		}
               
		$this->loadModel('Interest');
		$this->loadModel('Member');
                $this->loadModel('Language');
                 
                $language = $this->Language->find('all',array('conditions'=>array('Language.status'=>'1'),'fields'=>array('id','language','locale')));	
		
		$interest_list = '';
		$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest asc'));		
		if(!empty($interest)){
			foreach($interest as $interestVal):			
				$interest_list .= '{value:'."'".$interestVal['Interest']['interest']."'".", name:"."'".$interestVal['Interest']['interest']."'},";	
			endforeach;	
		}else{
			$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
		}	
		$this->set(compact('title_for_layout','interest_list','language','course_id'));

		/*if(empty($this->data))
		{			
			$this->captchaImage();
		}*/
		if(!empty($this->data)){
			$error = array();
			$error = $this->validate_register($this->data,1);
			
			if(count($error) == 0){
			
			
				$job_id = str_shuffle('1234567890');
				$job_id=substr($job_id,0,6);
				$activation_code = md5(microtime());
				$this->request->data['Member']['mem_acc_no']=$job_id;
				$this->request->data['Member']['password'] = md5($this->data['Member']['reg_password']);
				$this->request->data['Member']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$this->request->data['Member']['activation_link'] = $activation_code;
				$this->request->data['Member']['status'] = '0';
				$this->request->data['Member']['role_id'] = '2';
					
                                $this->request->data['Member']['given_name'] = $this->data['Member']['reg_full_name'];
                                $this->request->data['Member']['email'] = $this->data['Member']['reg_email'];
                                $this->request->data['Member']['term_condition'] = $this->data['Member']['reg_term_condition'];
                                $this->request->data['Member']['locale']=$this->Session->read('LocTrain.locale');  
                                $m_locale=$this->Session->read('LocTrain.locale');  
                                 $activate_string = '';	               
				if($this->Member->save($this->data))
				{
					$member_id = $this->Member->getLastInsertId();
					App::uses('Folder','Utility');
					$dir = new Folder();
					$dir->create('files'.DS.'members'.DS.$member_id);		
					
					/*$keys=explode(",",$this->request->data['Member']['keywords']);
					$this->loadModel('MemberKeyword');
					$this->loadModel('Interest');
					foreach($keys as $key){
					 	if($key != "")	{
							$checkInterestExists = $this->Interest->find('count',array('conditions'=>array('Interest.interest'=>trim($key))));
							if($checkInterestExists == 0){
								$interest['Interest']['status']= '1';
								$interest['Interest']['interest']= trim($key);
								$interest['Interest']['date_added']= strtotime(date('Y-m-d H:i:s'));
								$this->Interest->create();
								$this->Interest->save($interest);
							}
							$checkMemberKeywordExists = $this->MemberKeyword->find('count',array('conditions'=>array('MemberKeyword.m_id'=>$member_id,'MemberKeyword.keyword'=>trim($key))));
							if($checkAuthorMemberKeywordExists == 0){
								$this->request->data['MemberKeyword']['m_id'] = $member_id;
								$this->request->data['MemberKeyword']['keyword'] = $key;
								$this->MemberKeyword->create();
								$this->MemberKeyword->save($this->data);
							}
							
						}
					 }	
                                        $m_locale=$this->data['Member']['locale']?$this->data['Member']['locale']:'en';			*/
					$this->loadModel('EmailTemplate');
					$email = $this->data['Member']['email'];
					$idd = $this->Member->getLastInsertId();
					$activate_string = array('en'=>'Activate my account','de'=>'Mein Konto aktivieren','es'=>'Activar mi cuenta','zh'=>'激活我的帐户','ar'=>'تفعيل حسابي','fr'=>'Activer mon compte','it'=>'Attivare mio account','ja'=>'アカウントをアクティブに','ko'=>'내 계정 을 활성화','pt'=>'Ative Minha Conta','ru'=>'Активируйте Мой счет');
					
					// --------------------Email Confirmation To Member-----------------							
					$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>1,'locale'=>$m_locale),'fields'=>array('subject','description','email_from','email_to')));		
		
					$emailData = $emailTemplate['EmailTemplate']['description'];						
					$link = HTTP_ROOT.'Home/activate_account/'.base64_encode(convert_uuencode($idd))."/".$activation_code.'/'.$course_id;							
					$link = "<a href='".$link."' style='text-decoration:none;color:#00aeef'>".$activate_string[$m_locale]."</a>";						
					$emailData = str_replace(array('{final_name}','{activation_link}'),array($this->data['Member']['given_name'],$link),$emailData);							
					$emailTo = $email;
					$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
					$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
					$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);	
					
					// --------------------Email Confirmation To Admin-----------------	
					$emailTemplateAdmin = $this->EmailTemplate->find('first',array('conditions'=>array('id'=>5),'fields'=>array('subject','description','email_from','email_to')));					
					$emailData = $emailTemplateAdmin['EmailTemplate']['description'];
					$emailData = str_replace(array('{m_name}','{m_email}'),array($this->data['Member']['reg_full_name'],$email),$emailData);							
					$emailTo = ADMIN_EMAIL;
                                        $emailCc= $emailTemplateAdmin['EmailTemplate']['email_to'];
					$emailFrom = $emailTemplateAdmin['EmailTemplate']['email_from'];
					$emailSubject = $emailTemplateAdmin['EmailTemplate']['subject'];	
					$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData,$emailCc);
					$this->before_update_notification();
					$temp_id = $this->Session->read('LocTrain.visitor_id');
					$this->loadModel('Notification');
					$notification['Notification']['mem_id'] =  0;
					$notification['Notification']['notification_type'] = 'register_success';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['notification_sender_id'] = $temp_id;
					$notification['Notification']['read'] = 1;
					$this->Notification->create();
					$this->Notification->save($notification);
					$this->redirect(array('action'=>'index'));
		
				}
			}else{
				$this->set(compact('error'));
			}
		}	
	}
	function validate_register_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_register($this->data,1);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Member'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v\n";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
                                       if( array_key_exists ( 'reg_captcha_chk', $errors) ){
						foreach ( $errors [ 'reg_captcha_chk' ] as $k => $v ){
							$errors_msg .= "error|reg_captcha_chk|$v";
						}	
					}
					else {
						$errors_msg .= "ok|reg_captcha_chk\n";
					}
			}
			echo $errors_msg;
			die;
		}	
	}
	
		function validate_register($data,$sta)
	{		
		$errors = array ();		
		if (trim(@$data['Member']['reg_email']) == ""){
			$errors['reg_email'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif($this->validEmail(trim($data['Member']['reg_email'])) =='false'){
			$errors['reg_email'][] = __(INVALID_EMAIL)."\n";
		}
		elseif(strlen(trim($data['Member']['reg_email'])) > 150 ){
			$errors['reg_email'][] = __(EMAIL_LENGTH_VALIDATION)."\n";
		}elseif((trim($data['Member']['reg_email']) !="")){
			$this->loadModel('Member');
			$chkEmail = $this->Member->find('count',array('conditions'=>array('Member.email'=>trim($data['Member']['reg_email']))));
			if($chkEmail > 0){
				$errors['reg_email'][] = __(EMAIL_EXISTS)."\n";
			}
		}
		
		if(trim(@$data['Member']['reg_password']) == ''){
			$errors['reg_password'][] = __(FIELD_REQUIRED)."\n";
		}
		else if(trim($data['Member']['reg_password']) != ''){
		
			if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()\-_=+{};:,<.>~]).{8,20}$/", trim($data['Member']['reg_password']))) {
				//$errors['reg_password'][] = __(PASSWORD_VALIDATION)."\n";
                             $errors['reg_password'][] = __("please_choose_a_password_consisting_of")."<br>".__("a_minimum_of_8_and_maximum_of_20_characters").'<br>'.__("at_least_one_upper_case_character").'<br>'. __("at_least_one_lower_case_character").'<br>'. __("at_least_one_number").'<br>'.__("at_least_one_non_alphanumeric_character");
			}
		}
		
		if(trim($data['Member']['reg_full_name']) == ''){
			$errors['reg_full_name'][] = __(FIELD_REQUIRED)."\n";
		}
		if(trim($_POST['recaptcha_response_field']) == ''){
			$errors['reg_captcha_chk'][] = __(FIELD_REQUIRED)."\n";
		}
		if($data['Member']['reg_term_condition'] == '' || $data['Member']['reg_term_condition'] != '1'){
			$errors['reg_term_condition'][] = __("please_accept_the_terms_of_use")."\n";
		}
		
                 App::import('Vendor','recaptchalib');
                 $privatekey =CAPTCHA_PRIVATEKEY;
                 $resp = recaptcha_check_answer( $privatekey, 
					$_SERVER['REMOTE_ADDR'],
					$_POST['recaptcha_challenge_field'],
					$_POST['recaptcha_response_field']);
                  if($sta==0){
                if (!$resp->is_valid) {
                      //  $this->set('recaptcha_error', 
                       //         'You did not enter the words correctly.  Please try again.');
                        $errors['reg_captcha_chk'][]=__('you_did_not_enter_the_words_correctly_please_try_again');
                }
                 }
               
		return $errors;			
	}
function checkCaptchaCode(){
$this->layout="";
		$this->autoRender=false;	
//print_r($this->data);

App::import('Vendor','recaptchalib');
                 $privatekey = CAPTCHA_PRIVATEKEY;
                 $resp = recaptcha_check_answer( $privatekey, 
					$_SERVER['REMOTE_ADDR'],
					$_POST['recaptcha_challenge_field'],
					$_POST['recaptcha_response_field']); 
                  if($sta==0){
                if (!$resp->is_valid) {
                      //  $this->set('recaptcha_error', 
                       //         'You did not enter the words correctly.  Please try again.');
                      echo __('you_did_not_enter_the_words_correctly_please_try_again');
                }
                 }
die();
}
	
		function reactivate($mem_id=NULL)  // if member reactivate his cancelled account. 
	{       
		$locale = $this->Session->read('LocTrain.locale');	
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$locale = $this->Session->read('LocTrain.locale');
		$this->loadModel('Member');
		App::import('Vendor','recaptchalib');
                $publickey = CAPTCHA_PUBLICKEY;
                $this->set('publickey',$publickey);
		if(!empty($mem_id)) 
		{
			$mem_id = convert_uudecode(base64_decode($mem_id));
			$data = $this->Member->find('first',array('conditions'=>array('Member.id'=>$mem_id),'contain'=>array('CancelReason','MemberKeyword'=>array('fields'=>array('keyword')))));
			//echo "<pre>"; print_r($data); die;
			
			if(empty($data) || empty($data['CancelReason']['id']))
			{
				$this->redirect(array('controller'=>'Home','action'=>'login'));
			}
			$this->set('data',$data);
			
			$interest_list = '';
			$this->loadModel('Interest');
			$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest asc'));		
			if(!empty($interest)){
				foreach($interest as $interestVal):			
					$interest_list .= '{value:'."'".$interestVal['Interest']['interest']."'".", name:"."'".$interestVal['Interest']['interest']."'},";	
				endforeach;	
			}else{
				$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
			}	
			$this->set(compact('title_for_layout','interest_list'));
			//$this->captchaImage();					
		}
		else if(!empty($this->data))
		{
			$error = array();
			$error = $this->validate_reactivate($this->data,1);
			
			//echo "<pre>"; print_r($error); die;
			
			if(count($error) == 0){				
				
				$this->request->data['Member']['id'] = convert_uudecode(base64_decode($this->data['Member']['id']));
				$this->request->data['Member']['status'] = 1;
				$this->request->data['Member']['email'] = $this->data['Member']['rea_email'];
			
				$chkInfo = $this->Member->find('first',array('conditions'=>array('Member.id'=>$this->data['Member']['id']),'contain'=>false));
				
				$new_email = 'no';
				
				if($chkInfo['Member']['email'] != $this->data['Member']['rea_email'])
				{
					$new_email = 'yes';
					
					$activation_code = md5(microtime());
					$this->request->data['Member']['activation_link'] = $activation_code;
					$this->request->data['Member']['status'] = '0';
					
					$this->loadModel('EmailTemplate');
					$id = $this->data['Member']['id'];		
                                     					$m_locale = $this->data['Member']['locale']?$this->data['Member']['locale']:'en';

					// --------------------Email Confirmation To Member-----------------							
					$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>1,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
					$emailData = $emailTemplate['EmailTemplate']['description'];						
					$link = HTTP_ROOT.'Home/activate_account/'.base64_encode(convert_uuencode($id))."/".$activation_code;							
					$link = "<a href='".$link."' style='text-decoration:none;color:#00aeef'>".__('activate_my_account')."</a>";						
					$emailData = str_replace(array('{final_name}','{activation_link}'),array($this->data['Member']['final_name'],$link),$emailData);							
					$emailTo = $email;
					$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
					$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
					$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);					
				}				

				//echo "<pre>"; print_r($this->data); die;
				
				if($this->Member->save($this->data))
				{
					$login_m_id = $this->data['Member']['id'];
					
					if($this->Session->read('LocTrain.login') != '1')
					{
				
						$temp_id = $this->Session->read('LocTrain.visitor_id');
						$this->before_update_notification();
						$this->loadModel('Notification');
						$notification['Notification']['mem_id'] =  0;
						$notification['Notification']['notification_type'] = 'account_reactivate';
						$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['notification_sender_id'] = $temp_id;
						$notification['Notification']['read'] = 1;
						$this->Notification->create();
						$this->Notification->save($notification);
															
					}
					

					$this->loadModel('CancelReason');
					$this->CancelReason->deleteAll(array('CancelReason.m_id'=>$login_m_id));
					
					$keys=explode(",",$this->data['Member']['keyword']);
					$this->loadModel('MemberKeyword');
					$this->loadModel('Interest');
										
					$this->MemberKeyword->deleteAll(array('MemberKeyword.m_id'=>$login_m_id));
					foreach($keys as $key){
					 	if($key != ""){
							$checkInterestExists = $this->Interest->find('count',array('conditions'=>array('Interest.interest'=>trim($key))));
							
							if($checkInterestExists == 0){
								$interest['Interest']['status'] = '1';
								$interest['Interest']['interest'] = trim($key);
								$interest['Interest']['date_added'] = strtotime(date('Y-m-d H:i:s'));
								$this->Interest->create();
								$this->Interest->save($interest);
							}
							$checkMemberKeywordExists = $this->MemberKeyword->find('count',array('conditions'=>array('MemberKeyword.m_id'=>$login_m_id,'MemberKeyword.keyword'=>trim($key))));
							if($checkMemberKeywordExists == 0){
								$this->request->data['MemberKeyword']['m_id'] = $login_m_id;
								$this->request->data['MemberKeyword']['keyword'] = $key;
								$this->MemberKeyword->create();
								$this->MemberKeyword->save($this->data);
							}							
						}
					}
					if($new_email == 'yes')
					{
						$this->redirect(array('action'=>'register_success'));
					} else {
					//	$this->Session->write("LocTrain.flashMsg",__('Account has been reactivated successfully'));
						$this->redirect(array('controller'=>'Home','action'=>'index'));
					}
				}
				
			}else{
				$this->set(compact('error'));
			}
		} 
		else
		{
			$this->redirect(array('controller'=>'Home','action'=>'index'));
		}
	}
	
	
	function validate_reactivate_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_reactivate($this->data,0);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Member'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
  if( array_key_exists ( 'captcha_chk', $errors) ){
						foreach ( $errors [ 'captcha_chk' ] as $k => $v ){
							$errors_msg .= "error|captcha_chk|$v";
						}	
					}
					else {
						$errors_msg .= "ok|captcha_chk\n";
					}
			}
			echo $errors_msg;
			die;
		}	
	}
	
		function validate_reactivate($data,$sta)
	{			
		$errors = array ();		
		
		if(trim($data['Member']['id']) == ''){
			$errors['id'][] = __(FIELD_REQUIRED)."\n";
		}		
		if(trim($data['Member']['rea_email']) == ''){
			$errors['email'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif($this->validEmail(trim($data['Member']['rea_email'])) =='false')
		{
			$errors['email'][] = __(INVALID_EMAIL,true)."\n";
		}
		else
		{
			$this->loadModel('Member');
			$id = convert_uudecode(base64_decode($data['Member']['id']));
			$chkEmail = $this->Member->find('count',array('conditions'=>array('Member.id !='=>$id,'Member.email'=>trim($data['Member']['rea_email']))));
			if($chkEmail > 0){
				$errors['rea_email'][] = __(EMAIL_EXISTS)."\n";
			}		
		}
	/*	if(trim($data['Member']['final_name']) == ''){
			$errors['final_name'][] = __(FIELD_REQUIRED)."\n";
		}*/
		if($data['Member']['term_condition'] == '' || $data['Member']['term_condition'] != '1'){
			$errors['term_condition'][] = __("please_accept_the_terms_of_use")."\n";
		}
		if(trim($data['Member']['given_name']) == ''){
			$errors['given_name'][] = __(FIELD_REQUIRED)."\n";
		}
		if(is_numeric($data['Member']['given_name'])){
			$errors['given_name'][] =  __("numbers_are_not_allowed")."\n";
		}
	/*	if(trim($data['Member']['captcha_chk']) == ''){
			$errors['captcha_chk'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif(strtolower(str_replace(" ",'',$data['Member']['captcha_chk'])) != strtolower(str_replace(" ",'',base64_decode($this->Session->read('LocTrain.captchaCode'))))){			
			$errors['captcha_chk'][] = __("enter_all_words_exactly_as_shown_in_the_image_above")."\n";
		}*/
    App::import('Vendor','recaptchalib');
                 $privatekey = CAPTCHA_PRIVATEKEY;
                 $resp = recaptcha_check_answer( $privatekey, 
					$_SERVER['REMOTE_ADDR'],
					$_POST['recaptcha_challenge_field'],
					$_POST['recaptcha_response_field']);
                 if($sta==0){
                if (!$resp->is_valid) {
                      //  $this->set('recaptcha_error', 
                       //         'You did not enter the words correctly.  Please try again.');
                        $errors['captcha_chk'][]=__('you_did_not_enter_the_words_correctly_please_try_again');
                }
                 }
		return $errors;			
	}
	
	function activate_account($id=NULL,$actvationcode=NULL,$course_id='')
	{
		$id=convert_uudecode(base64_decode($id));
		echo $id.'-'.$actvationcode;
		$this->loadModel('Member');
		$checkvarification=$this->Member->find('first',array('conditions'=>array('Member.id'=>$id,'Member.activation_link'=>$actvationcode,'Member.status'=>0),'contain'=>array()));
		
		if(count($checkvarification)!=0)
		{
	
			$this->Session->write("LocTrain.login",1);
			$this->Session->write("LocTrain.id",$checkvarification['Member']['id']);	
			$this->Session->write("LocTrain.member_type",$checkvarification['Member']['role_id']);				
			$this->Session->write('LocTrain.status',$checkvarification['Member']['status']);
			$this->Session->write('LocTrain.email',$checkvarification['Member']['email']);
			$this->Session->write('LocTrain.Fname', $checkvarification['Member']['final_name']);
			$this->Member->updateAll(array('Member.status' =>1),array('Member.id' =>$id,'Member.activation_link' => $actvationcode));
			//$this->Session->write("LocTrain.flashMsg",'Your Account has been activated, You may change your membership details any time by selecting your e-Mail address in the header.');
			
			$login_m_id = $checkvarification['Member']['id'];
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'account_activation';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			$this->Notification->create();
			$this->Notification->save($notification);
                        $temp_id = $this->Session->read('LocTrain.visitor_id');
			$this->Notification->updateAll(array('Notification.notification_sender_id'=>$login_m_id),array('Notification.notification_sender_id'=>$temp_id));


            //$this->Session->write("LocTrain.flashMsg",__('Your Account has been activated, You may change your membership details any time by selecting your e-Mail address in the header.',true));
			//$this->redirect(array('action'=>'index'));
		//print_r($this->Cookie->read('LocTrainPurchase'));die();

			if($course_id)
			{
				
				$this->redirect(array('controller'=>'Home','action'=>'view_course_details',$course_id));
			}
			if($this->Cookie->read('LocTrainPurchase'))
			{
				$this->redirect(array('action'=>'purchase'));
			}
			else if($this->Cookie->read('takesubscription'))
			{
				$this->Cookie->delete('takesubscription');
				$this->redirect(array('controller'=>'Members','action'=>'subscription'));
			}
			else
			{
				$this->redirect(array('action'=>'index'));
			}
			
		}
		else
		{
			$login_m_id = $this->Session->read('LocTrain.visitor_id')? base64_encode(convert_uuencode($this->Session->read('LocTrain.visitor_id'))):base64_encode(convert_uuencode($this->Session->read('LocTrain.visitor_id')));
			
			$this->redirect(array('action'=>'link_expired',$login_m_id));
		}
	}
	
	function register_success()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
	}
	
	function register_unsuccess()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
	}
	//-------END OF REGISTRATION PROCESS---------

	function login($id=NULL)
	{
		if($id != NULL)
		{
		
			if($id == 'login_first')
			{
				$this->Cookie->write('takesubscription',1,false,60*60*24*365);
				
			}
			
			$temp_id = $this->Session->read('LocTrain.visitor_id');
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'member_not_login';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $temp_id;
			$notification['Notification']['read'] = 1;
			$this->Notification->create();
			$this->Notification->save($notification);
			
			//$this->Session->write('LocTrain.flashMsg',__('Please login to access this section.'));
			$this->redirect(array('controller'=>'Home','action'=>'login'));
		}
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$cookieVal = $this->Cookie->read('LocTrain.emailCookie');
		$this->set('cookieVal',$cookieVal);	
		
	}
	

	function terms_of_use()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$terms = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'terms_of_use','CmsPage.locale'=>$locale),'contain'=>array()));
		$this->set(compact('terms'));
	}
	function help()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$help = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'help','CmsPage.locale'=>$locale),'contain'=>array()));
		$this->set(compact('help'));
	}
	function about_company()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$about_us = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'about','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('about_us'));
	}
	
	function membership_detail()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');		
		$membership_detail = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'membership','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('membership_detail'));
	}
function about_refund()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');		
		$about_refund = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'about_refunds','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('about_refund'));
	}


	function contact_us()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');		
		$contact_us = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'contact_us','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('contact_us'));
	}
	function author_terms()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');		
		$author_terms = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'author_terms','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('author_terms'));
	}
	function best_practices()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$contact_us = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'best_practices','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('contact_us'));
	}
	
	function training_proposal()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$contact_us = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'training_proposal','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('contact_us'));
	}
	function training_style()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$contact_us = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'training_style','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('contact_us'));
	}
	function settle_dispute()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$contact_us = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'settle_dispute','CmsPage.locale'=>$locale),'contain'=>array()));

		$this->set(compact('contact_us'));
	}

	function cookie_policy()
	{
	    $this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$cookie_policy = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'cookie_policy','CmsPage.locale'=>$locale),'contain'=>array()));
		$this->set(compact('cookie_policy'));
	}
	
	function capturecode()
	{
		if($this->RequestHandler->isAjax()) 
		{
			$this->layout='';
			$this->autoRender = false;
			Configure::write('debug', 2);
			$this->captchaImage();
			die;
			//$this->viewPath = 'Elements'.DS.'frontElements/home';
			//$this->render('imagecapture');
		}			
	}	
	
	function privacy_policy()
	{	
	    $this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$privacy_policy = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'privacy_policy','CmsPage.locale'=>$locale),'contain'=>array()));
		$this->set(compact('privacy_policy'));
	}
	
	
	function link_expired($m_id=NULL)
	{
		
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		if(!empty($m_id))
		{
			$member_id  = convert_uudecode(base64_decode($m_id));
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'unable_account_activation';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $member_id;
			$this->Notification->create();
			$this->Notification->save($notification);
		}
		
		$this->redirect(array('controller'=>'Home','action'=>'index'));
	}
	

	function login_check_ajax()
	{		
		$this->layout="";
		$this->autoRender=false;
		if($this->RequestHandler->isAjax())
		{
			$errors_msg = null;
			$errors=$this->validate_login($this->data);							
			if ( is_array ( $this->data ) )
			{
				foreach ($this->data['Login'] as $key => $value )
				{
					if( array_key_exists ( $key, $errors) )
					{
						foreach ( $errors [ $key ] as $k => $v )
						{
							$errors_msg .= "error|$key|$v";
						}		
					}
					else 
					{
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	/*function validate_login($data)
	{			
		$errors = array ();
		if(trim($data['Login']['email']==""))
		{
			$errors['email'] []= __(FIELD_REQUIRED,true)."\n";
		}
		elseif($this->validEmail(trim($data['Login']['email'])) =='false')
		{
			$errors['email'][] = __(INVALID_EMAIL,true)."\n";
		}
		if (trim($data['Login']['password'])=="")
		{
			$errors ['password'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		elseif(trim($data['Login']['password'])!="" && trim($data['Login']['password'])!="")
		{
			$this->loadModel('Member');
			$checkexistUser = $this->Member->find('first',array('conditions'=>array('Member.email'=>trim($data['Login']['email']),'Member.password'=>md5(trim($data['Login']['password']))),'contain'=>array('CancelReason')));	
			//echo "<pre>"; print_r($checkexistUser); die;
			
			if(empty($checkexistUser))
			{
				$errors ['password'] [] = __(WRONG_CREDENTIALS,true)."\n";
			}
			else if(!empty($checkexistUser))
			{
				if($checkexistUser['Member']['status']=='0'){
					$errors ['password'] [] = __('Your account is not active. Ask for support to rectify this.',true)."\n";
					/*$activatelink=HTTP_ROOT.'Home/activation_mail/'.base64_encode(convert_uuencode($data['Login']['email']));
					$link="<a href='".$activatelink."' style='text-decoration:none;color:#00aeef'>".__('Request an activation e-mail')."</a>";	
					$errors ['password'] [] = __('Your membership must be activated before you can sign in.You can '.$link.' to be sent to the email address above.',true)."\n";					
					
				} else if($checkexistUser['Member']['status']=='2')
				{
					$date = date('d-M-y',strtotime($checkexistUser['CancelReason']['date']));
					$url = HTTP_ROOT.'Home/reactivate/'.base64_encode(convert_uuencode($checkexistUser['Member']['id']));
					$link = '<a href="'.$url.'">reactivate</a>';
					$errors['password'][] = String::insert(__("Your account was cancelled on :date. Do you want to :link your account?"),array('date' => $date,'link'=>$link));"\n";
				}
			}
		}
		return $errors;
	}*/
       
      function validate_login($data)
	{			
		$errors = array ();
		if(trim($data['Login']['email']==""))
		{
			$errors['email'] []= __(FIELD_REQUIRED,true)."\n";
		}
		elseif($this->validEmail(trim($data['Login']['email'])) =='false')
		{
			$errors['email'][] = __(INVALID_EMAIL,true)."\n";
		}
		if (trim($data['Login']['password'])=="")
		{
			$errors ['password'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		elseif(trim($data['Login']['password'])!="" && trim($data['Login']['password'])!="")
		{
			$this->loadModel('Member');
			$checkexistUser = $this->Member->find('first',array('conditions'=>array('Member.email'=>trim($data['Login']['email']),'Member.password'=>md5(trim($data['Login']['password']))),'contain'=>array('CancelReason')));	
			//echo "<pre>"; print_r($checkexistUser); die;
			
			if(empty($checkexistUser))
			{
				$errors ['password'] [] = __(WRONG_CREDENTIALS,true)."\n";
			}
			else if(!empty($checkexistUser))
			{
				if($checkexistUser['Member']['status']=='0'){
					$errors ['password'] [] = __('your_account_is_not_active_ask_for_support_to_rectify_this',true)."\n";
					/*$activatelink=HTTP_ROOT.'Home/activation_mail/'.base64_encode(convert_uuencode($data['Login']['email']));
					$link="<a href='".$activatelink."' style='text-decoration:none;color:#00aeef'>".__('Request an activation e-mail')."</a>";	
					$errors ['password'] [] = __('Your membership must be activated before you can sign in.You can '.$link.' to be sent to the email address above.',true)."\n";*/						
					
				} else if($checkexistUser['Member']['status']=='2')
				{
					//$date = date('d-M-y',strtotime($checkexistUser['CancelReason']['date']));
					
					$formatter = IntlDateFormatter::create($this->Session->read('LocTrain.locale'),
					IntlDateFormatter::FULL,
					IntlDateFormatter::FULL,
					$this->Session->read('Timezone'));

					//$date = $formatter->format($checkexistUser['CancelReason']['date']); 
$date = $formatter->format(strtotime($checkexistUser['CancelReason']['date']));

					$url = HTTP_ROOT.'Home/reactivate/'.base64_encode(convert_uuencode($checkexistUser['Member']['id']));
					$link = '<a href="'.$url.'"><span>'.__('reactivate').'</span></a>';
					$errors['password'][] = String::insert(__("your_account_was_cancelled_on_date_do_you_want_to_link_your_account"),array('date' => $date,'link'=>$link))."\n";
				}
			}
		}
		return $errors;
	}
	


	function member_login()
	{
		$this->layout='';
		$error=array();
		
		$error=$this->validate_login($this->data);
		
		
		if(count($error)==0){	
			
			$this->loadModel('Member');
			
			$checkexistUser=$this->Member->find('first',array('conditions'=>array('Member.status !='=>0,'Member.email'=>$this->data['Login']['email'],'Member.password'=>md5($this->data['Login']['password'])),'contain'=>false));

			$this->Session->write("LocTrain.login",1);
			$this->Session->write("LocTrain.id",$checkexistUser['Member']['id']);
			$this->Session->write("LocTrain.member_type",$checkexistUser['Member']['role_id']);				
			$this->Session->write('LocTrain.status',$checkexistUser['Member']['status']);
			$this->Session->write('LocTrain.email',$checkexistUser['Member']['email']);
			$this->Session->write('LocTrain.Fname', $checkexistUser['Member']['given_name']);
			$temp_id = $this->Session->read('LocTrain.visitor_id');
			$mem_id = $this->Session->read('LocTrain.id');
			
			$this->loadModel('Notification');
			
			//$this->Notification->updateAll(array('Notification.notification_sender_id'=>$mem_id),array('Notification.notification_sender_id'=>$temp_id));
			
			$this->Session->delete("LocTrain.visitor_id");
			
			$this->Session->write("LocTrain.timeout",time());
			if($_POST['course_to_learning'])
			{
				$res=$this->add_to_learning_login($_POST['redirect']);
				if($res){
				$this->redirect(array('controller'=>'Home','action'=>'view_course_details',$_POST['redirect']));}
			}
			if($_POST['redirect'])
			{
				
				$this->redirect(array('controller'=>'Home','action'=>'view_course_details',$_POST['redirect']));
			}
			if($_POST['redirectLesson'])
			{
				$this->redirect($_POST['redirectLesson']);
			}	
			if($_POST['redirectSearch'])
			{
				$this->redirect($this->referer().'&v='.$_POST['redirectSearch']);
			}
			if($_POST['redirectSearchnoSubcribe'])
			{
				$this->redirect($this->referer().'&nv='.$_POST['redirectSearchnoSubcribe']);
			}
			if($_POST['redirectSearchZip'])
			{
				$this->redirect(array('controller'=>'home','action'=>'play_video_swf/'.$_POST['redirectSearchZip']));
			}			
			if(isset($_POST['redirectPage']) && $_POST['redirectPage']=='purchase')
			{
				$this->redirect(array('action'=>'purchase'));
			}
			if(isset($_POST['redirectPage']) && $_POST['redirectPage']=='becoming_an_author')
			{	if($checkexistUser['Member']['role_id'] !=3){
				$this->redirect(array('controller'=>'Members','action'=>'upgrade_membership'));
				}else{
				$this->before_update_notification();
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'already_author';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $mem_id;
				$this->Notification->create();
				$this->Notification->save($notification);
				$this->redirect(array('action'=>'index'));
				}
			}
			
			if(isset($this->data['Login']['remember']) && $this->data['Login']['remember']=='1')
			{
				$this->Cookie->write('LocTrain.emailCookie',$this->data['Login']['email'],false,60*60*24*365);
					
			}
			else
			{
				$this->Cookie->delete('LocTrain.emailCookie');
				$this->Cookie->delete('LocTrain.passCookie');
			}
			
			if($this->Cookie->read('LocTrainPurchase'))
			{
				$this->redirect(array('action'=>'purchase'));
			}
			else if($this->Cookie->read('takesubscription'))
			{
				$this->Cookie->delete('takesubscription');
				$this->redirect(array('controller'=>'Members','action'=>'subscription'));
			}
			else
			{
				//$this->redirect(array('action'=>'index'));
$this->redirect($this->referer());
			}
		}
		else{
			$this->set('error',$error);					
		}
	}
	
	function logout()
	{
		$this->layout='';		
		$this->Session->delete('LocTrain');
		$this->redirect(array('controller'=>'Home','action'=>'index'));
	}
	function  forgot_password()
	{
		$this->layout =	'';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$locale = $this->Session->read('LocTrain.locale');
	
		if(!empty($this->data))
		{
			$errors=array();
			$error=$this->validate_forgot_pass($this->data);
			if(count($error)==0)
			{	

$this->loadModel('Member');
				$getMemberDetail =$this->Member->find('first',array('conditions'=>array('Member.email'=>$this->data['Member']['email'],'Member.status'=>1),'contain'=>array()));				
				$email=$getMemberDetail['Member']['email'];
$m_locale=$getMemberDetail['Member']['locale']?$getMemberDetail['Member']['locale']:'en';
				
					if($m_locale == 'de')
						{
							$reset='Kennwort zurücksetzen';
							
						 
						}
						else if($m_locale == 'ar')
						{	
							$reset='إعادة تعيين كلمة المرور';
							
						}
						else if($m_locale == 'fr')
						{	
							$reset='réinitialiser le mot de';
							
						}
						else if($m_locale == 'it')
						{	
							$reset='Resettare la password';
							
						}
						else if($m_locale == 'ja')
						{	
							$reset='パスワードを�?設定�?�る';
							
						}
						else if($m_locale == 'ko')
						{	
							$reset='암호 재설정';
						
						}
						else if($m_locale == 'pt')
						{	
							$reset='Redefinição de senha';
							
						}
						else if($m_locale == 'ru')
						{	
							$reset='Сбро�? парол�?';
							
						}
						else if($m_locale == 'zh')
						{	
							$reset='�?设密�?';
							
						}
						else if($m_locale == 'es')
						{	
							$reset='restablecer contraseña';
							
						}
					    else
						{
							$reset='Reset Password';
						}			
			
			
			
				
				
				$reset_code = md5(microtime());
				$this->Member->updateAll(array('Member.reset_code' =>"'".$reset_code."'"),array('Member.email' =>$email));	
				$this->loadModel('EmailTemplate');									
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>2,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
				$emailData = $emailTemplate['EmailTemplate']['description'];						
				//$link = HTTP_ROOT.'Home/reset_password/'.base64_encode(convert_uuencode($email))."/".$reset_code;		
                                $link = HTTP_ROOT.'Home/reset_password/'.urlencode(base64_encode($email))."/".$reset_code;							
				$link = "<a href='".$link."' style='text-decoration:none;color:#00aeef'>".$reset."</a>";						
				$emailData = str_replace(array('{final_name}','{reset_link}'),array($getMemberDetail['Member']['final_name'],$link),$emailData);
				//pr($emailData);die;							
				$emailTo =$email;
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
				echo "sucess-".__('password_reset_link_has_been_sent_to_your_e-mail_address');				
			}
			else
			{
				echo $error['email'][0];				
			}
		}
		die;	
	}
	
	function validate_forgot_pass_ajax()
	{
		$this->layout="";
		$this->autoRender=false;
		if($this->RequestHandler->isAjax())
		{
			$errors_msg = null;
			$errors = $this->validate_forgot_pass($this->data);							
			if ( is_array ( $this->data ) )
			{
				foreach ($this->data['Member'] as $key => $value )
				{
					if( array_key_exists ( $key, $errors) )
					{
						foreach ( $errors [ $key ] as $k => $v )
						{
							$errors_msg .= "error|$key|$v";
						}		
					}
					else 
					{
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_forgot_pass($data)
	{			
		$errors = array ();		
		if (trim($data['Member']['email'])=="")
		{
			$errors ['email'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		elseif($this->validEmail(trim($data['Member']['email'])) =='false')
		{
			$errors['email'][] = __(INVALID_EMAIL,true)."\n";
		}			
		elseif(trim($data['Member']['email'])!="")
		{
			$this->loadModel('Member');
			$checkexistUser=$this->Member->find('count',array('conditions'=>array('Member.email'=>$data['Member']['email'],'Member.status'=>1)));	
			if($checkexistUser==0)
			{
				$errors ['email'] [] = __(EMAIL_NOT_EXIST,true)."\n";
			}
		}
		return $errors;
	}
	
	function success_password_reset()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
	}
	function teaching_view()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
	}
	
	function catalogue($id=0)
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Course');	
		$this->loadModel('Interest');
		$this->loadModel('Member');
		
		$last_id = $this->pageLimit;
		$last_record_id = $this->pageLimit;
		$update_record = 0;
		$locale = $this->Session->read('LocTrain.locale');
		$locale_id = $this->Session->read('LocTrain.LangId');
		$this->loadModel('MemberSubscription');
		$date = date("Y-m-d H:i:s");
		//$getCourses = $this->Cookie->read('LocTrainPurchase');
		$this->loadModel('purchaseItem');
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
		if($login_m_id){
                    $order_ids_mem=array();
            $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>		$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                    if(!empty($get_array)){
                                    foreach($get_array as $row){
                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                    }
                                    } 
$getCourses = $order_ids_mem;
}else{
		$getCourses = $this->Cookie->read('LocTrainPurchase');
}
		$cookie_courses = array();
		if(!empty($getCourses)){
			foreach($getCourses as $course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
			}
		}
		$subscribed=0;
		
		if($this->Session->read('LocTrain.id'))
			{
			$login_m_id=$this->Session->read('LocTrain.id');
			$contain=array('CourseLesson','Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id)));
			$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
		}
		else
		{
			$contain = array('CoursePurchaseList'=>array('fields'=>array('mem_id','type'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))),'CourseLesson'=>array('order'=>array('CourseLesson.id desc'),'fields'=>array('video','extension','image')));
		}
		
		
		if($this->RequestHandler->isAjax()){
			$this->layout = '';
			$this->autoRender = false;
			$conditions = array('Course.status'=>'1');
			$order = array('Course.id DESC');
			$id = $this->params->query['limit'];	
				
			$last_id = $this->params->query['limit'] + $this->pageLimit;
			$last_record_id = $this->params->query['limit'] +  $this->pageLimit;
			if(isset($this->params->query['keyword']) && !empty($this->params->query['keyword'])){
				$this->loadModel('CourseKeyword');
				$keyword = explode(",",$this->params->query['keyword']);				
				$member_keyword_ids = $this->CourseKeyword->find('list',array('conditions'=>array('CourseKeyword.keyword'=>$keyword),'fields'=>array('course_id')));
				$conditions = array_merge($conditions,array('Course.id'=>$member_keyword_ids));
			}
			if(isset($this->params->query['author']) && !empty($this->params->query['author'])){
				$author_id = $this->params->query['author'];					
				$conditions = array_merge($conditions,array('Course.m_id'=>$author_id));
			}
			if(isset($this->params->query['order']) && !empty($this->params->query['order'])){
				switch(trim($this->params->query['order'])){
					case 'title': $order = array('Course.title ASC'); break;
					case 'date': $order = array('Course.date_added DESC','Course.id DESC'); break;
					case 'rating': $order = array('Course.rating DESC'); break;
					case 'reviews': $order = array('Course.review DESC'); break;
				}
			}
			if(isset($this->params->query['duration_opt']) && !empty($this->params->query['duration_opt'])){
				switch(trim($this->params->query['duration_opt'])){
					case 'all': $additional_cond = array('Course.duration_seconds >='=>0); break;
					case '<15': $additional_cond = array('Course.duration_seconds BETWEEN ? AND ?'=>array(0,900)); break;
					case '15>30<': $additional_cond = array('Course.duration_seconds BETWEEN ? AND ?'=>array(900,1800)); break;
					case '30>60<': $additional_cond = array('Course.duration_seconds BETWEEN ? AND ?'=>array(1800,3600)); break;
					case '60>': $additional_cond = array('Course.duration_seconds >='=>3600); break;
				}
				$conditions = array_merge($conditions,$additional_cond);
			}
			if(isset($this->params->query['score']) && !empty($this->params->query['score'])){
				$additional_cond_rating = array('Course.rating'=>$this->params->query['score']);
				$conditions = array_merge($conditions,$additional_cond_rating);
			}
			
			$this->set(compact('last_record_id','update_record'));
			if($this->params->query['type'] == '1'){
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => $id));
				$this->set(compact('course_list','last_id','cookie_courses','subscribed'));
				$this->viewPath = 'Elements'.DS.'frontElements/home';
				$this->render('courses_list');
			}
			else if($this->params->query['type'] == '2'){
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => $id));
				$this->set(compact('course_list','last_id','subscribed'));		
				$this->viewPath = 'Elements'.DS.'frontElements/home';
				$this->render('courses_list');
			}
			else if($this->params->query['type'] == '3'){
				$last_id =  $this->pageLimit;
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => $id));
				$this->set(compact('course_list','last_id','subscribed'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('courses_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_courses_match_the_filter').'</div>';die;
				}
			}
			else if($this->params->query['type'] == '4'){
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));			
				$this->set(compact('course_list','last_id','subscribed'));
				$this->viewPath = 'Elements'.DS.'frontElements/home';
				$this->render('courses_list');
			}
			else if($this->params->query['type'] == '5'){			
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->params->query['total_record'],'offset' => null ));
				$update_record = 1;
				$this->set(compact('course_list','last_id','update_record','subscribed'));
				$this->viewPath = 'Elements'.DS.'frontElements/home';
				$this->render('courses_list');
			}
			else if($this->params->query['type'] == '6'){
				
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));
				$this->set(compact('course_list','last_id','update_record','subscribed'));
				$this->viewPath = 'Elements'.DS.'frontElements/home';
				$this->render('courses_list');
			}
			else if($this->params->query['type'] == '7'){				
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));				$this->set(compact('course_list','last_id','subscribed'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('courses_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_courses_match_the_filter').'</div>';die;
				}
			}
			else if($this->params->query['type'] == '8'){				
				$course_list = $this->Course->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));				$this->set(compact('course_list','last_id','subscribed'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('courses_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_courses_match_the_filter').'</div>';die;
				}
			}
		}
		else{
			$author = $this->Member->find('list',array('conditions'=>array('Member.status'=>'1','Member.role_id'=>'3'),'order'=>array('Member.given_name desc'),'fields'=>array('given_name')));
			
			
			if($this->Session->read('LocTrain.id'))
			{
				$login_m_id=$this->Session->read('LocTrain.id');
				$course_list = $this->Course->find('all',array('conditions'=>array('Course.status'=>'1'),'order'=>array('Course.id desc'),'contain'=>array('CourseLesson','Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id))),'limit'=>$this->pageLimit,'offset' => null));	
			}
			else
			{
				$course_list = $this->Course->find('all',array('conditions'=>array('Course.status'=>'1'),'order'=>array('Course.id desc'),'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));
			}
			
			//pr($course_list);die;
			$interest_list = '';
			$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest asc'));		
			if(!empty($interest)){
				foreach($interest as $interestVal):			
					$interest_list .= '{value:'."'".$interestVal['Interest']['interest']."'".", name:"."'".$interestVal['Interest']['interest']."'},";	
				endforeach;	
			}else{
				$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
			}
		}
		$this->set(compact('title_for_layout','course_list','last_id','interest_list','author','last_record_id','update_record','cookie_courses','subscribed'));		
	}
	
	function catalogue_video($id=NULL)
	{
		$this->layout =	'';
		$title_for_layout = 'Localization Training';
		$login_m_id = $this->Session->read('LocTrain.id');
		$this->loadModel('Course');
		$this->loadModel('CourseWatch');
		
		$date=date("Y-m-d H:i:s");
		
		$array_purchased_lesson=array();
		$course_lesson_id = @convert_uudecode(base64_decode($id));
		$video = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_lesson_id),'contain'=>array('CourseLesson'=>array('conditions'=>array('CourseLesson.extension !='=>'zip','CourseLesson.extension !='=>''),'fields'=>array('video','title','extension')))));	
		$this->loadModel('MemberSubscription');
		$date = date("Y-m-d H:i:s");
		$videos = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_lesson_id),'contain'=>array('Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CourseLesson'=>array('fields'=>array('video','title','extension')))));
		$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
		//pr($video);
		$new=$video;
			if(!$subscribed)
			{
					if(!empty($videos['Subscription_course']))
					{	
						$j=0;
						$watched_before = $this->CourseWatch->find('first',array('conditions'=>array('CourseWatch.course_id'=>$course_lesson_id,'CourseWatch.member_id'=>$login_m_id)));
						if(empty($watched_before))
						{
							$data=$this->CourseWatch->create();
							$data['CourseWatch']['member_id']= $login_m_id;
							$data['CourseWatch']['course_id']= $course_lesson_id;
							$data['CourseWatch']['date_viewed']= strtotime($date);
							$this->CourseWatch->save($data);
						}
						else
						{
						
							$this->CourseWatch->id=$watched_before['CourseWatch']['id'];
							$this->CourseWatch->saveField('date_viewed',strtotime($date));
						}
						foreach($new['CourseLesson'] as $lesson)
						{
							
							$new['CourseLesson'][$j++]['purchased']='_watermark';
						}
					}
					else if(!empty($videos['Subscription_lesson']))
					{
						$watched_before = $this->CourseWatch->find('first',array('conditions'=>array('CourseWatch.course_id'=>$course_lesson_id,'CourseWatch.member_id'=>$login_m_id)));
						if(empty($watched_before))
						{
							$data=$this->CourseWatch->create();
							$data['CourseWatch']['member_id']= $login_m_id;
							$data['CourseWatch']['course_id']= $course_lesson_id;
							$data['CourseWatch']['date_viewed']= strtotime($date);
							$this->CourseWatch->save($data);
						}
						else
						{
							$this->CourseWatch->id=$watched_before['CourseWatch']['id'];
							$this->CourseWatch->saveField('date_viewed',strtotime($date));
						}
						foreach($videos['Subscription_lesson'] as $candidiate_lesson)
						{
							$array_purchased_lesson[] = $candidiate_lesson['lesson_id'];
						}
						
						$m=0;
						foreach($new['CourseLesson'] as $lesson)
						{
							if(in_array($lesson['id'],$array_purchased_lesson))
							{
								$new['CourseLesson'][$m++]['purchased']='_watermark';
							}
							else
							{
								$new['CourseLesson'][$m++]['purchased']='_preview';
							}
						}
					}
					else
					{
						$p=0;
						foreach($new['CourseLesson'] as $lesson)
						{
							$new['CourseLesson'][$p++]['purchased']='_preview';
						}
						
					}
			}
			else
			{
				$j=0;
						$watched_before = $this->CourseWatch->find('first',array('conditions'=>array('CourseWatch.course_id'=>$course_lesson_id,'CourseWatch.member_id'=>$login_m_id)));
						if(empty($watched_before))
						{
							$data=$this->CourseWatch->create();
							$data['CourseWatch']['member_id']= $login_m_id;
							$data['CourseWatch']['course_id']= $course_lesson_id;
							$data['CourseWatch']['date_viewed']= strtotime($date);
							$this->CourseWatch->save($data);
						}
						else
						{
							$this->CourseWatch->id=$watched_before['CourseWatch']['id'];
							$this->CourseWatch->saveField('date_viewed',strtotime($date));
						}
						foreach($new['CourseLesson'] as $lesson)
						{
							
							$new['CourseLesson'][$j++]['purchased']='_watermark';
						}
					
			}
		//pr($new);
		$this->set(compact('title_for_layout','new','id'));	
		if($this->RequestHandler->isAjax()){
			$this->viewPath = 'Elements'.DS.'frontElements/home';
			$this->render('render_video');
			
		}
	}
	
	 function authors_video($id=NULL)
	{
	
		$this->layout =	'';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Course');	
		$locale_id = $this->Session->read('LocTrain.LangId');
		$course_lesson_id = @convert_uudecode(base64_decode($id));
		$video = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_lesson_id),'contain'=>array('CourseLesson'=>array('fields'=>array('video','title','extension')))));	

		$this->set(compact('title_for_layout','video','id'));	
		if($this->RequestHandler->isAjax()){
			$this->viewPath = 'Elements'.DS.'frontElements/home';
			$this->render('render_authors_video');			
		}
	} 

		//-----------  View Course details page-----------------------		
	function view_course_details($id=NULL,$less_id=NULL,$less_yn=NULL)
	{		
			
		$decoded_id = @convert_uudecode(base64_decode($id));
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('Course');
		$locale_id = $this->Session->read('LocTrain.LangId');
		$this->loadModel('Member'); 
		$this->loadModel('MemberSubscription');
		$date = date("Y-m-d H:i:s");
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;		
		$m_id = $this->Session->read('LocTrain.id');  
		$this->Course->unbindModel(
		array('hasMany' => array('Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','CourseSubcategory','CourseCategory','CourseSlSubcategory')));

		$course_details = $this->Course->find('first',array('conditions'=>array('Course.id'=>$decoded_id),'contain'=>array('Member'=>array('id','mem_acc_no','final_name','about_me','image'),'CourseKeyword'=>array('fields'=>array('keyword')),'CourseRating'=>array('fields'=>array('rating')),'CourseLesson'=>array('conditions'=>array('CourseLesson.status !='=>'2','CourseLesson.upload_zip_status !='=>'1' ),'order'=>'CourseLesson.id ASC'),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0)),'CourseRemark'=>array('conditions'=>array('CourseRemark.block'=>0),'Member'=>array('id','family_name','given_name'),'RemarkReport'=>array('Member'=>array('id','given_name','family_name'))))));
$this->Course->unbindModel(
		array('hasMany' => array('Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','CourseSubcategory','CourseCategory','CourseSlSubcategory')));

		$course_details_wish = $this->Course->find('first',array('conditions'=>array('Course.id'=>$decoded_id),'contain'=>array('CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>1)))));

		if(empty($course_details)){
			$this->redirect(array('controller'=>'Home','action'=>'index'));	
		}
		// new code for set
		$this->course_viewed($decoded_id); // update course user views count
		$course_lesson=$course_details['CourseLesson'];
		if(!empty($course_lesson)){
			$count=0;
			foreach($course_lesson as $lesson){
				if($lesson['price'] !='0.00'){
					$count++;
				}
			}
			$total_course_lesson = $count;
			$course_purchase=$course_details['CoursePurchaseList'];
			$total_purchase_course =0;
			if(!empty($course_purchase)){
				foreach($course_purchase as $pur){
					if(in_array($pur['lesson_id'],$course_lesson)){
					$total_purchase_course++;
					}
				}
			}
			if($total_course_lesson==$total_purchase_course)
			{
				$set = __('unlink');
			}else{
				$set = __('link');
			}
			$this->set(compact('set'));
		}

		$subscription=array();
		$subscription=$course_details['CoursePurchaseList'];		
		$less_subscribed = $subscribed= $this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
	
		$mem_acc_no = ' ';
		if(!empty($login_m_id)){
			$mem_acc_no = $this->Member->find('first',array('conditions'=>array('Member.id'=>$m_id),'fields'=>array('mem_acc_no'),'contain'=>false));
			$mem_acc_no = $mem_acc_no['Member']['mem_acc_no'];
		}		
		$this->set(compact('course_details','course_details_wish','mem_acc_no','subscription','subscribed','m_id',"less_subscribed",'less_id','less_yn'));
	}
	function check_basket($decoded_id=NULL)
	{
		$this->loadModel('CourseLesson');
		$this->loadModel('CoursePurchaseList');
		$course_lesson = $this->CourseLesson->find('list',array('conditions'=>array('CourseLesson.course_id'=>$decoded_id,'CourseLesson.price NOT'=>0.00),'fields'=>array('id'),'contain'=>array('')));
		$total_course_lesson = count($course_lesson);
		$count_course_purchase = $this->CoursePurchaseList->find('all',array('conditions'=>array('CoursePurchaseList.type'=>0,'CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'),'CoursePurchaseList.lesson_id'=>$course_lesson)));
		$total_purchase_course = count($count_course_purchase);
		if($total_course_lesson==$total_purchase_course)
		{
			$set = __('unlink');
		}
		else
		{
			$set = __('link');
		}
		return $set;
	}

	function print_content($id=NULL)
	{
	    $this->layout =	'print';
		$id = convert_uudecode(base64_decode($id));
	    $this->loadModel('CmsPage');
		$content = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>$id),'contain'=>array()));
		$this->set(compact('content'));
	}
	function activation_mail($email=NULL)
	{
		$email=convert_uudecode(base64_decode($email));
                 $locale = $this->Session->read('LocTrain.locale');	
		
		$this->loadModel('Member');
		$activation=$this->Member->find('first',array('conditions'=>array('Member.email'=>$email,'Member.status'=>0),'contain'=>array()));
		if(count($activation)!=0)
		{
	
			$this->loadModel('EmailTemplate');
			// --------------------Email Confirmation To Member-----------------							
			$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>1,'locale'=>$activation['Member']['locale']),'fields'=>array('subject','description','email_from')));						
			$emailData = $emailTemplate['EmailTemplate']['description'];						
			$link = HTTP_ROOT.'Home/activate_account/'.base64_encode(convert_uuencode($email))."/".$activation['Member']['activation_link'];							
			$link = "<a href='".$link."' style='text-decoration:none;color:#00aeef'>".__('Activate my account')."</a>";						
			$emailData = str_replace(array('{final_name}','{activation_link}'),array($activation['Member']['final_name'],$link),$emailData);									
			$emailTo =$email;
			$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);	
			$this->redirect(array('action'=>'sent_activation'));
		}
		
	}
	function sent_activation()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
	}
	function author()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
	}
	function reset_password($email=NULL,$varificationcode=NULL)
	{  
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		//$email=convert_uudecode(base64_decode($email));
		$email=base64_decode(urldecode($email));
		
		$this->loadModel('Member');
		$reset_member_password = $this->Member->find('first',array('conditions'=>array('Member.email'=>$email,'Member.status'=>1,'Member.reset_code' => $varificationcode),'contain'=>array()));
		//print_r($reset_member_password);die;
		if(empty($this->data) && empty($reset_member_password))
		{
			$this->redirect(array('controller'=>'Home','action'=>'index'));
		}
		$this->set(compact('reset_member_password'));
		if(!empty($this->data))
		{ //echo "<pre>"; print_r($this->data); print_r($_SESSION); die;
			$error = array();
			$error = $this->validate_reset_password($this->data);
			if(count($error) == 0)
			{
				
				$this->loadModel('Member');
				$checkexistUser = $this->Member->find('first',array('conditions'=>array('Member.id'=>convert_uudecode(base64_decode($this->data['Member']['id']))),'contain'=>false));
	 			$this->Member->updateAll(array('Member.reset_code' =>'"' . 0 .'"'),array('Member.email' =>$email,'Member.reset_code' => $varificationcode));
				$this->Session->write("LocTrain.login",1);
				$this->Session->write("LocTrain.id",$checkexistUser['Member']['id']);				
				$this->Session->write('LocTrain.status',$checkexistUser['Member']['status']);
				$this->Session->write('LocTrain.email',$checkexistUser['Member']['email']);
				$this->Session->write('LocTrain.Fname', $checkexistUser['Member']['final_name']);
				$this->Session->write('LocTrain.succ_reset', base64_encode(convert_uuencode($checkexistUser['Member']['id']._.time())));
//echo "<pre>"; print_r($this->data); print_r($_SESSION); die;
				$this->redirect(array('controller'=>'Members','action'=>'reset_password',base64_encode(convert_uuencode($checkexistUser['Member']['id']._.time()))));	
			}
		  	
		}
   	}
	function validate_reset_password_ajax()
	{
		$this->layout="";
		$this->autoRender=false;
		if($this->RequestHandler->isAjax())
		{
			$errors_msg = null;
			$errors = $this->validate_reset_password($this->data);							
			if ( is_array ( $this->data ) )
			{
				foreach ($this->data['Member'] as $key => $value )
				{
					if( array_key_exists ( $key, $errors) )
					{
						foreach ( $errors [ $key ] as $k => $v )
						{
							$errors_msg .= "error|$key|$v";
						}		
					}
					else 
					{
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_reset_password($data)
	{			
		$errors = array ();	
		$id = convert_uudecode(base64_decode($data['Member']['id']));	
		$this->loadModel('Member');
		$member = $this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));
		if (trim($data['Member']['ques1'])=="")
		{
			$errors ['ques1'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		/*elseif($member['Member']['final_name'] != trim($data['Member']['ques1'])){
			$errors ['ques1'] [] = __('Incorrect answer',true)."\n";
		}*/
                elseif($member['Member']['given_name'] != trim($data['Member']['ques1'])){
			$errors ['ques1'] [] = __('Incorrect answer',true)."\n";
		}
		if (trim($data['Member']['ques2'])=="")
		{
			$errors ['ques2'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		elseif($member['Member']['email'] != trim($data['Member']['ques2'])){
			$errors ['ques2'] [] = __('Incorrect answer',true)."\n";
		}
		return $errors;
	}
	
	function search()
	{
		
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('CourseKeyword');
		$this->loadModel('Course');
                $this->loadModel('CourseRating');
		$this->loadModel('CourseLesson');
		$this->loadModel('MemberSubscription');
		$this->loadModel('News');
		$this->loadModel('FreeVideo');
		$locale_id = $this->Session->read('LocTrain.LangId');

		$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
		$this->set('locale',$locale);

		$date = date("Y-m-d H:i:s");
		$q = trim($this->request->query['q']);
		if(isset($this->request->query['v'])){
			$v = trim($this->request->query['v']);
		}else{
			$v='';	
		}
		if(isset($this->request->query['nv'])){
			$nv = trim($this->request->query['nv']);
		}else{
			$nv='';	
		}
		$this->set(compact('v','nv'));	
		$last_id = $this->pageLimit;
		App::uses('String', 'Utility');
		$HighLight = new String();
	// SIMPLE SEARCHING
		$login_member_id = $this->Session->read('LocTrain.id');
		$cookie_courses = array();
$this->loadModel('purchaseItem');
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
		if($login_m_id){
                    $order_ids_mem=array();
            $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>		$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                    if(!empty($get_array)){
                                    foreach($get_array as $row){
                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                    }
                                    } 
$getCourses = $order_ids_mem;
}else{
		$getCourses = $this->Cookie->read('LocTrainPurchase');
}
		if(!empty($getCourses)){
			foreach($getCourses as $course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
			}
		}
		$subscribed=0;
		
		if($this->Session->read('LocTrain.id'))
		{
			$login_m_id=$this->Session->read('LocTrain.id');
			
			$contain=array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id)));
			$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
		}
		else
		{
			//$contain = array('CoursePurchaseList'=>array('fields'=>array('mem_id','type'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))),'CourseLesson'=>array('order'=>array('CourseLesson.id desc'),'fields'=>array('video')));
			$login_m_id=0;
			$contain=array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id)));
			$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
		}
		
		$this->set(compact('subscribed','cookie_courses'));
		
		if(!$this->RequestHandler->isAjax() && $q != ''){			
			
			$member_keyword_ids = $this->CourseKeyword->find('list',array('conditions'=>array('CourseKeyword.keyword LIKE'=>'%'.$q.'%'),'fields'=>array('course_id')));
		
			$courses = $this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)),'fields'=>array('id','date_added','title','description','date_modified','m_id','price','rating','image'),'contain'=>$contain));				
			
			$search_arr = array();
		
			$i =0;
			if(!empty($courses)){
				
				foreach($courses as $courses)
				{
					if(!empty($courses['CourseLesson']))
					{
						foreach($courses['CourseLesson'] as $course_lesson)
						{
							if(!empty($course_lesson['video']))
							{
								$search_arr[$i]['video'] = $course_lesson['video'];
							}
							$search_arr[$i]['extension'] = $course_lesson['extension'];
							if(!empty($course_lesson['video']))
							{
								
								if(in_array($course_lesson['extension'],array('zip','swf')) && !empty($course_lesson['image']))
								{   
									
									$search_arr[$i]['image']= $course_lesson['image'];
								}else if($course_lesson['extension']!='zip' && $course_lesson['extension']!='swf') 
								{
									$search_arr[$i]['image']= $course_lesson['video'];
                                }else if(in_array($course_lesson['extension'],array('zip','swf')) && empty($course_lesson['image']))
								{
									$search_arr[$i]['image'] = '';
								}								
								break;
							}
							else
							{
								$search_arr[$i]['image'] = '';
								//break;
							}
						}
						
					}
					else
					{
						$search_arr[$i]['image'] = '';
						$search_arr[$i]['extension'] ='';
					}
					
					$search_arr[$i]['date_modified'] = $courses['Course']['date_modified'];
					$d[$i]['date_modified'] = $courses['Course']['date_modified'];
					$d[$i]['title'] =  $HighLight->highlight($courses['Course']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
//******************************************************					
					$search_arr[$i]['date_added'] = $courses['Course']['date_added'];
//******************************************************							
					$search_arr[$i]['title'] =  $HighLight->highlight($courses['Course']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
			$search_arr[$i]['title_org'] =trim(strtolower($courses['Course']['title']));
					$search_arr[$i]['description'] =  $HighLight->highlight($courses['Course']['description'], $q, array('format' => '<span class="search_highlight">\1</span>'));
					$search_arr[$i]['id'] = base64_encode(convert_uuencode($courses['Course']['id']));	
					$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_course_details');
					$search_arr[$i]['apply'] = !(empty($courses['CoursePurchaseList']))?'1':'0';
					$search_arr[$i]['type'] = '1';
                                        $search_arr[$i]['item_type'] =  __('course');
					$search_arr[$i]['folder'] = $courses['Course']['m_id'];
					$search_arr[$i]['CoursePurchaseList'] = $courses['CoursePurchaseList'];
					$search_arr[$i]['CourseLesson'] = $courses['CourseLesson'];
					$search_arr[$i]['Subscription_course'] = $courses['Subscription_course'];
					$search_arr[$i]['Subscription_lesson'] = $courses['Subscription_lesson'];
					$search_arr[$i]['Course'] = $courses['Course'];
					
					$i++;
				}
			}
			//pr('hello');
			//pr($search_arr); die;
			/*$lessons = $this->CourseLesson->find('all',array('conditions'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')),'contain'=>array('Course'=>array('fields'=>array('Course.id','Course.title','Course.m_id'),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_member_id),'fields'=>array('id')))),'fields'=>array('CourseLesson.id','CourseLesson.title','CourseLesson.description','CourseLesson.course_id','CourseLesson.video')));				
			//pr($lessons);die;
			if(!empty($lessons)){
				foreach($lessons as $lessons)
				{
					$search_arr[$i]['title'] = $HighLight->highlight($lessons['CourseLesson']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
					$search_arr[$i]['description'] = $HighLight->highlight($lessons['CourseLesson']['description'], $q, array('format' => '<span class="search_highlight">\1</span>'));
					$search_arr[$i]['id'] = base64_encode(convert_uuencode($lessons['CourseLesson']['id']));	
					$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_course_details');
					$search_arr[$i]['apply'] = !(empty($lessons['Course']['CoursePurchaseList']))?'1':'0';
					$search_arr[$i]['image'] = !(empty($lessons['CourseLesson']['video']))?$lessons['CourseLesson']['video']:'';
					$search_arr[$i]['folder'] = $lessons['Course']['m_id'];
					$search_arr[$i]['type'] = '2';
					$i++;
					//pr($search_arr); die;
				}
			}*/
			$news = $this->News->find('all',array('conditions'=>array('News.status'=>1,'OR'=>array('News.title_'.$locale.' LIKE'=>'%'.$q.'%','News.description_'.$locale.' LIKE'=>'%'.$q.'%')),'fields'=>array('id','title_'.$locale,'sub_title_'.$locale,'description_'.$locale,'date_modified','date_added','image'),'contain'=>false));
			//pr($news);
			if(!empty($news)){
				foreach($news as $news)
				{
					$search_arr[$i]['title'] = $HighLight->highlight(strip_tags($news['News']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($news['News']['title_'.$locale]));
$search_arr[$i]['sub_title']=$news['News']['sub_title_'.$locale];
					$search_arr[$i]['description'] = $HighLight->highlight($news['News']['description_'.$locale], $q, array('format' => '<span class="search_highlight">\1</span>'));
					$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['News']['id']));	
					$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'news_landing');
					$search_arr[$i]['image'] = $news['News']['image'];
					$search_arr[$i]['type'] = '3';
                                         $search_arr[$i]['item_type'] = __('news');
//******************************************************					
					$search_arr[$i]['date_added'] = $news['News']['date_added'];
					$search_arr[$i]['date_modified'] = $news['News']['date_modified'];
					$d[$i]['title'] = $HighLight->highlight(strip_tags($news['News']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
					$d[$i]['date_modified'] = $news['News']['date_modified'];
//*******************************************************					
					
					$i++;
				}
			}
			
			//$search_arr = array_slice($search_arr, 0, $this->pageLimit);

//***************************FREE VIDEO***********************************************
			$freeVideos = $this->FreeVideo->find('all',array('conditions'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),'fields'=>array('id','title_'.$locale,'description_'.$locale,'free_video_url','own_video_url','date_modified','date_added'),'contain'=>false));	
			//pr($freeVideos);//die;
			
			if(!empty($freeVideos))
			{
				foreach($freeVideos as $news)
				{
					$search_arr[$i]['title'] = $HighLight->highlight(strip_tags($news['FreeVideo']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($news['FreeVideo']['title_'.$locale]));
					$search_arr[$i]['description'] = $HighLight->highlight(strip_tags($news['FreeVideo']['description_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
					$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['FreeVideo']['id']));	
					$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_free_video');
					
					if($news['FreeVideo']['free_video_url']!='')
					{
						$search_arr[$i]['free_video_url'] = $news['FreeVideo']['free_video_url'];
						$search_arr[$i]['own_video_url'] ='';
						
					}else
					{
						$search_arr[$i]['own_video_url'] = $news['FreeVideo']['own_video_url'];
						$search_arr[$i]['free_video_url'] ='';
					}
//******************************************************					
					$search_arr[$i]['date_added'] = $news['FreeVideo']['date_added'];
					$search_arr[$i]['date_modified'] = $news['FreeVideo']['date_modified'];
					$d[$i]['title'] = $HighLight->highlight(strip_tags($news['FreeVideo']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
					$d[$i]['date_modified'] = $news['FreeVideo']['date_modified'];
//*******************************************************					
					$search_arr[$i]['type'] = '1';
                                         $search_arr[$i]['item_type'] = __('free_videos');
					$i++;
				}
			}			
			/* To check order **/
			//pr($d);
			/*$d = Set::sort($d, '{n}.date_modified', 'desc');
			pr($d); 
			$d = array_slice($d, 0, $this->pageLimit);
			pr($d); die;*/
			//$search_arr = Set::sort($search_arr, '{n}.date_modified', 'desc');
			//echo '<pre>';print_r($search_arr);echo '</pre>';
			//echo count($search_arr);
			$search_arr = Hash::sort($search_arr, '{n}.title_org', 'ASC');
			//echo '<pre>';print_r($search_arr);echo '</pre>';
			$search_arr = array_slice($search_arr, 0, $this->pageLimit);
			
			
//***************************FREE VIDEO***********************************************
			
		}
		// SEARCHING ON PAGE SCROLLING 
		elseif($this->RequestHandler->isAjax())
		{
			$this->layout = '';
			$this->autoRender = false;
			$order_courses = array();
			$order_lessons = array();
			$freeOrder = array();
			$order_news = array();
			$request_type = trim($this->request->query['type']);
			//pr($this->request->query['sort_option']);
			/*if(isset($this->request->query['sort_option']))
			{
				switch(trim($this->request->query['sort_option'])){
					
					case '1': 
						$order_courses = array('Course.date_added ASC', 'Course.date_modified ASC');
						$freeOrder = array('FreeVideo.date_added ASC', 'FreeVideo.date_modified ASC');
						$order_lessons = array('CourseLesson.date_added ASC','CourseLesson.date_modified ASC');
						$order_news=array('News.date_modified ASC','News.date_added ASC');
						break;
						
					case '2':
						$order_courses = array('Course.date_added DESC', 'Course.date_modified DESC');
						$freeOrder = array('FreeVideo.date_added DESC', 'FreeVideo.date_modified DESC');
						
						$order_lessons = array('CourseLesson.date_added DESC','CourseLesson.date_modified DESC');
						$order_news=array('News.date_modified DESC','News.date_added DESC');
						break;
				}
				
			}*/
	
	
			if($request_type == '1')
			{
				
					$q = trim($this->request->query['q']);
					$record_limit = trim($this->request->query['records']);
					$last_id = $record_limit + $this->pageLimit;
					
					$member_keyword_ids = $this->CourseKeyword->find('list',array('conditions'=>array('CourseKeyword.keyword LIKE'=>'%'.$q.'%'),'fields'=>array('course_id')));
					$news_conditions = array('News.status'=>1,'OR'=>array('News.title_'.$locale.' LIKE'=>'%'.$q.'%','News.description_'.$locale.' LIKE'=>'%'.$q.'%'));
					if(isset($this->request->query['date_from'])&& !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
					{
						$locale_id = $this->Session->read('LocTrain.LangId');
						$o_f=date('Y-m-d',$this->request->query['date_from']);
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from=trim(strtotime($o_f.'00:00:00'));
						$to=trim(strtotime($o_t.'23:59:59'));
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)),array('OR'=>array('Course.date_modified between ? and ?'=>array($from, $to),'Course.date_added between ? and ?'=>array($from, $to))));
						
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')),array('OR'=>array('CourseLesson.date_modified between ? and ?'=>array($from, $to),'CourseLesson.date_added between ? and ?'=>array($from, $to)) ));
						
						$news_conditions = array_merge($news_conditions,array('News.date_modified between ? and ?'=>array($from, $to)));
						
					}else if(isset($this->request->query['date_from'])&& !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && empty($this->request->query['date_to']))
					{
						$locale_id = $this->Session->read('LocTrain.LangId');
						$o_f=date('Y-m-d',$this->request->query['date_from']);						
						$from=trim(strtotime($o_f.'00:00:00'));
						$to='';
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)),array('OR'=>array('Course.date_modified >='=>$from,'Course.date_added >='=>$from)));
						
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')),array('OR'=>array('CourseLesson.date_modified >='=>$from,'CourseLesson.date_added >='=>$from) ));
						
						$news_conditions = array_merge($news_conditions,array('News.date_modified >='=>$from));
						
					}else if(isset($this->request->query['date_from'])&& empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
					{
						$locale_id = $this->Session->read('LocTrain.LangId');
						
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from='';
						$to=trim(strtotime($o_t.'23:59:59'));
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)),array('OR'=>array('Course.date_modified <='=>$to,'Course.date_added <='=>$to)));
						
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')),array('OR'=>array('CourseLesson.date_modified <='=>$to,'CourseLesson.date_added <='=>$to) ));
						
						$news_conditions = array_merge($news_conditions,array('News.date_modified <='=>$to));
						
					}
					else
					{	$locale_id = $this->Session->read('LocTrain.LangId');
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)));
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')));
					}
					
					//$courses = $this->Course->find('all',array('conditions'=>$course_conditions,'order'=>$order_courses,'fields'=>array('id','title','description','m_id','date_added','date_modified'),'contain'=>$contain));
					
					$courses = $this->Course->find('all',array('conditions'=>$course_conditions,'fields'=>array('id','title','description','m_id','date_added','date_modified','price','rating','image'),'contain'=>$contain));
					
					$search_arr = array();
					$i =0;	
					if(!empty($courses)){
						foreach($courses as $courses)
						{
							if(!empty($courses['CourseLesson']))
							{
						
								foreach($courses['CourseLesson'] as $course_lesson)
								{
									if(!empty($course_lesson['video']))
									{
										$search_arr[$i]['video'] = $course_lesson['video'];
									}
									if(!empty($course_lesson['video']))
									{
										$search_arr[$i]['extension'] = $course_lesson['extension'];
										if(in_array($course_lesson['extension'],array('zip','swf')) && !empty($course_lesson['image']))
										{
											$search_arr[$i]['image']= $course_lesson['image'];
										}else if($course_lesson['extension']!='zip' && $course_lesson['extension']!='swf') {
											$search_arr[$i]['image']= $course_lesson['video'];
										}else if(in_array($course_lesson['extension'],array('zip','swf')) && empty($course_lesson['image']))
										{
											$search_arr[$i]['image'] = '';
										}								
																
										break;
									}
									else
									{
										$search_arr[$i]['image'] = '';
										//break;
									}
								}
						
							}
							else
							{
								$search_arr[$i]['image'] = '';
								$search_arr[$i]['extension'] ='';
							}
//*****************************************************									
							$search_arr[$i]['date_modified'] = $courses['Course']['date_modified'];
							$search_arr[$i]['date_added'] = $courses['Course']['date_added'];
							
							//$d[$i]['date_modified'] = $courses['Course']['date_modified'];
							//$d[$i]['title'] =  $HighLight->highlight($courses['Course']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
							
//*****************************************************
							$search_arr[$i]['title'] = $HighLight->highlight($courses['Course']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($courses['Course']['title']));
							$search_arr[$i]['description'] = $HighLight->highlight($courses['Course']['description'], $q, array('format' => '<span class="search_highlight">\1</span>'));	
							$search_arr[$i]['id'] = base64_encode(convert_uuencode($courses['Course']['id']));
							$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_course_details');
							$search_arr[$i]['apply'] = !(empty($courses['CoursePurchaseList']))?'1':'0';
							//$search_arr[$i]['image'] = '';
							$search_arr[$i]['type'] = '1';
                                                        $search_arr[$i]['item_type'] =  __('course');
							$search_arr[$i]['folder'] = $courses['Course']['m_id'];
							$search_arr[$i]['CoursePurchaseList'] = $courses['CoursePurchaseList'];
							$search_arr[$i]['CourseLesson'] = $courses['CourseLesson'];
							$search_arr[$i]['Subscription_course'] = $courses['Subscription_course'];
							$search_arr[$i]['Subscription_lesson'] = $courses['Subscription_lesson'];
							$search_arr[$i]['Course'] = $courses['Course'];
							$i++;
						}
					}
									
				/*	$lessons = $this->CourseLesson->find('all',array('conditions'=>$lesson_conditions ,'order'=>$order_lessons,'contain'=>array('Course'=>array('fields'=>array('Course.id','Course.title','Course.m_id'),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_member_id),'fields'=>array('id')))),'fields'=>array('CourseLesson.id','CourseLesson.title','CourseLesson.description','CourseLesson.course_id','CourseLesson.video')));	
					//pr($lessons);
					if(!empty($lessons)){
						foreach($lessons as $lessons)
						{
							$search_arr[$i]['title'] = $HighLight->highlight($lessons['CourseLesson']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
							$search_arr[$i]['description'] = $HighLight->highlight($lessons['CourseLesson']['description'], $q, array('format' => '<span class="search_highlight">\1</span>'));
							$search_arr[$i]['id'] = base64_encode(convert_uuencode($lessons['CourseLesson']['id']));		
							$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_course_details');
							$search_arr[$i]['apply'] = !(empty($lessons['Course']['CoursePurchaseList']))?'1':'0';
							//$search_arr[$i]['image'] = '';
							$search_arr[$i]['image'] = !(empty($lessons['CourseLesson']['video']))?$lessons['CourseLesson']['video']:'';
							$search_arr[$i]['folder'] = $lessons['Course']['m_id'];
							$search_arr[$i]['type'] = '2';
							$i++;
						}
					}
			*/
					//$news = $this->News->find('all',array('conditions'=>$news_conditions,'order'=>$order_news ,'fields'=>array('id','title','description','date_modified','date_added'),'contain'=>false));				
		
					$news = $this->News->find('all',array('conditions'=>$news_conditions,'fields'=>array('id','title_'.$locale,'description_'.$locale,'sub_title_'.$locale,'date_modified','date_added'),'contain'=>false));
					if(!empty($news)){
						foreach($news as $news)
						{
							$search_arr[$i]['date_modified'] = $news['News']['date_modified'];
							$search_arr[$i]['date_added'] = $news['News']['date_added'];
							
							$search_arr[$i]['sub_title']=$news['News']['sub_title_'.$locale];
							$search_arr[$i]['title'] = $HighLight->highlight(strip_tags($news['News']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($news['News']['title_'.$locale]));
							$search_arr[$i]['description'] = $HighLight->highlight(strip_tags($news['News']['description_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
							$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['News']['id']));	
							$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['News']['id']));	
							$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'news_landing');
							$search_arr[$i]['image'] = '';
							$search_arr[$i]['type'] = '3';
                                                        $search_arr[$i]['item_type'] = __('news');
							
							////$d[$i]['date_modified'] = $news['News']['date_modified'];
							//$d[$i]['title'] =  $HighLight->highlight(strip_tags($news['News']['title']), $q, array('format' => '<span class="search_highlight">\1</span>'));
							
							
							$i++;
						}
					}
					
					
//***************************FREE VIDEO***********************************************

		if(isset($this->request->query['date_from']) && !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
		{
			
			/*$from=trim($this->request->query['date_from']);
			$to=trim($this->request->query['date_to']);*/
						$o_f=date('Y-m-d',$this->request->query['date_from']);
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from=trim(strtotime($o_f.'00:00:00'));
						$to=trim(strtotime($o_t.'23:59:59'));
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),array('OR'=>array('FreeVideo.date_modified between ? and ?'=>array($from, $to),'FreeVideo.date_added between ? and ?'=>array($from, $to))));
		}else if(isset($this->request->query['date_from']) && !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && empty($this->request->query['date_to']))
		{
			
			
						$o_f=date('Y-m-d',$this->request->query['date_from']);
						
						$from=trim(strtotime($o_f.'00:00:00'));
						$to='';
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),array('OR'=>array('FreeVideo.date_modified >='=>$from,'FreeVideo.date_added >='=>$from)));
		}else if(isset($this->request->query['date_from']) && empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
		{
			
			
						
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from='';
						$to=trim(strtotime($o_t.'23:59:59'));
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),array('OR'=>array('FreeVideo.date_modified <='=> $to,'FreeVideo.date_added <='=> $to)));
		}
		else
		{ 
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')));
			
		}
			
			
			
			//$freeVideos = $this->FreeVideo->find('all',array('conditions'=>$free_video_conditions,'order'=>$freeOrder,'fields'=>array('id','title','date_added','description','free_video_url','own_video_url','date_modified'),'contain'=>false));	
			
			$freeVideos = $this->FreeVideo->find('all',array('conditions'=>$free_video_conditions,'fields'=>array('id','title_'.$locale,'date_added','description_'.$locale,'free_video_url','own_video_url','date_modified'),'contain'=>false));	
			
			
			if(!empty($freeVideos))
			{
				foreach($freeVideos as $news)
				{
					$search_arr[$i]['title'] = $HighLight->highlight(strip_tags($news['FreeVideo']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($news['FreeVideo']['title_'.$locale]));
					$search_arr[$i]['description'] = $HighLight->highlight(strip_tags($news['FreeVideo']['description_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
					$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['FreeVideo']['id']));	
					$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_free_video');
					
					if($news['FreeVideo']['free_video_url']!='')
					{
						$search_arr[$i]['free_video_url'] = $news['FreeVideo']['free_video_url'];
						$search_arr[$i]['own_video_url'] ='';
						
					}else
					{
						$search_arr[$i]['own_video_url'] = $news['FreeVideo']['own_video_url'];
						$search_arr[$i]['free_video_url'] ='';
						
					}
					
					$search_arr[$i]['type'] = '1';
                                        $search_arr[$i]['item_type'] = __('free_videos');
					$search_arr[$i]['date_modified'] = $news['FreeVideo']['date_modified'];
					$search_arr[$i]['date_added'] = $news['FreeVideo']['date_added'];
					
					// $d[$i]['date_modified'] = $news['FreeVideo']['date_modified'];
					// $d[$i]['title'] =  $HighLight->highlight(strip_tags($news['FreeVideo']['title']), $q, array('format' => '<span class="search_highlight">\1</span>'));
					
					$i++;
				}
			}
			
			
//***************************FREE VIDEO***********************************************
					//**** TESTING ARRAY ******
					/*if(trim($this->request->query['sort_option']) == 1)
					{
						$d = Set::sort($d, '{n}.date_modified', 'ASC');
					}
					if(trim($this->request->query['sort_option']) == 2)
					{
						$d = Set::sort($d, '{n}.date_modified', 'desc');
					}
					pr($d);die;*/
					//**** TESTING ARRAY ******
					
					
					if(trim($this->request->query['sort_option']) == 1 && $this->request->query['filter_opt'] =='date')
					{
						$search_arr = Set::sort($search_arr, '{n}.date_modified', 'ASC');
                                        }
					if(trim($this->request->query['sort_option']) == 1 && $this->request->query['filter_opt'] =='title')
					{
                                           $search_arr = Hash::sort($search_arr, '{n}.title_org', 'ASC'); 
                                        }
					if(trim($this->request->query['sort_option']) == 2  && $this->request->query['filter_opt'] =='date')
					{
						$search_arr = Set::sort($search_arr, '{n}.date_modified', 'desc');
					}
					if(trim($this->request->query['sort_option']) == 2  && $this->request->query['filter_opt'] =='title')
					{
                                           $search_arr = Hash::sort($search_arr, '{n}.title_org', 'desc'); 
                                        }
					
					
					
					
					//pr($search_arr);die;
					//asort($search_arr);
					$search_arr = array_slice($search_arr, $record_limit, $this->pageLimit);
					$this->set(compact('last_id','search_arr','q'));	
					$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('search_list');
					
					
			}
			
			if($request_type == '2'){
					
					$q = trim($this->request->query['q']);
					$record_limit = trim($this->request->query['records']);
					$last_id = $record_limit + $this->pageLimit;
					$member_keyword_ids = $this->CourseKeyword->find('list',array('conditions'=>array('CourseKeyword.keyword LIKE'=>'%'.$q.'%'),'fields'=>array('course_id')));
					//print_r($member_keyword_ids); 
					//die;
					
					$news_conditions = array('News.status'=>1,'OR'=>array('News.title_'.$locale.' LIKE'=>'%'.$q.'%','News.description_'.$locale.' LIKE'=>'%'.$q.'%'));
					
				
					if(isset($this->request->query['date_from'])&& !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
					{
						$locale_id = $this->Session->read('LocTrain.LangId');
						$o_f=date('Y-m-d',$this->request->query['date_from']);
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from=trim(strtotime($o_f.'00:00:00'));
						$to=trim(strtotime($o_t.'23:59:59'));
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)),array('OR'=>array('Course.date_modified between ? and ?'=>array($from, $to),'Course.date_added between ? and ?'=>array($from, $to))));
						
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')),array('OR'=>array('CourseLesson.date_modified between ? and ?'=>array($from, $to),'CourseLesson.date_added between ? and ?'=>array($from, $to)) ));
						
						$news_conditions = array_merge($news_conditions,array('News.date_modified between ? and ?'=>array($from, $to)));
						
					}else if(isset($this->request->query['date_from'])&& !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && empty($this->request->query['date_to']))
					{
						$locale_id = $this->Session->read('LocTrain.LangId');
						$o_f=date('Y-m-d',$this->request->query['date_from']);						
						$from=trim(strtotime($o_f.'00:00:00'));
						$to='';
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)),array('OR'=>array('Course.date_modified >='=>$from,'Course.date_added >='=>$from)));
						
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')),array('OR'=>array('CourseLesson.date_modified >='=>$from,'CourseLesson.date_added >='=>$from) ));
						
						$news_conditions = array_merge($news_conditions,array('News.date_modified >='=>$from));
						
					}else if(isset($this->request->query['date_from'])&& empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
					{
						$locale_id = $this->Session->read('LocTrain.LangId');
						
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from='';
						$to=trim(strtotime($o_t.'23:59:59'));
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)),array('OR'=>array('Course.date_modified <='=>$to,'Course.date_added <='=>$to)));
						
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')),array('OR'=>array('CourseLesson.date_modified <='=>$to,'CourseLesson.date_added <='=>$to) ));
						
						$news_conditions = array_merge($news_conditions,array('News.date_modified <='=>$to));
						
					}else
					{	$locale_id = $this->Session->read('LocTrain.LangId');
						$course_conditions = array('AND'=>array('Course.status'=>1,'Course.isPublished'=>1,'OR'=>array('Course.title LIKE'=>'%'.$q.'%','Course.description LIKE'=>'%'.$q.'%','Course.id'=>$member_keyword_ids)));
						$lesson_conditions = array('AND'=>array('CourseLesson.status'=>1,'OR'=>array('CourseLesson.title LIKE'=>'%'.$q.'%','CourseLesson.description LIKE'=>'%'.$q.'%')));
					}

					//print_r($course_conditions);
					//$courses = $this->Course->find('all',array('conditions'=>$course_conditions,'order'=>$order_courses,'fields'=>array('id','title','description','date_modified','date_added','m_id'),'contain'=>$contain));
					
					$courses = $this->Course->find('all',array('conditions'=>$course_conditions,'fields'=>array('id','title','description','date_modified','date_added','m_id','price','rating','image'),'contain'=>$contain));
				
					$search_arr = array();
					$i = 0;	
					if(!empty($courses)){
					
						foreach($courses as $courses)
						{
							if(!empty($courses['CourseLesson']))
							{
						
								foreach($courses['CourseLesson'] as $course_lesson)
								{
									if(!empty($course_lesson['video']))
									{
										$search_arr[$i]['video'] = $course_lesson['video'];
									}
									if(!empty($course_lesson['video']))
									{
										$search_arr[$i]['extension'] = $course_lesson['extension'];
										if(in_array($course_lesson['extension'],array('zip','swf')) && !empty($course_lesson['image']))
										{
											$search_arr[$i]['image']= $course_lesson['image'];
										}else if($course_lesson['extension']!='zip' && $course_lesson['extension']!='swf') {
											$search_arr[$i]['image']= $course_lesson['video'];
										}else if(in_array($course_lesson['extension'],array('zip','swf')) && empty($course_lesson['image']))
										{
											$search_arr[$i]['image']= '';
										}								
										break;
									}
									else
									{
										$search_arr[$i]['image'] = '';
										//break;
									}
								}
							}
							else
							{
								$search_arr[$i]['image'] = '';
								$search_arr[$i]['extension'] ='';
							}
							$search_arr[$i]['title'] = $HighLight->highlight($courses['Course']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($courses['Course']['title']));
							$search_arr[$i]['description'] = $HighLight->highlight($courses['Course']['description'], $q, array('format' => '<span class="search_highlight">\1</span>'));	
							$search_arr[$i]['id'] = base64_encode(convert_uuencode($courses['Course']['id']));
							$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_course_details');
							$search_arr[$i]['apply'] = !(empty($courses['CoursePurchaseList']))?'1':'0';
							//$search_arr[$i]['image'] = '';
							$search_arr[$i]['type'] = '1';
                                                        $search_arr[$i]['item_type'] = 'Course';
							$search_arr[$i]['folder'] = $courses['Course']['m_id'];
							$search_arr[$i]['CoursePurchaseList'] = $courses['CoursePurchaseList'];
							$search_arr[$i]['CourseLesson'] = $courses['CourseLesson'];
							$search_arr[$i]['Subscription_course'] = $courses['Subscription_course'];
							$search_arr[$i]['Subscription_lesson'] = $courses['Subscription_lesson'];
							$search_arr[$i]['Course'] = $courses['Course'];
							$search_arr[$i]['date_modified'] = $courses['Course']['date_modified'];
							$search_arr[$i]['date_added'] = $courses['Course']['date_added'];
							
							//$d[$i]['date_modified'] = $courses['Course']['date_modified'];
							//$d[$i]['title'] =  $HighLight->highlight($courses['Course']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
							
							$i++;
						}
					}
					//	pr($search_arr); 
				/*	$lessons = $this->CourseLesson->find('all',array('conditions'=>$lesson_conditions,'order'=>$order_lessons,'contain'=>array('Course'=>array('fields'=>array('Course.id','Course.title','Course.m_id'),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_member_id),'fields'=>array('id')))),'fields'=>array('CourseLesson.id','CourseLesson.title','CourseLesson.description','CourseLesson.course_id','CourseLesson.video')));	
					//pr($lessons); 	
					if(!empty($lessons)){
						foreach($lessons as $lessons)
						{
							$search_arr[$i]['title'] = $HighLight->highlight($lessons['CourseLesson']['title'], $q, array('format' => '<span class="search_highlight">\1</span>'));
							$search_arr[$i]['description'] = $HighLight->highlight($lessons['CourseLesson']['description'], $q, array('format' => '<span class="search_highlight">\1</span>'));
							$search_arr[$i]['id'] = base64_encode(convert_uuencode($lessons['CourseLesson']['id']));		
							$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_course_details');
							$search_arr[$i]['apply'] = !(empty($lessons['Course']['CoursePurchaseList']))?'1':'0';
							//$search_arr[$i]['image'] = '';
							$search_arr[$i]['image'] = !(empty($lessons['CourseLesson']['video']))?$lessons['CourseLesson']['video']:'';
							$search_arr[$i]['folder'] = $lessons['Course']['m_id'];
							$search_arr[$i]['type'] = '2';
							$i++;
						}
					}*/
			//pr($search_arr);
					//$news = $this->News->find('all',array('conditions'=>$news_conditions,'order'=>$order_news ,'fields'=>array('id','title','description','date_modified','date_added'),'contain'=>false));				
					
					$news = $this->News->find('all',array('conditions'=>$news_conditions,'fields'=>array('id','title_'.$locale,'description_'.$locale,'sub_title_'.$locale,'date_modified','date_added'),'contain'=>false));
					if(!empty($news)){
						foreach($news as $news)
						{

							$search_arr[$i]['title'] = $HighLight->highlight(strip_tags($news['News']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($news['News']['title_'.$locale]));
$search_arr[$i]['sub_title']=$news['News']['sub_title_'.$locale];
							$search_arr[$i]['description_'.$locale] = $HighLight->highlight(strip_tags($news['News']['description_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));

							$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['News']['id']));	
							$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['News']['id']));	
							$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'news_landing');
							$search_arr[$i]['image'] = '';
							$search_arr[$i]['type'] = '3';
                                                        $search_arr[$i]['item_type'] = 'News';
							$search_arr[$i]['date_modified'] = $news['News']['date_modified'];
							$search_arr[$i]['date_added'] = $news['News']['date_added'];
							
							//$d[$i]['date_modified'] = $news['News']['date_modified'];
							///$d[$i]['title'] = $HighLight->highlight(strip_tags($news['News']['title']), $q, array('format' => '<span class="search_highlight">\1</span>'));
							
							$i++;
						}
					}
					
///**********************************FREE VIDEO************************************************************					
					
						
					
					/*if(isset($this->request->query['date_from']) && isset($this->request->query['date_to']))
						{
							$from=trim($this->request->query['date_from']);
							$to=trim($this->request->query['date_to']);
							$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),array('OR'=>array('FreeVideo.date_modified between ? and ?'=>array($from, $to),'FreeVideo.date_added between ? and ?'=>array($from, $to))));
						}
						else
						{
							$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title LIKE'=>'%'.$q.'%','FreeVideo.description LIKE'=>'%'.$q.'%')));
							
						}*/
if(isset($this->request->query['date_from']) && !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
		{
			
			/*$from=trim($this->request->query['date_from']);
			$to=trim($this->request->query['date_to']);*/
						$o_f=date('Y-m-d',$this->request->query['date_from']);
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from=trim(strtotime($o_f.'00:00:00'));
						$to=trim(strtotime($o_t.'23:59:59'));
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),array('OR'=>array('FreeVideo.date_modified between ? and ?'=>array($from, $to),'FreeVideo.date_added between ? and ?'=>array($from, $to))));
		}else if(isset($this->request->query['date_from']) && !empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && empty($this->request->query['date_to']))
		{
			
			
						$o_f=date('Y-m-d',$this->request->query['date_from']);
						
						$from=trim(strtotime($o_f.'00:00:00'));
						$to='';
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),array('OR'=>array('FreeVideo.date_modified >='=>$from,'FreeVideo.date_added >='=>$from)));
		}else if(isset($this->request->query['date_from']) && empty($this->request->query['date_from']) && isset($this->request->query['date_to']) && !empty($this->request->query['date_to']))
		{
			
			
						
						$o_t=date('Y-m-d',$this->request->query['date_to']);
						$from='';
						$to=trim(strtotime($o_t.'23:59:59'));
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')),array('OR'=>array('FreeVideo.date_modified <='=> $to,'FreeVideo.date_added <='=> $to)));
		}
		else
		{ 
			$free_video_conditions = array('AND'=>array('FreeVideo.status'=>1,'OR'=>array('FreeVideo.title_'.$locale.' LIKE'=>'%'.$q.'%','FreeVideo.description_'.$locale.' LIKE'=>'%'.$q.'%')));
			
		}
							
							//$freeVideos = $this->FreeVideo->find('all',array('conditions'=>$free_video_conditions,'order'=>$freeOrder,'fields'=>array('id','title','description','date_added','date_modified','free_video_url','own_video_url'),'contain'=>false));	
							
							$freeVideos = $this->FreeVideo->find('all',array('conditions'=>$free_video_conditions,'fields'=>array('id','title_'.$locale,'description_'.$locale,'date_added','date_modified','free_video_url','own_video_url'),'contain'=>false));	
							
							//pr($freeVideos);//die;
							
							if(!empty($freeVideos))
							{
								foreach($freeVideos as $news)
								{
									$search_arr[$i]['title'] = $HighLight->highlight(strip_tags($news['FreeVideo']['title_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
$search_arr[$i]['title_org'] =trim(strtolower($news['FreeVideo']['title_'.$locale]));
									$search_arr[$i]['description'] = $HighLight->highlight(strip_tags($news['FreeVideo']['description_'.$locale]), $q, array('format' => '<span class="search_highlight">\1</span>'));
									$search_arr[$i]['id'] = base64_encode(convert_uuencode($news['FreeVideo']['id']));	
									$search_arr[$i]['link'] = array('controller'=>'Home','action'=>'view_free_video');
									
									if($news['FreeVideo']['free_video_url']!='')
									{
										$search_arr[$i]['free_video_url'] = $news['FreeVideo']['free_video_url'];
										$search_arr[$i]['own_video_url'] ='';
										
									}else
									{
										$search_arr[$i]['own_video_url'] = $news['FreeVideo']['own_video_url'];
										$search_arr[$i]['free_video_url'] ='';
									}
									
									$search_arr[$i]['type'] = '1';
                                                                        $search_arr[$i]['item_type'] = 'Free Video';
									$search_arr[$i]['date_modified'] = $news['FreeVideo']['date_modified'];
									$search_arr[$i]['date_added'] = $news['FreeVideo']['date_added'];
									
										//$d[$i]['date_modified'] = $news['FreeVideo']['date_modified'];
										//$d[$i]['title'] = $HighLight->highlight(strip_tags($news['FreeVideo']['title']), $q, array('format' => '<span class="search_highlight">\1</span>'));
									
									$i++;
								}
							}
							//pr($search_arr);
					
//************************************FREE VIDEO************************************************************					
					
					//echo '<pre>';print_r($search_arr);echo '</pre>'; die; 
					//pr($d);
					//echo '2--'$this->request->query['filter_opt'];
					if(trim($this->request->query['sort_option']) == 1 && $this->request->query['filter_opt'] =='date')
					{
						$search_arr = Set::sort($search_arr, '{n}.date_modified', 'ASC');
                                        }
					if(trim($this->request->query['sort_option']) == 1 && $this->request->query['filter_opt'] =='title')
					{
                                           $search_arr = Hash::sort($search_arr, '{n}.title_org', 'ASC'); 
                                        }
					if(trim($this->request->query['sort_option']) == 2  && $this->request->query['filter_opt'] =='date')
					{
						$search_arr = Set::sort($search_arr, '{n}.date_modified', 'desc');
					}
					if(trim($this->request->query['sort_option']) == 2  && $this->request->query['filter_opt'] =='title')
					{
                                           $search_arr = Hash::sort($search_arr, '{n}.title_org', 'desc'); 
                                        }
					
					
					
					$search_arr = array_slice($search_arr, $record_limit, $this->pageLimit);
					
					$this->set(compact('last_id','search_arr','q'));
					$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('search_list');
			}
		}
		$this->loadModel('purchaseItem');
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
		if($login_m_id){
                    $order_ids_mem=array();
            $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>		$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                    if(!empty($get_array)){
                                    foreach($get_array as $row){
                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                    }
                                    } 
$getCourses = $order_ids_mem;
}else{
		$getCourses = $this->Cookie->read('LocTrainPurchase');
		
}
$cookie_lessons = array();
		if(!empty($getCourses)){
			
			foreach($getCourses as $key=>$val){
				if(strlen($key)>8)
				{
					$cookie_lessons[] = convert_uudecode(base64_decode($val));
				}
				
			}
		}
		
		$this->set(compact('title_for_layout','last_id','new_cou','q','search_arr','cookie_lessons'));
	}
	function news($id=NULL)
	{
		App::import('Helper', 'Timezone'); 
                 $timezone = new TimezoneHelper(new View(null));
                 
		$this->layout =	'public';		
		
		$title_for_layout = 'Localization Training';
		$this->loadModel('News');
		
		$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
		$this->set('locale',$locale);
		//pr($locale);die;
		//$course_list = $this->Course->find('all',array('conditions'=>array('Course.status'=>'1'),'order'=>array('Course.id desc'),'contain'=>false,'limit'=>'0,'.$this->pageLimit));	
		$news_list = $this->News->find('all',array('conditions'=>array('News.status'=>'1'),'order'=>'News.id desc','contain'=>false,'limit'=>$this->pageLimit,'offset' => null));	
$latest_news_list = $this->News->find('all',array('conditions'=>array('News.status'=>'1'),'order'=>'News.date_added desc','contain'=>false,'limit'=>'4','offset' => null));	
		$news_author=$this->News->find('list',array('conditions'=>array('News.status'=>'1'),'contain'=>array('Admin'),'fields'=>array('News.admin_id','Admin.username')));
		//pr($news_author); die;
		if($this->RequestHandler->isAjax()){
			$this->layout = '';
			$this->autoRender = false;
			
			if(isset($this->params->query['limit']))
			{
				$id=$this->params->query['limit'];
				$last_id = $id + $this->pageLimit;
			}
			
			$conditions=array('News.status'=>'1');
			
			if(isset($this->params->query['author']) && !empty($this->params->query['author']))
			{
				$admin_id=$this->params->query['author'];
				if($admin_id != 0)
				{
				
				$cond=array('News.admin_id'=>$admin_id);
				$conditions=array_merge($conditions,$cond);
				}
				
			}
			if(isset($this->params->query['DateFrom']) && !empty($this->params->query['DateFrom'])  && isset($this->params->query['DateTo']) && !empty($this->params->query['DateTo']))
			{
                                  $date_From= $timezone->convertData($this->Session->read('LocTrain.locale'), $this->params->query['DateFrom']);
                                 $date_to =  $timezone->convertData($this->Session->read('LocTrain.locale'), $this->params->query['DateTo']);                              
                                //$lower_lmt = strtotime($date_From."- 1 day");
				//$upper_lmt = strtotime($date_to."+ 1 day");
				$lower_lmt = strtotime($date_From.'00:00:00');
				$upper_lmt = strtotime($date_to.'23:00:00');
			 	//echo '-'.date('Y-m-d', $lower_lmt).'-'.date('Y-m-d', $upper_lmt);
				$cond=array('News.date_modified between ? and ?'=>array($lower_lmt,$upper_lmt));
				//$cond=array('News.date_added between ? and ?'=>array($lower_lmt,$upper_lmt));
				//$cond=array('News.date_modified >'=>$lower_lmt,'News.date_modified <'=>$upper_lmt);
				$conditions=array_merge($conditions,$cond);
			}else if (isset($this->params->query['DateFrom']) && !empty($this->params->query['DateFrom'])  && isset($this->params->query['DateTo']) && empty($this->params->query['DateTo'])){
			  $date_From= $timezone->convertData($this->Session->read('LocTrain.locale'), $this->params->query['DateFrom']);
                                         
                               
				$lower_lmt = strtotime($date_From.'00:00:00');				
				$cond=array('News.date_modified >'=>$lower_lmt);
				$conditions=array_merge($conditions,$cond);	

			}else if (isset($this->params->query['DateFrom']) && empty($this->params->query['DateFrom'])  && isset($this->params->query['DateTo']) && !empty($this->params->query['DateTo'])){
			
        $date_to =  $timezone->convertData($this->Session->read('LocTrain.locale'), $this->params->query['DateTo']);                              
                                
				
				$upper_lmt = strtotime($date_to.'23:00:00');
			 	
				$cond=array('News.date_modified <'=>$upper_lmt);
				$conditions=array_merge($conditions,$cond);

			}
		
			if(isset($this->params->query['type']) && $this->params->query['type'] == '1'){	
				//$last_id = $this->params->query['limit'];
				$last_id = $this->pageLimit;
				$l_id=0;
				$news_list = $this->News->find('all',array('conditions'=>$conditions,'order'=>array('News.id desc'),'contain'=>false,'limit'=>$l_id.','.$this->pageLimit));
				$this->set(compact('news_list','last_id'));
				if(!empty($news_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('news_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_news_matches_the_filters').'</div>';die;
				}
				
				
			}
			else if(isset($this->params->query['type']) && $this->params->query['type'] == '2'){	
				$last_id = $this->pageLimit;
				$l_id=0;
				$news_list = $this->News->find('all',array('conditions'=>$conditions,'order'=>array('News.id desc'),'contain'=>false,'limit'=>$this->pageLimit,'offset' => $l_id));
				$this->set(compact('news_list','last_id'));
				$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('news_list');
				
			}
			else if(isset($this->params->query['type']) && $this->params->query['type'] == '3')
			{
			
			$news_list = $this->News->find('all',array('conditions'=>$conditions,'order'=>array('News.id desc'),'contain'=>false,'limit'=>$this->pageLimit,'offset' => $id));
			$this->set(compact('news_list','last_id'));
			$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('news_list');
			}
		}else{
			$last_id = $this->pageLimit;
		}
		$this->set(compact('title_for_layout','latest_news_list','news_list','last_id','news_author'));		
	}
	
	function news_landing($id=NULL)
	{		
		$this->layout =	'public';		
		$title_for_layout = 'Localization Training';
		$this->loadModel('News');
		
		$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
		
		$id = convert_uudecode(base64_decode($id));
		$news = $this->News->find('first',array('conditions'=>array('News.status'=>'1','News.id'=>$id)));
		$news_others=$this->News->find('all',array('conditions'=>array('News.status'=>'1','News.id <>'=>$id),'limit'=>3));
		
		if(empty($news)){
			$news = $this->News->find('first',array('conditions'=>array('News.status'=>'1'),'order'=>array('News.id desc')));
		}
		$this->set(compact('title_for_layout','news','news_others','locale'));
	}
	
	function catalogue_landing1()
	{		
		
		$this->layout =	'public';		
		$title_for_layout = 'Localization Training';
		$this->loadModel('Interest');
		$this->loadModel('CourseKeyword');
		$this->loadModel('Course');
		$locale_id = $this->Session->read('LocTrain.LangId');
		$interest = $this->Interest->find('all',array('fields'=>array('interest_'.$locale),'order'=>'Interest.interest_'.$locale.' asc'));	
		$arr = array();
		$max = array();
		if(!empty($interest)){
			foreach($interest as $interest){
				$check = $this->CourseKeyword->find('count',array('conditions'=>array('CourseKeyword.keyword'=>trim($interest['Interest']['interest_'.$locale]))));
				static $i = 0;
				if($check > 0){
					$arr[$i]['keyword'] = $interest['Interest']['interest_'.$locale];
					$arr[$i]['count'] = $check;
					$max[$i] = $check;
					$i++;
				}
			}	
			unset($i);
			/*function arrange_counter($a,$b)
			{
				if ($a['count'] == $b['count']) return 0;
				return ($a['count'] < $b['count']) ? 1 : 0;
			}			
			usort($arr,'arrange_counter');*/
			
		}	
		if(!empty($max)){
			$max = max($max);
		}
		$courses = $this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.primary_lang'=>$locale_id),'order'=>array('Course.id'=>'desc'),'contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.id desc'),'fields'=>array('video'))),'limit'=>2,'fields'=>array('id','title','m_id')));	
		//pr($courses);die;
$this->loadModel('purchaseItem');
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
		if($login_m_id){
                    $order_ids_mem=array();
            $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>		$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                    if(!empty($get_array)){
                                    foreach($get_array as $row){
                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                    }
                                    } 
$getCourses = $order_ids_mem;
}else{
		$getCourses = $this->Cookie->read('LocTrainPurchase');
		
}
$cookie_lessons = array();
		if(!empty($getCourses)){
			
			foreach($getCourses as $key=>$val){
				if(strlen($key)>8)
				{
					$cookie_lessons[] = convert_uudecode(base64_decode($val));
				}
				
			}
		}
		$this->set(compact('title_for_layout','arr','courses','max','cookie_lessons'));
	}
	function add_to_learning_login($id = NULL){


		$this->layout = '';
		$this->autoRender = false;
		$id = @convert_uudecode(base64_decode($id));
		$locale_id = $this->Session->read('LocTrain.LangId');
		$this->loadModel('Course');
		$course_list = $this->Course->find('first',array('conditions'=>array('Course.id'=>$id),'contain'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));	
		//print_r($course_list);die;	
		$login_m_id=$this->Session->read('LocTrain.id');
		if(!empty($course_list)){
			if(empty($course_list['CoursePurchaseList'])){
				if($this->Session->read('LocTrain.login') == '1'){
					$this->loadModel('CoursePurchaseList');
					$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
					$memberCourse['CoursePurchaseList']['c_id'] = $id;
					if($course_list['Course']['price']=='0.00'){
						$memberCourse['CoursePurchaseList']['type'] = 0;
						}else{
							$memberCourse['CoursePurchaseList']['type'] = 1;
							}
					
					$this->CoursePurchaseList->save($memberCourse);	
					$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);				
				$this->Notification->save($notification);						
					$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
					echo json_encode($resp);
				}
				else{
					$temp_login=$this->Session->read('LocTrain.visitor_id');
					$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'member_not_login';
				//$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $temp_login;
				$this->Notification->create();
				$this->Notification->save($notification);
					//$this->Session->write('LocTrain.flashMsg',__('Please login to access this section.'));
					$resp = array('status'=>'error','url'=>HTTP_ROOT."Home/register");
					echo json_encode($resp);
				}
			} 
			else{
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'already_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);
				$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
				echo json_encode($resp);
			}
		}
		else{			
			$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_not_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);	
			$resp = array('status'=>'ok','msg'=>__('course_not_added_in_learning_plan_please_try_again'));
			echo json_encode($resp);
		}
		

		}
	
	
	function add_to_learning($id = NULL)
	{
		$this->layout = '';
		$this->autoRender = false;
		$id = @convert_uudecode(base64_decode($id));
		$locale_id = $this->Session->read('LocTrain.LangId');
		$this->loadModel('Course');
		$course_list = $this->Course->find('first',array('conditions'=>array('Course.id'=>$id),'contain'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));	
		//print_r($course_list);die;	
		$login_m_id=$this->Session->read('LocTrain.id');
		if(!empty($course_list)){
			if(empty($course_list['CoursePurchaseList'])){
				if($this->Session->read('LocTrain.login') == '1'){
					$this->loadModel('CoursePurchaseList');
					$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
					$memberCourse['CoursePurchaseList']['c_id'] = $id;
					if($course_list['Course']['price']=='0.00'){
						$memberCourse['CoursePurchaseList']['type'] = 0;
						}else{
							$memberCourse['CoursePurchaseList']['type'] = 1;
							}
					
					$this->CoursePurchaseList->save($memberCourse);	
					$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);				
				$this->Notification->save($notification);						
					$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
					echo json_encode($resp);
				}
				else{
					$temp_login=$this->Session->read('LocTrain.visitor_id');
					$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'member_not_login';
				//$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $temp_login;
				$this->Notification->create();
				$this->Notification->save($notification);
					//$this->Session->write('LocTrain.flashMsg',__('Please login to access this section.'));
					$resp = array('status'=>'error','url'=>HTTP_ROOT."Home/register");
					echo json_encode($resp);
				}
			} 
			else{
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'already_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);
				$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
				echo json_encode($resp);
			}
		}
		else{			
			$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_not_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);	
			$resp = array('status'=>'ok','msg'=>__('course_not_added_in_learning_plan_please_try_again'));
			echo json_encode($resp);
		}
		die;		
	}
	
	function add_to_my_learning($id = NULL)
	{
		$this->layout = '';
		$this->autoRender = false;
		$id = @convert_uudecode(base64_decode($id));
		$locale_id = $this->Session->read('LocTrain.LangId');
		$this->loadModel('Course');
		$course_list = $this->Course->find('first',array('conditions'=>array('Course.id'=>$id),'contain'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));	
		//pr($course_list);die;	
		$login_m_id=$this->Session->read('LocTrain.id');
		if(!empty($course_list)){
			if(empty($course_list['CoursePurchaseList'])){
				if($this->Session->read('LocTrain.login') == '1'){
					$this->loadModel('CoursePurchaseList');
					$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
					$memberCourse['CoursePurchaseList']['c_id'] = $id;
					$memberCourse['CoursePurchaseList']['type'] = 0;
					$memberCourse['CoursePurchaseList']['date_created'] = strtotime(date('Y-m-d H:i:s'));
					$this->CoursePurchaseList->save($memberCourse);	
					$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);				
				$this->Notification->save($notification);				
					//$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
				//	echo json_encode($resp);
				$this->redirect($this->referer());
				}
				else{
				$temp_login=$this->Session->read('LocTrain.visitor_id');
				
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'member_not_login';
				//$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $temp_login;
				$this->Notification->create();
				$this->Notification->save($notification);
					//$this->Session->write('LocTrain.flashMsg',__('Please login to access this section.'));
					/*$resp = array('status'=>'error','url'=>HTTP_ROOT."Home/register");
					echo json_encode($resp);*/
					$this->redirect(array('controller'=>'Home','action'=>'register'));
				}
			} 
			else{
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'already_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);
				/*$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
				echo json_encode($resp);*/
				$this->redirect($this->referer());
			}
		}
		else
		{		
			$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_not_added_learning_plan';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);	
		/*	$resp = array('status'=>'ok','msg'=>__('course_not_added_in_learning_plan_please_try_again'));
			echo json_encode($resp);*/
			$this->redirect($this->referer());
		}
		die;		
	}
	/*
	This function is used to add courses to my learning in case if user applies 100% discount
	*/
	function add_to_my_learning1($id = NULL)
	{
		$id = @convert_uudecode(base64_decode($id));
		$this->loadModel('Course');
		$this->loadModel('CourseLesson');
		$this->loadModel('Member');
		$course_list = $this->Course->find('first',array('conditions'=>array('Course.id'=>$id),'contain'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));
		
		$login_m_id=$this->Session->read('LocTrain.id');
		if(!empty($course_list)){
			//if(empty($course_list['CoursePurchaseList'])){
				if($this->Session->read('LocTrain.login') == '1'){
					$this->loadModel('CoursePurchaseList');
					$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
					$memberCourse['CoursePurchaseList']['c_id'] = $id;
					$memberCourse['CoursePurchaseList']['type'] = 0;
					$memberCourse['CoursePurchaseList']['date_created'] = strtotime(date('Y-m-d H:i:s'));
					$this->CoursePurchaseList->save($memberCourse);	
					$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_purchased';
				$notification['Notification']['c_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $course_list['Course']['m_id'];
				$this->Notification->create();
				$this->Notification->save($notification);				
				$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
					echo json_encode($resp);
				
				}				
			 
		}
		else
		{
		
		$courselesson_list = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$id),'contain'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));
		
			if(!empty($courselesson_list)){
			//if(empty($course_list['CoursePurchaseList'])){
				if($this->Session->read('LocTrain.login') == '1'){
					$this->loadModel('CoursePurchaseList');
					$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
					$memberCourse['CoursePurchaseList']['c_id']=$courselesson_list['CourseLesson']['course_id'];
					$memberCourse['CoursePurchaseList']['lesson_id']=$id;
					$memberCourse['CoursePurchaseList']['type'] = 0;
					$memberCourse['CoursePurchaseList']['date_created'] = strtotime(date('Y-m-d H:i:s'));
					$this->CoursePurchaseList->save($memberCourse);

					$course_owner = $this->Course->find('first',array('conditions'=>array('Course.id'=>$courselesson_list['CourseLesson']['course_id']),'fields'=>array('id','m_id'),'contain'=>false));
					$course_owner_id = $course_owner['Course']['m_id'];											
					$course_owner_name = $this->Member->find('first',array('conditions'=>array('Member.id'=>$course_owner_id),'fields'=>array('given_name','id'),'contain'=>false));
					
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'lesson_purchased';
				$notification['Notification']['c_id'] = $courselesson_list['CourseLesson']['course_id'];
				$notification['Notification']['lesson_id'] = $id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $course_owner_name['Member']['id'];
				$this->Notification->create();
				$this->Notification->save($notification);				
				$resp = array('status'=>'ok','msg'=>__('course_already_added_to_your_learning_plan'));
					echo json_encode($resp);
				
				}				
			 
		}			
		
		}
		die;		
	}
	
	
	function remove_from_learning()
	{
		
		$this->autoRender = false;
		$id = @convert_uudecode(base64_decode($_GET['id']));
		$this->loadModel('CoursePurchaseList');
		$record = $this->CoursePurchaseList->find('first',array('conditions'=>array('CoursePurchaseList.id'=>$id)));	
		if(!empty($record)){
			if($this->CoursePurchaseList->delete($id))
			{
				echo "deleted"; die;
			}
			else
			{
				echo "Error deleting. Please try again later."; die;
			}
		}
		else{			
			echo "Error deleting. Please try again later."; die;
		}
		 die;		
	}

		//-----------  Add to cart -----------------------	
    function add_to_cart($id = NULL,$tab=NULl)
    {
        $i = 0;$deleted=0;//defining variables
        $to_delete=array();
        $cookie_deleted_lessons=array();
        $this->loadModel('Course'); //loading models
        $this->loadModel('CourseLesson');
        $this->loadModel('purchaseItem');
        $login_m_id=$this->Session->read('LocTrain.id'); //getting session member id
        $locale_id = $this->Session->read('LocTrain.LangId');//getting session language id
        $c_id=convert_uudecode(base64_decode($id)); //decrypting the course id
        
        //getting course info by using the $c_id
        $course = $this->Course->find('count',array('conditions'=>array('Course.id'=>convert_uudecode(base64_decode($id)))));
        //getting the all lessons under the $c_id in courseLesson table
        $lessons_of_course=$this->CourseLesson->find('list',array('conditions'=>array('CourseLesson.course_id'=>convert_uudecode(base64_decode($id))),'fields'=>array('CourseLesson.id','CourseLesson.title')));
        if($course > 0)
            {///if course is exsists then continue forther
                if($this->Cookie->read('LocTrainPurchase'))
                    {//getting the cookies for courses/lessons if user adds in case of logout case
                        $order_ids = $this->Cookie->read('LocTrainPurchase');
                        $order_ids_mac = $this->Cookie->read('LocTrainPurchase');
                        foreach($order_ids as $keys=>$values)
                        {
                            if(strlen($keys)>8)
                            {
                                $cookie_lesson = convert_uudecode(base64_decode($values));
                                foreach($lessons_of_course as $lesson_id => $lesson_title)
                                {
                                    if($lesson_id ==$cookie_lesson)
                                    {
                                        $to_delete[]=$keys;
                                        $cookie_deleted_lessons[]=$lesson_title;
										$this->purchaseItem->deleteAll(array('item_value' => base64_encode(convert_uuencode($lesson_id)),'m_id'=>$login_m_id));
                                        $deleted=1;
                                    }
                                }
                            }

                        }
                        end($order_ids);
                        $last_order_id_key = key($order_ids);				
                        $explode_ast_order_id_key = explode('m',$last_order_id_key);
                        $num_windows = $explode_ast_order_id_key[1]+1;
                        reset($order_ids_mac);
                        $last_order_id_key = key($order_ids_mac);				
                        $explode_ast_order_id_key = explode('m',$last_order_id_key);
                        $num_mac = $explode_ast_order_id_key[1]+1;
                        if($num_windows > $num_mac)
                        {
                                $i=$num_windows;
                        }
                        else
                        {
                                $i=$num_mac;
                        }

                    }	
                if($login_m_id)
                    {
                        $order_ids_mem=array();
                        $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>$login_m_id),'order'=>array('purchaseItem.id'=>'asc'),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                            if(!empty($get_array)){
                                foreach($get_array as $row){
                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                }
                            }               
                        $order_ids_mac_mem=$order_ids_mem;
                            foreach($order_ids_mem as $keys=>$values)
                            {  
                                if(strlen($keys)>8)
                                {
                                    $cookie_lesson = convert_uudecode(base64_decode($values));
                                    foreach($lessons_of_course as $lesson_id => $lesson_title)
                                    {
                                        if($lesson_id ==$cookie_lesson)
                                        {
                                            $to_delete[]=$keys;
                                            $cookie_deleted_lessons[]=$lesson_title;											
                                            $this->purchaseItem->deleteAll(array('item_value' => base64_encode(convert_uuencode($lesson_id)),'m_id'=>$login_m_id));
                                            $deleted=1;
                                        }
                                    }
                                }

                            }			
                        end($order_ids_mem); 	
                        $last_order_id_key_mem = key($order_ids_mem);				
                        $explode_ast_order_id_key_mem = explode('m',$last_order_id_key_mem);
                        $num_windows_mem = $explode_ast_order_id_key_mem[1]+1;
                        reset($order_ids_mac_mem);
                        $last_order_id_key = key($order_ids_mac_mem);				
                        $explode_ast_order_id_key = explode('m',$last_order_id_key);
                        $num_mac = $explode_ast_order_id_key[1]+1;					
                        if($num_windows_mem > $num_mac)
                        {
                                $j=$num_windows_mem;
                        }
                        else
                        {
                                 $j=$num_mac;
                        }
                    }
                if($deleted)
                    {
                        $append ='';                        
                        foreach($cookie_deleted_lessons as $lesson)
                        {
                        
                        }
                    }
                else
                    {
                        $append ='';
                    }
                if(isset($login_m_id) && !empty($login_m_id))
                    {
                        $item_data=$this->purchaseItem->create();
                        $item_data['purchaseItem']['m_id']=$login_m_id;
                        $item_data['purchaseItem']['item_name']='item'.$j;
                        $item_data['purchaseItem']['item_value']=$id;
                        $this->purchaseItem->saveAll($item_data);	
                    }
                    else
                        {
                            $this->Cookie->write('LocTrainPurchase.item'.$i,$id,false,'2 Days');
                        }
                $order_ids=$this->Cookie->read('LocTrainPurchase');					
                foreach($to_delete as $cookie_to_del)
                {                   
                    $this->Cookie->delete('LocTrainPurchase.'.$cookie_to_del);
                }
                $order_ids=$this->Cookie->read('LocTrainPurchase');
                if($this->Session->read('LocTrain.login') != '1')
                    {				
                        $temp_id = $this->Session->read('LocTrain.visitor_id');
                        $this->before_update_notification();
                        $this->loadModel('Notification');
                        $notification['Notification']['mem_id'] =  0;
                        $notification['Notification']['notification_type'] = 'member_not_login';
                        $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['notification_sender_id'] = $temp_id;
                        $notification['Notification']['read'] = 1;					
                        $this->redirect(array('controller'=>'Home','action'=>'register'));
                    }
                    else
                        {
                            $this->before_update_notification();
                            $login_m_id = $this->Session->read('LocTrain.id');
                            $this->loadModel('Notification');
                            $notification['Notification']['mem_id'] =  0;
                            $notification['Notification']['notification_type'] = 'add_course_shopping_basket';
                            $notification['Notification']['c_id'] = $c_id;
                            $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                            $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                            $notification['Notification']['notification_sender_id'] = $login_m_id;
                            $this->Notification->create();
                            $this->Notification->save($notification);        
                            if($tab)
                                {                                            
                                    $this->redirect(array('controller'=>'Members','action'=>'my_learning','2','tab3'));
                                }else
                                    {
                                        $this->redirect($this->referer());
                                    }
                        }
            }
            else
                {// course is not exsists then inform to user with error message
                    $login_m_id = $this->Session->read('LocTrain.id')? $this->Session->read('LocTrain.id'):$this->Session->read('LocTrain.visitor_id');
                    $this->before_update_notification();
                    $this->loadModel('Notification');
                    $notification['Notification']['mem_id'] =  0;
                    $notification['Notification']['notification_type'] = 'course_not_added_shopping_basket';
                    $notification['Notification']['c_id'] = $c_id;
                    $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                    $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                    $notification['Notification']['notification_sender_id'] = $login_m_id;
                    $this->Notification->create();
                    $this->Notification->save($notification);                        
                    if($tab)
                        {                                            
                            $this->redirect(array('controller'=>'Members','action'=>'my_learning','2','tab3'));
                        }
                        else
                            {
                                $this->redirect($this->referer());
                            }
                }
		
	}
	//-----------  Add lesson to cart -----------------------	
    function add_lesson_to_cart($id = NULL)
        {
            $i = 0;$j=0;$k=0;
            $this->loadModel('CourseLesson');
            $this->loadModel('purchaseItem');
            $login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
            if($this->Session->read('LocTrain.login') == '1')
                {//If user is logged in
                    $course_lesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>convert_uudecode(base64_decode($id)))));
                    $l_id=convert_uudecode(base64_decode($id));
                    $course_id=base64_encode(convert_uuencode($course_lesson['CourseLesson']['course_id']));
                    $array_course=array();
                    if(!empty($course_lesson))
                        {
                            if($login_m_id)                            
                                {
                                    $order_ids_mem=array();
                                    $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>		$login_m_id),'order'=>array('purchaseItem.id'=>'asc'),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                    if(!empty($get_array))
                                        {
                                            foreach($get_array as $row)
                                                {
                                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                                }
                                        } 
                                    end($order_ids_mem);
                                    $last_order_id_key = key($order_ids_mem);				
                                    $explode_ast_order_id_key = explode('m',$last_order_id_key);
                                    $j = $explode_ast_order_id_key[1]+1;
                                    foreach($order_ids_mem as $key=>$v)
                                        {
                                            if(strlen($key)>8)
                                            {
                                                $array_course[$k]= $v;
                                            }
                                        }
                                }else
                                    {
                                        if($this->Cookie->read('LocTrainPurchase'))
                                            {
                                                $order_ids = $this->Cookie->read('LocTrainPurchase');
                                                end($order_ids);
                                                $last_order_id_key = key($order_ids);				
                                                $explode_ast_order_id_key = explode('m',$last_order_id_key);
                                                $i = $explode_ast_order_id_key[1]+1;
                                                foreach($order_ids as $key=>$v)
                                                    {
                                                        if(strlen($key)>8)
                                                            {
                                                                $array_course[$k]= $v;
                                                            }
                                                    }
                                            }
                                    }
                            if(!(in_array($course_id, $array_course)))
                            {
                                    if(isset($login_m_id) && !empty($login_m_id)){
                                        $item_data=$this->purchaseItem->create();
                                        $item_data['purchaseItem']['m_id']=$login_m_id;
                                        $item_data['purchaseItem']['item_name']='less_item'.$j;
                                        $item_data['purchaseItem']['item_value']=$id;
                                        $this->purchaseItem->saveAll($item_data);	
                                    }else{
                                    $this->Cookie->write('LocTrainPurchase.less_item'.$i,$id,false,'2 Days');}	
                                    $login_m_id = $this->Session->read('LocTrain.id');
                                    $this->before_update_notification();
                                    $this->loadModel('Notification');
                                    $notification['Notification']['mem_id'] =  0;
                                    $notification['Notification']['notification_type'] = 'lesson_added_shopping_basket';
                                    $notification['Notification']['lesson_id'] = $l_id;
                                    $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                                    $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                                    $notification['Notification']['notification_sender_id'] = $login_m_id;
                                    $this->Notification->create();
                                    $this->Notification->save($notification);                           
                                    $this->redirect($this->referer());
                            }
                            else{
                                    $this->before_update_notification();
                                    $login_m_id = $this->Session->read('LocTrain.id');
                                    $this->loadModel('Notification');
                                    $notification['Notification']['mem_id'] =  0;
                                    $notification['Notification']['notification_type'] = 'lesson_present_shopping_basket';
                                    $notification['Notification']['lesson_id'] = $l_id;
                                    $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                                    $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                                    $notification['Notification']['notification_sender_id'] = $login_m_id;
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                    $this->redirect($this->referer());
                            }
                        }
                        else{
                                        $this->before_update_notification();
                                        $login_m_id = $this->Session->read('LocTrain.id');
                                        $this->loadModel('Notification');
                                        $notification['Notification']['mem_id'] =  0;
                                        $notification['Notification']['notification_type'] = 'lesson_not_added_shopping_basket';
                                        $notification['Notification']['lesson_id'] = $l_id;
                                        $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                                        $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                                        $notification['Notification']['notification_sender_id'] = $login_m_id;
                                        $this->Notification->create();
                                        $this->Notification->save($notification);
                                        $this->redirect($this->referer());	
                        }
                }
                else{

                        $this->before_update_notification();
                        $temp_id = $this->Session->read('LocTrain.visitor_id');
                        $this->loadModel('Notification');
                        $notification['Notification']['mem_id'] =  0;
                        $notification['Notification']['notification_type'] = 'member_not_login';
                        $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['notification_sender_id'] = $temp_id;
                        $notification['Notification']['read'] = 1;                                   
                        $this->redirect(array('controller'=>'Home','action'=>'register'));
                }
	}
        
    //-----------  Add lesson to cart ajax -----------------------	    
    function add_lesson_to_cart_ajax()
	{  
            $i = 0;$j=0;		
            $lesson_list=$this->request->data['array'];    
            $this->loadModel('purchaseItem');
            $login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
            if(!empty($lesson_list))
                {
                    foreach($lesson_list as $row)
                        {
                            $id=$row;
                            $k=0;
                            $this->loadModel('CourseLesson');		
                            $course_lesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>convert_uudecode(base64_decode($id)))));
                            $l_id=convert_uudecode(base64_decode($id));
                            $course_id=base64_encode(convert_uuencode($course_lesson['CourseLesson']['course_id']));
                            $array_course=array();
                            if(!empty($course_lesson))
                                {
                                    if($login_m_id)
                                        {
                                            $order_ids_mem=array();
                                            $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>$login_m_id),'order'=>array('purchaseItem.id'=>'asc'),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                            if(!empty($get_array))
                                                {
                                                    foreach($get_array as $row){
                                                        $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                                    }
                                                } 
                                                end($order_ids_mem);
                                                $last_order_id_key = key($order_ids_mem);						
                                                $explode_ast_order_id_key = explode('m',$last_order_id_key);					
                                                $j = $explode_ast_order_id_key[1]+1;
                                                foreach($order_ids_mem as $key=>$v)
                                                {
                                                    if(strlen($key)>8)
                                                    {
                                                        $array_course[$k]= $v;
                                                    }
                                                }
                                        }else
                                            {
                                                if($this->Cookie->read('LocTrainPurchase')){
                                                    $order_ids = $this->Cookie->read('LocTrainPurchase');					
                                                    end($order_ids);
                                                    $last_order_id_key = key($order_ids);				
                                                    $explode_ast_order_id_key = explode('m',$last_order_id_key);
                                                    $i = $explode_ast_order_id_key[1]+1;
                                                    foreach($order_ids as $key=>$v)
                                                    {
                                                                    if(strlen($key)>8)
                                                                    {
                                                                            $array_course[$k]= $v;
                                                                    }
                                                    }
                                                }
                                            } 
				
                                    if(!(in_array($course_id, $array_course)))
                                    {
                                        if(isset($login_m_id) && !empty($login_m_id)){
                                            $item_data=$this->purchaseItem->create();
                                            $item_data['purchaseItem']['m_id']=$login_m_id;
                                            $item_data['purchaseItem']['item_name']='less_item'.$j;
                                            $item_data['purchaseItem']['item_value']=$id;
                                            $this->purchaseItem->saveAll($item_data);
                                            $login_m_id = $this->Session->read('LocTrain.id');
                                            $this->before_update_notification();
                                            $this->loadModel('Notification');
                                            $notification['Notification']['mem_id'] =  0;
                                            $notification['Notification']['notification_type'] = 'lesson_added_shopping_basket';
                                            $notification['Notification']['lesson_id'] = $l_id;
                                            $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                                            $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                                            $notification['Notification']['notification_sender_id'] = $login_m_id;
                                            $this->Notification->create();
                                            $this->Notification->saveAll($notification);
                                        }else{				
                                            $this->Cookie->write('LocTrainPurchase.less_item'.$i,$id,false,'2 Days');					
                                            $this->before_update_notification();
                                            $temp_id = $this->Session->read('LocTrain.visitor_id');
                                            $this->loadModel('Notification');
                                            $notification['Notification']['mem_id'] =  0;
                                            $notification['Notification']['notification_type'] = 'member_not_login';
                                            $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                                            $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                                            $notification['Notification']['notification_sender_id'] = $temp_id;
                                            $notification['Notification']['read'] = 1;					
                                        }
                                    }
                                    else{
                                            $this->before_update_notification();
                                            $login_m_id = $this->Session->read('LocTrain.id');
                                            $this->loadModel('Notification');
                                            $notification['Notification']['mem_id'] =  0;
                                            $notification['Notification']['notification_type'] = 'lesson_present_shopping_basket';
                                            $notification['Notification']['lesson_id'] = $l_id;
                                            $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                                            $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                                            $notification['Notification']['notification_sender_id'] = $login_m_id;
                                            $this->Notification->create();
                                            $this->Notification->saveAll($notification);
                                    }
                                }
                                else{
					$this->before_update_notification();
					$login_m_id = $this->Session->read('LocTrain.id');
					$this->loadModel('Notification');
					$notification['Notification']['mem_id'] =  0;
					$notification['Notification']['notification_type'] = 'lesson_not_added_shopping_basket';
					$notification['Notification']['lesson_id'] = $l_id;
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['notification_sender_id'] = $login_m_id;
					$this->Notification->create();
					$this->Notification->saveAll($notification);					
                                    }
                        
		
                            $j++;$i++;
                        }
			
			 $this->redirect(array('action'=>'purchase'));exit;
                   // $this->redirect($this->referer());                  
                }else{	
                        $this->before_update_notification();
                        $temp_id = $this->Session->read('LocTrain.visitor_id');
                        $this->loadModel('Notification');
                        $notification['Notification']['mem_id'] =  0;
                        $notification['Notification']['notification_type'] = 'member_not_login';
                        $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['notification_sender_id'] = $temp_id;
                        $notification['Notification']['read'] = 1;					 
                        echo 'index';			
                    }
            die();
        }
	
	//-----------  Purchase -----------------------	
    function purchase()
    {
        $this->layout =	'public';	
        $this->loadModel('Course');
        $locale_id = $this->Session->read('LocTrain.LangId');
        $this->loadModel('CourseLesson');
        $this->loadModel('purchaseItem');
        $this->loadModel('CoursePurchaseList');
        $login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
        $course_list = array();
        $encode_ids_new  = $this->Cookie->read('LocTrainPurchase');		
        if(!empty($login_m_id)){
			$c_array=array();
			$l_array=array();
			$item_array=$this->purchaseItem->find('all',array('conditions'=>array('m_id'=>$login_m_id)));
			if(!empty($item_array)){
                            foreach($item_array as $row){
                                if(strlen($row['purchaseItem']['item_name'])<8){
                                    $c_array[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_name'];
                                }else{
                                    $l_array[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_name'];
                                }			
                            }				
			}
			
			ksort($c_array);end($c_array);
			ksort($l_array);end($l_array);
			if(!empty($c_array)){
                            $last_order_id_key_mem = key($c_array);				
                            $explode_ast_order_id_key_mem = explode('m',$last_order_id_key_mem);
                            $num_c= $explode_ast_order_id_key_mem[1]+1;
			}else{
                            $num_c=0;	
			}
			if(!empty($l_array)){
                            $last_order_id_key = key($l_array);				
                            $explode_ast_order_id_key = explode('m',$last_order_id_key);
                            $num_l = $explode_ast_order_id_key[1]+1;
			}else{
                            $num_l=0;	
			}			
			if(!empty($encode_ids_new)){//print_r($encode_ids_new);exit;
                            foreach($encode_ids_new as $key=>$val){
                                    $keydata=explode("m",$key);
                                    if($keydata[0]=='ite')	
                                    {
                                        $less_courid = @convert_uudecode(base64_decode($val));
                                        $checkpurchaseditem=$this->CoursePurchaseList->find('count',array('conditions'=>array('mem_id'=>$login_m_id,'CoursePurchaseList.type !='=>1,'c_id'=> $less_courid)));
                                       
                                    }
                                    else
                                    {
                                        $less_courid = @convert_uudecode(base64_decode($val)); 
                                        $c_id=$this->CourseLesson->query("Select `course_id` from `course_lessons` where `id`=".$less_courid);
                                        
                                        $checkpurchaseditem=$this->CoursePurchaseList->find('count',array('conditions'=>array('mem_id'=>$login_m_id,'CoursePurchaseList.type !='=>1,'c_id'=>$c_id[0]['course_lessons']['course_id'],'lesson_id'=>NULL)));
                                       
                                        if($checkpurchaseditem==0)
                                        {
                                            $checkpurchaseditem=$this->CoursePurchaseList->find('count',array('conditions'=>array('mem_id'=>$login_m_id,'CoursePurchaseList.type !='=>1,'c_id'=>$c_id[0]['course_lessons']['course_id'],'lesson_id'=>$less_courid)));	
                                        }
                                    }
                                    $checkItem=$this->purchaseItem->find('count',array('conditions'=>array('m_id'=>$login_m_id,'purchaseItem.item_value'=>$val)));
                                   
                                    if($checkItem>0 || $checkpurchaseditem>0){
					$this->before_update_notification();
					$login_m_id = $this->Session->read('LocTrain.id');
					$this->loadModel('Notification');
					$notification['Notification']['mem_id'] =  0;
					$notification['Notification']['notification_type'] = 'lesson_present_shopping_basket';
					if($keydata[0]=='ite')	
                                            {
                                                $notification['Notification']['c_id'] =  $less_courid; 
                                            }
                                            else
                                            {
                                                    $notification['Notification']['lesson_id'] =  $less_courid; 
                                            }
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['notification_sender_id'] = $login_m_id;
					$this->Notification->create();
					$this->Notification->saveAll($notification);				
                                    }else{
										
					$item_data=$this->purchaseItem->create();
					$item_data['purchaseItem']['m_id']=$login_m_id;
					if(strlen($key)<8){
                                            $item_data['purchaseItem']['item_name']='item'.$num_c;
                                            $num_c++;
					}else{
                                            $item_data['purchaseItem']['item_name']='less_item'.$num_l;
                                            $num_l++;
					}
					$item_data['purchaseItem']['item_value']=$val;
					$this->purchaseItem->saveAll($item_data);
						 $order_ids_mem=array();
                        $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>$login_m_id),'order'=>array('purchaseItem.id'=>'asc'),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                            if(!empty($get_array)){
                                foreach($get_array as $row){
                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                }
                            } 
                           
						 if($keydata[0]=='ite')
                                {
									//checking the lessons of corses added or not
										$lessons_of_course=$this->CourseLesson->find('list',array('conditions'=>array('CourseLesson.course_id'=>convert_uudecode(base64_decode($val))),'fields'=>array('CourseLesson.id','CourseLesson.title')));
										  foreach($order_ids_mem as $keys=>$values)
											{
												if(strlen($keys)>8)
												{
										  $cookie_lesson = convert_uudecode(base64_decode($values));
											foreach($lessons_of_course as $lesson_id => $lesson_title)
											{
												if($lesson_id ==$cookie_lesson)
												{											
													$this->purchaseItem->deleteAll(array('item_value' => base64_encode(convert_uuencode($lesson_id)),'m_id'=>$login_m_id));
													
												}
											}
										}
										}
							}else{
								$course_of_lesson=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>convert_uudecode(base64_decode($val))),'fields'=>array('CourseLesson.course_id','CourseLesson.title')));
								//print_r($course_of_lesson);exit;
										  foreach($order_ids_mem as $keys=>$values)
											{
												if(strlen($keys)<8)
												{  $cookie_lesson = convert_uudecode(base64_decode($values));
												  if($course_of_lesson['CourseLesson']['course_id'] ==$cookie_lesson)
														{											
															$this->purchaseItem->deleteAll(array('item_value' => $val,'m_id'=>$login_m_id));
															
														}
										}
										}
								}
						
                                    }
                                    $this->Cookie->delete('LocTrainPurchase.'.$key);				
				}
                            }
                    }
        $get_array=$this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
       
		$encode_ids=array();
        if(!empty($get_array)){
            foreach($get_array as $row){
            $encode_ids[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
            }
        }
        $locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
        $language_folder = $this->Session->read('Config.language') ? $this->Session->read('Config.language') : 'en_US';
        $this->set('language_folder',$language_folder);
        $this->set('locale',$locale);
        if(!empty($encode_ids)){
            foreach($encode_ids as $key=>$ids){
                $decode_id = @convert_uudecode(base64_decode($ids));
                if(strlen($key)>8)
                { 
                    $course=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'contain'=>array('Course'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id','lesson_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id')))))));                         
                }else{
                    $course = $this->Course->find('first',array('conditions'=>array('Course.id'=>$decode_id),'contain'=>array('CourseLesson','CoursePurchaseList'=>array('fields'=>array('mem_id','lesson_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));
                }
                if(!empty($course)){
                    if($course['Course']['m_id'] != $this->Session->read('LocTrain.id')){
                        $course['Course']['pro_id'] = base64_encode(convert_uuencode($key));
                        $course_list[$key] = $course;	
                    }else{
                        $this->purchaseItem->delete(array('item_value' => $ids,'m_id'=>$login_m_id));
                        $this->Cookie->delete('LocTrainPurchase.'.$key);
                    }
                }else{
                    $this->purchaseItem->delete(array('item_value' => $ids,'m_id'=>$login_m_id));
                    $this->Cookie->delete('LocTrainPurchase.'.$key);
                }
            }
        }
        $this->Cookie->delete('LocTrainPurchase');
        $cookPurchase1=count($course_list);
        $this->set(compact('course_list','id'));
    }
	
	function remove_product($cookie_id= NULL,$c_id = NULL)
	{	
		$course_id = @convert_uudecode(base64_decode($c_id));
		$cookie_id = @convert_uudecode(base64_decode($cookie_id));
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;		
		$this->loadModel('purchaseItem');		
		$checkItem=$this->purchaseItem->find('count',array('conditions'=>array('purchaseItem.item_value'=>$c_id,'purchaseItem.m_id'=>$login_m_id)));
		if($checkItem > 0){
			$this->purchaseItem->deleteAll(array('item_value' =>$c_id,'m_id'=>$login_m_id));
			$login_m_id = $this->Session->read('LocTrain.id');
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'course_removed_shopping_basket';
			
			if(strlen($cookie_id)>8)
			{ 
				$notification['Notification']['lesson_id'] = $course_id;
			} else {
				$notification['Notification']['c_id'] = $course_id;
			}
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			$this->Notification->create();
			$this->Notification->save($notification);
		}else if($this->Cookie->read('LocTrainPurchase.'.$cookie_id) == $c_id) {
			//$arr=$this->Cookie->read('LocTrainPurchase');
			$this->Cookie->delete('LocTrainPurchase.'.$cookie_id);
			
			$login_m_id = $this->Session->read('LocTrain.id');
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'course_removed_shopping_basket';
			
			if(strlen($cookie_id)>8)
			{ 
				$notification['Notification']['lesson_id'] = $course_id;
			} else {
				$notification['Notification']['c_id'] = $course_id;
			}
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			$this->Notification->create();
			$this->Notification->save($notification);
			
		//	$this->Session->write('LocTrain.flashMsg',__('Course removed from basket.'));				
		}
		else{
			$this->before_update_notification();
			$login_m_id = $this->Session->read('LocTrain.id');
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'course_not_removed_shopping_basket';
			$notification['Notification']['c_id'] = $course_id;
			$notification['Notification']['lesson_id'] = $course_id;
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			$this->Notification->create();
			$this->Notification->save($notification);
			//$this->Session->write('LocTrain.flashMsg',__('Course not removed from cart. Please try again'));
		}
		$this->redirect($this->referer());
	}
	function remove_product_ajax()
	{
            $remove_list=$this->request->data['array'];
            if(!empty($remove_list)){
              foreach($remove_list as $row){
                  $str=explode('-',$row);
                     $cookie_id=$str[0];
                     $c_id=$str[1];
                   $course_id = @convert_uudecode(base64_decode($c_id));
                  $cookie_id = @convert_uudecode(base64_decode($cookie_id));
$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
		//echo $cookie_id;
               	$this->loadModel('purchaseItem');		
		$checkItem=$this->purchaseItem->find('count',array('conditions'=>array('purchaseItem.item_value'=>$c_id,'purchaseItem.m_id'=>$login_m_id)));
		//print_r($checkItem);
		if($checkItem > 0){
			$this->purchaseItem->deleteAll(array('item_value' =>$c_id,'m_id'=>$login_m_id));
			$login_m_id = $this->Session->read('LocTrain.id');
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'course_removed_shopping_basket';
			
			if(strlen($cookie_id)>8)
			{ 
				$notification['Notification']['lesson_id'] = $course_id;
			} else {
				$notification['Notification']['c_id'] = $course_id;
			}
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			$this->Notification->create();
			$this->Notification->saveAll($notification);
		}else if($this->Cookie->read('LocTrainPurchase.'.$cookie_id) == $c_id) {
                      $this->Cookie->delete('LocTrainPurchase.'.$cookie_id);
			$login_m_id = $this->Session->read('LocTrain.id');
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'course_removed_shopping_basket';
			
			if(strlen($cookie_id)>8)
			{ 
				$notification['Notification']['lesson_id'] = $course_id;
			} else {
				$notification['Notification']['c_id'] = $course_id;
			}
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			$this->Notification->create();
			$this->Notification->saveAll($notification);
                  }else{
                    $this->before_update_notification();
                    $login_m_id = $this->Session->read('LocTrain.id');
                    $this->loadModel('Notification');
                    $notification['Notification']['mem_id'] =  0;
                    $notification['Notification']['notification_type'] = 'course_not_removed_shopping_basket';
                    $notification['Notification']['c_id'] = $course_id;
                    $notification['Notification']['lesson_id'] = $course_id;
                    $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                    $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                    $notification['Notification']['notification_sender_id'] = $login_m_id;
                    $this->Notification->create();
                    $this->Notification->saveAll($notification); 
                  }
                 // pr($this->Cookie->read('LocTrainPurchase'));
              }  
            }
           echo $this->referer();die;
            //$this->redirect($this->referer());
	}
	function add_learning_cart($cookie_id = NULL,$c_id= NULL)
	{
		if($this->Session->read('LocTrain.login') == '1'){
			$course_id = @convert_uudecode(base64_decode($c_id));
			$cookie_id = @convert_uudecode(base64_decode($cookie_id));
			$this->loadModel('Course');
			 $this->loadModel('purchaseItem');
                        $login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
			$locale_id = $this->Session->read('LocTrain.LangId');
			$course_list = $this->Course->find('count',array('conditions'=>array('Course.id'=>$course_id)));	
			//pr($course_list);die;
			//pr($course_list);die;
                        $checkItem=$this->purchaseItem->find('first',array('conditions'=>array('purchaseItem.item_name'=>$cookie_id,'m_id'=>$login_m_id)));
                        if($course_list > 0 && isset($checkItem['purchaseItem']) && $checkItem['purchaseItem']['item_value']  == $c_id){
                            $this->loadModel('CoursePurchaseList');
				$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
				$memberCourse['CoursePurchaseList']['c_id'] = $course_id;
				$memberCourse['CoursePurchaseList']['type'] = 1;
				$this->CoursePurchaseList->save($memberCourse);				
				$this->Cookie->delete('LocTrainPurchase.'.$cookie_id);
                                $this->purchaseItem->delete(array('item_name' => $cookie_id,'m_id'=>$login_m_id));
				$this->before_update_notification();
				$login_m_id = $this->Session->read('LocTrain.id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_added_learning_plan';
				$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);
				
				//$this->Session->write('LocTrain.flashMsg',__('This course has been added to your learning plan'));
				$this->redirect($this->referer());
                        }else if($course_list > 0 && $this->Cookie->check('LocTrainPurchase.'.$cookie_id) == $c_id){
				$this->loadModel('CoursePurchaseList');
				$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
				$memberCourse['CoursePurchaseList']['c_id'] = $course_id;
				$memberCourse['CoursePurchaseList']['type'] = 1;
				$this->CoursePurchaseList->save($memberCourse);				
				$this->Cookie->delete('LocTrainPurchase.'.$cookie_id);	
				$this->before_update_notification();
				$login_m_id = $this->Session->read('LocTrain.id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_added_learning_plan';
				$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);
				
				//$this->Session->write('LocTrain.flashMsg',__('This course has been added to your learning plan'));
				$this->redirect($this->referer());						
			}	
			else{	
				$this->before_update_notification();
				$login_m_id = $this->Session->read('LocTrain.id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_not_added_learning_plan';
				$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);
			//	$this->Session->write('LocTrain.flashMsg',__('Course not added in learning plan. Please try again.'));		
				$this->redirect($this->referer());	
			}		
		}
		else{
				$ths->before_update_notification();
				$temp_id = $this->Session->read('LocTrain.visitor_id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'member_not_login';
				//$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $temp_id;
				$notification['Notification']['read'] = 1;
				$this->Notification->create();
				$this->Notification->save($notification);
				
				//$this->Session->write('LocTrain.flashMsg',__('Please login to access this section.'));
				$this->redirect(array('controller'=>'Home','action'=>'index'));				
		}		
	}
function add_learning_cart_ajax()
	{
            if($this->Session->read('LocTrain.login') == '1'){
                $remove_list=$this->request->data['array'];
            if(!empty($remove_list)){
              foreach($remove_list as $row){

                    $str=explode('-',$row);
                    $cookie_id=$str[0];
                     $c_id=$str[1];
			 $c_c_id=$str[2];
                    $course_id = @convert_uudecode(base64_decode($c_id));
			$check_course_id= @convert_uudecode(base64_decode($c_c_id));
                     $cookie_id = @convert_uudecode(base64_decode($cookie_id));

                    $this->loadModel('Course');
			 $this->loadModel('purchaseItem');
                       $login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
                    $locale_id = $this->Session->read('LocTrain.LangId');
                    $course_list = $this->Course->find('count',array('conditions'=>array('Course.id'=>$course_id)));	
                  	$checkItem=$this->purchaseItem->find('first',array('conditions'=>array('purchaseItem.item_name'=>$cookie_id,'m_id'=>$login_m_id)));
//echo '-'.$checkItem['purchaseItem']['item_value'];
		
                        if($course_list > 0 && isset($checkItem['purchaseItem']) && $checkItem['purchaseItem']['item_value']  == $c_c_id){
				
                            $this->loadModel('CoursePurchaseList');
				$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
				$memberCourse['CoursePurchaseList']['c_id'] = $course_id;
				$memberCourse['CoursePurchaseList']['type'] = 1;
				$this->CoursePurchaseList->save($memberCourse);				
				$this->Cookie->delete('LocTrainPurchase.'.$cookie_id);
                                $this->purchaseItem->deleteALL(array('item_name' => $cookie_id,'m_id'=>$login_m_id));
				$this->before_update_notification();
				$login_m_id = $this->Session->read('LocTrain.id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_added_learning_plan';
				$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->saveAll($notification);
				
				//$this->Session->write('LocTrain.flashMsg',__('This course has been added to your learning plan'));
				//$this->redirect($this->referer());
                        }else if($course_list > 0 && $this->Cookie->check('LocTrainPurchase.'.$cookie_id) == $c_id) {
                      $this->loadModel('CoursePurchaseList');
				$memberCourse['CoursePurchaseList']['mem_id'] = $this->Session->read('LocTrain.id');
				$memberCourse['CoursePurchaseList']['c_id'] = $course_id;
				$memberCourse['CoursePurchaseList']['type'] = 1;
				$this->CoursePurchaseList->saveAll($memberCourse);				
				$this->Cookie->delete('LocTrainPurchase.'.$cookie_id);	
				$this->before_update_notification();
				$login_m_id = $this->Session->read('LocTrain.id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_added_learning_plan';
				$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->saveAll($notification);
				
				//$this->Session->write('LocTrain.flashMsg',__('This course has been added to your learning plan'));
				
                  }else{
                    $this->before_update_notification();
				$login_m_id = $this->Session->read('LocTrain.id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'course_not_added_learning_plan';
				$notification['Notification']['c_id'] = $course_id;
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->saveAll($notification);
			//	$this->Session->write('LocTrain.flashMsg',__('Course not added in learning plan. Please try again.'));		
				
                  }
                 
                 }

              $this->redirect($this->referer());
                }
            }else{
                $ths->before_update_notification();
                $temp_id = $this->Session->read('LocTrain.visitor_id');
                $this->loadModel('Notification');
                $notification['Notification']['mem_id'] =  0;
                $notification['Notification']['notification_type'] = 'member_not_login';
                //$notification['Notification']['c_id'] = $course_id;
                $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                $notification['Notification']['notification_sender_id'] = $temp_id;
                $notification['Notification']['read'] = 1;
                $this->Notification->create();
                $this->Notification->saveAll($notification);
                echo 'index';
				//$this->Session->write('LocTrain.flashMsg',__('Please login to access this section.'));
				//$this->redirect(array('controller'=>'Home','action'=>'index'));				
            }
            		
	}
	function cookie_setting()
	{
		//sleep(5);
		$this->layout = '';
		$this->autoRender = false;
		$this->viewPath = 'Elements'.DS.'frontElements/home';
		$this->render('cookies_settings');			
	}

	
	function add_reviews()
	{
		if(!empty($this->data))
		{
			if(!empty($this->data['CourseRemark']['comment']))
			{
			   if(strlen($this->data['CourseRemark']['comment'])<=1024)
			   {
					$course_id = $this->data['CourseRemark']['course_id'];
					$this->request->data['CourseRemark']['member_id']=$this->Session->read("LocTrain.id");
					$this->request->data['CourseRemark']['date_added']=strtotime(date('Y-m-d H:i:s'));
					$this->request->data['CourseRemark']['block']=0;
					$this->request->data['CourseRemark']['promote']=0;
					$this->request->data['CourseRemark']['report']=0;
					$this->loadModel('CourseRemark');
					if($this->CourseRemark->save($this->data))
					{
						$locale_id = $this->Session->read('LocTrain.LangId');
						$this->loadModel('Course');
						$course=$this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id)));
						$review=$course['Course']['review'];
						$review++;
						$this->Course->id=$course_id;
						$this->Course->saveField('review',$review);
					  					  
						$resp=array('status'=>'ok','msg'=>'Thank you for the review.');
						$this->before_update_notification();
						$login_m_id = $this->Session->read('LocTrain.id');
						$this->loadModel('Notification');
						$notification['Notification']['mem_id'] =  0;
						$notification['Notification']['notification_type'] = 'review';
						$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['notification_sender_id'] = $login_m_id;
						$this->Notification->create();
						$this->Notification->save($notification);
						//$this->Session->write('LocTrain.flashMsg',__('Thank you for your review.'));		
						
						
					}
			   }
			 else
			   {
			   	 $resp=array('status'=>'error','msg'=>__('Comment has exceeded 1024 characters'));
			   }
				
			}
			else
			{
			  $resp=array('status'=>'error','msg'=>__('this_field_is_required'));	
			}
		}
		echo json_encode($resp);die;
	}	

	function play_video($id=NULL)
	{
		$course_id = @convert_uudecode(base64_decode($id));
		$this->loadModel('Course');
		$locale_id = $this->Session->read('LocTrain.LangId');
		$course_list = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id)));
		
		if(!empty($course_list['CourseLesson'])){
			$video = '';
			foreach($course_list['CourseLesson'] as $lesson){
				if($lesson['video'] != ''  && $lesson['order'] == '0'){
					$video = $lesson['video'];
					break;
				}
			}
			if(!empty($video)){
				$res = array('status'=>'true','video'=>$video,'container'=>$course_list['Course']['m_id'],'extension'=>$lesson['extension']);
				echo json_encode($res);
			}else{
				$res = array('status'=>'false');
				echo json_encode($res);
			}
		}else{
			$res = array('status'=>'nolesson','error'=>__('No video available'));
			echo json_encode($res);
		}
		die;
	}
	function play_lesson_video($id=NULL)
	{
		$lesson_id = @convert_uudecode(base64_decode($id));
		$this->loadModel('CourseLesson');
		$lesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$lesson_id)));

		if(!empty($lesson)){
				$video = '';
				if($lesson['CourseLesson']['video'] != ''){
					$video = $lesson['CourseLesson']['video'];
					
				}
			
			if(!empty($video)){
				$res = array('status'=>'true','video'=>$video,'container'=>$lesson['Course']['m_id'],'extension'=>$lesson['CourseLesson']['extension']);
				echo json_encode($res);
			}else{
				$res = array('status'=>'false');
				echo json_encode($res);
			}
		}else{
			$res = array('status'=>'nolesson','error'=>__('No video available'));
			echo json_encode($res);
		}
		die;
	}
	function transaction_success()
	{
		$this->layout="public";
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$this->loadModel('CoursePurchase');
		$this->loadModel('CoursePurchaseList');
		$this->Cookie->delete('LocTrainPurchase');
		
		$login_m_id = $this->Session->read('LocTrain.id');
		$this->before_update_notification();
		$this->loadModel('Notification');
		$notification['Notification']['mem_id'] =  0;
		$notification['Notification']['notification_type'] = 'course_purchased_sucessfully';
		$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['notification_sender_id'] = $login_m_id;
		$this->Notification->create();
		$this->Notification->save($notification);
		$this->redirect(array('controller'=>'Home','action'=>'purchase'));
		
	}
	function transaction_failure()
	{
		$this->layout="public";
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		$login_m_id = $this->Session->read('LocTrain.id');
		$this->before_update_notification();
		$this->loadModel('Notification');
		$notification['Notification']['mem_id'] =  0;
		$notification['Notification']['notification_type'] = 'course_not_purchased';
		$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['notification_sender_id'] = $login_m_id;
		$this->Notification->create();
		$this->Notification->save($notification);
$this->redirect(array('controller'=>'Home','action'=>'purchase'));

	}
	function subscription_success()
	{
		$this->layout="public";
		$title_for_layout = 'Localization Training';
		
		
		$login_m_id = $this->Session->read('LocTrain.id');
        $locale = $this->Session->read('LocTrain.locale');	
		$this->before_update_notification();
		$this->loadModel('Notification');
		$notification['Notification']['mem_id'] =  0;
		$notification['Notification']['notification_type'] = 'subscription_success';
		$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['notification_sender_id'] = $login_m_id;
		$this->Notification->create();
		$this->Notification->save($notification);
		
		$this->set(compact('title_for_layout'));
		
		



		
		/*$item_name = $_POST['item_name'];
		$item_number = $_POST['item_number'];
		$payment_status = $_POST['payment_status'];
		$payment_amount = $_POST['mc_gross'];
		$payment_currency = $_POST['mc_currency'];
		$txn_id = $_POST['txn_id'];
		$receiver_email = $_POST['receiver_email'];
		$payer_email = $_POST['payer_email'];
		$member_id = $_POST['custom'];
		
		if(strtolower($payment_status) == 'completed'){	
			switch($item_number){
				case '0':
					$date = date("Y-m-d H:i:s");
					//increment 1 month
					$expiry_date = strtotime($date."+ 1 month");	
					$pay_for = 'One Month';				
					$subscription['MemberSubscription']['date_expired'] = $expiry_date;
				break;
				case '1':
					$date = date("Y-m-d H:i:s");
					//increment 3 months
					$expiry_date = strtotime($date."+ 3 month");	
					$pay_for = 'Three months';						
					$subscription['MemberSubscription']['date_expired'] = $expiry_date;
				break;
				case '2':
					$date = date("Y-m-d H:i:s");
					//increment 6 months
					$expiry_date = strtotime($date."+ 6 month");
					$pay_for = 'Six months';							
					$subscription['MemberSubscription']['date_expired'] = $expiry_date;
				break;
				case '3':
					$date = date("Y-m-d H:i:s");
					//increment 1 year
					$expiry_date = strtotime($date."+ 1 year");		
					$pay_for = 'Twelve months';					
					$subscription['MemberSubscription']['date_expired'] = $expiry_date;
				break;
			}
			$this->loadModel('EmailTemplate');
			$this->loadModel('Member');
			$mem_id=$this->Session->read('LocTrain.id');
			$mem = $this->Member->find('first',array('conditions'=>array('Member.id'=>$mem_id),'contain'=>false,'fields'=>array('email','final_name')));
			$email = $mem['Member']['email'];
			$final_name = $mem['Member']['final_name'];		
					// --------------------Transaction Confirmation To Member-----------------							
			$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>9,'locale'=>$locale),'fields'=>array('subject','description','email_from')));						
			$emailData = $emailTemplate['EmailTemplate']['description'];						
			$emailData = str_replace(array('{name}','{payment_for}','{txn_id}','{amount}','{expiry_date}'),array($final_name,$pay_for,$txn_id,$payment_amount,date('d M Y',$expiry_date)),$emailData);							
			$emailTo = $email;
			$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);	
					//------------- sent---------//
			$this->loadModel('MemberSubscription');
			$getRecord = $this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.txn_id'=>$txn_id)));
			if($getRecord == 0){
				
				$subscription['MemberSubscription']['m_id'] = convert_uudecode(base64_decode($member_id));
				$subscription['MemberSubscription']['type'] = $item_number;
				$subscription['MemberSubscription']['payment_status'] = $payment_status;
				$subscription['MemberSubscription']['payment_amount'] = $payment_amount;
				$subscription['MemberSubscription']['txn_id'] = $txn_id;
				$subscription['MemberSubscription']['date_added'] = strtotime(date('Y-m-d h:i:s'));
				$subscription['MemberSubscription']['status'] = '1';
				
				try{				
					$this->MemberSubscription->save($subscription);						
				}catch(Exception $e){				
					$error = ($e->getMessage().'<pre>'.$e->getTraceAsString().'</pre>');
				}
				
			}
		}*/
	}
	function subscription_failure()
	{
		$this->layout="public";
		$title_for_layout = 'Localization Training';
		$this->before_update_notification();
		$login_m_id = $this->Session->read('LocTrain.id');
		$this->loadModel('Notification');
		$notification['Notification']['mem_id'] =  0;
		$notification['Notification']['notification_type'] = 'subscription_failure';
		$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['notification_sender_id'] = $login_m_id;
		$this->Notification->create();
		$this->Notification->save($notification);
		
		$this->set(compact('title_for_layout'));
	}
	
	function cancel_account()
	{ 
	
	   
		$this->layout = 'public';
		$this->loadModel('CancelReason');
		$this->loadModel('Member');
		$this->loadModel('CoursePurchaseList');
		$this->loadModel('Reason');
		$reasons =$this->Reason->find('all');
		$this->loadModel('MemberSubscription');
		
		
		$mid = $this->Session->read('LocTrain.id');
		
		$m_data=$this->MemberSubscription->find('first',array('conditions'=>array('MemberSubscription.m_id'=>$mid,'MemberSubscription.status'=>'1'),'order'=>array('MemberSubscription.id'=>'desc'),'fields'=>array('date_expired','status','cancel_request')));
		//pr($m_data);die;
		if(!empty($m_data))
		{	
			/*App::import("Helper", "Time");
			$time = new TimeHelperzone('view'=>NULL);
			$exp_time = $time->convert($m_data['MemberSubscription']['date_expired'],$this->Session->read('Timezone'));
			$exp_date = $this->Time->format('d-m-Y H:i', $m_data['MemberSubscription']['date_expired'], NULL, $this->Session->read('Timezone')); 
			$exp_date=date('d/m/Y',$exp_time); */
			$exp_date=date('d-M-Y',$m_data['MemberSubscription']['date_expired']);
			$cancel_request=$m_data['MemberSubscription']['cancel_request'];
			
		}
		else
		{$exp_date='';
			$cancel_request=0;
		}
		
		
		$author_data=$this->Member->find('first',array('conditions'=>array('Member.id'=>$mid),'fields'=>array('role_id')));
		$is_author=$author_data['Member']['role_id'];
		if(!empty($m_data))
		{		
			$type='subscriber';
		}
		elseif($is_author=='3')
		{
			$type='author';
		}
		elseif($is_author=='2')
		{
			$type='purchaser';
		}
		
		$purchase_info=$this->CoursePurchaseList->find('all',array('conditions'=>array('CoursePurchaseList.mem_id'=>$mid,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0),'limit'=>'5','order'=>'CoursePurchaseList.id desc'));
		$purchase_count=$this->CoursePurchaseList->find('count',array('conditions'=>array('CoursePurchaseList.mem_id'=>$mid,'CoursePurchaseList.status'=>0)));
		
		$purchase_count=$purchase_count-5;
		//pr($purchase_info);die;
		//pr($purchase_count);die;
		
	   $this->set(compact('reasons','exp_date','mid','sub_status','pur_count','is_author','type','cancel_request','purchase_info','purchase_count'));
		if(!empty($this->data))
		{
			 
			$error = array();
			$error = $this->validate_cancelAccount($this->data);
	        
			if(count($error) == 0){
				$memberId = $this->Session->read('LocTrain.id');
				
				$this->Member->updateAll(array('Member.status'=>'2'),array('Member.id'=>$memberId));
				
				$this->request->data['CancelReason']['m_id'] = $memberId;
				$this->request->data['CancelReason']['date'] = date('Y-m-d H:i:s');
				
				$this->CancelReason->save($this->data);
				
				$last_id = $this->CancelReason->getLastInsertId();				
				$unique = array_unique($this->data['CancelReason']['reason_id']);
				$this->loadModel('UserReason');
				foreach($unique as $val)
				{
					if($val != 0)
					{						
						$data['UserReason']['cancel_reason_id'] = $last_id;
						$data['UserReason']['reason_id'] = $val;
						$this->UserReason->create();
						$this->UserReason->save($data);
					}
				}
				
				$this->Session->delete('LocTrain');
				$this->redirect(array('controller'=>'Home','action'=>'index'));
			}
		}
	 }
	function validate_cancel_account_ajax()
	{
		$this->layout="";
		$this->autoRender=false;	
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_cancelAccount($this->data);		
			if(is_array ( $this->data ) )	{
				foreach ($this->data['CancelReason'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	//-----lalit 13 nov--------
	function expire_subscription()
	{		
		    $this->loadModel('MemberSubscription');
			$this->loadModel('Member');
			$this->loadModel('CancelReason');
$locale = $this->Session->read('LocTrain.locale');	
			
			$this->loadModel('UserReason');
			$mid=$_GET['id'];
			$text=$_GET['text_data'];
			$resn=$_GET['resn'];
			
			
			$m_data=$this->MemberSubscription->find('first',array('conditions'=>array('MemberSubscription.m_id'=>$mid,'MemberSubscription.status'=>'1'),'order'=>array('MemberSubscription.id'=>'desc'),'fields'=>array('date_expired','status','cancel_request','id')));
			$tem_id=$m_data['MemberSubscription']['id'];
					
			$this->MemberSubscription->updateAll(array('MemberSubscription.cancel_request'=>'1'),array('MemberSubscription.id'=>$tem_id));
			
			$this->request->data['CancelReason']['m_id'] = $mid;
			$this->request->data['CancelReason']['reason'] =$text;
			$this->request->data['CancelReason']['date'] = date('Y-m-d H:i:s');
				
			$this->CancelReason->save($this->data);
			$last_id = $this->CancelReason->getLastInsertId();
			foreach($resn as $resn)
				{ 
					if($resn !='0')
					{	$resn = strip_tags($resn);
						$data['UserReason']['cancel_reason_id'] = $last_id;
						$data['UserReason']['reason_id'] = $resn;
						$this->UserReason->create();
						$this->UserReason->save($data);
					}
				}
			
			
			
			$m_data=$this->Member->find('first',array('conditions'=>array('Member.id'=>$mid),'fields'=>array('email','final_name','locale')));
			$email=$m_data['Member']['email'];
			$m_locale=$m_data['Member']['locale']?$m_data['Member']['locale']:'en';
			$name = $m_data['Member']['final_name'];
			$this->loadModel('EmailTemplate');
			// --------------------Email Confirmation To Cancellation-----------------							
			$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>12,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
			$emailData = $emailTemplate['EmailTemplate']['description'];						
			$emailData = str_replace(array('{final_name}'),array($name),$emailData);									
			$emailTo =$email;
			$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
			$this->Session->delete('LocTrain');

                               $visitor_id = str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ');
				$visitor_id=substr($visitor_id,0,8);
				$this->Session->write('LocTrain.visitor_id',$visitor_id);
				//$this->Session->write('LocTrain.noti_type','after');
				$this->before_update_notification();
				$this->loadModel('Notification');
				$temp_id = $this->Session->read('LocTrain.visitor_id');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'account_cancel_success';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $temp_id;
				$notification['Notification']['read'] = 1;
				$this->Notification->create();
				$this->Notification->save($notification);
			echo 'done';
			die;
	}
	
	
	function purchaser_cancel_account()
	{		$this->loadModel('CoursePurchaseList');
			$this->loadModel('Member');
			$this->loadModel('MemberSubscription');
			$this->loadModel('CancelReason');
			$this->loadModel('Reason');
			$this->loadModel('UserReason');
$locale = $this->Session->read('LocTrain.locale');	
			$mid=$_GET['id'];
			$text=$_GET['text_data_first'];
			$resn=$_GET['resn'];
			$this->Member->updateAll(array('Member.status'=>2),array('Member.id'=>$mid));
			//$this->CoursePurchaseList->deleteAll(array('CoursePurchaseList.mem_id'=>$mid));
			$this->CoursePurchaseList->updateAll(array('CoursePurchaseList.status'=>1),array('CoursePurchaseList.mem_id'=>$mid));
			$this->request->data['CancelReason']['m_id'] = $mid;
			$this->request->data['CancelReason']['reason'] =$text;
			$this->request->data['CancelReason']['date'] = date('Y-m-d H:i:s');
				
			$this->CancelReason->save($this->data);
			$last_id = $this->CancelReason->getLastInsertId();
			foreach($resn as $resn)
				{ 
					if($resn !='0')
					{	$resn = strip_tags($resn);
						$data['UserReason']['cancel_reason_id'] = $last_id;
						$data['UserReason']['reason_id'] = $resn;
						$this->UserReason->create();
						$this->UserReason->save($data);
					}
				}
			
			
			
			$m_data=$this->Member->find('first',array('conditions'=>array('Member.id'=>$mid),'fields'=>array('email','final_name','locale')));
			$email=$m_data['Member']['email'];
			$m_locale=$m_data['Member']['locale']?$m_data['Member']['locale']:'en';
			$name = $m_data['Member']['final_name'];
			$this->loadModel('EmailTemplate');
			// --------------------Email Confirmation To Cancellation-----------------							
			$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>12,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
			$emailData = $emailTemplate['EmailTemplate']['description'];						
			$emailData = str_replace(array('{final_name}'),array($name),$emailData);									
			$emailTo =$email;
			$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
			echo 'done';
			$this->Session->delete('LocTrain');
                                $visitor_id = str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ');
				$visitor_id=substr($visitor_id,0,8);
				$this->Session->write('LocTrain.visitor_id',$visitor_id);
				//$this->Session->write('LocTrain.noti_type','after');
				$this->before_update_notification();
				$this->loadModel('Notification');
				$temp_id = $this->Session->read('LocTrain.visitor_id');
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'account_cancel_success';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $temp_id;
				$notification['Notification']['read'] = 1;
				$this->Notification->create();
				$this->Notification->save($notification);
			die;
	
	}
	function author_cancel_account()
	{
			$this->loadModel('CoursePurchaseList');
			$this->loadModel('Member');
			$this->loadModel('Course');
			$this->loadModel('CancelReason');
			$this->loadModel('Reason');
			$this->loadModel('UserReason');
$locale = $this->Session->read('LocTrain.locale');	
			$mid=$_GET['id'];
			
			$text=$_GET['text_data_second'];
			$resn=$_GET['resn'];
			$locale_id = $this->Session->read('LocTrain.LangId');
			$my_course=$this->Course->find('all',array('conditions'=>array('Course.m_id'=>$mid)));
			if(!empty($my_course))
			{
				foreach($my_course as $my_course)
				{
				 if(empty($my_course['CoursePurchaseList']) && empty($my_course['Subscription_course']) && empty($my_course['Subscription_lesson']))
				 {
				 $this->Course->delete(array('Course.id'=>$my_course['Course']['id']));
				 }
						
				}
			}
			$this->Member->updateAll(array('Member.status'=>2),array('Member.id'=>$mid));
			$this->request->data['CancelReason']['m_id'] = $mid;
			$this->request->data['CancelReason']['reason'] =$text;
			$this->request->data['CancelReason']['date'] = date('Y-m-d H:i:s');
				
			$this->CancelReason->save($this->data);
			$last_id = $this->CancelReason->getLastInsertId();
			foreach($resn as $resn)
				{ 
					if($resn !='0')
					{	$resn = strip_tags($resn);
						$data['UserReason']['cancel_reason_id'] = $last_id;
						$data['UserReason']['reason_id'] = $resn;
						$this->UserReason->create();
						$this->UserReason->save($data);
					}
				}
			
			$m_data=$this->Member->find('first',array('conditions'=>array('Member.id'=>$mid),'fields'=>array('email','final_name','locale')));
			$email=$m_data['Member']['email'];
			$m_locale=$m_data['Member']['locale']?$m_data['Member']['locale']:'en';
			$name = $m_data['Member']['final_name'];
			$this->loadModel('EmailTemplate');
			// --------------------Email Confirmation To Cancellation-----------------							
			$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>12,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
			$emailData = $emailTemplate['EmailTemplate']['description'];						
			$emailData = str_replace(array('{final_name}'),array($name),$emailData);									
			$emailTo =$email;
			$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
			echo 'done';
			$this->Session->delete('LocTrain');


	$visitor_id = str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ');
				$visitor_id=substr($visitor_id,0,8);
				$this->Session->write('LocTrain.visitor_id',$visitor_id);
			//	$this->Session->write('LocTrain.noti_type','after');
				$this->before_update_notification();
				$this->loadModel('Notification');
				$temp_id = $this->Session->read('LocTrain.visitor_id');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'account_cancel_success';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $temp_id;
				$notification['Notification']['read'] = 1;
				$this->Notification->create();
				$this->Notification->save($notification);
			die;
	}
	
	function validate_cancelAccount($data)
	{			
		$errors = array ();
		$selected=0;
		foreach($data['CancelReason']['reason_val'] as $key=>$val)
		{
		
			if(trim($val) != 0){
			echo 	$selected = 1;
			}
		}
		if(!$selected)
		{
			$errors['reason_val'][] = __(FIELD_REQUIRED)."\n";
		}
		return $errors;		
	} 
	
	

	function catalogue_landing()
	{	//Configure::write('debug',2);
		$this->layout =	'public';		
		$title_for_layout = 'Localization Training';
		$this->loadModel('Interest');
		$this->loadModel('News');
		$this->loadModel('CourseKeyword');
		$this->loadModel('Course');
		$this->loadModel('Member');
		$this->loadModel('FeatureCourse');
		$this->loadModel('CourseCategory');
		$this->loadModel('FreeVideo');
		$this->loadModel('CoursePurchaseList');
		$mem_login_id=$this->Session->read('LocTrain.id');
		$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
		$this->set('locale',$locale);
		$locale_id = $this->Session->read('LocTrain.LangId');
		//pr($locale_id);die;
		if($this->RequestHandler->isAjax())
		{
			$this->layout = '';
			$this->autoRender = false;
			$locale_id = $this->Session->read('LocTrain.LangId');
			
			if(isset($this->params->query['alphabet']) && !empty($this->params->query['alphabet'])){
					$q = $this->params->query['alphabet'];
					//echo $q; die;
					if(trim($q)==1)
					{
					
						$alpha_courses = $this->Course->find('all',array('conditions'=>array('Course.title REGEXP'=>'^[^a-zA-Z]','Course.status'=>1,'Course.isPublished'=>1),'fields'=>array('id','title','description','m_id'),'contain'=>array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'CoursePurchaseList')));
					}
					else
					{
						$alpha_courses = $this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.title LIKE ? OR ?'=>array($q.'%',strtoupper($q).'%')),'fields'=>array('id','title','description','m_id'),'contain'=>array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'CoursePurchaseList')));	
					}
					
					$this->set(compact('alpha_courses'));
					if(!empty($alpha_courses)){
						$this->viewPath = 'Elements'.DS.'frontElements/home';
						$this->render('alphabetical_course');
						}else{
						echo '<div class="mytch_rcd">'.__('no_records_found').'</div>';die;
					}
			}
			if(isset($this->params->query['authorId']) && !empty($this->params->query['authorId'])){
					$auth_id = convert_uudecode(base64_decode($this->params->query['authorId']));
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
					$author_course=$this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.m_id'=>$auth_id),'contain'=>array('CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)),'CourseLesson'=>array('fields'=>array('id','video','extension','image'),'order'=>'CourseLesson.order ASC'),'CourseView'=>array('order'=>'CourseView.no_of_views DESC'))));
					$this->set(compact('author_course'));
					if(!empty($author_course)){
						$this->viewPath = 'Elements'.DS.'frontElements/home';
						$this->render('author_course');
						}else{
						echo '<div class="mytch_rcd">'.__('no_records_found').'</div>';die;
					}
			}
			if(isset($this->params->query['sub_category']) && !empty($this->params->query['sub_category'])){
					//if($this->params->query['sub_category']!='subcategory-others_')
					if(! preg_match('/_/',$this->params->query['sub_category']))
					{
						$subcategory_id = convert_uudecode(base64_decode($this->params->query['sub_category']));
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
						$course_subcategory=$this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.subcategory_id'=>$subcategory_id),'contain'=>array('CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)),'CourseLesson'=>array('fields'=>array('id','video','extension','image'),'order'=>'CourseLesson.order ASC'))));
					}else{
						$ss = preg_split('/_/',$this->params->query['sub_category']);
						$subcategory_id=convert_uudecode(base64_decode($ss[1]));
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
						$course_subcategory=$this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.subcategory_id'=>NULL,'Course.category_id'=>$subcategory_id),'contain'=>array('CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)),'CourseLesson'=>array('fields'=>array('id','video','image','extension'),'order'=>'CourseLesson.order ASC'))));
					}
					$this->set(compact('course_subcategory'));
					if(!empty($course_subcategory)){
						$this->viewPath = 'Elements'.DS.'frontElements/home';
						$this->render('category_course');
						}else{
						echo '<div class="mytch_rcd">'.__('no_records_found').'</div>';die;
					}
			}			
			if(isset($this->params->query['category']) && !empty($this->params->query['category'])){
					$category_id = $this->params->query['category'];
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
					$course_subcategory=$this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.category_id'=>$category_id),'contain'=>array('CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)),'CourseLesson'=>array('fields'=>array('id','video','image','extension'),'order'=>'CourseLesson.order ASC'))));
					$this->set(compact('course_subcategory'));
					if(!empty($course_subcategory)){
						$this->viewPath = 'Elements'.DS.'frontElements/home';
						$this->render('category_course');
						}else{
						echo '<div class="mytch_rcd">'.__('no_records_found').'</div>';die;
					}
			}
			if(isset($this->params->query['sl_sub_category']) && !empty($this->params->query['sl_sub_category'])){
				
				$sl_subcategory_id = $this->params->query['sl_sub_category'];
				
				if($sl_subcategory_id == 'others')
				{
					$sub_cat_id = convert_uudecode(base64_decode($this->params->query['sub_category']));
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
					$course_subcategory=$this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.subcategory_id'=>$sub_cat_id,'Course.sl_subcategory_id'=>0),'contain'=>array('CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)),'CourseLesson'=>array('fields'=>array('id','video','image','extension'),'order'=>'CourseLesson.order ASC'))));
				} else {
								
					$sl_subcategory_id = convert_uudecode(base64_decode($this->params->query['sl_sub_category']));
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
					$course_subcategory=$this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.sl_subcategory_id'=>$sl_subcategory_id),'contain'=>array('CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)),'CourseLesson'=>array('fields'=>array('id','video','image','extension'),'order'=>'CourseLesson.order ASC'))));
				}
				
				$this->set(compact('course_subcategory'));
				if(!empty($course_subcategory)){
					$this->viewPath = 'Elements'.DS.'frontElements/home';
					$this->render('category_course');
					}else{
					echo '<div class="mytch_rcd">'.__('no_records_found').'</div>';die;
				}
			}
		}
		else
		{ //$time1 = time(); echo "Time Before: ". $time1;
			/*$interest = $this->Interest->find('all',array('fields'=>array('interest'),'order'=>'Interest.interest asc'));*/
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
				$new = $this->Course->find('all',array('fields'=>array('id','m_id','rating','price','title','image','description'),'limit'=>10,'conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),'order'=>'Course.id DESC','contain'=>array('CourseLesson'=>array('fields'=>array('id','video','extension','image'),'order'=>'CourseLesson.order ASC'),'CourseView'=>array('order'=>'CourseView.no_of_views DESC'),'CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)))));
$this->Course->unbindModel(array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')) );
			$course_most_popular=$this->Course->find('all',array('fields'=>array('id','m_id','rating','price','title','image','description'),'conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),'contain'=>
    array('CourseLesson'=>array('fields'=>array('id','video','extension','image'),'order'=>'CourseLesson.order ASC'),'CourseView'=>array('order'=>'CourseView.no_of_views DESC'),'CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0))),'limit'=>5));
			/*$course_owner = $this->CoursePurchaseList->find('all',array('conditions'=>array('CoursePurchaseList.type'=>'0'),'contain'=>array('Course'=>array('Member'=>array('id','given_name')))));*/
$course_owner=$this->CoursePurchaseList->query('SELECT `CoursePurchaseList`.`id` ,`Member`.`id`, `Member`.`given_name` FROM `course_purchase_lists` AS `CoursePurchaseList` LEFT JOIN `courses` AS `Course` ON (`CoursePurchaseList`.`c_id` = `Course`.`id`) left JOIN `members` AS `Member` ON (`Course`.`m_id` = `Member`.`id`) WHERE `CoursePurchaseList`.`type` = 0');
		//$time2 = time(); echo "<br/>Time After: ".$time2; echo "<br/>Difference (in Seconds): ".($time2 - $time1); die;

			if(!empty($course_owner))
			{
				$author_member = array();
				foreach($course_owner as $owner)
				{
					$author_member[] = @$owner['Course']['Member']['id'];
				}
				$author_member = array_unique($author_member);
				$author_men = array();
				foreach($author_member as $author)
				{
					if(!empty($author))
					{
						$author_men[] = $author;
					} 	
				}
			}
			
			$course = $this->Course->find('list',array('conditions'=>array('Course.status'=>'1','Course.isPublished'=>1),'fields'=>array('m_id'),'contain'=>false));
			$mem_course = array();	
			foreach($course as $k=>$val)
			{
				$mem_course[] = $val;
			}
			$mem_course = array_unique($mem_course);
			//$author = $this->Member->find('list',array('conditions'=>array('Member.status'=>'1','Member.role_id'=>'3'),'order'=>array('Member.given_name ASC'),'fields'=>array('given_name')));
			$this->Member->virtualFields = array('name'=>'SELECT CONCAT(given_name," ",family_name) FROM members WHERE members.id = Member.id');
			$author = $this->Member->find('list',array('conditions'=>array('OR'=>array('Member.id'=>$author_men,'Member.status'=>'1'),array('Member.status'=>'1','Member.role_id'=>'3','Member.id'=>$mem_course)),'order'=>array('Member.given_name ASC'),'fields'=>array('name')));
				
			$key=key($author);
			
			
			$categories= $this->CourseCategory->find('all',array('order'=>'CourseCategory.'.$locale.' ASC','conditions'=>array('CourseCategory.status'=>'1'),'contain'=>array('CourseSubcategory'=>array('conditions'=>array('CourseSubcategory.status'=>'1'),'CourseSlSubcategory'=>array('conditions'=>array('CourseSlSubcategory.status'=>'1'))),'Course'=>array('conditions'=>array('Course.status'=>'1','Course.isPublished'=>1,'Course.subcategory_id'=>NULL)))));
			//pr($categories); die;
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
			$author_course=$this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.m_id'=>$key),'contain'=>array('CourseLesson'=>array('fields'=>array('id','video','extension','image'),'order'=>'CourseLesson.order ASC'),'CourseView'=>array('order'=>'CourseView.no_of_views DESC'),'CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)))));
			//$member_keyword_ids = $this->CourseKeyword->find('list',array('conditions'=>array('CourseKeyword.keyword'=>$q),'fields'=>array('course_id')));
			$q='l';
			
                                $feature_by=$this->FeatureCourse->find('first');
				$choice=$feature_by['FeatureCourse']['featured_by'];
				switch ($choice)
				{
					case 1:
						
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
						/*$courses= $this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),'contain'=>array('CourseView'=>array('order'=>'CourseView.no_of_views DESC'),'CourseLesson'=>array('order'=>array('CourseLesson.order ASC'))),'limit'=>10,'fields'=>array('id','title','m_id','extension','image','price','rating','image')));*/
$courses = $this->Course->find('all',
        array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),
            'contain'=>array('CourseView'=>array('order'=>'CourseView.no_of_views DESC'),'CourseLesson'=>array('order'=>array('CourseLesson.order ASC'),'fields'=>array('id','video','extension','image'))
                ,'CourseRating'=>array('fields'=>array('rating'))
                ,'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0))
                ),'limit'=>10,
            'fields'=>array('id','title','m_id','extension','image','price','rating','image')));
						break;
					case 2:
						//$feature_by="Most recently published";
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
						/*$courses = $this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),'order'=>array('Course.id'=>'desc'),'contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.order ASC'),'fields'=>array('video','extension','image'))),'limit'=>10,'fields'=>array('id','title','m_id','price','rating','image')));*/
$courses = $this->Course->find('all',
        array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),
            'order'=>array('Course.id'=>'desc'),
            'contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.order ASC'),'fields'=>array('id','video','extension','image'))
                ,'CourseRating'=>array('fields'=>array('rating'))
                ,'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0))
                ),'limit'=>10,
            'fields'=>array('id','title','m_id','price','rating','image')));	
				
						break;
					case 3:
						//$feature_by="Alphabetical title";
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
						/*$courses = $this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),'order'=>array('Course.title'=>'ASC'),'contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.order ASC desc'),'fields'=>array('video','extension','image'))),'limit'=>10,'fields'=>array('id','title','m_id','price','rating','image')));*/	
						$courses = $this->Course->find('all',
        array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),
            'order'=>array('Course.title'=>'ASC'),
            'contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.order ASC'),'fields'=>array('id','video','extension','image'))
                ,'CourseRating'=>array('fields'=>array('rating'))
                ,'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0))
                ),'limit'=>10,
            'fields'=>array('id','title','m_id','price','rating','image')));
						break;
					case 4:
						//$feature_by="Randomly";
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
						/*$courses = $this->Course->find('all',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),'order'=>'rand()','contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.order ASC'),'fields'=>array('video','extension','image'))),'limit'=>10,'fields'=>array('id','title','m_id','price','rating','image')));*/
$courses = $this->Course->find('all',
        array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1),
            'order'=>'rand()',
            'contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.order ASC'),'fields'=>array('id','video','extension','image'))
                ,'CourseRating'=>array('fields'=>array('rating'))
                ,'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$mem_login_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0))
                ),'limit'=>10,
            'fields'=>array('id','title','m_id','price','rating','image')));	
				
						break;
					
				}
				
				//$courses = $this->Course->find('all',array('conditions'=>array('Course.status'=>1),'order'=>array('Course.id'=>'desc'),'contain'=>array('CourseLesson'=>array('order'=>array('CourseLesson.id desc'),'fields'=>array('video'))),'limit'=>2,'fields'=>array('id','title','m_id')));	
				//pr($courses);die;
				
		}	
//print_r($courses);	
		$freevideo = $this->FreeVideo->find('all',array('conditions'=>array('FreeVideo.status'=>'1'),'order'=>array('FreeVideo.id DESC'),'contain'=>false));
		$this->set(compact('title_for_layout','freevideo','courses','new','course_most_popular','author','author_course','categories'));
		
	}

	function course_viewed($course_id)
	{
		$this->loadModel('CourseView');
		$course_view=$this->CourseView->find('first',array('conditions'=>array('CourseView.course_id'=>$course_id)));
		
		if(!empty($course_view))
		{
			$no_of_views=$course_view['CourseView']['no_of_views'];
			$no_of_views++;
			$id=$course_view['CourseView']['id'];
			//$this->CourseView->id = $id;
			//$this->CourseView->saveField('no_of_views',$no_of_views);
			$this->CourseView->updateAll(
			    array('no_of_views' => "'$no_of_views'"),
			    array('id' => $id)
			);
		}
		else
		{
			$data = $this->CourseView->create();
			$data['CourseView']['course_id']=$course_id;
			$data['CourseView']['no_of_views']=1;
			$this->CourseView->save($data);
		}
	}
	function find_subcategories()
	{
		$this->loadModel('CourseSubcategory');		
		$category_id = $_GET['category_id'];
		
		$locale = $this->Session->read('LocTrain.locale');
		
		if(!empty($category_id) && $this->request->is('ajax'))
		{
			$subs='';
			$sub_categories = $this->CourseSubcategory->find('list',array('fields'=>array('id',$locale),'conditions'=>array('CourseSubcategory.category_id'=>$category_id,'CourseSubcategory.status'=>'1'),'order'=>array('CourseSubcategory.'.$locale =>'ASC')));
			if(!empty($sub_categories))
			{
				foreach($sub_categories as $info=>$val)
				{
					$subs .= '<span class="selectOption sub-cat-sub" value="'.$info.'">'.$val.'</span>';
				}
				$subs.='<span class="selectOption sub-cat-sub" value="0">***others***</span>'; 	
				echo $subs;
				die;
			}
			else
			{
				echo 'no';
			}
			die;
		}
		die;
	}
	
	function find_sl_subcategories()
	{
		$this->loadModel('CourseSlSubcategory');
		
		$locale = $this->Session->read('LocTrain.locale');
		
		$subcategory_id = $_POST['subcategory_id'];
		if(!empty($subcategory_id) && $this->request->is('ajax'))
		{
			$subs='';
			$sub_categories = $this->CourseSlSubcategory->find('list',array('fields'=>array('id',$locale),'conditions'=>array('CourseSlSubcategory.sub_category_id'=>$subcategory_id,'CourseSlSubcategory.status'=>'1'),'order'=>array('CourseSlSubcategory.'.$locale =>'ASC')));
			if(!empty($sub_categories))
			{
				foreach($sub_categories as $info=>$val)
				{
					$subs .= '<span class="selectOption sub-cat-sub" value="'.$info.'">'.$val.'</span>';
				}
				$subs.='<span class="selectOption sub-cat-sub" value="0">***others***</span>'; 	
				echo $subs;
				die;
			}
			else
			{
				echo 'no';
			}
			die;
		}
		die;
	}
	
	function course_watched()
	{
		$this->loadModel('CourseWatch');
		$member_id= ($this->Session->read('LocTrain.id'))?$this->Session->read('LocTrain.id'):0;
		$course_id=convert_uudecode(base64_decode($_POST['course_id']));
		$date=date("Y-m-d H:i:s");
		$watched_before=$this->CourseWatch->find('first',array('conditions'=>array('CourseWatch.course_id'=>$course_id,'CourseWatch.member_id'=>$member_id)));
		if(!empty($watched_before))
		{
			
			$this->CourseWatch->id=$watched_before['CourseWatch']['id'];
			$this->CourseWatch->saveField('date_viewed',strtotime($date));
			echo 'not'; die;
		}
		else
		{
				//echo 'im in no';
			$data=$this->CourseWatch->create();
			$data['CourseWatch']['member_id']= $member_id;
			$data['CourseWatch']['course_id']= $course_id;
			$data['CourseWatch']['date_viewed'] = strtotime($date);
			if($this->CourseWatch->save($data))
			{
				echo 'watched'; die;
			}
		}
		die;
	}

	/*function review_permit()
	{
		$this->loadModel('CourseWatch');
		$this->loadModel('CourseRemark');
		$member_id= $this->Session->read('LocTrain.id');
		$course_id=convert_uudecode(base64_decode($_POST['course_id']));
		$watched_before=$this->CourseWatch->find('first',array('conditions'=>array('CourseWatch.course_id'=>$course_id,'CourseWatch.member_id'=>$member_id)));
		$if_reviewed = $this->CourseRemark->find('count',array('conditions'=>array('CourseRemark.course_id'=>$course_id,'CourseRemark.member_id'=>$member_id)));
		if(!empty($watched_before) && !($if_reviewed))
		{
			echo __('Add a review'); die;
		}
		else
		{
			
			$editRe = $this->CourseRemark->find('first',array('conditions'=>array('CourseRemark.member_id'=>$member_id,'CourseRemark.course_id'=>$course_id),'recursive'=>-1,'fields'=>array('comment','id')));
			echo 'not*&'.$editRe['CourseRemark']['id'].'*&'.$editRe['CourseRemark']['comment']; die;
		}
		die;
	}*/
	
	function review_permit()
	{
		
		$this->loadModel('CourseWatch');
		$this->loadModel('CourseRemark');
		$this->loadModel('Course');
               
		$member_id= $this->Session->read('LocTrain.id');
		$course_id=convert_uudecode(base64_decode($_POST['course_id']));
		$course_data = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),'contain'=>array('CourseLesson'=>array('id','extension'))));
                
		$if_reviewed = $this->CourseRemark->find('count',array('conditions'=>array('CourseRemark.course_id'=>$course_id,'CourseRemark.member_id'=>$member_id)));
               if(isset($course_data['CourseLesson'][0]['extension']) && in_array($course_data['CourseLesson'][0]['extension'],array('swf','zip')))
		{
			if(!($if_reviewed))
			{
				echo __('add_a_review'); die;
			}
			else
			{
			
				$editRe = $this->CourseRemark->find('first',array('conditions'=>array('CourseRemark.member_id'=>$member_id,'CourseRemark.course_id'=>$course_id),'recursive'=>-1,'fields'=>array('comment','id')));
				echo 'not*&'.$editRe['CourseRemark']['id'].'*&'.$editRe['CourseRemark']['comment']; die;
			}
			
		}else {
		
			$watched_before=$this->CourseWatch->find('first',array('conditions'=>array('CourseWatch.course_id'=>$course_id,'CourseWatch.member_id'=>$member_id)));

			if(!empty($watched_before) && !($if_reviewed))
			{
				echo __('add_a_review'); die;
			}
			else
			{
				
				$editRe = $this->CourseRemark->find('first',array('conditions'=>array('CourseRemark.member_id'=>$member_id,'CourseRemark.course_id'=>$course_id),'recursive'=>-1,'fields'=>array('comment','id')));
				echo 'not*&'.$editRe['CourseRemark']['id'].'*&'.$editRe['CourseRemark']['comment']; die;
			}
			
		}
		die;
	}
	
	function is_course_subscribed()
	{
		$course_id = convert_uudecode(base64_decode($_POST['cors_id']));
		$lesson_id = convert_uudecode(base64_decode($_POST['less_id']));
		$m_id = $this->Session->read('LocTrain.id');
		$this->loadModel('CoursePurchaseList');
		$this->loadModel('MemberSubscription');
		$this->loadModel('Course');
		$this->Course->id=$course_id;
		$member_id=$this->Course->field('m_id');
		$date = date("Y-m-d H:i:s");
		$subscription = $this->CoursePurchaseList->find('count',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>$lesson_id,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0),array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0)))));
		$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired <='=>strtotime($date))));
		
		if($m_id==$member_id)
		{
			echo 1; die;
		}
		else
		{
			if($subscribed)
			{
				echo $subscribed; die;
			}
			else
			{
				echo $subscription; die;
			}
		}
		die;
	}
	
	function give_rating()
	{
		$this->loadModel('CourseRating');
		$this->loadModel('Course');
		$course_id = convert_uudecode(base64_decode($_POST['course_id']));
		$rating = trim($_POST['rating']);
		
		if($this->Session->read('LocTrain.id'))
		{
			
			$mem_id = $this->Session->read('LocTrain.id');
			$is_reviewed = $this->CourseRating->find('first',array('conditions'=>array('CourseRating.member_id'=>$mem_id,'CourseRating.course_id'=>$course_id)));
			if(!empty($is_reviewed))
			{
				$review = $this->CourseRating->create();
				$this->CourseRating->id=$is_reviewed['CourseRating']['id'];
				$this->CourseRating->saveField('rating',$rating);
				
				$all_ratings=$this->CourseRating->find('all', array('conditions'=>array('CourseRating.course_id'=>$course_id)));
				if(!empty($all_ratings))
				{
					$rate=0;
					$no_recs=0;
					foreach($all_ratings as $ratings)
					{
						$rate+=$ratings['CourseRating']["rating"];
						$no_recs++;
					}
					$avg = $rate/$no_recs;
					$this->Course->id= $course_id;
					$this->Course->saveField('rating',$avg);	
				}
				
				if(strpos($rating, ".") == false) {
				printf(ngettext("%d ".__("rating"), "%d ".__("ratings"),$rating),$rating); die();
				}else{				
				printf(ngettext("%1\$.1f ".__("rating"), "%1\$.1f ".__("ratings"),$rating),$rating); die();
				}
				//echo "updated"; die;
			}
			else
			{
				$review = $this->CourseRating->create();
				$review['CourseRating']['member_id'] = $mem_id;
				$review['CourseRating']['course_id'] = $course_id;
				$review['CourseRating']['rating'] = $rating;
				$this->CourseRating->save($review);
				
				
				$all_ratings=$this->CourseRating->find('all', array('conditions'=>array('CourseRating.course_id'=>$course_id)));
				if(!empty($all_ratings))
				{
					$rate=0;
					$no_recs=0;
					foreach($all_ratings as $ratings)
					{
						$rate+=$ratings['CourseRating']["rating"];
						$no_recs++;
					}
					$avg = $rate/$no_recs;
					$this->Course->id= $course_id;
					$this->Course->saveField('rating',$avg);	
				}
			
				if(strpos($rating, ".") == false) {
				printf(ngettext("%d ".__("rating"), "%d ".__("ratings"),$rating),$rating); die();
				}else{				
				printf(ngettext("%1\$.1f ".__("rating"), "%1\$.1f ".__("ratings"),$rating),$rating); die();
				}
				
				//echo "inserted"; die;
				
				//printf(ngettext("%d ".__("rating"), "%d ".__("ratings"),$rating),$rating); die;
			}	
		}
		else
		{
			echo "no_login"; die;
		}
		echo "error"; die;
	}
	
	function report_review()
	{
		$rid = $_POST['rid'];
		if(!empty($rid) && $this->request->is('ajax'))
		{
			$this->loadModel('CourseRemark');
			$this->loadModel('RemarkReport');
			
			$rid = convert_uudecode(base64_decode($rid));
			$this->CourseRemark->id = $rid;
			$this->CourseRemark->saveField('report',1);
			
			$data['RemarkReport']['member_id'] = $this->Session->read('LocTrain.id');
			$data['RemarkReport']['course_remark_id'] = $rid;
			$data['RemarkReport']['date'] = date('Y-m-d H:i:s');
			$this->RemarkReport->save($data);
			
			echo "done";
		}
		die;
	}
	
	function find_discount()
	{
		$discount_code = $_POST['discount_code'];
		$this->loadModel('Discount');
		$result=$this->Discount->find('first',array('conditions'=>array('Discount.discount_code'=>$discount_code)));
		if(!empty($result))
		{
			$discount=$result['Discount']['initial_value'];
			echo $discount; die;
		}
		else
		{
			echo __("invalid_code"); die;
		}
		die;
	}
  function round_discount()
	{
		$discount = $_POST['discount'];
		$price=$_POST['price'];
		$netprice=($discount*$price)/100;
		echo round($netprice,2);
		die;
	}
	function like_course()
	{
		$this->loadModel('CourseLike');
		$this->loadModel('Member');
		$this->loadModel('Course');
		$this->loadModel('Notification');
		$locale_id = $this->Session->read('LocTrain.LangId');

		
		$member_id= $this->Session->read('LocTrain.id');
		$course_id=convert_uudecode(base64_decode($_POST['course_id']));
		
		$course_owner = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),'fields'=>array('id','m_id'),'contain'=>false));
		
		$course_owner_id = $course_owner['Course']['m_id'];
		
		$course_owner_name = $this->Member->find('first',array('conditions'=>array('Member.id'=>$course_owner_id),'fields'=>array('given_name','id'),'contain'=>false));
		
		$liked_before=$this->CourseLike->find('first',array('conditions'=>array('CourseLike.course_id'=>$course_id,'CourseLike.member_id'=>$member_id)));
		if(!empty($liked_before))
		{ 
			$remove_id=$liked_before['CourseLike']['id'];
			
			$this->CourseLike->delete($remove_id);
			
				
				//*** Notification for member *****
					$this->before_update_notification();
					$notification['Notification']['mem_id'] =  $member_id;
					$notification['Notification']['notification_type'] = 'unlike';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['c_id'] = $course_id;
					$notification['Notification']['notification_sender_id'] = $course_owner_name['Member']['id'];
					$this->Notification->create();
					$this->Notification->save($notification);
			
				
			echo __('like'); die;
		}
		else
		{ 
			
			
			$data=$this->CourseLike->create();
			$data['CourseLike']['member_id']= $member_id;
			$data['CourseLike']['course_id']= $course_id;
			
			
			/*$id = $this->Notification->find('first',array('conditions'=>array('Notification.mem_id'=>$member_id,'Notification.mem_id'=>$c_id,'Notification.notification_type'=>'like'),'fields'=>array('id')));
			pr($id);*/
			
				
			if($this->CourseLike->save($data))
			{ 
				//*** Notification for member *****
					$this->before_update_notification();
					$notification['Notification']['mem_id'] =  $member_id;
					$notification['Notification']['notification_type'] = 'like';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['c_id'] = $course_id;
					$notification['Notification']['notification_sender_id'] = $course_owner_name['Member']['id'];
					$this->Notification->create();
					$this->Notification->save($notification);
				echo __('unlike'); die;
				
			}

		}
		die;
		
	}
	
	function report_remark()                          //report a review/remark
	{
		$this->loadModel('RemarkReport');
		$member_id= $this->Session->read('LocTrain.id');
		$remark_id=convert_uudecode(base64_decode($_POST['remark_id']));
		$reason=$_POST['reason'];
		$reported_before=$this->RemarkReport->find('first',array('conditions'=>array('RemarkReport.remark_id'=>$remark_id,'RemarkReport.member_id'=>$member_id)));
		if(empty($reported_before))
		{
			$data=$this->RemarkReport->create();
			$data['RemarkReport']['member_id']= $member_id;
			$data['RemarkReport']['remark_id']= $remark_id;
			$data['RemarkReport']['reason']= $reason;
			if($this->RemarkReport->save($data))
			{
				echo 'reported'; die;
			}
		}
		else
		{
			echo "You have already reported this review.";
		}
		die;
		
	}
	
	function cookie_for_css($type=NULL)
	{
		if($this->Session->read('LocTrain.locale')=="ar")
		{
			if($type == 'style_arabic')
			{
				$this->Cookie->delete('LocTrain.style_arabic');
				$this->Cookie->write('LocTrain.style_arabic','style_arabic',false,60*60*24*365);
				
			}
			else
			{
				$this->Cookie->delete('LocTrain.style_arabic');
				$this->Cookie->write('LocTrain.style_arabic','contrast_arabic',false,60*60*24*365);
			}
			
		}
		else{
			
			if($type == 'style')
			{
				$this->Cookie->delete('LocTrain.style');
				$this->Cookie->write('LocTrain.style','style',false,60*60*24*365);
				
			}
			else
			{
				$this->Cookie->delete('LocTrain.style');
				$this->Cookie->write('LocTrain.style','contrast',false,60*60*24*365);
			}
		}
			
		$this->redirect($this->referer());
	}

    function test()
	{
		
	}
	
	function reactivate_account_again()
	{
		//echo "hi";die;
		$this->loadModel('MemberSubscription');
		$this->loadModel('CancelReason');
		$this->loadModel('UserReasons');
		
		$mid= $this->Session->read('LocTrain.id');
		$m_data=$this->MemberSubscription->find('first',array('conditions'=>array('MemberSubscription.m_id'=>$mid,'MemberSubscription.status'=>'1'),'order'=>array('MemberSubscription.id'=>'desc'),'fields'=>array('date_expired','status','cancel_request','id')));
		$tem_id=$m_data['MemberSubscription']['id'];
		$this->MemberSubscription->updateAll(array('MemberSubscription.cancel_request'=>'0'),array('MemberSubscription.id'=>$tem_id));
		$reason=$this->CancelReason->find('first',array('conditions'=>array('CancelReason.m_id'=>$mid),'order'=>array('CancelReason.id'=>'desc'),'fields'=>array('CancelReason.id')));
		$r_id=$reason['CancelReason']['id'];
		$this->UserReasons->deleteAll(array('UserReasons.cancel_reason_id'=>$r_id));
		$this->CancelReason->deleteAll(array('CancelReason.m_id'=>$mid));
		$this->before_update_notification();
		$this->loadModel('Notification');
		$notification['Notification']['mem_id'] =  0;
		$notification['Notification']['notification_type'] = 'subsptn_account_reactivate';
		$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
		$notification['Notification']['notification_sender_id'] = $mid;
		$this->Notification->create();
		$this->Notification->save($notification);

		//$this->Session->write('LocTrain.flashMsg','Your Account has been Activated successfully.');
		$this->redirect(array('controller'=>'Members','action'=>'update_profile'));
	}
	
	function error()
	{
		$this->layout =	'public';
	} 
	function view_free_video($id=NULL)
	{
		
		$this->layout =	'public';
		$this->loadModel('FreeVideo');
		$this->loadModel('Member');
		$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
		$this->set('locale',$locale);
		
		//************** TEsting for JW player *************
		
		$mem_acc_number = $this->Member->find('first',array('conditions'=>array('Member.id'=>$this->Session->read('LocTrain.id')),'fields'=>array('mem_acc_no'),'contain'=>false));
		if(!empty($id))
		{
			$decoded_id = @convert_uudecode(base64_decode($id));
		}
		
		$mem_acc_no = '';
		
		if(!empty($mem_acc_number))
		{
				$mem_acc_no = 	$mem_acc_number['Member']['mem_acc_no'];
		}		
		$this->set('mem_acc_no',$mem_acc_no);
		
		//************** TEsting for JW player *************
		
		if(!empty($id))
		{
			$decoded_id = $decoded_id;
			//pr($id);die;
			$free_video_detail = $this->FreeVideo->find('first',array('conditions'=>array('FreeVideo.id'=>$decoded_id,'FreeVideo.status'=>1),'fields'=>array('title_'.$locale,'free_video_url','id','own_video_url','description_'.$locale,'viewer')));
		}
		$this->set('free_video_detail',$free_video_detail);
	}
	function play_free_video($id=NULL)
	{
		$video_id = @convert_uudecode(base64_decode($id));
		$this->loadModel('FreeVideo');
		$freeVideo = $this->FreeVideo->find('first',array('conditions'=>array('FreeVideo.id'=>$video_id),'fields'=>array('own_video_url','extension','status')));
		$video = $freeVideo['FreeVideo']['own_video_url'];
		
		if(!empty($video))
		{
			$res = array('status'=>'true','video'=>$video,'extension'=>$freeVideo['FreeVideo']['extension']);
			///pr($res);die;
			echo json_encode($res);
		}
		else
		{
			$res = array('status'=>'nofreevideo','error'=>__('No video available'));
			echo json_encode($res);
		}
		die;
	}
	function watch_free_video($id=NULL)
	{
		$video_id = @convert_uudecode(base64_decode($id));
		$m_id = $this->Session->read('LocTrain.id');
		if(!empty($m_id))
		{
			$this->loadModel('WatchFreeVideo');
			$isWatched = $this->WatchFreeVideo->find('count',array('conditions'=>array('WatchFreeVideo.free_video_id'=>$video_id,'WatchFreeVideo.mem_id'=>$m_id)));
			if($isWatched >= 1)
			{
				$res = 'Already watched';
				echo json_encode($res);
			}
			else
			{
				$data['WatchFreeVideo']['mem_id'] = $m_id;
				$data['WatchFreeVideo']['free_video_id'] = $video_id;
				$data['WatchFreeVideo']['watch_date'] = strtotime(date('Y-m-d H:i:s'));
				$this->WatchFreeVideo->create();
				
				if($this->WatchFreeVideo->save($data))
				{
				
					$this->loadModel('FreeVideo');
					$viewer = $this->FreeVideo->find('first',array('conditions'=>array('FreeVideo.id'=>$video_id),'fields'=>array('viewer')));
			
					if(!empty($viewer))
					{
						$this->loadModel('FreeVideo');
						$viewer = $viewer['FreeVideo']['viewer'];
						$viewer++;
						$this->FreeVideo->updateAll(array('FreeVideo.viewer'=>$viewer),array('FreeVideo.id'=>$video_id));
					}
					$res = 'watched';
					echo json_encode($res);
				}
			}
		}
		else
		{
			echo json_encode('');
		}
		die;
		
	}
	 
	function block_video()
	{
		$this->layout =	'public';
		$this->loadModel('Block video');
	}
	
	function delete_notify()
	{ 	
		$this->loadModel('Notification');
		$n_id= $_POST['id'];
		$dataid= $_POST['dataid']+1;
		$mem_id = $this->Session->read('LocTrain.id');
		$msg="";
		if(!empty($n_id) && $this->request->is('ajax'))
		{	
			$n_id=convert_uudecode(base64_decode($n_id));
			
			$this->Notification->delete($n_id);
			if($this->Session->read('LocTrain.login')==1)
			{
				$conditions = array('Notification.mem_id !='=>$mem_id,'Notification.notification_sender_id'=>$mem_id);
			}
			else
			{
				$temp_id =$this->Session->read('LocTrain.visitor_id');
				$conditions = array('Notification.notification_sender_id'=>$temp_id);
			}	
		
			$notification=$this->Notification->find('all',array('conditions'=>$conditions,'limit'=>5,'order'=>'Notification.id DESC'));
			
			$notification_count = count($notification);
			$countnotification=$this->Notification->find('all',array('conditions'=>$conditions,'order'=>'Notification.id DESC'));
			$heading="";
			$n_text="";
			if($notification_count==5)
			{
				$notification_id = $notification[4]['Notification']['id'];
				if($notification[4]['Notification']['notification_type']=='like' && !empty($notification[4]['CourseLesson']['id']))
						{
							$heading=__('like_lesson');
														
							$n_text = String::insert(__('member_liked_your_lesson_lesson'),array('member' => $notification[4]['Member']['given_name'],'course' => $notification[4]['CourseLesson']['title']));
							
							//pr($n_text);die;
						}
						else if($notification[4]['Notification']['notification_type']=='like' && empty($notification[4]['CourseLesson']['id']))
						{
							$heading=__('like_course');
							
							
							$n_text = String::insert(__('member_liked_your_course_course'),array('member' => $notification[4]['Member']['given_name'],'course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='unlike' && !empty($notification[4]['CourseLesson']['id']))
						{
							$heading=__('unlike_lesson');
														
							$n_text = String::insert(__('member_unliked_your_lesson_lesson'),array('member' => $notification[4]['Member']['given_name'],'lesson' => $notification[4]['CourseLesson']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='unlike' && empty($notification[4]['CourseLesson']['id']))
						{
							$heading= __('unlike_course');
							
							
							$n_text = String::insert(__('member_unliked_your_course_course'),array('member' => $notification[4]['Member']['given_name'],'course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='course_purchased')
						{
							$heading= __('course_purchased');
							
							$n_text = String::insert(__('member_has_purchased_your_course_course'),array('member' => $notification[4]['Member']['given_name'],'course' => $notification[4]['Course']['title']));
							
						}
						else if($notification[4]['Notification']['notification_type']=='lesson_purchased')
						{
							$heading= __('lesson_purchased');
							$n_text = String::insert(__('member_has_purchased_your_lesson_lesson'),array('member' => $notification[4]['Member']['given_name'],'lesson' => $notification[4]['CourseLesson']['title']));						
							
						}
						else if($notification[4]['Notification']['notification_type']=='become_author')
						{
							$heading= __('author_request_accepted');
							$n_text=__('you_have_been_accepted_as_an_author');
						}
						else if($notification[4]['Notification']['notification_type']=='reject_author')
						{
							$heading= __('author_request_rejected');
							$n_text=__('your_request_to_become_an_author_has_been_rejected');
						}
						else if($notification[4]['Notification']['notification_type']=='become_member')
						{
							$heading= __('member');
							$n_text=__('you_have_been_become_a_member');
						}
						else if($notification[4]['Notification']['notification_type']=='course_saved')
						{
							$heading= __('course_saved');
							$n_text = String::insert(__('course_saved_'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='delete_lesson')
						{
							$heading= __('delete_lesson');
							$n_text=__('lesson_deleted');
						}
						else if($notification[4]['Notification']['notification_type']=='video_is_being_deleted')
						{
							$heading= __('video_is_being_deleted');
							$n_text=__('video_is_being_deleted');
						}
						else if($notification[4]['Notification']['notification_type']=='retire_course')
						{
							$heading= __('retire_course');
							$n_text = String::insert(__('course_is_retired'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='course_update')
						{
							$heading= __('course_updated');
							
							$n_text = String::insert(__('course_updated_'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='lesson_save')
						{
							$heading= __('lesson_saved');
							
							 $n_text = String::insert(__(':lesson has been saved. You will receive an e-mail when it is ready to be viewed.'),array('lesson' => $notification[4]['CourseLesson']['title']));

							
						}else if($notification[4]['Notification']['notification_type']=='upload_progress')
						{
							$heading= __('upload_progress');
							
							 $n_text = String::insert(__("lesson_Video_upload_is_in_process_we_will_let_you_know_once_the_video_has_been_successfully_uploaded"),array('lesson' => $notification[4]['CourseLesson']['title']));

							
						}
						else if($notification[4]['Notification']['notification_type']=='lesson_update')
						{
							$heading= __('lesson_updated');
							
							$n_text = String::insert(__('lesson_has_been_updated'),array('lesson' => $notification[4]['CourseLesson']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='update_profile')
						{
							$heading= __('profile_updated');
							$n_text=__('profile_updated');
						}
						else if($notification[4]['Notification']['notification_type']=='password_changed')
						{
							$heading= __('password_changed');
							$n_text=__('your_password_has_been_changed');
						}
						else if($notification[4]['Notification']['notification_type']=='request_course_author')
						{
							$heading= __('author_request');
							$n_text=__('your_request_to_become_an_author_has_been_sent_to_a_membership_manager_for_review');
						}
						else if($notification[4]['Notification']['notification_type']=='already_author')
						{
							$heading= __('already_author');
							$n_text=__('you_are_an_author_already');
						}
						else if($notification[4]['Notification']['notification_type']=='add_course_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_has_been_added_to_your_shopping_basket'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='course_not_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_was_not_added_to_the_shopping_basket_please_try_again'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='lesson_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							$n_text = String::insert(__('lesson_has_been_added_to_your_shopping_basket'),array('lesson' => $notification[4]['CourseLesson']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='lesson_present_shopping_basket')
						{
							
								if($notification[4]['Notification']['c_id']==NULL)
							{
									$heading= __('shopping_basket');
							
							$n_text = String::insert(__('a_course_in_the_shopping_basket_already_contains_lesson'),array('lesson' => $notification[4]['CourseLesson']['title']));
							}
							else
							{
								$heading= __('shopping_basket'); 
							
							$n_text = String::insert(__('a_course_in_the_shopping_basket_already_contains_lesson'),array('lesson' => $notification[4]['Course']['title']));
							}
							
							
						}
						else if($notification[4]['Notification']['notification_type']=='lesson_not_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('lesson_was_not_added_to_the_shopping_basket_please_try_again'),array('lesson' => $notification[4]['CourseLesson']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='course_removed_shopping_basket' && !empty($notification[4]['CourseLesson']['id']) )
						{
							$heading= __('shopping_basket');
							$n_text = String::insert(__('lesson_was_removed_from_the_shopping_basket'),array('lesson' => $notification[4]['CourseLesson']['title']));
							
							
						}
						else if($notification[4]['Notification']['notification_type']=='course_removed_shopping_basket' && !empty($notification[4]['Course']['id']) )
						{
							$heading= __('shopping_basket');
							$n_text = String::insert(__('course_was_removed_from_the_shopping_basket'),array('course' => $notification[4]['Course']['title']));
							
							
						}
						else if($notification[4]['Notification']['notification_type']=='course_not_removed_shopping_basket' && !empty($notification[4]['CourseLesson']['id']))
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('lesson_not_removed_from_cart_please_try_again'),array('lesson' => $notification[4]['CourseLesson']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='course_not_removed_shopping_basket' && empty($notification[4]['CourseLesson']['id']))
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_was_not_removed_from_the_shopping_basket_please_try_again'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='course_added_learning_plan')
						{
							$heading= __('Learning Plan');
							
							$n_text = String::insert(__('course_was_added_to_your_learning_plan'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='course_not_added_learning_plan')
						{
							$heading= __('Learning Plan');
						
							$n_text = String::insert(__('course_was_not_added_to_your_learning_plan_please_try_again'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='already_added_learning_plan')
						{
							$heading= __('learning_plan');
							
							$n_text = String::insert(__('course_is_already_in_your_learning_plan'),array('course' => $notification[4]['Course']['title']));
						}
						else if($notification[4]['Notification']['notification_type']=='review')
						{
							$heading= __('review');
							$n_text=__('thank_you_for_your_review');
						}
						else if($notification[4]['Notification']['notification_type']=='course_not_purchased')
						{
							$heading= __('course_not_purchased');
							$n_text=__('the_order_has_been_cancelled_and_you_have_not_been_charged');
						}
						else if($notification[4]['Notification']['notification_type']=='course_purchased_sucessfully')
						{
							$heading= __('your_transaction_is_complete');
							$n_text=__('a_confirmation_message_has_been_sent_to_your_registered_e-mail_address');
						}
						else if($notification[4]['Notification']['notification_type']=='subsptn_account_reactivate')
						{
							$heading= __('account_activation');
							$n_text=__('your_account_has_been_activated_successfully');
						}
						else if($notification[4]['Notification']['notification_type']=='subscription_success')
						{
							$heading= __('your_subscription_is_complete');
							$n_text=__('a_confirmation_message_has_been_sent_to_your_registered_e-mail_address');
						}
						else if($notification[4]['Notification']['notification_type']=='subscription_failure')
						{
							$heading= __('subscription_not_purchased');
							$n_text=__('the_order_has_been_cancelled_and_you_have_not_been_charged');
						}
						else if($notification[4]['Notification']['notification_type']=='member_not_login')
						{
							$heading= __('access_denied');
							$n_text=__('account_reactivated');
						}	
						else if($notification[4]['Notification']['notification_type']=='account_activation')
						{
							$heading= __('account_activation');
							$n_text=__('your_account_has_been_activated_you_may_change_your_membership_details_any_time_by_selecting_your_e-mail_address_in_the_header');
						}
						else if($notification[4]['Notification']['notification_type']=='account_reactivate')
						{
							$heading= __('account_reactivate');
							$n_text=__('account_has_been_reactivated');
						}
                        else if($notification[4]['Notification']['notification_type']=='permission_denied')
						{
							$heading= __('permission_denied');
							$n_text=__('permission_denied');
						}
                        else if($notification[4]['Notification']['notification_type']=='account_cancel_success')
						{
							$heading= __('account_cancelled');
							$n_text=__('your_account_has_been_cancelled_but_we_will_be_happy_to_see_you_again');
						}
						
						else if($notification[4]['Notification']['notification_type']=='unable_account_activation')
						{
							$heading= __('unable_to_activate_membership');
							$n_text=__('the_activate_my_membership_link_has_already_been_used_or_it_has_passed_its_expiry_date_and_time_if_your_membership_has_already_been_activated_please_sign_in_using_your_e-mail_address_and_the_password_you_entered_during_registration_if_your_membership_was_not_activated_within_24-hours_of_registration_you_will_need_to_register_again');
						}
						
						
			
				    $msg .='<li class="item js-item " data-id="'.$dataid.'"><div class="details"><span class="title">'.$heading.'</span><span class="date">'.$n_text.'</span></div><button type="button" class="button-default button-dismiss js-dismiss delete_notify">×</button> <input type="hidden" value="'.base64_encode(convert_uuencode($notification_id)).'" class="n_id '.$notification_id.'" /></div>';
						
				$result = array('process'=>'save','msg'=>$msg,'n_id'=>$n_id,'condition'=>$notification);
				
					$result = array_merge(array('countN'=>count($countnotification)),$result);
				echo json_encode($result);
				
			}else if($notification_count < 5 && $notification_count > 0)
			{ 
				$msg = ''   ;
				$result = array('process'=>'last_record','msg'=>$msg,'n_id'=>$n_id);		
					$result = array_merge(array('countN'=>count($countnotification)),$result);
				echo json_encode($result);
			}
			else if($notification_count==0)
			{
				//$msg='<li class="item no-data">'.__('no_messages').'</li>';
				$msg = ''   ;		
				$result = array('process'=>'last','msg'=>$msg);		
				
				$result = array_merge(array('countN'=>count($countnotification)),$result);
				echo json_encode($result);
			}
		}	
		die;
	
	}
        function edit_reviews()
	{
		if(!empty($this->data))
		{
			if(!empty($this->data['CourseRemark']['comment']))
			{
			   if(strlen($this->data['CourseRemark']['comment'])<=1024)
			   {
					$course_id = $this->data['CourseRemark']['course_id'];
					$this->request->data['CourseRemark']['member_id']=$this->Session->read("LocTrain.id");
					$this->request->data['CourseRemark']['date_added']=strtotime(date('Y-m-d H:i:s'));
					$this->request->data['CourseRemark']['block']=0;
					$this->request->data['CourseRemark']['promote']=0;
					$this->request->data['CourseRemark']['report']=0;
					$this->loadModel('CourseRemark');
					if($this->CourseRemark->save($this->data))
					{					  					  
						$resp=array('status'=>'ok','msg'=>'Edit Review successfully.');
						
					}
			   }
			   else
			   {
			   	 $resp=array('status'=>'error','msg'=>__('Comment has exceeded 1024 characters'));
			   }
				
			}
			else
			{
			  $resp=array('status'=>'error','msg'=>__('this_field_is_required'));	
			}
		}
		echo json_encode($resp);die;
	}


function test_vedio($id=NULL)
	{		
		
		
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->set(compact('title_for_layout'));
		
	}

        function extzip($file=null)
		{				
	 	$path = realpath('../../app/webroot/files/members/'.$this->Session->read('LocTrain.id').'/'.$file);
        $extract_path = realpath('../../app/webroot/files/members/'.$this->Session->read('LocTrain.id').'/');

		if($file)
		{
			
			$zip = new ZipArchive;
			$res = $zip->open($path);
			if ($res === TRUE) { 
				$p = realpath('../../app/webroot/files/members/'.$this->Session->read('LocTrain.id'));
				$f = str_replace('.zip','',$file);
				mkdir($p.'/'.$f,0777);
				$extract_path = realpath('../../app/webroot/files/members/'.$this->Session->read('LocTrain.id').'/'.$f.'/');
				$zip->extractTo($extract_path ,null);
				$zip->close();
				unlink($path);
				
				echo 'Success';  die;				
			} else {
				die('error:Some error occurred. Try again.');
			}
		}
		else
		{
			die('error:Some error occurred. Try again.');
		}
           die;
	}
//** this for upload html5 vedio s to aws server set as a cron***
	function cron_upload_video($file=null)
	{	
		//get all vedios to upload rom course_lesson where upload_zip_status=1 'order'=>'Notification.id DESC'
		$this->loadModel('CourseLesson');
		$all_lesson_deatils =	$this->CourseLesson->find('all',array('conditions'=>array('CourseLesson.upload_zip_status'=>1),'limit'=>2,'order'=>'CourseLesson.date_modified ASC','contain'=>array('Course')));
          			//print_r($all_lesson_deatils); exit;
			if(!empty($all_lesson_deatils)){
				foreach($all_lesson_deatils as $all_lesson_deatils){
					$file=$all_lesson_deatils['CourseLesson']['video'].'.'.$all_lesson_deatils['CourseLesson']['extension'];
					$mem_id=$all_lesson_deatils['Course']['m_id'];
					if($file)
					{
						$path = WWW_ROOT.'files/members/'.$mem_id.'/'.$file;
						$extract_path = WWW_ROOT.'files/members/'.$mem_id.'/';
					
					//import aws component
					App::import('Component', 'Aws');
					$this->Aws = new AwsComponent();
					$f = str_replace('.zip','',$file);
					$iter = new RecursiveDirectoryIterator(WWW_ROOT.'files/members/'.$mem_id.'/'.$f);
					foreach (new RecursiveIteratorIterator($iter) as $fileName => $fileInfo) {
						$info = new SplFileInfo($fileInfo);
						if(!empty($info->getExtension()))
						{
							$extense=	explode('/'.$mem_id,$fileInfo);
							$extense1=explode("../",$fileInfo);
							//echo '-'.$mem_id.$extense[1].'<br>';
							//echo $mem_id.$extense[1];
							$data=$this->Aws->uploadFile($mem_id.$extense[1],LESSON_VIDEOS_BUCKET, WWW_ROOT.'files/members/'.$mem_id.$extense[1]);
						}
					}

					$path_f= WWW_ROOT.'files/members/'.$mem_id.'/'.$f.'/';
					$vedio_path_file= WWW_ROOT.'video.html';
					$files = glob($path_f.'*.html');
					$html_files=array();
					if(!empty($files)){
						foreach($files as $file){
						$html_files[]=str_replace($path_f,'',$file);
						}
					}
					if(!empty($html_files)){
						$src='';
						if(in_array('index.html',$html_files)){
						$src='index.html';						
						}else if(in_array('index_SCORM.html',$html_files)){
						$src='index_SCORM.html';					
						}else if(in_array('index_TINCAN.html',$html_files)){
						$src='index_TINCAN.html';					
						}else if(in_array('story.html',$html_files)){
						$src='story.html';					
						}else if(in_array('presenter.html',$html_files)){
						$src='presenter.html';					
						}else{}
						//echo $src;
						if(!empty($src)){
						$msg='<!DOCTYPE html><html><head><meta name="viewport" content="minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0,user-scalable=no" /></head><body><iframe type="text/html" width="100%" height="600px" id="iframe_2" border="0" style="border:none;" src="'.$src.'"  allowfullscreen webkitallowfullscreen mozallowfullscreen ></iframe><script type="text/javascript">
(function(w, s, d) {  var timer;  var screen_height_info = d.getElementById("iframe_2");var mode=0; var OSName="";if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";var isOpera = !!window.opera || navigator.userAgent.indexOf("Opera") >= 0;    var isFirefox = typeof InstallTrigger !== "undefined"; var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor") > 0;    var isChrome = !!window.chrome;    var isIE = /*@cc_on!@*/false; function detect_full_screen() {    clearInterval(timer);    var d_width = d.documentElement.clientWidth || 0;    var d_height = d.documentElement.clientHeight || 0;    var w_width = w.innerWidth || 0;    var w_height = w.outerHeight || 0;var d_w_height = w.screen.height || 0;    var s_width = s.width || 0;    var s_height = s.height || 0;    if ((w_width === s_width && w_height === s_height) || (d_width === s_width && d_height === s_height)) {      
	mode=1;    }    else {	mode=0;   }if(screen_height_info != null){if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {if(mode==1 ){	document.getElementById("iframe_2").style.height=(d_w_height-200)+"px";	}else{	document.getElementById("iframe_2").style.height=(d_w_height-200)+"px";	}}else{if(mode==1 ){	document.getElementById("iframe_2").style.height=s_height+"px";	}else{
	  if(OSName == "MacOS" && isChrome == true){  document.getElementById("iframe_2").style.height=(s_height-230)+"px"; }else{document.getElementById("iframe_2").style.height=(s_height-200)+"px";}	}}	} readDeviceOrientation();}  
function readDeviceOrientation() {  	
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {			              		
		    if (Math.abs(window.orientation) === 90) {
			/* Landscape */
				document.getElementById("iframe_2").style.height=(w.screen.height-200)+"px";	
		    } else {
		    	/* Portrait  */
			document.getElementById("iframe_2").style.height=(w.screen.height-350)+"px";
		    }
}
		}
detect_full_screen();  function bridge() {    clearInterval(timer);    timer = setInterval(detect_full_screen, 100);  }  if (w.addEventListener) {    w.addEventListener("resize", bridge, false);  }  else if (w.attachEvent) {    w.attachEvent("onresize", bridge);  }  else {    w.onresize = bridge;  }})(this, this.screen, this.document);
</script></body></html>';
						if(file_exists($vedio_path_file)){
							 file_put_contents($vedio_path_file,$msg);
							}
						$this->Aws->uploadFile($mem_id.'/'.$f.'/video.html',LESSON_VIDEOS_BUCKET,$vedio_path_file);	
						}
					}
					
					
						//update the record in database with upload_zip_status=2
						$this->CourseLesson->id=$all_lesson_deatils['CourseLesson']['id'];
						if($this->CourseLesson->saveField('upload_zip_status',2)){
						//remove the video			
						if(file_exists(WWW_ROOT.'files/members/'.$mem_id."/".$all_lesson_deatils['CourseLesson']['video']))
							{ 
								$path = WWW_ROOT.'files/members/'.$mem_id."/".$all_lesson_deatils['CourseLesson']['video'];                          
								exec('rm -r '.$path);
							}
						}
						//send mail to user that vedio is ready
					$lesson_title = $all_lesson_deatils['CourseLesson']['title'];
					$this->loadModel('Member');
					$mem_info = $this->Member->find('first',array('conditions'=>array('Member.id'=>$mem_id),'fields'=>array('given_name','email','locale'),'contain'=>false));
					$video_title=$all_lesson_deatils['CourseLesson']['video'].'.'.$all_lesson_deatils['CourseLesson']['extension'];
					$final_name = $mem_info['Member']['given_name'];
					$email = $mem_info['Member']['email'];     
					$locale= $mem_info['Member']['locale'];      
					$course_title = $all_lesson_deatils['Course']['title'];
					$this->loadModel('EmailTemplate');
					$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>17,'locale'=>$locale),'fields'=>array('subject','description','email_from')));
					$emailData = $emailTemplate['EmailTemplate']['description'];
					$emailTo = $email;
					
					$emailData = str_replace(array('{final_name}','{lesson_title}','{course_title}','{video_title}'),array($final_name,$lesson_title,$course_title,$video_title),$emailData);
					$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
					$emailSubject = $emailTemplate['EmailTemplate']['subject'];
					
					$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
						echo 'sucess';	
						
					
				}
				}			
			}else{
				echo 'No vedios find to upload';
				}
           die;
	}
	function url_exists($url) {
    if (!$fp = curl_init($url)) return false;
    return true;
	}
	//** this for delete html5 vedio from aws server set as a cron***
	function cron_delete_video($file=null)
	{
		//get all vedios to upload rom course_lesson where delete_zip_status=1
		$this->loadModel('CourseLesson');
		$this->loadModel('Course');
		App::import('Helper', 'Utility'); 
		$this->Utility = new UtilityHelper(new View(null));
		
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();
		
		$all_lesson_deatils =	$this->CourseLesson->find('all',array('conditions'=>array('CourseLesson.delete_zip_status'=>array(1,2)),'limit'=>2,'contain'=>array('Course')));
		//$all_lesson_deatils =	$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>1381),'contain'=>array('Course')));
		//print_r($all_lesson_deatils);
		if(!empty($all_lesson_deatils)){
			foreach($all_lesson_deatils as $all_lesson_deatils){
				
				$file=$all_lesson_deatils['CourseLesson']['to_delete_video'];
				$f = str_replace('.zip','',$file);		
				$mem_id=$all_lesson_deatils['Course']['m_id'];
				if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$mem_id."/".$f))){
					$prefix=$mem_id."/".$f.'/';
					$result = $this->Aws->listObjects(LESSON_VIDEOS_BUCKET,$prefix);
					//print_r($result);
					if(!empty($result)){
						//delete the filese from aws
						$keys=$result;
						$keys[]=$mem_id."/".$f;
						//print_r($keys);
						$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
					}
				}
				if($all_lesson_deatils['CourseLesson']['delete_zip_status']==1){
					//only update the staus of 'delete_zip_status' to 3 (as files deleted from aws), and empty the value of 'to_delete_video'
					$this->CourseLesson->updateAll(array('CourseLesson.delete_zip_status'=>3,'CourseLesson.to_delete_video'=>NULL),array('CourseLesson.id'=>$all_lesson_deatils['CourseLesson']['id']));
				}else if($all_lesson_deatils['CourseLesson']['delete_zip_status']==2){
					$this->CourseLesson->delete($all_lesson_deatils['CourseLesson']['id']);
				}else{}
				if($all_lesson_deatils['Course']['status']==3){
					$this->Course->delete($all_lesson_deatils['Course']['id']);
				}
				//echo 'sucess';	
			}
			
		}else{
				//echo 'No vedios find to delete';
			}
		die;
	}
	function deleteFiles($keys,$bucket){
	// uploading to AWS server 
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();
		if(!empty($keys)){
			foreach($keys as $key){
				$this->Aws->deleteFile($bucket,$key);
			}
		}	
	}
       function recurse_copy($src)
	{
		
		$src = $src;
                $actual_file = ''; 
		// Open a directory, and read its contents
		if (is_dir($src))
		{
		  if ($dh = opendir($src)){
			while (($file = readdir($dh)) !== false){
			       $actual_file .= str_replace('..','',$file) ; 
 			}	
		   }
		}
                  return  $actual_file; 
      }
      
     function  ext_file($f=NULL ,$mem_id=NULL)
	{
		if(!empty($f) && !empty($mem_id))
		{
			$src = realpath('../../app/webroot/files/members/'.$mem_id.'/'.$f.'/');
			
            $actual_file = ''; 
			// Open a directory, and read its contents
			if (is_dir($src))
			{
			  if ($dh = opendir($src)){
				while (($file = readdir($dh)) !== false){
					   $actual_file .= str_replace('..','',$file) ; 
				}	
			   }
			}
            echo  'ok';//str_replace('.','',$actual_file);  
		}
		die;
	}
	
	function check_notification($count=NULL)
	{
		$this->layout="";
		$result=array();
		if($this->request->is('ajax'))
		{
			
			$v=$this->Session->read('LocTrain.visitor_id');
			$this->loadModel('Notification');
			if($this->Session->read('LocTrain.login')==1)
			{
				$mem_id =$this->Session->read('LocTrain.id');
				$conditions = array('Notification.mem_id !='=>$mem_id,'Notification.notification_sender_id'=>$mem_id);
			}
			else{
				$temp_id =$this->Session->read('LocTrain.visitor_id');
				$conditions = array('Notification.notification_sender_id'=>$temp_id); 
			}
			$notification_count=$this->Notification->find('count',array('conditions'=>$conditions,'order'=>'Notification.id DESC'));
			
			
			if($notification_count == $count)
			{
				
				$fetch_record = $notification_count - $count;
				$notification_all = $this->Notification->find('all',array('conditions'=>$conditions,'order'=>'Notification.id DESC','limit'=>'5'));
				/*$notification_all = $this->Notification->find('all',array('conditions'=>array('OR'=>array(array('Notification.notification_sender_id'=>$v),array('Notification.notification_sender_id'=>$this->Session->read('LocTrain.id')))),'order'=>'Notification.id DESC','limit'=>'5'));*/
				
				
				$msg='<li class="item no-data">You don\'t have notifications</li>'; $heading = ''; $n_text='';$i=0;
				if(!empty($notification_all))
				{
					foreach($notification_all as $index=>$notification)
					{
						$notification_id = $notification['Notification']['id'];
						if($notification['Notification']['notification_type']=='like' && !empty($notification['CourseLesson']['id']))
						{
								$heading=__('like_lesson');
															
								$n_text = String::insert(__('member_liked_your_lesson_lesson'),array('member' => $notification['Member']['given_name'],'course' => $notification['CourseLesson']['title']));
								
								//pr($n_text);die;
						}
						else if($notification['Notification']['notification_type']=='like' && empty($notification['CourseLesson']['id']))
						{
							$heading=__('like_course');
							
							
							$n_text = String::insert(__('member_liked_your_course_course'),array('member' => $notification['Member']['final_name'],'course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='unlike' && !empty($notification['CourseLesson']['id']))
						{
							$heading=__('unlike_lesson');
														
							$n_text = String::insert(__('member_unliked_your_lesson_lesson'),array('member' => $notification['Member']['given_name'],'lesson' => $notification['CourseLesson']['title']));
						}
						else if($notification['Notification']['notification_type']=='unlike' && empty($notification['CourseLesson']['id']))
						{
							$heading= __('unlike_course');
							
							
							$n_text = String::insert(__('member_unliked_your_course_course'),array('member' => $notification['Member']['given_name'],'course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='course_purchased')
						{
							$heading= __('course_purchased');
							
							$n_text = String::insert(__('member_has_purchased_your_course_course'),array('member' => $notification['Member']['given_name'],'course' => $notification['Course']['title']));
							
						}
						else if($notification['Notification']['notification_type']=='lesson_purchased')
						{
							$heading= __('lesson_purchased');
							$n_text = String::insert(__('member_has_purchased_your_lesson_lesson'),array('member' => $notification['Member']['given_name'],'lesson' => $notification['CourseLesson']['title']));						
							
						}
						else if($notification['Notification']['notification_type']=='become_author')
						{
							$heading= __('author_request_accepted');
							$n_text=__('you_have_been_accepted_as_an_author');
						}
						else if($notification['Notification']['notification_type']=='reject_author')
						{
							$heading= __('author_request_rejected');
							$n_text=__('your_request_to_become_an_author_has_been_rejected');
						}
						else if($notification['Notification']['notification_type']=='become_member')
						{
							$heading= __('member');
							$n_text=__('you_have_been_become_a_member');
						}
						else if($notification['Notification']['notification_type']=='course_saved')
						{
							$heading= __('course_saved');
							$n_text = String::insert(__(':course saved.'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='delete_lesson')
						{
							$heading= __('delete_lesson');
							$n_text=__('lesson_deleted');
						}
						else if($notification['Notification']['notification_type']=='video_is_being_deleted')
						{
							$heading= __('video_is_being_deleted');
							$n_text=__('video_is_being_deleted');
						}
						else if($notification['Notification']['notification_type']=='retire_course')
						{
							$heading= __('retire_course');
							$n_text = String::insert(__('course_is_retired'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='course_update')
						{
							$heading= __('course_updated');
							
							$n_text = String::insert(__('course_updated_'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='lesson_save')
						{
							$heading= __('lesson_saved');
							
							 $n_text = String::insert(__('lesson_has_been_saved_you_will_receive_an_e-mail_when_it_is_ready_to_be_viewed'),array('lesson' => $notification['CourseLesson']['title']));

							
						}else if($notification['Notification']['notification_type']=='upload_progress')
						{
							$heading= __('upload_progress');
							
							 $n_text = String::insert(__("lesson_Video_upload_is_in_process_we_will_let_you_know_once_the_video_has_been_successfully_uploaded"),array('lesson' => $notification['CourseLesson']['title']));

							
						}
						else if($notification['Notification']['notification_type']=='lesson_update')
						{
							$heading= __('lesson_updated');
							
							$n_text = String::insert(__('lesson_has_been_updated'),array('lesson' => $notification['CourseLesson']['title']));
						}
						else if($notification['Notification']['notification_type']=='update_profile')
						{
							$heading= __('profile_updated');
							$n_text=__('profile_updated');
						}
						else if($notification['Notification']['notification_type']=='password_changed')
						{
							$heading= __('password_changed');
							$n_text=__('your_password_has_been_changed');
						}
						else if($notification['Notification']['notification_type']=='request_course_author')
						{
							$heading= __('author_request');
							$n_text=__('your_request_to_become_an_author_has_been_sent_to_a_membership_manager_for_review');
						}
						else if($notification['Notification']['notification_type']=='already_author')
						{
							$heading= __('already_author');
							$n_text=__('you_are_an_author_already');
						}
						else if($notification['Notification']['notification_type']=='add_course_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_has_been_added_to_your_shopping_basket'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='course_not_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_was_not_added_to_the_shopping_basket_please_try_again'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='lesson_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							$n_text = String::insert(__('lesson_has_been_added_to_your_shopping_basket'),array('lesson' => $notification['CourseLesson']['title']));
						}
						else if($notification['Notification']['notification_type']=='lesson_present_shopping_basket')
						{
							
								if($notification['Notification']['c_id']==NULL)
							{
								$heading= __('shopping_basket');
							
							$n_text = String::insert(__('a_course_in_the_shopping_basket_already_contains_lesson'),array('lesson' => $notification['CourseLesson']['title']));
							}
							else
							{
								$heading= __('shopping_basket');
							
							$n_text = String::insert(__('a_course_in_the_shopping_basket_already_contains_lesson'),array('lesson' => $notification['Course']['title']));
							}
							
						}
						else if($notification['Notification']['notification_type']=='lesson_not_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('lesson_was_not_added_to_the_shopping_basket_please_try_again'),array('lesson' => $notification['CourseLesson']['title']));
						}
						else if($notification['Notification']['notification_type']=='course_removed_shopping_basket' && !empty($notification['CourseLesson']['id']) )
						{
							$heading= __('shopping_basket');
							$n_text = String::insert(__('lesson_was_removed_from_the_shopping_basket'),array('lesson' => $notification['CourseLesson']['title']));
							
							
						}
						else if($notification['Notification']['notification_type']=='course_removed_shopping_basket' && !empty($notification['Course']['id']) )
						{
							$heading= __('shopping_basket');
							$n_text = String::insert(__('course_was_removed_from_the_shopping_basket'),array('course' => $notification['Course']['title']));
							
							
						}
						else if($notification['Notification']['notification_type']=='course_not_removed_shopping_basket' && !empty($notification['CourseLesson']['id']))
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('lesson_not_removed_from_cart_please_try_again'),array('lesson' => $notification['CourseLesson']['title']));
						}
						else if($notification['Notification']['notification_type']=='course_not_removed_shopping_basket' && empty($notification['CourseLesson']['id']))
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_was_not_removed_from_the_shopping_basket_please_try_again'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='course_added_learning_plan')
						{
							$heading= __('learning_plan');
							
							$n_text = String::insert(__('course_was_added_to_your_learning_plan'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='course_not_added_learning_plan')
						{
							$heading= __('learning_plan');
						
							$n_text = String::insert(__('course_was_not_added_to_your_learning_plan_please_try_again'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='already_added_learning_plan')
						{
							$heading= __('learning_plan');
							
							$n_text = String::insert(__('course_is_already_in_your_learning_plan'),array('course' => $notification['Course']['title']));
						}
						else if($notification['Notification']['notification_type']=='review')
						{
							$heading= __('review');
							$n_text=__('thank_you_for_your_review');
						}
						else if($notification['Notification']['notification_type']=='course_not_purchased')
						{
							$heading= __('course_not_purchased');
								$n_text=String::insert(__('the_order_has_been_cancelled_and_you_have_not_been_charged'));
						}
						else if($notification['Notification']['notification_type']=='course_purchased_sucessfully')
						{
							$heading= __('your_transaction_is_complete');
							$n_text=__('a_confirmation_message_has_been_sent_to_your_registered_e-mail_address');
						}
						else if($notification['Notification']['notification_type']=='subsptn_account_reactivate')
						{
							$heading= __('account_activation');
							$n_text=__('your_account_has_been_activated_successfully');
						}
						else if($notification['Notification']['notification_type']=='subscription_success')
						{
							$heading= __('your_subscription_is_complete');
							$n_text=__('a_confirmation_message_has_been_sent_to_your_registered_e-mail_address');
						}
						else if($notification['Notification']['notification_type']=='subscription_failure')
						{
							$heading= __('subscription_not_purchased');
							$n_text=String::insert(__('the_order_has_been_cancelled_and_you_have_not_been_charged'));
						}
						else if($notification['Notification']['notification_type']=='member_not_login')
						{
							$heading= __('access_denied');
							$n_text=__('please_sign_in_to_access_this_section');
						}	
						else if($notification['Notification']['notification_type']=='account_activation')
						{
							$heading= __('account_activation');
							$n_text=__('your_account_has_been_activated_you_may_change_your_membership_details_any_time_by_selecting_your_e-mail_address_in_the_header');
						}
						else if($notification['Notification']['notification_type']=='account_reactivate')
						{
							$heading= __('account_reactivate');
							$n_text=__('account_has_been_reactivated');
						}
						else if($notification['Notification']['notification_type']=='permission_denied')
						{
							$heading= __('permission_denied');
							$n_text=__('permission_denied');
						}
					   else if($notification['Notification']['notification_type']=='account_cancel_success')
						{
							$heading= __('account_cancelled');
							$n_text=__('your_account_has_been_cancelled_but_we_will_be_happy_to_see_you_again');
						}
						else if($notification['Notification']['notification_type']=='register_success')
						{
							$heading= __('registration');
							$n_text=__('a_confirmation_message_has_been_sent_to_the_e-mail_address_you_entered_please_select_activate_my_membership_when_the_e-mail_arrives');
						}
						else if($notification['Notification']['notification_type']=='unable_account_activation')
						{
							$heading= __('unable_to_activate_membership');
							$n_text=__('the_activate_my_membership_link_has_already_been_used_or_it_has_passed_its_expiry_date_and_time_if_your_membership_has_already_been_activated_please_sign_in_using_your_e-mail_address_and_the_password_you_entered_during_registration_if_your_membership_was_not_activated_within_24-hours_of_registration_you_will_need_to_register_again');
						}
					
						$msg .='<li class="item js-item " data-id="'.$i.'"><div class="details"><span class="title">'.$heading.'</span><span class="date">'.$n_text.'</span></div><button type="button" class="button-default button-dismiss js-dismiss delete_notify">×</button> <input type="hidden" value="'.base64_encode(convert_uuencode($notification_id)).'" class="n_id '.$notification_id.'" /></div>';
$i++;
						if($index == 0)
						{
                                                $single_msg =  '<li class="item js-item " data-id="0"><div class="details"><span class="title">'.$heading.'</span><span class="date">'.$n_text.'</span></div><button type="button" class="button-default button-dismiss js-dismiss delete_notify">×</button> <input type="hidden" value="'.base64_encode(convert_uuencode($notification_id)).'" class="n_id" /></div>';
						}

					}			
					
					$result = array('process'=>'save','msg'=>$msg,'single_msg'=>$single_msg,'noti-count'=>$notification_count,'count'=>$count);
				}	
			} else{
					$result = array('process'=>'not_save','msg'=>'','notification_all'=>$notification_count,'count'=>$count);
			}
			$result = array_merge(array('countN'=>$notification_count),$result);
			echo json_encode($result);
			die;
		}
	}
        
    function play_video_swf($video=NULL,$ext=NULL,$m_id=NULL,$title=null)
	{
      
		$this->layout='';
		$title = $title ? $title:__('interactive_video');
		$extension = $ext=='swf' ? '(SWF)':'(HTML5)';
		$title_for_layout = $title.' '.$extension;
		$this->set(compact('title_for_layout'));
		if(!empty($video) && !empty($ext) && !empty($m_id))
		{  
			$member_id = convert_uudecode(base64_decode($m_id));
			
			if($ext=='swf')
			{ 
				/*if(file_exists('files/members/'.$member_id.'/'.$video.'.'.$ext))
				{  */
			
						$path=$this->getAWSDisplayUrl(LESSON_VIDEOS_BUCKET,$member_id.'/'.$video.'.'.$ext);
				if($path==true)
				{  
					$this->set(compact('member_id','video','ext'));	
				}
				
			}else if($ext=='zip')
			{ 
				
			$path=$this->getAWSDisplayUrl(LESSON_VIDEOS_BUCKET,$member_id.'/'.$video);
				if($path==true)
				{  
					$this->set('index','index.html');
                    $this->set(compact('member_id','video','ext'));	

				}
			} 
		}
		
	}
	function getAWSDisplayUrl($bucket,$key){
		if($key){
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();
		$url=  $this->Aws->getFileURL($bucket, $key);		
		$file_headers = @get_headers($url);
		if($file_headers[0] == 'HTTP/1.0 404 Not Found')
		{
		return   $file_exists = false;
		} else {
		return   $file_exists =true;
		}
		}else{
		return $file_exists = false;
		}
		//return $file_exists;
	}
  function user_preferences($preference='normal'){
            $this->Session->delete('LocTrain.preference');
            $this->Session->write('LocTrain.preference',$preference);
           //   echo $this->Session->read('LocTrain.preference');
            $this->layout =	'public';		
            $title_for_layout = 'Localization Training';
            $locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
            $this->set(compact('title_for_layout','news','locale','preference'));
            
        }
      function update_rating()
	{
		$this->loadModel('CourseRating');
		$this->loadModel('Course');
		$course_id = $_POST['course_id'];
		$rating = trim($_POST['rating']);
		if($this->Session->read('LocTrain.id'))
		{
			
			$mem_id = $this->Session->read('LocTrain.id');
			$is_reviewed = $this->CourseRating->find('first',array('conditions'=>array('CourseRating.member_id'=>$mem_id,'CourseRating.course_id'=>$course_id)));
			if(!empty($is_reviewed))
			{
				$review = $this->CourseRating->create();
				$this->CourseRating->id=$is_reviewed['CourseRating']['id'];
				$this->CourseRating->saveField('rating',$rating);
				
				$all_ratings=$this->CourseRating->find('all', array('conditions'=>array('CourseRating.course_id'=>$course_id)));
				if(!empty($all_ratings))
				{
					$rate=0;
					$no_recs=0;
					foreach($all_ratings as $ratings)
					{                                        
						$rate+=$ratings['CourseRating']["rating"];
						$no_recs++;
					}
					$avg = $rate/$no_recs;
					$this->Course->id= $course_id;
					$this->Course->saveField('rating',$avg);	
				}
				echo "updated"; die;
			}
			else
			{
				$review = $this->CourseRating->create();
				$review['CourseRating']['member_id'] = $mem_id;
				$review['CourseRating']['course_id'] = $course_id;
				$review['CourseRating']['rating'] = $rating;
				$this->CourseRating->save($review);
				
				
				$all_ratings=$this->CourseRating->find('all', array('conditions'=>array('CourseRating.course_id'=>$course_id)));
				if(!empty($all_ratings))
				{
					$rate=0;
					$no_recs=0;
					foreach($all_ratings as $ratings)
					{
						$rate+=$ratings['CourseRating']["rating"];
						$no_recs++;
					}
					$avg = $rate/$no_recs;
					$this->Course->id= $course_id;
					$this->Course->saveField('rating',$avg);	
				}
				
				
				echo "inserted"; die;
			}	
		}
		else
		{
			echo "no_login"; die;
		}
		echo "error"; die;
	}

   function access_denied(){
            $this->before_update_notification();
            $temp_id = $this->Session->read('LocTrain.visitor_id');
            $this->loadModel('Notification');
            $notification['Notification']['mem_id'] =  0;
            $notification['Notification']['notification_type'] = 'member_not_login';
            $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
            $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
            $notification['Notification']['notification_sender_id'] = $temp_id;
            $notification['Notification']['read'] = 1;
            $this->Notification->create();
            $this->Notification->save($notification);   
            echo 'index';
            die();
        }
	function filesave($data){
	
	$fp=fopen("abc.txt","w");
			
			fwrite($fp,$data);  
			fclose($fp);  
	}
	
	function runquery()
	{
	$this->loadModel('Notification');
	for($i=0;$i<10;$i++)
	{
	$this->Notification->query("INSERT INTO `loctrain`.`notifications` (`id`, `notification_type`, `read`, `mem_id`, `notification_sender_id`, `c_id`, `lesson_id`, `date_added`, `date_modified`) VALUES (NULL, 'course_purchased', '0', '164', '256', '29', NULL, '1418284030', '1418284030')");	  
	}
	exit;
	}
	
	function findcookiepurchase()
	{
		$this->loadModel('Course');		
		$this->loadModel('CourseLesson');
		$encode_ids_new= $this->Cookie->read('LocTrainPurchase');
		
		//echo json_encode($encode_ids_new);
    	if(!empty($encode_ids_new)){
		$checkpurchasedlessonitem=array();
		foreach($encode_ids_new as $key=>$val){
		$keydata=explode("m",$key);
		if($keydata[0]=='ite')	
		{
		$less_courid = @convert_uudecode(base64_decode($val)); 
		$checkpurchaseditem=$this->Course->find('all',array('conditions'=>array('Course.id'=> $less_courid),'fields'=>array('title')));
		}
		else
		{
		$less_courid = @convert_uudecode(base64_decode($val)); 
		$checkpurchasedlessonitem[]=$this->CourseLesson->find('all',array('conditions'=>array('CourseLesson.id'=> $less_courid),'fields'=>array('title')));
		
		}		
		}
		}
		//echo json_encode(array('lessonlists'=>$checkpurchasedlessonitem));
		echo json_encode(array('countcart'=>count($encode_ids_new),'courseslist'=>$checkpurchaseditem,'lessonlists'=>$checkpurchasedlessonitem));
		exit;
		}
function coursetitle($id=NULL)
{
	$this->loadModel('CourseLesson');
	$less_courid = @convert_uudecode(base64_decode($id)); 
	$coursetitle=$this->CourseLesson->find('all',array('conditions'=>array('CourseLesson.id'=> $less_courid),'fields'=>array('title')));
	echo $coursetitle[0]['CourseLesson']['title'];
exit;
}
	function image_resizepng(){

			if(!empty($_FILES['image']['name']))
			{
				$fileCount = count($_FILES['image']['name']);


                                $destination = 'files/courses/';
				$thumb = 'files/courses/thumb/';
				$medium = 'files/courses/medium/';
				$catalogue = 'files/courses/catalogue/';
				$imgName = pathinfo($_FILES['image']['name']);
				$ext = strtolower($imgName['extension']);
				if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
				{
					$newImgName =$imgName['filename'].".".$ext;						
					$img = $_FILES['image'];
					
					App::uses('UploadComponent', 'Controller/Component'); 
					$this->Upload = new UploadComponent(new ComponentCollection());	
													
					$result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resizecrop','size'=>array('262','138'),'quality'=>'90'));	
					$result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));
					$result2 =  $this->Upload->upload($img, $medium, $newImgName, array('type'=>'resizecrop','size'=>array('530','298'), 'quality'=>'90'));
					$result3 =  $this->Upload->upload($img, $catalogue, $newImgName, array('type'=>'resizecrop','size'=>array('256','144'), 'quality'=>'90'));

					
			$this->redirect(array('action'=>'image_resize'));

							
				}
			}
		
	}
function image_resize(){

			
			if($this->data['image']){echo 'im here';
				//print_r($this->data);
				echo '--'.$fileCount = count($this->data['image']['image']);
				 $destination = 'files/courses/';
				$thumb = 'files/courses/thumb/';
				$medium = 'files/courses/medium/';
				$catalogue = 'files/courses/catalogue/';
				foreach($this->data['image']['image'] as $key=>$val){
				$imgName = pathinfo($val['name']);
				$ext = strtolower($imgName['extension']);
						print_r($val);print_r($imgName);echo $ext;
				if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
								{
									echo '--'.$newImgName =$imgName['filename'].".".$ext;						
									$img = $val;
					
									App::uses('UploadComponent', 'Controller/Component'); 
									$this->Upload = new UploadComponent(new ComponentCollection());	
													
									$result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resizecrop','size'=>array('262','138'),'quality'=>'90'));	
									$result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));
									$result2 =  $this->Upload->upload($img, $medium, $newImgName, array('type'=>'resizecrop','size'=>array('530','298'), 'quality'=>'90'));
									$result3 =  $this->Upload->upload($img, $catalogue, $newImgName, array('type'=>'resizecrop','size'=>array('256','144'), 'quality'=>'90'));

					
							//$this->redirect(array('action'=>'image_resize'));

							
								}
				}
			}
		
	}
        //----------------- Check lessons of Course are buy or not
	function check_lessons_course(){
		$course_id = convert_uudecode(base64_decode($_POST['course_id']));
		$login_m_id=$this->Session->read('LocTrain.id'); //getting session member id
		$this->loadModel('Course');
		$this->loadModel('CoursePurchaseList');
		$this->Course->unbindModel(
        array('hasOne'=>array('CourseView'),'hasMany' => array('CourseRating','CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
		$course_info = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),'contain'=>array('CourseLesson'=>array('conditions'=>array('CourseLesson.status !='=>'2'),'order'=>'CourseLesson.order ASC'))));

  		if(!empty($course_info['CourseLesson'])){
			foreach($course_info['CourseLesson'] as $lesson){
				$lesson_ids[]=$lesson['id'];
			}

		$subscription = $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>$lesson_ids,'CoursePurchaseList.mem_id'=>$login_m_id,'CoursePurchaseList.type'=>0),array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$login_m_id,'CoursePurchaseList.type'=>0)))));

			if(!empty($subscription)){echo '1';}else{echo '0';}
		}else{
		echo '0';		
		}
	exit;
	
	}

	function testMail(){
		//if($this->send_email('sagarika.jaina@gmx.com','noreply@l10ntrain.com','test','test')){echo 'yes';}else{echo 'no';}
mail("sagarika.jaina@gmx.com","My subject",'test');
exit;
		}
}
?>
