<?php
 ini_set('memory_limit', '1024M'); // or you could use 1G
ob_start();
class MembersController extends AppController 
{
	var $name =	'Members';
	var $components = array('RequestHandler','Session','Cookie');
	var $helpers = array('Form','Js','Html','Text','Time','Timezone','Utility');
	var $pageLimit = '12';
	
	function beforeFilter()
	{
		parent::beforeFilter(); 
		$this->disableCache();
		
		if($this->request->params['action']!='subscription' && $this->request->params['action']!='lesson_detail' && $this->request->params['action']!='upgrade_membership')
		{
			if($this->RequestHandler->isAjax() && $this->Session->read('LocTrain.login') != '1'){
				//$this->Session->write('LocTrain.flashMsg',__('You are not logged in or your session has expired. Please login to access this section.'));
				echo 'error';die;
			}
			if($this->Session->read('LocTrain.login') != '1')
			{	
					$this->before_update_notification();
					$temp_id = $this->Session->read('LocTrain.visitor_id');
					$this->loadModel('Notification');
					$notification['Notification']['mem_id'] =  0;
					$notification['Notification']['notification_type'] = 'member_not_login';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['notification_sender_id'] = $temp_id;
					$notification['Notification']['read'] = 1;
					$this->Notification->create();
					$this->Notification->save($notification);
					
				//$this->Session->write('LocTrain.flashMsg',__('Please login to access this section.'));
				$this->redirect(array('controller'=>'Home','action'=>'index'));
			}
		}
		//  TESTING
		$this->loadModel('Notification');
		$this->loadModel('Member');
		$isNotify = $this->Notification->find('all',array('conditions'=>array('Notification.mem_id'=>$this->Session->read('LocTrain.id'),'Notification.read'=>'0')));
		
		$mem_name = $this->Member->find('first',array('conditions'=>array('Member.id'=>$this->Session->read('LocTrain.id')),'contain'=>false,'fields'=>array('given_name','role_id')));
		
		$this->set(compact('isNotify','mem_name'));
		
		//pr($isNotify);die;
	}
	
	function my_learning($id=NULL,$tab=NULL)
	{	
		 $f_type=convert_uudecode(base64_decode($id));
		//pr($f_type);die;
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('CoursePurchaseList');
		$this->loadModel('Course');
		$this->loadModel('MemberSubscription');
		$locale_id = $this->Session->read('LocTrain.LangId');
		$this->loadModel('Member');
		$date = date("Y-m-d H:i:s");
		$login_m_id = $this->Session->read('LocTrain.id');
		$info=$this->Member->find('first',array('conditions'=>array('Member.id'=>$login_m_id),'fields'=>array('role_id')));
		$role_id=$info['Member']['role_id'];
		//pr($role_id);die;
		if($f_type==2)
		{
			/*$conditions = array('CoursePurchaseList.mem_id'=>$login_m_id,'CoursePurchaseList.type'=>'2','CoursePurchaseList.status'=>'0');*/
$conditions = array('CoursePurchaseList.mem_id'=>$login_m_id,'CoursePurchaseList.type'=>array(0),'CoursePurchaseList.status'=>'0');
		}else
		{			
			//$conditions = array('CoursePurchaseList.mem_id'=>$login_m_id,'CoursePurchaseList.type'=>array(0),'CoursePurchaseList.status'=>'0');
$conditions = array('CoursePurchaseList.mem_id'=>$login_m_id,'CoursePurchaseList.type'=>array(0));	
	}

		//$conditions = array('CoursePurchaseList.mem_id'=>$login_m_id,'CoursePurchaseList.type'=>array(0,1));
		
		$order = array('CoursePurchaseList.id desc');
		//$getCourses = $this->Cookie->read('LocTrainPurchase');
$this->loadModel('purchaseItem');
$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
		if($login_m_id){
                    $order_ids_mem=array();
            $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>		$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                    if(!empty($get_array)){
                                    foreach($get_array as $row){
                                    $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
                                    }
                                    } 
		$getCourses = $order_ids_mem;
		}else{
		$getCourses = $this->Cookie->read('LocTrainPurchase');
		}
		$last_id = $this->pageLimit;
		$last_record_id = $this->pageLimit;
		$update_record = 0;	
		$cookie_courses = array();
		
		if(!empty($getCourses)){
			foreach($getCourses as $course=>$val){
				if(strlen($course)<8)
				{
					$cookie_courses[] = convert_uudecode(base64_decode($val));
				}
			}
		}
		$sort_oder='Course.id desc';
		if($this->RequestHandler->isAjax()){
			
			$this->layout = '';
			$this->autoRender = false;
			
			$id = $this->params->query['limit'];
			$last_id = $this->params->query['limit'] + $this->pageLimit;
			$last_record_id = $this->params->query['limit'] +  $this->pageLimit;
			if(isset($this->params->query['sort_order']) && $this->params->query['sort_order'] =='1'){
					$sort_oder='Course.id desc';

				}
				
				/*else{

			$sort_oder='Course.id asc';
				}*/
			if(isset($this->params->query['completed']) || isset($this->params->query['current']) || isset($this->params->query['planed'])){
				$aa = array();
				if($this->params->query['completed'] != '' && $this->params->query['completed'] == 1){
					$aa[] = '2';
				}
				if($this->params->query['current'] != '' && $this->params->query['current'] == 1){
					$aa[] = '0';
				}
				if($this->params->query['planed'] != '' && $this->params->query['planed'] == 1){
					$aa[] = '1';
				}
				$conditions =array_merge($conditions, array('CoursePurchaseList.type'=>$aa));
				
			}
	
			$this->set(compact('last_record_id','update_record','cookie_courses'));
			
			if($this->params->query['type'] == '1'){	

			
				$courses_id = $this->CoursePurchaseList->find('list',array('conditions'=>$conditions,'order'=>$order,'fields'=>array('c_id')));			
				$course_list = $this->Course->find('all',array('conditions'=>array('Course.id'=>$courses_id),'order'=>array($sort_oder),'contain'=>array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CourseWatch'=>array('conditions'=>array('CourseWatch.member_id'=>$login_m_id)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id))),'limit'=>$this->pageLimit,'offset' => $id));	
				$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
				//$course_list = $this->MemberCourse->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>array('Course'=>array('id','m_id','title','description')),'limit'=>$id.','.$this->pageLimit));
				//pr($course_list); die;
				$countres[]=count($course_list);
				$this->set(compact('course_list','last_id','subscribed','countres'));
				$this->viewPath = 'Elements'.DS.'frontElements/course';
				$this->render('my_learning_list');
			}
			
			else if($this->params->query['type'] == '2' || $this->params->query['type'] == '3'){
					
					
			if($this->params->query['planed']==0)
			{
			$tabtype='other';
				$courses_id = $this->CoursePurchaseList->find('list',array('conditions'=>$conditions,'order'=>$order,'fields'=>array('c_id')));			
				$course_list = $this->Course->find('all',array('conditions'=>array('Course.id'=>$courses_id),'order'=>array($sort_oder),'contain'=>array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CourseWatch'=>array('conditions'=>array('CourseWatch.member_id'=>$login_m_id)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id)), 'CourseLesson'),'limit'=>$this->pageLimit,'offset' => null));
				$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
				//$course_list = $this->MemberCourse->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>array('Course'=>array('id','m_id','title','description')),'limit'=>'0,'.$this->pageLimit)); 
				//pr($course_list); die;
				$this->set(compact('course_list','last_id','subscribed','tabtype'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/course';
					$this->render('my_learning_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_records_found_').'</div>';die;
				}
			}
			else
			{
			$tabtype='wishlist';
			$courses_id = $this->CoursePurchaseList->find('list',array('conditions'=>$conditions,'order'=>$order,'fields'=>array('c_id')));			
				$course_list = $this->Course->find('all',array('conditions'=>array('Course.id'=>$courses_id),'order'=>array($sort_oder),'contain'=>array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id'=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>1)),'CourseWatch'=>array('conditions'=>array('CourseWatch.member_id'=>$login_m_id)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id)), 'CourseLesson'),'limit'=>$this->pageLimit,'offset' =>  $id));
				$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
				//$course_list = $this->MemberCourse->find('all',array('conditions'=>$conditions,'order'=>$order,'contain'=>array('Course'=>array('id','m_id','title','description')),'limit'=>'0,'.$this->pageLimit)); 
				//pr($course_list); die;
				$this->set(compact('course_list','last_id','subscribed','tabtype'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/course';
					$this->render('my_learning_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_records_found_').'</div>';die;
				}
			}
			
			}
			
		}
		else{
		
	if($this->params->query['planed']==0)
			{
			$tabtype='other';
				$courses_id = $this->CoursePurchaseList->find('list',array('conditions'=>$conditions,'order'=>$order,'fields'=>array('c_id')));
			
			$course_list = $this->Course->find('all',array('conditions'=>array('Course.id'=>$courses_id),'order'=>array($sort_oder),'contain'=>array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CourseWatch'=>array('conditions'=>array('CourseWatch.member_id'=>$login_m_id)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id))),'limit'=>$this->pageLimit,'offset' => $id));	
			$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
			
				
			}
			else
			{
			$tabtype='wishlist';
			$courses_id = $this->CoursePurchaseList->find('list',array('conditions'=>$conditions,'order'=>$order,'fields'=>array('c_id')));
			
			$course_list = $this->Course->find('all',array('conditions'=>array('Course.id'=>$courses_id),'order'=>array($sort_oder),'contain'=>array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id'=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>1)),'CourseWatch'=>array('conditions'=>array('CourseWatch.member_id'=>$login_m_id)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id))),'limit'=>$this->pageLimit,'offset' =>  $id));	
			$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
			
			}
			
			
		}
		
		
		/** RTSH **/		
		$this->loadModel('CourseLesson');
		$this->loadModel('CoursePurchaseList');
		$set= array();
		foreach($course_list as $c)
		{
			$course_lesson = $this->CourseLesson->find('list',array('conditions'=>array('CourseLesson.course_id'=>$c['Course']['id'],'CourseLesson.price NOT'=>0.00),'fields'=>array('id'),'contain'=>false)); 
		
			$total_course_lesson = count($course_lesson);
			$count_course_purchase = $this->CoursePurchaseList->find('all',array('conditions'=>array('CoursePurchaseList.type'=>0,'CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'),'CoursePurchaseList.lesson_id'=>$course_lesson )));	
	
			$total_purchase_course = count($count_course_purchase);


			if($total_course_lesson==$total_purchase_course)
			{ 
				$set[] = __('unlink');
			}
			else
			{ 
				$set[] = __('link');
			}
		}
		
		/** END RTSH **/
		$this->set(compact('title_for_layout','course_list','last_id','last_record_id','update_record','cookie_courses','subscribed','set','f_type','tabtype'));
	}
	function course($id=NULL)
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Course');	
		$login_m_id = $this->Session->read('LocTrain.id');
		$locale_id = $this->Session->read('LocTrain.LangId');		
		$last_record_id = $last_id=$this->pageLimit;		
		$update_record = 0;	
		$contain = array('CourseLesson'=>array('order'=>array('CourseLesson.order ASC'),'fields'=>array('id','video','extension','image')),'CourseRating'=>array('fields'=>array('rating')),'CoursePurchaseList'=>array('CourseLesson'=>array('id','title','price'),'CoursePurchase','Course'=>array('id','title','price')));
		if($this->RequestHandler->isAjax()){			
			$this->layout = '';
			$this->autoRender = false;
			$conditions = array('Course.status'=>'1','Course.m_id'=>$login_m_id);
			$order = array('Course.id desc');
			$id = $this->params->query['limit'];
			$last_id = $id  + $last_id;
			$last_record_id = $id  + $last_id;			
			if(isset($this->params->query['keyword']) && !empty($this->params->query['keyword'])){
				$this->loadModel('CourseKeyword');
				$keyword = explode(",",$this->params->query['keyword']);				
				$member_keyword_ids = $this->CourseKeyword->find('list',array('conditions'=>array('CourseKeyword.keyword'=>$keyword),'fields'=>array('course_id')));
				$conditions = array_merge($conditions,array('Course.id'=>$member_keyword_ids));
			}
			
			if(isset($this->params->query['order']) && !empty($this->params->query['order'])){
				switch(trim($this->params->query['order'])){
					case 'title':
					if(isset($this->params->query['sort_order']) && !empty($this->params->query['sort_order']) && $this->params->query['sort_order']=='1'){
					 $order = array('Course.title ASC'); break;
			}else{
			 $order = array('Course.title DESC'); break;
			}
					case 'date': 
				if(isset($this->params->query['sort_order']) && !empty($this->params->query['sort_order']) && $this->params->query['sort_order']=='1'){
			$order = array('Course.date_added ASC','Course.id ASC'); break;
			}else{
			 $order = array('Course.date_added DESC','Course.id DESC'); break;
			}
					case 'rating':
			    if(isset($this->params->query['sort_order']) && !empty($this->params->query['sort_order']) && $this->params->query['sort_order']=='1'){
			$order = array('Course.rating ASC'); break;
			}else{
			 $order = array('Course.rating DESC'); break;
			}
					case 'reviews':
			    if(isset($this->params->query['sort_order']) && !empty($this->params->query['sort_order']) && $this->params->query['sort_order']=='1'){
			$order = array('Course.review ASC'); break;
			}else{
			 $order = array('Course.review DESC'); break;
			}
				}
			}

			
			if(isset($this->params->query['duration_opt']) && !empty($this->params->query['duration_opt'])){
				switch(trim($this->params->query['duration_opt'])){
					case 'all': $additional_cond = array('Course.duration_seconds >='=>0); break;
					case '<15': $additional_cond = array('Course.duration_seconds BETWEEN ? AND ?'=>array(0,900)); break;
					case '15>30<': $additional_cond = array('Course.duration_seconds BETWEEN ? AND ?'=>array(900,1800)); break;
					case '30>60<': $additional_cond = array('Course.duration_seconds BETWEEN ? AND ?'=>array(1800,3600)); break;
					case '60>': $additional_cond = array('Course.duration_seconds >='=>3600); break;
				}
				$conditions = array_merge($conditions,$additional_cond);
			}
			if(isset($this->params->query['score']) && !empty($this->params->query['score']) && is_numeric($this->params->query['score'])){
				
				$additional_cond_rating = array('Course.rating'=>$this->params->query['score']);
				$conditions = array_merge($conditions,$additional_cond_rating);
			}
			$this->set(compact('last_record_id','update_record'));
			if($this->params->query['type'] == '1'){
				$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => $id));
				$this->set(compact('course_list','last_id'));
				$this->viewPath = 'Elements'.DS.'frontElements/course';
				$this->render('my_teaching_list');
			}
			else if($this->params->query['type'] == '2'){
				$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => $id));				
				$this->set(compact('course_list','last_id'));				
				$this->viewPath = 'Elements'.DS.'frontElements/course';
				$this->render('my_teaching_list');
			}
			else if($this->params->query['type'] == '3'){
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));
				$this->set(compact('course_list','last_id'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/course';
					$this->render('my_teaching_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_courses_match_the_filter').'</div>';die;
				}
			}
			else if($this->params->query['type'] == '4'){	
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));				
				$this->set(compact('course_list','last_id'));
				$this->viewPath = 'Elements'.DS.'frontElements/course';
				$this->render('my_teaching_list');
			}
			else if($this->params->query['type'] == '5'){
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));			
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->params->query['total_record'],'offset' => null));
				$update_record = 1;
				$this->set(compact('course_list','last_id','update_record'));
				$this->viewPath = 'Elements'.DS.'frontElements/course';
				$this->render('my_teaching_list');
			}
			else if($this->params->query['type'] == '6'){	
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));			
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));		
				$this->set(compact('course_list','last_id','update_record'));
				$this->viewPath = 'Elements'.DS.'frontElements/course';
				$this->render('my_teaching_list');
			}
			else if($this->params->query['type'] == '7'){
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));				
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));				$this->set(compact('course_list','last_id'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/course';
					$this->render('my_teaching_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_courses_match_the_filter').'</div>';die;
				}
			}
			else if($this->params->query['type'] == '8'){
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));				
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));				$this->set(compact('course_list','last_id'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/course';
					$this->render('my_teaching_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_courses_match_the_filter').'</div>';die;
				}
			}
			else if($this->params->query['type'] == '9'){	
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));			
				$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>$conditions,'order'=>$order,'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));				$this->set(compact('course_list','last_id'));
				if(!empty($course_list)){
					$this->viewPath = 'Elements'.DS.'frontElements/course';
					$this->render('my_teaching_list');
				}else{
					echo '<div class="mytch_rcd">'.__('no_courses_match_the_filter').'</div>';die;
				}
			}	
		}
		else{
			$this->loadModel('Interest');
			$interest_list = '';
			$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest asc'));		
		
			if(!empty($interest)){
				foreach($interest as $interestVal):			
					$interest_list .= '{value:'."'".$interestVal['Interest']['interest']."'".", name:"."'".$interestVal['Interest']['interest']."'},";	
				endforeach;	
			}else{
				$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
			}
$this->Course->unbindModel(
        array('hasMany' => array('CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
			$course_list = $this->Course->find('all',array('fields'=>array('id','image','title','m_id','price','description','review'),'conditions'=>array('Course.status'=>'1','Course.m_id'=>$login_m_id),'order'=>array('Course.id desc'),'contain'=>$contain,'limit'=>$this->pageLimit,'offset' => null));
			
		}
		
		$this->set(compact('title_for_layout','course_list','last_id','interest_list','last_record_id','update_record','commission'));		
	}
	
//------------ADD NEW COURSE 
	function course_create()
	{ 
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Interest');
		$this->loadModel('Language');
		$this->loadModel('CoursePrice');
		$this->loadModel('CourseCategory');
		$this->loadModel('Notification');

		$locale = $this->Session->read('LocTrain.locale');
		$this->set('locale',$locale);
		 
		$interest_list = '';
		$categories=array();
		$language_name = $this->language_name;
		$languages = $this->Language->find('all',array('conditions'=>array('Language.status'=>1)));
		
		$course_prices = $this->CoursePrice->find('list',array('order'=>'CoursePrice.price ASC','fields'=>array('id','price')));
		
		$categories = $this->CourseCategory->find('all',array('order'=>'CourseCategory.'.$locale.' ASC','fields'=>array('id',$locale),'conditions'=>array('CourseCategory.status'=>1),'contain'=>array('CourseSubcategory'=>array('id','category_id',$locale,'status'=>array('CourseSlSubcategory'=>array('id','sub_category_id',$locale,'status'))))));
		//pr($locale);
		//pr($categories); die;
		//$categories = $this->CourseCategory->find('all',array('order'=>'CourseCategory.'.$locale.' ASC','fields'=>array('id',$locale),'conditions'=>array('CourseCategory.status'=>1),'contain'=>array('CourseSubcategory'=>array('id','category_id',$locale,'status'=>array('CourseSlSubcategory'=>array('id','sub_category_id',$locale,'status'))))));
		
		$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest ASC'));		
		if(!empty($interest)){
			foreach($interest as $interestVal):	
					$str=$interestVal['Interest']['interest'];
				//$interest_list .= '{value:'.'"'.$str.'"'.", name:".'"'.$str.'"},';	
				$interest_list .="{value:"."'".$str."'".',name:'."'".$str."'},";
			endforeach;	
		}else{
			$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
		}
		
		
		$this->set('categories',$categories);
		$this->set(compact('title_for_layout','interest_list','languages','categories','course_prices'));

		if(!empty($this->data)){
 			 $this->request->data['Course']['description']=nl2br(trim($this->data['Course']['description'])); 
			$login_m_id = $this->Session->read('LocTrain.id');
			$error = array();
			$error = $this->validate_course($this->data);
			if(count($error) == 0){
 if(!empty($_FILES['image']['name']))
			{
				$destination = 'files/members/courses/';
				$thumb = 'files/members/courses/thumb/';
				$medium = 'files/members/courses/medium/';
				$catalogue = 'files/members/courses/catalogue/';
				

				$imgName = pathinfo($_FILES['image']['name']);
				$ext = strtolower($imgName['extension']);

				if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
				{
					$newImgName = md5(time()).".".$ext;						
					$img = $_FILES['image'];
					$key_thumb='thumb/'.$newImgName;
					$key_medium='medium/'.$newImgName;
					$key_catalogue='catalogue/'.$newImgName;
					$key=$newImgName;
					App::uses('UploadComponent', 'Controller/Component'); 
					$this->Upload = new UploadComponent(new ComponentCollection());	
													
					$result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resizecrop','size'=>array('262','138'),'quality'=>'90'));	
					$result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));
					$result2 =  $this->Upload->upload($img, $medium, $newImgName, array('type'=>'resizecrop','size'=>array('530','298'), 'quality'=>'90'));
					$result3 =  $this->Upload->upload($img, $catalogue, $newImgName, array('type'=>'resizecrop','size'=>array('256','144'), 'quality'=>'90'));
					// uploading to AWS server

					$key_thumb='thumb/'.$newImgName;
					$key_medium='medium/'.$newImgName;
					$key_catalogue='catalogue/'.$newImgName;
					$key=$newImgName;
					 App::import('Component', 'Aws');
    					$this->Aws = new AwsComponent();
					$result_aws = $this->Aws->uploadFile($key, COURSES_BUCKET, $destination.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_thumb, COURSES_BUCKET,$thumb.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_medium, COURSES_BUCKET, $medium.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_catalogue, COURSES_BUCKET, $catalogue.$newImgName);
					

					//After

					@unlink('files/members/courses/'.$newImgName);
                                        @unlink('files/members/courses/thumb/'.$newImgName);
                                        @unlink('files/members/courses/medium/'.$newImgName);
					@unlink('files/members/courses/catalogue/'.$newImgName);
					


					$this->request->data['Course']['image'] = $newImgName;			
				}
			}
				//	echo '<pre>';print_r($this->data);echo '</pre>'; 
				$this->loadModel('Course');				
				$this->request->data['Course']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$this->request->data['Course']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$this->request->data['Course']['m_id'] = $login_m_id;
				$this->request->data['Course']['status'] = 1;
                                $this->request->data['Course']['meta_title'] = $this->data['Course']['title'];
                                $this->request->data['Course']['meta_description'] = $this->data['Course']['description'];
                                $this->request->data['Course']['meta_keyword'] = $this->data['Course']['keywords'];
                                $this->request->data['Course']['locale']= $this->data['Course']['primary_lang'];                   
				 $price_val= $this->data['Course']['price'];;
                $price_val_mod= trim(str_replace(array('$','€','¤'),array('','',''),$price_val));
                $locale=$this->Session->read('LocTrain.locale');
                if($locale=='en' || $locale=='zh'){
					$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
					}else if($locale=='de' || $locale=="es"){
						$price_val1=trim(str_replace(array(','),array('.'),$price_val_mod));
						 $count=substr_count($price_val1, '.');
							if($count>1){
							$price_val_new=	preg_replace('/\./', '', $price_val1, 1);
							}else{
								$price_val_new=$price_val1;
							}						
							
						
						}
						else {
							$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
						}
					$this->request->data['Course']['price']=$price_val_new;
				//$this->request->data['Course']['locale'] = $locale;
				
				if($this->data['Course']['category_id']==NULL)
				{
					$this->request->data['Course']['category_id']=NULL;
				}
				if($this->data['Course']['subcategory_id']==NULL)
				{
					$this->request->data['Course']['subcategory_id']=NULL;
				}
				if($this->data['Course']['sl_subcategory_id']== NULL)
				{
					$this->request->data['Course']['sl_subcategory_id']=NULL;
				}
		//	echo '<pre>';print_r($this->data);echo '</pre>'; die();
				if($this->Course->save($this->data)){
					$get_id = $this->Course->getlastinsertId();
					$this->Session->write('CourseId.id',$get_id);
					$encode_id =	base64_encode(convert_uuencode($get_id));
					$this->set('encode_id',$encode_id);
					$explodeKeyword = explode(",",$this->data['Course']['keywords']);
					//pr($explodeKeyword); die;
					if(!empty($explodeKeyword)){
						$this->loadModel('CourseKeyword');
						$this->loadModel('Interest');
						foreach($explodeKeyword as $words){
							if($words != ''){
								$checkInterestExists = $this->Interest->find('count',array('conditions'=>array('Interest.interest'=>trim($words))));
								if($checkInterestExists == 0){
									$this->Interest->create();
									$interest['Interest']['status']= '1';
									$interest['Interest']['interest']= trim($words);
									$interest['Interest']['date_added']= strtotime(date('Y-m-d H:i:s'));
									
									$this->Interest->save($interest);
								}								
								$checCourseKeywordExists = $this->CourseKeyword->find('count',array('conditions'=>array('CourseKeyword.m_id'=>$login_m_id,'CourseKeyword.course_id'=>$get_id,'CourseKeyword.keyword'=>trim($words))));
								if($checCourseKeywordExists == 0){
									$this->CourseKeyword->create();
									$data['CourseKeyword']['m_id'] =	$login_m_id;
									$data['CourseKeyword']['course_id'] =	$get_id;
									$data['CourseKeyword']['keyword'] =	trim($words);
									
									$this->CourseKeyword->save($data);
									
								}
								
							}		
						}
					}
					if(@$this->data['Course']['actionType'] == '2'){
						$this->before_update_notification();
						$notification['Notification']['mem_id'] = 0;
						$notification['Notification']['notification_type'] = 'course_saved';
						$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['c_id'] = $get_id;
						$notification['Notification']['notification_sender_id'] = $login_m_id;
						$this->Notification->create();
						$this->Notification->save($notification);
					
					
						//$this->Session->write('LocTrain.flashMsg','Course saved successfully.');
						$this->redirect(array('action'=>'course'));
					}else if (@$this->data['Course']['actionType'] == '3'){	
						$this->saveCourse($get_id,$encode_id);
					}
					else {
						
						$this->redirect(array('action'=>'lesson_add',$encode_id));
						
					}
				}
				
			}
		}
		
	}
 function saveCourse($get_id,$encode_id){
            $this->loadModel('Course');
            $this->Course->id=$get_id;
             //   print_r($this->Course->saveField('isPublished',1));

                if($this->Course->saveField('isPublished',1))
                {
                       $this->redirect(array('action'=>'lesson_add',$encode_id));
                }
                else
                {
                       $this->redirect(array('action'=>'course'));
                }
        }
 function saveCourse_ajax($get_id,$encode_id){
            $this->loadModel('Course');
            $this->Course->id=$get_id;
            
                if($this->Course->saveField('isPublished',1))
                {
                      echo 'success';
                }
                else
                {
                   echo 'error';
                }
die();
        }
	function validate_course_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_course($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Course'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_course($data)
	{			
		$errors = array ();		
		
		if (trim($data['Course']['title']) == ""){
			$errors['title'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif(strlen($data['Course']['title']) > 250 ){
			$errors['title'][] = __("a_maximum_of_250_characters_are_allowed")."\n";
		}
		elseif(ctype_digit($data['Course']['title'])){
			$errors['title'][] = __("numbers_are_not_allowed")."\n";
		}
		if (trim($data['Course']['description']) == ""){
			$errors['description'][] = __(FIELD_REQUIRED)."\n";
		}
		if (trim($data['Course']['primary_lang']) == ""){
			$errors['primary_lang'][] = __(FIELD_REQUIRED)."\n";
		}
		if (trim($data['Course']['price']) == ""){
			$errors['price'][] = __(FIELD_REQUIRED)."\n";
		}
		$price_val=trim($data['Course']['price']);
                $price_val_mod= trim(str_replace(array('$','€','¤'),array('','',''),$price_val));
                $locale=$this->Session->read('LocTrain.locale');
                if($locale=='en' || $locale=='zh'){
					$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
					}else if($locale=='de' || $locale=="es"){
						$price_val1=trim(str_replace(array(','),array('.'),$price_val_mod));
						 $count=substr_count($price_val1, '.');
							if($count>1){
							$price_val_new=	preg_replace('/\./', '', $price_val1, 1);
							}else{
								$price_val_new=$price_val1;
							}	
						}
						else {
							$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
						}
							
		if(strlen($price_val_new)<3 || strlen($price_val_new)>7){
			$errors['price'][] =__('enter_a_number_between_000_and_999999')."\n";
			}
		if (trim($data['Course']['category_id']) == ""){

			$errors['category_id'][] = __(FIELD_REQUIRED)."\n";
		}
		else
		{
			if(isset($data['Course']['subcategory_id']))
			{
				/*if (trim($data['Course']['subcategory_id']) == ""){
					$errors['subcategory_id'][] = __(FIELD_REQUIRED)."\n";
				}	*/			
			}
		}
		
		//pr($errors); die;
		/*
		else if (!is_numeric($data['Course']['price'])){
			$errors['price'][] = "Only numeric value allowed"."\n";
		}*/
		/*if (str_replace(",","",$data['Course']['keywords'])  == ""){
			$errors['keywords'][] = 'Please select any keyword from auto suggest box'."\n";
		}*/
		return $errors;			
	}
	function course_edit($id= NULL , $lesson_deleted = NULL)
	{
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Interest');	
		$this->loadModel('Language');
		$this->loadModel('CourseCategory');
		$this->loadModel('CoursePrice');		
		$login_m_id = $this->Session->read('LocTrain.id');
		if($lesson_deleted =="deleted")
		{
				$this->loadModel('Notification');
				$this->before_update_notification();
				$notification['Notification']['mem_id'] = 0;
				$notification['Notification']['notification_type'] = 'delete_lesson';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $login_m_id;
				$this->Notification->create();
				$this->Notification->save($notification);
				$this->redirect(array('controller'=>'Members','action'=>'course_edit',$id));
		}
		$locale = $this->Session->read('LocTrain.locale');		
		$course_prices = $this->CoursePrice->find('list',array('order'=>'CoursePrice.price ASC','fields'=>array('price','price')));	
		$interest_list = '';
		$pre_fill = '';
		if(!empty($id))
		{
			$this->loadModel('Course');
			$encoded_id = @convert_uudecode(base64_decode($id));
			$course_info = $this->Course->find('first',array('conditions'=>array('Course.id'=>$encoded_id),'contain'=>array('CourseCategory'=>array('fields'=>array('id',$locale)),'CourseSubcategory'=>array('fields'=>array('id',$locale)),'CourseSlSubcategory'=>array('fields'=>array('id',$locale)),'CourseKeyword'=>array('fields'=>array('keyword')),'CourseLesson'=>array('conditions'=>array('CourseLesson.status !='=>'2'),'order'=>'CourseLesson.id ASC'),'CourseRemark'=>array('Member'=>array('id','given_name','family_name')),'Language'=>array('fields'=>array('lang_code','locale')))));	
				
			if(empty($course_info)){
			$this->redirect(array('action'=>'course'));
			}
			if(!empty($course_info['CourseKeyword'])){
			foreach($course_info['CourseKeyword'] as $keyword)
			$pre_fill .= '{value:'."'".$keyword['keyword']."'".", name:"."'".$keyword['keyword']."'},";	
			}
		}
		else if(!empty($id) && empty($this->data)){
			$this->redirect(array('action'=>'course'));
		}
		$categories = $this->CourseCategory->find('all',array('order'=>'CourseCategory.'.$locale.' ASC','fields'=>array('id',$locale),'conditions'=>array('CourseCategory.status'=>1),'contain'=>array('CourseSubcategory'=>array('id','category_id',$locale,'status'=>array('CourseSlSubcategory'=>array('id','sub_category_id',$locale,'status'))))));
		$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest asc'));		
		if(!empty($interest)){
			foreach($interest as $interestVal):			
				$interest_list .= '{value:'."'".$interestVal['Interest']['interest']."'".", name:"."'".$interestVal['Interest']['interest']."'},";	
			endforeach;	
		}else{
			$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
		}	
		$this->set(compact('title_for_layout','interest_list','course_info','course_prices','pre_fill','id','categories'));
		if(!empty($this->data)){
		 $this->request->data['Course']['description']=nl2br(trim($this->data['Course']['description'])); 
			$error = array();
			$error = $this->validate_course($this->data);
                      
			if(count($error) == 0){
				$login_member_id = $this->Session->read('LocTrain.id');
				$this->loadModel('Course');
                                $this->request->data['Course']['meta_title'] = $this->data['Course']['title'];
                                $this->request->data['Course']['meta_description'] = $this->data['Course']['description'];
                                $this->request->data['Course']['meta_keyword'] = $this->data['Course']['keywords'];
                $this->request->data['Course']['locale']= $this->data['Course']['primary_lang'];
				$this->request->data['Course']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$this->request->data['Course']['status'] = 1;
				$this->request->data['Course']['lessons_order']= $this->data['Course']['lessons_order'];
				$price_val= $this->data['Course']['price'];
                $price_val_mod= trim(str_replace(array('$','€','¤'),array('','',''),$price_val));
                $locale=$this->Session->read('LocTrain.locale');
                if($locale=='en' || $locale=='zh')
                {
				$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
				}
				else if($locale=='de' || $locale=="es")
				{
				$price_val1=trim(str_replace(array(','),array('.'),$price_val_mod));
				$count=substr_count($price_val1, '.');
				if($count>1)
				{
				$price_val_new=	preg_replace('/\./', '', $price_val1, 1);
				}
				else
				{
				$price_val_new=$price_val1;
				}	
				}
				else
				{
				$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
				}
				$this->request->data['Course']['price']=$price_val_new;
     			if(!empty($_FILES['image']['name']))
                {
				$destination = 'files/members/courses/';
				$thumb = 'files/members/courses/thumb/';
				$medium = 'files/members/courses/medium/';
				$catalogue = 'files/members/courses/catalogue/';
				$imgName = pathinfo($_FILES['image']['name']);
				$ext = strtolower($imgName['extension']);

				if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
				{
					$newImgName = md5(time()).".".$ext;						
					$img = $_FILES['image'];
					
					App::uses('UploadComponent', 'Controller/Component'); 
					$this->Upload = new UploadComponent(new ComponentCollection());	
													
					$result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resizecrop','size'=>array('262','138'),'quality'=>'90'));	
					$result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));
					$result2 =  $this->Upload->upload($img, $medium, $newImgName, array('type'=>'resizecrop','size'=>array('530','298'), 'quality'=>'90'));
					$result3 =  $this->Upload->upload($img, $catalogue, $newImgName, array('type'=>'resizecrop','size'=>array('256','144'), 'quality'=>'90'));

                    @unlink('files/members/courses/'.$this->data['Course']['old_image']);
                    @unlink('files/members/courses/thumb/'.$this->data['Course']['old_image']);
                    @unlink('files/members/courses/medium/'.$this->data['Course']['old_image']);
					@unlink('files/members/courses/catalogue/'.$this->data['Course']['old_image']);
					 
					// uploading to AWS server 
					App::import('Component', 'Aws');
    				$this->Aws = new AwsComponent();
    				
					//deleteing the old image form aws server
					$old_img=$this->data['Course']['old_image'];
					if(!empty($old_img))
					{
					$keys=array($old_img,'thumb/'.$old_img,'medium/'.$old_img,'catalogue/'.$old_img);
					$this->Aws->deleteFiles(COURSES_BUCKET, $keys);
					}
									
					$key_thumb='thumb/'.$newImgName;
					$key_medium='medium/'.$newImgName;
					$key_catalogue='catalogue/'.$newImgName;
					$key=$newImgName;
					$result_aws = $this->Aws->uploadFile($key, COURSES_BUCKET, $destination.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_thumb, COURSES_BUCKET,$thumb.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_medium, COURSES_BUCKET, $medium.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_catalogue, COURSES_BUCKET, $catalogue.$newImgName);					

					//After

				@unlink('files/members/courses/'.$newImgName);
                @unlink('files/members/courses/thumb/'.$newImgName);
                @unlink('files/members/courses/medium/'.$newImgName);
				@unlink('files/members/courses/catalogue/'.$newImgName);
                $this->request->data['Course']['image'] = $newImgName;
				}
                }
				else
				{
				if(empty($this->data['Course']['old_image'])){
				$this->request->data['Course']['image'] ='';
				}
				}
				if($this->data['Course']['category_id']=='NULL')
				{
				$this->request->data['Course']['category_id']=NULL;
				}
				if($this->data['Course']['subcategory_id']=='NULL')
				{
				$this->request->data['Course']['subcategory_id']=NULL;
				}
				if($this->data['Course']['sl_subcategory_id']=='NULL')
				{
				$this->request->data['Course']['sl_subcategory_id']=NULL;
				}
				if($this->Course->save($this->data)){
					$explodeKeyword = explode(",",$this->data['Course']['keywords']);
					if(!empty($explodeKeyword))
					{
						$this->loadModel('CourseKeyword');
						$this->loadModel('Interest');	
						$this->CourseKeyword->deleteAll(array('CourseKeyword.m_id'=>$login_member_id,'CourseKeyword.course_id'=>$this->data['Course']['id']));
						foreach($explodeKeyword as $words){
							if($words != '')
							{
							$checkInterestExists = $this->Interest->find('count',array('conditions'=>array('Interest.interest'=>trim($words))));
							if($checkInterestExists == 0){
									$interest['Interest']['status']= '1';
									$interest['Interest']['interest']= trim($words);
									$interest['Interest']['date_added']= strtotime(date('Y-m-d H:i:s'));
									$this->Interest->create();
									$this->Interest->save($interest);
							}
							$checCourseKeywordExists = $this->CourseKeyword->find('count',array('conditions'=>array('CourseKeyword.m_id'=>$login_member_id,'CourseKeyword.course_id'=>$this->data['Course']['id'],'CourseKeyword.keyword'=>trim($words))));
									if($checCourseKeywordExists == 0){
									$data['CourseKeyword']['m_id'] =	$login_member_id;
									$data['CourseKeyword']['course_id'] =	$this->data['Course']['id'];
									$data['CourseKeyword']['keyword'] =	trim($words);
									$this->CourseKeyword->create();
									$this->CourseKeyword->save($data);
								}								
							}		
						}
					}
					
					if(trim($this->data['Course']['sub_type']) == '1')
					{
					$this->redirect(array('controller'=>'Members','action'=>'lesson_add',base64_encode(convert_uuencode($this->data['Course']['id']))));
					}
					else
					{
						$this->loadModel('Notification');
						$this->before_update_notification();	
						$notification['Notification']['mem_id'] = 0;
						$notification['Notification']['notification_type'] = 'course_update';
						$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['c_id'] = $this->data['Course']['id'] ;
						$notification['Notification']['lesson_id'] = $lesson_id;
						$notification['Notification']['notification_sender_id'] = $login_m_id;
						$this->Notification->create();
						$this->Notification->save($notification);		
					
				
						$this->redirect($this->referer());
					}
				}
			}
		}
	}
	//------------END OF ADD NEW COURSE
	//------------ADD NEW LESSON IN COURSE
	//------------ADD NEW LESSON IN COURSE
	function lesson_add($c_id=NULL)
	{
		$c_deatils = '';
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';		
		$this->loadModel('Course');
		$this->loadModel('LessonPrice');
		$this->loadModel('Notification');
		$this->loadModel('Language');
		$login_m_id = $this->Session->read('LocTrain.id');
		$encoded_id = @convert_uudecode(base64_decode($c_id));
		$locale = $this->Session->read('LocTrain.locale');
		
		$lesson_prices = $this->LessonPrice->find('list',array('order'=>'LessonPrice.price ASC','fields'=>array('price','price')));
		$course_deatils =	$this->Course->find('first',array('conditions'=>array('Course.id'=>$encoded_id),'contain'=>false));
		if(empty($this->data) && empty($course_deatils)){
			$this->redirect(array('action'=>'course'));
		}
		
		$this->set(compact('course_deatils','c_id','lesson_prices'));
		if(!empty($this->data)){		
			if(!empty($_FILES['image']['name']))
			{
				$destination = 'files/members/course_lesson/';
				$thumb = 'files/members/course_lesson/thumb/';
				$medium = 'files/members/course_lesson/medium/';
				$catalogue = 'files/members/course_lesson/catalogue/';
				
				$imgName = pathinfo($_FILES['image']['name']);
				$ext = strtolower($imgName['extension']);

				if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
				{
					$newImgName = md5(time()).".".$ext;						
					$img = $_FILES['image'];
					
					App::uses('UploadComponent', 'Controller/Component'); 
					$this->Upload = new UploadComponent(new ComponentCollection());	
													
					$result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resizecrop','size'=>array('262','138'),'quality'=>'90'));	
					$result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));
					$result2 =  $this->Upload->upload($img, $medium, $newImgName, array('type'=>'resizecrop','size'=>array('530','298'), 'quality'=>'90'));
					$result3 =  $this->Upload->upload($img, $catalogue, $newImgName, array('type'=>'resizecrop','size'=>array('256','144'), 'quality'=>'90'));
					// uploading to AWS server

					$key_thumb='thumb/'.$newImgName;
					$key_medium='medium/'.$newImgName;
					$key_catalogue='catalogue/'.$newImgName;
					$key=$newImgName;
					 App::import('Component', 'Aws');
    					$this->Aws = new AwsComponent();
					$result_aws = $this->Aws->uploadFile($key, LESSON_BUCKET, $destination.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_thumb, LESSON_BUCKET,$thumb.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_medium, LESSON_BUCKET, $medium.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_catalogue, LESSON_BUCKET, $catalogue.$newImgName);
					

					//After

					@unlink('files/members/course_lesson/'.$newImgName);
                    @unlink('files/members/course_lesson/thumb/'.$newImgName);
                    @unlink('files/members/course_lesson/medium/'.$newImgName);
					@unlink('files/members/course_lesson/catalogue/'.$newImgName);
					

					$this->request->data['CourseLesson']['image'] = $newImgName;			
				}
			}
			
			$this->loadModel('CourseLesson');
			$this->loadModel('Watermark');
				 $price_val= $this->data['CourseLesson']['price'];;
             
                 $price_val_mod= trim(str_replace(array('$','€','¤'),array('','',''),$price_val));
                $locale=$this->Session->read('LocTrain.locale');
                if($locale=='en' || $locale=='zh'){
					$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
					}else if($locale=='de' || $locale=="es"){
						$price_val1=trim(str_replace(array(','),array('.'),$price_val_mod));
						 $count=substr_count($price_val1, '.');
							if($count>1){
							$price_val_new=	preg_replace('/\./', '', $price_val1, 1);
							}else{
			$price_val_new=$price_val1;
			}	
			}
			else
			{
			$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
			}
			$this->request->data['CourseLesson']['description']=nl2br(trim($this->data['CourseLesson']['description'])); 
			$this->request->data['CourseLesson']['price']=$price_val_new;
			$this->request->data['CourseLesson']['date_added'] = 	strtotime(date('Y-m-d H:i:s'));
			$this->request->data['CourseLesson']['status'] =1;
			$courseId=$this->data['CourseLesson']['course_id'];
			$the_course=$this->Course->find('first',array('conditions'=>array('Course.id'=>$courseId),'fields'=>array('duration_seconds')));
			$duration_sec= $the_course['Course']['duration_seconds'];
			$chkLesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>@convert_uudecode(base64_decode($this->data['CourseLesson']['newid'])))));
			if(!empty($chkLesson)){
				$this->request->data['CourseLesson']['id'] = $chkLesson['CourseLesson']['id'];
				if($chkLesson['CourseLesson']['extension'] =='zip'){
				$this->request->data['CourseLesson']['upload_zip_status'] =1;}else{
					$this->request->data['CourseLesson']['upload_zip_status'] =0;
					}
				$this->Watermark->create();
				$candidiate['Watermark']['lesson_id'] = $chkLesson['CourseLesson']['id'];
				$candidiate['Watermark']['video'] = $chkLesson['CourseLesson']['video'];
				$this->Watermark->save($candidiate);
			}
			
			if($this->CourseLesson->save($this->data))
			{
			
				$l_id=$this->CourseLesson->getlastinsertId();
		
		
				$chkLessonvideo = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>@convert_uudecode(base64_decode($this->data['CourseLesson']['newid'])))));
				
				//**** send mail all lesson but not html5 vedio lessons ******
				if($chkLessonvideo['CourseLesson']['video']!='' && $chkLesson['CourseLesson']['extension'] !='zip' )
				{
					
				//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUEs***********************
				$course_title = $this->Course->find('first',array('conditions'=>array('Course.id'=>$this->data['CourseLesson']['course_id']),'contain'=>false,'fields'=>array('title')));
				
				
				$lesson_title = $this->data['CourseLesson']['title'];
				$this->loadModel('Member');
				$mem_info = $this->Member->find('first',array('conditions'=>array('Member.id'=>$this->Session->read('LocTrain.id')),'fields'=>array('given_name','email'),'contain'=>false));
				$final_name = $mem_info['Member']['given_name'];
				$email = $mem_info['Member']['email'];
              //$m_locale = $mem_info['Member']['locale']?$mem_info['Member']['locale']:'en';
				$course_title = $course_title['Course']['title'];
				$this->loadModel('EmailTemplate');
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>11,'locale'=>$locale),'fields'=>array('subject','description','email_from')));
				$emailData = $emailTemplate['EmailTemplate']['description'];
				$emailTo = $email;
				
				$emailData = str_replace(array('{final_name}','{lesson_title}','{course_title}'),array($final_name,$lesson_title,$course_title),$emailData);
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];
				
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
				
				//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUES***********************	
			}
			
			if($chkLessonvideo['CourseLesson']['video']!='' && $chkLesson['CourseLesson']['extension'] =='zip' )
				{
					
				//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUEs***********************
				$course_title = $this->Course->find('first',array('conditions'=>array('Course.id'=>$this->data['CourseLesson']['course_id']),'contain'=>false,'fields'=>array('title')));
				
				
				$lesson_title = $this->data['CourseLesson']['title'];
				$this->loadModel('Member');
				$mem_info = $this->Member->find('first',array('conditions'=>array('Member.id'=>$this->Session->read('LocTrain.id')),'fields'=>array('given_name','email'),'contain'=>false));
				$final_name = $mem_info['Member']['given_name'];
				$email = $mem_info['Member']['email'];
              //$m_locale = $mem_info['Member']['locale']?$mem_info['Member']['locale']:'en';
				$course_title = $course_title['Course']['title'];
				$this->loadModel('EmailTemplate');
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>18,'locale'=>$locale),'fields'=>array('subject','description','email_from')));
				$emailData = $emailTemplate['EmailTemplate']['description'];
				$emailTo = $email;
				
				$emailData = str_replace(array('{final_name}','{lesson_title}','{course_title}'),array($final_name,$lesson_title,$course_title),$emailData);
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];
				
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
				
				//*************SEND CONFIRM MAIL TO RELEASE VIDEO IN CATALOGUES***********************	
			}
				$duration_sec+=($chkLesson['CourseLesson']['hours'])*60*60+($chkLesson['CourseLesson']['mins'])*60 + $chkLesson['CourseLesson']['secs'];
				
				$this->Course->updateAll(array('Course.duration_seconds' =>$duration_sec), array('Course.id' =>$courseId));
			}
				
			$this->before_update_notification();
			if($chkLessonvideo['CourseLesson']['video']!='' && $chkLesson['CourseLesson']['extension'] =='zip' )
				{
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'upload_progress';
			$notification['Notification']['lesson_id'] = $chkLesson['CourseLesson']['id'];			
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			}else{				
				$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'lesson_save';
			$notification['Notification']['lesson_id'] = $chkLesson['CourseLesson']['id'];
			
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
				}
			$this->Notification->create();
			$this->Notification->save($notification);			
			$this->redirect(array('controller'=>'Members','action'=>'course_edit',base64_encode(convert_uuencode($this->data['CourseLesson']['course_id']))));
		}
	}

	function validate_lesson_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_lesson($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['CourseLesson'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_lesson($data)
	{	
		$errors = array ();		
		
		if (trim($data['CourseLesson']['title']) == ""){
			$errors['title'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif(strlen($data['CourseLesson']['title']) > 250 ){
			$errors['title'][] = __("a_maximum_of_250_characters_are_allowed")."\n";
		}
		elseif(ctype_digit($data['CourseLesson']['title'])){
			$errors['title'][] = __("numbers_are_not_allowed")."\n";
		}
		if(empty($data['CourseLesson']['description'])){
			$errors['description'][] = __(FIELD_REQUIRED)."\n";
		}
		if(empty($data['CourseLesson']['price'])){
			$errors['price'][] = __(FIELD_REQUIRED)."\n";
		}	
		$price_val=trim($data['CourseLesson']['price']);
                $price_val_mod= trim(str_replace(array('$','€','¤'),array('','',''),$price_val));
                $locale=$this->Session->read('LocTrain.locale');
                if($locale=='en' || $locale=='zh'){
					$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
					}else if($locale=='de' || $locale=="es"){
						$price_val1=trim(str_replace(array(','),array('.'),$price_val_mod));
						 $count=substr_count($price_val1, '.');
							if($count>1){
							$price_val_new=	preg_replace('/\./', '', $price_val1, 1);
							}else{
								$price_val_new=$price_val1;
							}	
						}
						else {
							$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
						}
							
		if(strlen($price_val_new)<3 || strlen($price_val_new)>7){
			$errors['price'][] =__('enter_a_number_between_000_and_999999')."\n";
			}
        if(trim($data['CourseLesson']['picture']) != '')
		{
			$info = pathinfo($data['CourseLesson']['picture']);
			$ext = strtolower($info['extension']);
			$extArr = array('jpg','jpeg','png','gif');
			
			if( ! in_array($ext,$extArr))
			{
				$errors ['picture'] [] = __('image_format_not_recognized_please_try_again')."\n";
			}
		}		
		/*if(empty($data['CourseLesson']['video'])){
			$errors['video'][] = 'Please upload a video'."\n";
		}*/
		
		return $errors;			
	}
	
	function lesson_edit($l_id=NULL)
	{
		$c_deatils = '';
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';		
		$this->loadModel('CourseLesson');
		$this->loadModel('LessonPrice');
		$this->loadModel('Course');
		$encoded_id = 	@convert_uudecode(base64_decode($l_id));
        $lesson_prices = $this->LessonPrice->find('list',array('order'=>'LessonPrice.price ASC','fields'=>array('price','price')));		
		$lesson_deatils =	$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$encoded_id),'contain'=>array('Course'=>array('fields'=>array('id','title')))));
		
		
		if(empty($this->data) && empty($lesson_deatils)){
			
			$this->redirect(array('action'=>'course'));
		}
		//pr($lesson_deatils); die;
		$this->set(compact('lesson_deatils','l_id','lesson_prices'));
		if(!empty($this->data)){
			$this->request->data['CourseLesson']['description']=nl2br(trim($this->data['CourseLesson']['description']));
			//print_r($this->data);print_r($_FILES['image']);
				$dat=$this->data;
				 $old_img=$dat['CourseLesson']['old_image'];
					$remove_image=$dat['CourseLesson']['remove_image'];
					if(!empty($remove_image)){
					 App::import('Component', 'Aws');
					$this->Aws = new AwsComponent();
					if(!empty($remove_image)){
					$keys=array($remove_image,'thumb/'.$remove_image,'medium/'.$remove_image,'catalogue/'.$remove_image);
					$this->Aws->deleteFiles(LESSON_BUCKET, $keys);
					}
					}

				if(!empty($_FILES['image']['name']))
				{
					$destination = 'files/members/course_lesson/';
					$thumb = 'files/members/course_lesson/thumb/';
					$medium = 'files/members/course_lesson/medium/';
					$catalogue = 'files/members/course_lesson/catalogue/';

					$imgName = pathinfo($_FILES['image']['name']);
					$ext = strtolower($imgName['extension']);

					if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
					{
						$newImgName = md5(time()).".".$ext;						
						$img = $_FILES['image'];
						
						App::uses('UploadComponent', 'Controller/Component'); 
						$this->Upload = new UploadComponent(new ComponentCollection());	
                                              			
						$result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resizecrop','size'=>array('262','138'), 'quality'=>'90'));
							
							
						$result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));
						$result2 =  $this->Upload->upload($img, $medium, $newImgName, array('type'=>'resizecrop','size'=>array('530','298'), 'quality'=>'90'));
						$result3 =  $this->Upload->upload($img, $catalogue, $newImgName, array('type'=>'resizecrop','size'=>array('256','144'), 'quality'=>'90'));

        	// uploading to AWS server

					$key_thumb='thumb/'.$newImgName;
					$key_medium='medium/'.$newImgName;
					$key_catalogue='catalogue/'.$newImgName;
					$key=$newImgName;
					 App::import('Component', 'Aws');
    					$this->Aws = new AwsComponent();
					$result_aws = $this->Aws->uploadFile($key, LESSON_BUCKET, $destination.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_thumb, LESSON_BUCKET,$thumb.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_medium, LESSON_BUCKET, $medium.$newImgName);
					$result_aws = $this->Aws->uploadFile($key_catalogue, LESSON_BUCKET, $catalogue.$newImgName);
					

					//After

					@unlink('files/members/course_lesson/'.$newImgName);
                                        @unlink('files/members/course_lesson/thumb/'.$newImgName);
                                        @unlink('files/members/course_lesson/medium/'.$newImgName);
					@unlink('files/members/course_lesson/catalogue/'.$newImgName);
  
						if($result1)
						{
							//deleteing the old image form aws server
							  $old_img=$dat['CourseLesson']['old_image'];
							if(!empty($old_img)){
							$keys=array($old_img,'thumb/'.$old_img,'medium/'.$old_img,'catalogue/'.$old_img);
							$this->Aws->deleteFiles(LESSON_BUCKET, $keys);
							}
							@unlink('files/members/course_lesson/'.$dat['CourseLesson']['old_image']);
							@unlink('files/members/course_lesson/thumb/'.$dat['CourseLesson']['old_image']);
							@unlink('files/members/course_lesson/medium/'.$dat['CourseLesson']['old_image']);
							@unlink('files/members/course_lesson/catalogue/'.$dat['CourseLesson']['old_image']);
							$dat['CourseLesson']['image'] = $newImgName;		
						}
						}
				}else{if(isset($dat['CourseLesson']['old_image']) && empty($dat['CourseLesson']['old_image'])){							
						$dat['CourseLesson']['image']='';		
					}
}
				
				
				$lesson_deatils =	$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>convert_uudecode(base64_decode($dat['CourseLesson']['newid'])))));
				//pr($lesson_deatils);die;
				$current_lesson_video	= $lesson_deatils['CourseLesson']['video'];
				$previous_lesson_video = $dat['CourseLesson']['previous_lesson_video'];
				$previous_lesson_ext = $dat['CourseLesson']['previous_lesson_video_extension'];
				//pr(gettype($previous_lesson_video)); pr(gettype($current_lesson_video));
				if($previous_lesson_video != $current_lesson_video)
				{
					$this->delete_lesson_file($previous_lesson_video, $previous_lesson_ext);
				}
				unset($dat['CourseLesson']['previous_lesson_video']);
				unset($dat['CourseLesson']['previous_lesson_video_extension']);
							
			$this->LoadModel('Watermark');
			$this->LoadModel('CourseLesson');
			
			$courseId=@convert_uudecode(base64_decode($dat['CourseLesson']['course_id']));
			$the_course=$this->Course->find('first',array('conditions'=>array('Course.id'=>$courseId),'fields'=>array('duration_seconds')));
			$duration_sec= $the_course['Course']['duration_seconds'];
			
			//$this->request->data['CourseLesson']['id'] = @convert_uudecode(base64_decode($this->data['CourseLesson']['newid']));
			$dat['CourseLesson']['course_id'] = @convert_uudecode(base64_decode($dat['CourseLesson']['course_id']));
			$dat['CourseLesson']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$dat['CourseLesson']['status'] ='1';
				 $price_val= $this->data['CourseLesson']['price'];
				  $price_val_mod= trim(str_replace(array('$','€','¤'),array('','',''),$price_val));
                $locale=$this->Session->read('LocTrain.locale');
                if($locale=='en' || $locale=='zh'){
					$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
					}else if($locale=='de' || $locale=="es"){
						$price_val1=trim(str_replace(array(','),array('.'),$price_val_mod));
						 $count=substr_count($price_val1, '.');
							if($count>1){
							$price_val_new=	preg_replace('/\./', '', $price_val1, 1);
							}else{
								$price_val_new=$price_val1;
							}	
						}
						else {
							$price_val_new=trim(str_replace(array(','),array(''),$price_val_mod));
						}
					$dat['CourseLesson']['price']=$price_val_new;
                //$dat['CourseLesson']['price']=trim(str_replace(array('$','€','¤',','),array('','','','.'),$price_val));
			$chkLesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>@convert_uudecode(base64_decode($dat['CourseLesson']['newid'])))));
			
			if(!empty($chkLesson)){
				$dat['CourseLesson']['id'] = $chkLesson['CourseLesson']['id'];
				$this->Watermark->create();
				$candidiate['Watermark']['lesson_id'] = $chkLesson['CourseLesson']['id'];
				$candidiate['Watermark']['video'] = $chkLesson['CourseLesson']['video'];
				$this->Watermark->save($candidiate);
			}
			
			if($this->CourseLesson->save($dat))
			{
				//$duration_sec+=($chkLesson['CourseLesson']['hours'])*60*60+($chkLesson['CourseLesson']['mins'])*60 + $chkLesson['CourseLesson']['secs'];
				//echo $duration_sec;echo $encoded_id;die;
				//$this->Course->updateAll(array('Course.duration_seconds' =>$duration_sec), array('Course.id' =>$courseId));
			}
			$this->before_update_notification();	
			$login_m_id = $this->Session->read('LocTrain.id');
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'lesson_update';
			$notification['Notification']['lesson_id'] = $lesson_deatils['CourseLesson']['id'];
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $login_m_id;
			$this->Notification->create();
			$this->Notification->save($notification);

		//	$this->Session->write('LocTrain.flashMsg','Your lesson has been successfully updated.');
			$this->redirect(array('controller'=>'Members','action'=>'course_edit',base64_encode(convert_uuencode($dat['CourseLesson']['course_id']))));
		}
		
		
		
		
	}
function deleteFiles($keys,$bucket){
// uploading to AWS server 
App::import('Component', 'Aws');
$this->Aws = new AwsComponent();
if(!empty($keys)){
foreach($keys as $key){
$this->Aws->deleteFile($bucket,$key);
}
}
//$this->Aws->deleteFiles($bucket, $keys);
}
function url_exists($url) {
    if (!$fp = curl_init($url)) return false;
    return true;
}
	function remove_lesson_video($id=NULL)
	{
		$this->layout = NULL;
		$this->autoRender = NULL;
		$this->loadModel('CourseLesson');
		$this->loadModel('Course');
		
                
		App::import('Helper', 'Utility'); 
		$this->Utility = new UtilityHelper(new View(null));
		$decode_id = convert_uudecode(base64_decode($id));		
		$course_lesson=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'fields'=>array('hours','mins','secs')));		
		$checkRecord = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'contain'=>false));		

		if(!empty($checkRecord['CourseLesson']['video'])){
		
			if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.flv"))){

				$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.flv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".flv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png",
				$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4",
				$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4",
				$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf",
				$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
			}
			if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".wmv"))){

				$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".wmv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.wmv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
			}
			if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mov"))){

				$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mov",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mov",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
			}
			if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4"))){

				$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
			}

			if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png"))){

				$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

			}

			if($checkRecord['CourseLesson']['extension']=='zip')
			{


				$file_name=$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'];
				// update the status delete_zip_status to 1 (only delete video from aws)				
				$this->CourseLesson->updateAll(array('CourseLesson.delete_zip_status'=>1,'CourseLesson.to_delete_video'=>"'".$file_name."'"),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
				
			}

			if($checkRecord['CourseLesson']['extension']=='swf')
			{
				if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))){

					$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'],$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'_preview.'.$checkRecord['CourseLesson']['extension']);
					$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
				}	
			}
			// for loal server
			if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_watermark.flv")){
				
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_watermark.flv");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.swf");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf");

			}
			if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png")){
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.swf");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf");
			}
			if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png")){
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
			}
         
			if($checkRecord['CourseLesson']['extension']=='swf')
			{
				if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))
				{ 
					unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']);
					if($checkRecord['CourseLesson']['image'] && file_exists('files/members/course_lesson/'.$checkRecord['CourseLesson']['image']))
                    {
					}
				}
				
			}
			if($checkRecord['CourseLesson']['extension']=='zip' || $checkRecord['CourseLesson']['extension']=='swf'){
				$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.extension'=>NULL,'CourseLesson.mins'=>'0','CourseLesson.secs'=>'0'),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
			}else {
				$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.extension'=>NULL,'CourseLesson.mins'=>'0','CourseLesson.secs'=>'0'),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
			}
			$this->loadModel('Notification');
			$this->before_update_notification();
			$notification['Notification']['mem_id'] = 0;
			$notification['Notification']['notification_type'] = 'video_is_being_deleted';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $this->Session->read('LocTrain.id');
			$this->Notification->create();
			$this->Notification->save($notification);
			
			echo 'done';
		}else{
			echo 'done';
		}
		die;
	}
	function remove_lesson_video_delete($id=NULL)
	{
            
		$this->layout = NULL;
		$this->autoRender = NULL;
		$this->loadModel('CourseLesson');
		$this->loadModel('Course');
		
		App::import('Helper', 'Utility'); 
		$this->Utility = new UtilityHelper(new View(null));
		$decode_id = convert_uudecode(base64_decode($id));		
		$course_lesson=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'fields'=>array('hours','mins','secs')));		
		$checkRecord = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'contain'=>false));		

		if(!empty($checkRecord['CourseLesson']['video'])){

		
			if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.flv"))){

$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.flv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".flv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png",
	   $this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4",
        $this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4",
      $this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf",
		$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

				}
	if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".wmv"))){

$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".wmv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.wmv",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

				}
if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mov"))){

$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mov",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mov",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

				}
if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4"))){

$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_catalogueThumb.png",$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

				}

if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png"))){

$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
				$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);

				}

			if($checkRecord['CourseLesson']['extension']=='zip')
			{$file_name=$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'];
				// update the status delete_zip_status to 2				
				$this->CourseLesson->updateAll(array('CourseLesson.delete_zip_status'=>2,'CourseLesson.to_delete_video'=>"'".$file_name."'"),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
				
			
			}

			if($checkRecord['CourseLesson']['extension']=='swf')
			{

				if($this->url_exists($this->Utility->getAWSUrl(LESSON_VIDEOS_BUCKET,$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))){

					$keys=array($this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension'],$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'_preview.'.$checkRecord['CourseLesson']['extension']);
					$this->deleteFiles($keys,LESSON_VIDEOS_BUCKET);
				}			
				
			}
		// for loal server

			if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_watermark.flv")){
				
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_watermark.flv");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4");

                                unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.swf");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf");


			}
			if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png")){
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_thumb.png");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".mp4");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.mp4");


                                 unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_preview.swf");
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].".swf");
			


			}
			if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png")){
				unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']."_smallThumb.png");
			}
            if($checkRecord['CourseLesson']['extension']=='zip')
			{
				if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']))
				{ 
					$path = realpath('../../app/webroot/files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video']);                          
					exec('rm -r '.$path);
				}
				
			}
			if($checkRecord['CourseLesson']['extension']=='swf')
			{
				if(file_exists('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']))
				{ 
					unlink('files/members/'.$this->Session->read('LocTrain.id')."/".$checkRecord['CourseLesson']['video'].'.'.$checkRecord['CourseLesson']['extension']);
					if($checkRecord['CourseLesson']['image'] && file_exists('files/members/course_lesson/'.$checkRecord['CourseLesson']['image']))
                    {
						
						
					}
 					
				}
				
			}
			if($checkRecord['CourseLesson']['extension']=='zip' || $checkRecord['CourseLesson']['extension']=='swf'){
				
			$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.extension'=>NULL,'CourseLesson.mins'=>'0','CourseLesson.secs'=>'0'),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
			}else {
				$this->CourseLesson->updateAll(array('CourseLesson.video'=>NULL,'CourseLesson.extension'=>NULL,'CourseLesson.mins'=>'0','CourseLesson.secs'=>'0'),array('CourseLesson.id'=>$checkRecord['CourseLesson']['id']));
			}
			
			//echo 'done';
		}else{
			//echo 'done';
		}
		
	}

// function for  update profile //////	
	function validate_updateprofile_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_updateprofile($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Member'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}

	function validate_updateprofile($data)
	{			
		$errors = array ();		
		if($data['Member']['given_name'] == ''){
			$errors['given_name'][] = __(FIELD_REQUIRED)."\n";
		}
		if(is_numeric($data['Member']['given_name']))
		{
			$errors ['given_name'] [] = __("numbers_are_not_allowed")."\n";
		}
		/*if($data['Member']['final_name'] == ''){
			$errors['final_name'][] = __(FIELD_REQUIRED)."\n";
		}
		if(is_numeric($data['Member']['final_name']))
		{
			$errors ['final_name'] [] = __("numbers_are_not_allowed")."\n";
		}*/
		
		if($data['Member']['email'] == ''){
			$errors['email'][] = __(FIELD_REQUIRED)."\n";
		} else {
			if($this->validEmail(trim($data['Member']['email'])) == 'false'){
				$errors['email'][] = __(INVALID_EMAIL)."\n";
			}
			else if($this->Session->read('LocTrain.email') != $data['Member']['email'])
			{
				$this->loadModel('Member');
				$chk = $this->Member->find('count',array('conditions'=>array('Member.id !='=>$this->Session->read('LocTrain.id'),'Member.email'=>$data['Member']['email'])));
				if($chk > 0)
				{
					$errors['email'][] = __(EMAIL_EXISTS)."\n";
				}
			}
		}
		/*if($this->Session->read('LocTrain.member_type') == 3)
		{*/
                         $arr_main_array=$data['Member'];
                        // print_r($arr_main_array);
                            $i=0;
                            foreach($arr_main_array as $key => $value){
                            $exp_key = explode('-', $key);  
                            
                            if($exp_key[0] == 'about_me'){
                                 $arr_result[$i]['about_me'] = $value;                                
                                 if(trim($data['Member'][$key]) == '')
                                    {
                                        $errors [$key] [] = __(FIELD_REQUIRED)."\n";
                                    }
                            }
                            if($exp_key[0] == 'lang_locale'){
                                 $arr_result[$i]['lang_locale'] = $value;
                                 if(trim($data['Member'][$key]) == '')
                                    {
                                        $errors [$key] [] = __(FIELD_REQUIRED)."\n";
                                    }
                            }
                            $i++;
                            }
//print_r($errors);
			/*if(trim($data['Member']['about_me']) == '')
			{
				$errors ['about_me'] [] = __(FIELD_REQUIRED)."\n";
			}*/
                        if($data['Member']['check_form']==1){
                        if($data['Member']['notification'] == '' || $data['Member']['notification'] > '1'){
			$errors['notification'][] = __(FIELD_REQUIRED)."\n";
                        }
			if(isset($data['Member']['picture']) && trim($data['Member']['picture']) != '')
			{
				$info = pathinfo($data['Member']['picture']);
				$ext = strtolower($info['extension']);
				$extArr = array('jpg','jpeg','png','gif');
				
				if( ! in_array($ext,$extArr))
				{
					$errors ['picture'] [] = __('image_format_not_recognized_please_try_again')."\n";
				}
			}
                        }else{
                            if($data['Member']['notification_1'] == '' || $data['Member']['notification_1'] > '1'){
			$errors['notification_1'][] = __(FIELD_REQUIRED)."\n";
                            }
                        if(isset($data['Member']['picture_1']) && trim($data['Member']['picture_1']) != '')
			{
				$info = pathinfo($data['Member']['picture_1']);
				$ext = strtolower($info['extension']);
				$extArr = array('jpg','jpeg','png','gif');
				
				if( ! in_array($ext,$extArr))
				{
					$errors ['picture_1'] [] = __('image_format_not_recognized_please_try_again')."\n";
				}
			}
                        }
		/*}*/
		return $errors;			
	}
	
	function update_profile($id=NULL)
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Interest');
		$this->loadModel('MemberSubscription');
		$interest_list = '';
		$selectinterest_list='';
		$date = date("Y-m-d H:i:s");
		$locale = $this->Session->read('LocTrain.locale');
		if(!empty($id)){
			$encode_id = @convert_uudecode(base64_decode($id));		
			if($encode_id == 'reset'){
				$pop_up = 'pop_up';
				$this->set('pop_up',$pop_up);
			}
		}	
		$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest asc'));	
	
		if(!empty($interest)){
			foreach($interest as $interestVal):			
				$interest_list .= '{value:'."'".$interestVal['Interest']['interest']."'".", name:"."'".$interestVal['Interest']['interest']."'},";	
			endforeach;	
		}else{
		
			$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
		}
              
	
		$this->loadModel('Member');
               $this->loadModel('Language');

            //  $language = $this->Language->find('all',array('conditions'=>array('Language.status'=>'1'),'fields'=>array('id','language','locale')));
             $language = $this->Language->find('all',array('conditions'=>array('Language.status'=>'1'),'fields'=>array('id',$locale,'locale')));
			// pr($language);die;
		$login_member_id = $this->Session->read("LocTrain.id");
		$memberDetail =	$this->Member->find('first',array('conditions'=>array('Member.id'=>$login_member_id),'contain'=>array('MemberKeyword'=>array('fields'=>array('keyword')))));
		$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_member_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1)));
		if(!empty($memberDetail['MemberKeyword'])){
			foreach($memberDetail['MemberKeyword'] as $interestVal):	
				$selectinterest_list .= '{value:'."'".$interestVal['keyword']."'".", name:"."'".$interestVal['keyword']."'},";	
			endforeach;
		}
		$this->set(compact('title_for_layout','interest_list','selectinterest_list','memberDetail','subscribed','language'));

		if(!empty($this->data)){
                          ///
                            $arr_main_array=$this->data['Member'];
                            
                            $arr_result=array();
                            foreach($arr_main_array as $key => $value){
                            $exp_key = explode('-', $key);
                            if($exp_key[0] == 'about_me'){
                                 $arr_result[$exp_key[1]]['about_me'] = $value;
                            }
                            if($exp_key[0] == 'lang_locale'){
                                 $arr_result[$exp_key[1]]['lang_locale'] = $value;
                            }
                           
                            }
                            $arr_result = array_values($arr_result);
                            $about_me=  json_encode($arr_result);
                            $this->request->data['Member']['about_me']=$about_me;
                            $error = array();
                      //  print_r($this->data);
			$error = $this->validate_updateprofile($this->data);
			if(count($error) == 0){
			
			//echo '<pre>';print_r($this->data);echo '</pre>';
				$this->loadModel('Member');
				$login_m_id = $this->Session->read("LocTrain.id");
				
				$this->Session->write('LocTrain.email',$this->data['Member']['email']);
				
				$this->request->data['Member']['id'] =	$login_m_id;			
				$this->request->data['Member']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                       if($this->data['Member']['check_form']==1){//$this->request->data['Member']['notification']=$this->data['Member']['notification'];


                                        if(!empty($_FILES['image']['name']))
                                        {
                                                //$destination = 'files/members/profile/';
                                                //$thumb = 'files/members/profile/thumb/';
						/** Added by Neema **/	
						$destination= realpath('../../app/webroot/files/members/profile').'/';
						$thumb= realpath('../../app/webroot/files/members/profile/thumb').'/';
						/** Added by Neema **/
                                                $imgName = pathinfo($_FILES['image']['name']);
                                                $ext = strtolower($imgName['extension']);

                                                if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
                                                {
                                                        $newImgName = md5(time()).".".$ext;						
                                                        $img = $_FILES['image'];

                                                        App::uses('UploadComponent', 'Controller/Component'); 
                                                        $this->Upload = new UploadComponent(new ComponentCollection());	

                                                        $result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resize','size'=>'250', 'quality'=>'90'));	
                                                        $result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));




                                                        if($result1)
                                                        {
                                                             //  @unlink('files/members/profile/'.$this->data['Member']['old_image']);
                                                                //@unlink('files/members/profile/thumb/'.$this->data['Member']['old_image']);
                                                                
                                                        // uploading to AWS server 
														App::import('Component', 'Aws');
														$this->Aws = new AwsComponent();
														if(!empty($this->data['Member']['old_image'])){
															//deleteing the old image form aws server
															$old_img=$this->data['Member']['old_image'];
															$keys=array($old_img,'thumb/'.$old_img);
															$this->Aws->deleteFiles(MEMBERS_BUCKET, $keys);
														}
														$key_thumb='thumb/'.$newImgName; 
														$key=$newImgName;
														$result_aws = $this->Aws->uploadFile($key, MEMBERS_BUCKET, $destination.$newImgName);
														$result_aws = $this->Aws->uploadFile($key_thumb, MEMBERS_BUCKET,$thumb.$newImgName);
															@unlink($destination.$newImgName);
															@unlink($thumb.$newImgName);
                                       

														
                                                        $this->request->data['Member']['image'] = $newImgName;			
                                                        }
                                                }
                                        }
                                        else
										{
											 // uploading to AWS server 
														App::import('Component', 'Aws');
														$this->Aws = new AwsComponent();
														//deleteing the old image form aws server
														$old_img=$this->data['Member']['old_image'];
														$keys=array($old_img,'thumb/'.$old_img);
														
														$this->Aws->deleteFiles(MEMBERS_BUCKET, $keys);
													
										$this->request->data['Member']['image'] = '';	
										}
                                }elseif($this->data['Member']['check_form']==0){


 //$this->request->data['Member']['notification']=$this->data['Member']['notification_1'];
                                  if(!empty($_FILES['image_1']['name']))
                                        {


 
                                                //$destination = 'files/members/profile/';
                                                //$thumb = 'files/members/profile/thumb/';
						/** Added by Neema **/	
						$destination= realpath('../../app/webroot/files/members/profile').'/';
						$thumb= realpath('../../app/webroot/files/members/profile/thumb').'/';
						/** Added by Neema **/
                                                $imgName = pathinfo($_FILES['image_1']['name']);
                                                $ext = strtolower($imgName['extension']);

                                                if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
                                                {
                                                        $newImgName = md5(time()).".".$ext;						
                                                        $img = $_FILES['image_1'];

                                                        App::uses('UploadComponent', 'Controller/Component'); 
                                                        $this->Upload = new UploadComponent(new ComponentCollection());	

                                                        $result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resize','size'=>'250', 'quality'=>'90'));	
                                                        $result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'90'));

                                                        if($result1)
                                                        {
                                                               // @unlink('files/members/profile/'.$this->data['Member']['old_image_1']);
                                                              //  @unlink('files/members/profile/thumb/'.$this->data['Member']['old_image_1']);
                                                                
                                                                 // uploading to AWS server 
														App::import('Component', 'Aws');
														$this->Aws = new AwsComponent();
														//deleteing the old image form aws server
									$old_img=$this->data['Member']['old_image_1'];
								$keys=array($old_img,'thumb/'.$old_img);
									if(!empty($old_img)){					
								$this->Aws->deleteFiles(MEMBERS_BUCKET, $keys);}
														$key_thumb='thumb/'.$newImgName;
														$key=$newImgName;
														$result_aws = $this->Aws->uploadFile($key, MEMBERS_BUCKET, $destination.$newImgName);
														$result_aws = $this->Aws->uploadFile($key_thumb, MEMBERS_BUCKET,$thumb.$newImgName);
														
																@unlink($destination.$newImgName);
											                                        @unlink($thumb.$newImgName);
                                       
														
                                                                $this->request->data['Member']['image'] = $newImgName;			
                                                        }
                                                }
                                        } 


                                }


				if($this->data['Member']['notification_chk']==1){
                                 $this->request->data['Member']['notification']=$this->data['Member']['notification_1'];
                                }else{
 				$this->request->data['Member']['notification']=$this->data['Member']['notification'];
				}
					//echo '<pre>';print_r($this->data);echo '</pre>';die;
				if($this->Member->save($this->data)){
					$keys=explode(",",$this->data['Member']['keyword']);
					$this->loadModel('MemberKeyword');
					$this->loadModel('Interest');
					$this->MemberKeyword->deleteAll(array('MemberKeyword.m_id'=>$login_m_id));
					foreach($keys as $key){
					 	if($key != ""){
							$checkInterestExists = $this->Interest->find('count',array('conditions'=>array('Interest.interest'=>trim($key))));
							
							if($checkInterestExists == 0){
								$interest['Interest']['status'] = '1';
								$interest['Interest']['interest'] = trim($key);
								$interest['Interest']['date_added'] = strtotime(date('Y-m-d H:i:s'));
								$this->Interest->create();
								$this->Interest->save($interest);
							}
							$checkMemberKeywordExists = $this->MemberKeyword->find('count',array('conditions'=>array('MemberKeyword.m_id'=>$login_m_id,'MemberKeyword.keyword'=>trim($key))));
							if($checkMemberKeywordExists == 0){
								$this->request->data['MemberKeyword']['m_id'] = $login_m_id;
								$this->request->data['MemberKeyword']['keyword'] = $key;
								$this->MemberKeyword->create();
								$this->MemberKeyword->save($this->data);
							}							
						}
					 }
					$this->before_update_notification();
					$this->loadModel('Notification');
					$notification['Notification']['mem_id'] =  0;
					$notification['Notification']['notification_type'] = 'update_profile';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['notification_sender_id'] = $login_m_id;
					$this->Notification->create();
					$this->Notification->save($notification);
		
					// updating user interface lang according to member locale
					
					 $lang =$this->request->data['Member']['locale'];	
					if(!empty($language))
					    	{
						    foreach($language as $lang)
						    {
					 		if($lang['Language']['locale']==  $this->request->data['Member']['locale']){
								$langId=$lang['Language']['id'];
							}
		 				    }
						}				
					$language = array('en_US'=>'en','de_DE'=>'de','ar_AR'=>'ar','fr_FR'=>'fr','it_IT'=>'it','ja_JP'=>'ja','ko_KR'=>'ko','pt_PT'=>'pt','ru_RU'=>'ru','zh_ZH'=>'zh','es_ES'=>'es');
						
				
					if(!empty($lang) && in_array($this->request->data['Member']['locale'],$language) && !empty($langId))
					{
						$val = array_search($this->request->data['Member']['locale'],$language);
						$this->Cookie->write('LocTrain.locale',$this->request->data['Member']['locale'],false,60*60*24*365);
						$this->Cookie->write('LocTrain.LangId',$langId,false,60*60*24*365);
						$this->Session->write('Config.language',$val);
						$this->Session->write('LocTrain.locale',$this->request->data['Member']['locale']);
						$this->Session->write('LocTrain.LangId',$langId);
				
						if($this->Session->read('LocTrain.locale')=="ar")
						{ 
							$this->Cookie->write('LocTrain.style_arabic','style_arabic',false,60*60*24*365);
						} else {
				
							$this->Cookie->write('LocTrain.style','style',false,60*60*24*365);
						}
				
						//pr($val); die;
					}
					 
					//$this->Session->write("LocTrain.flashMsg",__('Profile updated.'));					
					$this->redirect(array('action'=>'update_profile'));		
				}
			}else{
				$this->set(compact('error'));
			}
		}
	}
/// end of update profile function //
	function reset_password($id=NULL)
	{  
	
	
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		if(empty($this->data) && $this->Session->read('LocTrain.succ_reset') != $id){
			$this->redirect(array('controller'=>'Home','action'=>'index'));
		}
		if(!empty($this->data)){
			$error = array();
			$error = $this->validate_reset_password_ajax($this->data);
			if(count($error) == 0){
				$memberId = $this->Session->read('LocTrain.id');
			
				$this->Member->updateAll(array('Member.password'=>'"'.md5($this->data['change_password']['new_password']).'"'),array('Member.id'=>$memberId));
				$this->before_update_notification();
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'password_changed';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $memberId;
				$this->Notification->create();
				$this->Notification->save($notification);
				
			//	$this->Session->write('LocTrain.flashMsg','Your password has been changed.');
				$this->redirect(array('controller'=>'Members','action'=>'update_profile'));
			}
		}
	}
	function validate_reset_password_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_reset_password($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['change_password'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}	
	function validate_reset_password($data)
	{			
		$errors = array ();			
		
		if(trim($data['change_password']['new_password']) == ''){
			$errors['new_password'][] = __(FIELD_REQUIRED)."\n";
		}
		else if(trim($data['change_password']['new_password']) != ''){		
			if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()\-_=+{};:,<.>~]).{8,20}$/", trim($data['change_password']['new_password']))) {
				$errors['new_password'][] = __("please_choose_a_password_consisting_of")." ".__("a_minimum_of_8_and_maximum_of_20_characters").','.__("at_least_one_upper_case_character").','. __("at_least_one_lower_case_character").','. __("at_least_one_number").','.__("at_least_one_non_alphanumeric_character")."\n";
			}
		}
		if (trim($data['change_password']['confirm_password']) == ""){
			$errors['confirm_password'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif(trim($data['change_password']['new_password']) != '' && trim($data['change_password']['new_password'] != trim($data['change_password']['confirm_password'])))	{
			$errors['confirm_password'][] = __(PASSWORD_MISTACHMATCH)."\n";
		}
		
		return $errors;			
	}	
	function change_password()
	{  
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		
		if(!empty($this->data)){
			$error = array();
			$error = $this->validate_changePassword($this->data);
			if(count($error) == 0){
				$memberId = $this->Session->read('LocTrain.id');
			
				$this->Member->updateAll(array('Member.password'=>'"'.md5($this->data['change_password']['new_password']).'"'),array('Member.id'=>$memberId));
				
				$this->before_update_notification();
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] =  0;
				$notification['Notification']['notification_type'] = 'password_changed';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['notification_sender_id'] = $memberId;
				$this->Notification->create();
				$this->Notification->save($notification);
             //   $this->Session->write('LocTrain.flashMsg','Your password has been changed.');
				$this->redirect(array('controller'=>'Members','action'=>'update_profile'));
			}
		}
   }
    function validate_changePassword_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_changePassword($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['change_password'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_changePassword($data)
	{			
		$errors = array ();			
		if(trim($data['change_password']['old_password']) == ''){
			$errors['old_password'][] = __(FIELD_REQUIRED)."\n";
		}
		if(trim($data['change_password']['old_password'])!= ''){
		$this->loadModel('Member');
		$chek_old = $this->Member->find('count',array('conditions'=>array('Member.password'=>md5($data['change_password']['old_password']),'Member.id'=>$this->Session->read("LocTrain.id"))));
			if($chek_old == 0){
				$errors['old_password'][] = __(WRONG_PASSWORD)."\n";
			}
		}
		if(trim($data['change_password']['new_password']) == ''){
			$errors['new_password'][] = __(FIELD_REQUIRED)."\n";
		}
		else if(trim($data['change_password']['new_password']) != ''){		
			if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()\-_=+{};:,<.>~]).{8,20}$/", trim($data['change_password']['new_password']))) {
				$errors['new_password'][] = __(PASSWORD_VALIDATION2)."\n";
			}
		}
		if (trim($data['change_password']['confirm_password']) == ""){
			$errors['confirm_password'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif((trim($data['change_password']['old_password']) !="" && trim($data['change_password']['new_password']) != '') && (trim($data['change_password']['new_password']) != trim($data['change_password']['confirm_password'])))	{
			$errors['confirm_password'][] = __(PASSWORD_MISTACHMATCH)."\n";
		}
		
		return $errors;			
	}
	function upgrade_membership()
	{ 	
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		
		if($this->Session->read('LocTrain.id')=='')
		{
			$temp_id = $this->Session->read('LocTrain.visitor_id');
			$this->before_update_notification();
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] =  0;
			$notification['Notification']['notification_type'] = 'member_not_login';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['notification_sender_id'] = $temp_id;
			$notification['Notification']['read'] = 1;
			$this->Notification->create();
			$this->Notification->save($notification);
			$this->redirect(array('controller'=>'Home','action'=>'register'));
		}
		
		$this->loadModel('Interest');
		$this->loadModel('CourseKeyword');	
		$this->loadModel('CmsPage');
		
		$locale = $this->Session->read('LocTrain.locale');
		$become_author = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.page'=>'become_author','CmsPage.locale'=>$locale),'contain'=>array()));
		
		
		
		$interest_list = '';
		$arr = array();
		$max = array();
		$interest = $this->Interest->find('all',array('conditions'=>array('Interest.status'=>'1'),'fields'=>array('interest'),'order'=>'Interest.interest ASC'));		
		if(!empty($interest)){
			foreach($interest as $interestVal):
				$check = $this->CourseKeyword->find('count',array('conditions'=>array('CourseKeyword.keyword'=>trim($interestVal['Interest']['interest']))));
				static $i = 0;
				if($check > 0){
					$arr[$i]['keyword'] = $interestVal['Interest']['interest'];
					$arr[$i]['count'] = $check;
					$max[$i] = $check;
					$i++;
				}	
				$interest_list .= '{value:'."'".$interestVal['Interest']['interest']."'".", name:"."'".$interestVal['Interest']['interest']."'},";	
			endforeach;
			unset($i);	
			if(!empty($max)){
				$max = max($max);
			}
		}else{
			$interest_list .= '{value:'."'".''."'".", name:"."'".''."'},";	
		}	
		
		//pr($arr); die;
		$cloud = '<div class="inner_catalogue">
                    <div class="main_catalogue">
                    <p class="ipsumpara">';
                        if(!empty($arr)) { 
						 foreach($arr as $arr) { 
						 $cloud .='<a href="'.HTTP_ROOT.'Home/search/?q='.$arr['keyword'].'" style="text-decoration: underline;color:#3F3F3F;" >' ;
						   $per =  $arr['count']/$max * 100; 
                            	 if($per < 20){
                            		$cloud .='<span class="quis1s keyword_hover">'.$arr['keyword'].'</span>';
								 } elseif($per >= 20 && $per < 40) { 
                                	$cloud .='<span class="aliqua keyword_hover">'.$arr['keyword'].'</span>'; 
                                 } elseif($per >= 40 && $per < 60)  { 
                                	$cloud .='<span class="minim keyword_hover">'.$arr['keyword'].'</span>';
                                 } elseif($per >= 60 && $per < 80)  { 
                                	$cloud .='<span class="ipsum_sm keyword_hover">'.$arr['keyword'].'</span>';                                 
                                 } else {                                 	
                                	$cloud .='<span class="ipsum_size keyword_hover">'.$arr['keyword'].'</span>';                                	 
                                 }                                 
							 } 
                         $cloud .='</a></p>
                    </div>';
						 } else {  
                        	$cloud .='<div class="mytch_rcd">There are no keywords available.</div>';                        	
					 } 
                     
                  $cloud .='</div>';
		 $this->loadModel('Member');				
		$login_m_id = $this->Session->read('LocTrain.id');
		$check_author_member = $this->Member->find('count',array('conditions'=>array('Member.id'=>$login_m_id,'Member.role_id'=>'3'),'contain'=>false));
		$member_details = $this->Member->find('first',array('conditions'=>array('Member.id'=>$login_m_id,'Member.upgrade_profile'=>'1'),'contain'=>false));

		$description = str_replace(array('{word_cloud}'),array($cloud),$become_author['CmsPage']['description']);
		$title = $become_author['CmsPage']['page_title'];
		$this->set(compact('title_for_layout','check_author_member','member_details','interest_list','arr','max','description','title'));
		
		if(!empty($this->data)){
				$error = array();
				$error = $this->validate_upgrade($this->data);
				if(count($error) == 0){
					$this->loadModel('Member');				
					$login_m_id = $this->Session->read('LocTrain.id');
					$member_details = $this->Member->find('first',array('conditions'=>array('Member.id'=>$login_m_id),'fields'=>array('final_name','email'),'contain'=>false));			
						
					$this->request->data['Member']['id'] = $login_m_id;
					$this->request->data['Member']['date_request'] = strtotime(date('Y-m-d H:i:s'));
					$this->request->data['Member']['upgrade_profile'] = '1';				
					if($this->Member->save($this->data)){	
						$this->loadModel('MemberAuthorKeyword');
						$this->loadModel('EmailTemplate');	
						$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.main_id'=>'3','EmailTemplate.locale'=>$locale)));
						$emailData = $emailTemplate['EmailTemplate']['description'];
						$emailData = str_replace(array('{m_name}','{m_email}'),array($member_details['Member']['final_name'],$member_details['Member']['email']),$emailData);					
													
						if(isset($this->data['Member']['author_keyword']) && str_replace(",","",$this->data['Member']['author_keyword']) != ''){
							$explodeKeywords = explode(",",$this->data['Member']['author_keyword']);
							foreach($explodeKeywords as $keywords){
								if(trim($keywords) != ''){
									$checkInterestExists = $this->Interest->find('count',array('conditions'=>array('Interest.interest'=>trim($keywords))));
									if($checkInterestExists == 0){
										$interest['Interest']['status'] = '1';
										$interest['Interest']['interest'] = trim($keywords);
										$interest['Interest']['date_added'] = strtotime(date('Y-m-d H:i:s'));
										$this->Interest->create();
										$this->Interest->save($interest);
									}
									$checkAuthorMemberKeywordExists = $this->MemberAuthorKeyword->find('count',array('conditions'=>array('MemberAuthorKeyword.m_id'=>$login_m_id,'MemberAuthorKeyword.keyword'=>trim($keywords))));
									if($checkAuthorMemberKeywordExists == 0){
										$authorKeyword['MemberAuthorKeyword']['m_id'] = $login_m_id;
										$authorKeyword['MemberAuthorKeyword']['keyword'] = trim($keywords);
										$this->MemberAuthorKeyword->create();
										$this->MemberAuthorKeyword->save($authorKeyword);
									}
								}
							}
						}
						$emailTo = ADMIN_EMAIL;
						$emailCc = $emailTemplate['EmailTemplate']['email_to'];
						$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
						$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
						$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData,$emailCc);	
						//$this->Session->write('LocTrain.flashMsg',' Your request to become a course author has been sent to a membership manager.');
						
						$this->before_update_notification();
						$this->loadModel('Notification');
						$notification['Notification']['mem_id'] =  0;
						$notification['Notification']['notification_type'] = 'request_course_author';
						$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
						$notification['Notification']['notification_sender_id'] = $login_m_id;
						$this->Notification->create();
						$this->Notification->save($notification);
					  //  $this->Session->write('LocTrain.flashMsg',__('Your request to become a course author has been sent to a membership manager.',true));
					}
					
					$this->redirect(array('controller'=>'Home','action'=>'index'));				
				}
				else{
					$this->set('error',$error);
				}
						
		}
	}
	function validate_upgrade_ajax()
	{
		$this->layout="";
		$this->autoRender=false;
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors = $this->validate_upgrade($this->data);							
			if ( is_array ( $this->data ) ){
				foreach ($this->data['Member'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v )	{
							$errors_msg .= "error|$key|$v";
						}		
					}
					else	{
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_upgrade($data)
	{			
		$errors = array ();	
		$this->loadModel('Member');				
		$login_m_id = $this->Session->read('LocTrain.id');
		$check_author_member = $this->Member->find('count',array('conditions'=>array('Member.id'=>$login_m_id,'Member.role_id'=>'3'),'contain'=>false));
		$member_details = $this->Member->find('count',array('conditions'=>array('Member.id'=>$login_m_id,'Member.upgrade_profile'=>'1'),'contain'=>false));
		if($check_author_member > 0){
			$errors ['author_term_condition'] [] = __('request_not_sent_you_are_already_an_author',true)."\n";
		}else if($member_details > 0){
			$errors ['author_term_condition'] [] = __('you_have_already_applied_and_your_application_is_awaiting_a_membership_manager_s_decision',true)."\n";
		}
		else{
			/*if (trim($data['Member']['author_keyword'])==""){
				$errors ['author_keyword'] [] = __(FIELD_REQUIRED,true)."\n";
			}*/
			if (trim($data['Member']['author_notification'])==""){
				$errors ['author_notification'] [] = __(FIELD_REQUIRED,true)."\n";
			}
			if (empty($data['Member']['author_term_condition']) || $data['Member']['author_term_condition']< 1 || $data['Member']['author_term_condition']>1){
				$errors ['author_term_condition'] [] = __('accept_author_terms_and_conditions',true)."\n";
			}
		}
		return $errors;
	}
        function course_purchase($initial_price=NULL,$discount_amount=NULL,$discount=NULL)
	{
		
		$this->loadModel('Price');
		$this->loadModel('Course');
		$this->loadModel('CourseLesson');
		$this->loadModel('Commission');
		$this->loadModel('purchaseItem');
		$course_list = array();
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;
		$login_member_id = $this->Session->read('LocTrain.id');
		$commission= $this->Commission->find('first');
		 $commission_rate = $commission['Commission']['commission'];
		//$encode_ids  = $this->Cookie->read('LocTrainPurchase');
		$get_array=$this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>$login_m_id),'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
		
		$encode_ids=array();
		if(!empty($get_array)){
			foreach($get_array as $row){
			$encode_ids[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
			}
		}
		 $org_amt='';
		
		if(!empty($encode_ids)){
			foreach($encode_ids as $key=>$ids){
				$decode_id = @convert_uudecode(base64_decode($ids));
				
				if(strlen($key)>8)
				{ 
					$course=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$decode_id),'contain'=>array('Course'=>array('CoursePurchaseList'=>array('fields'=>array('mem_id','lesson_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id')))))));
					
				}else{
				$course = $this->Course->find('first',array('conditions'=>array('Course.id'=>$decode_id),'contain'=>array('CourseLesson','CoursePurchaseList'=>array('fields'=>array('mem_id','lesson_id'),'conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'))))));
				
				}
				
				
				
				if(!empty($course)){
					if($course['Course']['m_id'] != $this->Session->read('LocTrain.id')){
					$course['Course']['pro_id'] = base64_encode(convert_uuencode($key));
					$course_list[$key] = $course;	
					}else{
					$this->purchaseItem->delete(array('item_value' => $ids,'m_id'=>$login_m_id));
 					$this->Cookie->delete('LocTrainPurchase.'.$key);
					}
					
				}else{
					$this->purchaseItem->delete(array('item_value' => $ids,'m_id'=>$login_m_id));
					$this->Cookie->delete('LocTrainPurchase.'.$key);
				}
				
				
			}
		}
		$total_price=0;
		$total_items=0; $minus_price=0;
		foreach($course_list as $key=>$courses) { 
			
			if(strlen($key)>8)
				{
					$price=$courses['CourseLesson']['price'];
					$total_price+=$price;
					$total_items++;
				}
				else
				{
					
									  if(!empty($courses['CoursePurchaseList'])){// If previously purchased a lesson of a course,then  minus the cost  have already paid.
										  
										  foreach($courses['CoursePurchaseList'] as $list){
											  if($list['lesson_id']){
												  //$key=array_search($list['lesson_id'], $courses['CourseLesson']);
													// $minus_price+=$courses['CourseLesson'][$key]['price'];
													foreach($courses['CourseLesson'] as $k=>$les){	
															if($les['id'] ==$list['lesson_id']){ 
																$key=$k;$minus_price+=$courses['CourseLesson'][$key]['price'];
																}
															}
											  }
										  }
									  }
					$price=$courses['Course']['price']-$minus_price;
					 $total_items++;
					 $total_price += $price;
				}
		
		}
                $org_amt=round($total_price,2);
		if($discount_amount!=NULL)
		{
			$total_price= $total_price-$discount_amount;
			$total_price=round($total_price,2);
		}
		$net_price='';
		if(trim($initial_price)==trim($total_price))
		{
			$net_price=$total_price;
		}
		
		foreach($encode_ids as $key=>$id)
		{
			$arr[]=$key.'-'.convert_uudecode(base64_decode($id)).'-';
		}
                
		$cousre_ids =  implode(',',$arr);
		$login_member_id = $login_member_id.','.$commission_rate;	
		
		$this->loadModel('Paypal');
		$info=$this->Paypal->find('first');
		$state=$info['Paypal']['paypal_status'];
		if($state)
		{
			$p_url =SANDBOX_URL;
			$p_mem= SANDBOX_MEMBER;	
		}
		else
		{
			$p_url =LIVE_URL;			
			$p_mem= LIVE_MEMBER;
		}
                // Set request-specific fields.
		$paymentAmount = urlencode($net_price);
		$currencyID = urlencode('USD');	
                $item_name=urlencode('Course purchase');
                        //// or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
		//$paymentType = urlencode('Authorization');				// or 'Sale' or 'Order'
		$desc = urlencode('Course purchase');				// or 'Sale' or 'Order'

		
                $item=array();$res_arr=array();
                $i=0;
               foreach($course_list as $key=>$courses) { 
			
                                          
                      //  $item['L_PAYMENTREQUEST_0_NUMBER'.$i]=urlencode($login_member_id);        
			if(strlen($key)>8)
				{
				  $item['L_PAYMENTREQUEST_0_NAME'.$i]=$courses['CourseLesson']['title'];               
                                    $item['L_PAYMENTREQUEST_0_DESC'.$i]=strip_tags($courses['CourseLesson']['description']);
                                    $item['L_PAYMENTREQUEST_0_AMT'.$i]=($courses['CourseLesson']['price']);
                                    $item['L_PAYMENTREQUEST_0_QTY'.$i]=1;
							    $res_arr['L_PAYMENTREQUEST_0_NAME'.$i]=urlencode($courses['CourseLesson']['title'].' - '.strip_tags($courses['CourseLesson']['description']));              
                                    $res_arr['L_PAYMENTREQUEST_0_DESC'.$i]=urlencode($courses['CourseLesson']['title'].' - '.strip_tags($courses['CourseLesson']['description']));
                                    $res_arr['L_PAYMENTREQUEST_0_AMT'.$i]=urlencode($courses['CourseLesson']['price']);
									$res_arr['L_PAYMENTREQUEST_0_QTY'.$i]=1;
                                  
                            
				}
				else
				{
							        $item['L_PAYMENTREQUEST_0_NAME'.$i]=$courses['Course']['title'];    
                                    $item['L_PAYMENTREQUEST_0_DESC'.$i]=strip_tags($courses['Course']['description']);
                                    $item['L_PAYMENTREQUEST_0_AMT'.$i]=($courses['Course']['price']-$minus_price);	
                                    $item['L_PAYMENTREQUEST_0_QTY'.$i]=1;     
									$res_arr['L_PAYMENTREQUEST_0_NAME'.$i]=urlencode($courses['Course']['title'].' - '.strip_tags($courses['Course']['description']));  
                                    $res_arr['L_PAYMENTREQUEST_0_DESC'.$i]=urlencode($courses['Course']['title'].' - '.strip_tags($courses['Course']['description']));
                                    $res_arr['L_PAYMENTREQUEST_0_AMT'.$i]=urlencode($courses['Course']['price']-$minus_price);	
                                    $res_arr['L_PAYMENTREQUEST_0_QTY'.$i]=1;                                
				}
		
                                $i++;
		}                
		
		

               
                //$item['custom']=$cousre_ids.'*'.urlencode($login_member_id);
                if($discount_amount!=NULL)
				{
				$item['PAYMENTREQUEST_0_CUSTOM']=$cousre_ids.'*'.urlencode($login_member_id).'*'.$discount;
				}
				else
				{
				$item['PAYMENTREQUEST_0_CUSTOM']=$cousre_ids.'*'.urlencode($login_member_id);
				}   
                $response_str='';
                 if($discount_amount!=NULL)
				{
				$response_str.='PAYMENTREQUEST_0_ITEMAMT='.$org_amt.'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode('-'.$discount_amount).'&';
			}
			foreach($res_arr as $i=>$v){
					  $response_str.=$i.'='.$v.'&'; 			
					}
                
                $item['PAYMENTREQUEST_0_ITEMAMT']=$org_amt;
                $item['PAYMENTREQUEST_0_SHIPDISCAMT']=urlencode('-'.$discount_amount);
                $item['PAYMENTREQUEST_0_AMT']=$paymentAmount;
                
		$returnURL = urlencode(HTTP_ROOT."Members/paypal_response/".$net_price."/".base64_encode($response_str));
		$cancelURL = urlencode(HTTP_ROOT."Members/paypal_response/".$net_price."/".base64_encode($response_str));
                $item['ReturnUrl']=$returnURL;
                $item['CANCELURL']=$cancelURL;
                $item['CURRENCYCODE']=$currencyID;
                $t='';
                foreach($item as $i=>$v){
                  $t.=$i.'='.$v.'&'; 
			
                }
 		
		// $nvpStr = "&".$t."&custom=$cousre_ids&Amt=$paymentAmount&ReturnUrl=$returnURL&CANCELURL=$cancelURL&CURRENCYCODE=$currencyID";
		$nvpStr = "&".$t;
                if($state){
                    $username = TEST_PAYPAL_USER;
                    $password =TEST_PAYPAL_PASSWORD;
                    $signature = TEST_PAYPAL_SIGNATURE;
                     $environment='sandbox';
                }else{
                    $username = LIVE_PAYPAL_USER;
                    $password =LIVE_PAYPAL_PASSWORD;
                    $signature = LIVE_PAYPAL_SIGNATURE;
                    $environment='live';  
                }
             
                $this->loadModel('PaypalExpress');
		$data=$this->PaypalExpress->setExpressCheckout($nvpStr,$username,$password,$signature,$environment);      
		  
               if($data){
                 echo  $this->PaypalExpress->getPaypalUrl($data,$environment);
               }else{  
                    $this->loadModel('Notification');
                    $notification['Notification']['mem_id'] =  0;
                    $notification['Notification']['notification_type'] = 'course_not_purchased';  
                    $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                    $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                    $notification['Notification']['notification_sender_id'] = $login_member_id;
                    $this->Notification->create();
                    $this->Notification->save($notification);
                    echo HTTP_ROOT."Home/purchase";
               }
		die;
	}   
        
	
        /**
	 * Send HTTP POST Request - helper function
	 *
	 * @param	string	The API method name
	 * @param	string	The POST Message fields in &name=value pair format
	 * @return	array	Parsed HTTP Response body
	 */
	function PPHttpPost($methodName_, $nvpStr_, $environment, $username, $password, $signature) {
		

		// Set up your API credentials, PayPal end point, and API version.
		$API_UserName = urlencode($username);
		$API_Password = urlencode($password);
		$API_Signature = urlencode($signature);
		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === $environment || "beta-sandbox" === $environment) {
			$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
		}
		$version = urlencode('2.3');
                // Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// Get response from the server.
		$httpResponse = curl_exec($ch);

		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}

		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}

		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}

		return $httpParsedResponseAr;
		
	}

 /*
	*
	* function used to get paypal response
	*
	*/

	function paypal_response($amount,$response_str) {
		
		$query_string_data = $this->convert_urlstring_array($_SERVER["QUERY_STRING"]);
               // print_r($query_string_data);
		// do actual paypal payment
		if(!empty($query_string_data["token"]) && !empty($query_string_data["PayerID"])) {
			// Set request-specific fields.
			$paymentAmount = urlencode($amount);
			$currencyID = urlencode('USD');							// or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
			$paymentType = urlencode('Sale');	
                     $this->loadModel('Paypal');
                        $info=$this->Paypal->find('first');
                        $state=$info['Paypal']['paypal_status'];
                        if($state){
                            $username = TEST_PAYPAL_USER;
                            $password =TEST_PAYPAL_PASSWORD;
                            $signature = TEST_PAYPAL_SIGNATURE;
                             $environment ='sandbox';
                        }else{
                            $username = LIVE_PAYPAL_USER;
                            $password =LIVE_PAYPAL_PASSWORD;
                            $signature = LIVE_PAYPAL_SIGNATURE;  
                            $environment ='live';
                        }
			$token = @$query_string_data["token"];
			$payerid = @$query_string_data["PayerID"];
			// Add request-specific fields to the request string.
			$notify_url=urlencode(HTTP_ROOT."Common/paypal_ipn_update_purchase");
			//&PAYMENTREQUEST_0_DESC=$desc&
			$nvpStr1 = "&VERSION=97.0&TOKEN=$token&PayerID=$payerid&PAYMENTREQUEST_0_PAYMENTACTION=$paymentType&PAYMENTREQUEST_0_AMT=$paymentAmount&PAYMENTREQUEST_0_CURRENCYCODE=$currencyID&PAYMENTREQUEST_0_NOTIFYURL=$notify_url";
			$nvpStr1.='&'.base64_decode($response_str);
			 $nvpStr=rtrim($nvpStr1,'&');
//echo $nvpStr;exit;

			//$environment =PAYPAL_BOX;
			// Execute the API operation; see the PPHttpPost function above.
			$httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $nvpStr, $environment, $username, $password, $signature);
			//echo '<pre>';print_r($httpParsedResponseAr);exit;
			if($httpParsedResponseAr["ACK"]=="Success") {
	//echo $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"];
				$token = urldecode($httpParsedResponseAr["TOKEN"]);
				$nvpStr = "&VERSION=97.0&TOKEN=$token";
				//$environment =PAYPAL_BOX;
				// Execute the API operation; see the PPHttpPost function above.
				$httpParsedResponseAr =$this-> PPHttpPost('GetExpressCheckoutDetails', $nvpStr, $environment, $username, $password, $signature);
				$tranx_data["TRANX_APPROVED"]		= urldecode($httpParsedResponseAr["ACK"]);
				$tranx_data["TRANX_ID"]				= urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_TRANSACTIONID"]);			
				$tranx_data["ORIGINAL_TRANX_MSG"]	= urldecode($httpParsedResponseAr["CHECKOUTSTATUS"]);						
				$tranx_data["TRANX_AMNT"]			= urldecode($httpParsedResponseAr["PAYMENTREQUEST_0_AMT"]);
				$tranx_data["TRANX_ORDER_NUMBER"]	= urldecode($httpParsedResponseAr["CORRELATIONID"]);
                                $tranx_data["TRANX_STATUS"]	= $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"];
				$tranx_data["TRANX_MSG"] = "TRANSACTION_APPROVED";					
				
			} else {
				$tranx_data["TRANX_APPROVED"]		= urldecode($httpParsedResponseAr["ACK"]);
				$tranx_data["TRANX_ID"]				= isset($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"])?urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]):'';			
				$tranx_data["ORIGINAL_TRANX_MSG"]	= isset($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"])?urldecode($httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]):'';						
				$tranx_data["TRANX_AMNT"]			=isset($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"])? urldecode($httpParsedResponseAr["PAYMENTINFO_0_AMT"]):'';
				$tranx_data["TRANX_ORDER_NUMBER"]	= isset($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"])?urldecode($httpParsedResponseAr["CORRELATIONID"]):'';
                                  $tranx_data["TRANX_STATUS"]	= isset($httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])?$httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"]:'';
				$tranx_data["TRANX_MSG"] = "TRANSACTION_DECLINED";					
			}
		
                        $this->layout="public";
                        $title_for_layout = 'Localization Training';
                        $this->set(compact('title_for_layout'));
                        $this->loadModel('CoursePurchase');
                        $this->loadModel('CoursePurchaseList');
                        $this->Cookie->delete('LocTrainPurchase');
			$this->loadModel('purchaseItem');
                        $login_m_id = $this->Session->read('LocTrain.id');
			$this->purchaseItem->deleteAll(array('m_id' => $login_m_id));
			
                        $this->before_update_notification();
                        $this->loadModel('Notification');
                        $notification['Notification']['mem_id'] =  0;
                        if($tranx_data["TRANX_STATUS"]=='Pending'){
                        $notification['Notification']['notification_type'] = 'course_not_purchased';  
                        }else{
                        $notification['Notification']['notification_type'] = 'course_purchased_sucessfully';
                        }
                        $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['notification_sender_id'] = $login_m_id;
                        $this->Notification->create();
                        $this->Notification->save($notification);
			if($tranx_data["TRANX_STATUS"]=='Pending'){
						$this->redirect(array('controller'=>'Home','action'=>'purchase'));}
			else{
			  $this->redirect(array('controller'=>'Members','action'=>'my_learning'));
			}
			
		} else {
                    $this->layout="public";
                        $title_for_layout = 'Localization Training';
                        $this->set(compact('title_for_layout'));
                        $this->loadModel('CoursePurchase');
                        $this->loadModel('CoursePurchaseList');
                        //$this->Cookie->delete('LocTrainPurchase');

                        $login_m_id = $this->Session->read('LocTrain.id');
                        $this->before_update_notification();
                        $this->loadModel('Notification');
                        $notification['Notification']['mem_id'] =  0;
                        $notification['Notification']['notification_type'] = 'course_not_purchased';  
                        $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                        $notification['Notification']['notification_sender_id'] = $login_m_id;
                        $this->Notification->create();
                        $this->Notification->save($notification);
                        $this->redirect(array('controller'=>'Home','action'=>'purchase'));
		}
                exit;
	}
        
	/*
	*   --- helper function
	* function used to convert url string to array
	*
	*/

	function convert_urlstring_array($httpResponse) {
		$httpParsedResponseAr = array();
		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
		return $httpParsedResponseAr;
	}
	
	function subscription($id=NULL)
	{   
			$this->loadModel('Subscription');
			$subs_data=$this->Subscription->find('first');
			$subs_function=$subs_data['Subscription']['subscription_status'];
			if($subs_function==1)
			{
		
				$this->layout = 'public';
				$title_for_layout = 'Localization Training';
				$this->loadModel('Price');
				$date = date("Y-m-d H:i:s");
				$this->loadModel('MemberSubscription');
				$login_member_id = $this->Session->read('LocTrain.id');
				
				if($id == NULL)
				{
					$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_member_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
					if($subscribed)
					{
						$this->redirect(array('controller'=>'Members','action'=>'renew_subscription'));
					}
				}
				$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
				$language_folder = $this->Session->read('Config.language') ? $this->Session->read('Config.language') : 'en_US';
				$this->set('language_folder',$language_folder);
				
				$this->set('locale',$locale);
				$subscriptions=$this->Price->find('all',array('order'=>'Price.id ASC'));
				$this->set(compact('subscriptions','login_member_id'));
				if($this->RequestHandler->isAjax() && ($id != NULL)){
					$this->loadModel('Price');	
					$type = $this->Price->find('list',array('fields'=>array('id')));
					$id=convert_uudecode(base64_decode($id));
					if(array_key_exists($id,$type)){
						$mem_type = $id;
						$final_id = $type[$id];
						$p = $this->Price->find('first',array('conditions'=>array('Price.id'=>$id),'fields'=>array('initial_value')));
						$price=$p['Price']['initial_value'];
					}
					/*else{
						$mem_type = 0;
						$final_id = 2;
					}*/	
					$this->loadModel('Paypal');
					$info=$this->Paypal->find('first');
					$state=$info['Paypal']['paypal_status'];
					if($state)
					{
						$p_url ="https://www.sandbox.paypal.com/cgi-bin/webscr";
						$p_mem= "bc1@lt.com";	
					}
					else
					{
						$p_url ="https://www.paypal.com/cgi-bin/webscr";
						//$p_mem= "martin_api1.l10ntrain.com";
						//$p_mem= "martin_api1@l10ntrain.com";
						$p_mem= "PCLJ5UAPEUSZC";
					}	
						
					$var = base64_encode(convert_uuencode($login_member_id));
					$data = '<form action="'. $p_url .'" method="post" target="_top"><input type="hidden" name="cmd" value="_xclick"><input type="hidden" name="business" value="'.$p_mem.'"> <input type="hidden" name="item_name" value="Localization Subscription"><input type="hidden" name="item_number" value="'.$mem_type.'"><input type="hidden" name="amount" id="price" value="'. $price .'"><input type="hidden" name="rm" value="2"><input type="hidden" name="quantity" value="1"><input type="hidden" name="custom" value="'.$var.'"><input type="hidden" name="notify_url" value="'.HTTP_ROOT.'Common/paypal_ipn_update_subscription"><input type="hidden" name="return" value="'.HTTP_ROOT.'Home/subscription_success"><input type="hidden" name="cancel_return" value="'.HTTP_ROOT.'Home/subscription_failure"></form>';
					echo $data;
					die;
				}
			}
			else
			{
				$this->redirect(array('controller'=>'Home','action'=>'error'));
			}
		
	}
	function lesson_detail($courseId=NULL ,$lessonId=NULL,$lesson_play=NULL){

		$course_id = $decoded_c_id = @convert_uudecode(base64_decode($courseId));
		$lesson_id = $decoded_less_id = @convert_uudecode(base64_decode($lessonId));
		$this->layout = '';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Course');
		$this->loadModel('Member');
		$this->loadModel('CourseLesson');
		$this->loadModel('CoursePurchaseList');
		$date = date("Y-m-d H:i:s");
		$login_m_id=$this->Session->read('LocTrain.id')?$this->Session->read('LocTrain.id'):0;

		if($login_m_id){
			$this->loadModel('purchaseItem');
            $order_ids_mem=array();
            $get_array = $this->purchaseItem->find('all',array('conditions'=>array('purchaseItem.m_id'=>$login_m_id),
            	'contain'=>array('purchaseItem'=>array('fields'=>array('item_name','item_val')))));
            if(!empty($get_array)){
		        foreach($get_array as $row){
		        $order_ids_mem[$row['purchaseItem']['item_name']]=$row['purchaseItem']['item_value'];
		        }
            } 
		$getCourses = $order_ids_mem;
		}else{
			$getCourses = $this->Cookie->read('LocTrainPurchase');
		}

		$cookie_courses = array();
		$cookie_lessons = array();
		$course_in_cart=0;
		if(!empty($getCourses)){
			foreach($getCourses as $key=>$course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
				if(strlen($key)>8){
					$cookie_lessons[] = convert_uudecode(base64_decode($course));
				}else{
					if($courseId==$course){
						$course_in_cart=1;
					}
				}
			}
		}

		$this->loadModel('MemberSubscription');
		$login_mem_id = $login_m_id;
		$course = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),
			'contain'=>array('Member'=>array('mem_acc_no'),'CourseLesson'=>array('order'=>'CourseLesson.order ASC'),
			'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_mem_id)))));
		$course_mem_id=$course['Course']['m_id'];

		if($course_mem_id==$login_mem_id){
			$my_course=1;
		}
		else{
			$my_course=0;
		}
		$lesson_highlight = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$lesson_id),'contain'=>array('Course')));
		$subscribed=$this->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$login_mem_id, 
			'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));

		$subscription = $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$course_id,
			'CoursePurchaseList.lesson_id'=>$lesson_id,'CoursePurchaseList.mem_id'=>$login_mem_id,'CoursePurchaseList.type'=>0),
			array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$login_mem_id,'CoursePurchaseList.type'=>0)))));

		$mem_acc_no = $this->Member->find('first',array('conditions'=>array('Member.id'=>$this->Session->read('LocTrain.id')),'fields'=>array('mem_acc_no'),'contain'=>false));
	
		if(empty($mem_acc_no))
		{	
			$mem_acc_no = ' ';
		}else
		{
			$mem_acc_no = $mem_acc_no['Member']['mem_acc_no'];
		}
		$course_details = $this->Course->find('first',array('conditions'=>array('Course.id'=>$decoded_c_id),'contain'=>array('Member'=>array('id','mem_acc_no','final_name','about_me','image'),'CourseKeyword'=>array('fields'=>array('keyword')),'CourseLike'=>array('conditions'=>array('CourseLike.member_id'=>$this->Session->read('LocTrain.id'))),'CourseLesson'=>array('conditions'=>array('CourseLesson.status'=>1),'order'=>'CourseLesson.order ASC'),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'),'CoursePurchaseList.status'=>0)),'CourseRemark'=>array('conditions'=>array('CourseRemark.block'=>0),'Member'=>array('id','family_name','given_name'),'RemarkReport'=>array('Member'=>array('id','given_name','family_name'))))));
		$this->set(compact('lesson_play','course_details','cookie_courses','course','lesson_highlight','mem_acc_no','cookie_lessons','course_in_cart','subscription','subscribed','my_course'));
	}
  function check_retire_course()
  {
        $course_id = convert_uudecode(base64_decode($_GET['course_id']));		
		$this->loadModel('Course');
		$this->Course->unbindModel(
        array('hasOne'=>array('CourseView'),'hasMany' => array('CourseLesson','CourseLesson','CoursePurchaseList','CourseRating','CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
		$course_info=$this->Course->find('all',array('fields'=>array('status'),'conditions'=>array('Course.id'=>$course_id)));
		if(isset($course_info['Course']['status']) && $course_info['Course']['status'] == 2)
		{
		echo '1'; die;
		}
		else
		{
		echo '0'; die;
		}
		die;
        }
	function retire_course()
	{
		$course_id = convert_uudecode(base64_decode($_GET['course_id']));
		$this->loadModel('Course');
                            $this->loadModel('CoursePurchaseList');
                                    $this->Course->unbindModel(
                                    array('hasOne'=>array('CourseView'),'hasMany' => array('CourseRating','CourseKeyword','CourseRemark','Subscription_course','Subscription_lesson','CourseWatch'),'belongsTo'=>array('Language','Member','CourseSubcategory','CourseCategory','CourseSlSubcategory')));
                                            $course_info = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),
                            'contain'=>array('CourseLesson'=>array('conditions'=>array('CourseLesson.status !='=>'2'),'order'=>'CourseLesson.order ASC'))));

                            //print_r($course_info);
                            if(!empty($course_info['CourseLesson'])){
                            foreach($course_info['CourseLesson'] as $lesson){
                            $lesson_ids[]=$lesson['id'];
                            }

                            $subscription = $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$course_id,
                                                    'CoursePurchaseList.lesson_id'=>$lesson_ids,'CoursePurchaseList.type'=>0),
                                                    array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.type'=>0)))));
                            }else{

                            $subscription = $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.type'=>0)))));
                            }
                            //print_r($subscription);
                            if(!empty($subscription)){
                            // chnage status of course in db to 2(deleted)
                            //and keep the files in aws as it is

                                            $this->Course->id=$course_id;
                                            if($this->Course->saveField('status',2))
                                            {
                                                    echo 'retired'; die;
                                            }
                                            else
                                            {
                                                    echo 'retired'; die;
                                            }

                            }else{
                            //delete the record from db and delete all files related it from aws server
                                                    App::import('Component', 'Aws');
                                                    $this->Aws = new AwsComponent();
                            //deleting lessons under course
                                    if(!empty($course_info['CourseLesson'])){
                                            foreach($course_info['CourseLesson'] as $lesson){
                                                        $previous_lesson_video=$lesson['video'];
                                                        $previous_lesson_ext=$lesson['extension'];	
                                                         $current_lesson_video = $lesson['video'];
                                                             $current_lesson_ext = $lesson['extension'];			
                                                             $old_img=$lesson['image'];
                                                            if(!empty($old_img)){

                                                            $keys=array($old_img,'thumb/'.$old_img,'medium/'.$old_img,'catalogue/'.$old_img);
                                                            $this->Aws->deleteFiles(LESSON_BUCKET, $keys);
                                                            }
                                                            //unlinking the deleted lessons
                                                            $this->delete_lesson_files($previous_lesson_video, $current_lesson_video, $previous_lesson_ext, $current_lesson_ext);
                                                            $this->remove_lesson_video_delete(base64_encode(convert_uuencode($lesson['id'])));
                                                            $this->CourseLesson->id=$lesson['id'];
                                                            if($this->CourseLesson->saveField('status',2))//if($this->CourseLesson->delete($lesson['id']))
                                                            {
                                                                    //echo "done";
                                                            }
                                            }
                                    }

                                                    //delete course
                                                            $old_img=$course_info['Course']['image'];
                                                            if(!empty($old_img)){

                                                            $keys=array($old_img,'thumb/'.$old_img,'medium/'.$old_img,'catalogue/'.$old_img);
                                                            $this->Aws->deleteFiles(COURSES_BUCKET, $keys);
                                                            }
                                                            $this->Course->id=$course_info['Course']['id'];
                                                            if($this->Course->saveField('status',3))//if($this->Course->delete($course_info['Course']['id']))
                                                            {
                                                                    //echo "done";
                                                            }
                            echo 'retired';
                            }
			$this->loadModel('Notification');
			$this->before_update_notification();
			$notification['Notification']['mem_id'] = 0;
			$notification['Notification']['notification_type'] = 'retire_course';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['c_id'] = $course_info['Course']['id'];
			$notification['Notification']['notification_sender_id'] = $this->Session->read('LocTrain.id');
			$this->Notification->create();
			$this->Notification->save($notification);
		die;
	}
	
	function promote_review()
	{
		$id = $_POST['id'];
		if(!empty($id) && $this->request->is('ajax'))
		{
			$this->loadModel('CourseRemark');
			$id = convert_uudecode(base64_decode($id));
			
			$info = $this->CourseRemark->find('first',array('conditions'=>array('CourseRemark.id'=>$id),'contain'=>false));
			if(!empty($info))
			{
				if($info['CourseRemark']['promote'] == 1)
				{
					$this->CourseRemark->updateAll(array('CourseRemark.promote'=>'"0"'),array('CourseRemark.id'=>$id));
				} else {
					$this->CourseRemark->updateAll(array('CourseRemark.promote'=>'"1"'),array('CourseRemark.id'=>$id));
				}
				echo "done";
			}
		}		
		die;
		
	}

	function block_review()
	{
		$id = $_POST['id'];
		if(!empty($id) && $this->request->is('ajax'))
		{
			$this->loadModel('CourseRemark');
			$id = convert_uudecode(base64_decode($id));
			
			$info = $this->CourseRemark->find('first',array('conditions'=>array('CourseRemark.id'=>$id),'contain'=>false));
			if(!empty($info))
			{
				$course_id = $info['CourseRemark']['course_id'];
				if($info['CourseRemark']['block'] == 1)
				{
					$this->CourseRemark->updateAll(array('CourseRemark.block'=>'"0"'),array('CourseRemark.id'=>$id));
					$this->loadModel('Course');
					$course=$this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id)));
					$review=$course['Course']['review'];
					$review--;
					$this->Course->id=$course_id;
					$this->Course->saveField('review',$review);
					
				} else {
					$this->CourseRemark->updateAll(array('CourseRemark.block'=>'"1"'),array('CourseRemark.id'=>$id));
					$this->loadModel('Course');
					$course=$this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id)));
					$review=$course['Course']['review'];
					$review++;
					$this->Course->id=$course_id;
					$this->Course->saveField('review',$review);
					
					
				}
				echo "done";
			}
		}		
		die;
		
	}

	function lessons_order()
	{
		if($this->request->is('ajax') && !empty($_POST['order']))
		{
			$this->loadModel('CourseLesson');
			
			foreach($_POST['order'] as $key => $val)
			{
				if(isset($key) && !empty($val))
				{
					$this->CourseLesson->id = $val;
					$this->CourseLesson->saveField('order',$key);
				}
			}
		}
		die;
	}
	
	function read_reviews()
	{
		if($this->request->is('ajax'))
		{
			$course_id = $_POST['course_id'];
			$course_id = convert_uudecode(base64_decode($course_id));
			$this->loadModel('Course');
			
			$course_info = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),'contain'=>array('CourseRemark'=>array('Member'=>array('id','family_name','given_name')))));
			$this->set('course_info',$course_info);
			
			
			$this->render('/Elements/frontElements/course/review');			
		}
	}
	function renew_subscription()
	{
		$this->loadModel('Subscription');
		$subs_data=$this->Subscription->find('first');
		$subs_function=$subs_data['Subscription']['subscription_status'];
		if($subs_function==1)
		{
			$this->layout = 'public';
			$title_for_layout = 'Localization Training';
			$this->loadModel('Price');
			$date = date("Y-m-d H:i:s");
			$this->loadModel('MemberSubscription');
			$login_mem_id = $this->Session->read('LocTrain.id');
			$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';
			$language_folder = $this->Session->read('Config.language') ? $this->Session->read('Config.language') : 'en_US';
			$this->set('language_folder',$language_folder);
			$this->set('locale',$locale);
			$subscriptions=$this->Price->find('all',array('order'=>'Price.id ASC'));
			$subscribed=$this->MemberSubscription->find('first',array('conditions'=>array('MemberSubscription.m_id'=>$login_mem_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
			
			//pr($subscribed);die;
			$time_stamp=$subscribed['MemberSubscription']['date_expired'];
			$end_date= $subscribed['MemberSubscription']['date_expired'];
			//pr($end_date);
			
						
			if($locale == 'it')
			{
				setlocale(LC_ALL, "it_IT",'italian');
				$end_date = strftime('%d %B %Y',$end_date);
				
			}
			else if($locale == 'es')
			{ 
				$newLocale = setlocale(LC_TIME, 'es', 'spanish', 'esp', 'es_ES');
				$end_date = strftime('%d %B %Y',$end_date);
   				
			}
			else if($locale == 'de')
			{ 
				$newLocale = setlocale(LC_TIME, 'de_DE@euro', 'de_DE', 'deu_deu');
				$end_date = strftime('%d. %B %Y',$end_date);
   				
			}
			
			
			else if($locale == 'fr')
			{ 
				$newLocale = setlocale(LC_TIME, "fr_FR",'french');
				$end_date = strftime('%d %B %Y',$end_date);
   				
			}
			else if($locale == 'pt')
			{ 
				$newLocale = setlocale(LC_TIME, "pt_PT",'Portuguese');
				$end_date = strftime('%d %B %Y',$end_date);
   				
			}
			else if($locale == 'ru')
			{  
			
			//echo date('Y', $end_date); die;
				$months = array(
					"Jan" => 'января',
					"Feb" => 'февраля',
					"Mar" => 'марта',
					"Apr" => 'апреля',
					"May" => 'мая',
					"Jun" => 'июня',
					"Jul" => 'июля',
					"Aug" => 'августа',
					"Sep" => 'сентября',
					"Oct" => 'октября',
					"Nov" => 'ноября',
					"Dec" => 'декабря'
				);

				//$your_date = date('d-m-y'); // The Current Date

				$en_month = date("M", $end_date);

				foreach ($months as $en => $ar) {
					if ($en == $en_month) {
						$ar_month = $ar;
					}
				}
				header('Content-Type: text/html; charset=utf-8');
				$end_date=date('d',$end_date).'-'.$ar_month.'-'.date('Y',$end_date);


					//echo russian_date;die;
   				
			}
			else if($locale == 'ja')
			{ 
				$months = array(
					"Jan" => '一月',
					"Feb" => '二月',
					"Mar" => '三月',
					"Apr" => '四月',
					"May" => '五月',
					"Jun" => '六月',
					"Jul" => '七月',
					"Aug" => '八月',
					"Sep" => '九月',
					"Oct" => '十月',
					"Nov" => '十一月',
					"Dec" => '十二月'
				);

				//$your_date = date('d-m-y'); // The Current Date

				$en_month = date("M", $end_date);

				foreach ($months as $en => $ar) {
					if ($en == $en_month) {
						$ar_month = $ar;
					}
				}
				header('Content-Type: text/html; charset=utf-8');
				$end_date=date('d',$end_date).'-'.$ar_month.'-'.date('Y',$end_date);


					//echo japanese_date;die;
   				
			}
			else if($locale == 'ko')
			{ 
				$months = array(
					"Jan" => '일 월',
					"Feb" => '이 월',
					"Mar" => '삼 윌',
					"Apr" => '사 월',
					"May" => '오 월',
					"Jun" => '유 월',
					"Jul" => '칠 월',
					"Aug" => '팔 월',
					"Sep" => '구 월',
					"Oct" => '시 월',
					"Nov" => '십 일 월',
					"Dec" => '십 이 윌'
				);

				//$your_date = date('d-m-y'); // The Current Date

				$en_month = date("M",$end_date);

				foreach ($months as $en => $ar) {
					if ($en == $en_month) {
						$ar_month = $ar;
					}
				}
				header('Content-Type: text/html; charset=utf-8');
				$end_date=date('d',$end_date).'-'.$ar_month.'-'.date('Y',$end_date);


					//echo korian_date;die;
   				
			}
			
			else if($locale == 'zh')
			{ 
				$end_date=date('Y年m月d日',$end_date);
				//echo $end_date; 
				
   				
			}
			
			else if($locale == 'ar')
			{
				$months = array(
								"Jan" => "يناير",
								"Feb" => "فبراير",
								"Mar" => "مارس",
								"Apr" => "أبريل",
								"May" => "مايو",
								"Jun" => "يونيو",
								"Jul" => "يوليو",
								"Aug" => "أغسطس",
								"Sep" => "سبتمبر",
								"Oct" => "أكتوبر",
								"Nov" => "نوفمبر",
								"Dec" => "ديسمبر"
								);

								//$your_date = date('d-m-y'); // The Current Date

								$en_month = date("M",$end_date);

								foreach ($months as $en => $ar) {
									if ($en == $en_month) {
										$ar_month = $ar;
									}
								}

								header('Content-Type: text/html; charset=utf-8');
								$standard = array("0","1","2","3","4","5","6","7","8","9");
								$eastern_arabic_symbols = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
								$current_date=date('d',$end_date).' '.$ar_month.' '.date('Y',$end_date);
								$end_date = str_replace($standard , $eastern_arabic_symbols , $current_date);
								//echo $arabic_date ;die;
	
			
			}
			
			else
			{
				setlocale(LC_ALL, "en_us");
				$end_date = strftime('%d %B %Y',$end_date);
				
			}
			

			$this->set(compact('subscriptions','end_date'));
		}
		else
		{
			$this->redirect(array('controller'=>'Home','action'=>'error'));
		}
	}
	function summary()
	{
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		
	}
	function cancel_lesson_change()
	{
		$lesson_id = $_POST['id'];
		$video = $_POST['video'];
		$ext = $_POST['extension'];
		$hours = $_POST['hours'];
		$mins = $_POST['mins'];
		$secs = $_POST['secs'];
		$status = $_POST['status'];
		$leson_id = convert_uudecode(base64_decode($lesson_id));
		$this->loadModel('CourseLesson');
		$this->loadModel('Course');
		$prev_lesson_duration = ($hours*60*60)+$mins*60+$secs;
		$lesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$leson_id),'contain'=>array('Course')));
		$c_id =$lesson['Course']['id'];
		$curr_lesson_video	=	$lesson['CourseLesson']['video'];
		$curr_lesson_ext	=	$lesson['CourseLesson']['extension'];
		if(trim($video) != $curr_lesson_video)
		{
			$new_lesson_duration = ($lesson['CourseLesson']['hours'] *60*60)+($lesson['CourseLesson']['mins']*60)+$lesson['CourseLesson']['secs'];
			$new_course_duration = $lesson['Course']['duration_seconds'] - $new_lesson_duration + $prev_lesson_duration;
			$this->CourseLesson->updateAll(array('CourseLesson.video'=>'"'.$video.'"','CourseLesson.extension'=>'"'.$ext.'"','CourseLesson.hours'=>$hours,'CourseLesson.mins'=>$mins,'CourseLesson.secs'=>$secs,'CourseLesson.status'=>$status),array('CourseLesson.id'=>$leson_id));
			$this->Course->id = $c_id;
			$this->Course->saveField('duration_seconds',$new_course_duration);
			//cleanup the files
			$this->delete_lesson_file($curr_lesson_video, $curr_lesson_ext);
			echo "done";
		}
		echo "done";
		die;
	}
	function delete_lesson()
	{
		$lesson_id = $_POST['id'];
		$previous_lesson_video = $_POST['video'];
		$previous_lesson_ext = $_POST['extension'];
		
		$leson_id = convert_uudecode(base64_decode($lesson_id));
		//loading the models
		$this->loadModel('CourseLesson');
		$this->loadModel('CoursePurchaseList');
		$this->loadModel('Course');
		//current lesson
		$lesson = $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.id'=>$leson_id),'contain'=>array('Course')));
		if(!empty($lesson )){
		$c_id =$lesson['Course']['id'];
                //checking lesson is already purchased or not
                $subscription = $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$c_id,
                                        'CoursePurchaseList.lesson_id'=>$leson_id,'CoursePurchaseList.type'=>0),
                                        array('CoursePurchaseList.c_id'=>$c_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.type'=>0)))));

                if(!empty($subscription)){
                // chnage status of lesson in db to 2(deleted)
                //and keep the files in aws as it is
                $this->CourseLesson->id=$leson_id;
                if($this->CourseLesson->saveField('status',2))
                                {
                                        echo "done";die;
                                }
                                else
                                {
                                        echo 'done'; die;
                                }
                }else{
                //delete the record from db and delete all files related it from aws server
                $current_lesson_duration = ($lesson['CourseLesson']['hours'] *60*60)+($lesson['CourseLesson']['mins']*60)+$lesson['CourseLesson']['secs'];
                                $current_lesson_video = $lesson['CourseLesson']['video'];
                                $current_lesson_ext = $lesson['CourseLesson']['extension'];
                                $course_duration = $lesson['Course']['duration_seconds'];

                                        $old_img=$lesson['CourseLesson']['image'];
                                        if(!empty($old_img)){
                                        App::import('Component', 'Aws');
                                        $this->Aws = new AwsComponent();
                                        $keys=array($old_img,'thumb/'.$old_img,'medium/'.$old_img,'catalogue/'.$old_img);
                                        $this->Aws->deleteFiles(LESSON_BUCKET, $keys);
                                        }
                                        //unlinking the deleted lessons
                                        $this->delete_lesson_files($previous_lesson_video, $current_lesson_video, $previous_lesson_ext, $current_lesson_ext);
                                        $this->remove_lesson_video_delete($lesson_id);
                                        $new_lesson_duration = ($lesson['CourseLesson']['hours'] *60*60)+($lesson['CourseLesson']['mins']*60)+$lesson['CourseLesson']['secs'];
                                        $new_course_duration = $course_duration - $current_lesson_duration;
                                        $this->Course->id = $c_id;
                                        $this->Course->saveField('duration_seconds',$new_course_duration);
                                        //we will delete the record from cron
                                        $this->CourseLesson->id=$leson_id;
                                        if($this->CourseLesson->saveField('status',2))//if($this->CourseLesson->delete($leson_id))			
                                        {
                                                echo "done";
                                        }
                                        else
                                        {
                                                echo "done";
                                        }
                                        die;

                }
                echo "done";
                }
                die;
		
	}
	
	/*---------------Loctrain@12Nov start by karan------------------------------*/
	
	function view_course_income($id=NULL)
	{
		
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		if(!empty($id))
		{
			App::import('Helper', 'Timezone'); 
			$this->loadModel('CoursePurchaseList');
			$this->loadModel('Commission');
			$this->loadModel('Course');
			$c_id = convert_uudecode(base64_decode($id));
			$course_name = $this->Course->find('first',array('conditions'=>array('Course.id'=>$c_id),'contain'=>false,'fields'=>array('title')));
			
			$conditions = array('CoursePurchaseList.c_id'=>$c_id,'CoursePurchaseList.type'=>'0','Course.isPublished'=>1);
			if($this->request->is('post'))
			{ 
				$type = $_POST['Type'];
				if($type=="week")
				{
					$week_days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
					$today_no_of_days  = date('N');
					$today = date('Y-M-d');
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$first_day_week = date('Y-m-d',strtotime($today.'-'.$today_no_of_days.' day'));
				
					$firstDayweek= strtotime($first_day_week);
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >'=> $firstDayweek,'CoursePurchase.date_added <='=> $current_day));
					
				}else if($type=="month")
				{
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$month_first_date = strtotime(date('Y-m-01'));
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $month_first_date,'CoursePurchase.date_added <='=> $current_day));
				}else if($type=="quarter")
				{
					$month = date('m');
					$month_quarter = floor(($month -1) / 3) + 1;
					$from_date = '';
					$to_date='';
					if($month_quarter=="1")
					{
						$from_date = strtotime(date('Y-01-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');		
					}else if($month_quarter==2)
					{
						$from_date = strtotime(date('Y-04-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					elseif($month_quarter==3)
					{
						$from_date = strtotime(date('Y-07-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					else{
						$from_date = strtotime(date('Y-10-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_date,'CoursePurchase.date_added <='=> $to_date));
				}else if($type=="year")
				{
					$from_year = strtotime(date('Y-01-01'));
					$current_date = strtotime(date('Y-m-d').'23:59:00');
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_year,'CoursePurchase.date_added <='=> $current_date));
				}else if($type=="range")
				{
					  $timezone = new TimezoneHelper(new View(null));
                                      $date_from=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['DateFrom']));
                                      $date_to=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['DateTo']).' 23:59:00');
					
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $date_from,'CoursePurchase.date_added <='=> $date_to));
				}
				else if($type=='all')
				{
					$conditions = $conditions;
				}
			}
		
			$commission = $this->Commission->findById('1');
			$this->paginate = array('order'=>'CoursePurchaseList.id DESC');			
			$course = $this->paginate('CoursePurchaseList',$conditions);
			$total = '0';
			
			/*foreach($course as $total_payment)
			{ 
				if($total_payment['CoursePurchaseList']['lesson_id']!="")
				{
					$total = $total+$total_payment['CourseLesson']['price']; 
				} else {
				
					$total = $total+$total_payment['Course']['price'];
				
				}
			}*/
			
			$this->set(compact('course','total','c_id','commission','course_name'));
		}else {
			$this->redirect(array('action'=>'course'));
		}
		if($this->RequestHandler->isAjax())
		{
			$this->layout = false;			
			$this->viewPath = 'Elements/frontElements/course';
			$this->render('view_course_income_list');
		}
	}
	
	function view_trainer_income($id=NULL)
	{ 
	
		App::import('Helper', 'Timezone'); 
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('Course');
		$this->loadModel('CoursePurchaseList');
		$this->loadModel('Commission');
		$course = $this->Course->find('all',array('conditions'=>array('Course.m_id'=>$this->Session->read('LocTrain.id'),'Course.isPublished'=>1),'fields'=>array('id'),'contain'=>false));
		$c_id = array();
		foreach($course as $course)
		{
			$c_id[] = $course['Course']['id'];
		}
		
	$conditions = array('CoursePurchaseList.c_id'=>$c_id ,'CoursePurchaseList.type'=>'0');
		$this->CoursePurchaseList->virtualFields = array(
							'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
							
							//'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
							'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0',
							
							'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',
							
							'quantity_sort'=>'SELECT CASE WHEN (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) > 0 THEN  (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) ELSE (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL) END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
							'price'=>'SELECT CASE WHEN 
												c.lesson_id IS NULL 
											THEN 
												(SELECT price FROM courses WHERE courses.id = c.c_id)
											ELSE
												(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
											END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id');		
		if($this->Session->read('LocTrain.id')) /*----------------Post_data---------------*/
		{
		
			if($this->request->is('post'))
			{  
				$type = $_POST['Type'];
				
				
				if($type=="week")
				{
					$week_days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
					$today_no_of_days  = date('N');
					
					$today = date('Y-M-d');
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$first_day_week = date('Y-m-d',strtotime($today.'-'.$today_no_of_days.' day'));
					$firstDayweek= strtotime($first_day_week);
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >'=> $firstDayweek,'CoursePurchase.date_added <='=> $current_day));
					$this->CoursePurchaseList->virtualFields = array(
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	//'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'price'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'quantity_sort'=>
																		'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
				}else if($type=="month")
				{
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$month_first_date = strtotime(date('Y-m-01'));
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $month_first_date,'CoursePurchase.date_added <='=> $current_day));
					$this->CoursePurchaseList->virtualFields = array(
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,

																	
																	
																	//'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																	
																);
					
				}else if($type=="quarter")
				{
					$month = date('m');
					$month_quarter = floor(($month -1) / 3) + 1;
					$from_date = '';
					$to_date='';
					if($month_quarter=="1")
					{
						$from_date = strtotime(date('Y-01-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');		
					}else if($month_quarter==2)
					{
						$from_date = strtotime(date('Y-04-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					elseif($month_quarter==3)
					{
						$from_date = strtotime(date('Y-07-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					else{
						$from_date = strtotime(date('Y-10-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_date,'CoursePurchase.date_added <='=> $to_date));
					
					$this->CoursePurchaseList->virtualFields = array(
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
					
					
				}else if($type=="year")
				{
					$from_year = strtotime(date('Y-01-01'));
					$current_date = strtotime(date('Y-m-d').'23:59:00');
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_year,'CoursePurchase.date_added <='=> $current_date));
					
					$this->CoursePurchaseList->virtualFields = array(
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
					
				}else if($type=="range")
				{
					if(isset($_POST['DateFrom']) && !empty($_POST['DateFrom']) && isset($_POST['DateTo']) && !empty($_POST['DateTo'])){
$timezone = new TimezoneHelper(new View(null));
                                        $date_from=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['DateFrom']));
                                        $date_to=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['DateTo']).' 23:59:00');

$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $date_from,'CoursePurchase.date_added <='=> $date_to));
					$this->CoursePurchaseList->virtualFields = array(
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 

																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') > 0 

																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 

																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 

																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 

																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 

																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 

																					c.lesson_id IS NULL 
																				THEN 

																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE

																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);


}else if(isset($_POST['DateFrom']) && !empty($_POST['DateFrom']) && isset($_POST['DateTo']) && empty($_POST['DateTo'])){
$timezone = new TimezoneHelper(new View(null));
                                        $date_from=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['DateFrom']));
                                       

$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $date_from));
					$this->CoursePurchaseList->virtualFields = array(
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from,'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from,
																	
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);

}else if(isset($_POST['DateFrom']) && empty($_POST['DateFrom']) && isset($_POST['DateTo']) && !empty($_POST['DateTo'])){
$timezone = new TimezoneHelper(new View(null));
                                       
                                        $date_to=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['DateTo']).' 23:59:00');

$conditions = array_merge($conditions,array('CoursePurchase.date_added <='=> $date_to));
					$this->CoursePurchaseList->virtualFields = array(
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0  AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL  AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM  (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0  AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);

}else{

}

  					
					
					
				}
				else if($type=='all') 
				{
					$conditions = $conditions;
				}
			}
			
			//pr($conditions);die;
			$commission = $this->Commission->findById('1');
			
			//$this->paginate = array('contain'=>array('Course'=>array('id','title','price'),'CourseLesson','CoursePurchase'),'group'=>array('c_id','lesson_id'),'order'=>'CoursePurchaseList.id ASC');
			$this->paginate = array('order'=>'CoursePurchaseList.c_id ASC');
			if($type=='month' || $type=='range' || $type=='week' || $type=='quarter' || $type=='year')
			{
			$course_purchase = $this->CoursePurchaseList->find('all',array('conditions'=>$conditions));
			}	
			
			else 
			{
			$course_purchase=$this->CoursePurchaseList->find('all',array('conditions'=>array('CoursePurchaseList.c_id'=>$c_id ,'CoursePurchaseList.type'=>'0'),'order'=>'CoursePurchaseList.c_id ASC'));
		}


				/*	$course_purchase =$this->CoursePurchaseList->find('all',$conditions,'contain'=>array('Course'=>array('id','title','price'),'CourseLesson','CoursePurchase'),'group'=>array('c_id','lesson_id'),'order'=>'CoursePurchaseList.id ASC');*/
			$this->set(compact('course_purchase','c_id','commission','id'));
			//pr($course_purchase);die;
						
		}else 
		{
			$this->redirect(array('action'=>'course'));
		}
		
		if($this->RequestHandler->isAjax())
		{
			$this->layout = false;			
			$this->viewPath = 'Elements/frontElements/course';
			$this->render('view_trainer_income_list');
		}
		
	}	
	
	function purchase_course()
	{ 
		  App::import('Helper', 'Timezone'); // loadHelper('Html'); in CakePHP 1.1.x.x
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('CoursePurchaseList');
		$this->loadModel('CoursePurchase');
		$conditions = array('CoursePurchaseList.mem_id'=>$this->Session->read('LocTrain.id'));

		if($this->Session->read('LocTrain.id')) /*----------------Post_data---------------*/
		{
			if($this->request->is('post'))
			{ 
				if(!empty($_POST['option']))
				{
					$type = $_POST['option'];
					$sort_type = $_POST['sort_option'];
					if(isset($this->request->query['sort_option'])){
						switch(trim($this->request->query['sort_option']))
						{
							
							case '1': 
							   
								$order_courses = array('CoursePurchase.date_added ASC');

								break;
								
							case '2':
							
								$order_courses = array('CoursePurchase.date_added DESC');
								
								break;
						}
					
					}
					
					if($type=="month")
					{	
						$current_day = strtotime(date('Y-m-d').'23:59:00');	
						$month_first_date = strtotime(date('Y-m-01'));
						$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $month_first_date,'CoursePurchase.date_added <='=> $current_day));
						
						
					}else if($type=="year")
					{  
						$from_year = strtotime(date('Y-01-01'));
						$current_date = strtotime(date('Y-m-d').'23:59:00');
						$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_year,'CoursePurchase.date_added <='=> $current_date));
						
						
						
					}else if($type=="range")
					{	
                                               

						if(isset($_POST['date_from']) && !empty($_POST['date_from']) && isset($_POST['date_to']) && !empty($_POST['date_to'])){ $timezone = new TimezoneHelper(new View(null));
                                                $date_from=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['date_from']));
                                               $date_to=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['date_to']).' 23:59:00');

						$cond = array('CoursePurchase.date_added >='=> $date_from,'CoursePurchase.date_added <='=> $date_to);
}else if(isset($_POST['date_from']) && !empty($_POST['date_from']) && isset($_POST['date_to']) && empty($_POST['date_to'])){ $timezone = new TimezoneHelper(new View(null));
                                                $date_from=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['date_from']));
                                             

						$cond = array('CoursePurchase.date_added >='=> $date_from);
}else if(isset($_POST['date_from']) && empty($_POST['date_from']) && isset($_POST['date_to']) && !empty($_POST['date_to'])){ $timezone = new TimezoneHelper(new View(null));
                                              
                                               $date_to=strtotime($timezone->convertData($this->Session->read('LocTrain.locale'),$_POST['date_to']).' 23:59:00');

						$cond = array('CoursePurchase.date_added <='=> $date_to);
}else{}
$conditions = array_merge($conditions,$cond);
						
					}
					else if($type=='all')
					{
						$conditions = $conditions;
					}
				
					if($sort_type=='1')
					{	
						//$conditions = array_merge($conditions,array('CoursePurchase.date_added'=>'ASC'));
						//$order=array('order'=>'CoursePurchase.date_added ASC');
						$order='CoursePurchase.date_added ASC';
						
					}
					else if($sort_type=='2')
					{
						//$conditions = array_merge($conditions,array('CoursePurchase.date_added'=>'DESC'));
						//$order=array('order'=>'CoursePurchase.date_added DESC');
				$order='CoursePurchase.date_added DESC';
					}
					
					
				}
			}
			
		// print_r($conditions);
			/*$this->paginate = array('order'=>'CoursePurchase.date_added DESC');
			if($this->request->is('post'))
			{
				if(!empty($order))	
				{
					foreach($order as $order){}
					$this->paginate = array('order'=>$order);
					
				}
			}
			$info = $this->paginate('CoursePurchaseList',$conditions);*/
			if($this->request->is('post'))
			{
				if(!empty($order))	
				{
					$info= $this->CoursePurchaseList->find('all',array('conditions'=>$conditions,'order'=>$order));
					
				}
			}else{
			$info= $this->CoursePurchaseList->find('all',array('conditions'=>$conditions,'order'=>'CoursePurchase.date_added DESC'));}
			//pr($info);die;
			
			$this->set(compact('info'));
			
		}else 
		{
			$this->redirect(array('action'=>'course'));
		}
		
		if($this->RequestHandler->isAjax())
		{
			$this->layout = false;			
			$this->viewPath = 'Elements/frontElements/course';
			$this->render('purchase_history_list');
		}
		//pr($course_purchase);die;
	}
	
	//---- lalit@15 nov-----------
	
	/*function purchase_course()
	{
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		$this->loadModel('CoursePurchase');
		$this->loadModel('CoursePurchaseList');
		$m_id=$this->Session->read('LocTrain.id');
		
		$conditions = array('CoursePurchaseList.mem_id'=>$m_id);
		$this->paginate = array('order'=>'CoursePurchase.date_added DESC');
		$info = $this->paginate('CoursePurchaseList',$conditions);
		$this->set(compact('info'));
		if($this->RequestHandler->isAjax())
		{ 
			$this->layout = false;			
			$this->viewPath = 'Elements/frontElements/course';
			$this->render('purchase_history_list');
		}
		
	}*/
	function quick_start()
	{
		$this->layout = 'public';
		$title_for_layout = 'Localization Training';
		
	}
	function like_lesson()
	{	
	
		$this->loadModel('CourseLike');
		$this->loadModel('Member');
		$this->loadModel('Course');
		$this->loadModel('Notification');
		$this->loadModel('LessonLike');
		
		$member_id= $this->Session->read('LocTrain.id');
		$course_id=convert_uudecode(base64_decode($_POST['course_id']));
		$lesson_id=convert_uudecode(base64_decode($_POST['lessonId']));
		$course_owner = $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),'fields'=>array('id','m_id'),'contain'=>false));
		$course_owner_id = $course_owner['Course']['m_id'];
		$course_owner_name = $this->Member->find('first',array('conditions'=>array('Member.id'=>$course_owner_id),'fields'=>array('given_name','id'),'contain'=>false));
		$liked_before=$this->LessonLike->find('first',array('conditions'=>array('LessonLike.course_id'=>$course_id,'LessonLike.lesson_id'=>$lesson_id,'LessonLike.member_id'=>$member_id)));
		if(!empty($liked_before))
		{
			$remove_id=$liked_before['LessonLike']['id'];
			$this->LessonLike->delete($remove_id);
			//*** Notification for member *****
					$this->before_update_notification();
					$notification['Notification']['mem_id'] =  $member_id;
					$notification['Notification']['notification_type'] = 'unlike';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['c_id'] = $course_id;
					$notification['Notification']['lesson_id'] = $lesson_id;
					$notification['Notification']['notification_sender_id'] = $course_owner_name['Member']['id'];
					$this->Notification->create();
					$this->Notification->save($notification);
			echo __('Like'); die;
		}
		else
		{
			
			$data=$this->LessonLike->create();
			$data['LessonLike']['member_id']= $member_id;
			$data['LessonLike']['course_id']= $course_id;
			$data['LessonLike']['lesson_id']= $lesson_id;
			if($this->LessonLike->save($data))
			{
				//*** Notification for member *****
					$this->before_update_notification();
					$notification['Notification']['mem_id'] =  $member_id;
					$notification['Notification']['notification_type'] = 'like';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['c_id'] = $course_id;
					$notification['Notification']['lesson_id'] = $lesson_id;
					$notification['Notification']['notification_sender_id'] = $course_owner_name['Member']['id'];
					$this->Notification->create();
					$this->Notification->save($notification);
			
				echo __('Unlike'); die;
			}
		}
		die;
		
	}
	
	
	
	function test()
	{
		$date=date('Y-m-d',1382506803);
		//$date = strtotime($date);
		echo $date;die;
	}
	
	function notification()
	{
		$this->layout =	'public';
		$title_for_layout = 'Localization Training';
		
		$this->loadModel('Notification');
		$limit = $this->pageLimit;
		$member_id= $this->Session->read('LocTrain.id');
		
		$conditions = array('Notification.mem_id !='=>$member_id,'Notification.notification_sender_id'=>$member_id);
		$notify_count=$this->Notification->find('count',array('conditions'=>$conditions));
		//pr($notify_count);die;
				
		$this->set('limit',$limit);
		
		$this->paginate = array('order'=>array('Notification.id'=>'DESC'),'offset'=>'0','limit'=>$limit);
		$notify = $this->paginate('Notification',$conditions);
	//pr($notify);die;
		$this->set(compact('notify'));
		
		
	}
	function notify_result()
	{	
		
	
		 $order = array('Notification.id'=>'DESC');

		 if(isset($_GET['limit']))
		 {			  
			 $offest = $_GET['limit'];

		 } else {
			 $offest = 0;
		 }
		 
		 
		 $limit = $offest + $this->pageLimit;
		 //pr($offest);die;
		 if($this->request->is('ajax'))
		 {	//pr('hello');die;
			$this->layout='';
			$this->loadModel('Notification');
			$member_id= $this->Session->read('LocTrain.id');
				
			$conditions = array('Notification.mem_id !='=>$member_id,'Notification.notification_sender_id'=>$member_id);
			$this->paginate = array('offset'=>$offest,'limit'=>$this->pageLimit,'order'=>'Notification.id DESC');
			$notify = $this->paginate('Notification',$conditions); 
			//pr($notify);
			$this->set(compact('notify','limit'));
			$this->render('/Elements/frontElements/home/notify');
		 }
		
	}
	
	
}
?>
