<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
	function importData($import_type){	
	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
	switch ($import_type) {
	    case "course":
		echo 'course';
		$filepath='files/members/courses/';
		$main_list=$this->listDirectory($filepath);						
		/*foreach($main_list as $row=>$val){
		$key=$row;
		$this->importFiles($key,COURSES_BUCKET,$val);		
		//break;
		}*/
		$thumb_list=$this->listDirectory($filepath.'thumb/');	
		$x=0;				
		foreach($thumb_list as $row=>$val){
		$key='thumb/'.$row;
		$this->importFiles($key,COURSES_BUCKET,$val);
		break;
		}
		$medium_list=$this->listDirectory($filepath.'medium/');		
		foreach($medium_list as $row=>$val){
		$key='medium/'.$row;		
		$this->importFiles($key,COURSES_BUCKET,$val);			
		break;
		}
		
		$catalogue_list=$this->listDirectory($filepath.'catalogue/');
		foreach($catalogue_list as $row=>$val){		
		$key='catalogue/'.$row;
		$this->importFiles($key,COURSES_BUCKET,$val);
		break;
		}
		
		
		break;
	    case "lesson":
		$filepath='files/members/course_lesson/';
		$main_list=$this->listDirectory($filepath);
		foreach($main_list as $row=>$val){
		$key=$row;
		$this->importFiles($key,LESSON_BUCKET,$val);
		break;
		}
		$thumb_list=$this->listDirectory($filepath.'thumb/');			
		$x=0;
		foreach($thumb_list as $row=>$val){
		$key='thumb/'.$row;
		$this->importFiles($key,LESSON_BUCKET,$val);
		break;
		}
		$medium_list=$this->listDirectory($filepath.'medium/');
		foreach($medium_list as $row=>$val){
		$key='medium/'.$row;
		$this->importFiles($key,LESSON_BUCKET,$val);
		break;
		}
		$catalogue_list=$this->listDirectory($filepath.'catalogue/');
		foreach($catalogue_list as $row=>$val){		
		$key='catalogue/'.$row;
		$this->importFiles($key,LESSON_BUCKET,$val);
		break;
		}
		break;
	    case "vedio":
		$filepath='files/members/';
		$folders=scandir($filepath);
		unset($folders[0]);
		unset($folders[1]);
		unset($folders[300]);
		unset($folders[299]);
		unset($folders[298]);
		
		foreach($folders as $key=>$folder)
		{
		if($key==2)
		{
		
		$filepath='files/members/'.$folder."/";
		$main_list=$this->listDirectory($filepath);
		
		foreach($main_list as $row=>$val){
		$key=$folder."/".$row;
		
		$this->importFiles($key,LESSON_VIDEOS_BUCKET,$val);
	}
		
		}
		
	
		}
		break;
	    case "profile":
		$filepath='files/members/profile/';
		$main_list=$this->listDirectory($filepath);
		foreach($main_list as $row=>$val){
		$key=$row;
		$this->importFiles($key,MEMBERS_BUCKET,$val);
		
		}
		/*$thumb_list=$this->listDirectory($filepath.'thumb/');			
		$x=0;
		foreach($thumb_list as $row=>$val){
		$key='thumb/'.$row;
		$this->importFiles($key,MEMBERS_BUCKET,$val);
		
		}*/
		break;
	    case "news":
		$filepath='img/news/original/';
		$main_list=$this->listDirectory($filepath);
		foreach($main_list as $row=>$val){
		$key='original/'.$row;
		$this->importFiles($key,NEWS_BUCKET,$val);
		
		}
		$medium_list=$this->listDirectory('img/news/thumbnails/');
		foreach($medium_list as $row=>$val){
		$key='thumbnails/'.$row;
		$this->importFiles($key,NEWS_BUCKET,$val);
		
		}
		break;
	    default:
		echo "Please pass your argument to import the data ";
	}
	die();

	}
function importFiles($key,$bucket,$filepath){
// uploading to AWS server 
App::import('Component', 'Aws');
$this->Aws = new AwsComponent();
echo $this->Aws->uploadFile($key,$bucket, $filepath);
}
function listDirectory($dir)
  {
    $result = array();
   echo  $root = scandir($dir);

    foreach($root as $value) {
      if($value === '.' || $value === '..') {
        continue;
      }
	//if(is_file("$dir$value")) {
         $result[$value] ="$dir$value";
        //continue;
     // }
	
     /* if(is_file("$dir$value")) {
         $result[] = "$dir$value";
        continue;
      }
   if(is_dir("$dir$value")) {
         $result[] = "$dir$value/";
      }
     foreach(self::listDirectory("$dir$value/") as $value)
      {
        $result[] = $value;
      }*/
    }

    return $result;
  }



	function uploaderrorlog()
	{
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();	
		$errorfile=$_SERVER['DOCUMENT_ROOT']."/kuulum/app/tmp/logs/error.log";
		$debugfile=$_SERVER['DOCUMENT_ROOT']."/kuulum/app/tmp/logs/debug.log";
		$hash=substr(md5(rand()),0,10);
		$time=date('Y-m-d H:i:s');
		$this->Aws->uploadFile($hash."-".$time."/".HOSTNAME."/error.log",LOGS_BUCKET,$errorfile);
		$this->Aws->uploadFile($hash."-".$time."/".HOSTNAME."/debug.log",LOGS_BUCKET,$debugfile);
		exit;
	}

	function testAws(){
Configure::write('debug',2);
$data = $this->Course->query("SHOW TABLES");
echo "<pre>";
print_r($data); die;
//	    App::import('Component', 'AwsLive');
//	    $this->Aws = new AwsLiveComponent();
//echo "<pre>"; print_r($this->Aws->listBuckets()); die;
	  $medium = 'img/front/contrast_img_high.jpg';
	 // $upload = $this->Aws->uploadFile('thumbNails/Tulipsss1.jpg', 'dev-kuulum-courses', $medium.'1371884081-Tulips.jpg');
//$upload = $this->Aws->uploadFile('main1.png', 'l10ntrain-kuulum-members', $medium);
//$upload1 = $this->Aws->uploadFile('thumb/contrast_img_high.jpg', 'l10ntrain-kuulum-members', $medium);
$destination = 'files/members/profile/';
$imageName = '23317244a2563a85b0a1cbc07a20bb3d.jpg';
//$result_aws = $this->Aws->uploadFile($imageName,'l10ntrain-kuulum-members', $destination.$imageName);

//echo $result_aws; die;
//$upload = $this->Aws->uploadFile('thumbNails/Tulipsss3.jpg', 'dev-kuulum-courses', $medium.'1371884081-Tulips.jpg');
//$upload1 = $this->Aws->deleteFile('dev-kuulum-courses','thumbNails/Tulipsss2.jpg');
//echo $this->Aws->getFileURL('dev-kuulum-courses','thumbNails/Tulipsss3.jpg');
	    //print_r($upload1);
	}

	function testRDSspeed(){
		$time1 = time();
		$msg=  "Time Before: ". $time1;
		$this->loadModel('CoursePurchaseList');
$this->loadModel('Course');$this->loadModel('CourseLesson');
$this->loadModel('LessonLike');$this->loadModel('Member');$this->loadModel('PurchaseItem');
		/*$course_owner = $this->CoursePurchaseList->find('all',array('recursive'=>'2','conditions'=>array('CoursePurchaseList.type'=>'0')));*/

/*$options['joins'] = array(
    array('table' => 'courses',
        'alias' => 'Course',
        'type' => 'left',
        'conditions' => array(
            'CoursePurchaseList.c_id = Course.id'
        )
    ),
    array('table' => 'Member',
        'alias' => 'Member',
        'type' => 'inner',
        'conditions' => array(
            'Course.m_id = Member.id'
        )
    )
);

$options['conditions'] = array(
    'CoursePurchaseList.type' => '0'
);
$new_course_owner= $this->CoursePurchaseList->find('all',$options);
print_r($new_course_owner);*/
		$course_owner = $this->CoursePurchaseList->query('SELECT `CoursePurchaseList`.`id`, `CoursePurchaseList`.`purchase_id`, `CoursePurchaseList`.`c_id`, `CoursePurchaseList`.`lesson_id`, `CoursePurchaseList`.`purchasedprice`, `CoursePurchaseList`.`discount`, `CoursePurchaseList`.`mem_id`, `CoursePurchaseList`.`commission`, `CoursePurchaseList`.`type`, `CoursePurchaseList`.`status`, `CoursePurchaseList`.`date_created`, `Course`.`id`, `Course`.`m_id`, `Course`.`title`, `Course`.`description`, `Course`.`category_id`, `Course`.`subcategory_id`, `Course`.`sl_subcategory_id`, `Course`.`primary_lang`, `Course`.`price`, `Course`.`rating`, `Course`.`review`, `Course`.`locale`, `Course`.`duration_seconds`, `Course`.`date_added`, `Course`.`date_modified`, `Course`.`status`, `Course`.`image`, `Course`.`isPublished`, `Course`.`lessons_order`, (SELECT COUNT(*) FROM course_likes c WHERE `c`.`course_id` = `Course`.`id`) AS `Course__CourseLikeNumber`, `Member`.`id`, `Member`.`email`, `Member`.`given_name`, `Member`.`family_name` FROM `kuulum`.`course_purchase_lists` AS `CoursePurchaseList` LEFT JOIN `kuulum`.`courses` AS `Course` ON (`CoursePurchaseList`.`c_id` = `Course`.`id`) left JOIN `kuulum`.`members` AS `Member` ON (`Course`.`m_id` = `Member`.`id`) WHERE `CoursePurchaseList`.`type` = 0');
print_r($course_owner);
//$course_rating=$this->Course->CourseRating->find('all',array('conditions'=>array('CourseRating.course_id'=>'686')));
//$first_lesson=$this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.status'=>1,'CourseLesson.course_id'=>'686'),'order'=>array('CourseLesson.date_added ASC'),'limit'=>1));
//$liked_before=$this->LessonLike->find('first',array('conditions'=>array('LessonLike.course_id'=>$course_id,'LessonLike.lesson_id'=>$lesson_id,'LessonLike.member_id'=>$member_id)));
//$this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>'686','CoursePurchaseList.lesson_id'=>'989','CoursePurchaseList.mem_id'=>'239','CoursePurchaseList.type'=>0),array('CoursePurchaseList.c_id'=>686,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>'239','CoursePurchaseList.type'=>0)))));
//$date = date("Y-m-d H:i:s");
//$this->Member->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>'239', 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
//$arr=$this->PurchaseItem->find('all',array('conditions'=>array('PurchaseItem.m_id'=>'239'),'contain'=>array('PurchaseItem'=>array('fields'=>array('item_name','item_val')))));
//print_r($arr);
		$time2 = time();
		$msg.= "<br/>Time After: ".$time2;
		$msg.=  "<br/>Difference (in Seconds): ".($time2 - $time1);
echo $msg;
$this->set('msg',$msg);
//$this->render('test_r_d_sspeed');
//echo "<pre>"; print_r($course_owner);
die;
	}
}
