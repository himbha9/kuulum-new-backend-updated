<?php
ob_start();
class UsersController extends AppController{
     var $name='Users';
	 var $helpers = array('Session','Html','Js','Form','Text','Paginator');
	 var $components=array('Session','Email','RequestHandler','Upload');      
     var $uses=array('Admin','Commission','EmailTemplate','CmsPage','Member','Course','News','CourseLesson','Interest','LessonPrice','CoursePurchase','CourseCategory','CourseSubcategory','Discount','Price','Reason','CoursePrice','RemarkReport','FreeVideo'); 

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->disableCache();
		$actionUrl= array('admin_login','validate_admin_login_ajax','validate_admin_login','validate_recover_password_ajax','recover_password_ajax','admin_password');
		if($this->Session->read('LocTrainAdmin.id') == '' && !in_array($this->params['action'],$actionUrl))
		{
			$this->redirect(array('controller'=>'users','action' => 'login','admin' => true));
		}
		else
		{
			$news_managerUrl=array('admin_login','admin_dashboard','admin_news','admin_edit_news','admin_view_news','admin_add_news','admin_update_status','admin_delete_record','admin_logout','admin_change_password','validate_change_password_ajax');
			
			if($this->Session->read('LocTrainAdmin.role_id') == '4' && !in_array($this->params['action'],$news_managerUrl))
			{
				$this->redirect(array('controller'=>'users','action' => 'dashboard','admin' => true));
			}
			
			$member_managerUrl=array('admin_login','admin_dashboard','admin_author_requests','admin_members','admin_authors','admin_view_member','admin_edit_member','admin_view_author','admin_update_status','admin_delete_record','admin_author_requests','admin_edit_member','admin_add_member','admin_edit_author','admin_authreq_report','admin_adminEmailSearch','admin_adminauthorSearch','admin_authreq_report','sortauthreq_condition','admin_author_report','admin_members_report','admin_adminauthreqSearch','sortAuthor_condition','validate_add_newMember_ajax','validate_add_newMember','admin_logout','admin_change_password','validate_change_password_ajax','validate_paypal_info','validate_paypal_ajax');
			if($this->Session->read('LocTrainAdmin.role_id') == '5' && !in_array($this->params['action'],$member_managerUrl))
			{
				$this->redirect(array('controller'=>'users','action' => 'dashboard','admin' => true));
			}
			
			$courss_managerUrl=array('admin_login','admin_dashboard','admin_news','admin_courses','admin_add_courses','admin_edit_course','admin_view_course','admin_update_status','admin_delete_record','validate_cours_ajax','validate_cours','admin_change_password','validate_change_password_ajax','admin_logout');			
			if($this->Session->read('LocTrainAdmin.role_id') == '6' && !in_array($this->params['action'],$courss_managerUrl))
			{
				$this->redirect(array('controller'=>'users','action' => 'dashboard','admin' => true));
			}

		}
	}
        
////**************************** View Income ***********************************
//function admin_author_income($id=NULL)
//{ 
//	
//		$id=convert_uudecode(base64_decode($id));
//		
//                
//		$this->layout="admin";
//		$this->loadModel('Course');
//		$this->loadModel('CoursePurchaseList');
//		$this->loadModel('Commission');
//                $this->loadModel('Member');
//                $member_name=$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'fields'=>array('given_name'),'contain'=>false));
//	
//                $course = $this->Course->find('all',array('conditions'=>array('Course.m_id'=>$id),'fields'=>array('id'),'contain'=>false));
//		$c_id = array();
//		foreach($course as $course)
//		{
//			$c_id[] = $course['Course']['id'];
//		}
//		$conditions = array('CoursePurchaseList.c_id'=>$c_id ,'CoursePurchaseList.type'=>'0');
//		$this->CoursePurchaseList->virtualFields = array(
//							'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
//							'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
//							'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0',
//							
//							'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',
//							
//							'quantity_sort'=>'SELECT CASE WHEN (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) > 0 THEN  (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) ELSE (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL) END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
//							'price'=>'SELECT CASE WHEN 
//												c.lesson_id IS NULL 
//											THEN 
//												(SELECT price FROM courses WHERE courses.id = c.c_id)
//											ELSE
//												(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
//											END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id');		
//		 /*----------------Post_data---------------*/
//		
//		
//			if($this->request->is('post'))
//			{  
//				$type = $_POST['Type'];
//				
//				$id= $_POST['m_id'];
//				$course = $this->Course->find('all',array('conditions'=>array('Course.m_id'=>$id),'fields'=>array('id'),'contain'=>false));
//				$c_id = array();
//				foreach($course as $course)
//				{
//					$c_id[] = $course['Course']['id'];
//				}
//				$conditions = array('CoursePurchaseList.c_id'=>$c_id ,'CoursePurchaseList.type'=>'0');
//						
//				if($type=="week")
//				{
//					$week_days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
//					$today_no_of_days  = date('N');
//					
//					$today = date('Y-M-d');
//					$current_day = strtotime(date('Y-m-d').'23:59:00');	
//					$first_day_week = date('Y-m-d',strtotime($today.'-'.$today_no_of_days.' day'));
//					$firstDayweek= strtotime($first_day_week);
//					$conditions = array_merge($conditions,array('CoursePurchase.date_added >'=> $firstDayweek,'CoursePurchase.date_added <='=> $current_day));
//					$this->CoursePurchaseList->virtualFields = array(
//																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//																	
//																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//																	
//																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//																	
//																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//																	
//																	'price'=>
//																	'SELECT CASE WHEN (
//																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
//																		THEN  
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
//																		ELSE 
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
//																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
//																		
//																		'quantity_sort'=>
//																		'SELECT CASE WHEN (
//																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
//																		THEN  
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
//																		ELSE 
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
//																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
//																		
//																		'price'=>'SELECT CASE WHEN 
//																					c.lesson_id IS NULL 
//																				THEN 
//																					(SELECT price FROM courses WHERE courses.id = c.c_id)
//																				ELSE
//																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
//																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
//																);
//					
//				}else if($type=="month")
//				{
//					$current_day = strtotime(date('Y-m-d').'23:59:00');	
//					$month_first_date = strtotime(date('Y-m-01'));
//					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $month_first_date,'CoursePurchase.date_added <='=> $current_day));
//					$this->CoursePurchaseList->virtualFields = array(
//																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//																	
//																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//
//																	
//																	
//																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//																
//																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
//																	
//																	'quantity_sort'=>
//																	'SELECT CASE WHEN (
//																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
//																		THEN  
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
//																		ELSE 
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
//																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
//																		
//																		'price'=>'SELECT CASE WHEN 
//																					c.lesson_id IS NULL 
//																				THEN 
//																					(SELECT price FROM courses WHERE courses.id = c.c_id)
//																				ELSE
//																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
//																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
//																	
//																);
//					
//				}else if($type=="quarter")
//				{
//					$month = date('m');
//					$month_quarter = floor(($month -1) / 3) + 1;
//					$from_date = '';
//					$to_date='';
//					if($month_quarter=="1")
//					{
//						$from_date = strtotime(date('Y-01-01'));
//						$to_date = strtotime(date('Y-m-d').'23:59:00');		
//					}else if($month_quarter==2)
//					{
//						$from_date = strtotime(date('Y-04-01'));
//						$to_date = strtotime(date('Y-m-d').'23:59:00');
//					}
//					elseif($month_quarter==3)
//					{
//						$from_date = strtotime(date('Y-07-01'));
//						$to_date = strtotime(date('Y-m-d').'23:59:00');
//					}
//					else{
//						$from_date = strtotime(date('Y-10-01'));
//						$to_date = strtotime(date('Y-m-d').'23:59:00');
//					}
//					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_date,'CoursePurchase.date_added <='=> $to_date));
//					
//					$this->CoursePurchaseList->virtualFields = array(
//																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
//																	
//																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
//																	
//																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
//																
//																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
//																	
//																	
//																	'quantity_sort'=>
//																	'SELECT CASE WHEN (
//																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') > 0 
//																		THEN  
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
//																		ELSE 
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
//																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
//																		
//																		'price'=>'SELECT CASE WHEN 
//																					c.lesson_id IS NULL 
//																				THEN 
//																					(SELECT price FROM courses WHERE courses.id = c.c_id)
//																				ELSE
//																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
//																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
//																);
//					
//					
//					
//				}else if($type=="year")
//				{
//					$from_year = strtotime(date('Y-01-01'));
//					$current_date = strtotime(date('Y-m-d').'23:59:00');
//					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_year,'CoursePurchase.date_added <='=> $current_date));
//					
//					$this->CoursePurchaseList->virtualFields = array(
//																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
//																	
//																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
//																
//																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
//																	
//																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
//																	
//																	'quantity_sort'=>
//																	'SELECT CASE WHEN (
//																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') > 0 
//																		THEN  
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
//																		ELSE 
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
//																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
//																		
//																		'price'=>'SELECT CASE WHEN 
//																					c.lesson_id IS NULL 
//																				THEN 
//																					(SELECT price FROM courses WHERE courses.id = c.c_id)
//																				ELSE
//																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
//																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
//																);
//					
//					
//				}else if($type=="range")
//				{
//					$date_from = strtotime($_POST['DateFrom']);
//					$date_to = strtotime($_POST['DateTo'].' 23:59:00');
//					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $date_from,'CoursePurchase.date_added <='=> $date_to));
//					$this->CoursePurchaseList->virtualFields = array(
//																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
//																	
//																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
//																
//																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
//																	
//																	
//																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
//																	
//																	'quantity_sort'=>
//																	'SELECT CASE WHEN (
//																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') > 0 
//																		THEN  
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
//																		ELSE 
//																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
//																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
//																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
//																		
//																		'price'=>'SELECT CASE WHEN 
//																					c.lesson_id IS NULL 
//																				THEN 
//																					(SELECT price FROM courses WHERE courses.id = c.c_id)
//																				ELSE
//																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
//																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
//																);
//				}
//				else if($type=='all')
//				{
//					$conditions = $conditions;
//				}
//				//pr($conditions);die;
//			}
//			//pr($conditions);die;
//			$commission = $this->Commission->findById('1');
//			
//				
//			$this->paginate = array('contain'=>array('Course'=>array('id','title','price'),'CourseLesson','CoursePurchase'),'group'=>array('c_id','lesson_id'),'order'=>'CoursePurchaseList.id ASC');
//			$course_purchase = $this->paginate('CoursePurchaseList',$conditions);
//			
//			$this->set(compact('course_purchase','c_id','commission','id','member_name'));
//			//pr($course_purchase);die;
//						
//		
//		
//		if($this->RequestHandler->isAjax())
//		{
//			$this->layout = false;			
//			$this->viewPath = 'Elements/adminElements/admin/member/';
//			$this->render('author_income_list');
//		}
//
//		
//	}
	
//**************************** View Income ***********************************
	
/* This function is used to fetch particular author income details */
        function admin_author_income($id=NULL)
        {               
            $id=convert_uudecode(base64_decode($id));
            $this->layout="admin";
            $this->loadModel('Course');
            $this->loadModel('CoursePurchaseList');
            $this->loadModel('Commission');
            $member_name=$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'fields'=>array('given_name'),'contain'=>false));
            if($this->request->is('post'))
            {  
            $type = $_POST['Type'];
            /*
            Find list of all course authors 
            */            
            
            $all_course_income=array();
            
                        /*
            Find all courses of this current author in loop
            */
            $courses = $this->Course->find('all',array('conditions'=>array('Course.m_id'=>$id),'fields'=>array('id','title'),'contain'=>false)); 
            
           
            $c_id = array();
            foreach($courses as $key=>$course)
            {           
              
            /* 
            Set condition for fetching each transactions of course or lession
            */
            $conditions = array('CoursePurchaseList.c_id'=>$course['Course']['id'] ,'CoursePurchaseList.type'=>'0');
            /* 
            Create all virtual fields from CoursePurchaseList
            */
            
                                if($type=="week")
				{
					$week_days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
					$today_no_of_days  = date('N');
					
					$today = date('Y-M-d');
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$first_day_week = date('Y-m-d',strtotime($today.'-'.$today_no_of_days.' day'));
					$firstDayweek= strtotime($first_day_week);
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >'=> $firstDayweek,'CoursePurchase.date_added <='=> $current_day));
					$this->CoursePurchaseList->virtualFields = array(
                                                                     
																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'lesson_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                    

																	'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'lesson_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                    
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'price'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'quantity_sort'=>
																		'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
				}
                                else if($type=="month")
				{
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$month_first_date = strtotime(date('Y-m-01'));
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $month_first_date,'CoursePurchase.date_added <='=> $current_day));
					$this->CoursePurchaseList->virtualFields = array(

																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,

                                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,

																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,

																	
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																	
																);
					
				}
                                else if($type=="quarter")
				{
					$month = date('m');
					$month_quarter = floor(($month -1) / 3) + 1;
					$from_date = '';
					$to_date='';
					if($month_quarter=="1")
					{
						$from_date = strtotime(date('Y-01-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');		
					}else if($month_quarter==2)
					{
						$from_date = strtotime(date('Y-04-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					elseif($month_quarter==3)
					{
						$from_date = strtotime(date('Y-07-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					else{
						$from_date = strtotime(date('Y-10-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_date,'CoursePurchase.date_added <='=> $to_date));
					
					$this->CoursePurchaseList->virtualFields = array(

																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																
                                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
					
					
				}
                                else if($type=="year")
				{
					$from_year = strtotime(date('Y-01-01'));
					$current_date = strtotime(date('Y-m-d').'23:59:00');
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_year,'CoursePurchase.date_added <='=> $current_date));
					
					$this->CoursePurchaseList->virtualFields = array(
                                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
                                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
                                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
					
				}
                                else if($type=="range")
				{
					$date_from = strtotime($_POST['DateFrom']);
					$date_to = strtotime($_POST['DateTo'].' 23:59:00');
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $date_from,'CoursePurchase.date_added <='=> $date_to));
					$this->CoursePurchaseList->virtualFields = array(
		                                                            'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
		                                                            'lesson_discount' =>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
		                                                            'lesson_purchasedprice' =>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
				}
				else if($type=='all')
				{
           	            /* 
            Create all virtual fields from CoursePurchaseList
            */
            $this->CoursePurchaseList->virtualFields = array(
                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0',
                                                    'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',
                                                    'quantity_sort'=>'SELECT CASE WHEN (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) > 0 THEN  (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) ELSE (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL) END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
                                                    'price'=>'SELECT CASE WHEN 
                                                                                            c.lesson_id IS NULL 
                                                                                    THEN 
                                                                                            (SELECT price FROM courses WHERE courses.id = c.c_id)
                                                                                    ELSE
                                                                                            (SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
                                                                                    END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id');


            
            
         

            }
               
           	

             $purchaseddata=$this->CoursePurchaseList->find('all',array('contain'=>array('Course'=>array('id','title','price'),'CourseLesson','CoursePurchase'),'fields'=>array('CoursePurchaseList.course_purchasedprice','CoursePurchaseList.lesson_purchasedprice','CoursePurchaseList.lesson_sold','CoursePurchaseList.lesson_discount','CoursePurchaseList.course_discount','CoursePurchaseList.course_sold','CoursePurchaseList.lesson_id','CoursePurchaseList.purchasedprice','CoursePurchaseList.commission','CoursePurchaseList.c_id','CoursePurchaseList.commissions','CoursePurchaseList.l_commissions','CoursePurchaseList.price','CoursePurchaseList.discount'),'conditions'=>$conditions,'group'=>array('c_id','lesson_id'),'order'=>'CoursePurchaseList.id ASC'));

          
            $grnt_total =0; 
            $total_commision=0;
            $net_total_income=0;
            
            
           
            if(!empty($purchaseddata))
            {
                $newcom=0;
                foreach($purchaseddata as $key1=>$member_detail)
                { 
                    
                    $unit  =  $member_detail['CoursePurchaseList']['lesson_id']?$member_detail['CoursePurchaseList']['lesson_sold']:$member_detail['CoursePurchaseList']['course_sold'];
                                       
                    if($member_detail['CoursePurchaseList']['lesson_id']!='')
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['lesson_discount']); 
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['lesson_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['l_commissions']); 
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {   
                        	                                         
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;

                    } 
                    else
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['course_discount']);
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['course_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['commissions']);
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {                           
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;
                    }
                    $net_income = $total-$endComm;											
                    $net_total_income = $net_total_income+$net_income;
                    
                } 
                        
            }
                       
            $total_commision= number_format($total_commision,'2','.','');

            $net_total_income=number_format($net_total_income,'2','.',''); 
            $all_course_income[$key]['course_id']=$course['Course']['title'];
            $all_course_income[$key]['gross_income']=$grnt_total;
            $all_course_income[$key]['commission']=$total_commision;
            $all_course_income[$key]['net_revenue']=$net_total_income;

            
            }      
            }
            else
            {
            /*
            Find list of all course authors 
            */            
            
            $all_course_income=array();
            
                        /*
            Find all courses of this current author in loop
            */
            $courses = $this->Course->find('all',array('conditions'=>array('Course.m_id'=>$id),'fields'=>array('id','title'),'contain'=>false)); 
            
           
            $c_id = array();
            foreach($courses as $key=>$course)
            {           
              
            /* 
            Set condition for fetching each transactions of course or lession
            */
            $conditions = array('CoursePurchaseList.c_id'=>$course['Course']['id'] ,'CoursePurchaseList.type'=>'0');
            /* 
            Create all virtual fields from CoursePurchaseList
            */
            $this->CoursePurchaseList->virtualFields = array(
                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0',
                                                    'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',
                                                    'quantity_sort'=>'SELECT CASE WHEN (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) > 0 THEN  (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) ELSE (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL) END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
                                                    'price'=>'SELECT CASE WHEN 
                                                                                            c.lesson_id IS NULL 
                                                                                    THEN 
                                                                                            (SELECT price FROM courses WHERE courses.id = c.c_id)
                                                                                    ELSE
                                                                                            (SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
                                                                                    END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id');	


            
            
            

            $purchaseddata=$this->CoursePurchaseList->find('all',array('contain'=>array('Course'=>array('id','title','price'),'CourseLesson','CoursePurchase'),'fields'=>array('CoursePurchaseList.course_purchasedprice','CoursePurchaseList.lesson_purchasedprice','CoursePurchaseList.lesson_sold','CoursePurchaseList.lesson_discount','CoursePurchaseList.course_discount','CoursePurchaseList.course_sold','CoursePurchaseList.lesson_id','CoursePurchaseList.purchasedprice','CoursePurchaseList.commission','CoursePurchaseList.c_id','CoursePurchaseList.commissions','CoursePurchaseList.l_commissions','CoursePurchaseList.price','CoursePurchaseList.discount'),'conditions'=>$conditions,'group'=>array('c_id','lesson_id'),'order'=>'CoursePurchaseList.id ASC'));
            
          
            $grnt_total =0; 
            $total_commision=0;
            $net_total_income=0;
            
            
           
            if(!empty($purchaseddata))
            {
                $newcom=0;
                foreach($purchaseddata as $key1=>$member_detail)
                { 
                    
                    $unit  =  $member_detail['CoursePurchaseList']['lesson_id']?$member_detail['CoursePurchaseList']['lesson_sold']:$member_detail['CoursePurchaseList']['course_sold'];
                                       
                    if($member_detail['CoursePurchaseList']['lesson_id']!='')
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['lesson_discount']); 
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['lesson_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['l_commissions']); 
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {   
                        	                                         
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;

                    } 
                    else
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['course_discount']);
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['course_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['commissions']);
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {                           
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;
                    }
                    $net_income = $total-$endComm;											
                    $net_total_income = $net_total_income+$net_income;
                    
                } 
                        
            }
                       
            $total_commision= number_format($total_commision,'2','.','');

            $net_total_income=number_format($net_total_income,'2','.',''); 
            $all_course_income[$key]['course_title']=$course['Course']['title'];
            $all_course_income[$key]['gross_income']=$grnt_total;
            $all_course_income[$key]['commission']=$total_commision;
            $all_course_income[$key]['net_revenue']=$net_total_income;

            
            }

            }
        
            $this->set(compact('all_course_income','member_name'));

            if($this->RequestHandler->isAjax())
		{
			$this->layout = false;			
			$this->viewPath = 'Elements/adminElements/admin/member/';
			$this->render('author_income_list');
		}

        }
        //**************************** View Income ***********************************
        
        
        
        
        //******************************* View All Author Income *********************************** 
        /* This function is used to fetch all authors income details */
        function admin_author_incomes()
        {               

            $this->layout="admin";
            $this->loadModel('Course');
            $this->loadModel('CoursePurchaseList');
            $this->loadModel('Commission');
            
            if($this->request->is('post'))
            {  
            $type = $_POST['Type'];
				/*
            Find list of all course authors 
            */
            $authors = $this->Member->find('all',array('conditions'=>array('Member.role_id'=>3),'fields'=>array('id','given_name'),'contain'=>array('Course')));
            
            $all_author_income=array();
            foreach($authors as $key=>$author)
            {
            $all_author_income[$key]['member_id']=$author['Member']['id'];
            $all_author_income[$key]['name']=$author['Member']['given_name'];
            /*
            Find all courses of this current author in loop
            */
            $courses = $this->Course->find('all',array('conditions'=>array('Course.m_id'=>$author['Member']['id']),'fields'=>array('id'),'contain'=>false)); 
                       
            $c_id = array();
            foreach($courses as $course)
            {
                $c_id[] = $course['Course']['id'];
            }          
            /* 
            Set condition for fetching each transactions of course or lession
            */
            $conditions = array('CoursePurchaseList.c_id'=>$c_id ,'CoursePurchaseList.type'=>'0');    
            /* 
            Create all virtual fields from CoursePurchaseList
            */
            
                                if($type=="week")
				{
					$week_days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
					$today_no_of_days  = date('N');
					
					$today = date('Y-M-d');
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$first_day_week = date('Y-m-d',strtotime($today.'-'.$today_no_of_days.' day'));
					$firstDayweek= strtotime($first_day_week);
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >'=> $firstDayweek,'CoursePurchase.date_added <='=> $current_day));
					$this->CoursePurchaseList->virtualFields = array(
                                                                     
																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'lesson_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                    

																	'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'lesson_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                    
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'price'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'quantity_sort'=>
																		'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$firstDayweek.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
				}
                                else if($type=="month")
				{
					$current_day = strtotime(date('Y-m-d').'23:59:00');	
					$month_first_date = strtotime(date('Y-m-01'));
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $month_first_date,'CoursePurchase.date_added <='=> $current_day));
					$this->CoursePurchaseList->virtualFields = array(

																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,

                                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
                                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,

																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,

																	
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$month_first_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_day.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																	
																);
					
				}
                                else if($type=="quarter")
				{
					$month = date('m');
					$month_quarter = floor(($month -1) / 3) + 1;
					$from_date = '';
					$to_date='';
					if($month_quarter=="1")
					{
						$from_date = strtotime(date('Y-01-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');		
					}else if($month_quarter==2)
					{
						$from_date = strtotime(date('Y-04-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					elseif($month_quarter==3)
					{
						$from_date = strtotime(date('Y-07-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					else{
						$from_date = strtotime(date('Y-10-01'));
						$to_date = strtotime(date('Y-m-d').'23:59:00');
					}
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_date,'CoursePurchase.date_added <='=> $to_date));
					
					$this->CoursePurchaseList->virtualFields = array(

																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																
                                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date,
																	
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_date.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$to_date.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
					
					
				}
                                else if($type=="year")
				{
					$from_year = strtotime(date('Y-01-01'));
					$current_date = strtotime(date('Y-m-d').'23:59:00');
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $from_year,'CoursePurchase.date_added <='=> $current_date));
					
					$this->CoursePurchaseList->virtualFields = array(
                                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
                                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
                                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$from_year.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$current_date.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
					
					
				}
                                else if($type=="range")
				{
					$date_from = strtotime($_POST['DateFrom']);
					$date_to = strtotime($_POST['DateTo'].' 23:59:00');
					$conditions = array_merge($conditions,array('CoursePurchase.date_added >='=> $date_from,'CoursePurchase.date_added <='=> $date_to));
					$this->CoursePurchaseList->virtualFields = array(
		                                                            'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
		                                                            'lesson_discount' =>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
		                                                            'lesson_purchasedprice' =>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																
																	'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	
																	'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND (SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to,
																	
																	'quantity_sort'=>
																	'SELECT CASE WHEN (
																		SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') > 0 
																		THEN  
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
																		ELSE 
																		(SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.lesson_id IS NULL AND c.type = 0 AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) >= '.$date_from.' AND 
																			(SELECT date_added FROM course_purchases WHERE c.purchase_id = course_purchases.id) <= '.$date_to.') 
																		END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
																		
																		'price'=>'SELECT CASE WHEN 
																					c.lesson_id IS NULL 
																				THEN 
																					(SELECT price FROM courses WHERE courses.id = c.c_id)
																				ELSE
																					(SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
																				END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id'
																);
				}
				else if($type=='all')
				{
                /* 
            Create all virtual fields from CoursePurchaseList
            */
            $this->CoursePurchaseList->virtualFields = array(
                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0',
                                                    'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',
                                                    'quantity_sort'=>'SELECT CASE WHEN (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) > 0 THEN  (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) ELSE (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL) END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
                                                    'price'=>'SELECT CASE WHEN 
                                                                                            c.lesson_id IS NULL 
                                                                                    THEN 
                                                                                            (SELECT price FROM courses WHERE courses.id = c.c_id)
                                                                                    ELSE
                                                                                            (SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
                                                                                    END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id');	


            
            
         

            }
               
           	

            $purchaseddata=$this->CoursePurchaseList->find('all',array('contain'=>array('Course'=>array('id','title','price'),'CourseLesson','CoursePurchase'),'fields'=>array('CoursePurchaseList.course_purchasedprice','CoursePurchaseList.lesson_purchasedprice','CoursePurchaseList.lesson_sold','CoursePurchaseList.lesson_discount','CoursePurchaseList.course_discount','CoursePurchaseList.course_sold','CoursePurchaseList.lesson_id','CoursePurchaseList.purchasedprice','CoursePurchaseList.commission','CoursePurchaseList.c_id','CoursePurchaseList.commissions','CoursePurchaseList.l_commissions','CoursePurchaseList.price','CoursePurchaseList.discount'),'conditions'=>$conditions,'group'=>array('c_id','lesson_id'),'order'=>'CoursePurchaseList.id ASC'));
                                 
            $grnt_total =0; 
            $total_commision=0;
            $net_total_income=0;
            
            if(!empty($purchaseddata))
            {
                $newcom=0;
                foreach($purchaseddata as $key1=>$member_detail)
                { 
                    
                    $unit  =  $member_detail['CoursePurchaseList']['lesson_id']?$member_detail['CoursePurchaseList']['lesson_sold']:$member_detail['CoursePurchaseList']['course_sold'];
                                       
                    if($member_detail['CoursePurchaseList']['lesson_id']!='')
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['lesson_discount']); 
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['lesson_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['l_commissions']); 
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {   
                        	                                         
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;

                    } 
                    else
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['course_discount']);
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['course_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['commissions']);
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {                           
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;
                    }
                    $net_income = $total-$endComm;											
                    $net_total_income = $net_total_income+$net_income;
                
                } 
                        
            }
                       
            $total_commision= number_format($total_commision,'2','.','');

            $net_total_income=number_format($net_total_income,'2','.',''); 

            $all_author_income[$key]['gross_income']=$grnt_total;
            $all_author_income[$key]['commission']=$total_commision;
            $all_author_income[$key]['net_revenue']=$net_total_income;
            
            }
                       
            }
            else
            {
            /*
            Find list of all course authors 
            */
            $authors = $this->Member->find('all',array('conditions'=>array('Member.role_id'=>3),'fields'=>array('id','given_name'),'contain'=>array('Course')));
            
            $all_author_income=array();
            foreach($authors as $key=>$author)
            {
            $all_author_income[$key]['member_id']=$author['Member']['id'];

            $all_author_income[$key]['name']=$author['Member']['given_name'];

            /*
            Find all courses of this current author in loop
            */
            $courses = $this->Course->find('all',array('conditions'=>array('Course.m_id'=>$author['Member']['id']),'fields'=>array('id'),'contain'=>false)); 
            
           
            $c_id = array();
            foreach($courses as $course)
            {
                $c_id[] = $course['Course']['id'];
            }
            /* 
            Set condition for fetching each transactions of course or lession
            */
            $conditions = array('CoursePurchaseList.c_id'=>$c_id ,'CoursePurchaseList.type'=>'0');
            /* 
            Create all virtual fields from CoursePurchaseList
            */
            $this->CoursePurchaseList->virtualFields = array(
                                                    'course_discount'=>'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_discount' => 'SELECT GROUP_CONCAT(discount) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_purchasedprice'=>'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_purchasedprice' => 'SELECT GROUP_CONCAT(purchasedprice) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',    
                                                    'course_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL',
                                                    'lesson_sold'=>'SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0',
                                                    'l_commissions'=>'SELECT GROUP_CONCAT(commission) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id = CoursePurchaseList.lesson_id',
                                                    'quantity_sort'=>'SELECT CASE WHEN (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) > 0 THEN  (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.lesson_id = CoursePurchaseList.lesson_id AND c.type = 0) ELSE (SELECT COUNT(*) FROM course_purchase_lists as c WHERE c.c_id = CoursePurchaseList.c_id AND c.type = 0 AND c.lesson_id IS NULL) END FROM course_purchase_lists WHERE course_purchase_lists.id = CoursePurchaseList.id',
                                                    'price'=>'SELECT CASE WHEN 
                                                                                            c.lesson_id IS NULL 
                                                                                    THEN 
                                                                                            (SELECT price FROM courses WHERE courses.id = c.c_id)
                                                                                    ELSE
                                                                                            (SELECT price FROM course_lessons WHERE course_lessons.id = c.lesson_id)
                                                                                    END FROM course_purchase_lists c WHERE c.id = CoursePurchaseList.id');	


            
            
            

            $purchaseddata=$this->CoursePurchaseList->find('all',array('contain'=>array('Course'=>array('id','title','price'),'CourseLesson','CoursePurchase'),'fields'=>array('CoursePurchaseList.course_purchasedprice','CoursePurchaseList.lesson_purchasedprice','CoursePurchaseList.lesson_sold','CoursePurchaseList.lesson_discount','CoursePurchaseList.course_discount','CoursePurchaseList.course_sold','CoursePurchaseList.lesson_id','CoursePurchaseList.purchasedprice','CoursePurchaseList.commission','CoursePurchaseList.c_id','CoursePurchaseList.commissions','CoursePurchaseList.l_commissions','CoursePurchaseList.price','CoursePurchaseList.discount'),'conditions'=>$conditions,'group'=>array('c_id','lesson_id'),'order'=>'CoursePurchaseList.id ASC'));
            
           
            
            $grnt_total =0; 
            $total_commision=0;
            $net_total_income=0;
            
            if(!empty($purchaseddata))
            {
                $newcom=0;
                foreach($purchaseddata as $key1=>$member_detail)
                { 
                    
                    $unit  =  $member_detail['CoursePurchaseList']['lesson_id']?$member_detail['CoursePurchaseList']['lesson_sold']:$member_detail['CoursePurchaseList']['course_sold'];
                                       
                    if($member_detail['CoursePurchaseList']['lesson_id']!='')
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['lesson_discount']); 
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['lesson_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['l_commissions']); 
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {   
                        	                                         
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;

                    } 
                    else
                    {
                        // If user applies discount coupon from front end
                        $discount = explode(',',$member_detail['CoursePurchaseList']['course_discount']);
                        $purchasedprice = explode(',',$member_detail['CoursePurchaseList']['course_purchasedprice']); 
                        $comm = explode(',',$member_detail['CoursePurchaseList']['commissions']);
                        $endComm = 0;
                        $total=0;
                        foreach($comm as $c_k=>$c)
                        {                           
                            $price=$purchasedprice[$c_k]-$discount[$c_k];
                            $total += $price;
                            $endComm += ($c*$price)/100;
                        }
                        $endComm_part =  0.01 * (int) ($endComm*100);
                        $grnt_total=$grnt_total+$total;
                        $total_commision=$total_commision+$endComm;
                    }
                    $net_income = $total-$endComm;											
                    $net_total_income = $net_total_income+$net_income;
                
                } 
                        
            }
                       
            $total_commision= number_format($total_commision,'2','.','');

            $net_total_income=number_format($net_total_income,'2','.',''); 

            $all_author_income[$key]['gross_income']=$grnt_total;
            $all_author_income[$key]['commission']=$total_commision;
            $all_author_income[$key]['net_revenue']=$net_total_income;

            
            }

            }
            
            
	
            $this->set(compact('all_author_income'));

            if($this->RequestHandler->isAjax())
		{
			$this->layout = false;			
			$this->viewPath = 'Elements/adminElements/admin/member/';
			$this->render('author_income_lists');
		}

        }
	
        
//******************************* View All Author Income *********************************** 

//**************************** FREEEEEE VIDEO***********************************
	function admin_free_video()
	{
		$this->layout="admin";
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('FreeVideo');
		$this->loadModel('Language');
		
		$language1 = $this->Language->find('list',array('conditions'=>array('Language.status'=>'1'),'fields'=>array('locale','language'),'order'=>'Language.id ASC'));
		
		$this->set('language1',$language1);
		
		
		$this->paginate=array('limit'=>10,'order'=>'FreeVideo.id DESC','contain'=>false);
		
		$info=$this->paginate('FreeVideo');
		
		$this->set(compact('page','info'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('free_video_list');
		}
	}
	
	function admin_adminSearchFreeVideo($condition=null)
	{
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$condition=array();
		$cond=array();
		$qryStr="";
		//pr($this->params['named']['page']);die;
		if(isset($this->params['named']['condition']))
		{
			$condition=$this->sortCourse_condition();	
			
			$qryStr=$this->params['named']['condition'];
		}
		else
		{
			$locale = $this->data['search']['locale']; 
			
			if($this->data['search']['type']=="title" && $this->data['search']['title']!='')
			{
				$condition=array_merge($condition,array("FreeVideo.title_".$locale." LIKE"=> '%'.$this->data['search']['title'].'%'));
				$qryStr=$qryStr."FreeVideo.title_".$locale."-||".$this->data['search']['title']."||-";
			}
			
			if($this->data['search']['type']=="status" && $this->data['search']['status']!='')
			{
				$condition=array_merge($condition,array("FreeVideo.status"=>$this->data['search']['status']));
				$qryStr=$qryStr."FreeVideo.status-||".$this->data['search']['status']."||-";
			}
			
			if($this->data['search']['type']=="published_date" && $this->data['search']['published_date']!='')
			{
				$date = strtotime($this->data['search']['published_date']);            // works
				$new_date = strtotime($this->data['search']['published_date'].'+1 day');
				$condition=array_merge($condition,array("FreeVideo.date_added BETWEEN ? AND ?"=>array($date,$new_date)));
			}
			//pr($this->data['search']['viewer']);die;
			if($this->data['search']['type']=="date_modified" && $this->data['search']['date_modified']!='')
			{
				$date = strtotime($this->data['search']['date_modified']);            // works
				$new_date = strtotime($this->data['search']['date_modified'].'+1 day');
				$condition=array_merge($condition,array("FreeVideo.date_modified BETWEEN ? AND ?"=>array($date,$new_date)));
			}
			if($this->data['search']['type']=="viewer" && $this->data['search']['viewer']!='')
			{
			
				$condition=array_merge($condition,array('FreeVideo.viewer'=>$this->data['search']['viewer']));
			}
			
			$this->loadModel('FreeVideo');
			$query=$this->FreeVideo->find('all',array('conditions'=>$condition));
			
			if(!isset($this->params['named']['condition'])){			
				$qryStr=base64_encode($qryStr);
			}	
		}
		$this->set('qryStr',$qryStr);
		if($this->RequestHandler->isAjax()) 
		{
			$this->layout="";
			$this->autoRender=false;
			
			$this->paginate=array('limit' => 10,'conditions'=>$condition,'order'=>'FreeVideo.id DESC');
			$info = $this->paginate('FreeVideo');
			$this->set('info', $info);
			$this->set('page', $page);				
			$this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			$this->render('free_video_list');
		}		
		else
		{
			echo "error";die;
		}	
	}
	function admin_view_free_video($id=NULL)
	{
		$this->layout="admin";
		if(!empty($id))
		{
			$id=convert_uudecode(base64_decode($id));
			$info=$this->FreeVideo->findById($id);
			$this->set(compact('info'));
		}
	}

	
//**************************** FREEEEEE VIDEO***********************************	
	function admin_login()
	{
		$admin_info=$this->Admin->find('first');
		$this->Set('admin_info',$admin_info);
		$this->layout="";
		
		if($this->Session->read('LocTrainAdmin.id'))
		{
			$this->redirect(array('controller'=>'users','action' => 'dashboard','admin' => true));
		}
		
		if (!empty($this->data))
		{ 			 	
			if($this->data['Admin']['email']!="" && $this->data['Admin']['password']!="")
			{			
				$email = $this->data['Admin']['email'];
				$password = $this->data['Admin']['password'];
				
				$getAdminData = $this->Admin->find('first',array('conditions' => array('email'=>$email,'password'=>md5($password))));
				if($getAdminData)
				{					
					$this->Admin->id=$getAdminData['Admin']['id'];
					$data['Admin']['last_login']= strtotime(date('Y-m-d H:i:s'));
					$this->Admin->save($data);
					$this->Session->write('LocTrainAdmin.username', $getAdminData['Admin']['username']);
					$this->Session->write('LocTrainAdmin.id', $getAdminData['Admin']['id']);
					$this->Session->write('LocTrainAdmin.role_id', $getAdminData['Admin']['role_id']);
					$this->Session->write('LocTrainAdmin.last_login', $getAdminData['Admin']['last_login']);
					$this->Session->write('LocTrainAdmin.last_login', $getAdminData['Admin']['last_login']);
					$this->redirect(array('controller'=>'users','action' => 'dashboard','admin' => true));
				}
			}
			$this->Session->write('error',AUTHENTICATION_FAILED);		
		}		
	}
        
        function admin_forgot_password(){
            $admin_info=$this->Admin->find('first');
		$this->Set('admin_info',$admin_info);
		$this->layout="";
          
            
        }

	function validate_admin_login()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax())
		{
			$errors_msg = null;
			$errors=$this->validate_admin_login_ajax($this->data);
			if ( is_array ( $this->data ) )
			{
				foreach ($this->data['Admin'] as $key => $value )
				{
					if( array_key_exists ( $key, $errors) )
					{
						foreach ( $errors [ $key ] as $k => $v )
						{
							$errors_msg .= "error|$key|$v";
						}	
					}
					else 
					{
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}

	function validate_admin_login_ajax($data)
	{			
		$email = $data['Admin']['email'];
		$password = $data['Admin']['password'];
		$checkexistUser=$this->Admin->find('count',array('conditions'=>array('email'=>$email,'password'=>md5($password))));	
		echo $checkexistUser;
		if($checkexistUser==0)
		{
			$errors ['password'] [] = 'Your e-mail address and/or password have not been recognized. Please try again.'."\n";
		}			
		return $errors;			
	}

//--- END OF ADMIN LOGIN-//


//-ADMIN DASHBOARD-----//
	
	function admin_dashboard()
	{	     
		$this->layout="admin";
		$emailTemp = $this->EmailTemplate->find('all',array('conditions'=>array('EmailTemplate.locale'=>'en')));
		$info= $this->CmsPage->find('all',array('conditions'=>array('CmsPage.locale'=>'en'),'order'=>'CmsPage.id'));
		$this->set(compact('info','emailTemp'));
		if($this->Session->read('LocTrainAdmin.role_id')==4)
		{
			$info= $this->Admin->find('first',array('conditions'=>array('Admin.id'=>$this->Session->read('LocTrainAdmin.id'))));
			$this->set(compact('info'));
		}
		if($this->Session->read('LocTrainAdmin.role_id')==5)
		{
			$info= $this->Admin->find('first',array('conditions'=>array('Admin.id'=>$this->Session->read('LocTrainAdmin.id'))));
			$this->set(compact('info'));
		}
		if($this->Session->read('LocTrainAdmin.role_id')==6)
		{
			$info= $this->Admin->find('first',array('conditions'=>array('Admin.id'=>$this->Session->read('LocTrainAdmin.id'))));
			$this->set(compact('info'));
		}
	}
	
//-----END ADMIN DASHBOARD--//

//--- MANAGE Members---//
	function admin_members()
	{
		$this->layout='admin';
		$this->loadModel('Member');		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		
		$condition=array('Member.role_id !='=>'0');
		
		if( ! isset($this->params['named']['page']))
		{
			$this->Session->delete('condition');
			//$this->Session->write('condition',$condition);
		}
		
		if(!empty($this->data))
		{
                        if($this->data['search']['type']=="name" && $this->data['search']['text']!=''){
				$condition=array_merge($condition,array("Member.final_name"=>$this->data['search']['text']));
			}
                        
			if($this->data['search']['type']=="email" && $this->data['search']['text']!=''){
				$condition=array_merge($condition,array("Member.email"=>$this->data['search']['text']));
			}
			if($this->data['search']['type']=="date_register" && $this->data['search']['register_date']!=''){
				
				$date = strtotime($this->data['search']['register_date']);            // works
				$new_date = strtotime($this->data['search']['register_date'].'+1 day');
				$condition=array_merge($condition,array("Member.date_added BETWEEN ? AND ?"=>array($date,$new_date)));
			}
			if($this->data['search']['type']=="status" && $this->data['search']['status']!=''){
				$condition=array_merge($condition,array("Member.status"=>$this->data['search']['status']));
			}
			$this->Session->write('condition',$condition);
		}
		
		$condition = $this->Session->read('condition');
                $this->loadModel("Role");
                    $roles=$this->Role->find('all');
                    $role=array();
                    foreach($roles as $row=>$roles)
                    {
                         $role[$roles["Role"]["id"]]=$roles["Role"]["role"];
                    }
		$this->paginate=array('conditions'=>$condition,'limit'=>'-1','order'=>'Member.id DESC');
		$info=$this->paginate('Member');
		$this->set(compact('info','page','courses','role'));
				
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('member_list');
		}
	}
//--- END MANAGE Members---//	

//---- function for edit email template---//
	function admin_edit_email_template($id=NULL)
	{
		$this->layout="admin";
		$this->all_language();
		
		
		if(!empty($id))
		{ 
			$main_id = convert_uudecode(base64_decode($id));
			$this->loadModel('Language');
			$lang = $this->Language->find('list',array('conditions'=>array('Language.status'=>'1'),'order'=>'Language.id ASC','fields'=>array('id','locale')));
			$info = $this->EmailTemplate->find('all',array('conditions'=>array('EmailTemplate.main_id'=>$main_id,'EmailTemplate.locale'=>$lang),'contain'=>array()));
			//pr($info);die;
			$this->set('info',$info);
			
		}

		if(!empty($this->data))
		{ //pr($this->data);die;
			foreach($this->data as $data)
			{ 	
				$this->EmailTemplate->save($data);
			}
				$this->Session->write('success','The Template has been updated');
				$this->redirect(array('action'=>'email_templates','admin' => true));
		}
	}
//---- end of function for edit email template---//

//---- function for e email template---//
	function admin_email_templates()
	{
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->layout="admin";
		$conditions = array('EmailTemplate.locale'=>'en');
		$this->paginate = array('order'=>'EmailTemplate.id DESC','limit'=>'10');
		$info = $this->paginate('EmailTemplate',$conditions); 
		$this->set(compact('info','page'));
		 if($this->RequestHandler->isAjax())
		{
		   $this->layout="";
		   $this->viewPath = 'Elements'.DS.'adminElements/admin/emailTemplate';
		   $this->render('template_list');
		}
	}
//---end of email template function--- //	

//---- function for edit cms page---//
	/*function admin_edit_cms($id=NULL)
	{
		$this->layout='admin';		
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}		
		if(!empty($this->data)){ 
			$this->request->data['CmsPage']['date_modified']= strtotime(date('Y-m-d H:i:s'));
			$this->CmsPage->save($this->data); 
			$this->Session->write('success','Information has been updated successfully');
			$this->redirect(array('action'=>'cmspages','admin' => true));									
		}
	}*/
	
	function admin_edit_cms($page=NULL)
	{
		$this->layout="admin";
		
		if(!empty($page))
		{
			$this->loadModel('Language');
			$lang = $this->Language->find('list',array('conditions'=>array('Language.status'=>'1'),'order'=>'Language.id ASC','fields'=>array('id','locale')));
			$info = $this->CmsPage->find('all',array('conditions'=>array('CmsPage.page'=>$page,'CmsPage.locale'=>$lang),'order'=>'CmsPage.id ASC'));
			$this->set('info',$info);
			
			//pr($info); die;
		}
		
		if(!empty($this->data))
		{
                   	//pr($this->data); die;
			foreach($this->data as $data)
			{
			   $data['CmsPage']['date_modified'] =strtotime(date('Y-m-d H:i:s'));
			     $this->CmsPage->create(); 
			   $this->CmsPage->save($data); 
			}
			
			$this->Session->write('success','Information has been updated');
			$this->redirect(array('action'=>'cmspages','admin' => true));									
		}
	}
	
//---- End of function for edit cms template---//
	
	
	
//---- function for Listing cmspage---//	
	function admin_cmspages()
	{
		$this->layout='admin';
		$this->paginate=array('conditions'=>array('CmsPage.locale'=>'en'),'limit'=>'10','order'=>'CmsPage.id ASC'); 
		$info=$this->paginate('CmsPage');
		$this->set(compact('info'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $this->render('cms_list');
		}
	}
//---- end of  function for Listing cmspage---//



//------FUNCTION FOR CHANGE STATUS(ENABLE/DISABLE)-------------------------------------

	function admin_update_status($page=NULL,$id=NULL,$tableName=NULL,$renderPath=NULL,$renderElement=NULL)
	{
		$this->layout='';
		$id=convert_uudecode(base64_decode($id));
		$tableName=base64_decode($tableName);
		$renderPath=base64_decode($renderPath);
		$renderElement=base64_decode($renderElement);
	

		if($this->RequestHandler->isAjax()){		
			$this->layout=false;
			$this->autoRender = false;		
			$getRec = $this->$tableName->find('first',array('conditions'=>array($tableName.'.id'=>$id),'contain'=>array()));			
			//pr($getRec);die;
			$field=array();
			if(!empty($getRec)){
				if($getRec[$tableName]['status']=='0'){
					$this->$tableName->id=$id;					
					$field[$tableName]['status'] = 1;					
					$this->$tableName->save($field);
				}
				else{
					$this->$tableName->id=$id;					
					$field[$tableName]['status'] = '0';					
					$this->$tableName->save($field);					
				}
			}
			if(trim($renderElement) == 'author_list')	{				
				$this->paginate=array('limit' =>'10','page'=>$page,'order'=>$tableName.'.id DESC','conditions'=>array($tableName.'.role_id'=>3));
			}else{
				$this->paginate=array('limit' =>'10','page'=>$page,'order'=>$tableName.'.id DESC');
			}
			$info = $this->paginate($tableName);
			$this->set(compact('info','page'));			
			$this->viewPath = 'Elements'.DS.'adminElements/'.$renderPath;
			$this->render($renderElement);			
		}		
	} 
//----END OF FUNCTION FOR CHANGE STATUS(ENABLE/DISABLE)--


    function admin_view_member($id=NULL)
	{
		$this->layout='admin';
		if(!empty($id))
		{
			$this->loadModel('Member');
			$this->loadModel('MemberKeyword');
			$id=convert_uudecode(base64_decode($id));
			$info=$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));
			$intrest=$this->MemberKeyword->find('all',array('conditions'=>array('MemberKeyword.m_id'=>$id),'contain'=>array()));
			$this->set(compact('info','intrest'));
		}
	}
	  function admin_view_member_interests($role_id = 0, $id = NULL) {
        $this->layout = 'admin';
        if (!empty($id)) {

            $this->loadModel('Member');
            $this->loadModel('MemberKeywords');
            $this->loadModel('MemberAuthorKeywords');
            $id = convert_uudecode(base64_decode($id));
            $info = $this->Member->find('first', array('conditions' => array('Member.id' => $id), 'contain' => array()));
            if ($role_id == 3) {
                $intrest = $this->MemberAuthorKeywords->find('all', array('conditions' => array('MemberAuthorKeywords.m_id' => $id), 'contain' => array()));
                $this->paginate = array('conditions' => array('MemberAuthorKeywords.m_id' => $id), 'limit' => '-1', 'order' => 'MemberAuthorKeywords.id DESC');
                $intrest = $this->paginate('MemberAuthorKeywords');
            } else {
                $intrest = $this->MemberKeywords->find('all', array('conditions' => array('MemberKeywords.m_id' => $id), 'contain' => array()));
                $this->paginate = array('conditions' => array('MemberKeywords.m_id' => $id), 'limit' => '-1', 'order' => 'MemberKeywords.id DESC');
                $intrest = $this->paginate('MemberKeywords');
            }


            $this->set(compact('info', 'intrest', 'role_id'));
            if ($this->RequestHandler->isAjax()) {
                $this->layout = '';
                $this->viewPath = 'Elements' . DS . '/adminElements/admin/member/';
                $this->render('member_interest_list');
            }
        }
    }
	 function admin_view_author($id=NULL)
	{
		$this->layout='admin';
		if(!empty($id))
		{
			$this->loadModel('Member');
			$this->loadModel('MemberAuthorKeyword');
			$id=convert_uudecode(base64_decode($id));
			$info=$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));
			$intrest=$this->MemberAuthorKeyword->find('all',array('conditions'=>array('MemberAuthorKeyword.m_id'=>$id),'contain'=>array()));
			$this->set(compact('info','intrest'));
			
		}
	}
	function admin_add_free_video()
	{	
		$this->layout='admin';
		
		if(!empty($this->data))
		{
			
			//$error=$this->validate_free_video($this->data);	
			$error=$this->validate_add_free_video($this->data);	
			
			
			if(count($error)==0)
			{
				$data= $this->data;
				
			
				$data['FreeVideo']['date_modified']= strtotime(date('Y-m-d H:i:s'));
				$data['FreeVideo']['date_added']= strtotime(date('Y-m-d H:i:s'));
				$data['FreeVideo']['status']= 1;
				//$data['FreeVideo']['title']= $this->data['FreeVideo']['title'];
				//$data['FreeVideo']['description']= $this->data['FreeVideo']['description'];
				//$data['FreeVideo']['viewer']= $this->data['FreeVideo']['viewer'];
				if($this->data['FreeVideo']['video_type']=='own_video' && !empty($this->data['FreeVideo']['own_video_url']['name']))
				{	
					$fileExt = pathinfo($this->data['FreeVideo']['own_video_url']['name']);
					$ext = strtolower($fileExt['extension']);
							
//********************************FREE VIDEO SEARCHING*********************************************************
					$targetFolder = 'files/admin_free_video/';
					$tempFile = $this->data['FreeVideo']['own_video_url']['tmp_name'];
					$fileParts = pathinfo($this->data['FreeVideo']['own_video_url']['name']);
					$ext = strtolower($fileParts['extension']);
					
					
					
					$file_name = time().'-'.$this->data['FreeVideo']['own_video_url']['name'];
					
					$file_new_name = $file_name;	
					
				
					if($ext=='mp4' || $ext=='wmv' || $ext=='flv' || $ext=='avi' || $ext=='mov')
					{					 
						$img = $this->data['FreeVideo']['own_video_url'];		
							
				
						$result = $this->Upload->upload($img, $targetFolder, $file_new_name, NULL, array('mov','mp4','wmv','flv','avi'));
					
						$video = $file_name;
						$data['FreeVideo']['own_video_url'] = $video;		
						$data['FreeVideo']['extension'] = $ext;
						
						
						
						if($result)
						{
							$cmdThumb = '/opt/ffmpeg/bin/ffmpeg  -y -itsoffset -6  -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320X170 '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_name.'_thumb.png 2>&1';	
							$cmdSmallThumb = '/opt/ffmpeg/bin/ffmpeg  -y -itsoffset -6  -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 200X125 '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_name.'_smallThumb.png 2>&1';	
							
							exec($cmdSmallThumb);
							exec($cmdThumb,$thumbResponse, $thumbErr);
							
							
							if($thumbErr == 0)
							{
								
								$getDuration = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.'  2>&1 | grep Duration';
								exec($getDuration, $getDurationResponse, $getDurationErr);
								
								$preview_cmd = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.' -ss 00:00:0.0 -t 00:01:0.0 -acodec copy -vcodec copy '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_name.'_preview.'.$ext;
								
								exec($preview_cmd, $preview_cmdResponse, $preview_cmdErr);
								
								if($getDurationErr == 0)
								{
									$search = '/Duration: (.*?)[.]/';
									$duration = preg_match($search, $getDurationResponse[0], $matches, PREG_OFFSET_CAPTURE);							
									$duration = $matches[1][0];
									list($hours, $mins, $secs) = split('[:]', $duration);		
									$data['FreeVideo']['hours'] = $hours;
									$data['FreeVideo']['mins'] = $mins;
									$data['FreeVideo']['secs'] = $secs;
									$curr_lesson_duration = ($hours*60*60)+$mins*60+$secs;
								}
							}
						}
						
						if($this->FreeVideo->save($data))
						{	
							$this->Session->write('success','Video has been added successfully');
							$this->redirect(array('controller'=>'users','action'=>'free_video','admin'=>true));
						}
					}
				}
				else 
				{
					$data['FreeVideo']['free_video_url']= $this->data['FreeVideo']['free_video_url'];
					if($this->data['FreeVideo']['own_video_url']['name']=='')
					{
						$data['FreeVideo']['own_video_url']='';
					}
					//pr($data);die;
					if($this->FreeVideo->save($data))
					{	
						$this->Session->write('success','Video has been added successfully');
						$this->redirect(array('controller'=>'users','action'=>'free_video','admin'=>true));
					}
				}
			}
			else
			{
				$this->set('error',$error);	
			}

		}
	}
	function validate_add_free_video_ajax()
	{
		
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_add_free_video($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['FreeVideo'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_add_free_video($data)
	{			
		$errors = array ();		
		$locale = $data['FreeVideo']['locale'];
		/* if($data['FreeVideo']['video_type']!='free_video')
		{
			if (trim($data['FreeVideo']['own_video_url']) =='1')
			{
				$errors['own_video_url'][] = __(FIELD_REQUIRED,true)."\n";
			}
		} */
		if (trim($data['FreeVideo']['title_'.$locale]) == "")
		{
			$errors['title_'.$locale][] = __(FIELD_REQUIRED,true)."\n";
		}
		if (empty($data['FreeVideo']['description_'.$locale]))
		{
			$errors['description_'.$locale][] = __(FIELD_REQUIRED,true)."\n";
		}
		
	/*	if(trim($data['FreeVideo']['viewer'])!='')
		{
			if(!is_numeric($data['FreeVideo']['viewer']))
			{
				$errors ['viewer'] [] = __('Please enter numeric value')."\n";
			}
			if(is_numeric($data['FreeVideo']['viewer']))
			{
				if(trim($data['FreeVideo']['viewer']) < 0 || $data['FreeVideo']['viewer'] > 2147483647)
				{
					$errors ['viewer'] [] = __('Please enter value between 0 to 2147483647')."\n";
				}
				
			}
		}
		if(trim($data['FreeVideo']['viewer'])=='')
		{
			$errors['viewer'][] = __(FIELD_REQUIRED,true)."\n";
		}
		if($data['FreeVideo']['video_type']=='free_video')
		{ */
			if(isset($data['FreeVideo']['free_video_url']))
			{
				if(trim($data['FreeVideo']['free_video_url']==""))
				{
					$errors['free_video_url'][]=__(FIELD_REQUIRED,true)."\n";
				}else
					if(!preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['FreeVideo']['free_video_url']) ){
				$errors['free_video_url'][]=__("Not a valid Youtube URL *")."\n";
				}
			}
			
		 //}
		
		return $errors;			
	}
	function  admin_edit_free_video($id=NULL)
	{	
		$this->layout='admin';
		
		$id=convert_uudecode(base64_decode($id));
		if(!empty($id))
		{
		   $freeVideoiIfo=$this->FreeVideo->findById($id);
		   $this->set('freeVideoiIfo',$freeVideoiIfo);
		}	
		
		
		if(!empty($this->data))
		{
			
			
			$error=$this->validate_free_video($this->data);	
			$data= $this->data;
			if(count($error)==0)
			{ 
				
				$old_info = $this->FreeVideo->find('first',array('conditions'=>array('FreeVideo.id'=>$this->data['FreeVideo']['id']),'fields'=>array('own_video_url','free_video_url')));
				
				$data['FreeVideo']['date_modified']= strtotime(date('Y-m-d H:i:s'));
				
				
				if($data['FreeVideo']['video_type']=='own_video' && !empty($this->data['FreeVideo']['own_video_url']['name']))
				{	
				
					if(!empty($old_info['FreeVideo']['own_video_url']))
					{
						unlink('files/admin_free_video/'.$old_info['FreeVideo']['own_video_url']);
						unlink('files/admin_free_video/'.$old_info['FreeVideo']['own_video_url'].'_smallThumb.png');
						unlink('files/admin_free_video/'.$old_info['FreeVideo']['own_video_url'].'_thumb.png');
						unlink('files/admin_free_video/'.$old_info['FreeVideo']['own_video_url'].'_preview.mp4');
						
					}
					$fileExt = pathinfo($this->data['FreeVideo']['own_video_url']['name']);
					$ext = strtolower($fileExt['extension']);
							
//*****************************************************************************************
					$targetFolder = 'files/admin_free_video/';
					$tempFile = $this->data['FreeVideo']['own_video_url']['tmp_name'];
					$fileParts = pathinfo($this->data['FreeVideo']['own_video_url']['name']);
					$ext = strtolower($fileParts['extension']);
					
					
					
					$file_name = time().'-'.$this->data['FreeVideo']['own_video_url']['name'];
					
					$file_new_name = $file_name;	
					
				
					if($ext=='mp4' || $ext=='wmv' || $ext=='flv' || $ext=='avi' || $ext=='mov')
					{					 
						$img = $this->data['FreeVideo']['own_video_url'];		
							
				
						$result = $this->Upload->upload($img, $targetFolder, $file_new_name, NULL, array('mov','mp4','wmv','flv','avi'));
					
						$video = $file_name;
						$data['FreeVideo']['own_video_url'] = $video;		
						$data['FreeVideo']['free_video_url'] = '';		
						$data['FreeVideo']['extension'] = $ext;
						
						
						
						if($result)
						{
							$cmdThumb = '/opt/ffmpeg/bin/ffmpeg  -y -itsoffset -6  -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320X170 '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_name.'_thumb.png 2>&1';	
							$cmdSmallThumb = '/opt/ffmpeg/bin/ffmpeg  -y -itsoffset -6  -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 200X125 '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_name.'_smallThumb.png 2>&1';	
							
							exec($cmdSmallThumb);
							exec($cmdThumb,$thumbResponse, $thumbErr);
							
							
							if($thumbErr == 0)
							{
								
								$getDuration = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.'  2>&1 | grep Duration';
								exec($getDuration, $getDurationResponse, $getDurationErr);
								
								$preview_cmd = '/opt/ffmpeg/bin/ffmpeg -i '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_new_name.' -ss 00:00:0.0 -t 00:01:0.0 -acodec copy -vcodec copy '.FILE_PATH.'app/webroot/files/admin_free_video/'.$file_name.'_preview.'.$ext;
								
								exec($preview_cmd, $preview_cmdResponse, $preview_cmdErr);
								if($getDurationErr == 0)
								{
									$search = '/Duration: (.*?)[.]/';
									$duration = preg_match($search, $getDurationResponse[0], $matches, PREG_OFFSET_CAPTURE);							
									$duration = $matches[1][0];
									list($hours, $mins, $secs) = split('[:]', $duration);		
									$data['FreeVideo']['hours'] = $hours;
									$data['FreeVideo']['mins'] = $mins;
									$data['FreeVideo']['secs'] = $secs;
									$curr_lesson_duration = ($hours*60*60)+$mins*60+$secs;
								}
							}
						}
						
						if($this->FreeVideo->save($data))
						{	
							$this->Session->write('success','Video has been updated successfully');
							$this->redirect(array('controller'=>'users','action'=>'free_video','admin'=>true));
						}
					}else
					{
						$this->set('error',$error);	
					}
				}
				else 
				{
					
					if(!empty($this->data['FreeVideo']['free_video_url']))
					{
						$data['FreeVideo']['free_video_url']= $this->data['FreeVideo']['free_video_url'];
						$data['FreeVideo']['own_video_url']='';
						//pr($data);die;
						if($this->FreeVideo->save($data))
						{	
							$this->Session->write('success','Video has been updated successfully');
							$this->redirect(array('controller'=>'users','action'=>'free_video','admin'=>true));
						}
					}
					if(!empty($old_info['FreeVideo']['free_video_url']) && empty($data['FreeVideo']['own_video_url']['name']))
					{
						$data['FreeVideo']['free_video_url']=$old_info['FreeVideo']['free_video_url'];
						$data['FreeVideo']['own_video_url']='';
						if($this->FreeVideo->save($data))
						{	
							$this->Session->write('success','Video has been updated successfully');
							$this->redirect(array('controller'=>'users','action'=>'free_video','admin'=>true));
						}
					}
					
					
					
					if(!empty($old_info['FreeVideo']['own_video_url']) && empty($data['FreeVideo']['own_video_url']['name']))
					{
						$data['FreeVideo']['own_video_url']=$old_info['FreeVideo']['own_video_url'];
						if($this->FreeVideo->save($data))
						{	
							$this->Session->write('success','Video has been updated successfully');
							$this->redirect(array('controller'=>'users','action'=>'free_video','admin'=>true));
						}
					}
					
				}
					
				
			}
			else
			{
				$this->set('error',$error);	
			}

		}
	}

	
	function validate_free_video_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_free_video($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['FreeVideo'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	function isValidUrl($url)
	{
		$urlregex = "^(https?)\:\/\/([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)*(\:[0-9]{2,5})?(\/([a-z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?(#[a-z_.-][a-z0-9+\$_.-]*)?\$";
		
		if(eregi($urlregex, $url))
		{			
			 return false;
		} else {
			 
			return true;
		}
	}
	
	function validate_free_video($data)
	{			
		$errors = array ();
		$locale = $data['FreeVideo']['locale'];
		
		if (trim($data['FreeVideo']['title_'.$locale])=="")
		{
			$errors ['title_'.$locale] [] = __(FIELD_REQUIRED,true)."\n";
		}
		if (trim($data['FreeVideo']['description_'.$locale])=="")
		{
			$errors ['description_'.$locale] [] = __(FIELD_REQUIRED,true)."\n";
		}
		
		 /* if(trim($data['FreeVideo']['viewer'])!='')
		{
			if(!is_numeric($data['FreeVideo']['viewer']))
			{
				$errors ['viewer'] [] = __('Please enter numeric value')."\n";
			}
			if(is_numeric($data['FreeVideo']['viewer']) && ($data['FreeVideo']['viewer']<0 || $data['FreeVideo']['viewer']>2147483647))
			{
				$errors ['viewer'] [] = __('Please enter value between 0 to 2147483647')."\n";
			}
		}
		if(trim($data['FreeVideo']['viewer'])=='')
		{
			$errors['viewer'][] = __(FIELD_REQUIRED,true)."\n";
		}
		if($data['FreeVideo']['video_type']=='free_video')
		{  */
			if(isset($data['FreeVideo']['free_video_url']))
			{
				if(trim($data['FreeVideo']['free_video_url']==""))
				{
					$errors['free_video_url'][]=__(FIELD_REQUIRED,true)."\n";
				}else
					if(!preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['FreeVideo']['free_video_url']) ){
				$errors['free_video_url'][]=__("Not a valid Youtube URL *")."\n";
				}
			}
			
		//}
		
		
		return $errors;			
	}
	
	 function admin_edit_member($id = NULL) {
        $this->layout = 'admin';
         $string_keywords='';
        if (!empty($id)) {
            $id = convert_uudecode(base64_decode($id));
            $this->loadModel('Member');
             $this->loadModel('MemberAuthorKeywords');
             $this->loadModel('MemberKeywords');
            $info = $this->Member->find('first', array('conditions' => array('Member.id' => $id), 'contain' => array()));
            if($info['Member']['role_id']==3){
                $keywords=$this->MemberAuthorKeywords->find('all',array('conditions' => array('MemberAuthorKeywords.m_id' => $info['Member']['id']), 'contain' => array()));
                foreach($keywords as $keyword){
                    $string_keywords.=$keyword["MemberAuthorKeywords"]["keyword"].",";
                }
            }
            else{
           $keywords=$this->MemberKeywords->find('all',array('conditions' => array('MemberKeywords.m_id' => $info['Member']['id']), 'contain' => array()));
           foreach($keywords as $keyword){
                    $string_keywords.=$keyword["MemberKeywords"]["keyword"].",";
                }
            }
            $string_keywords=trim($string_keywords,','); 
            $this->loadModel("Role");
        $roles=$this->Role->find('all');
        $role=array();
        foreach($roles as $row=>$roles)
        {
            if($roles["Role"]["id"]!=1)
           $role[$roles["Role"]["id"]]=$roles["Role"]["role"];
        }      
       
            $this->set(compact('info','string_keywords','role'));
        }
        if (!empty($this->data)) {
        
            
            $error = $this->validate_edit_member($this->data);

            if (count($error) == 0) {
                  $this->request->data['Member']['given_name'] =$this->request->data['Member']['final_name'];
                $this->loadModel('Member');
                if(isset($this->request->data['Member']['remove_picture']) && $this->request->data['Member']['remove_picture']==1){
                      @unlink('files/members/profile/' . $this->data['Member']['old_image']);
                @unlink('files/members/profile/thumb/' . $this->data['Member']['old_image']);
                 $this->request->data['Member']['image']='';
                }
              
             // if (!empty($_FILES['image']['name']) && $this->data['Member']['role_id'] == 3) {
                 //   $destination = 'files/members/profile/';
                //    $thumb = 'files/members/profile/thumb/';
                 $destination = $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/files/members/profile/";
                    $thumb = $_SERVER['DOCUMENT_ROOT'] . '/kuulum/app/webroot/files/members/profile/thumb/';
                   // echo $destination;
                    $imgName = pathinfo($_FILES['image']['name']);
                    $ext = strtolower($imgName['extension']);

                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                        $newImgName = md5(time()) . "." . $ext;
                        $img = $_FILES['image'];

                        App::uses('UploadComponent', 'Controller/Component');
                        $this->Upload = new UploadComponent(new ComponentCollection());
                        $result = $this->Upload->upload($img, $thumb, $newImgName, array('type' => 'resize', 'size' => '250', 'quality' => '100'));
                        $result1 = $this->Upload->upload($img, $destination, $newImgName, array('type' => 'resize', 'size' => '800', 'quality' => '100'));
                        chmod($thumb.$newImgName, 0777);
                        chmod($destination.$newImgName, 0777);
                        
                        if ($result1) {
                            //@unlink('files/members/profile/' . $this->data['Member']['old_image']);
                          //  @unlink('files/members/profile/thumb/' . $this->data['Member']['old_image']);
                            $this->request->data['Member']['image'] = $newImgName;
                        }
                    }
               // } 


                //if($this->data['Member']['role_id'] == 3)
                //{
//**************************************THIS IS FOR NOTIFICATIONS ******************		
                $this->loadModel('Notification');
                $notification['Notification']['mem_id'] = 0;
                $notification['Notification']['notification_sender_id'] = $this->data['Member']['id'];
                $notification['Notification']['notification_type'] = 'become_author';
                $notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                $notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
                $this->Notification->create();
                $this->Notification->save($notification);
//**************************************THIS IS FOR NOTIFICATIONS ******************
                //}

                $this->Member->save($this->data);


               /* if ($this->data['Member']['old_type'] != $this->data['Member']['role_id']) {
                    if ($this->data['Member']['role_id'] == 2) {
                        $profileType = 'Member Profile';
                    } else {
                        $profileType = 'Author Profile';
                    }
                    $getMemberDetail = $this->Member->find('first', array('conditions' => array('Member.id' => $this->data['Member']['id']), 'contain' => array()));
                    $email = $getMemberDetail['Member']['email'];
                    $final_name = $getMemberDetail['Member']['final_name'];
                    $this->loadModel('EmailTemplate');
                    $emailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('id' => 6), 'fields' => array('subject', 'description', 'email_from')));
                    $emailData = $emailTemplate['EmailTemplate']['description'];
                    $emailData = str_replace(array('{final_name}', '{profile_type}'), array($final_name, $profileType), $emailData);
                    $emailTo = $email;
                    $emailFrom = $emailTemplate['EmailTemplate']['email_from'];
                    $emailSubject = $emailTemplate['EmailTemplate']['subject'];
                    //$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
                } */
                $this->Session->write('success', 'Member information has been updated successfully');
                $this->redirect(array('controller' => 'users', 'action' => 'members', 'admin' => true));
            } else {
                $this->set('error', $error);
            }
        }
    }
	
	function admin_edit_author($id=NULL)
	{
		$this->layout='admin';
		if(!empty($id))
		{
			$id=convert_uudecode(base64_decode($id));
			$this->loadModel('Member');
			$info=$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));
			$this->set(compact('info'));
			
			
		}
		if(!empty($this->data))
		{	
			$error=$this->validate_edit_member($this->data);
			
			if(count($error)==0){
				$this->loadModel('Member');
				
				if(!empty($_FILES['image']['name']) && $this->data['Member']['role_id'] == 3)
				{
					$destination = 'files/members/profile/';
					$thumb = 'files/members/profile/thumb/';
					
					$imgName = pathinfo($_FILES['image']['name']);
					$ext = strtolower($imgName['extension']);

					if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
					{
						$newImgName = md5(time()).".".$ext;						
						$img = $_FILES['image'];
						
						App::uses('UploadComponent', 'Controller/Component'); 
						$this->Upload = new UploadComponent(new ComponentCollection());					
						$result = $this->Upload->upload($img, $thumb, $newImgName, array('type'=>'resize','size'=>'250', 'quality'=>'100'));	
						$result1 = $this->Upload->upload($img, $destination, $newImgName, array('type'=>'resize','size'=>'800', 'quality'=>'100'));

						if($result1)
						{
							@unlink('files/members/profile/'.$this->data['Member']['old_image']);
							@unlink('files/members/profile/thumb/'.$this->data['Member']['old_image']);
							$this->request->data['Member']['image'] = $newImgName;			
						}
					}
				}
				
				if($this->data['Member']['role_id']==2)
				{
//**************************************THIS IS FOR NOTIFICATIONS ******************		
					$this->loadModel('Notification');
					$notification['Notification']['mem_id'] = 0;
					$notification['Notification']['notification_sender_id'] = $this->data['Member']['id'];
					$notification['Notification']['notification_type'] = 'become_member';
					$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
					$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
					$this->Notification->create();
					$this->Notification->save($notification);		
//**************************************End IS FOR NOTIFICATIONS ******************
				}
				
				$this->Member->save($this->data);
				if($this->data['Member']['old_type']!=$this->data['Member']['role_id'])
				{
				    if($this->data['Member']['role_id']==2)
					{
						$profileType='Member Profile';
					}
					else
					{
						$profileType='Author Profile';
					}
					$getMemberDetail =$this->Member->find('first',array('conditions'=>array('Member.id'=>$this->data['Member']['id']),'contain'=>array()));				
					$email=$getMemberDetail['Member']['email'];
					$final_name=$getMemberDetail['Member']['final_name'];
					$this->loadModel('EmailTemplate');									
					$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('id'=>6),'fields'=>array('subject','description','email_from')));						
					$emailData = $emailTemplate['EmailTemplate']['description'];						
					$emailData = str_replace(array('{final_name}','{profile_type}'),array($final_name,$profileType),$emailData);
					$emailTo =$email;
					$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
					$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
					//$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
				}
				$this->Session->write('success','Author information has been updated successfully');
				$this->redirect(array('controller'=>'users','action'=>'authors'));
			}else{
				$this->set('error',$error);	
			}
		}
	}
        function admin_edit_member_interest($id = NULL) {
        $this->layout = 'admin';
        $this->loadModel('MemberKeywords');

        if (!empty($id)) {
            $id = convert_uudecode(base64_decode($id));

            $info = $this->MemberKeywords->find('first', array('fields' => array('MemberKeywords.*'), 'conditions' => array('MemberKeywords.id' => $id), 'contain' => array(true)));
            //$info=$this->MemberKeywords->find('first',array('conditions'=>array('MemberKeywords.id'=>$id),'contain'=>array()));
            $this->set(compact('info'));
        }

        if ($this->request->data) {
            $m_id = base64_encode(convert_uuencode($this->request->data['MemberKeywords']['m_id']));
            // $error=$this->validate_keywords($this->request->data);
            // if(count($error)==0){

            $this->request->data['MemberKeywords']['date_added'] = date('Y-m-d H:i:s');

            $this->MemberKeywords->save($this->request->data);


            $this->Session->write('success', 'Interest have been added successfully');
            $this->redirect(array('controller' => 'users', 'action' => 'view_member_interests/2/' . $m_id, 'admin' => true));
        }
        //else{
        //	$this->set('error',$error);
        //}
    }
        function admin_edit_author_interest($id = NULL) {
        $this->layout = 'admin';
        $this->loadModel('MemberAuthorKeywords');

        if (!empty($id)) {
            $id = convert_uudecode(base64_decode($id));


            $info = $this->MemberAuthorKeywords->find('first', array('fields' => array('MemberAuthorKeywords.*'), 'conditions' => array('MemberAuthorKeywords.id' => $id), 'contain' => array(true)));
            $this->set(compact('info'));
        }

        if ($this->request->data) {
            $m_id = base64_encode(convert_uuencode($this->request->data['MemberAuthorKeywords']['m_id']));
            // $error=$this->validate_keywords($this->request->data);
            // if(count($error)==0){

            $this->request->data['MemberAuthorKeywords']['date_added'] = date('Y-m-d H:i:s');

            $this->MemberAuthorKeywords->save($this->request->data);


            $this->Session->write('success', 'Interest have been updated successfully');
            $this->redirect(array('controller' => 'users', 'action' => 'view_member_interests/3/' . $m_id, 'admin' => true));
        }
    }
	
  function	admin_change_password()
  {
 	 $this->layout='admin';
  	 $username=$this->Admin->find('first',array('conditions'=>array('Admin.id'=>$this->Session->read('LocTrainAdmin.id'))));
	 $this->set('username',$username);
	 if(!empty($this->data)){ 
		$error=array();
		$error=$this->change_password_ajax($this->data);	
		if(count($error)==0){
			$data['Admin']['id']=$this->data['ChangePass']['id'];
			$data['Admin']['password']=md5($this->data['ChangePass']['new_pass']);
			$this->Admin->save($data);
			$this->Session->write('success','Password has been updated successfully');
			$this->redirect(array('controller'=>'Users','action'=>'change_password','admin'=>true));
		}
		else{
			$this->set('error',$error);
			}
		}
  	}
  
  	function validate_change_password_ajax()
	{
		$this->layout="";
		$this->autoRender=false;
		
		if($this->RequestHandler->isAjax())
		{
			$errors_msg = null;
			$errors=$this->change_password_ajax($this->data);
						
			if ( is_array ( $this->data ) )
			{
				foreach ($this->data['ChangePass'] as $key => $value )
				{
					if( array_key_exists ( $key, $errors) )
					{
						foreach ( $errors [ $key ] as $k => $v )
						{
							$errors_msg .= "error|$key|$v";
						}	
					}
					else 
					{
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function change_password_ajax($data)
	{			
		$errors = array ();
		$admin_info=$this->Admin->findById($data['ChangePass']['id']);	
		if (trim($data['ChangePass']['old_pass'])==""){
			$errors ['old_pass'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		if (trim($data['ChangePass']['new_pass'])==""){
			$errors ['new_pass'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		if (trim($data['ChangePass']['con_pass'])==""){
			$errors ['con_pass'] [] = __(FIELD_REQUIRED,true)."\n";
		}		
		elseif($admin_info['Admin']['password']!=md5($data['ChangePass']['old_pass'])){
			$errors ['old_pass'] [] = __('Please enter correct old password',true)."\n";
		}
		elseif(trim($data['ChangePass']['new_pass'])!=trim($data['ChangePass']['con_pass'])){
			$errors ['con_pass'] [] = __(PASSWORD_MISTACHMATCH,true)."\n";
		}
		return $errors;			
	}
	function admin_logout()
	{
		$this->Session->delete('LocTrainAdmin');

		$this->redirect(array('action' => 'login','admin' => true));
	}
	
	function validate_member_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_member($this->data);						
			if(is_array ($this->data) )	{
				foreach ($this->data['Member'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_member($data)
	{			
		$errors = array ();		
		if($data['Member']['final_name'] == ''){
			$errors['final_name'][] = FIELD_REQUIRED."\n";
		}
		return $errors;			
	}
	
	//--- MANAGE courses---//
	/*function admin_courses()
	{
		
		$this->layout='admin';	

		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		
		$this->loadModel('Course');
		$this->loadModel('Language');
		//$courses = $this->Course->find('all');
		
		$lang = $this->Language->find('list',array('fields'=>'Language.language'));
		//pr($lang);
		$this->paginate=array('limit'=>'10','order'=>'Course.id DESC');
		$info=$this->paginate('Course');
		
		
		$this->set(compact('page','info','lang'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('cours_list');
		}
	}*/
	
	function admin_courses($searchcondition=null)
	{
		
		$this->layout='admin';	

		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		
		$this->loadModel('Course');
		$this->loadModel('Language');
		//$courses = $this->Course->find('all');
		$lang = $this->Language->find('list',array('fields'=>'Language.language'));
		
		$condition = array();
		if(empty($searchcondition))
		{ 
			$this->Session->delete('Course');
			
			$this->Session->write('searchConditions',$condition);
		}
		if(!empty($this->data))
		{ 
			if($this->data['search']['type']=="title" && $this->data['search']['title']!=''){
				$condition=array_merge($condition,array("Course.title LIKE"=> '%'.$this->data['search']['title'].'%'));
				$this->Session->write('Course.title',$this->data['search']['title']);
			}
			if($this->data['search']['type']=="primarylanguage" && $this->data['search']['language']!=''){
				$lang = $this->data['search']['language'];
				$condition=array_merge($condition,array("Course.primary_lang"=>$lang ));
				$this->Session->write('Course.lang',$lang);
				
			}
                        if($this->data['search']['type']=="date_added" && $this->data['search']['date_added']!=''){
				
				$date = strtotime($this->data['search']['date_added']);            // works
				$new_date = strtotime($this->data['search']['date_added'].'+1 day');
                                $condition=array_merge($condition,array("Course.date_added BETWEEN ? AND ?"=>array($date,$new_date)));
			}
			if($this->data['search']['type']=="author" && $this->data['search']['author']!=''){
				$lang = $this->data['search']['author'];
				$condition=array_merge($condition,array("Member.final_name LIKE"=>'%'.$lang.'%' ));
				$this->Session->write('Course.author',$lang);
				
			}
			if($this->data['search']['type']=="status" && $this->data['search']['status']!=''){
				$condition=array_merge($condition,array("Course.status"=>$this->data['search']['status']));
				$this->Session->write('Course.status',$this->data['search']['type']);
			}
			$this->Session->write('searchConditions',$condition);
		}
		
		$search_conditions = $this->Session->read('searchConditions');
		
		$this->paginate=array('order'=>'Course.id DESC');
		$info=$this->paginate('Course',$search_conditions);
               
               
		$this->set(compact('page','info','lang'));
                
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('cours_list');
		}
		
		
	}
  
   function  admin_view_course($id)
   {
       $course_id=$id;
        $this->layout='admin';
		if(!empty($id))
		{
			$this->loadModel('Course');
			$this->loadModel('Member');
			$id=convert_uudecode(base64_decode($id));
                       
			$info=$this->Course->findById($id);
                         
			$memberInfo=$this->Member->findById($info['Course']['m_id']);
                        
			$this->set(compact('info','memberInfo','course_id'));
		}
   }
   
   function admin_edit_course($id=NULL)
   {
		$this->layout='admin';
		$id=convert_uudecode(base64_decode($id));
		
		if(!empty($id))
		{
		    $this->loadModel('Course');
			
			$coursName=$this->Course->find('first',array('conditions'=>array('Course.id'=>$id),'fields'=>array('id','title','description','price','meta_title','meta_description','meta_keyword'),'contain'=>false));
			$coursPrice=$this->CoursePrice->find('list',array('fields'=>array('price','price'),'order'=>array('CoursePrice.price ASC')));
			
			$this->set(compact('coursName','coursPrice'));		
		}

		if(!empty($this->data))
		{
			$error=$this->validate_cours($this->data);	
			
			if(count($error)==0)
			{
				$data =  $this->data;
				$data['Course']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$this->Course->save($data);				
				$this->Session->write('success','Course has been updated successfully');
				$this->redirect(array('controller'=>'users','action'=>'courses','admin'=>true));
			}else{
				$this->set('error',$error);	
			}
		}	
	}
	
	function validate_cours_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_cours($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Course'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}

	
	function validate_cours($data)
	{			
		$errors = array ();
		
		if (trim($data['Course']['title']) == ""){
			$errors['title'][] = __(FIELD_REQUIRED)."\n";
		}
		elseif(strlen($data['Course']['title']) > 250 ){
			$errors['title'][] = __("A maximum of 250 characters are allowed")."\n";
		}
		if(is_numeric($data['Course']['title']))
		{
			$errors ['title'] [] = "Numeric value not allowed"."\n";
		}
		if (trim($data['Course']['description'])=="")
		{
			$errors ['description'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		
		if(trim($data['Course']['price'])!='')
		{
			if(!is_numeric($data['Course']['price']))
			{
				$errors ['price'] [] = __('Please enter integer')."\n";
			}
		}
		if (trim($data['Course']['price'])=="")
		{
			$errors ['price'] [] = __(FIELD_REQUIRED,true)."\n";
		}
		
		
		return $errors;			
	}

	
	
	
	function find_subcategories()
	{
		$this->loadModel('CourseSubcategory');
		
		$category_id = $_GET['category_id'];
		if($this->request->is('ajax'))
		{
			$subs='<option value="">Select Sub Category</option>';
			$sub_categories = $this->CourseSubcategory->find('list',array('fields'=>array('id','en'),'conditions'=>array('CourseSubcategory.category_id'=>$category_id,'CourseSubcategory.status'=>'1'),'order'=>array('CourseSubcategory.en' =>'ASC')));
			
			if(!empty($sub_categories))
			{
				foreach($sub_categories as $key=>$val)
				{
					$subs .= '<option value="'.$key.'">'.$val.'</option>';
				}
				
				$subs .='<option value="0">***others***</option><option value="NULL" style="display:none">NULL</option>'; 	
				echo $subs;
			}
			else
			{
				echo 'no';
			}
		}
		die;
	}
	
	function find_sl_subcategories()
	{
	
		$this->loadModel('CourseSlSubcategory');
		
		$subcategory_id = $_POST['subcategory_id'];
		
		if(!empty($subcategory_id) && $this->request->is('ajax'))
		{
			
			$subs='<option value="">Select SL Sub-Category</option>';
			
			$sub_categories = $this->CourseSlSubcategory->find('list',array('conditions'=>array('CourseSlSubcategory.sub_category_id'=>$subcategory_id,'CourseSlSubcategory.status'=>'1'),'order'=>array('CourseSlSubcategory.en' =>'ASC'),'fields'=>array('id','en')));
			
			
			if(!empty($sub_categories))
			{
				foreach($sub_categories as $key=>$val)
				{
					$subs .= '<option value="'.$key.'">'.$val.'</option>';
				}
				
				$subs .='<option value="0">***others***</option><option value="NULL" style="display:none">NULL</option>'; 	
				echo $subs;
			}
			else
			{
				echo 'no';
			}
		}
		die;
	}
 //--- END MANAGE courses---//
 	
	function sortMember_condition()
	{
		$conditions=array();
		$action = $this->params['action'];
		if(isset($this->params['named']['condition']))
		{
			$this->set('qryStr',$this->params['named']['condition']);
			$qryStr=$this->params['named']['condition'];
			$con=explode("||-",base64_decode($qryStr));
			
			if($con[count($con)-1]=='')
			{
				array_pop($con);
			}
			
			$or['OR']=array();
			foreach($con as $val):
				$qry=explode("-||",$val);
				if($qry[0]=="Member.email")
				{	
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.email'=>$var));
				}
				if($qry[0]=="Member.date_added")
				{
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.date_added'=>$var));
				}

			endforeach;
		}
		return $conditions;
	}
	
	
	function admin_members_report($query=NULL)
	{
		Configure::write('debug',0);
		$this->layout='';
		$condition=array();
		$qryStr='';
		if(!isset($this->params['named']['condition'])){
			$this->request->params['named']['condition']=$query;
			$condition=$this->sortMember_condition();
			$qryStr=$this->params['named']['condition'];
		}
		$this->loadModel('Member');
		$info = $this->Member->find('all',array('conditions'=>$condition ,'order'=>'Member.id DESC'));
		
		$this->set(compact('info'));	
	}
	
	function admin_add_member()
	{
		$this->layout='admin';
		$this->loadModel("Role");
        $roles=$this->Role->find('all');
        $role=array();
        foreach($roles as $row=>$roles)
        {
             if($roles["Role"]["id"]!=1)
           $role[$roles["Role"]["id"]]=$roles["Role"]["role"];
        }
        //print_r($role);
        //die("fdf");
        $this->set(compact('role'));
	}
       function admin_add_member_interest($role_id=0,$id=NULL)
	{
		$this->layout='admin';
		 $this->loadModel('MemberKeywords');
		 $this->loadModel("MemberAuthorKeywords");
                $m_id=convert_uudecode(base64_decode($id));
                $this->set(compact('m_id','role_id'));
                           
//echo $rolr_id;
          
          
		if($this->request->data)
                {
                  $id = base64_encode(convert_uuencode($this->request->data['MemberKeywords']['m_id'])) ;  
                  
                    $role_id = $this->request->data['MemberKeywords']['role_id'] ;
                    $author_data=array();
                    if($role_id!=0){
                       
                   if($role_id==3){
                       
                        $author_data['MemberAuthorKeywords']['m_id']=  $this->request->data['MemberKeywords']['m_id'];
                        $author_data['MemberAuthorKeywords']['keyword']=$this->request->data['MemberKeywords']['keyword'];
                         
                         $author_data['MemberAuthorKeywords']['status']= 1;
                       
                        //$this->loadModel("MemberAuthorKeywords");
                      
                     // print_r($this->request->data);
                       //  die;
                        $this->MemberAuthorKeywords->save($author_data);
                    }
                    else{
                           $this->request->data['MemberKeywords']['status']= 1;
                        
                         //print_r($this->request->data);
                        // die;
     
                    $this->MemberKeywords->save($this->request->data);
                    }
				
		    $this->Session->write('success','Interest have been added successfully');
		    $this->redirect(array('controller'=>'users','action'=>'view_member_interests/'.$role_id."/".$id,'admin'=>true));
			}
			else{
				$this->set('error',$error);
			}
		}
	}



	 function admin_add_newMember() {
        $this->layout = 'admin';
        $this->loadModel("Role");
        $roles=$this->Role->find('all');
        $this->set(compact('roles'));
        if (!empty($this->data)) {
            $error = array();
            $error = $this->validate_add_newMember($this->data);
            if (count($error) == 0) {
                $this->loadModel('Member');
                $this->loadModel("MemberAuthorKeywords");
                $this->loadModel("MemberKeywords");
                $job_id = str_shuffle('1234567890');
                $job_id = substr($job_id, 0, 6);
                //pr($job_id);die;
                $this->request->data['Member']['mem_acc_no'] = $job_id;
                //$this->request->data['Member']['password'] = md5($this->data['Member']['password']);
                $this->request->data['Member']['date_added'] = strtotime(date('Y-m-d H:i:s'));
                $this->request->data['Member']['given_name'] =$this->request->data['Member']['final_name'];

                if (!empty($_FILES['image']['name'])) {

                   $destination = $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/files/members/profile/";
                    $thumb = $_SERVER['DOCUMENT_ROOT'] . '/kuulum/app/webroot/files/members/profile/thumb/';

                    $imgName = pathinfo($_FILES['image']['name']);
                    $ext = strtolower($imgName['extension']);

                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                        $newImgName = md5(time()) . "." . $ext;
                        $img = $_FILES['image'];

                        App::uses('UploadComponent', 'Controller/Component');
                        $this->Upload = new UploadComponent(new ComponentCollection());
                      //  print_r($this->Upload);
                      //  echo ", $thumb, $newImgName";

                        $result = $this->Upload->upload($img, $thumb, $newImgName, array('type' => 'resize', 'size' => '250', 'quality' => '100'));
                        $result1 = $this->Upload->upload($img, $destination, $newImgName, array('type' => 'resize', 'size' => '800', 'quality' => '100'));
                        chmod($thumb.$newImgName, 0777);
                        chmod($destination.$newImgName, 0777);
                        
                        if ($result1) {
                            $this->request->data['Member']['image'] = $newImgName;
                        }
                    }
                }

                if ($this->Member->save($this->data)) {
                    $member_id = $this->Member->getLastInsertId();
                    // echo $this->request->data["Member"]["keywords"];
                    if ($this->request->data["Member"]["keywords"]) {
                        $keywords = explode(",", $this->request->data["Member"]["keywords"]);
                        //print_r($keywords);
                        $keyword_values = array();


                        if ($this->request->data['Member']['role_id'] == 3) {
                            foreach ($keywords as $keyword) {
                                $values["MemberAuthorKeywords"] = array('m_id' => $member_id, "keyword" => $keyword, "status" => 1);
                                array_push($keyword_values, $values);
                            }
                            // $key_data["MemberAuthorKeywords"]=$keyword_values;

                            $this->MemberAuthorKeywords->saveMany($keyword_values);
                        } else {
                            foreach ($keywords as $keyword) {
                                $values["MemberKeywords"] = array('m_id' => $member_id, "keyword" => $keyword, "status" => 1);
                                array_push($keyword_values, $values);
                            }
                            //$key_data["MemberKeywords"]=$keyword_values;

                            $this->MemberKeywords->saveMany($keyword_values);
                        }
                    }
                    App::uses('Folder', 'Utility');
                    $dir = new Folder();
                    $dir->create('files' . DS . 'members' . DS . $member_id);
                    $this->Session->write('success', 'Member have been added successfully');
                    $this->redirect(array('controller' => 'users', 'action' => 'members', 'admin' => true));
                }
            } else {
                $this->set(compact('error'));
            }
        }
    }
	
	function validate_add_newMember_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_add_newMember($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Member'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	   function validate_add_newMember($data) {
        $errors = array();
        if (trim($data['Member']['final_name']) == '') {
            $errors['final_name'][] = FIELD_REQUIRED . "\n";
        }
        if (is_numeric($data['Member']['final_name'])) {
            $errors ['final_name'] [] = "Numeric value not allowed" . "\n";
            }		
	        if (!trim($data['Member']['locale'])) {		
	            $errors ['locale'] [] = "Select language" . "\n";
        }
        if (trim($data['Member']['email']) == "") {
            $errors['email'][] = FIELD_REQUIRED . "\n";
        } elseif ($this->validEmail(trim($data['Member']['email'])) == 'false') {
            $errors['email'][] = INVALID_EMAIL . "\n";
        } elseif (strlen(trim($data['Member']['email'])) > 150) {
            $errors['email'][] = EMAIL_LENGTH_VALIDATION . "\n";
        } elseif (trim($data['Member']['email'])) {
            $this->loadModel('Member');
            $chkEmail = $this->Member->find('count', array('conditions' => array('Member.email' => trim($data['Member']['email']))));
            if ($chkEmail > 0) {
                $errors['email'][] = EMAIL_EXISTS . "\n";
            }
        }
        /* 	if(trim($data['Member']['password']) == ''){
          $errors['password'][] = FIELD_REQUIRED."\n";
          }
          else if(trim($data['Member']['password']) != ''){
          if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()\-_=+{};:,<.>~]).{6,20}$/", trim($data['Member']['password']))) {
          $errors['password'][] = 'Password does not meet the basic requirements'."\n";
          }
          } */

        if ($data['Member']['notification'] == '' || $data['Member']['notification'] > '1') {
            $errors['notification'][] = FIELD_REQUIRED . "\n";
        }
        /* if(trim($data['Member']['given_name']) == ''){
          $errors['given_name'][] = FIELD_REQUIRED."\n";
          }
          if(is_numeric($data['Member']['given_name']))
          {
          $errors ['given_name'] [] = "Numeric value not allowed"."\n";
          } */

        //if(trim($data['Member']['role_id']) == '3')
        //{
        //	if(trim($data['Member']['about_me']) == ''){
        //		$errors['about_me'][] = FIELD_REQUIRED."\n";
        //	}
        if (trim($data['Member']['picture']) != '') {
            $info = pathinfo($data['Member']['picture']);
            $ext = strtolower($info['extension']);
            $extArr = array('jpg', 'jpeg', 'png', 'gif');

            if (!in_array($ext, $extArr)) {
                $errors ['picture'] [] = __('Image format not recognized. Please try again.') . "\n";
            }
        }
        //}
        //  print_r($errors);
        //die;
        return $errors;
    }
	
	function validate_edit_member_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_member($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Member'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	  function validate_edit_member($data) {
        $errors = array();

        if (trim($data['Member']['final_name']) == '') {
            $errors['final_name'][] = FIELD_REQUIRED . "\n";
        }
        if (is_numeric($data['Member']['final_name'])) {
            $errors ['final_name'] [] = "Numeric value not allowed" . "\n";
        }
         if (!trim($data['Member']['locale'])) {		
	            $errors ['locale'] [] = "Select language" . "\n";
        }
       /*  if ($data['Member']['notification'] == '' || $data['Member']['notification'] > '1') {
            $errors['notification'][] = FIELD_REQUIRED . "\n";
        }
        if(trim($data['Member']['given_name']) == ''){
          $errors['given_name'][] = FIELD_REQUIRED."\n";
          }
          if(is_numeric($data['Member']['given_name']))
          {
          $errors ['given_name'] [] = "Numeric value not allowed"."\n";
          } */
//		if(trim($data['Member']['role_id']) == '3')
//		{
//			if(trim($data['Member']['about_me']) == ''){
//				$errors['about_me'][] = FIELD_REQUIRED."\n";
//			}
        if (trim($data['Member']['picture']) != '') {
            $info = pathinfo($data['Member']['picture']);
            $ext = strtolower($info['extension']);
            $extArr = array('jpg', 'jpeg', 'png', 'gif');

            if (!in_array($ext, $extArr)) {
                $errors ['picture'] [] = __('Image format not recognized. Please try again.') . "\n";
            }
        }
        //}

        return $errors;
    }
	
	function admin_author_requests()
	{
		$this->layout='admin';
		$this->loadModel('Member');		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';

		$condition=array('Member.upgrade_profile'=>'1');
		
		if( ! isset($this->params['named']['page']))
		{
			$this->Session->delete('condition');
			$this->Session->write('condition',$condition);
		}
		
		if(!empty($this->data))
		{
			if($this->data['search']['type']=="email" && $this->data['search']['text']!=''){
				$condition=array_merge($condition,array("Member.email"=>$this->data['search']['text']));
			}
                        if($this->data['search']['type']=="name" && $this->data['search']['text']!=''){
				$condition=array_merge($condition,array("Member.final_name"=>$this->data['search']['text']));
			}
			if($this->data['search']['type']=="date_requested" && $this->data['search']['date_request']!=''){
				
				$date = strtotime($this->data['search']['date_request']);            // works
				$new_date = strtotime($this->data['search']['date_request'].'+1 day');
				$condition=array_merge($condition,array("Member.date_request BETWEEN ? AND ?"=>array($date,$new_date)));
			}			
			$this->Session->write('condition',$condition);
		}
		
		$condition = $this->Session->read('condition');	
		
		$this->paginate=array('limit'=>'-1','conditions'=>$condition,'order'=>'Member.id DESC');
		$info=$this->paginate('Member');
		$this->set(compact('info','page'));
		
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('author_request_list');
		}
	}
	
	//------FUNCTION FOR CHANGE STATUS(ENABLE/DISABLE)-------------------------------------

	function admin_accept_author_request($id=NULL)
	{
		$id=convert_uudecode(base64_decode($id));
		if($this->Member->updateAll(array('Member.role_id'=>"'". 3 ."'",'Member.upgrade_profile'=>"'". 0 ."'"),array('Member.id'=>$id)))
		{
		
//**************************************THIS IS FOR NOTIFICATIONS ******************		
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] = 0;
			$notification['Notification']['notification_sender_id'] =$id;
			$notification['Notification']['notification_type'] = 'become_author';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$this->Notification->create();
			$this->Notification->save($notification);		
//**************************************THIS IS FOR NOTIFICATIONS ******************

				$this->Session->write('success','Author Request Accepted successfully');				
				$getMemberDetail =$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));				
				$email=$getMemberDetail['Member']['email'];
				$final_name=$getMemberDetail['Member']['final_name'];
				$this->loadModel('EmailTemplate');									
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.main_id'=>7,'EmailTemplate.locale'=>$getMemberDetail['Member']['locale']),'fields'=>array('subject','description','email_from')));						
				$emailData = $emailTemplate['EmailTemplate']['description'];						
				$emailData = str_replace(array('{final_name}'),array($final_name),$emailData);
				$emailTo =$email;
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
				$this->redirect(array('controller'=>'users','action'=>'author_requests','admin'=>true));
				
		}
		
	} 
        
        	function admin_accept_author_requests()
	{
                     if(!empty($this->data))
        {
                        
                 $selectedids=$this->data['selectedids'];
               //  print_r($selectedids);
              
                // print_r($selectedids);
                 // die;
		//$id=convert_uudecode(base64_decode($id));
		if($this->Member->updateAll(array('Member.role_id'=>"'". 3 ."'",'Member.upgrade_profile'=>"'". 0 ."'"),array('Member.id'=>$selectedids)))
		{
		
//**************************************THIS IS FOR NOTIFICATIONS ******************
                    foreach($selectedids as $id){
			$this->loadModel('Notification');
			$notification['Notification']['mem_id'] = 0;
			$notification['Notification']['notification_sender_id'] =$id;
			$notification['Notification']['notification_type'] = 'become_author';
			$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
			$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			$this->Notification->create();
			$this->Notification->save($notification);		
//**************************************THIS IS FOR NOTIFICATIONS ******************

				//$this->Session->write('success','Author Request Accepted successfully');				
				 $getMemberDetail =$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));				
				$email=$getMemberDetail['Member']['email'];
				$final_name=$getMemberDetail['Member']['final_name'];
				$this->loadModel('EmailTemplate');									
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.main_id'=>7,'EmailTemplate.locale'=>$getMemberDetail['Member']['locale']),'fields'=>array('subject','description','email_from')));						
				$emailData = $emailTemplate['EmailTemplate']['description'];						
				$emailData = str_replace(array('{final_name}'),array($final_name),$emailData);
				$emailTo =$email;
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData); 
                    }
                     $this->autoRender = false;
                    $this->Session->write('success','Author Request Accepted successfully');
                    echo "true";
	//$this->redirect(array('controller'=>'users','action'=>'author_requests','admin'=>true));
				
		}
		
	} 
        }
	
	function admin_reject_author_request($id=NULL)
	{
		$id=convert_uudecode(base64_decode($id));
//		if($this->Member->updateAll(array('Member.role_id'=>"'". 1 ."'",'Member.upgrade_profile'=>"'". 0 ."'"),array('Member.id'=>$id)))
if($this->Member->updateAll(array('Member.role_id'=>"'". 2 ."'",'Member.upgrade_profile'=>"'". 0 ."'"),array('Member.id'=>$id)))
		{
				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] = 0;
				$notification['Notification']['notification_sender_id'] =$id;
				$notification['Notification']['notification_type'] = 'reject_author';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$this->Notification->create();
				$this->Notification->save($notification);
				
				
				$this->Session->write('success','Author Request Rejected successfully');
				$getMemberDetail =$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));				
				$email=$getMemberDetail['Member']['email'];
				$final_name=$getMemberDetail['Member']['final_name'];
				$this->loadModel('EmailTemplate');									
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.main_id'=>4,'EmailTemplate.locale'=>$getMemberDetail['Member']['locale']),'fields'=>array('subject','description','email_from')));						
				$emailData = $emailTemplate['EmailTemplate']['description'];						
				$emailData = str_replace(array('{final_name}'),array($final_name),$emailData);
				$emailTo =$email;
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
				$this->redirect(array('controller'=>'users','action'=>'author_requests','admin'=>true));
				
		}
		
	} 
        
        function admin_reject_author_requests()
	{
		
//		if($this->Member->updateAll(array('Member.role_id'=>"'". 1 ."'",'Member.upgrade_profile'=>"'". 0 ."'"),array('Member.id'=>$id)))
             if(!empty($this->data))
        {
                       
                 $selectedids=$this->data['selectedids'];
if($this->Member->updateAll(array('Member.role_id'=>"'". 2 ."'",'Member.upgrade_profile'=>"'". 0 ."'"),array('Member.id'=>$selectedids)))
		{
    foreach($selectedids as $id){

				$this->loadModel('Notification');
				$notification['Notification']['mem_id'] = 0;
				$notification['Notification']['notification_sender_id'] =$id;
				$notification['Notification']['notification_type'] = 'reject_author';
				$notification['Notification']['date_added'] = strtotime(date('Y-m-d H:i:s'));
				$notification['Notification']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
				$this->Notification->create();
				$this->Notification->save($notification);
				
				
				
				$getMemberDetail =$this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array()));				
				$email=$getMemberDetail['Member']['email'];
				$final_name=$getMemberDetail['Member']['final_name'];
				$this->loadModel('EmailTemplate');									
				$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.main_id'=>4,'EmailTemplate.locale'=>$getMemberDetail['Member']['locale']),'fields'=>array('subject','description','email_from')));						
				$emailData = $emailTemplate['EmailTemplate']['description'];						
				$emailData = str_replace(array('{final_name}'),array($final_name),$emailData);
				$emailTo =$email;
				$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
				$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
				//$this->redirect(array('controller'=>'users','action'=>'author_requests','admin'=>true)); 
				
                }
                 $this->autoRender = false;
                $this->Session->write('success','Author Request Rejected successfully');
                echo "true";
    }	
    
		//$this->redirect(array('controller'=>'users','action'=>'author_requests','admin'=>true));		
		}
        }
		
	
//----END OF FUNCTION FOR CHANGE STATUS(ENABLE/DISABLE)--

//--- MANAGE AUTHOR---//
	function admin_authors()
	{
		$this->layout='admin';
		$this->loadModel('Member');		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		
		$condition=array('Member.role_id'=>'3');
		
		if( ! isset($this->params['named']['page']))
		{
			$this->Session->delete('condition');
			$this->Session->write('condition',$condition);
		}
		
		if(!empty($this->data))
		{
			if($this->data['search']['type']=="email" && $this->data['search']['text']!=''){
				$condition=array_merge($condition,array("Member.email"=>$this->data['search']['text']));
			}
			if($this->data['search']['type']=="date_register" && $this->data['search']['register_date']!=''){
				
				$date = strtotime($this->data['search']['register_date']);            // works
				$new_date = strtotime($this->data['search']['register_date'].'+1 day');
				$condition=array_merge($condition,array("Member.date_added BETWEEN ? AND ?"=>array($date,$new_date)));
			}
			if($this->data['search']['type']=="status" && $this->data['search']['status']!=''){
				$condition=array_merge($condition,array("Member.status"=>$this->data['search']['status']));
			}
			$this->Session->write('condition',$condition);
		}
		
		$condition = $this->Session->read('condition');
		
		$this->paginate=array('limit'=>'10','conditions'=>$condition,'order'=>'Member.id Desc');
		
		$info=$this->paginate('Member');
		
		$this->set(compact('info','page','courses'));
		//pr($info); die;
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('author_list');
		}
	}
//--- END MANAGE AUTHOR---//	
	function admin_role_responsibility()
	{
		$this->layout = 'admin';
		$this->loadModel('Role');	
		$page = isset($this->params['named']['page'])?$this->params['named']['page']:'1';		
		$this->paginate = array('order'=>'Role.id asc');
		$info = $this->paginate('Role');
		$this->set(compact('info','page','role_responsibility'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/manage';
			 $this->render('role_responsibility_list');
		}	
	}
	function admin_view_role_responsibility($id=NULL)
	{
		$this->layout='admin';
		if(!empty($id))
		{
			$this->loadModel('Role');			
			$id = convert_uudecode(base64_decode($id));
			$info = $this->Role->find('first',array('conditions'=>array('Role.id'=>$id),'contain'=>false));
			//pr($info);die;			
			$this->set(compact('info'));
			
		}
	}
        
        function admin_add_role_responsibility()
	{
		$this->layout='admin';		
		$this->loadModel('Role');
	//	$pageid = @convert_uudecode(base64_decode($id));
	//	$info = $this->Role->find('first',array('conditions'=>array('Role.id'=>$pageid),'contain'=>array()));
		$arr = array();
			
		
		//$this->set(compact('info','arr'));
		if(!empty($this->data)){ 
			$error=$this->validate_role_per($this->data);
                        
                        if(count($error)==0){
			//$data['Role']['id'] = $this->data['Role']['id'];
			$options = array('BC','EAP','EOM','PC','PS','CC','PN','MM','MC','MP','MA','VCFR');
			foreach($options  as $val){
				$data['Role'][$val] = '0';
			}
			foreach($this->data['Role']['permission']  as $val){
				$data['Role'][$val] = '1';
			}
                        
                        $data["Role"]["role"]=$this->data["Role"]["role"];
                        
			$this->Role->save($data); 
			$this->Session->write('success','Information has been updated successfully');
			$this->redirect(array('action'=>'role_responsibility','admin' => true));									
                }
                else {
                $this->set(compact('error'));
            }
                        }
	}
        
	function admin_edit_role_responsibility($id=NULL)
	{
		$this->layout='admin';		
		$this->loadModel('Role');
		$pageid = @convert_uudecode(base64_decode($id));
		$info = $this->Role->find('first',array('conditions'=>array('Role.id'=>$pageid),'contain'=>array()));
		$arr = array();
		if(empty($info) && empty($this->data)){
			$this->redirect(array('action'=>'','role_responsibility' => true));	
		}	
		if(empty($this->data)){
			
			if($info['Role']['BC'] == '1'){
				$arr[] = 'BC';
			}
			if($info['Role']['EAP'] == '1'){
				$arr[] = 'EAP';
			}
			if($info['Role']['EOM'] == '1'){
				$arr[] = 'EOM';
			}
			if($info['Role']['PC'] == '1'){
				$arr[] = 'PC';
			}
			if($info['Role']['PS'] == '1'){
				$arr[] = 'PS';
			}
			if($info['Role']['CC'] == '1'){
				$arr[] = 'CC';
			}
			if($info['Role']['PN'] == '1'){
				$arr[] = 'PN';
			}
			if($info['Role']['MM'] == '1'){
				$arr[] = 'MM';
			}
			if($info['Role']['MC'] == '1'){
				$arr[] = 'MC';
			}
			if($info['Role']['MP'] == '1'){
				$arr[] = 'MP';
			}
			if($info['Role']['MA'] == '1'){
				$arr[] = 'MA';
			}
			if($info['Role']['VCFR'] == '1'){
				$arr[] = 'VCFR';
			}
		}
		$this->set(compact('info','arr'));
		if(!empty($this->data)){
                   $error=$this->validate_role_per($this->data);
                        
                        if(count($error)==0){
			$data['Role']['id'] = $this->data['Role']['id'];
                        $data['Role']['role'] = $this->data['Role']['role'];
			$options = array('BC','EAP','EOM','PC','PS','CC','PN','MM','MC','MP','MA','VCFR');
			foreach($options  as $val){
				$data['Role'][$val] = '0';
			}
			foreach($this->data['Role']['permission']  as $val){
				$data['Role'][$val] = '1';
			}
		
			$this->Role->save($data); 
			$this->Session->write('success','Information has been updated successfully');
			$this->redirect(array('action'=>'role_responsibility','admin' => true));									
                }else{
                    $this->set(compact('error')); 
                }
	}
        }
	
	function validate_role_per_ajax()
	{
            
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_role_per($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Role'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_role_per($data)
	{			
		$errors = array ();
                if (trim($data['Role']['role']) == ""){
			$errors['role'][] = FIELD_REQUIRED."\n";
		}
		
		if (empty($data['Role']['permission'])){
			$errors['permission'][] = SELECT_CHECKBOX."\n";
		}
		return $errors;			
	}
	
	function sortAuthor_condition()
	{
		$conditions=array();
		$action = $this->params['action'];
		
		if(isset($this->params['named']['condition']))
		{
			$this->set('qryStr',$this->params['named']['condition']);
			$qryStr=$this->params['named']['condition'];
			$con=explode("||-",base64_decode($qryStr));
			
			if($con[count($con)-1]=='')
			{
				array_pop($con);
			}
			
			$or['OR']=array();
			foreach($con as $val):
				$qry=explode("-||",$val);
				if($qry[0]=="Member.email")
				{	
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.email'=>$var));
				}
				if($qry[0]=="Member.date_added")
				{
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.date_added'=>$var));
				}
				if($qry[0]=="Member.role_id")
				{
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.role_id'=>$var));
				}

			endforeach;
		}
		return $conditions;
	}
	
	
	function admin_author_report($query=NULL)
	{
		Configure::write('debug',0);
		$this->layout='';
		$condition=array();
		$qryStr='';
		if(!isset($this->params['named']['condition'])){
			$this->request->params['named']['condition']=$query;
			$condition=$this->sortAuthor_condition();
			$qryStr=$this->params['named']['condition'];
			
		}
		$this->loadModel('Member');
		$info = $this->Member->find('all',array('conditions'=>$condition ,'order'=>'Member.id DESC'));
		$this->set(compact('info'));	
	}
	
	
	function sortauthreq_condition()
	{
		$conditions=array();
		$action = $this->params['action'];
		
		if(isset($this->params['named']['condition']))
		{
			$this->set('qryStr',$this->params['named']['condition']);
			$qryStr=$this->params['named']['condition'];
			$con=explode("||-",base64_decode($qryStr));
			
			if($con[count($con)-1]=='')
			{
				array_pop($con);
			}
			
			$or['OR']=array();
			foreach($con as $val):
				$qry=explode("-||",$val);
				if($qry[0]=="Member.email")
				{	
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.email'=>$var));
				}
				if($qry[0]=="Member.date_request")
				{
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.date_request'=>$var));
				}
				if($qry[0]=="Member.upgrade_profile")
				{
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Member.upgrade_profile'=>$var));
				}

			endforeach;
		}
		return $conditions;
	}
	
	
	function admin_authreq_report($query=NULL)
	{
		Configure::write('debug',0);
		$this->layout='';
		$condition=array();
		$qryStr='';
		if(!isset($this->params['named']['condition'])){
			$this->request->params['named']['condition']=$query;
			$condition=$this->sortauthreq_condition();
			$qryStr=$this->params['named']['condition'];
			
		}
		//pr($condition);die;
		$this->loadModel('Member');
		$info = $this->Member->find('all',array('conditions'=>$condition ,'order'=>'Member.id DESC'));
		
		$this->set(compact('info'));	
	}
	
	function admin_news()
	{
		$this->layout='admin';
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
                $condition=array('News.id !='=>'0');
                if( ! isset($this->params['named']['page']))
		{
			$this->Session->delete('condition');
			//$this->Session->write('condition',$condition);
		}
                if(!empty($this->data))
		{
                        if($this->data['search']['type']=="name" && $this->data['search']['text']!=''){
				$condition=array_merge($condition,array("News.title_en"=>$this->data['search']['text']));
			}
                        
			
			if($this->data['search']['type']=="date_register" && $this->data['search']['register_date']!=''){
				
				$date = strtotime($this->data['search']['register_date']);            // works
				$new_date = strtotime($this->data['search']['register_date'].'+1 day');
				$condition=array_merge($condition,array("News.date_added BETWEEN ? AND ?"=>array($date,$new_date)));
			}
			if($this->data['search']['type']=="status" && $this->data['search']['status']!=''){
				$condition=array_merge($condition,array("News.status"=>$this->data['search']['status']));
			}
			$this->Session->write('condition',$condition);
		}
                
                $condition = $this->Session->read('condition');
		
                
		$this->paginate=array('conditions'=>$condition,'order'=>'News.id DESC','contain'=>array('Admin')); 
		$info=$this->paginate('News');
		$this->set(compact('info','page'));
		//pr($info); die;
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $this->render('news_list');
		}
	}
	
	function admin_delete_commission($page=NULL,$id=NULL,$tableName=NULL,$renderPath=NULL,$renderElement=NULL)
	{
		$this->layout='';
		$id=convert_uudecode(base64_decode($id));
		$tableName=base64_decode($tableName);
		$renderPath=base64_decode($renderPath);
		$renderElement=base64_decode($renderElement);
		//pr($tableName);die;
		if($this->RequestHandler->isAjax())
		{
		$this->Commission->delete(array('Commission.id'=>$id));
		$this->paginate=array('limit'=>'10','order'=>'Commission.id DESC'); 
		$info=$this->paginate('Commission');
		$this->set(compact('info','page'));
		 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 
			 $this->render('commission_list');
		
		}
	
		}

	function admin_delete_record($page=NULL,$id=NULL,$tableName=NULL,$renderPath=NULL,$renderElement=NULL)
	{
		$this->layout='';
		$id=convert_uudecode(base64_decode($id));
		$tableName=base64_decode($tableName);
		$renderPath=base64_decode($renderPath);
		$renderElement=base64_decode($renderElement);
		//pr($tableName);die;
		if($this->RequestHandler->isAjax())
		{		
			$this->layout=false;
			$this->autoRender = false;	
						
			$getRec = $this->$tableName->find('first',array('conditions'=>array($tableName.'.id'=>$id)));
			//pr($getRec); die;
			$field=array();
			
			if($tableName=='FreeVideo')
			{
				$originalFile = realpath('../../app/webroot/files/admin_free_video').'/'.$getRec[$tableName]['own_video_url'];
				$smallThumb = realpath('../../app/webroot/files/admin_free_video').'/'.$getRec[$tableName]['own_video_url']."_smallThumb.png";
				$thumb = realpath('../../app/webroot/files/admin_free_video').'/'.$getRec[$tableName]['own_video_url']."_thumb.png";
			
				if(file_exists($originalFile) && $getRec[$tableName]['own_video_url']!="")
				{
					unlink('files/admin_free_video/'.$getRec[$tableName]['own_video_url']);
					unlink('files/admin_free_video/'.$getRec[$tableName]['own_video_url'].'_smallThumb.png');
					unlink('files/admin_free_video/'.$getRec[$tableName]['own_video_url'].'_thumb.png');
					unlink('files/admin_free_video/'.$getRec[$tableName]['own_video_url'].'_preview.mp4');
					
				}
			}
			
			
			if(!empty($getRec))
			{
					//$record[$tableName]['id']=$id;
					$this->$tableName->delete(array($tableName.'.id'=>$id));
					//$this->Session->write('success','Record has been deleted successfully');
					
					if($tableName == 'News'){
						$originalFile = realpath('../../app/webroot/img/news/original/') .'/'.$getRec[$tableName]['image'];
						$thumbFile = realpath('../../app/webroot/img/news/thumbnails/') .'/'.$getRec[$tableName]['image'];
						if(file_exists($originalFile) && $getRec[$tableName]['image']!=""){
						  unlink('img/news/original/'.$getRec[$tableName]['image']);						  
						}
						if(file_exists($thumbFile) && $getRec[$tableName]['image']!=""){						
						  unlink('img/news/thumbnails/'.$getRec[$tableName]['image']);
						}
					}
			}		   	
			$this->paginate=array('limit' => 10,'page'=>$page,'order'=>$tableName.'.id DESC');
			$info = $this->paginate($tableName);
			
			if(empty($info) && $page>1)
			{
				$page=$page-1;
				$this->paginate=array('limit' => 10,'page'=>$page,'order'=>$tableName.'.id DESC');
				$info = $this->paginate($tableName);
			}
			if($tableName=="Admin")
			{
				$condition=array('Admin.id >'=>'3');
				$this->paginate=array('limit' => 10,'page'=>$page,'conditions'=>$condition,'order'=>$tableName.'.id DESC');
				$info = $this->paginate($tableName);
			}
			if($tableName=="RemarkReport")
			{
				
				$this->paginate=array('limit'=>'10','order'=>'RemarkReport.id ASC','contain'=>array('CourseRemark'=>array('Course')));
				$info=$this->paginate('RemarkReport');
			}
			$this->set(compact('info','page'));			
			$this->viewPath = 'Elements'.DS.'adminElements/'.$renderPath;
			$this->render($renderElement);			
		}
	}
	
	function admin_delete_member($page=NULL,$id=NULL,$memberType=NULL,$renderPath=NULL,$renderElement=NULL)
	{
		$this->layout='';
		$id=convert_uudecode(base64_decode($id));
		$renderPath=base64_decode($renderPath);
		$renderElement=base64_decode($renderElement);
	
		if($this->RequestHandler->isAjax())
		{		
			$this->layout=false;
			$this->autoRender = false;

			$this->loadModel('Member');
			
			$this->Member->delete($id);
			
			$conditions = array();
			
			if($memberType == 'Member')
			{
				$condition=array('Member.role_id !='=>'3');
			}
			else if($memberType == 'Author')
			{
				$condition=array('Member.role_id'=>'3');
			}				
			
			$this->paginate=array('limit' => 10,'page'=>$page,'order'=>'Member.id DESC');
			$info = $this->paginate('Member',$condition);
			
			if(empty($info) && $page>1)
			{
				$page=$page-1;
				$this->paginate=array('limit' => 10,'page'=>$page,'order'=>'Member.id DESC');
				$info = $this->paginate('Member',$condition);
			}
			
			$this->set(compact('info','page'));			
			$this->viewPath = 'Elements'.DS.'adminElements/'.$renderPath;
			$this->render($renderElement);			
		}
	}
	
	//---- function for edit cms page---//
	
	function validate_edit_news_ajax()
	{
		$this->layout="";
		$this->autoRender=false;	
		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_news($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['News'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	

	 function validate_edit_news($data)
	 {	
		$locale=$data['News']['locale'];
		$errors = array ();	
		
		/*if (trim($data['News']['description_'.$locale]) == ""){
			$errors['description_'.$locale][] = FIELD_REQUIRED."\n";
		}*/
		if (empty($data['News']['title_'.$locale])){
			$errors['title_'.$locale][] = FIELD_REQUIRED."\n";
		}	
		if (empty($data['News']['sub_title_'.$locale])){
			$errors['sub_title_'.$locale][] = FIELD_REQUIRED."\n";
		}	
		if($data['News']['news_image']['name']!='')
		{ 
			$ext = pathinfo($data['News']['news_image']['name']);
			$ext = $ext['extension'];
			if ($ext!='jpg' && $ext!='png' && $ext!='gif' && $ext!='jpeg'){
				$errors['image'][] = "Invalid file type "."\n";
			}
		}
		return $errors;			
	}
	
//**********  add new validation **********  
	function admin_edit_news($id=NULL)
	{
		$this->layout='admin';		
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->News->find('first',array('conditions'=>array('News.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}	
               
		if(!empty($this->data))
		{ 
		 	$error = $this->validate_edit_news($this->data);
			
			if(count($error) == 0)
			{
                           //  pr($this->data);
                          //  echo ($this->data['News']['news_image']["name"]!= "");
             //die("heelo");
				$info = $this->News->find('first',array('conditions'=>array('News.id'=>$this->data['News']['id']),'contain'=>array()));				
				$oldimg = $info['News']['image'];
				
				if($this->data['News']['news_image']['name'] != "")
				{
					
				 	$oldimg = $info['News']['image'];
		         	$expath=explode('.',$this->data['News']['news_image']['name']);
				 	$extsarr=end($expath);
				 	$target=time().'-'.$this->data['News']['news_image']['name'];
                                        $destination1 = $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/img/news/original/";
					//$destination = realpath('../../app/webroot/img/news/original/').'/';
                                          $destination2= $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/img/news/thumbnails/";
                                        
                                        
				 	//$destination1 = realpath('../../app/webroot/img/news/original/').'/';
				 	//$destination2 = realpath('../../app/webroot/img/news/thumbnails/').'/';
				 	$file = $this->data['News']['news_image'];
					$result1 = $this->Upload->upload($file, $destination2, $target, array('type' => 'resizecrop','size'=>array('467','262'),'quality'=>'100'),array('jpg','jpeg','gif','png'));
			   	//	$result = $this->Upload->upload($file, $destination, $target,array('type' => 'resizecrop','size'=>array('530','270'),'quality'=>'100') ,array('jpg','jpeg','gif','png'));
					$result = $this->Upload->upload($file, $destination1, $target,array('type' => 'resizecrop','size'=>array('960','540'),'quality'=>'100') ,array('jpg','jpeg','gif','png'));
					
					App::import('Component', 'Aws');
                               $this->Aws = new AwsComponent();
                                if(!empty($oldimg)){
                                  //   deleteing the old image form aws server

                                        $keys=array($oldimg,'thumbnails/'.$oldimg);
                                       $this->Aws->deleteFiles(NEWS_BUCKET, $keys);
                               }
                             $key_thumb='thumbnails/'.$target; 
                               $key='original/'.$target;
                              $result_aws = $this->Aws->uploadFile($key, NEWS_BUCKET, $destination1.$target);
                               $result_aws = $this->Aws->uploadFile($key_thumb, NEWS_BUCKET,$destination2.$target);
                                    @unlink($destination1.$target);
                                      @unlink($destination2.$target);
                                       

														
                                                     
					
					$this->request->data['News']['image']= $target;
					if($result)
					{	
						if($oldimg != ''){
							if(file_exists('img/news/original/'.$oldimg)){
								unlink('img/news/original/'.$oldimg);
							}
							if(file_exists('img/news/thumbnails/'.$oldimg)){
								unlink('img/news/thumbnails/'.$oldimg);
							}
						}
					}
				}	
				else{
                                    if(isset($this->request->data['News']['remove_picture']) && $this->request->data['News']['remove_picture']==1)                                     {
                                        //echo "remvoe me";
                                   // $destination1 = realpath('../../app/webroot/img/news/original/').'/';
                                   // $destination2 = realpath('../../app/webroot/img/news/thumbnails/').'/';
                                    $destination1 = $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/img/news/original/";
					//$destination = realpath('../../app/webroot/img/news/original/').'/';
                                          $destination2= $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/img/news/thumbnails/";
					//$destination2 = realpath('../../app/webroot/img/news/thumbnails/').'/';
                                          App::import('Component', 'Aws');
                                          $this->Aws = new AwsComponent();
                                          $keys=array($oldimg,'thumbnails/'.$oldimg);
                                       $this->Aws->deleteFiles(NEWS_BUCKET, $keys);
                                    $this->request->data['News']['image']='';
                                  }
				 else{
				$this->request->data['News']['image']= $this->data['News']['old_image'];
                                 }
				}
				$this->request->data['News']['date_modified'] = strtotime(date('Y-m-d H:i:s'));
			//pr($this->request->data);
                               // die;
				$this->News->save($this->data); 
				$this->Session->write('success','Information has been updated successfully');
				$this->redirect(array('action'=>'news','admin' => true));
			}
			else
			{
				$info = $this->News->find('first',array('conditions'=>array('News.id'=>$this->data['News']['id']),'contain'=>array()));
				$contents = $this->data['News']['description'];
				$this->set(compact('error','contents','info'));
			}										
		}
	}
//---- End of function for edit cms template---//
      function admin_view_news($id=NULL)
	  {
		  $this->layout='admin';
		  $pageid=convert_uudecode(base64_decode($id));
		  $info = $this->News->find('first',array('conditions'=>array('News.id'=>$pageid),'contain'=>array()));
		  $this->set('info',$info);
	  }
	  
	function admin_add_news($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('Admin');
		$admin_id=$this->Session->read('LocTrainAdmin.id');
		//$member=$this->Admin->find('first',array('conditions'=>array('Admin.id'=>$admin_id),'fields'=>('member_id','username')));
		//pr($member); die;
		//if(!empty($member['Admin']['member_id']))
		//{
			
			//$this->loadModel('Member');
			//$member=$this->Member->find('first',array('conditions'=>array('Member.id'=>$member['Admin']['member_id']),'fields'=>('given_name')));
			$news_author = $admin_id;
		//}
		//else
		//{
		//	$news_author = 'Administrator';
		//}
		
		if(!empty($this->data)){
		$error = $this->validate_news($this->data);
               //pr ( $_FILES['image12'] );
                       
			if(count($error) == 0){
		//	pr($this->data);
                        //echo ($this->data['News']['image'] != "");
              //die;
			//pr($this->data);
                      //  die;
		  		if($this->data['News']['image']["name"] != ""){
                                  
			
					$expath=explode('.',$this->data['News']['image']['name']);
					$extsarr=end($expath);
					$target=time().'-'.$this->data['News']['image']['name'];
                                        $destination = $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/img/news/original/";
					//$destination = realpath('../../app/webroot/img/news/original/').'/';
                                          $destination2= $_SERVER['DOCUMENT_ROOT'] . "/kuulum/app/webroot/img/news/thumbnails/";
					//$destination2 = realpath('../../app/webroot/img/news/thumbnails/').'/';
					$file = $this->data['News']['image'];
						
					$result1 = $this->Upload->upload($file, $destination2, $target, array('type' => 'resizecrop','size'=>array('467','262'),'quality'=>'100'),array('jpg','jpeg','gif','png'));
					//$result = $this->Upload->upload($file, $destination, $target, array('type' => 'resizecrop','size'=>array('530','270'),'quality'=>'100'),array('jpg','jpeg','gif','png'));
					$result = $this->Upload->upload($file, $destination, $target, array('type' => 'resizecrop','size'=>array('960','540'),'quality'=>'100'),array('jpg','jpeg','gif','png'));	
				
				App::import('Component', 'Aws');
                    $this->Aws = new AwsComponent();


                  $key_thumb='thumbnails/'.$target; 
                   $key_original='original/'.$target;
                   $result_aws = $this->Aws->uploadFile($key_original, NEWS_BUCKET, $destination.$target);
                    $result_aws = $this->Aws->uploadFile($key_thumb, NEWS_BUCKET,$destination2.$target);
                   @unlink($destination.$target);
                   @unlink($destination2.$target);
                    $this->request->data['News']['image'] = $target;	
                                }
                                else{
                                     $this->request->data['News']['image']="";
                                }
				
                      
				
					//$this->request->data['News']['image'] = $target;
					$this->request->data['News']['status']=1;
					$this->request->data['News']['admin_id']= $news_author;
					$this->request->data['News']['date_modified'] = strtotime(date('Y-m-d H:i:s'));		
					$this->request->data['News']['date_added'] = strtotime(date('Y-m-d H:i:s'));		
				
				
				$this->News->save($this->data); 
				$this->Session->write('success','News has been added successfully');
				$this->redirect(array('action'=>'news','admin' => true));
			}                
			else
			{		
				//$contents = $this->request->data['News']['description'];
				$this->set(compact('error'));		
			}										
                }
	}
	

	//**********  add new validation **********
	
	function validate_add_news_ajax()
	{
		$this->layout="";
		$this->autoRender=false;	
		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_news($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['News'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	

	 function validate_news($data)
	 {	
		$locale=$data['News']['locale'];
		$errors = array ();	
		
		/*if (trim($data['News']['description_'.$locale]) == ""){
			$errors['description_'.$locale][] = FIELD_REQUIRED."\n";
		}*/
		if (empty($data['News']['title_'.$locale])){
			$errors['title_'.$locale][] = FIELD_REQUIRED."\n";
		}	
		if (empty($data['News']['sub_title_'.$locale])){
			$errors['sub_title_'.$locale][] = FIELD_REQUIRED."\n";
		}	
		if($data['News']['image']['name']!= '')
		{
			$ext = pathinfo($data['News']['image']['name']);
			$ext = $ext['extension'];

			$ext = pathinfo($data['News']['image']['name']);
			$ext = $ext['extension'];
			if ($ext!='jpg' && $ext!='png' && $ext!='gif' && $ext!='jpeg'){
				$errors['image'][] = "Invalid file type "."\n";
			}
		}
		return $errors;			
	}
	
//**********  add new validation **********  	

	function admin_assign_role($id=NULL)
	{
		$this->layout = 'admin';
		$id=convert_uudecode(base64_decode($id));	
		$page = isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		
		$info = $this->Member->find('first',array('conditions'=>array('Member.id'=>$id),'contain'=>array('Admin')));
		$this->set(compact('info'));		
		//echo "<pre>"; print_r($info); die;
		
		if(!empty($this->data))
		{
			$error = $this->validate_assign_role($this->data);
			
			if(!empty($error))
			{
				$this->set('error',$error);
				
			} else {
				
				if($this->data['Admin']['role_id']==9)
				{
					$this->Admin->delete(array('Admin.id'=>$this->data['Admin']['id']));
				}
				else
				{
					if(!empty($this->data['Admin']['password']))
					{
						$palainPassword=$this->data['Admin']['password'];
						$this->request->data['Admin']['password'] = md5($this->data['Admin']['password']);
					} else {
						unset($this->request->data['Admin']['password']);
					}

					//echo "<pre>"; print_r($this->data); die;		
				
					$this->Admin->save($this->data); 
					
					if(empty($this->data['Admin']['id']))
					{					
						$email=$this->data['Admin']['email'];
						$role=$this->data['Admin']['role'];
						$username=$this->data['Admin']['username'];
						$password=$palainPassword;						
						$final_name = $this->data['Admin']['final_name'];
						$m_locale =  $this->data['Admin']['locale'];
						$this->loadModel('EmailTemplate');									
						$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.main_id'=>8,'EmailTemplate.locale'=>$m_locale),'fields'=>array('subject','description','email_from')));
						$emailData = $emailTemplate['EmailTemplate']['description'];						
						$emailData = str_replace(array('{username}','{password}','{role}','{final_name}'),array($username,$password,$role,$final_name),$emailData);
						$emailTo =$email;
						$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
						$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
						
						//echo "<pre>"; print_r($emailData); die;		
						
						$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
					}					
				}
				$this->Session->write('success','Role assigned successfully');
				$this->redirect(array('action'=>'user_roles','admin' => true));		
			}
		}
	}
	
	function validate_assign_role_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_assign_role($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Admin'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_assign_role($data)
	{			
		$errors = array ();		
		
		if (trim($data['Admin']['role_id']) == ""){
			$errors['role_id'][] = FIELD_REQUIRED."\n";
		}
		if (trim($data['Admin']['email']) == ""){
			$errors['email'][] = FIELD_REQUIRED."\n";
		}
		elseif($this->validEmail(trim($data['Admin']['email'])) =='false'){
			$errors['email'][] = INVALID_EMAIL."\n";
		}
		if (empty($data['Admin']['username'])){
			$errors['username'][] = FIELD_REQUIRED."\n";
		}
		
		if (!empty($data['Admin']['password']) && strlen(trim($data['Admin']['password'])) <8){
				$errors['password'][] = LENGTH_VALIDATION."\n";
		}
		
		if(empty($data['Admin']['id']) || trim($data['Admin']['id']) == '')
		{
			if (!empty($data['Admin']['email']))
			{
				$this->loadModel('Admin');
				$chkuser = $this->Admin->find('first',array('conditions'=>array('Admin.email'=>trim($data['Admin']['email']))));
				
				
				if(count($chkuser) > 0)
				{
					$errors['email'][] = $user." role already assigned to this user"."\n";
				}
			}
			if (empty($data['Admin']['password'])){
				$errors['password'][] = FIELD_REQUIRED."\n";
			}				
		}
		
		return $errors;			
	}
	
	function admin_new_role()
	{
		$this->layout = 'admin';
	}
	
	function admin_user_roles()
	{
		$this->layout='admin';
		$this->loadModel('Admin');		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$members = $this->Admin->find('all');
		$this->paginate=array('conditions'=>array('Admin.id >'=>'3'),'limit'=>'10','order'=>'Admin.id DESC');
		$info=$this->paginate('Admin');
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('subadmin_list');
		}
	}

	function admin_edit_role($id=NULL)
	{
		$this->layout = 'admin';
		$id=convert_uudecode(base64_decode($id));	
		$page = isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('Admin');		
		$info = $this->Admin->find('first',array('conditions'=>array('Admin.id'=>$id),'contain'=>array()));
		$this->set(compact('info'));		
		if(!empty($this->data))
		{
			if($this->data['Admin']['role_id']==9)
			{
				$this->Admin->delete(array('Admin.id'=>$this->data['Admin']['id']));
			}
			else
			{
				if(!empty($this->data['Admin']['password']))
				{
					$palainPassword=$this->data['Admin']['password'];
					$this->request->data['Admin']['password'] = md5($this->data['Admin']['password']);
				}
				else
				{
					$info = $this->Admin->find('first',array('conditions'=>array('Admin.id'=>$this->data['Admin']['id']),'contain'=>array()));
					$this->request->data['Admin']['password'] = $info['Admin']['password'];
				}
				$this->Admin->save($this->data); 
			}
			

			//$email=$this->data['Admin']['email'];
			//$role=$this->data['Admin']['role'];
			//$username=$this->data['Admin']['username'];
			//$password=$assign_role;						
			//$this->loadModel('EmailTemplate');									
			//$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('id'=>8),'fields'=>array('subject','description','email_from')));						
			//$emailData = $emailTemplate['EmailTemplate']['description'];						
			//$emailData = str_replace(array('{username}','{password}','{role}'),array($username,$password,$role),$emailData);
			//$emailTo =$email;
			//$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			//$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			//$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
			$this->Session->write('success','Information has been updated successfully');
			$this->redirect(array('action'=>'user_roles','admin' => true));
		}
	}
	
	function validate_edit_role_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_role($this->data);						
			if(is_array ( $this->data ) )	{
				foreach ($this->data['Admin'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_edit_role($data)
	{			
		$errors = array ();
		if($data['Admin']['role_id']!=9)
		{		
			if (trim($data['Admin']['email']) == ""){
				$errors['email'][] = FIELD_REQUIRED."\n";
			}
			elseif($this->validEmail(trim($data['Admin']['email'])) =='false'){
				$errors['email'][] = INVALID_EMAIL."\n";
			}
			if (empty($data['Admin']['username'])){
				$errors['username'][] = FIELD_REQUIRED."\n";
			}
			if (!empty($data['Admin']['email']))
			{
				$this->loadModel('Admin');
				$chkuser = $this->Admin->find('count',array('conditions'=>array('Admin.email'=>trim($data['Admin']['email']),'Admin.id <>'=>$data['Admin']['id'])));
				if($chkuser > 0)
				{
					$errors['email'][] = EMAIL_EXISTS."\n";
				}
			}	
			if (!empty($data['Admin']['password']) && strlen(trim($data['Admin']['password'])) <8){
				$errors['password'][] = LENGTH_VALIDATION."\n";
			}
		}
		return $errors;			
	}
	
	function admin_lessons($id=NULL)
	{   $course_id=$id;
		$this->layout='admin';		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$id=convert_uudecode(base64_decode($id));		
		$this->loadModel('CourseLesson');
		$condition = array('CourseLesson.course_id'=>$id);
		$this->paginate=array('conditions'=>$condition,'order'=>'CourseLesson.id DESC');
		$info=$this->paginate('CourseLesson');		
		$this->set(compact('page','info','course_id'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('lesson_list');
		}
	}
	
	function  admin_view_lesson($id)
   {
        $this->layout='admin';
		if(!empty($id))
		{
			$this->loadModel('CourseLesson');
			$id=convert_uudecode(base64_decode($id));
			$info=$this->CourseLesson->findById($id);
			$this->set(compact('info'));
		}
   }
   
    function admin_edit_lesson($id=NULL)
   {
		$this->layout='admin';
		$id=convert_uudecode(base64_decode($id));
		if(!empty($id))
		{
		     $this->loadModel('CourseLesson');
			$lessonName=$this->CourseLesson->findById($id);			
			$this->set(compact('lessonName'));		
		}
		if(!empty($this->data))
		{
		//pr($this->data);die;	
			$error=$this->validate_edit_lesson($this->data);	
			if(count($error)==0)
			{
				$this->loadModel('CourseLesson');
				if($this->data['CourseLesson']['video']['name']!="")
				{
				   	$ext=explode('.',$this->data['CourseLesson']['video']['name']);
					if($ext[1]=='flv'|| $ext[1]=='mp4')
					{
						 $videodestination = realpath('../../app/webroot/files/members/'.$this->data['CourseLesson']['folderid'].'/').'/';
						 $videoname = time().'-'.$this->data['CourseLesson']['video']['name'];
						 move_uploaded_file($this->data['CourseLesson']['video']['tmp_name'],$videodestination.$videoname);
						 $this->request->data['CourseLesson']['video']=$videoname;	
					}
				}
				else
				{
					$this->request->data['CourseLesson']['video']=$this->data['CourseLesson']['old_vedio'];
				}
				$this->request->data['CourseLesson']['date_added']= strtotime(date('Y-m-d H:i:s'));
				$this->CourseLesson->save($this->data);
				$this->Session->write('success','Lesson has been updated successfully');
				$lId=base64_encode(convert_uuencode($this->data['CourseLesson']['lessonid']));
				$Fid=base64_encode(convert_uuencode($this->data['CourseLesson']['folderid']));
				$this->redirect(array('controller'=>'users','action'=>'lessons/'.$lId.'/'.$Fid,'admin'=>true));
			}else{
				$this->set('error',$error);	
			}
		}	
	}
	
	function validate_edit_lesson_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_lesson($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['CourseLesson'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_edit_lesson($data)
	{			
		$errors = array ();		
		if (trim($data['CourseLesson']['title']) == ""){
			$errors['title'][] = FIELD_REQUIRED."\n";
		}
		if (empty($data['CourseLesson']['description'])){
			$errors['description'][] = FIELD_REQUIRED."\n";
		}
		return $errors;			
	}

    function admin_add_lesson($courseId=NULL)
   {
		$this->layout='admin';
		if(!empty($courseId))
		{
		     $this->loadModel('Course');
			 $id=convert_uudecode(base64_decode($courseId));
			$mem_id=$this->Course->findById($id);
			$this->set(compact('lessonName','mem_id'));		
		}
		if(!empty($this->data))
		{
		//pr($this->data);die;	
			$error=$this->validate_add_lesson($this->data);	
			if(count($error)==0)
			{
				$this->loadModel('CourseLesson');
				if($this->data['CourseLesson']['video']['name']!="")
				{
				   	$ext=explode('.',$this->data['CourseLesson']['video']['name']);
					if($ext[1]=='flv'|| $ext[1]=='mp4')
					{
						 $videodestination = realpath('../../app/webroot/files/members/'.$this->data['CourseLesson']['mem_id'].'/').'/';
						 $videoname = time().'-'.$this->data['CourseLesson']['video']['name'];
						 move_uploaded_file($this->data['CourseLesson']['video']['tmp_name'],$videodestination.$videoname);
						 $this->request->data['CourseLesson']['video']=$videoname;	
					}
				}
				else
				{
					$this->request->data['CourseLesson']['video']=$this->data['CourseLesson']['old_vedio'];
				}
				$this->request->data['CourseLesson']['date_added']= strtotime(date('Y-m-d H:i:s'));
				$this->CourseLesson->save($this->data);
				$this->Session->write('success','Lesson has been added successfully');
				$lId=base64_encode(convert_uuencode($this->data['CourseLesson']['course_id']));
				$Fid=base64_encode(convert_uuencode($this->data['CourseLesson']['mem_id']));
				$this->redirect(array('controller'=>'users','action'=>'lessons/'.$lId.'/'.$Fid,'admin'=>true));
			}else{
				$this->set('error',$error);	
			}
		}	
	}
	
	function validate_add_lesson_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_lesson($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['CourseLesson'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_add_lesson($data)
	{			
		$errors = array ();		
		if (trim($data['CourseLesson']['title']) == ""){
			$errors['title'][] = FIELD_REQUIRED."\n";
		}
		if (empty($data['CourseLesson']['description'])){
			$errors['description'][] = FIELD_REQUIRED."\n";
		}
		return $errors;			
	}
		
	
	
	/*function admin_adminaCourseSearch($condition=null)
	{
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$condition=array();
		$cond=array();
		$qryStr="";
		if(isset($this->params['named']['condition'])){
			$condition=$this->sortCourse_condition();	
			$qryStr=$this->params['named']['condition'];
		}
		else{	
	
			if($this->data['search']['type']=="title" && $this->data['search']['title']!=''){
				$condition=array_merge($condition,array("Course.title LIKE"=> '%'.$this->data['search']['title'].'%'));
				$qryStr=$qryStr."Course.title-||".$this->data['search']['title']."||-";
			}
			if($this->data['search']['type']=="primarylanguage" && $this->data['search']['language']!=''){
				$lang = $this->data['search']['language'];
				$condition=array_merge($condition,array("Course.primary_lang"=>$lang ));
				$qryStr=$qryStr."Course.primary_lang-||". $lang."||-";
			}
			if($this->data['search']['type']=="author" && $this->data['search']['author']!=''){
				$lang = $this->data['search']['author'];
				$condition=array_merge($condition,array("Member.final_name"=>$lang ));
				$qryStr=$qryStr."Member.final_name-||".$lang."||-";
			}
			if($this->data['search']['type']=="status" && $this->data['search']['status']!=''){
				$condition=array_merge($condition,array("Course.status"=>$this->data['search']['status']));
				$qryStr=$qryStr."Course.status-||".$this->data['search']['status']."||-";
			}
			$this->loadModel('Course');
			$query = $this->Course->find('all',array('conditions'=>$condition));
			
			if(!isset($this->params['named']['condition'])){			
				$qryStr=base64_encode($qryStr);
			}	
		}
		$this->set('qryStr',$qryStr);
		if($this->RequestHandler->isAjax()) 
		{ 
			$this->layout="";
			$this->autoRender=false;
			$this->paginate=array('limit' => 2,'order'=>'Course.id DESC','contain'=>array('Member'=>array('conditions'=>$cond),'Language','CourseSubcategory','CourseView','CourseKeyword','CourseLesson','CoursePurchaseList'));
			$info = $this->paginate('Course',$condition);
			
			$this->set('info', $info);
			$this->set('page', $page);				
			$this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			$this->render('cours_list_search');
		}		
		else
		{
			echo "error";die;
		}	
	}*/
	
	
	function sortCourse_condition()
	{
		$conditions=array();
		$action = $this->params['action'];
		if(isset($this->params['named']['condition']))
		{
			$this->set('qryStr',$this->params['named']['condition']);
			$qryStr=$this->params['named']['condition'];
			$con=explode("||-",base64_decode($qryStr));
			
			if($con[count($con)-1]=='')
			{
				array_pop($con);
			}
			
			$or['OR']=array();
			foreach($con as $val):
				$qry=explode("-||",$val);
				if($qry[0]=="Course.title")
				{	
					$var=str_replace("%","",$qry[1]);
					
					$conditions=array_merge($conditions,array('Course.title'=>$var));					
				}
				if($qry[0]=="Course.primary_lang")
				{
					$var=str_replace("%","",$qry[1]);
					$conditions=array_merge($conditions,array('Course.primary_lang'=>$var));
				}

			endforeach;
		}
		return $conditions;
	}
	function admin_course_report($query=NULL)
	{
		Configure::write('debug',0);
		$this->layout='';
		$condition=array();
		$qryStr='';
		if(!isset($this->params['named']['condition'])){
			$this->request->params['named']['condition']=$query;
			$condition=$this->sortCourse_condition();
			$qryStr=$this->params['named']['condition'];
		}
		$this->loadModel('Course');
		$info = $this->Course->find('all',array('conditions'=>$condition ,'order'=>'Course.id DESC'));
		
		$this->set(compact('info'));	
	}
	
	
	function admin_update_lesson_status($page=NULL,$id=NULL,$tableName=NULL,$renderPath=NULL,$renderElement=NULL,$courseId=NULL)
	{
		$this->layout='';
		$id=convert_uudecode(base64_decode($id));
		$tableName=base64_decode($tableName);
		$renderPath=base64_decode($renderPath);
		$renderElement=base64_decode($renderElement);
		if($this->RequestHandler->isAjax()){		
			$this->layout=false;
			$this->autoRender = false;			
			$getRec = $this->$tableName->find('first',array('conditions'=>array($tableName.'.id'=>$id),'contain'=>array()));			
			$field=array();
			if(!empty($getRec)){
				if($getRec[$tableName]['status']=='0'){
					$this->$tableName->id=$id;					
					$field[$tableName]['status'] = 1;					
					$this->$tableName->save($field);
				}
				else{
					$this->$tableName->id=$id;					
					$field[$tableName]['status'] = '0';					
					$this->$tableName->save($field);					
				}
			}
			$this->paginate=array('limit' =>'10','page'=>$page,'order'=>$tableName.'.id DESC','conditions'=>array($tableName.'.course_id'=>$courseId));
			
			$info = $this->paginate($tableName);
			$this->set(compact('info','page'));			
			$this->viewPath = 'Elements'.DS.'adminElements/'.$renderPath;
			$this->render($renderElement);			
		}		
	}
	
	//---- function for Price View---//	
	function admin_price()
	{
		$this->layout='admin';
		$this->loadModel('Price');
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->paginate=array('limit'=>'10','order'=>'Price.id DESC'); 
		$info=$this->paginate('Price');
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $this->render('price_list');
		}
	}
	//---- end of  function for Listing cmspage---//

	//---- function for edit View---//
	function admin_edit_price($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('Price');		
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->Price->find('first',array('conditions'=>array('Price.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}		
		if(!empty($this->data)){ 
		    $this->request->data['Price']['description']=trim($this->data['Price']['description']);
			$this->Price->save($this->data); 
			$this->Session->write('success','Subscription Price has been updated successfully');
			$this->redirect(array('action'=>'price','admin' => true));									
		}
	}
	//---- End of function for edit cms template---//
	function validate_edit_price_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_price($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Price'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_edit_price($data)
	{			
		$errors = array ();	
		$locale = $data['Price']['locale'];	
		
		if (trim($data['Price']['name_'.$locale]) == ""){
			$errors['name_'.$locale][] = FIELD_REQUIRED."\n";
		}
	  /*if (trim($data['Price']['name_de']) == ""){
			$errors['name_de'][] = FIELD_REQUIRED."\n";
		}*/
		if (empty($data['Price']['description_'.$locale])){
			$errors['description_'.$locale][] = FIELD_REQUIRED."\n";
		}
		/*if (empty($data['Price']['description_de'])){
			$errors['description_de'][] = FIELD_REQUIRED."\n";
		}*/
		if (empty($data['Price']['no_of_months'])){
			$errors['no_of_months'][] = FIELD_REQUIRED."\n";
		}
		if (empty($data['Price']['initial_value'])){
			$errors['initial_value'][] = FIELD_REQUIRED."\n";
		}
		if(!empty($data['Price']['initial_value']))
		{ 
		  if(!is_numeric($data['Price']['initial_value']))
		 {
			$errors ['initial_value'] [] = "Please Enter Numeric Value"."\n";
		 }
		} 
		return $errors;			
	}
	
	//---- function for Price View---//	
	function admin_discounts()
	{
		$this->layout='admin';
		$this->loadModel('Discount');
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->paginate=array('group'=>array('discount_code'),'order'=>'Discount.id DESC'); 
		$info=$this->paginate('Discount');
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $this->render('discounts_list');
		}
	}
	//---- end of  function for Listing cmspage---//
	
	//---- function for Price View---//	
	function admin_discount()
	{
		$this->layout='admin';
		$this->loadModel('Discount');
		$this->paginate=array('limit'=>'-1','order'=>'Discount.id DESC'); 
		$info=$this->paginate('Discount');
		$this->set(compact('info'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $this->render('discount_list');
		}
	}
	//---- end of  function for Listing cmspage---//
	
	
	//---- End of function for edit cms template---//
	function validate_edit_discount_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_discount($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Discount'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_discount($data)
	{			
		$errors = array ();		
		//pr($data);die;
		$locale = $data['Discount']['locale'];
		if (trim($data['Discount']['name_'.$locale]) == ""){
			$errors['name_'.$locale][] = FIELD_REQUIRED."\n";
		}
		if (empty($data['Discount']['description_'.$locale])){
			$errors['description_'.$locale][] = FIELD_REQUIRED."\n";
		}
                if(isset($data['Discount']['unlimited_time']) && empty($data['Discount']['unlimited_time']))
                {             
                if (empty($data['Discount']['date_from'])){
			$errors['date_from'][] = FIELD_REQUIRED."\n";
		} 
                if (empty($data['Discount']['date_to'])){
			$errors['date_to'][] = FIELD_REQUIRED."\n";
		} 
                }                
                if (empty($data['Discount']['author_name'])){
			$errors['author_name'][] = FIELD_REQUIRED."\n";
		} 
                if (empty($data['Discount']['particular-course'])){
			$errors['particular-course'][] = FIELD_REQUIRED."\n";
		} 
                if (empty($data['Discount']['members'])){
			$errors['members'][] = FIELD_REQUIRED."\n";
		} 
                 
		if (empty($data['Discount']['discount_code'])){
			$errors['discount_code'][] = FIELD_REQUIRED."\n";
		}
		else if ( $this->checkOtherDiscountCodeExists($data['Discount']['discount_code']))
			{
				$errors['discount_code'][] = "This code already exists"."\n";
			}
		if (empty($data['Discount']['initial_value'])){
			$errors['initial_value'][] = FIELD_REQUIRED."\n";
		}
		if(!empty($data['Discount']['initial_value']))
		{ 
			if(!is_numeric($data['Discount']['initial_value']))
			{
			$errors ['initial_value'] [] = "Please Enter Numeric Value"."\n";
			}
			else
			{
				$exp1 = '/^[0-9]{1,2}([.][0-9]+)?+$/';
				if($data['Discount']['initial_value']!=100)
				{
					if(!(preg_match($exp1,$data['Discount']['initial_value'])))
					{
						$errors ['initial_value'] [] = "A Valid Percentage Value is Required."."\n";
						
					}
				}
			}
		}	
		return $errors;				
	}
	function validate_edit_discount($data)
	{			
		$errors = array ();		
		//pr($data);die;
		$locale = $data['Discount']['locale'];
		if (trim($data['Discount']['name_'.$locale]) == ""){
			$errors['name_'.$locale][] = FIELD_REQUIRED."\n";
		}
		if (empty($data['Discount']['description_'.$locale])){
			$errors['description_'.$locale][] = FIELD_REQUIRED."\n";
		}
                if(isset($data['Discount']['unlimited_time']) && empty($data['Discount']['unlimited_time']))
                {             
                if (empty($data['Discount']['date_from'])){
			$errors['date_from'][] = FIELD_REQUIRED."\n";
		} 
                if (empty($data['Discount']['date_to'])){
			$errors['date_to'][] = FIELD_REQUIRED."\n";
		} 
                }                
                if (empty($data['Discount']['author_name'])){
			$errors['author_name'][] = FIELD_REQUIRED."\n";
		} 
                if (empty($data['Discount']['particular-course'])){
			$errors['particular-course'][] = FIELD_REQUIRED."\n";
		} 
                if (empty($data['Discount']['members'])){
			$errors['members'][] = FIELD_REQUIRED."\n";
		} 
                 
		if (empty($data['Discount']['discount_code'])){
			$errors['discount_code'][] = FIELD_REQUIRED."\n";
		}
		else if ( $this->checkOtherEditDiscountCodeExists($data['Discount']['discount_code_old'],$data['Discount']['discount_code']))
			{
				$errors['discount_code'][] = "This code already exists"."\n";
			}
		if (empty($data['Discount']['initial_value'])){
			$errors['initial_value'][] = FIELD_REQUIRED."\n";
		}
		if(!empty($data['Discount']['initial_value']))
		{ 
			if(!is_numeric($data['Discount']['initial_value']))
			{
			$errors ['initial_value'] [] = "Please Enter Numeric Value"."\n";
			}
			else
			{
				$exp1 = '/^[0-9]{1,2}([.][0-9]+)?+$/';
				if($data['Discount']['initial_value']!=100)
				{
					if(!(preg_match($exp1,$data['Discount']['initial_value'])))
					{
						$errors ['initial_value'] [] = "A Valid Percentage Value is Required."."\n";
						
					}
				}
			}
		}	
		return $errors;			
	}
        
	
	 /*
         This function is used to check discount code exists in case of adding  
         */
	function checkOtherDiscountCodeExists($discount_code)
	{
		$this->loadModel('Discount');
		$result=$this->Discount->find('first',array('conditions'=>array('Discount.discount_code'=>$discount_code)));
		if(!empty($result))
		{
			return true;
		}
		else
		{
			return false;
		}
	
	}
        
        /*
         This function is used to check discount code exists in case of editing  
         */
        function checkOtherEditDiscountCodeExists($discount_old_code,$discount_code)
	{
		$this->loadModel('Discount');
                $discount_old_code=convert_uudecode(base64_decode($discount_old_code));
                if(!($discount_old_code==$discount_code))
                {
                $result=$this->Discount->find('first',array('conditions'=>array('Discount.discount_code'=>$discount_code)));
		if(!empty($result))
		{
                    return true;
		}
		else
		{
                    return false;
		}
                }
                else
                {
                    return false;
                }
		
	
	}

//---- function for Price View---//	
	function admin_interest()
	{
		$this->layout='admin';
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('Interest');
		$this->paginate=array('limit'=>'-1','order'=>'Interest.id DESC'); 
		$info=$this->paginate('Interest');
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/manage';
			 $this->render('interest_list');
		}
	}
	
	function admin_edit_interest($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('Interest');		
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->Interest->find('first',array('conditions'=>array('Interest.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}		
		if(!empty($this->data)){ 
			$this->request->data['Interest']['date_added']=strtotime(date('Y-m-d H:i:s'));
			$this->Interest->save($this->data); 
			$this->Session->write('success','Information has been updated successfully');
			$this->redirect(array('action'=>'interest','admin' => true));									
		}
	}
	
	function validate_edit_interest_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_interest($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Interest'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_interest($data)
	{
	
		$errors = array ();		
		//$locale = $data['Interest']['locale'];
		if (trim($data['Interest']['interest']) == ""){
			$errors['interest'][] = FIELD_REQUIRED."\n";
		}
		
		return $errors;			
	}
	
	function admin_add_interest()
	{
		$this->layout='admin';
		$this->loadModel('Interest');
		$error=array();
		if(!empty($this->data))
		{
			$error=$this->validate_interest($this->data);
			if(count($error)==0){
                            
				$this->request->data['Interest']['date_added']=strtotime(date('Y-m-d H:i:s'));
				$this->Interest->save($this->data);
				}	
				$this->Session->write('success','Keyword has been added successfully');
				$this->redirect(array('controller'=>'users','action'=>'interest','admin'=>true));
		}
		else{
				$this->set('error',$error);
		}
	}
		
	function admin_payments()
	{
		$this->layout='admin';
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('CoursePurchase');
		//echo"<pre>";print_r($this->CoursePurchase->find('all'));
		$this->paginate=array('limit'=>'-1','order'=>'CoursePurchase.id DESC'); 
		//echo"<pre>";print_r($this->paginate);die;
		$info=$this->paginate('CoursePurchase');
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/manage';
			 $this->render('payment_list');
		}
	}
	
	function admin_view_course_purchesed($id=NULL)
	{
	 $this->layout='admin';
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('CoursePurchaseList');
		$id=convert_uudecode(base64_decode($id));
		$condition=array('CoursePurchaseList.purchase_id'=>$id);
		$this->paginate=array('limit'=>'-1','conditions'=>$condition,'order'=>'CoursePurchaseList.id DESC','recursive'=>2); 
		$info=$this->paginate('CoursePurchaseList');
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/manage';
			 $this->render('purches_course_list');
		}
	
	}
	
	function admin_cancelled_users()
	{
		$this->layout='admin';
		$this->loadModel('CancelReason');		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';

		$this->paginate=array('limit'=>'10','conditions'=>array('Member.status'=>2),'order'=>'CancelReason.id DESC','contain'=>array('Member','UserReason'=>array('Reason')));
		$info=$this->paginate('CancelReason');	
		$this->set(compact('info','page'));
		
		//echo "<pre>"; print_r($info); die;
		
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('cancel_list');
		}
	}
	function admin_featured_courses()
	{
		$this->layout='admin';
		$this->loadModel('FeatureCourse');
		//echo"<pre>";print_r($this->CoursePurchase->find('all'));
		//$this->paginate=array('limit'=>'10','order'=>'CoursePurchase.id DESC'); 
		//echo"<pre>";print_r($this->paginate);die;
		//$info=$this->paginate('CoursePurchase');
		$feature=$this->FeatureCourse->find('first');
		$this->set(compact('feature'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/manage';
			 $this->render('payment_list');
		}
	}
	function admin_edit_featured_course($id=NULL)
	{
		
		$this->layout='admin';
		$this->loadModel('FeatureCourse');
                $feature=$this->FeatureCourse->find('first');
                $this->set(compact('feature'));
		if(!empty($id))
		{	
			$feature = $this->FeatureCourse->find('first');
			$this->set('feature',$feature);
		}		
		if(!empty($this->data))
		{ 
		 	$error = $this->validate_featured($this->data);
			if(count($error) == 0)
			{
				//pr($this->data); die;
				$this->FeatureCourse->id=convert_uudecode(base64_decode($this->data['FeatureCourse']['id']));
				if($this->FeatureCourse->saveField('featured_by',$this->data['FeatureCourse']['featured_by']))
				{
					$this->Session->write('success','Feature by criteria updated successfully');
					//$this->redirect(array('controller'=>'Users','action'=>'admin_featured_courses'));
				}
			}
			else
			{
				$this->set('errors',$error);
			}
												
		}
	}
	
	
	function validate_edit_featured_course_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_featured($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['FeatureCourse'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_featured($data)
	{
	
		$errors = array ();		
		if (trim($data['FeatureCourse']['featured_by']) == ""){
			$errors['FeatureCourse'][] = FIELD_REQUIRED."\n";
		}
		return $errors;			
	}
	
	function admin_course_categories()
	{
		$this->layout='admin';
		$this->loadModel('CourseCategory');
		//echo"<pre>";print_r($this->CoursePurchase->find('all'));
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->paginate=array('order'=>'CourseCategory.id DESC'); 
		//echo"<pre>";print_r($this->paginate);die;
		$info=$this->paginate('CourseCategory');
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/manage';
			 $this->render('category_list');
		}
	}
	
	function admin_add_category()
	{
		$this->layout='admin';
		$this->loadModel('CourseCategory');
		
		$this->all_language();
		
		if(!empty($this->data))
		{ 
		
			$error=$this->validate_add_category($this->data);
			if(count($error)==0){
			$this->request->data['CourseCategory']['date_added']=strtotime(date('Y-m-d H:i:s'));
			$this->request->data['CourseCategory']['status']=1;
			if($this->CourseCategory->save($this->data))
			{
				$this->Session->write('success','Category has been added successfully');
				$this->redirect(array('controller'=>'users','action'=>'course_categories','admin'=>true));
			
			}
			}	
		
		else{
				$this->set('error',$error);
			}
		}
	}
		

	function validate_add_category_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_add_category($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['CourseCategory'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_add_category($data)
	{		
		$errors = array ();		
		
		$locale = $data['CourseCategory']['locale'];
		if (trim($data['CourseCategory'][$locale]) == ""){
			$errors[$locale][] = FIELD_REQUIRED."\n";
		}
		if (trim($data['CourseCategory'][$locale]) != ""){
			$this->loadModel('CourseCategory');
			$check_exist=$this->CourseCategory->find('count',array('conditions'=>array('CourseCategory.'.$locale=>$data['CourseCategory'][$locale])));
			if($check_exist>0)
			{
				$errors[$locale][] = CATEGORY_EXIST."\n";
			}
		}
		return $errors;			
	}
	
	function admin_edit_category($id=NULL)
	{ 
		$this->layout='admin';
		$this->all_language();
		$this->loadModel('CourseCategory');
		$category_id = convert_uudecode(base64_decode($id));
		$info=$this->CourseCategory->find('first',array('conditions'=>array('CourseCategory.id'=>$category_id)));
		$this->set(compact('info'));
		if(!empty($this->data))
		{
			$error=$this->validate_edit_category($this->data);
			if(count($error)==0){
				//pr($this->data); die;
				//$this->request->data['CourseCategory']['date_added']=strtotime(date('Y-m-d H:i:s'));
				$data = $this->CourseCategory->create();
				$data['CourseCategory']['id']=convert_uudecode(base64_decode($this->data['CourseCategory']['id']));
				$data['CourseCategory']['en']=$this->data['CourseCategory']['en'];
				$data['CourseCategory']['de']=$this->data['CourseCategory']['de'];
				$data['CourseCategory']['ar']=$this->data['CourseCategory']['ar'];
				$data['CourseCategory']['fr']=$this->data['CourseCategory']['fr'];
				$data['CourseCategory']['it']=$this->data['CourseCategory']['it'];
				$data['CourseCategory']['ja']=$this->data['CourseCategory']['ja'];
				$data['CourseCategory']['ko']=$this->data['CourseCategory']['ko'];
				$data['CourseCategory']['pt']=$this->data['CourseCategory']['pt'];
				$data['CourseCategory']['ru']=$this->data['CourseCategory']['ru'];
				$data['CourseCategory']['zh']=$this->data['CourseCategory']['zh'];
				$data['CourseCategory']['es']=$this->data['CourseCategory']['es'];
				
				$data['CourseCategory']['status']=1;
				if($this->CourseCategory->save($data))
				{
					$this->Session->write('success','Category has been updated successfully');
					$this->redirect(array('controller'=>'users','action'=>'course_categories','admin'=>true));
			
				}
			}	
		
		else{
				$this->set('error',$error);
			}
		}
	}
	
	function validate_edit_category_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_category($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['CourseCategory'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_edit_category($data)
	{			
		$errors = array ();	
        $locale = $data['CourseCategory']['locale'];		
		if (trim($data['CourseCategory'][$locale]) == ""){
			$errors[$locale][] = FIELD_REQUIRED."\n";
		}
		if (trim($data['CourseCategory'][$locale]) != ""){
			$this->loadModel('CourseCategory');
			$check_exist=$this->CourseCategory->find('count',array('conditions'=>array('CourseCategory.'.$locale=>$data['CourseCategory'][$locale],'CourseCategory.id <>'=>convert_uudecode(base64_decode($data['CourseCategory']['id'])))));
			if($check_exist>0)
			{
				$errors[$locale][] = CATEGORY_EXIST."\n";
			}
		}
		return $errors;			
	}
	
	function admin_course_subcategory($Id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('CourseSubcategory');
		$this->loadModel('CourseCategory');
		$id=convert_uudecode(base64_decode($Id));
		$category=$this->CourseCategory->find('first',array('conditions'=>array('CourseCategory.id'=>$id),'fields'=>array('en','id'),'contain'=>false));
		//pr($category); die;
		$category_id=$category['CourseCategory']['id'];
		$category=$category['CourseCategory']['en'];
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->paginate=array('limit'=>'10','conditions'=>array('CourseSubcategory.category_id'=>$id),'order'=>'CourseSubcategory.id DESC','contain'=>false);
		$info=$this->paginate('CourseSubcategory');
		$this->set(compact('info','page','category','category_id','Id'));
		//pr($category); die;
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/manage';
			 $this->render('sub_category_list');
		}
	}
	
	function admin_add_subcategory($id=NULL)
	{ 
		$this->layout='admin';
		$this->loadModel('CourseSubcategory');
		$c_id=$id; 
		
		$this->set(compact('c_id'));
		$this->all_language();
		
		if(!empty($this->data))
		{
			$error=$this->validate_add_subcategory($this->data);
			
			if(count($error)==0){
				
				//pr($this->data);
				$data=$this->CourseSubcategory->create();
				$data['CourseSubcategory']['category_id'] = convert_uudecode(base64_decode($this->data['CourseSubcategory']['category_id']));
				$data['CourseSubcategory']['en'] = $this->data['CourseSubcategory']['en'];
				$data['CourseSubcategory']['de'] = $this->data['CourseSubcategory']['de'];
				$data['CourseSubcategory']['ar'] = $this->data['CourseSubcategory']['ar'];
				$data['CourseSubcategory']['fr'] = $this->data['CourseSubcategory']['fr'];
				$data['CourseSubcategory']['it'] = $this->data['CourseSubcategory']['it'];
				$data['CourseSubcategory']['ja'] = $this->data['CourseSubcategory']['ja'];
				$data['CourseSubcategory']['ko'] = $this->data['CourseSubcategory']['ko'];
				$data['CourseSubcategory']['pt'] = $this->data['CourseSubcategory']['pt'];
				$data['CourseSubcategory']['ru'] = $this->data['CourseSubcategory']['ru'];
				$data['CourseSubcategory']['zh'] = $this->data['CourseSubcategory']['zh'];
				$data['CourseSubcategory']['es'] = $this->data['CourseSubcategory']['es'];
				$data['CourseSubcategory']['status'] = 1;
				
				$data['CourseCategory']['date_added']=strtotime(date('Y-m-d H:i:s'));
				if($this->CourseSubcategory->save($data))
				{
					$this->Session->write('success','Subcategory has been added successfully');
					$this->redirect(array('controller'=>'users','action'=>'course_subcategory/'.$this->data['CourseSubcategory']['category_id'],'admin'=>true));
				}
			}	
		
			else{
				$this->set('error',$error);
			}
		
		}
	}	
		

	function validate_add_subcategory_ajax()
	{ 
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			
			$errors_msg = null;
			$errors=$this->validate_add_subcategory($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['CourseSubcategory'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_add_subcategory($data)
	{		
		$errors = array ();	
		$locale = $this->data['CourseSubcategory']['locale'];
		if (trim($data['CourseSubcategory'][$locale]) == ""){
			$errors[$locale][] = FIELD_REQUIRED."\n";
		}
		if (trim($data['CourseSubcategory'][$locale]) != ""){
			$this->loadModel('CourseSubcategory');
			$check_exist=$this->CourseSubcategory->find('count',array('conditions'=>array('CourseSubcategory.'.$locale=>$data['CourseSubcategory'][$locale])));
			if($check_exist>0)
			{
				$errors[$locale][] = SUBCATEGORY_EXIST."\n";
			}
		}
		return $errors;			
	}
	
	function admin_edit_subcategory($id=NULL)
	{  	
		$this->layout='admin';
		$this->loadModel('CourseSubcategory');
		$this->all_language();
		if(!empty($id))
		{
			$subcategory_id = convert_uudecode(base64_decode($id));
			$info=$this->CourseSubcategory->find('first',array('conditions'=>array('CourseSubcategory.id'=>$subcategory_id),'contain'=>false));
			$this->set(compact('info'));
		} 	
		if(!empty($this->data))
		{						
			$error=$this->validate_edit_subcategory($this->data);
			if(count($error)==0){
				$data = $this->data;
				$data['CourseSubcategory']['id']=convert_uudecode(base64_decode($data['CourseSubcategory']['id']));
				$data['CourseSubcategory']['category_id']=convert_uudecode(base64_decode($data['CourseSubcategory']['category_id']));

				//pr($data); die;
				$this->CourseSubcategory->save($data);
				$this->Session->write('success','Subcategory has been updated successfully');
				$this->redirect(array('controller'=>'users','action'=>'course_subcategory/'.$this->data['CourseSubcategory']['category_id'],'admin'=>true));
	
			} else {
				
				$this->set(compact('error'));
			
			}
		}
	}
	
	function validate_edit_subcategory_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_subcategory($this->data);
			if(is_array($this->data)){
				foreach ($this->data['CourseSubcategory'] as $key => $value ){
					if(array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
                        die;
		}	
	}
	
	function validate_edit_subcategory($data)
	{ 
		$errors = array ();
               
        $locale = $data['CourseSubcategory']['locale'];		
		if (trim($data['CourseSubcategory'][$locale]) == ""){
			$errors[$locale][] = FIELD_REQUIRED."\n";
		}
		
		if (trim($data['CourseSubcategory'][$locale]) != ""){
			$this->loadModel('CourseSubcategory');
			$check_exist=$this->CourseSubcategory->find('count',array('conditions'=>array('CourseSubcategory.'.$locale=>$data['CourseSubcategory'][$locale],'CourseSubcategory.id <>'=>convert_uudecode(base64_decode($data['CourseSubcategory']['id'])))));
			if($check_exist>0)
			{
				$errors[$locale][] = SUBCATEGORY_EXIST."\n";
			}
		}
		return $errors;
	}
	
	function admin_add_discount()
	{
		$this->layout='admin';
		$this->loadModel('Discount');                                               
        $this->loadModel('Member');			
		$this->loadModel('Courses');
        $this->loadModel('Language');
        $language=$this->Language->find('all',array('conditions'=>array('Language.status'=>1),'fields' => array('Language.locale')));
		$courseslist=$this->Courses->find('all',array('conditions'=>array('Courses.isPublished'=>1,'status !='=>2)));
		
		foreach($courseslist as $course)
		{
		$pub_courseids[]=$course['Courses']['m_id'];	
		}	 
		
		$authors=$this->Member->find('all',array('conditions'=>array('Member.role_id' => '3','Member.id'=>$pub_courseids)));
		
		$published_authors=array();
		foreach($authors as $author)
		{
		$key=$author['Member']['id'];
		$published_authors[$key]=$author['Member']['given_name'];
		}
		
		foreach($courseslist as $course)
		{
		$key1=$course['Courses']['id'];
		$courses[$key1]=$course['Courses']['title'];
		}
		asort($published_authors);
		$this->set('published_authors',$published_authors);
				
				
		if(!empty($this->data))
		{
			$error=$this->validate_discount($this->data);
			if(count($error)==0){
				
				// Preapare array to save data in one call for all courses and members
                                if(!empty($this->data))
                                {                                
                                $data=array();
                                // Count selected members and course
                                $total_course=count($this->data['Discount']['particular-course']);
                                $total_member=count($this->data['Discount']['members']);
                                
                                for($c_key=0;$c_key<$total_course;$c_key++)
                                {
                                //find author id for this course
                                $authiorid=$this->Courses->find('first',array('fields'=>array('Courses.m_id'),'conditions'=>array('Courses.id'=>$this->data['Discount']['particular-course'][$c_key])));
                                $i=0;
                                for($m_key=($total_member*$c_key+1);$m_key<=($total_member*($c_key+1));$m_key++)
                                {                 
                                foreach($language as $lang)
                                {
                                $title="name_".$lang['Language']['locale'];
                                $description="description_".$lang['Language']['locale'];
                                $data[$m_key-1]['Discount'][$title]=$this->data['Discount'][$title];
                                $data[$m_key-1]['Discount'][$description]=$this->data['Discount'][$description];   
                                }
                                $data[$m_key-1]['Discount']['discount_code']=$this->data['Discount']['discount_code'];
                                $data[$m_key-1]['Discount']['initial_value']=$this->data['Discount']['initial_value'];                              
                                $data[$m_key-1]['Discount']['author']=$authiorid['Courses']['m_id'];
                                $data[$m_key-1]['Discount']['particular_course']=$this->data['Discount']['particular-course'][$c_key];
                                $data[$m_key-1]['Discount']['member']=$this->data['Discount']['members'][$i];
                                if(isset($this->data['Discount']['unlimited_time']) && $this->data['Discount']['unlimited_time']==0)
                                {        
                                $data[$m_key-1]['Discount']['duration']=$this->data['Discount']['unlimited_time'];    
                                $data[$m_key-1]['Discount']['date_from']=date_format(date_create($this->data['Discount']['date_from']),"Y-m-d");
                                $data[$m_key-1]['Discount']['date_to']=date_format(date_create($this->data['Discount']['date_to']),"Y-m-d");
                                }
                                else
                                {
                                $data[$m_key-1]['Discount']['duration']=$this->data['Discount']['unlimited_time'];
                                $data[$m_key-1]['Discount']['date_from']=NULL;
                                $data[$m_key-1]['Discount']['date_to']=NULL;
                                }
                                $data[$m_key-1]['Discount']['member']=$this->data['Discount']['members'][$i];
                                $data[$m_key-1]['Discount']['member']=$this->data['Discount']['members'][$i];
                                $data[$m_key-1]['Discount']['status']=0;
                                $i++;
                                }
                                }
                                
                                }
                                
				if($this->Discount->saveAll($data))
				{
					$this->Session->write('success','Discount has been added successfully');
					$this->redirect(array('controller'=>'users','action'=>'discounts','admin'=>true));
				}
			}
			else{
				$this->set('error',$error);
			}
		}
	}
        
                
        //---- function for edit View---//
	function admin_edit_discount($code=NULL)
	{
		$this->layout='admin';
		$this->loadModel('Discount');	
                $this->loadModel('Member');			
		$this->loadModel('Courses');
                $this->loadModel('Language');
                $language=$this->Language->find('all',array('conditions'=>array('Language.status'=>1),'fields' => array('Language.locale')));
		if(!empty($code))
		{	
                    $codevalue=convert_uudecode(base64_decode($code));
                    $this->Discount->virtualFields = array(
                        'authors_selected'=>'SELECT GROUP_CONCAT(author) FROM discounts as d WHERE d.discount_code=Discount.discount_code',
                        'courses_selected'=>'SELECT GROUP_CONCAT(particular_course) FROM discounts as d WHERE d.discount_code=Discount.discount_code',
                        'member_selected'=>'SELECT GROUP_CONCAT(member) FROM discounts as d WHERE d.discount_code=Discount.discount_code',
                    );
                    $discount_info = $this->Discount->find('first',array('group'=>array('discount_code'),'conditions'=>array('Discount.discount_code'=>$codevalue),'contain'=>array()));          
                              
                    $courseslist=$this->Courses->find('all',array('conditions'=>array('Courses.isPublished'=>1,'status !='=>2)));

                    foreach($courseslist as $course)
                    {
                    $pub_courseids[]=$course['Courses']['m_id'];	
                    }	 

                    $authors=$this->Member->find('all',array('conditions'=>array('Member.role_id' => '3','Member.id'=>$pub_courseids)));

                    $published_authors=array();
                    foreach($authors as $author)
                    {
                    $key=$author['Member']['id'];
                    $published_authors[$key]=$author['Member']['given_name'];
                    }

                    foreach($courseslist as $course)
                    {
                    $key1=$course['Courses']['id'];
                    $courses[$key1]=$course['Courses']['title'];
                    }
                    asort($published_authors);
                    $authors_selected=explode(",",$discount_info['Discount']['authors_selected']);
                    $authors_selected=array_unique($authors_selected);
                    $courses_selected=explode(",",$discount_info['Discount']['courses_selected']);
                    $courses_selected=array_unique($courses_selected);
                    $members_selected=explode(",",$discount_info['Discount']['member_selected']);
                    $members_selected=array_unique($members_selected);
                    $this->set(array('info'=>$discount_info,'published_authors'=>$published_authors,'authors_selected'=>$authors_selected,'courses_selected'=>$courses_selected,'members_selected'=>$members_selected));		
                   
                    
		}
                
                            if(!empty($this->data))
		{
                             
                                
			$error=$this->validate_edit_discount($this->data);
			if(count($error)==0){
				
				// Preapare array to save data in one call for all courses and members
                                                               
                                $data=array();
                                // Count selected members and course
                                $total_course=count($this->data['Discount']['particular-course']);
                                $total_member=count($this->data['Discount']['members']);
                                
                                for($c_key=0;$c_key<$total_course;$c_key++)
                                {
                                //find author id for this course
                                $authiorid=$this->Courses->find('first',array('fields'=>array('Courses.m_id'),'conditions'=>array('Courses.id'=>$this->data['Discount']['particular-course'][$c_key])));
                                $i=0;
                                for($m_key=($total_member*$c_key+1);$m_key<=($total_member*($c_key+1));$m_key++)
                                {                 
                                foreach($language as $lang)
                                {
                                $title="name_".$lang['Language']['locale'];
                                $description="description_".$lang['Language']['locale'];
                                $data[$m_key-1]['Discount'][$title]=$this->data['Discount'][$title];
                                $data[$m_key-1]['Discount'][$description]=$this->data['Discount'][$description];   
                                }
                                $data[$m_key-1]['Discount']['discount_code']=$this->data['Discount']['discount_code'];
                                $data[$m_key-1]['Discount']['initial_value']=$this->data['Discount']['initial_value'];                              
                                $data[$m_key-1]['Discount']['author']=$authiorid['Courses']['m_id'];
                                $data[$m_key-1]['Discount']['particular_course']=$this->data['Discount']['particular-course'][$c_key];
                                $data[$m_key-1]['Discount']['member']=$this->data['Discount']['members'][$i];
                                if(isset($this->data['Discount']['unlimited_time']) && $this->data['Discount']['unlimited_time']==0)
                                {        
                                $data[$m_key-1]['Discount']['duration']=$this->data['Discount']['unlimited_time'];    
                                $data[$m_key-1]['Discount']['date_from']=date_format(date_create($this->data['Discount']['date_from']),"Y-m-d");
                                $data[$m_key-1]['Discount']['date_to']=date_format(date_create($this->data['Discount']['date_to']),"Y-m-d");
                                }
                                else
                                {
                                $data[$m_key-1]['Discount']['duration']=$this->data['Discount']['unlimited_time'];
                                $data[$m_key-1]['Discount']['date_from']=NULL;
                                $data[$m_key-1]['Discount']['date_to']=NULL;
                                }
                                $data[$m_key-1]['Discount']['member']=$this->data['Discount']['members'][$i];
                                $data[$m_key-1]['Discount']['member']=$this->data['Discount']['members'][$i];
                                $data[$m_key-1]['Discount']['status']=0;
                                $i++;
                                }
                                }
                                
                               
                                $codevalue=convert_uudecode(base64_decode($this->data['Discount']['discount_code_old']));
                                $this->Discount->deleteAll(array('Discount.discount_code' => $codevalue), false);
				
                                if($this->Discount->saveAll($data))
				{
					$this->Session->write('success','Discount has been edited successfully');
					$this->redirect(array('controller'=>'users','action'=>'discounts','admin'=>true));
				}
			}
			else{
				$this->set('error',$error);
			}
		}
                                
                                
                
                            
	}
	//---- End of function for edit cms template---//
        
        /*Delete multiple discounts*/
        function admin_delete_discounts()
        {
            if(!empty($this->data))
            {
            $discounts=$this->data['discounts'];
            $discountcodes=array();
            foreach($discounts as $discount)
            {
            $discountcode=convert_uudecode(base64_decode($discount));
            $discountcodes[]=$discountcode;
            }
            $deleted=$this->Discount->deleteAll(array('Discount.discount_code' => $discountcodes), false);
            if($deleted==true)
            {
            if(count($discounts)>1)
            {
            $this->Session->write('success','Discounts have been deleted successfully');
            }
            else
            {
            $this->Session->write('success','Discount has been deleted successfully');
            }
            echo "true";
            }
            }
            exit;
        }
        
        /*Toggle active-inactive multiple discounts*/
        function admin_change_discount_status()
        {
            if(!empty($this->data))
            {
            $discounts=$this->data['discounts'];
            $discountcodes=array();
            foreach($discounts as $discount)
            {
            $discountcode=convert_uudecode(base64_decode($discount));
            $discountcodes[]=$discountcode;
            $discounts_data=$this->Discount->find("all",array('conditions'=>array('Discount.discount_code'=>$discountcode),'fields'=>array('status','id')));
            if($discounts_data[0]['Discount']['status']==0)
            {
            $data=array();
            foreach($discounts_data as $key=>$d_data)
            {
            $data[$key]['Discount']['status']=1;
            $data[$key]['Discount']['id']=$d_data['Discount']['id'];
            }
            $this->Discount->saveAll($data);
            }
            else
            {
            $data=array();
            foreach($discounts_data as $key=>$d_data)
            {
            $data[$key]['Discount']['status']=0;
            $data[$key]['Discount']['id']=$d_data['Discount']['id'];
            }
            $this->Discount->saveAll($data);
            }            
            }
            echo "true";
            }
            exit;
        }
        
         /* To Delete Commissions */
	
		
	
	function admin_deletecommissions()
	{
	
        if(!empty($this->data))
        {
        $this->loadModel('Commission');
        
       
        $selectedids=$this->data['selectedids'];
            $deleted=$this->Commission->deleteAll(array('Commission.package_id' => $selectedids), false);
            if($deleted==true)
            {
            if(count($selectedids)>1)
            {
            $this->Session->write('success','Commissions have been deleted successfully');
            }
            else
            {
            $this->Session->write('success','Commission has been deleted successfully');
            }
            echo "true";
            }
           
        }
        exit;
			
	
	}
        
        
        
        
        
        function admin_delete_selected_records()
	{
	
        if(!empty($this->data))
        {
        
        $model=$this->data['model'];
        $selectedids=$this->data['selectedids'];
        
        $this->loadModel($model);
        $deleted=$this->$model->deleteAll(array($model.'.id' => $selectedids), false);
        //echo $deleted;
        
            if($deleted==true)
            {
            if(count($selectedids)>1)
            {
            $this->Session->write('success','Records have been deleted successfully');
            }
            else
            {
            $this->Session->write('success','Records has been deleted successfully');
            }
            echo "true";
            }
           
        }
        exit;
			
	
	}
        function admin_delete_selected_interests()
	{
	
        if(!empty($this->data))
        {
        $model_type=$this->data["model_type"];
                if($model_type==3){
                     $model='MemberAuthorKeywords';
                                     }else{
                                     $model='MemberKeywords'; }
       
        $selectedids=$this->data['selectedids'];
        
        $this->loadModel($model);
        $deleted=$this->$model->deleteAll(array($model.'.id' => $selectedids), false);
        //echo $deleted;
        
            if($deleted==true)
            {
            if(count($selectedids)>1)
            {
            $this->Session->write('success','Records have been deleted successfully');
            }
            else
            {
            $this->Session->write('success','Records has been deleted successfully');
            }
            echo "true";
            }
           
        }
        exit;
			
	
	}
        
         /*Toggle active-inactive multiple discounts*/
        function admin_change_records_status()
        {
            if(!empty($this->data))
            {
                $data=array();
            $selectedids=$this->data['selectedids'];
             $model=$this->data['model'];
              $this->loadModel($model);
            $selected_data=$this->$model->find("all",array('conditions'=>array($model.'.id'=>$selectedids),'fields'=>array('status','id'),'recursive'=>'-1'));
          //  print_r($selected_data);
            foreach($selected_data as $key=>$status_data){
                 
            if($status_data[$model]['status']==0)
            {
           
            
            $data[$key][$model]['status']=1;
            $data[$key][$model]['id']=$status_data[$model]['id'];
           }
            else
            {
          
             $data[$key][$model]['status']=0;
            $data[$key][$model]['id']=$status_data[$model]['id'];
            }}
           // print_r($data);
           // die;
            $this->$model->saveAll($data);
                        
            }
         $this->Session->write('success','Status has been toggled  successfully');
            echo "true";
            
            exit;
        }
        
        
         /*Toggle active-inactive multiple discounts*/
        function admin_change_interest_status()
        {
            if(!empty($this->data))
            {
                $data=array();
            $selectedids=$this->data['selectedids'];
            $model_type=$this->data["model_type"];
                if($model_type==3){
                     $model='MemberAuthorKeywords';
                                     }else{
                                     $model='MemberKeywords'; }
              $this->loadModel($model);
            $selected_data=$this->$model->find("all",array('conditions'=>array($model.'.id'=>$selectedids),'fields'=>array('status','id'),'recursive'=>'-1'));
          //  print_r($selected_data);
            foreach($selected_data as $key=>$status_data){
                 
            if($status_data[$model]['status']==0)
            {
           
            
            $data[$key][$model]['status']=1;
            $data[$key][$model]['id']=$status_data[$model]['id'];
           }
            else
            {
          
             $data[$key][$model]['status']=0;
            $data[$key][$model]['id']=$status_data[$model]['id'];
            }}
           // print_r($data);
           // die;
            $this->$model->saveAll($data);
                        
            }
        
            echo "true";
            
            exit;
        }
        
        
        /*This function is used to fetch data of members excluding selected author*/
        function admin_getallmembers()
        {
        $requestdata=$this->request->data;
        $notauthorid=$requestdata['authorid'][0];
         $published_members=$this->Member->find('all',array('conditions'=>array('OR'=>array('Member.role_id'=>2,'Member.role_id'=>3),'NOT'=>array('Member.id'=>$notauthorid)),'fields'=>array('id','given_name'),'contain'=>array(false)));
        $members=array();
        foreach($published_members as $published_m)
        {
        $key=$published_m['Member']['id'];
        $members[$key]=$published_m['Member']['given_name'];
        }
        
        asort($members);                
           
        $option='';
                
        if(!empty($members))
		{	
            foreach($members as $key=>$member)
            {

                $option.="<option value='".$key."'>".$member."</option>"; 
            }
	echo $option;
	}
        exit;
        }
        /* end */
        
        /* For getting all author names to display in drop-down for commmission*/

	function admin_getallcomm_authors() 
	{
	$authors=$this->data;
		
	$this->loadModel('Courses');	
	$this->loadModel('Member');	
	$option="<option value='all'>All Courses</option>";        		
	$member=$this->Member->find('all',array('conditions'=>array('Member.id'=>$authors['authors']),'fields'=>array('given_name')));
	$courses=$this->Courses->find('all',array('conditions'=>array('isPublished'=>1,'status !='=>2,'m_id'=>$authors['authors']),'fields'=>array('id','title')));
	if(!empty($courses))
	{	
	asort($courses);
        
	foreach($courses as $key=>$course)
	{
		$key=$course['Courses']['id'];
		$newcourse=$course['Courses']['title'];
	$option.="<option value='".$key."'>".$newcourse."</option>"; 
	}

	}
	
	echo $option;
	exit;    
	}
        
	function validate_add_discount_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_discount($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Discount'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	function admin_add_price()
	{
		$this->layout='admin';
		$this->loadModel('Price');
		if(!empty($this->data))
		{
			$error=$this->validate_edit_price($this->data);
			if(count($error)==0){
				
				if($this->Price->save($this->data))
				{
					//pr($this->data);die;
					$this->Session->write('success','Subscription Price has been added successfully');
					$this->redirect(array('controller'=>'users','action'=>'price','admin'=>true));
				}
			}
			else{
				$this->set('error',$error);
			}
		}
	}
	function validate_add_price_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_price($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Price'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function admin_sl_subcategories($sub_cat_id=NULL)
	{
		$this->layout='admin';
		$this->set('sub_cat_id',$sub_cat_id);
		
		$this->loadModel('CourseSubcategory');
		$this->loadModel('CourseSlSubcategory');
		
		$sub_cat_id = convert_uudecode(base64_decode($sub_cat_id));
		$sub_category = $this->CourseSubcategory->find('first',array('conditions'=>array('CourseSubcategory.id'=>$sub_cat_id),'fields'=>array('id','category_id','en'),'contain'=>false));		
		
		$page = isset($this->params['named']['page']) ? $this->params['named']['page'] : '1';
		$this->paginate = array('limit'=>'10','conditions'=>array('CourseSlSubcategory.sub_category_id'=>$sub_cat_id),'order'=>'CourseSlSubcategory.id DESC','contain'=>false);
		$info = $this->paginate('CourseSlSubcategory');		
		$this->set(compact('info','page','sub_category'));
		
		//pr($category); die;
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/manage';
			 $this->render('sub_category_list');
		}
	}
	
	function admin_add_sl_subcategory($cat_id=NULL,$sub_cat_id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('CourseSlSubcategory');
		$this->set(compact('cat_id','sub_cat_id'));
		
		if(!empty($this->data))
		{
		
		//pr($this->data);die;
			$error=$this->validate_sl_subcategory($this->data);	
			//pr($error);die;			
			if(count($error)==0){			
				
				$sub_cat_id = $this->data['CourseSlSubcategory']['sub_category_id'];
				$this->request->data['CourseSlSubcategory']['category_id'] = convert_uudecode(base64_decode($this->data['CourseSlSubcategory']['category_id']));
				$this->request->data['CourseSlSubcategory']['sub_category_id'] = convert_uudecode(base64_decode($this->data['CourseSlSubcategory']['sub_category_id']));
				$this->request->data['CourseSlSubcategory']['created'] = date('Y-m-d H:i:s');
				$this->request->data['CourseSlSubcategory']['modified'] = date('Y-m-d H:i:s');
				$this->request->data['CourseSlSubcategory']['status'] = 1;
				
				if($this->CourseSlSubcategory->save($this->data))
				{
					$this->Session->write('success','SL Sub-Category has been added successfully');
					$this->redirect(array('controller'=>'users','action'=>'sl_subcategories/'.$sub_cat_id,'admin'=>true));
			
				}
				}
		
			else{ 
				$this->set('error',$error);
			}
			
		}
	}
	
	function admin_edit_sl_subcategory($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('CourseSlSubcategory');
		
		$id = convert_uudecode(base64_decode($id));
		$info=$this->CourseSlSubcategory->find('first',array('conditions'=>array('CourseSlSubcategory.id'=>$id),'contain'=>false));
		$this->set(compact('info'));
		
		if(!empty($this->data))
		{
			/*$error=$this->validate_sl_subcategory($this->data);			
			if(count($error)==0){*/
			
				$sub_cat_id = $this->data['CourseSlSubcategory']['sub_category_id'];
				$this->request->data['CourseSlSubcategory']['id'] = convert_uudecode(base64_decode($this->data['CourseSlSubcategory']['id']));
				$this->request->data['CourseSlSubcategory']['sub_category_id'] = convert_uudecode(base64_decode($sub_cat_id));
				$this->request->data['CourseSlSubcategory']['modified'] = date('Y-m-d H:i:s');
				
				if($this->CourseSlSubcategory->save($this->data))
				{
					$this->Session->write('success','SL Sub Category has been updated successfully');
					$this->redirect(array('controller'=>'users','action'=>'sl_subcategories/'.$sub_cat_id,'admin'=>true));			
				}
			/*}		
			else
			{
				$this->set('error',$error);
			}*/
		}
	}
	
	function validate_sl_subcategory_ajax()
	{	
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_sl_subcategory($this->data);	
		   // pr($error);die;
			if(is_array($this->data))	{
				foreach ($this->data['CourseSlSubcategory'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			
			die;
		}	
	}
	
	function validate_sl_subcategory($data)
	{ //pr($data);die;
		$errors = array ();	
		$locale = $data['CourseSlSubcategory']['locale'];		
		if (trim($data['CourseSlSubcategory'][$locale]) == ""){
			$errors[$locale][] = FIELD_REQUIRED."\n";
			
		}		
		if (trim($data['CourseSlSubcategory'][$locale]) != "")
		{
			$this->loadModel('CourseSlSubcategory');
			$sub_cat_id = convert_uudecode(base64_decode($data['CourseSlSubcategory']['sub_category_id']));
			
			$conditions = array('CourseSlSubcategory.sub_category_id'=>$sub_cat_id,'CourseSlSubcategory.'.$locale=>$data['CourseSlSubcategory'][$locale]);
			
			if(!empty($data['CourseSlSubcategory']['id'])) // if edit case
			{
				$id = convert_uudecode(base64_decode($data['CourseSlSubcategory']['id']));
				$conditions = array_merge($conditions,array('CourseSlSubcategory.id !='=>$id));
			}
			
			$check_exist=$this->CourseSlSubcategory->find('count',array('conditions'=>$conditions));
			if($check_exist>0)
			{
				$errors[$locale][] = SL_SUBCATEGORY_EXIST."\n";
			}
		}
		
		return $errors;
	}
	
	function admin_setStatus($model=NULL)
	{
		$id = $_POST['id'];
		$this->loadModel($model);
		
		if(!empty($id) && !empty($model))
		{
			$id = convert_uudecode(base64_decode($id));
			$rec = $this->$model->find('first',array('conditions'=>array($model.'.id'=>$id),'recursive'=>'-1'));

			if($rec[$model]['status'] == "1")
			{
				$this->$model->updateAll(array($model.'.status'=>"'0'"),array($model.'.id'=>$id));
			}
			else
			{
				$this->$model->updateAll(array($model.'.status'=>"'1'"),array($model.'.id'=>$id));
			}
			echo "done";
		}
		die;	
	}
	
	function admin_common_delete($model=NULL)
	{
		$id = $_POST['id']; 
		$this->loadModel($model);
		
		if($this->request->is('ajax'))
		{
			if(!empty($model) && !empty($id))
			{
				$id = convert_uudecode(base64_decode($id));				
				$this->$model->delete($id);
				echo 'delete';
			}
		}
		die;
	}
	
	function admin_course_prices()
	{
		$this->layout='admin';		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('CoursePrice');
		$this->paginate=array('limit'=>'-1','order'=>'CoursePrice.id DESC');
		$info=$this->paginate('CoursePrice');
		//pr($info); die;
		$this->set(compact('page','info'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('course_price_list');
		}
	}
	function admin_add_course_price()
	{
		$this->layout='admin';
		$this->loadModel('CoursePrice');
		if(!empty($this->data))
		{
			$error=$this->validate_edit_course_price($this->data);
			if(count($error)==0){
				if($this->CoursePrice->save($this->data))
				{
					$this->Session->write('success','Course Price has been added successfully');
					$this->redirect(array('controller'=>'users','action'=>'course_prices','admin'=>true));
				}
			}
			else{
				$this->set('error',$error);
			}
		}
	}
	
	function validate_edit_course_price_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_course_price($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['CoursePrice'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	function validate_edit_course_price($data)
	{			
		$errors = array ();		
		
		if (empty($data['CoursePrice']['price'])){
			$errors['price'][] = FIELD_REQUIRED."\n";
		}
		if(!empty($data['CoursePrice']['price']))
		{ 
		  if(!is_numeric($data['CoursePrice']['price']))
		 {
			$errors ['price'] [] = "Please Enter Numeric Value"."\n";
		 }
		} 
		return $errors;			
	}
	
	function admin_edit_course_price($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('CoursePrice');		
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->CoursePrice->find('first',array('conditions'=>array('CoursePrice.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}		
		if(!empty($this->data))
		{
			$error=$this->validate_edit_course_price($this->data);
			if(count($error)==0){
				if($this->CoursePrice->save($this->data))
				{
					$this->Session->write('success','Course Price has been updated successfully');
					$this->redirect(array('controller'=>'users','action'=>'course_prices','admin'=>true));
				}
			}
			else{
				$this->set('error',$error);
			}
		}
	}
	
	
	function admin_lesson_prices()
	{
		$this->layout='admin';		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('LessonPrice');
		$this->paginate=array('limit'=>'10','order'=>'LessonPrice.id DESC');
		$info=$this->paginate('LessonPrice');
		//pr($info); die;
		$this->set(compact('page','info'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('lesson_price_list');
		}
	}
	function admin_add_lesson_price()
	{
		$this->layout='admin';
		$this->loadModel('LessonPrice');
		if(!empty($this->data))
		{
			$error=$this->validate_edit_lesson_price($this->data);
			if(count($error)==0){
				
				if($this->LessonPrice->save($this->data))
				{
					$this->Session->write('success','Lesson Price has been added successfully');
					$this->redirect(array('controller'=>'users','action'=>'lesson_prices','admin'=>true));
				}
			}
			else{
				$this->set('error',$error);
			}
		}
	}
	
	function validate_edit_lesson_price_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_lesson_price($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['LessonPrice'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	function validate_edit_lesson_price($data)
	{			
		$errors = array ();		
		
		if ($data['LessonPrice']['price']==''){
			$errors['price'][] = FIELD_REQUIRED."\n";
		}
		if(!empty($data['LessonPrice']['price']))
		{ 
		  if(!is_numeric($data['LessonPrice']['price']))
		 {
			$errors ['price'] [] = "Please Enter Numeric Value"."\n";
		 }
		} 
		return $errors;			
	}
	
	function admin_edit_lesson_price($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('LessonPrice');		
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->LessonPrice->find('first',array('conditions'=>array('LessonPrice.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}		
		if(!empty($this->data))
		{
			$error=$this->validate_edit_lesson_price($this->data);
			if(count($error)==0){
				if($this->LessonPrice->save($this->data))
				{
					$this->Session->write('success','Lesson Price has been updated successfully');
					$this->redirect(array('controller'=>'users','action'=>'lesson_prices','admin'=>true));
				}
			}
			else{
				$this->set('error',$error);
			}
		}
	}
	
	function admin_reasons()
	{
		$this->layout='admin';
		$this->loadModel('Reason');
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->paginate = array('limit'=>'-1','order'=>'Reason.id DESC');
		$info = $this->paginate('Reason');
		$this->set(compact('info','page'));
	}	
	
	function admin_add_reason()
	{
		$this->layout='admin';
		$this->loadModel('Reason');
		if(!empty($this->data))
		{
			$data = $this->data;
			$data['Reason']['date'] = date('Y-m-d');
			
			if($this->Reason->save($data))
			{
				$this->Session->write('success','Reason has been added successfully');
				$this->redirect(array('controller'=>'users','action'=>'reasons','admin'=>true));
			}
			
		}
	}
	
	function validate_add_reason_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_add_reason($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Reason'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_add_reason($data)
	{		
		$errors = array ();		
		
		$locale = $data['Reason']['locale'];
		if (trim($data['Reason'][$locale]) == ""){
			$errors[$locale][] = FIELD_REQUIRED."\n";
		}
		if (trim($data['Reason'][$locale]) != ""){
			$this->loadModel('Reason');
			$check_exist=$this->Reason->find('count',array('conditions'=>array('Reason.'.$locale=>$data['Reason'][$locale])));
			if($check_exist>0)
			{
				$errors[$locale][] = CATEGORY_EXIST."\n";
			}
		}
		return $errors;			
	}
	
	

	function admin_edit_reason($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('Reason');		
		
		$this->all_language();
		
		if(!empty($id))
		{	
			$id = convert_uudecode(base64_decode($id));
			$info = $this->Reason->find('first',array('conditions'=>array('Reason.id'=>$id)));
			$this->set('info',$info);
		}		
		if(!empty($this->data))
		{
			//pr($this->data); die;
			
			$this->Reason->save($this->data);
			$this->Session->write('success','Reason has been updated successfully');
			$this->redirect(array('controller'=>'users','action'=>'reasons','admin'=>true));
			
		}		
	}
	
	function validate_edit_reason_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_reason($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Reason'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_edit_reason($data)
	{		
		$errors = array ();		
		
		$locale = $data['Reason']['locale'];
		if (trim($data['Reason'][$locale]) == ""){
			$errors[$locale][] = FIELD_REQUIRED."\n";
		}
		if (trim($data['Reason'][$locale]) != ""){
			$this->loadModel('Reason');
			
			$check_exist=$this->Reason->find('count',array('conditions'=>array('Reason.'.$locale=>$data['Reason'][$locale],'Reason.id <>'=>$data['Reason']['id'])));
			
			if($check_exist>0)
			{
				$errors[$locale][] = REASON_EXIST."\n";
			}
		}
		return $errors;			
	}
	
	
	
	function admin_add_courses()
	{
		$this->layout='admin';
		
		$this->loadModel('Language');
		$this->loadModel('Member');
		$this->loadModel('Interest');
		$this->loadModel('CourseCategory');
		
		$language=$this->Language->find('list',array('fields' => array('Language.language')));
		$interests=$this->Interest->find('list',array('fields' => array('Interest.interest')));		
		$authors=$this->Member->find('list',array('conditions'=>array('Member.role_id'=>3),'fields' => array('Member.final_name')));
		$categories = $this->CourseCategory->find('list',array('order'=>'CourseCategory.title ASC','fields'=>array('id','title'),'conditions'=>array('CourseCategory.status'=>1),'contain'=>false));
		$this->set(compact('language','authors','interests','categories'));
		
		//echo"<pre>"; print_r($categories);die;
		
		if(!empty($this->data))
		{
			//echo"<pre>";print_r($this->data);die;
			$error=$this->validate_cours($this->data);
			if(count($error)==0){
				$this->loadModel('Course');
				$this->loadModel('CourseKeyword');
				$this->request->data['Course']['date_added']= strtotime(date('Y-m-d H:i:s'));
				
				if($this->data['Course']['category_id']=='NULL')
				{
					$this->request->data['Course']['category_id']=NULL;
				}
				if($this->data['Course']['subcategory_id']=='NULL')
				{
					$this->request->data['Course']['subcategory_id']=NULL;
				}
				if($this->data['Course']['sl_subcategory_id']=='NULL')
				{
					$this->request->data['Course']['sl_subcategory_id']=NULL;
				}
				
				$this->Course->save($this->data);
				$course_id = $this->Course->getLastInsertId();
				foreach($this->data['Course']['keyword'] as $key=>$value){
					
					$interest['CourseKeyword']['m_id']= $this->data['Course']['m_id'];
					$interest['CourseKeyword']['course_id']= $course_id;
					$interest['CourseKeyword']['keyword']= $value;
					$this->CourseKeyword->create();
					$this->CourseKeyword->save($interest);
				}	
				$this->Session->write('success','Course has been added Successfully');
				$this->redirect(array('controller'=>'users','action'=>'courses','admin'=>true));
			}
			else{
				$this->set('error',$error);
			}
		}
	}
	function all_language()
	{
		$this->loadModel('Language');
		$language = $this->Language->find('all',array('conditions'=>array('Language.status'=>'1'),'order'=>'Language.id ASC'));
		$this->set('language',$language);
		//echo "<pre>"; print_r($language); die;
	}
	function active_language()
	{
		
	}
	function admin_time()
	{
		$this->layout='admin';
		$this->loadModel('LogoutTime');
		
		$info = $this->LogoutTime->findById('1');
		$this->set('info',$info);
		
		if(!empty($this->data))
		{
			$this->request->data['LogoutTime']['id'] = 1;
			$this->LogoutTime->save($this->data);
			$this->Session->write('success','Logout Time has been updated successfully');
			$this->redirect($this->referer());
		}
	}
	function admin_reports_on_reviews()
	{
		$this->layout='admin';		
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('RemarkReport');
		$this->paginate=array('limit'=>'-1','order'=>'RemarkReport.id ASC','contain'=>array('CourseRemark'=>array('Course'),'Member.given_name'));
                
                $remarks=$this->RemarkReport->find('all',array('conditions'=>array()));
                
		$info=$this->paginate('RemarkReport');
                //echo "<pre>";
                //print_r($info);
		$this->set(compact('page','info'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath = 'Elements'.DS.'adminElements/admin/member';
			 $this->render('report_list');
		}
	}
        function admin_view_reports_review($id=NULL)
        {
            $this->layout='admin';	
            $id = convert_uudecode(base64_decode($id));
		//$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->loadModel('RemarkReport');
		//$this->paginate=array('limit'=>'-1','order'=>'RemarkReport.id ASC','contain'=>array('CourseRemark'=>array('Course'),'Member.given_name'));
            $info=$this->RemarkReport->find('first',array('conditions'=>array('RemarkReport.id' =>$id),'contain'=>array('CourseRemark'=>array('Course'),'Member.given_name')));
//            echo "<pre>";
//            print_r($info);
//            die;
            $this->set(compact('info'));
        }
	function delete_review()
	{
		$this->loadModel('CourseRemark');
		$remark_id=convert_uudecode(base64_decode($_GET['review_id']));
		if($this->CourseRemark->delete($remark_id, $cascade =true))
		{
			echo "deleted";
		}
		die;
	}
	function admin_commission()
	{
		$this->layout='admin';
		$this->loadModel('Commission');
		$this->loadModel('Defaultcommission');
		//$defaultcommission=$this->Defaultcommission->find('all',array('conditions'=>array('Defaultcommission.id'=>1)));
		
		//$defaultcom=$defaultcommission[0]['Defaultcommission']['commission'];
	
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$this->Commission->virtualFields = array(
                        'coursesid_selected'=>'(SELECT GROUP_CONCAT(c.particular-course) FROM commissions as c WHERE c.package_id=Commission.package_id)',
                    'coursestitle_selected'=>'(Select GROUP_CONCAT(title ) AS titles from courses where id IN(SELECT c.`particular-course` FROM commissions AS c WHERE c.package_id = Commission.package_id))',
                      
                    );
                
                /*
                  
                  
                  
             running two queries 
1
                  
                 SELECT com . * , (
(

SELECT GROUP_CONCAT( c.`particular-course` ) AS ids
FROM commissions AS c
WHERE c.author_name = com.author_name
AND com.courses =1
AND c.courses =1
)
) AS g, (

SELECT co.title
FROM courses AS co
WHERE id
IN (
(

SELECT GROUP_CONCAT( n.`particular-course` ) AS ids
FROM commissions AS n
WHERE n.author_name = com.author_name
AND com.courses =1
AND n.courses =1
)
)
) AS title
FROM  `commissions` AS com
GROUP BY  `commission` ,  `author_name` 
ORDER BY com.id
LIMIT 0 , 30


2
                 * 
Select * from (SELECT com . * , (
(

SELECT GROUP_CONCAT( c.`particular-course` ) AS ids
FROM commissions AS c
WHERE c.author_name = com.author_name
AND com.courses =1
AND c.courses =1
)
) AS g
FROM  `commissions` AS com
GROUP BY  `commission` ,  `author_name` 
) as first INNER JOIN  (Select GROUP_CONCAT(ct.title) as title from (SELECT co.m_id,co.title from courses as co  where co.id In(first.g)) as ct group by m_id) as second  
                 
                 3
                  
                 Select GROUP_CONCAT(ct.title) from (SELECT co.m_id,co.title from courses as co  where co.id In(330,353,533)) as ct group by m_id
 
                  
                                  */
		$info=$this->Commission->find('all',array('order'=>array('id'),'group'=>array('package_id')));
                
		$this->set(compact('defaultcom','info','page'));
		
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $this->render('commission_list');
		}
	}
	function admin_edit_commission($packageid=NULL)
	{          
        $this->layout='admin';
		$this->loadModel('Commission');
		$this->loadModel('Member');			
		$this->loadModel('Courses');	
		$pageId='';		
		if(!empty($packageid))
		{	
			$packageid=convert_uudecode(base64_decode($packageid));
			$this->Commission->virtualFields=array(	                    
	                'coursestitle_selected'=>'(Select title from courses as c where c.id=Commission.particular-course and Commission.courses=1)',
	                );
			$info = $this->Commission->find('all',array('conditions'=>array('Commission.package_id'=>$packageid),'contain'=>array()));
			
			$authors=explode(",",$info['Commission']['author_name']);
				
			$courseslist=$this->Courses->find('all',array('conditions'=>array('Courses.isPublished'=>1,'m_id'=>$info[0]['Commission']['author_name'],'Courses.status != '=>2)));
	                                  
			$authors=$this->Member->find('all',array('conditions'=>array('Member.role_id' => '3')));
			
			$members=array();
	                $courses=array("all"=>" All Courses");
			foreach($authors as $author)
			{
			$key=$author['Member']['id'];
			$members[$key]=$author['Member']['given_name'];
			}
			asort($members);
			foreach($courseslist as $course)
			{
			$key1=$course['Courses']['id'];
			$courses[$key1]=$course['Courses']['title'];
			}
			$this->set('members',$members);
			asort($courses);
			$this->set('courses',$courses);
			$this->set('info',$info);
		}		
              
		$data=array();
               
        if(!empty($this->data))
		{	
        	$info = $this->Commission->find('all',array('fields'=>array('id'),'conditions'=>array('Commission.package_id'=>convert_uudecode(base64_decode($this->data['Commission']['package_id']))),'contain'=>array()));
        	$ids=array();
	        foreach($info as $in)
	        {
	        	$ids[]=$in['Commission']['id'];
	        }
		if($this->data['Commission']['courses'][0]!='all')
		{
			$data=array();
			if(count($this->data['Commission']['courses'])<count($ids))
	        {		
		        for($update_i=0;$update_i<count($this->data['Commission']['courses']);$update_i++)
				{                  
					$coursedata=$this->Courses->find('all',array('conditions'=>array('id'=>$this->data['Commission']['courses'][$update_i])));
					$data[$update_i]['Commission']['commission']=$this->data['Commission']['commission'];				
					$data[$update_i]['Commission']['author_name']=$coursedata[0]['Courses']['m_id'];
					$data[$update_i]['Commission']['duration']=$this->data['Commission']['duration'];
					if($this->data['Commission']['duration']==0)
					{
						$data[$update_i]['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_from'])));
						$data[$update_i]['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_to'])));
					}
					$data[$update_i]['Commission']['courses']=1;
					$data[$update_i]['Commission']['particular-course']=$this->data['Commission']['courses'][$update_i];
					$data[$update_i]['Commission']['date']=$this->data['Commission']['date'];
					$data[$update_i]['Commission']['package_id']=convert_uudecode(base64_decode($this->data['Commission']['package_id']));
			        $data[$update_i]['Commission']['id']=$ids[$update_i];
			        $authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$coursedata[0]['Courses']['m_id'])));
					
					
			//		if($this->data['Commission']['duration']==0)
			//		{
			//		
			//		/* Locale date begin*/
			//				
			//		$m_locale = $authoremail['Member']['locale']? $authoremail['Member']['locale']:'en';
			//		
			//		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
			//		
			//		$date_from = $this->data['Commission']['date_from'];
			//		$date_from = strtotime($date_from);
			//		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
			//				
			//		$date_to = $this->data['Commission']['date_to'];
			//		$date_to = strtotime($date_to);
			//		
			//		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
			//						
			//		/* Locale date end*/	
			//		
			//		
			//		
			//		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
			//		$emailData = $emailTemplate['EmailTemplate']['description'];	
			//		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$this->data['Commission']['commission'],$coursedata[0]['Courses']['title'],$date_from,$date_to),$emailData);							
			//		$emailTo = $authoremail['Member']['email'];
			//		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			//		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			//		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
			//		
			//		}
			//		else
			//		{
			//			
			//		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
			//		$emailData = $emailTemplate['EmailTemplate']['description'];	
			//		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$this->data['Commission']['commission'],$coursedata[0]['Courses']['title']),$emailData);							
			//		$emailTo = $authoremail['Member']['email'];
			//		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
			//		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
			//		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);	
			//			
			//		}
				}
				for($delete_i=count($this->data['Commission']['courses']);$delete_i<count($ids);$delete_i++)
				{                   
					$this->Commission->delete(array('Commission.id'=>$ids[$delete_i])); 
				}	        
	        }
	        elseif(count($this->data['Commission']['courses'])>count($ids))
		    {
		        for($update_i=0;$update_i<count($ids);$update_i++)
				{                  
					$coursedata=$this->Courses->find('all',array('conditions'=>array('id'=>$this->data['Commission']['courses'][$update_i])));
					$data[$update_i]['Commission']['commission']=$this->data['Commission']['commission'];				
					$data[$update_i]['Commission']['author_name']=$coursedata[0]['Courses']['m_id'];
					$data[$update_i]['Commission']['duration']=$this->data['Commission']['duration'];
					if($this->data['Commission']['duration']==0)
					{
					$data[$update_i]['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_from'])));
					$data[$update_i]['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_to'])));
					}
					$data[$update_i]['Commission']['courses']=1;
					$data[$update_i]['Commission']['particular-course']=$this->data['Commission']['courses'][$update_i];
					$data[$update_i]['Commission']['date']=$this->data['Commission']['date'];
					$data[$update_i]['Commission']['package_id']=convert_uudecode(base64_decode($this->data['Commission']['package_id']));
			        $data[$update_i]['Commission']['id']=$ids[$update_i];	       
				}
				for($insert_i=count($ids);$insert_i<count($this->data['Commission']['courses']);$insert_i++)
				{		                  
					$coursedata=$this->Courses->find('all',array('conditions'=>array('id'=>$this->data['Commission']['courses'][$insert_i])));
					$data[$insert_i]['Commission']['commission']=$this->data['Commission']['commission'];				
					$data[$insert_i]['Commission']['author_name']=$coursedata[0]['Courses']['m_id'];
					$data[$insert_i]['Commission']['duration']=$this->data['Commission']['duration'];
					if($this->data['Commission']['duration']==0)
					{
					$data[$insert_i]['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_from'])));
					$data[$insert_i]['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_to'])));
					}
					$data[$insert_i]['Commission']['courses']=1;
					$data[$insert_i]['Commission']['particular-course']=$this->data['Commission']['courses'][$insert_i];
					$data[$insert_i]['Commission']['date']=$this->data['Commission']['date'];
					$data[$insert_i]['Commission']['package_id']=convert_uudecode(base64_decode($this->data['Commission']['package_id']));	        
				}	        
	        }
	        else
	    	{
		    	foreach($this->data['Commission']['courses'] as $key=>$course)
				{                  
					$coursedata=$this->Courses->find('all',array('conditions'=>array('id'=>$course)));
					$data[$key]['Commission']['commission']=$this->data['Commission']['commission'];				
					$data[$key]['Commission']['author_name']=$coursedata[0]['Courses']['m_id'];
					$data[$key]['Commission']['duration']=$this->data['Commission']['duration'];
					if($this->data['Commission']['duration']==0)
					{
						$data[$key]['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_from'])));
						$data[$key]['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_to'])));
					}
					$data[$key]['Commission']['courses']=1;
					$data[$key]['Commission']['particular-course']=$course;
					$data[$key]['Commission']['date']=$this->data['Commission']['date'];
					$data[$key]['Commission']['package_id']=convert_uudecode(base64_decode($this->data['Commission']['package_id']));
			        $data[$key]['Commission']['id']=$ids[$key];
				}	
	    	}
		
		}
		elseif($this->data['Commission']['courses'][0]=='all')
		{                
			$data=array();					
		    $key=0;
			$data[$key]['Commission']['commission']=$this->data['Commission']['commission'];				
			$data[$key]['Commission']['duration']=$this->data['Commission']['duration'];
			if($this->data['Commission']['duration']==0)
			{
				$data[$key]['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_from'])));
				$data[$key]['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_to'])));
			}
			$data[$key]['Commission']['courses']=0;
			$data[$key]['Commission']['particular-course']='';
			$data[$key]['Commission']['date']=$this->data['Commission']['date'];
	        $data[$key]['Commission']['id']=$info[0]['Commission']['id'];	        
	        for($delete_i=1;$delete_i<count($ids);$delete_i++)
			{                   
				$this->Commission->delete(array('Commission.id'=>$ids[$delete_i])); 
			}
		}     
		$this->Commission->saveAll($data);	
        $this->Session->write('success','Commission has been updated successfully');
		$this->redirect(array('action'=>'commission','admin' => true));			
		
	}
	}
        
	function validate_edit_commission_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_commission($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['Commission'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	
	function validate_edit_commission($data)
	{			
		$errors = array ();		
		
		if(empty($data['Commission']['commission01']) && $data['Commission']['commission01']!=0){
			$errors['commission'][] = FIELD_REQUIRED."\n";
		}
		if(!empty($data['Commission']['commission01']))
		{ 
			if(!is_numeric($data['Commission']['commission01']))
			{
			$errors ['commission'] [] = "Please Enter Numeric Value"."\n";
			}
			else
			{
				$exp1 = '/^[0-9]{1,2}([.][0-9]+)?+$/';
				if($data['Commission']['commission']!=100)
				{
					if(!(preg_match($exp1,$data['Commission']['commission'])))
					{
						$errors ['commission'] [] = "A Valid Percentage Value is Required."."\n";
						
					}
				}
			}
		}	
		return $errors;			
	}
	function admin_subscription()
	{
		$this->layout='admin';
		$this->loadModel('Subscription');
		$info=$this->Subscription->find('first');
		$state=$info['Subscription']['subscription_status'];
		if($state)
		{
			$value='on';		
		}
		else
		{
			$value='off';
		}
		
		$this->set('value',$value);
	}
	
	function admin_subs_status($val=NULL)
	{
		$this->loadModel('Subscription');
		if($val=='on')
		{
			$this->Subscription->updateAll(array('Subscription.subscription_status'=>1));
		}
		if($val=='off')
		{
			$this->Subscription->updateAll(array('Subscription.subscription_status'=>0));
		}
		die;
	}
	
	function admin_paypal()
	{
		$this->layout='admin';
		$this->loadModel('PaypalInfo');
		$info=$this->PaypalInfo->find('first');
		/*$state=$info['Paypal']['paypal_status'];
		if($state)
		{
			$value='on';		
		}
		else
		{
			$value='off';
		}*/
		
		$this->set('info',$info);
	}
        
        function admin_paypal_info()
	{
		$this->layout='admin';
		$this->loadModel('PaypalInfo');
	if(!empty($this->data))
            {
            $error=$this->validate_paypal($this->data);
            
            if(count($error)==0){
                
                 $this->PaypalInfo->save($this->data);
                 $this->Session->write('success','Paypal information has been updated successfully.');
                 $this->redirect(array('controller'=>'users','action'=>'paypal','admin'=>true));
            }
                
            }
           
		
        }
        function validate_paypal_ajax(){
            $this->layout="";
		$this->autoRender=false;	
		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_paypal($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['PaypalInfo'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
        }
        
        function validate_paypal($data)
        {
            $errors = array ();
                if($data['PaypalInfo']['paypal_live']=='testMode'){
                    $type='sandbox';
                }
                else{
                    $type='live';
                }
		if(trim($data['PaypalInfo'][$type.'_url'] == '')){
			$errors[$type.'_url'][] = FIELD_REQUIRED."\n";
		}
		if($data['PaypalInfo'][$type.'_api_pwd'] ==''){
			$errors[$type.'_api_pwd'][] = FIELD_REQUIRED."\n";
		}
                if($data['PaypalInfo'][$type.'_api_sign'] ==''){
			$errors[$type.'_api_sign'][] = FIELD_REQUIRED."\n";
		}
               if($data['PaypalInfo'][$type.'_api_user'] ==''){
			$errors[$type.'_api_user'][] = FIELD_REQUIRED."\n";
		}
                if($data['PaypalInfo'][$type.'_user'] ==''){
			$errors[$type.'_user'][] = FIELD_REQUIRED."\n";
		}
                
		return $errors;	
            
        }
	
	function admin_paypal_status($val=NULL)
	{
		$this->loadModel('Paypal');
		if($val=='on')
		{
			$this->Paypal->updateAll(array('Paypal.paypal_status'=>1));
		}
		if($val=='off')
		{
			$this->Paypal->updateAll(array('Paypal.paypal_status'=>0));
		}
		die;
	}
	function admin_languages_status()
	{
		$this->layout='admin';
		$this->loadModel('Language');
		$info=$this->Language->find('all',array('fields'=>array('language','status')));
		$this->set('info',$info);
		//pr($info);die;
		
	
	
	}
	function admin_lang_status_change($val=NULL)
	{ 
		$this->loadModel('Language');
		if($val=='on_german')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>2));
		}
		if($val=='off_german')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>2));
		}
		
		if($val=='on_arabic')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>4));
		}
		if($val=='off_arabic')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>4));
		}
		if($val=='on_french')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>5));
		}
		if($val=='off_french')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>5));
		}
		if($val=='on_italian')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>6));
		}
		if($val=='off_italian')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>6));
		}
		if($val=='on_japanese')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>7));
		}
		if($val=='off_japanese')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>7));
		}
		if($val=='on_korean')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>8));
		}
		if($val=='off_korean')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>8));
		}
		if($val=='on_portuguese')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>9));
		}
		if($val=='off_portuguese')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>9));
		}
		if($val=='on_russian')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>10));
		}
		if($val=='off_russian')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>10));
		}
		if($val=='on_chinese')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>11));
		}
		if($val=='off_chinese')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>11));
		}
		if($val=='on_spanish')
		{
			$this->Language->updateAll(array('Language.status'=>1),array('Language.id'=>12));
		}
		if($val=='off_spanish')
		{
			$this->Language->updateAll(array('Language.status'=>0),array('Language.id'=>12));
		}
		
		die;
	}
	function admin_news_letter()
	{
		$this->layout="admin";
		$this->loadModel('Newsletter');
		$conditions = array('Newsletter.locale'=>'en');
		$this->paginate = array('order'=>'id DESC','limit'=>'50');
		$info = $this->paginate('Newsletter',$conditions);
		$this->set(compact('info'));
		//pr($info); die;
	}
	function admin_add_news_letter()
	{
		$this->layout="admin";
		$this->all_language();
		
		$this->loadModel('Newsletter');
		if(!empty($this->data))
		{
			//pr($this->data); die;
			$i = 1;
			
		
			foreach($this->data as $data)
			{ //pr($data);die;
				if(!empty($main_id))
				{
					$data['Newsletter']['main_id'] = $main_id;
				}
				
				$this->Newsletter->create();
				
				$this->Newsletter->save($data);
				
				if($i == '1')
				{
					$main_id = $this->Newsletter->getLastInsertId();
					$info = $this->Newsletter->findById($main_id);
					$info['Newsletter']['main_id'] = $main_id;
					$this->Newsletter->save($info);
				}
				$i++;
			}
			
			$this->Session->write('success','Newsletter has been added');
			$this->redirect(array('controller'=>'users','action'=>'news_letter','admin'=>true));
				
		}
	}
	
	function admin_edit_news_letter($id=NULL)
	{
		$this->layout="admin";
		$this->all_language();
		$this->loadModel('Newsletter');
		if(!empty($id))
		{	
			$lang = $this->Language->find('list',array('conditions'=>array('Language.status'=>1),'fields'=>array('id','locale')));					
			$id = convert_uudecode(base64_decode($id));
			$info = $this->Newsletter->find('all',array('conditions'=>array('Newsletter.main_id'=>$id,'Newsletter.locale'=>$lang ),'order'=>'Newsletter.id ASC'));
			$this->set('info',$info);
			//pr($info); die;			
		}
		
		if(!empty($this->data))
		{
			//pr($this->data); die;
			$data = $this->data;			
			
			foreach($this->data as $data)
			{				
				$this->Newsletter->save($data);								
			}
			$this->Session->write('success','Newsletter  has been updated');
			$this->redirect(array('controller'=>'users','action'=>'news_letter','admin'=>true));
		}
		
	}
	
	function admin_delete_newsletter()
	{ 	
		$this->loadModel('Newsletter');
		$id = $_POST['id'];
		
		if(!empty($id))
		{
			$id = convert_uudecode(base64_decode($id));
			$this->Newsletter->deleteAll(array('Newsletter.main_id'=>$id));
		}
		die;
	}
	
	function admin_send_newsletter($id=Null)
	{  
		$this->layout="admin";
		$this->all_language();
		$this->loadModel('Member');
		$this->loadModel('Newsletter');
		$this->loadModel('Language');
		$lang = $this->Language->find('list',array('conditions'=>array('Language.status'=>'1'),'fields'=>array('id','locale')));
		if(!empty($id))
		{						
			$id = convert_uudecode(base64_decode($id));
			$info = $this->Newsletter->find('all',array('conditions'=>array('Newsletter.main_id'=>$id,'Newsletter.locale'=>$lang),'order'=>'Newsletter.id ASC'));
			$this->set('info',$info);
			//pr($info); die;			
		}
		
		if(!empty($this->data))
		{ 
			//pr($this->data);die;
			 if(isset($this->params['data']['sub']))
			 { 
				  foreach($this->data as $data)
					{			
						$this->Newsletter->save($data);
					}
					$news_id = $this->data[1]['Newsletter']['id'];
				   
				   $member = $this->Member->find('all',array('fields'=>array('id','email','notification','locale','given_name'),'recursive'=>'-1','conditions'=>array('Member.notification'=>1,'Member.status'=>1)));
				   
				   foreach($member as $member)
					{   
						$m_locale = $member['Member']['locale'] ? $member['Member']['locale'] : 'en';
						
						
						$news = $this->Newsletter->find('first',array('conditions'=>array('Newsletter.main_id'=>$news_id,'Newsletter.locale'=>$m_locale)));	
						$email=$member['Member']['email'];
						$final_name=$member['Member']['given_name'];
						$this->loadModel('EmailTemplate');									
						$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>14,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
						$emailData = $emailTemplate['EmailTemplate']['description'];						
						$emailData = str_replace(array('{name}','{title}','{description}'),array($final_name,$news['Newsletter']['title'],$news['Newsletter']['description']),$emailData);
						$emailTo =$email;
						$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
						$emailSubject = $emailTemplate['EmailTemplate']['subject'];
						$emailSubject = str_replace(array('{subject_title}'),array($news['Newsletter']['title']),$emailSubject);
						$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
						
					}
				$this->Session->write('success','Newsletter has been sent successfully');
				$this->redirect(array('controller'=>'users','action'=>'news_letter','admin'=>true));
				
			 }
			 else if(isset($this->params['data']['all']))
			 { 
				foreach($this->data as $data)
					{			
						$this->Newsletter->save($data);
					}
					$news_id = $this->data[1]['Newsletter']['id'];
				   //pr($news_id);die;
				   $member = $this->Member->find('all',array('fields'=>array('id','email','notification','locale','given_name'),'recursive'=>'-1','conditions'=>array('Member.status'=>1)));
				   
				   foreach($member as $member)
					{   
						$m_locale = $member['Member']['locale'] ? $member['Member']['locale'] : 'en';
						
						
						$news = $this->Newsletter->find('first',array('conditions'=>array('Newsletter.main_id'=>$news_id,'Newsletter.locale'=>$m_locale)));	
						$email=$member['Member']['email'];
						$final_name=$member['Member']['given_name'];
						$this->loadModel('EmailTemplate');									
						$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>14,'locale'=>$m_locale),'fields'=>array('subject','description','email_from')));						
						$emailData = $emailTemplate['EmailTemplate']['description'];						
						$emailData = str_replace(array('{name}','{title}','{description}'),array($final_name,$news['Newsletter']['title'],$news['Newsletter']['description']),$emailData);
						$emailTo =$email;
						$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
						$emailSubject = $emailTemplate['EmailTemplate']['subject'];
						$emailSubject = str_replace(array('{subject_title}'),array($news['Newsletter']['title']),$emailSubject);	
						$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
						
						
					}
				$this->Session->write('success','Newsletter has been sent successfully');
				$this->redirect(array('controller'=>'users','action'=>'news_letter','admin'=>true));
			 }
		}
	}
	/*********--------------------- Manage Email Start--------------------********** **/
	
	function admin_email()
	{
		$this->layout="admin";
		$this->loadModel('EmailTemplate');
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		$id = array('1','3');
		$conditions = array('EmailTemplate.locale'=>'en','EmailTemplate.id'=>$id);
		$this->paginate = array('order'=>'EmailTemplate.id ASC','limit'=>'10');
		$info = $this->paginate('EmailTemplate',$conditions);
		$this->set(compact('info','page'));
		 if($this->RequestHandler->isAjax())
		{
		   $this->layout="";
		   $this->viewPath = 'Elements'.DS.'adminElements/admin/emailTemplate';
		   $this->render('email_list');
		}
		
	}
	
	function admin_edit_email($id=NULL)
	{
		$this->layout="admin";
		$this->loadModel('EmailTemplate');
		if($id)
		{
			$id = convert_uudecode(base64_decode($id));
			$this->loadModel('EmailTemplate');
			$info = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.id'=>$id)));
			$this->set('info',$info);
			
		}
		
		if(!empty($this->data))
		{
			$this->EmailTemplate->updateAll(array('EmailTemplate.email_to'=>'"'.$this->data['EmailTemplate']['email_to'].'"'),array('EmailTemplate.main_id'=>$this->data['EmailTemplate']['id']));

			$this->Session->write('success','Email has been updated successfully');
			$this->redirect(array('controller'=>'users','action'=>'email','admin'=>true));	
		}
		
		
	}
	
	function validate_email_ajax()
	{
		$this->layout="";
		$this->autoRender=false;		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_email($this->data);						
			if(is_array ($this->data) )	{
				foreach ($this->data['EmailTemplate'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
	/*For searching commissions by author names , emails and commission values */
	function admin_search()
	{
		$this->layout='admin';
		$this->loadModel('Commission');
		$page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		
	if($this->request->data['CommissionSearch']['search_by']==0)
	{
	$memberidlist=array();
	$this->loadModel('Member');
	 $key=$this->request->data['CommissionSearch']['search'];
	 
	$memberid=$this->Member->find('all',array('conditions'=>array('Member.given_name'=>$key)));
	foreach($memberid as $member)
	{
	$memberidlist[]=$member['Member']['id'];
	}
	
	$commissions=$this->Commission->find('all');
	
	foreach($commissions as $commission)
	{
	$commission['Commission']['author_name'];
	$commission['Commission']['author_name']=explode(",",$commission['Commission']['author_name']);
	foreach($commission['Commission']['author_name'] as $author)
	{
	foreach($memberidlist as $memberidlist1)

{	if($memberidlist1==$author)
	{
	$commissionnids[]=$commission['Commission']['id'];
	
}	
	}
	
	}
	

	}

	
		$this->paginate=array('limit'=>'10','order'=>'Commission.id DESC','conditions'=>array('Commission.id'=>$commissionnids)); 
	}
	elseif($this->request->data['CommissionSearch']['search_by']==1)
	{
	$memberidlist=array();
	$this->loadModel('Member');
	$key=$this->request->data['CommissionSearch']['search'];
	$memberid=$this->Member->find('all',array('conditions'=>array('Member.email'=>$key)));
	foreach($memberid as $member)
	{
	$memberidlist[]=$member['Member']['id'];
	}
	$commissions=$this->Commission->find('all');

	foreach($commissions as $commission)
	{
	$commission['Commission']['author_name'];
	$commission['Commission']['author_name']=explode(",",$commission['Commission']['author_name']);
	foreach($commission['Commission']['author_name'] as $author)
	{
	foreach($memberidlist as $memberidlist1)
	{
	if($memberidlist1==$author)
	{
	$commissionnids[]=$commission['Commission']['id'];
	
	}	
	}
	
	}
	
	

	}

	
		$this->paginate=array('limit'=>'10','order'=>'Commission.id DESC','conditions'=>array('Commission.id'=>$commissionnids)); 
	}
	elseif($this->request->data['CommissionSearch']['search_by']==2)
	{
	$key=$this->request->data['CommissionSearch']['search'];
	
	$this->paginate=array('limit'=>'10','order'=>'Commission.id DESC','conditions'=>array('Commission.commission'=>$key)); 
	
	}
	
	
		  $page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
		
		$info=$this->paginate('Commission');
		$this->set(compact('info','page'));
		
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $this->render('commission-search');
		}
	
	}
	
	/* For saving new commissions to database */
	function admin_add_commission()
	{
		App::import('Helper', 'Timezone'); 
		$timezone = new TimezoneHelper(new View(null));
		$this->loadModel('Language');
		$this->layout='admin';
		$this->loadModel('Commission');	
		$this->loadModel('Member');			
		$this->loadModel('Courses');	
		$this->loadModel('EmailTemplate');
		$status="added";
		$courseslist=$this->Courses->find('all',array('conditions'=>array('Courses.isPublished'=>1,'status !='=>2)));
		
		foreach($courseslist as $course)
		{
		$pub_courseids[]=$course['Courses']['m_id'];	
		}
	 
		
		$authors=$this->Member->find('all',array('conditions'=>array('Member.role_id' => '3','Member.id'=>$pub_courseids)));
		
		$members=array();
		foreach($authors as $author)
		{
		$key=$author['Member']['id'];
		$members[$key]=$author['Member']['given_name'];
		}
		
		foreach($courseslist as $course)
		{
		$key1=$course['Courses']['id'];
		$courses[$key1]=$course['Courses']['title'];
		}
		asort($members);
		$this->set('members',$members);
		
		
		if(!empty($this->data))
		{	
		
		if($this->data['Commission']['courses'][0]!='all')
		{
		$data=array();
		
		foreach($this->data['Commission']['courses'] as $key=>$course)
		{
		$coursedata=$this->Courses->find('all',array('conditions'=>array('id'=>$course)));
		$data[$key]['Commission']['commission']=$this->data['Commission']['commission'];				
		$data[$key]['Commission']['author_name']=$coursedata[0]['Courses']['m_id'];
		$data[$key]['Commission']['duration']=$this->data['Commission']['duration'];
		if($this->data['Commission']['duration']==0)
		{
		$data[$key]['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_from'])));
		$data[$key]['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_to'])));
		}
		$data[$key]['Commission']['courses']=1;
		$data[$key]['Commission']['particular-course']=$course;
		$data[$key]['Commission']['date']=$this->data['Commission']['date'];
		$data[$key]['Commission']['package_id']=strtotime(date('Y-m-d H:i:s'));
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$coursedata[0]['Courses']['m_id'])));
		
		
//		if($this->data['Commission']['duration']==0)
//		{
//		
//		/* Locale date begin*/
//				
//		$m_locale = $authoremail['Member']['locale']? $authoremail['Member']['locale']:'en';
//		
//		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
//		
//		$date_from = $this->data['Commission']['date_from'];
//		$date_from = strtotime($date_from);
//		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
//				
//		$date_to = $this->data['Commission']['date_to'];
//		$date_to = strtotime($date_to);
//		
//		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
//						
//		/* Locale date end*/	
//		
//		
//		
//		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
//		$emailData = $emailTemplate['EmailTemplate']['description'];	
//		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$this->data['Commission']['commission'],$coursedata[0]['Courses']['title'],$date_from,$date_to),$emailData);							
//		$emailTo = $authoremail['Member']['email'];
//		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
//		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
//		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
//		
//		}
//		else
//		{
//			
//		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
//		$emailData = $emailTemplate['EmailTemplate']['description'];	
//		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$this->data['Commission']['commission'],$coursedata[0]['Courses']['title']),$emailData);							
//		$emailTo = $authoremail['Member']['email'];
//		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
//		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
//		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);	
//			
//		}
		}
		}
		elseif($this->data['Commission']['courses'][0]=='all')
		{		
                   
		$data=array();					
//		foreach($this->data['Commission']['author_name'] as $key=>$authorname)
//		{	
                    
		$data[$key]['Commission']['commission']=$this->data['Commission']['commission'];				
		$data[$key]['Commission']['author_name']=$this->data['Commission']['author_name'];
		$data[$key]['Commission']['duration']=$this->data['Commission']['duration'];
		if($this->data['Commission']['duration']==0)
		{
		$data[$key]['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_from'])));
		$data[$key]['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['Commission']['date_to'])));
		}
		$data[$key]['Commission']['courses']=0;
		$data[$key]['Commission']['particular-course']='';
		$data[$key]['Commission']['date']=$this->data['Commission']['date'];
                $data[$key]['Commission']['package_id']=strtotime(date('Y-m-d H:i:s'));
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$this->data['Commission']['author_name'])));
		$title='All Courses';
//		if($this->data['Commission']['duration']==0)
//		{
//		/* Locale date begin*/
//				
//		$m_locale = $authoremail['Member']['locale']? $authoremail['Member']['locale']:'en';
//		
//		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
//		
//		$date_from = $this->data['Commission']['date_from'];
//		$date_from = strtotime($date_from);
//		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
//				
//		$date_to = $this->data['Commission']['date_to'];
//		$date_to = strtotime($date_to);
//		
//		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
//						
//		/* Locale date end*/	
//		
//		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
//		$emailData = $emailTemplate['EmailTemplate']['description'];	
//		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$this->data['Commission']['commission'],$title,$date_from,$date_to),$emailData);							
//		$emailTo =$authoremail['Member']['email'];
//		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
//		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
//		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
//		}
//		else
//		{
//		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
//		$emailData = $emailTemplate['EmailTemplate']['description'];	
//		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$this->data['Commission']['commission'],$title),$emailData);							
//		$emailTo =$authoremail['Member']['email'];
//		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
//		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
//		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
//		}
		} 
                
		
		
		$this->Commission->saveAll($data);		
                $this->Session->write('success','Commission has been added successfully');
		$this->redirect(array('action'=>'commission','admin' => true));			
		
	}
		

}
	
	 
	function validate_email($data)
	{			
		$errors = array ();		
		if(trim($data['EmailTemplate']['email_to'] == '')){
			$errors['email_to'][] = FIELD_REQUIRED."\n";
		}
		elseif($this->validEmail(trim($data['EmailTemplate']['email_to'])) =='false'){
			$errors['email_to'][] = INVALID_EMAIL."\n";
		}		
		return $errors;			
	}
	
	/* For checking existing commissions for specific date range and commission value */
	
        function admin_commissioncheck(){
            
                $this->loadModel('Member');	
		$this->loadModel('Commission');
		$this->loadModel('Courses');
                                
		                
		$error='';
		$authorid=$this->data['author'];
		
		$particulars=explode(",",$this->data['particular']);	
			
		if($this->data['course']!='all' && $this->data['duration']==0)
		{
                   
                    if($this->data['date_from']!='' && $this->data['date_to']!='')
                    {
                        $date_from = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_from'])));	
                        $date_to = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_to'])));
                         $particulars=join(',',$particulars); 
                  
                        $results=$this->Commission->query("SELECT * FROM `commissions` WHERE (author_name=".$authorid.") and ((`date_from`>='".$date_from."' and `date_from`<='".$date_to."' and duration=0) OR (`date_to`>='".$date_from."' and `date_to`<='".$date_to."' and duration=0) OR (`date_from`<'".$date_from."' and `date_to`>'".$date_to."' and duration=0) OR duration=1) and ((`particular-course` IN(".$particulars.") and courses=1) OR courses=0 ) group by `courses`,`particular-course`");
                       
                foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                
                }
		
		}
		else if($this->data['course']!='all' && $this->data['duration']==1)
                {	
                $particulars=join(',',$particulars); 
               
		$results=$this->Commission->query("SELECT * FROM `commissions` WHERE (author_name=".$authorid.") and (duration=0 OR duration=1) and ((`particular-course` IN(".$particulars.") and courses=1) OR courses=0 ) group by `courses`,`particular-course`");
                 foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                  
                }
                
                else if($this->data['course']=='all' && $this->data['duration']==0)
		{
                if($this->data['date_from']!='' && $this->data['date_to']!=''){
			 
		$date_from = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_from'])));	
		$date_to = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_to'])));
			

		$results=$this->Commission->query("SELECT * FROM `commissions` WHERE (author_name=".$authorid.") and ((`date_from`>='".$date_from."' and `date_from`<='".$date_to."' and duration=0) OR (`date_to`>='".$date_from."' and `date_to`<='".$date_to."' and duration=0) OR (`date_from`<'".$date_from."' and `date_to`>'".$date_to."' and duration=0) OR duration=1) and (courses=1 OR courses=0) group by `courses`,`particular-course`");
                foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                
                }
		}
		else if($this->data['course']=='all' && $this->data['duration']==1)
		{	
                
		$results=$this->Commission->query("SELECT * FROM `commissions` WHERE (author_name=".$authorid.") and (duration=0 OR duration=1) and (courses=1 OR courses=0) group by `courses`,`particular-course`");
                foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                
		}		
		echo $error; 
		exit;
        }
	
	 function admin_commissioncheck_edit()
	{
		
		$this->loadModel('Member');	
		$this->loadModel('Commission');
		$this->loadModel('Courses');
                $package_id=$this->data['package_id'];
                
		$data=$this->Commission->find('all',array('conditions'=>array('package_id !='=>$package_id)));
                
		$error='';
		$authorid=$this->data['author'];
		//$newauthorslist=$authors;
		$particulars=explode(",",$this->data['particular']);	
			
		if($this->data['course']!='all' && $this->data['duration']==0)
		{
                    if($this->data['date_from']!='' && $this->data['date_to']!='')
                    {
                        $date_from = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_from'])));	
                        $date_to = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_to'])));
                        $particulars=join(',',$particulars); 
                        //id IN(SELECT id FROM `commissions` WHERE package_id !=".$package_id." and author_name=".$authorid.")
                   
                        $results=$this->Commission->query("SELECT * FROM `commissions` WHERE (package_id !=".$package_id." and author_name=".$authorid.") and ((`date_from`>='".$date_from."' and `date_from`<='".$date_to."' and duration=0) OR (`date_to`>='".$date_from."' and `date_to`<='".$date_to."' and duration=0) OR (`date_from`<'".$date_from."' and `date_to`>'".$date_to."' and duration=0) OR duration=1) and ((`particular-course` IN(".$particulars.") and courses=1) OR courses=0 ) group by `courses`,`particular-course`");
                foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                
                }
		
		}
		else if($this->data['course']!='all' && $this->data['duration']==1)
                {	
                $particulars=join(',',$particulars); 
               
		$results=$this->Commission->query("SELECT * FROM `commissions` WHERE (package_id !=".$package_id." and author_name=".$authorid.") and (duration=0 OR duration=1) and ((`particular-course` IN(".$particulars.") and courses=1) OR courses=0 ) group by `courses`,`particular-course`");
                 foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                  
                }
                
                else if($this->data['course']=='all' && $this->data['duration']==0)
		{
                if($this->data['date_from']!='' && $this->data['date_to']!=''){
			 
		$date_from = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_from'])));	
		$date_to = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_to'])));
			
                      
		$results=$this->Commission->query("SELECT * FROM `commissions` WHERE (package_id !=".$package_id." and author_name=".$authorid.") and ((`date_from`>='".$date_from."' and `date_from`<='".$date_to."' and duration=0) OR (`date_to`>='".$date_from."' and `date_to`<='".$date_to."' and duration=0) OR (`date_from`<'".$date_from."' and `date_to`>'".$date_to."' and duration=0) OR duration=1) and (courses=1 OR courses=0) group by `courses`,`particular-course`");
                foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                
                }
		}
		else if($this->data['course']=='all' && $this->data['duration']==1)
		{	
              
		$results=$this->Commission->query("SELECT * FROM `commissions` WHERE (package_id !=".$package_id." and author_name=".$authorid.") and (duration=0 OR duration=1) and (courses=1 OR courses=0) group by `courses`,`particular-course`");
                foreach($results as $result)
                {         
                    $membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$authorid),'fields'=>array('given_name')));
                     
                    if($result['commissions']['courses']==1)
                    {
                        
                        $coursename=$this->Courses->query("select `title` from `courses` where `id`=".$result['commissions']['particular-course']." and `m_id`=".$authorid);
                        
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and course ".$coursename[0]['courses']['title']."*</p><br/>";   
                    }
                    else
                    {
                        $error.="<p style='color: red;'>* You have already added commission for selected date range for author ".$membername['Member']['given_name']." and all courses *</p><br/>";
                    }

		}
                
		}		
		echo $error; 
		exit;
		}
                
	/*	For getting all author names to display in drop-down */

	function admin_getallauthors() 
	{
	$authors=$this->data;
		
	$this->loadModel('Courses');	
	$this->loadModel('Member');	
	$option='';
	foreach($authors['authors'] as $author)
	{
	
	$member=$this->Member->find('all',array('conditions'=>array('Member.id'=>$author),'fields'=>array('given_name')));
	$courses=$this->Courses->find('all',array('conditions'=>array('isPublished'=>1,'status !='=>2,'m_id'=>$author),'fields'=>array('id','title')));
	if(!empty($courses))
	{
	$option.="<optgroup label='".$member[0]['Member']['given_name']."'>";	
	
	asort($courses);
	
	foreach($courses as $key=>$course)
	{
		$key=$course['Courses']['id'];
		$newcourse=$course['Courses']['title'];
	$option.="<option value='".$key."'>".$newcourse."</option>"; 
	}
	$option.="</optgroup>";
	}
	}
	echo $option;
	exit;    
	}
	/* For checking member ids for courses*/
	function admin_checkmembers()
	{
	$this->loadModel('Courses');	
	$courses=$this->data;
	$courses =explode(",",$courses['courseids']);
	foreach($courses as $course)
	{
	$memberids=$this->Courses->find('all',array('conditions'=>array('Courses.id'=>$course),'fields'=>array('m_id')));
	$allids[]=$memberids[0]['Courses']['m_id'];
	}
	echo json_encode($allids); 
	exit;
	}
	/* For checking courses for specific members */
	function admin_checkcourses()
	{
		$member=$this->data;
		$this->loadModel('Courses');
			
		$courseexists=$this->Courses->find('first',array('conditions'=>array('isPublished'=>1,'status'=>1,'Courses.m_id'=>$member['memberids'])));
		
	if(count($courseexists)>0){
		echo $flag=1;
		
		
		}
		else
		{
			echo $flag=0;
			}
	exit;	
	}
	
	/* For selecting member names */
	function admin_checkmembernames()
	{
		$ids=$this->data;
		$this->loadModel('Member');
		$membernames=$this->Member->find('all',array('conditions'=>array('Member.id'=>$ids['memberids']),'fields'=>array('given_name')));
		echo $membernames[0]['Member']['given_name'];
	    exit;	
	    }
	/* Checking if existing record for particular commission or particular date range exists then display error on edit commission  */
	function admin_modifycheck(){
	$this->loadModel('Commission');
	$this->loadModel('Member');
	$this->loadModel('Courses');
	
		
	$commissionids=json_decode($this->data['selectedids']);
	$commissionids=join(",",$commissionids);
	$commissions=$this->Commission->query("Select * from `commissions` where `id` IN(".$commissionids.")");	
	$date_from = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_from'])));	
	$date_to = date('Y-m-d', strtotime(str_replace('-', '/', $this->data['date_to'])));
    $error='';
	global $particular;
	global $all;
	global $particular1;
	global $all1;
    $all=0;
	foreach($commissions as $commission)
	{	  
	if($commission['commissions']['duration']==0 && $commission['commissions']['courses']==0)
	{
	if($this->data['duration']==0)
	{
	$foundcommissions=$this->Commission->query("Select * from `commissions` where `author_name`=".$commission['commissions']['author_name']." and ((`date_from`<='".$date_from."' and `date_to`>='".$date_from."') OR (`date_from`<='".$date_to."' and `date_to`>='".$date_to."') OR (`date_from`>'".$date_from."' and `date_to`<'".$date_to."')) and `date_from`!='1970-01-01' and `id` !=".$commission['commissions']['id']);	
	if(!empty($foundcommissions))
	{
	
	foreach($foundcommissions as $foundcommission)
	{
	if($foundcommission['commissions']['particular-course']!='')
	{
	$membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$foundcommission['commissions']['author_name'])));
	$coursename=$this->Courses->query("select `title` from `courses` where `id`=".$foundcommission['commissions']['particular-course']." and `m_id`=".$foundcommission['commissions']['author_name']);
	if(in_array($foundcommission['commissions']['particular-course'],$particular)==false)
	{
	$error.='<p style="color:red;">* You have already added commission for selected date range for author '.$membername['Member']['given_name'].' and course '.$coursename[0]['courses']['title'].". *</p><br />";
	}
	$particular[]=$foundcommission['commissions']['particular-course'];
	}
	else
	{
		
		$membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$foundcommission['commissions']['author_name'])));
		if($all!=$foundcommission['commissions']['author_name'])
	{
	$error.='<p style="color:red;">*You have already added commission for selected date range for author '.$membername['Member']['given_name'] ." and all courses. *</p><br />";
	}
	$all=$foundcommission['commissions']['author_name'];
	}
	}
	}
	}
	else
	{
	
	$foundcommissions=$this->Commission->query("Select * from `commissions` where `author_name`=".$commission['commissions']['author_name']." and (`courses`=0 && `duration`=0) and `id` !=".$commission['commissions']['id']);	
	if(empty($foundcommissions))
	{
	$foundcommissions=$this->Commission->query("Select * from `commissions` where `author_name`=".$commission['commissions']['author_name']." and (`courses`=1 && `duration`=0) and `id` !=".$commission['commissions']['id']);	
	
	foreach($foundcommissions as $foundcommission)
	{
	
	$membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$foundcommission['commissions']['author_name'])));
	$coursename=$this->Courses->query("select `title` from `courses` where `id`=".$foundcommission['commissions']['particular-course']." and `m_id`=".$foundcommission['commissions']['author_name']);
	if(in_array($foundcommission['commissions']['particular-course'],$particular)==false)
	{
	$error.='<p style="color:red;">* You have already added commission for selected date range for author '.$membername['Member']['given_name'].' and course '.$coursename[0]['courses']['title'].". *</p><br />";
	}
	$particular[]=$foundcommission['commissions']['particular-course'];
	}
	}
	else
	{
		foreach($foundcommissions as $foundcommission)
	{
		
		$membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$foundcommission['commissions']['author_name'])));
		if($all!=$foundcommission['commissions']['author_name'])
	{
	$error.='<p style="color:red;">* You have already added commission for selected date range for author '.$membername['Member']['given_name'] ." and all courses. *</p><br />";
	}
	$all=$foundcommission['commissions']['author_name'];
	}
	}
	}
	
	
	}
	
	elseif($commission['commissions']['duration']==0 && $commission['commissions']['courses']==1)
	{
	if($this->data['duration']==0)
	{
			
	$foundallcommissions=$this->Commission->query("Select * from `commissions` where `author_name`=".$commission['commissions']['author_name']." and ((`date_from`<='".$date_from."' and `date_to`>='".$date_from."') OR (`date_from`<='".$date_to."' and `date_to`>='".$date_to."') OR (`date_from`>'".$date_from."' and `date_to`<'".$date_to."')) and `particular-course`='' and `date_from`!='1970-01-01' and `id` !=".$commission['commissions']['id']);	
	if(empty($foundallcommissions))
	{
		
		$foundcommissions=$this->Commission->query("Select * from `commissions` where `author_name`=".$commission['commissions']['author_name']." and ((`date_from`<='".$date_from."' and `date_to`>='".$date_from."') OR (`date_from`<='".$date_to."' and `date_to`>='".$date_to."') OR (`date_from`>'".$date_from."' and `date_to`<'".$date_to."')) and `particular-course`=".$commission['commissions']['particular-course']." and `date_from`!='1970-01-01' and `id` !=".$commission['commissions']['id']);
	
 	foreach($foundcommissions as $foundcommission)
	{
	$membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$foundcommission['commissions']['author_name'])));
	$coursename=$this->Courses->query("select `title` from `courses` where `id`=".$foundcommission['commissions']['particular-course']." and `m_id`=".$foundcommission['commissions']['author_name']);
	if(in_array($foundcommission['commissions']['particular-course'],$particular)==false) 
	{ 
	$error.='<p style="color:red;">* You have already added commission for selected date range for author '.$membername['Member']['given_name'].' and course '.$coursename[0]['courses']['title'].". *</p><br />";
	}
	$particular[]=$foundcommission['commissions']['particular-course'];
	}
	}
	else
	{
	foreach($foundallcommissions as $foundcommission)
	{		
	$membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$foundcommission['commissions']['author_name'])));
	if($all!=$foundcommission['commissions']['author_name'])
	{
	$error.='<p style="color:red;">* You have already added commission for selected date range for author '.$membername['Member']['given_name'] ." and all courses. *</p><br />";
	}
	$all=$foundcommission['commissions']['author_name'];
	}
	}
	}
	
	else
	{
		
	$foundcommissions=$this->Commission->query("Select * from `commissions` where `author_name`=".$commission['commissions']['author_name']." and `particular-course`=".$commission['commissions']['particular-course']." and `duration`=0 and `id` !=".$commission['commissions']['id']);	
	foreach($foundcommissions as $foundcommission)
	{
	$membername=$this->Member->find('first',array('conditions'=>array('Member.id'=>$foundcommission['commissions']['author_name'])));
	$coursename=$this->Courses->query("select `title` from `courses` where `id`=".$foundcommission['commissions']['particular-course']." and `m_id`=".$foundcommission['commissions']['author_name']);
	if(in_array($foundcommission['commissions']['particular-course'],$particular)==false) 
	{ 
	$error.='<p style="color:red;">* You have already added commission for selected date range for author '.$membername['Member']['given_name'].' and course '.$coursename[0]['courses']['title'].". *</p><br />";
	}
	$particular[]=$foundcommission['commissions']['particular-course'];
	}
	}
	}
	}
	echo $error;
	exit;
		}
	
	
	/* For modifying only commissions for all selections */
	function admin_modifycommissions1(){
		
		App::import('Helper', 'Timezone'); 
	$timezone = new TimezoneHelper(new View(null));
	$this->loadModel('Language');
	$this->loadModel('Commission');
	$this->loadModel('Course');
	$this->loadModel('Member');
	$this->loadModel('EmailTemplate');
	$status="edited";
	foreach(json_decode($this->data['modifyid']) as $modifyid)
	{
		
		$this->request->data['Commission']['commission']=$this->data['commissionrate'];	
		
		$this->request->data['Commission']['id']=$modifyid;
		$coursetype=$this->Commission->find('all',array('conditions'=>array('Commission.id'=>$modifyid)));
		
		$data=$this->data;
		$commissiondata=$this->Commission->find('first',array('conditions'=>array('Commission.id'=>$modifyid)));
			
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$commissiondata['Commission']['author_name'])));
		
		if($coursetype[0]['Commission']['courses']==0 && $coursetype[0]['Commission']['duration']==1)
		{
		$title='All Courses';
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$title),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($coursetype[0]['Commission']['courses']==1 && $coursetype[0]['Commission']['duration']==1)
		{
		$coursecomid=$this->Commission->find('all',array('conditions'=>array('Commission.id'=>$modifyid),'fields'=>array('particular-course')));
		$coursetitleid=$coursecomid[0]['Commission']['particular-course'];
		$coursetitle=$this->Course->find('all',array('conditions'=>array('Course.id'=>$coursetitleid)));
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$coursetitle[0]['Course']['title']),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($coursetype[0]['Commission']['courses']==0 && $coursetype[0]['Commission']['duration']==0)
		{
		$status="edited";
		$title='All Courses';
		$coursecomid=$this->Commission->find('all',array('conditions'=>array('Commission.id'=>$modifyid)));
		
		/* Locale date begin*/ 
				
		$m_locale = $authoremail['Member']['locale'] ? $authoremail['Member']['locale']:'en';
		
		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
		
		$date_from =$coursecomid[0]['Commission']['date_from'];
		$date_from = strtotime($date_from);
		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
				
		$date_to = $coursecomid[0]['Commission']['date_to'];
		$date_to = strtotime($date_to);
		
		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
						
		/* Locale date end*/	
		
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$title,$date_from,$date_to),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($coursetype[0]['Commission']['courses']==1 && $coursetype[0]['Commission']['duration']==0)
		{
		$coursecomid=$this->Commission->find('all',array('conditions'=>array('Commission.id'=>$modifyid)));
		$coursetitleid=$coursecomid[0]['Commission']['particular-course'];
		$coursetitle=$this->Course->find('all',array('conditions'=>array('Course.id'=>$coursetitleid)));
		
		/* Locale date begin*/
		
		
		$m_locale = $authoremail['Member']['locale'] ? $authoremail['Member']['locale']:'en';
				
		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
		
		$date_from =$coursecomid[0]['Commission']['date_from'];
		$date_from = strtotime($date_from);
		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
				
		$date_to = $coursecomid[0]['Commission']['date_to'];
		$date_to = strtotime($date_to);
		
		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
						
		/* Locale date end*/	
		
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$coursetitle[0]['Course']['title'],$date_from,$date_to),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
			
			
			
		$this->Commission->save($data);
	}
	
	$this->redirect(array('action'=>'commission','admin' => true));		
	
}
	
	/* For modifying commissions and dates for all selections */
		
	function admin_modifycommissions()
	{
	App::import('Helper', 'Timezone'); 
	$timezone = new TimezoneHelper(new View(null));
	$this->loadModel('Language');
	$this->loadModel('Commission');
	$this->loadModel('Courses');
	$this->loadModel('Member');
	$this->loadModel('EmailTemplate');
	$status="edited";
	foreach(json_decode($this->data['modifyid']) as $modifyid)
	{
		
	    $this->request->data['Commission']['commission']=$this->data['commissionrate'];
		$this->request->data['Commission']['duration']=$this->data['duration'];
		$this->request->data['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['datepicker'])));
		$this->request->data['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['datepicker1'])));			      
		$this->request->data['Commission']['id']=$modifyid;
		$data=$this->data;
		$commissiondata=$this->Commission->find('first',array('conditions'=>array('Commission.id'=>$modifyid)));
		if($commissiondata['Commission']['courses']==0 && $this->data['duration']==0)
		{
			
			
			
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$commissiondata['Commission']['author_name'])));
		
		$title="All Courses";
		
		/* Locale date begin*/
		
		
		$m_locale = $authoremail['Member']['locale']? $authoremail['Member']['locale']:'en';
		
		
		
		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
		
		$date_from = $this->data['Commission']['date_from'];
		$date_from = strtotime($date_from);
		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
				
		$date_to = $this->data['Commission']['date_to'];
		$date_to = strtotime($date_to);
		
		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
						
		/* Locale date end*/	
		
		
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$title,$date_from,$date_to),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($commissiondata['Commission']['courses']==1 && $this->data['duration']==0)
		{
		$coursedata=$this->Courses->find('all',array('conditions'=>array('Courses.id'=>$commissiondata['Commission']['particular-course'])));
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$coursedata[0]['Courses']['m_id'])));
		
		/* Locale date begin*/
				
		$m_locale = $authoremail['Member']['locale']? $authoremail['Member']['locale']:'en';
		
		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
		
		$date_from = $this->data['Commission']['date_from'];
		$date_from = strtotime($date_from);
		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
				
		$date_to = $this->data['Commission']['date_to'];
		$date_to = strtotime($date_to);
		
		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
						
		/* Locale date end*/	
		
		
		
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$coursedata[0]['Courses']['title'],$date_from,$date_to),$emailData);	
		$emailTo = $authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($commissiondata['Commission']['courses']==0 && $this->data['duration']==1)
		{
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$commissiondata['Commission']['author_name'])));
		$title="All Courses";
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$title),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($commissiondata['Commission']['courses']==1 && $this->data['duration']==1)         
		{
		$coursedata=$this->Courses->find('all',array('conditions'=>array('Courses.id'=>$commissiondata['Commission']['particular-course'])));
		
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$coursedata[0]['Courses']['m_id'])));
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));
		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$this->data['commissionrate'],$coursedata[0]['Courses']['title']),$emailData);							
		$emailTo = $authoremail['Member']['email'];
		
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		
		
			
			
		$this->Commission->save($data);
	}
	
	$this->redirect(array('action'=>'commission','admin' => true));		
	
	}
	
	/* For modifying dates for all selections */
	function admin_modifycommissions2()
	{
	
	App::import('Helper', 'Timezone'); 
	$timezone = new TimezoneHelper(new View(null));
	$this->loadModel('Language');	
	$this->loadModel('Commission');
	$this->loadModel('Courses');
	$this->loadModel('Member');
	$this->loadModel('EmailTemplate');
	$status="edited";
	foreach(json_decode($this->data['modifyid']) as $modifyid)
	{
		$this->request->data['Commission']['duration']=$this->data['duration'];
		$this->request->data['Commission']['date_from']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['datepicker2'])));
		$this->request->data['Commission']['date_to']=date('Y-m-d', strtotime(str_replace('-', '/', $this->data['datepicker3'])));		
		$this->request->data['Commission']['id']=$modifyid;
		$data=$this->data;
		$commissiondata=$this->Commission->find('first',array('conditions'=>array('Commission.id'=>$modifyid)));
				
		if($commissiondata['Commission']['courses']==0 && $this->data['duration']==0)
		{
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$commissiondata['Commission']['author_name'])));
		$commissionedited=$this->Commission->find('first',array('conditions'=>array('Commission.id'=>$modifyid)));
		$title="All Courses";
		/* Locale date begin*/
				
		$m_locale = $authoremail['Member']['locale']? $authoremail['Member']['locale']:'en';
		
		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
		
		$date_from = $this->data['Commission']['date_from'];
		$date_from = strtotime($date_from);
		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
				
		$date_to = $this->data['Commission']['date_to'];
		$date_to = strtotime($date_to);
		
		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
						
		/* Locale date end*/	
		
		
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$commissionedited['Commission']['commission'],$title,$date_from,$date_to),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($commissiondata['Commission']['courses']==1 && $this->data['duration']==0)
		{
		$coursedata=$this->Courses->find('all',array('conditions'=>array('Courses.id'=>$commissiondata['Commission']['particular-course'])));
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$coursedata[0]['Courses']['m_id'])));
		$commissionedited=$this->Commission->find('first',array('conditions'=>array('Commission.id'=>$modifyid)));
		/* Locale date begin*/
				
		$m_locale = $authoremail['Member']['locale']? $authoremail['Member']['locale']:'en';
		
		$language=$this->Language->find('all',array('conditions'=>array('Language.locale'=>$m_locale),'fields'=>array('lang_code')));
		
		$date_from = $this->data['Commission']['date_from'];
		$date_from = strtotime($date_from);
		$date_from=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_from);  
				
		$date_to = $this->data['Commission']['date_to'];
		$date_to = strtotime($date_to);
		
		$date_to=$timezone ->dateFormatAccoringLocaleLangNew($language[0]['Language']['lang_code'],$date_to);  
						
		/* Locale date end*/
		
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>16,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}','{date_from}','{date_to}'),array($authoremail['Member']['given_name'],$commissionedited['Commission']['commission'],$coursedata[0]['Courses']['title'],$date_from,$date_to),$emailData);	
		$emailTo = $authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
				
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($commissiondata['Commission']['courses']==0 && $this->data['duration']==1)
		{
		$titte="All Courses";
		$commissionedited=$this->Commission->find('first',array('conditions'=>array('Commission.id'=>$modifyid)));
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$commissiondata['Commission']['author_name'])));
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$commissionedited['Commission']['commission'],$title),$emailData);							
		$emailTo =$authoremail['Member']['email'];
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		elseif($commissiondata['Commission']['courses']==1 && $this->data['duration']==1)         
		{
		$coursedata=$this->Courses->find('all',array('conditions'=>array('Courses.id'=>$commissiondata['Commission']['particular-course'])));
		$commissionedited=$this->Commission->find('first',array('conditions'=>array('Commission.id'=>$modifyid)));
		$authoremail=$this->Member->find('first',array('conditions'=>array('Member.id' =>$coursedata[0]['Courses']['m_id'])));
		$emailTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('main_id'=>15,'locale'=>$authoremail['Member']['locale']),'fields'=>array('subject','description','email_from','email_to')));
		
		$emailData = $emailTemplate['EmailTemplate']['description'];	
		$emailData = str_replace(array('{final_name}','{commission}','{course_title}'),array($authoremail['Member']['given_name'],$commissionedited['Commission']['commission'],$coursedata[0]['Courses']['title']),$emailData);							
		$emailTo = $authoremail['Member']['email'];
		
		$emailFrom = $emailTemplate['EmailTemplate']['email_from'];
		$emailSubject = $emailTemplate['EmailTemplate']['subject'];	
		$this->send_email($emailTo,$emailFrom,$emailSubject,$emailData);
		}
		$this->Commission->save($data);
	}
	
	$this->redirect(array('action'=>'commission','admin' => true));		
	
	}
	
	
	function admin_add_commission1()
	{
	$this->loadModel('Defaultcommission');
       
	$data['Defaultcommission']['commission']=$this->data['Defaultcommission']['commission'];
	$data['Defaultcommission']['date']=date('Y-m-d');
	$data['Defaultcommission']['id']=1;
	$this->Defaultcommission->save($data);	
	$this->redirect(array('action'=>'commission','admin' => true));		
	}
             
        function admin_add_default_commission()
	{
	$this->loadModel('Defaultcommission');
        $this->layout='admin';
        $defaultcommission=$this->Defaultcommission->find('all',array('conditions'=>array('Defaultcommission.id'=>1)));
        $defaultcom=$defaultcommission[0]['Defaultcommission']['commission'];
        $this->set(compact('defaultcom'));
		
		if(!empty($this->data)){
                 //   print_r($this->data);
                   // die;
	$data['Defaultcommission']['commission']=$this->data['Defaultcommission']['commission'];
	$data['Defaultcommission']['date']=date('Y-m-d');
	$data['Defaultcommission']['id']=1;
	$this->Defaultcommission->save($data);	
        $this->Session->write('success','Default commission has been updated successfully');
	$this->redirect(array('action'=>'add_default_commission','admin' => true));		
	}
        }

	function admin_add_banners(){
	$this->loadModel('Banners');
		$this->layout='admin';
	
		
		if(!empty($this->data)){
			
					$image='image_'.$this->data['Banners']['locale'];
					$expath=explode('.',$this->data['Banners'][$image]['name']);
					$extsarr=end($expath);
					$target=time().'-'.$this->data['Banners'][$image]['name'];
					$realpath= realpath('../../app/webroot/img/front').'/';	
					if(!is_dir($realpath.$this->data['Banners']['lang_code']))	
					{
					mkdir($realpath.$this->data['Banners']['lang_code'],0777);					
					}
					$destination=$realpath.$this->data['Banners']['lang_code']."/";
					$file = $this->data['Banners'][$image];						
					$result = $this->Upload->upload($file, $destination, $target, array('type' => 'resizecrop','size'=>array('1140','272'),'quality'=>'100'),array('jpg','jpeg','gif','png'));
					
					App::import('Component', 'Aws');
    				$this->Aws = new AwsComponent();
					$key=$this->data['Banners']['lang_code']."/".$target;
					$result_aws = $this->Aws->uploadFile($key,BANNER_BUCKET, $destination.$target);
					@unlink($destination.$target);
					$order='order_'.$this->data['Banners']['locale'];
					$this->request->data['Banners']['image'] = $target;				
					$this->request->data['Banners']['locale']=$this->data['Banners']['locale'];
					$this->request->data['Banners']['date_created'] = date('Y-m-d H:i:s');		
					$this->request->data['Banners']['order']=$this->data['Banners'][$order];
					$this->request->data['Banners']['lang_code']=$this->data['Banners']['lang_code'];
					$this->request->data['Banners']['publish']=0;
				}
					$this->Banners->save($this->data); 
				$this->Session->write('success','Banner has been added successfully');
				$this->redirect(array('action'=>'banners','admin' => true,''=>$this->data['Banners']['keylang']));
			
										
		}
		//lines start for banner
		
	function admin_banners($param=Null)
	{
		$this->loadModel('Banners');			
		$this->layout='admin';
		$data='en';
		$this->set('localeset',$data);
		$this->render('admin_banners');
		
		
	
	
	}
	function admin_deletebanner()
	{
	$this->loadModel('Bannerdefaults');
	$this->loadModel('Banners');
	App::import('Component', 'Aws');
	$this->Aws = new AwsComponent();
	if($this->request->is('ajax'))
	{	
	$bannerimage=$this->Banners->find('first',array('conditions'=>array('locale'=>$this->data['locale'],'order'=>$this->data['order'])));
	$old_image=$bannerimage['Banners']['lang_code'].'/'.$bannerimage['Banners']['image'];
	
	
	$this->Aws->deleteFile(BANNER_BUCKET, $old_image);
	
	$recorddelted=$this->Banners->deleteAll(array('locale'=>$this->data['locale'],'order'=>$this->data['order']));
	$recordlocale=$this->Banners->find('all',array('conditions'=>array('locale'=>$this->data['locale'])));
	
	if(empty($recordlocale))
	{
	$defaultid=$this->Bannerdefaults->find('first',array('conditions'=>array('language'=>$this->data['locale'])));
	if(!empty($defaultid))
	{
		$this->request->data['Bannerdefaults']['id']=$defaultid['Bannerdefaults']['id'];
		$this->request->data['Bannerdefaults']['default']=0;
	$this->request->data['Bannerdefaults']['language']=$this->data['locale'];
	}else
	{
		$this->request->data['Bannerdefaults']['default']=0;
	$this->request->data['Bannerdefaults']['language']=$this->data['locale'];
	}	
	
	
	$this->Bannerdefaults->save($this->data);
	}
	}
	if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $data=$this->data['locale'];
			 $this->set('localeset',$data);
			 $this->render('banners_list');
		}
	}
	
	function admin_publishbanner()
	{
	$this->loadModel('Banners');
	if($this->request->is('ajax'))
	{
	if($this->data['value']==1)
	{	
	
	$count=$this->Banners->find('all',array('conditions'=>array('locale'=>$this->data['locale'],'publish'=>$this->data['value'])));	
	}
	if(count($count)==5)
	{
	echo "1";
	}
	else
	{
	$recorddelted=$this->Banners->query("Update `banners` set `publish`=".$this->data['value']." where `id`=".$this->data['id']);
	echo $recorddelted; 
	}
	}
	exit;
	}
	
	
	function admin_updateorder()
	{
		
		
		
	$this->loadModel('Banners');
	
	if($this->request->is('ajax'))
	{
	//$this->Notification->updateAll(array('Notification.notification_sender_id'=>$mem_id),array('Notification.notification_sender_id'=>$temp_id));
	if(isset($this->data['previd']))
	{
	$recordupdated=$this->Banners->query("Update `banners` set `order`=".$this->data['previd']." where `locale`='".$this->data['locale']."' and `id`=".$this->data['dataid']);
	$recordupdated=$this->Banners->query("Update `banners` set `order`=".$this->data['id']." where `locale`='".$this->data['locale']."' and `id`=".$this->data['dataid1']);
	
	}
	else
	{
	$recordupdated=$this->Banners->query("Update `banners` set `order`=".$this->data['nextid']." where `locale`='".$this->data['locale']."' and `id`=".$this->data['dataid']);
	$recordupdated=$this->Banners->query("Update `banners` set `order`=".$this->data['id']." where `locale`='".$this->data['locale']."' and `id`=".$this->data['dataid1']);
	
	
	
	}
	
	}
	if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/cmsPages';
			 $data=$this->data['locale'];
			 
			 $this->set('localeset',$data);
			 $this->render('banners_list');
			 
		}
	}
	
	function admin_bannerdefault()
	{
		$this->loadModel('Bannerdefaults');
		
		if($this->request->is('ajax'))
	{
	
	$defaultid=$this->Bannerdefaults->find('first',array('conditions'=>array('language'=>$this->data['lang'])));
	if(!empty($defaultid))
	{
		$this->request->data['Bannerdefaults']['id']=$defaultid['Bannerdefaults']['id'];
		$this->request->data['Bannerdefaults']['language']=$this->data['lang'];
		$this->request->data['Bannerdefaults']['default']=$this->data['default'];
	}else
	{
		$this->request->data['Bannerdefaults']['language']=$this->data['lang'];
		$this->request->data['Bannerdefaults']['default']=$this->data['default'];
	}
		$this->Bannerdefaults->save($this->data);
		
		
	}
	exit;
}
	
	//liners end for banner

//---- function for Listing meatag---//	
	function admin_meta_tags()
	{
          
               $this->loadModel('MetaTag');
		$this->layout='admin';
                $page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
                $this->paginate=array('order'=>'MetaTag.id DESC'); 
		$info=$this->paginate('MetaTag');
               // print_r($info);exit;
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/metaTags';
			 $this->render('meta_tag_list');
		}
	}
//---- end of  function for Listing meatag---//
        
        
        //---- strat of  function for adding meatag---//
	function admin_add_meta_tags($id=NULL)
	{
		$this->layout='admin';
		$this->loadModel('MetaTag');
		
		if(!empty($this->data)){
		$error = $this->validate_metatags($this->data);
                
			if(count($error) == 0){	
                            $url = $this->request->data['MetaTag']['page_url'];
                            $parse = parse_url($url);
                            $replace_str=$parse['host'];
                            $website_url=str_replace(array('https://'.$replace_str.'/kuulum/','http://'.$replace_str.'/kuulum/'),array('',''),$url);
                            $this->request->data['MetaTag']['page_url']=$website_url;
                                                    
                              /*  if($this->MetaTag->save($this->data)){
                                    echo 'success';
                                }else{
                                    echo 'no';
                                }
                                exit;*/
                               $this->MetaTag->save($this->data);
				$this->Session->write('success','Meta tags has been added successfully');
				$this->redirect(array('action'=>'meta_tags','admin' => true));
			}
			else
			{		
				
				$this->set(compact('error','contents'));		
			}										
		}
	}
         //---- end of  function for adding meatag---//
        function isValidURL1($url) {
	  
            return preg_match('%^((https?://)|(www\.)|([a-zA-Z0-9]{0,10}\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i',$url);

	}
         //---- strat of  function for validating meatag---//
        function validate_metatags($data)
	 {	
		$locale=$data['MetaTag']['locale'];
		$errors = array ();     
                
                if (empty($data['MetaTag']['page_title'])){
			$errors['page_title'][] = __(FIELD_REQUIRED)."\n";
		}
		if (trim($data['MetaTag']['page_url']) == ""){
			$errors['page_url'][] = __(FIELD_REQUIRED)."\n";                
                      }else if(trim($data['MetaTag']['page_url']) != "" && !$this->isValidURL1(trim($data['MetaTag']['page_url']))) {
                        
                    $errors['page_url'][] = __("Invalid Page URL")."\n";;
                  }else if(trim($data['MetaTag']['page_url']) != ""){
                      $this->loadModel('MetaTag');
                            $url = $data['MetaTag']['page_url'];
                            $parse = parse_url($url);
                            $replace_str=$parse['host'];
                            $website_url=str_replace(array('https://'.$replace_str.'/kuulum/','http://'.$replace_str.'/kuulum/'),array('',''),$url);
                            $url=$website_url;
			$chkUrl = $this->MetaTag->find('count',array('conditions'=>array('MetaTag.page_url'=>trim($url))));
			if($chkUrl > 0){
				$errors['page_url'][] = __('Page URL already exists')."\n";
			}
                  }
		
		/*if (empty($data['MetaTag']['meta_title_'.$locale])){
			$errors['meta_title_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
		if (empty($data['MetaTag']['meta_keywords_'.$locale])){
			$errors['meta_keywords_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
                if (trim($data['MetaTag']['meta_description_'.$locale]) == ""){
			$errors['meta_description_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}*/
		
		return $errors;			
	}
         //---- strat of  function for validating meatag---//
        
         //---- strat of  function for validating ajax meatag---//
        function validate_metatags_ajax()
	{
		$this->layout="";
		$this->autoRender=false;	
		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_metatags($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['MetaTag'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
         //---- strat of  function for validating ajax meatag---//
        
         //---- strat of  function for delete meatag---//
        function admin_delete_meta($page=NULL,$id=NULL,$tableName=NULL,$renderPath=NULL,$renderElement=NULL)
	{
		$this->layout='';
                $this->loadModel('MetaTag');
		$id=convert_uudecode(base64_decode($id));
		$tableName=base64_decode($tableName);
		$renderPath=base64_decode($renderPath);
		$renderElement=base64_decode($renderElement);
		//pr($tableName);die;
		if($this->RequestHandler->isAjax())
		{		
			$this->layout=false;
			$this->autoRender = false;	
						
			$getRec = $this->$tableName->find('first',array('conditions'=>array($tableName.'.id'=>$id)));
			//print_r($getRec); die;
			$field=array();
			
			if(!empty($getRec))
			{
                            //$record[$tableName]['id']=$id;
                            $this->$tableName->delete(array($tableName.'.id'=>$id));
                            //$this->Session->write('success','Record has been deleted successfully');
					
					
			}		   	
			$this->paginate=array('limit' => 10,'page'=>$page,'order'=>$tableName.'.id DESC');
			$info = $this->paginate($tableName);
			
			if(empty($info) && $page>1)
			{
				$page=$page-1;
				$this->paginate=array('limit' => 10,'page'=>$page,'order'=>$tableName.'.id DESC');
				$info = $this->paginate($tableName);
			}			
			$this->set(compact('info','page'));			
			$this->viewPath = 'Elements'.DS.'adminElements/'.$renderPath;
			$this->render($renderElement);			
		}
	}
         //---- end of  function for delete meatag---//
        
        
         //---- strat of  function for edit meatag---//
        function admin_edit_metatag($id){
            $this->layout='admin';
             $this->loadModel('MetaTag');
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->MetaTag->find('first',array('conditions'=>array('MetaTag.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}	
		if(!empty($this->data))
		{ 
		 	$error = $this->validate_edit_metatags($this->data);
			
			if(count($error) == 0)
			{
				$info = $this->MetaTag->find('first',array('conditions'=>array('MetaTag.id'=>$this->data['MetaTag']['id'])));				
				$url = $this->request->data['MetaTag']['page_url'];
                                $parse = parse_url($url);
                                $replace_str=$parse['host'];
                                $website_url=str_replace(array('https://'.$replace_str.'/kuulum/','http://'.$replace_str.'/kuulum/'),array('',''),$url);
                                $this->request->data['MetaTag']['page_url']=$website_url;
				$this->MetaTag->save($this->data); 
				$this->Session->write('success','Information has been updated successfully');
				$this->redirect(array('action'=>'meta_tags','admin' => true));
			}
			else
			{
				$info = $this->MetaTag->find('first',array('conditions'=>array('MetaTag.id'=>$this->data['MetaTag']['id'])));
				$this->set(compact('error','info'));
			}										
		}
        }
         //---- strat of  function for edit meatag---//
        
         //---- strat of  function for validating meatag---//
        function validate_edit_metatags($data)
	 {	
		$locale=$data['MetaTag']['locale'];
		$errors = array ();                
		if (trim($data['MetaTag']['page_url']) == ""){
			$errors['page_url'][] = __(FIELD_REQUIRED)."\n";                
                      }else if(trim($data['MetaTag']['page_url']) != "" && !$this->isValidURL1(trim($data['MetaTag']['page_url']))) {
                        
                    $errors['page_url'][] = __("Invalid Page URL")."\n";;
                  }else if(trim($data['MetaTag']['page_url']) != ""){
                      $this->loadModel('MetaTag');
                            $url = $data['MetaTag']['page_url'];
                            $parse = parse_url($url);
                            $replace_str=$parse['host'];
                            $website_url=str_replace(array('https://'.$replace_str.'/kuulum/','http://'.$replace_str.'/kuulum/'),array('',''),$url);
                            $url=$website_url;
			$chkUrl = $this->MetaTag->find('count',array('conditions'=>array('MetaTag.id !='=>$data['MetaTag']['id'],'MetaTag.page_url'=>trim($url))));
			if($chkUrl > 0){
				$errors['page_url'][] = __('Page URL is exsists')."\n";
			}
                  }
		
		/*if (empty($data['MetaTag']['meta_title_'.$locale])){
			$errors['meta_title_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
		if (empty($data['MetaTag']['meta_keywords_'.$locale])){
			$errors['meta_keywords_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
                if (trim($data['MetaTag']['meta_description_'.$locale]) == ""){
			$errors['meta_description_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}
		*/
		return $errors;			
	}
         //---- strat of  function for validating meatag---//
        
         //---- strat of  function for validating ajax meatag---//
        function validate_edit_metatags_ajax()
	{
		$this->layout="";
		$this->autoRender=false;	
		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_edit_metatags($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['MetaTag'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
         //---- strat of  function for validating ajax  meatag---//
        
        
        //---- strat of  function for view meatag---//
        function admin_view_metatag($id=NULL)
	  {
		  $this->layout='admin';
                  $this->loadModel('MetaTag');
		  $pageid=convert_uudecode(base64_decode($id));
		  $info = $this->MetaTag->find('first',array('conditions'=>array('MetaTag.id'=>$pageid),'contain'=>array()));
		  $this->set('info',$info);
	  }
	   //---- strat of  function for view meatag---//
           //
        function admin_meta_data_settings($id=NULL)
	{
		$this->loadModel('MetatagSetting');
		$this->layout='admin';
                $page=isset($this->params['named']['page'])?$this->params['named']['page']:'1';
                $this->paginate=array('limit'=>'10','order'=>'MetatagSetting.id DESC'); 
		$info=$this->paginate('MetatagSetting');
                //print_r($info);
		$this->set(compact('info','page'));
		if($this->RequestHandler->isAjax())
		{
			 $this->layout="";
			 $this->viewPath ='Elements'.DS.'adminElements/admin/metaTags';
			 $this->render('meta_data_setting_list');
		}
	}
           //---- strat of  function for adding meatag---//
	function admin_edit_meta_data_settings($id=NULL)
	{
		
                 $this->layout='admin';
             $this->loadModel('MetatagSetting');
		if(!empty($id))
		{	
			$pageid=convert_uudecode(base64_decode($id));
			$info = $this->MetatagSetting->find('first',array('conditions'=>array('MetatagSetting.id'=>$pageid),'contain'=>array()));
			$this->set('info',$info);
		}	
		if(!empty($this->data))
		{ 
		 	$error = $this->validate_meta_data_settings($this->data);
			
			if(count($error) == 0)
			{
				$info = $this->MetatagSetting->find('first',array('conditions'=>array('MetatagSetting.id'=>$this->data['MetatagSetting']['id'])));				
                            	$this->MetatagSetting->save($this->data); 				
                                $this->Session->write('success','Information has been updated successfully');
                                
				$this->redirect(array('action'=>'meta_data_settings','admin' => true));
			}
			else
			{
				$info = $this->MetatagSetting->find('first',array('conditions'=>array('MetatagSetting.id'=>$this->data['MetatagSetting']['id'])));
				$this->set(compact('error','info'));
			}										
		}
	}
         //---- end of  function for adding meatag---//
        
         //---- strat of  function for validating meatag---//
        function validate_meta_data_settings($data)
	 {	
		$locale=$data['MetatagSetting']['locale'];
		$errors = array ();
                if($locale=="en"){
		if (empty($data['MetatagSetting']['meta_title_'.$locale])){
			$errors['meta_title_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
		if (empty($data['MetatagSetting']['meta_keywords_'.$locale])){
			$errors['meta_keywords_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
                if (trim($data['MetatagSetting']['meta_description_'.$locale]) == ""){
			$errors['meta_description_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}
                }
                else{
                    if($locale=="en"){
		if (empty($data['MetatagSetting']['meta_title_en'])){
			$errors['meta_title_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
		if (empty($data['MetatagSetting']['meta_keywords_en'])){
			$errors['meta_keywords_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}	
                if (trim($data['MetatagSetting']['meta_description_en']) == ""){
			$errors['meta_description_'.$locale][] = __(FIELD_REQUIRED)."\n";
		}
                }
                }
		
		return $errors;			
	}
         //---- strat of  function for validating meatag---//
        
         //---- strat of  function for validating ajax meatag---//
        function validate_meta_data_settings_ajax()
	{
		$this->layout="";
		$this->autoRender=false;	
		
		if($this->RequestHandler->isAjax()){
			$errors_msg = null;
			$errors=$this->validate_meta_data_settings($this->data);						
			if(is_array($this->data))	{
				foreach ($this->data['MetatagSetting'] as $key => $value ){
					if( array_key_exists ( $key, $errors) ){
						foreach ( $errors [ $key ] as $k => $v ){
							$errors_msg .= "error|$key|$v";
						}	
					}
					else {
						$errors_msg .= "ok|$key\n";
					}
				}
			}
			echo $errors_msg;
			die;
		}	
	}
         //---- strat of  function for validating ajax meatag---//
        
         //---- strat of  function for view meatag---//
        function admin_view_meta_data_settings($id=NULL)
	  {
		  $this->layout='admin';
                  $this->loadModel('MetatagSetting');
		  $pageid=convert_uudecode(base64_decode($id));
		  $info = $this->MetatagSetting->find('first',array('conditions'=>array('MetatagSetting.id'=>$pageid),'contain'=>array()));
		  $this->set('info',$info);
	  }
	   //---- strat of  function for view meatag---//
}   
?>
