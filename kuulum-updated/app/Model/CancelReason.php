<?php
class CancelReason extends AppModel {
	var $name = 'CancelReason';
	var $actsAs = array("Containable");
	
	
	var $belongsTo = array(						
						'Member'=>array(
							'className'	=>	'Member',
							'foreignKey'=>	'm_id',
							'fields'	=>	array('id','final_name','email','status')
						)
					);
	
	var $hasMany = array(						
						'UserReason'=>array(
							'className'	=>	'UserReason',
							'foreignKey'=>	'cancel_reason_id',
							'dependent' => true						
						)
					);	

}
