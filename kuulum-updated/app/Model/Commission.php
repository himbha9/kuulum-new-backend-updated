
<?php
App::uses('Sanitize', 'Utility');
class Commission extends AppModel { 
	var $name = 'Commission';

	 var $validate = array(
        'commission' => array(
            'rule' => 'notEmpty',
			'allowEmpty'=>false
           
        ),
		'author_name' => array(
            'rule' => 'notEmpty',
			'allowEmpty'=>false
            
        ),
        'duration' => array(
            'rule' => 'notEmpty',
			'allowEmpty'=>false
           
        ),
        'courses' => array(
            'rule' => 'notEmpty',
			'allowEmpty'=>false
        
        ),
		
		
		
    );	
					
	
}
