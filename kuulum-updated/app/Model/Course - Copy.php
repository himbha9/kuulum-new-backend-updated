<?php
App::uses('Sanitize', 'Utility');
class Course extends AppModel {
	var $name = 'Course';
	var $actsAs = array("Containable");
	var $hasMany = array(
						'CourseKeyword'=>array(
							'className'=>'CourseKeyword',
							'foreignKey'=>'course_id',
							'dependent' => true						
						),
						'CourseLesson'=>array(
							'className'=>'CourseLesson',
							'foreignKey'=>'course_id',
							'dependent' => true						
						),
						'MemberCourse'=>array(
							'className'=>'MemberCourse',
							'foreignKey'=>'c_id',
							'dependent' => true						
						)		
					);
	public function beforeSave($options = array())
	{
		/*foreach($this->data as $data){
			foreach($data as $key=>$d){
				$this->data['Course'][$key] = 	Sanitize::html($d,array('remove'=>true,'charset'=>true,'quotes' => false));
			}
		}	*/
		$this->data = 	Sanitize::clean($this->data,array('remove_html'=>true,'escape'=>false));
		return true;	
	}
}
