<?php
App::uses('Sanitize', 'Utility');
class Course extends AppModel {
	var $name = 'Course';
	var $actsAs = array("Containable");
	//var $virtualFields = array('CourseLikeNumber'=>'SELECT COUNT(*) FROM course_likes c WHERE c.course_id = Course.id'); 

	var $hasOne = array(
						'CourseView'=>array(
							'className'=>'CourseView',
							'foreignKey'=>'course_id',
							'dependent' => true
						)
						
						
					);
	var $hasMany = array(
						'CourseKeyword'=>array(
							'className'=>'CourseKeyword',
							'foreignKey'=>'course_id',
							'dependent' => true						
						),
						'CourseLesson'=>array(
							'className'=>'CourseLesson',
							'foreignKey'=>'course_id',
							'order'=>array('CourseLesson.date_added ASC'),
							'dependent' => true						
						),						
						'CoursePurchaseList'=>array(
							'className'=>'CoursePurchaseList',
							'foreignKey'=>'c_id',
							'dependent' => true						
						),
						'CourseRemark'=>array(
							'className'=>'CourseRemark',
							'foreignKey'=>'course_id',
							'order'=>array('CourseRemark.promote'=>'DESC','CourseRemark.date_added'=>'DESC'),
							'dependent' => true						
						),
						'Subscription_course'=>array(
							'className'=>'CoursePurchaseList',
							'foreignKey'=>'c_id',
							'dependent' => true						
						),
						'Subscription_lesson'=>array(
							'className'=>'CoursePurchaseList',
							'foreignKey'=>'c_id',
							'dependent' => true						
						),
						'CourseRating'=>array(
							'className'=>'CourseRating',
							'foreignKey'=>'course_id',
							'dependent' => true						
						),
						'CourseWatch'=>array(
							'className'=>'CourseWatch',
							'foreignKey'=>'course_id',
							'dependent' => true
						)/*,
						'CourseLike'=>array(
							'className'=>'CourseLike',
							'foreignKey'=>'course_id',
							'dependent' => true
						)*/
					);

	var $belongsTo = array(
						'Language'=>array(
							'className'=>'Language',
							'foreignKey'=>'primary_lang',
							'dependent' => true						
						),
						'Member'=>array(
							'className'=>'Member',
							'foreignKey'=>'m_id',
							'dependent' => true						
						),
						'CourseSubcategory'=>array(
							'className'=>'CourseSubcategory',
							'foreignKey'=>'subcategory_id',
							'dependent' => true
						),
						'CourseCategory'=>array(
							'className'=>'CourseCategory',
							'foreignKey'=>'category_id',
							'dependent' => true
						),
						'CourseSlSubcategory'=>array(
							'className'=>'CourseSlSubcategory',
							'foreignKey'=>'sl_subcategory_id',
							'dependent' => true
						)
					);
				
	public function beforeSave($options = array())
	{
		/*foreach($this->data as $data){
			foreach($data as $key=>$d){
				$this->data['Course'][$key] = 	Sanitize::html($d,array('remove'=>true,'charset'=>true,'quotes' => false));
			}
		}	*/
		//$this->data = 	Sanitize::clean($this->data,array('unicode'=>false,'remove_html'=>true,'escape'=>false));
		return true;	
	}
}
