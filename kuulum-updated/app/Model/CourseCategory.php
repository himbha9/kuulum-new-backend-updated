<?php 
App::uses('Sanitize','Utility');
class CourseCategory extends AppModel
{
	var $name = 'CourseCategory';
	var $actsAs=array('Containable');
	var $hasMany= array(
					
					'CourseSubcategory'=>array(
					'className'=>'CourseSubcategory',
					'conditions'=>array('CourseSubcategory.status'=>'1'),
					'foreignKey'=>'category_id'
					),
					'Course'=>array(
					'className'=>'Course',
					'foreignKey'=>'category_id'
					)
	
	
	);
}

?>