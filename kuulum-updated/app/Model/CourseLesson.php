<?php
App::uses('Sanitize', 'Utility');
class CourseLesson extends AppModel {
	var $name = 'CourseLesson';		
	var $actsAs = array("Containable");
	/*var $virtualFields = array('lesson_like'=>'SELECT COUNT(*) FROM lesson_likes l WHERE l.lesson_id = CourseLesson.id AND l.course_id = CourseLesson.course_id');*/
	var $belongsTo = array(
						'Course'=>array(
							'className'=>'Course',
							'foreignKey'=>'course_id',
							'dependent' => true						
						)
					);
					
	/*var $hasMany = array(
						'LessonLike'=>array(
							'className'=>'LessonLike',
							'foreignKey'=>'lesson_id',
							'dependent' => true						
						)
	);*/
					
	public function beforeSave($options = array())
	{
		/*foreach($this->data as $data){
			foreach($data as $key=>$d){
				$this->data['CourseLesson'][$key] = Sanitize::html($d,array('remove'=>true,'charset'=>true,'quotes' => false));
			}
		}	*/
		//$this->data = 	Sanitize::clean($this->data,array('remove_html'=>true,'escape'=>false));
		return true;	
	}
}
