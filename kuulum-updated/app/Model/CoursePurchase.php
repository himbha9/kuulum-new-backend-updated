<?php
App::uses('Sanitize', 'Utility');
class CoursePurchase extends AppModel {
	var $name = 'CoursePurchase';
	var $actsAs = array("Containable");
	var $belongsTo = array(
						'Member'=>array(
							'className'=>'Member',
							'foreignKey'=>'m_id',
							'dependent' => true,
						)
					);
	public function beforeSave($options = array())
	{
		/*foreach($this->data as $data){
			foreach($data as $key=>$d){
				$this->data['Course'][$key] = 	Sanitize::html($d,array('remove'=>true,'charset'=>true,'quotes' => false));
			}
		}	*/
		$this->data = Sanitize::clean($this->data,array('remove_html'=>true,'escape'=>false));
		return true;	
	}
}
