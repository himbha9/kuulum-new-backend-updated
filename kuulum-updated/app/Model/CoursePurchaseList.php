 <?php
App::uses('Sanitize', 'Utility');
class CoursePurchaseList extends AppModel {
	var $name = 'CoursePurchaseList';
	var $actsAs = array("Containable");
	
	
	var $belongsTo = array(
						'Course'=>array(
							'className'=>'Course',
							'foreignKey'=>'c_id',
							'dependent' => true								
						),
						'CoursePurchase'=>array(
							'className'=>'CoursePurchase',
							'foreignKey'=>'purchase_id',
							'fields'=>array('id','payment_amount','txn_id','date_added','deduct_commission','commission_rate','discount'),
							'dependent' => true								
						),
						'Member'=>array(
							'className'=>'Member',
							'foreignKey'=>'mem_id',
							'fields'=>array('id','email','given_name','family_name'),
							'dependent' => true								
						),
						'CourseLesson'=>array(
							'className'=>'CourseLesson',
							'foreignKey'=>'lesson_id',
							'fields'=>array('id','title','price'),
							'dependent' => true								
						)
					);
					
				
	public function beforeSave($options = array())
	{
		/*foreach($this->data as $data){
			foreach($data as $key=>$d){
				$this->data['Course'][$key] = 	Sanitize::html($d,array('remove'=>true,'charset'=>true,'quotes' => false));
			}
		}	*/
		$this->data = Sanitize::clean($this->data,array('remove_html'=>true,'escape'=>false));
		return true;	
	}
} 
