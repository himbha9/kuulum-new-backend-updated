<?php 
App::uses('Sanitize', 'Utility');
class CourseRemark extends AppModel {
	var $name = 'CourseRemark';
	var $actsAs = array("Containable");
	var $belongsTo = array(
							'Member' => array(
								'className' => 'Member',
								'foreignKey' => 'member_id',
								'dependent' => true
								),
								'Course' => array(
								'className' => 'Course',
								'foreignKey' => 'course_id',
								'dependent' => true
								),
								
					);
	var $hasMany = array(
							'RemarkReport' => array(
								'className' => 'RemarkReport',
								'foreignKey' => 'remark_id',
								'dependent' => true
								)
					);
}
?>