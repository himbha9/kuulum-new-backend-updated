<?php 
App::uses('Sanitize', 'Utility');
class CourseSubcategory  extends AppModel
{
	var $name='CourseSubcategory';
	var $actsAs=array("Containable");
	var $belongsTo = array(
							
							'CourseCategory'=>array(
								'className'=>'CourseCategory',
								'foreignKey'=>'category_id',
								)
							);
		var $hasMany = array(
							
							'Course'=>array(
								'className'=>'Course',
								'foreignKey'=>'subcategory_id',
								'dependent'=>true
							),
							'CourseSlSubcategory'=>array(
								'className'=>'CourseSlSubcategory',
								'foreignKey'=>'sub_category_id',
								'conditions'=>array('CourseSlSubcategory.status'=>'1'),
								'dependent'=>true
							)
						);
	
}


?>
