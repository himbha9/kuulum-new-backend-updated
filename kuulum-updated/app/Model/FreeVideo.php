<?php
App::uses('Sanitize', 'Utility');
class FreeVideo extends AppModel {
	var $name = 'FreeVideo';
	var $actsAs = array("Containable");

	var $hasOne = array(
						'WatchFreeVideo'=>array(
							'className'=>'WatchFreeVideo',
							'foreignKey'=>'free_video_id',
							'dependent' => true
						)
						
						
					);
}