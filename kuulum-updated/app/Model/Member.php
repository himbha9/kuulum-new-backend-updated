<?php
App::uses('Sanitize', 'Utility');
class Member extends AppModel {
	var $name = 'Member';
	var $actsAs = array("Containable");
	public $hasMany = array(
							'MemberKeyword' => array(
								'className' => 'MemberKeyword',
								'foreignKey' => 'm_id',
								'dependent' => true
								),
							'MmeberAuthorKeyword' => array(
									'className' => 'MemberAuthorKeyword',
									'foreignKey' => 'm_id',
									'dependent' => true
								),
							'MemberSubscription' => array(
									'className' => 'MemberSubscription',
									'foreignKey' => 'm_id',
									'dependent' => true
								)
					);
	
	public $hasOne = array(	
				
				'CancelReason'	=>	array(
					'className'	=>	'CancelReason',
					'foreignKey'=> 	'm_id',
					'dependent'	=> 	true
				),
				'Admin'	=>	array(
					'className'	=>	'Admin',
					'foreignKey'=> 	'member_id',
					'dependent'	=> 	true
				)
	);
		
					
	public function beforeSave($options = array())
	{
		/*foreach($this->data as $data){
			foreach($data as $key=>$d){
				$this->data['Member'][$key] = Sanitize::html($d,array('remove'=>true,'charset'=>true,'quotes' => false));
			}
		}	*/
		//$this->data = 	Sanitize::clean($this->data,array('remove_html'=>true,'escape'=>false));
		return true;
	}
}
