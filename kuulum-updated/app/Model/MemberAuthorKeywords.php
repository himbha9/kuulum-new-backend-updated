<?php
App::uses('Sanitize', 'Utility');
class MemberAuthorKeywords extends AppModel {
	var $name = 'MemberAuthorKeywords';
	
	var $actsAs = array("Containable");
	
	var $belongsTo = array(						
						'Member'=>array(
							'className'	=>	'Member',
							'foreignKey'=>	'm_id',
							'fields'	=>	array('id','final_name','email','status'),
							'dependent' => true
						)
					);				
	
}
