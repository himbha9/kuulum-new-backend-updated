<?php
App::uses('Sanitize', 'Utility');
class MemberKeywords extends AppModel {
	var $name = 'MemberKeywords';
	var $actsAs = array("Containable");
	
	var $belongsTo = array(						
						'Member'=>array(
							'className'	=>	'Member',
							'foreignKey'=>	'm_id',
							'fields'	=>	array('id','final_name','email','status')
						)
					);				
	
}
