<?php

App::uses('Sanitize', 'Utility');
class Notification extends AppModel {
	var $name = 'Notification';
	var $actsAs = array("Containable");
	var $belongsTo = array(
							'Member'=>array(
							'className'=>'Member',
							'foreignKey'=>'mem_id',
							'fields'=>array('id','final_name','given_name'),
							'dependent' => true	
								
						),
							'Course'=>array(
							'className'=>'Course',
							'foreignKey'=>'c_id',
							'fields'=>array('id','m_id','title'),
							'dependent' => true
						),
							
							'CourseLesson'=>array(
							'className'=>'CourseLesson',
							'foreignKey'=>'lesson_id',
							'fields'=>array('id','course_id','title'),
							'dependent' => true
						)
					);	
}
 ?>
