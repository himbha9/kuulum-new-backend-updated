<?php
class PaypalExpress extends AppModel {
    var $name = 'Paypal';
    var $useTable = false;
     
    //configuration
    var $environment = PAYPAL_BOX;   // or 'beta-sandbox' or 'live'
    var $version = '97';
    private $USE_PROXY = false;
    private $PROXY_HOST = '127.0.0.1';
    private $PROXY_PORT = '808';
    //give correct info below
    //var $API_UserName  = 'bodla.sagarika-facilitator_api1.gmail.com';
    //var $API_Password  = '1402667336';
   // var $API_Signature = 'ALmjzcbApVNL8CGdUpU.CTvQI.zGA8bF8847ZHs-gmy0wuRXYHzirKrI';
    /*var $API_UserName  = 'martin-facilitator_api1.l10ntrain.com';
    var $API_Password  = '1381441249';
    var $API_Signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AK37DFBlAT8O0NT.xLGYNfa96gC5';     */
    var $API_UserName  = null;
    var $API_Password  = null;
    var $API_Signature = null; 
    //variables
    var $errors        = null;   
    var $token         = null;
    var $transId       = null;
        
     
    /**
     * Send HTTP POST Request
     *
     * @param   string  The API method name
     * @param   string  The POST Message fields in &name=value pair format
     * @return  array   Parsed HTTP Response body
     */
    function PPHttpPost($methodName, $nvpStr,$UserName,$Password,$Signature,$environment) {
        // Set up your API credentials, PayPal end point, and API version.
        $API_UserName = $UserName;
        $API_Password = $Password;
        $API_Signature =$Signature;
        $API_Endpoint = "https://api-3t.paypal.com/nvp";
        if("sandbox" ===$environment || "beta-sandbox" === $environment) {
            $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
        }
        $version = urlencode($this->version);
 
        // Set the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
 
        // Turn off the server and peer verification (TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($this->USE_PROXY) curl_setopt($ch, CURLOPT_PROXY, $this->PROXY_HOST . ':' . $this->PROXY_PORT);
        // Set the API operation, version, and API signature in the request.
        $nvpreq = "METHOD=$methodName&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature&$nvpStr";
 
        // Set the request as a POST FIELD for curl.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
 
        // Get response from the server.
        $httpResponse = curl_exec($ch);
 
        if(!$httpResponse) {
            exit("$methodName failed: ".curl_error($ch).'('.curl_errno($ch).')');
        }
 
        // Extract the response details.
        $httpResponseAr = explode("&", $httpResponse);
 
        $httpParsedResponseAr = array();
        foreach ($httpResponseAr as $i => $value) {
            $tmpAr = explode("=", $value);
            if(sizeof($tmpAr) > 1) {
                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
            }
        }
 
        if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
            exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
        }
 
        return $httpParsedResponseAr;
    }
     
    /*
     * get PayPal Url for redirecting page
     */
    function getPaypalUrl($token,$environment) {            
         $payPalURL ="https://www.paypal.com/webscr&cmd=_express-checkout&token={$token}&useraction=commit";
        if("sandbox" === $environment|| "beta-sandbox" === $environment) {        
            $payPalURL = "https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token={$token}&useraction=commit";
        }
        return $payPalURL;
    }
 
         
    /*
     * call PayPal API: SetExpressCheckout
     */
    function setExpressCheckout($nvpStr,$UserName,$Password,$Signature,$environment) {
        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $nvpStr,$UserName,$Password,$Signature,$environment);
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
            $this->token = urldecode($httpParsedResponseAr["TOKEN"]);          
            return $this->token;
        } else  {
            $this->errors = $httpParsedResponseAr;
            return false;          
        }
    }
 
    /*
     * call PayPal API: DoExpressCheckoutPayment
     */
    function doExpressCheckoutPayment($nvpStr,$UserName,$Password,$Signature) {
        // Execute the API operation; see the PPHttpPost function above.
        $httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $nvpStr,$UserName,$Password,$Signature);
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
            $this->transId = urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);        
            return true;
        } else  {
            $this->errors = $httpParsedResponseAr;
            return false;
        }      
    }
}

?>
