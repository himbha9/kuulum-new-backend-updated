<?php
App::uses('Sanitize', 'Utility');
class PurchaseItem extends AppModel {
	var $name = 'PurchaseItem';
	//var $actsAs = array("Containable");
	var $belongsTo = array(
						'Member'=>array(
							'className'=>'Member',
							'foreignKey'=>'m_id',
                                                        'fields'=>array('id','email','given_name','family_name'),
							'dependent' => true,
						)
					);
	
}
