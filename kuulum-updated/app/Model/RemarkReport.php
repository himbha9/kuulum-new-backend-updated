<?php 
App::uses('Sanitize', 'Utility');
class RemarkReport extends AppModel {
	var $name = 'RemarkReport';
	var $actsAs = array("Containable");
	var $belongsTo = array(
							'Member' => array(
								'className' => 'Member',
								'foreignKey' => 'member_id',
								'dependent' => true
								),
							'CourseRemark' => array(
								'className' => 'CourseRemark',
								'foreignKey' => 'remark_id',
								'dependent' => true
								)
							
					);
	}
?>