<?php
App::uses('Sanitize', 'Utility');
class Role extends AppModel {
	var $name = 'Role';
	var $actsAs = array("Containable");
	
					
	public function beforeSave($options = array())
	{
		/*foreach($this->data as $data){
			foreach($data as $key=>$d){
				$this->data['Course'][$key] = 	Sanitize::html($d,array('remove'=>true,'charset'=>true,'quotes' => false));
			}
		}	*/
		$this->data = 	Sanitize::clean($this->data,array('remove_html'=>true,'escape'=>false));
		return true;	
	}
}
