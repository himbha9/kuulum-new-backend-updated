<?php
class UserReason extends AppModel {
	var $name = 'UserReason';
	var $actsAs = array("Containable");
	
	
	var $belongsTo = array(						
						'Reason'=>array(
							'className'	=>	'Reason',
							'foreignKey'=>	'reason_id'							
						)
					);
	
}
