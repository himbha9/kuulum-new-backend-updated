<?php
App::uses('Sanitize', 'Utility');
class Watermark extends AppModel {
	var $name = 'Watermark';
	var $actsAs = array("Containable");

	var $belongsTo = array(
						'CourseLesson'=>array(
							'className'=>'CourseLesson',
							'foreignKey'=>'lesson_id',
							'dependent' => true
						)
						
						
					);
	}
?>