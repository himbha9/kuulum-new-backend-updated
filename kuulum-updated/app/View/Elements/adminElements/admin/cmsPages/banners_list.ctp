<?php
echo $this->Html->script('newjquery-ui.js');
?>

<script type="text/javascript">

	$(document).ready(function() {
	
	
	
		// $.getScript("../../webroot/js/admin/common.js");		
		<?php 
		if(isset($this->request->named['']))
{
?>
$("#tabs").tabs({

'selected':<?php echo $this->request->named[''];?>,
'active':<?php echo $this->request->named[''];?>
});
<?php
}
else
{

		foreach($language as $key1=>$lang){  
		if($lang['Language']['locale']==$localeset)
		{
		
		?>

		$("#tabs").tabs({
		'active':<?php echo $key1;?>,
		'selected':<?php echo $key1;?>});
		
		<?php 
		}
		}
		}
		?>

		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			$langtitle=$lang.split("_");
			console.log($langtitle);
			$langcode = $(this).attr('id');
				
			$('#keylang').val($langtitle[1]);
			$('#lang_locale').val($langtitle[0]);
			$('#lang_code').val($langcode);
		});
		
	$(".imageupload").click(function(){ 
		
	var id=$(this).attr("id");
	var splittedid=id.split("_");
	console.log(splittedid[1]);
	console.log($(this).is(':checked'));
	if($(this).is(':checked')==true)
	{
	$.ajax({
	url:ajax_url+'admin/Users/bannerdefault',
	data:{lang:splittedid[1],default:0},
	type:'POST',
	success:function(resp)
	{
	console.log(resp);
	}
	
	});
	$("#imageupload_"+splittedid[1]+"_input").css("display","none");
	}	
	else
	{
	$.ajax({
	url:ajax_url+'admin/Users/bannerdefault',
	data:{lang:splittedid[1],default:1},
	type:'POST',
	success:function(resp)
	{
	console.log(resp);
	}
	
	});
	$("#imageupload_"+splittedid[1]+"_input").css("display","block");
	}
		
	});
	
		$(".imageupload1").click(function(){ 
		
	var id=$(this).attr("id");
	var splittedid=id.split("_");
	console.log(splittedid[1]);
	console.log($(this).is(':checked'));
	if($(this).is(':checked')==true)
	{
	$.ajax({
	url:ajax_url+'admin/Users/bannerdefault',
	data:{lang:splittedid[1],default:0},
	type:'POST',
	success:function(resp)
	{
	console.log(resp);
	}
	
	});
	//$("#imageupload_"+splittedid[1]+"_input").css("display","none");
	}	
	else
	{
	$.ajax({
	url:ajax_url+'admin/Users/bannerdefault',
	data:{lang:splittedid[1],default:1},
	type:'POST',
	success:function(resp)
	{
	console.log(resp);
	}
	
	});
	//$("#imageupload_"+splittedid[1]+"_input").css("display","block");
	}
		
	});
	

	$(".imageuploadify").on("change",function(){
	document.getElementById("addbanners").submit();
	
	});
		
	});
	
	
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Banners<br><small>Add banner for the Kuulum landing page</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
				<?php $i++;
                                }
                                ?>	
			</ul>
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
  
			   
			   <?php 
			   if(isset($this->request->named['']))
			   {
			   $localekey=$this->request->named[''];
			   $localehidden=$language[$localekey]['Language']['locale'];
			   $lang_codehidden=$language[$localekey]['Language']['lang_code'];
			   $localekeyhidden=$this->request->named[''];
			   }
			   elseif(isset($localeset))
			   {
			    foreach($language as $langkey=>$lang1)
			   {
				if($localeset==$lang1['Language']['locale'])
				{
			   $localehidden=$localeset;
			   $lang_codehidden=$lang1['Language']['lang_code'];
			   $localekeyhidden=$langkey;
				}
			   }			  
			  			   
			   }
			   else
			   {
			   $localehidden='en';
			   $lang_codehidden='en_US';
			   $localekey=0;
			   }
			  $localekey=$this->request->named[''];
                           ?>
			
			
				<?php echo $this->Form->create('Banners',array('class'=>'js-validation-bootstrap form-horizontal editTemplateForm','id'=>'addbanners','url'=>array('controller'=>'users','action'=>'add_banners'),'enctype'=>'multipart/form-data')); ?>
			
				<input type="hidden" id="lang_locale" value="<?php echo $localehidden;  ?>" name="data[Banners][locale]"/>	
				<input type="hidden" id="lang_code" value="<?php echo $lang_codehidden; ?>" name="data[Banners][lang_code]"/>	
				<input type="hidden" id="keylang" value="<?php echo $localekeyhidden;?>" name="data[Banners][keylang]"/>	
				   <div class="block-content tab-content">  
				<?php //pr($language);die; ?>
				  <?php 
				  
				 
					$i=1; 
					foreach($language as $lang)
					{
						$locale = $lang['Language']['locale'];								
					?>
			    <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>" title="<?php echo $lang['Language']['locale']; ?>">
                                <ul>	
				<?php 
				
				$banners=$this->Utility->getbanners($lang['Language']['locale']);
				
				 $count=sizeof($banners);
				
				if(!empty($banners))
				{
				
				?>
				<input type="hidden" id="lang_order" value="<?php echo $banners[0]['Banners']['order']+1;?>" name="data[Banners][order_<?php echo $lang['Language']['locale']; ?>]"/>	
				
				<?php if($lang['Language']['locale']!='en')
				{
				
				$bannerdefaults=$this->Utility->getbannerdefaults($lang['Language']['locale']);
				
				
				if($bannerdefaults['Bannerdefaults']['default']==1)
				{
                                        $checked="";
				}
				else
				{
                                        $checked="checked";
				}
				
				?>
				
					<div style="float:left;">
				<?php
				echo $this->Form->input('image_'.$lang['Language']['locale'].'_upload',array('checked'=>$checked,'class'=>'imageupload1','type'=>'checkbox','id'=>'image_'.$lang['Language']['locale'].'_upload','value'=>false,'label'=>false));
				?>
				</div>
				<label>Show default locale banners</label>
				
				
				<?php
				}
				foreach($banners as $key=>$banner)
				{
				
				
				$bannerimage=$banner['Banners']['image'];
				$bannerlangcode=$banner['Banners']['lang_code'];
				?>
				
				<li   id="image_<?php echo $lang['Language']['locale']; ?>_<?php echo $banner['Banners']['order'];?>" class="imageslist<?php echo ($key==0) ? ' first': ''; ?><?php echo ($key==count($banners)-1) ? ' last': ''; ?>">
				
                                <div class="col-xs-10" style="margin: 15px 0px 10px 0px;">  
				<?php
				
				//echo $this->Html->image('front/'.$lang['Language']['lang_code'].'/'.$banner['Banners']['image'],array('width'=>'1140','height'=>'272'));
				
				if(!empty($bannerimage))
				{
				echo $this->Html->image($this->Utility->getAWSImgUrl(BANNER_BUCKET,'/'.$bannerlangcode."/".$bannerimage),array('width'=>'1140','height'=>'272'));
				}
				
				?>
                                    
				<?php 
				$locale=$lang['Language']['locale'];
				$prev=$banners[$key-1]['Banners']['order'];
				$next=$banners[$key+1]['Banners']['order'];
				$current=$banners[$key]['Banners']['order'];
				$previd=$banners[$key-1]['Banners']['id'];
				$nextid=$banners[$key+1]['Banners']['id'];
				$currentid=$banners[$key]['Banners']['id'];
				if($count>1)
				{
				if($key==0)
				{
				?>
                                    <div style="position: absolute;left:95%; top:86%;"><button type="button" title="Down" class="nextbutton" onclick="return updateimagedecorder('image_<?php echo $locale; ?>_<?php echo $next;?>_<?php echo $nextid;?>','image_<?php echo $locale; ?>_<?php echo $current;?>_<?php echo $currentid;?>');"><img src="<?php echo HTTP_ROOT;?>/img/downBanner.png"></button></div>
				<?php
				}
				if($key>0 && $key<($count-1))
				{
				?>
                                <div style="position: absolute;left:95%; top:2%;">
				<button type="button" title="Up" class="prevbutton" onclick="return updateimageincorder('image_<?php echo $locale; ?>_<?php echo $prev;?>_<?php echo $previd;?>','image_<?php echo $locale; ?>_<?php echo $current;?>_<?php echo $currentid;?>');"><img src="<?php echo HTTP_ROOT;?>/img/upBanner.png"></button></div>
                                <div style="position: absolute;left:95%; top:86%;"><button type="button" title="Down" class="nextbutton" onclick="return updateimagedecorder('image_<?php echo $locale; ?>_<?php echo $next;?>_<?php echo $nextid;?>','image_<?php echo $locale; ?>_<?php echo $current;?>_<?php echo $currentid;?>');"><img src="<?php echo HTTP_ROOT;?>/img/downBanner.png"></button></div>
				
				<?php
				}
				if($key==($count-1))
				{
				?>
				 <div class="text-right" style="position: absolute;left:95%; top:2%;"><button type="button" title="Up" class="prevbutton" onclick="return updateimageincorder('image_<?php echo $locale; ?>_<?php echo $prev;?>_<?php echo $previd;?>','image_<?php echo $locale; ?>_<?php echo $current;?>_<?php echo $currentid;?>');"><img src="<?php echo HTTP_ROOT;?>/img/upBanner.png"></button></div>
				<?php
				}
				}
				?>
				 <div style="position: absolute;top:10px;"><button onclick="return deleteimage('image_<?php echo $lang['Language']['locale']; ?>_<?php echo $banners[$key]['Banners']['order'];?>');" title="Delete"><img src="<?php echo HTTP_ROOT;?>/img/deleteBanner.png"></button></div>
			
                                </div>
				<div class=" col-xs-2">
				

				<div id="slideset<?php echo $current;?>">
				<script>
				  $(function() {
					$( "#slider_<?php echo $currentid;?>" ).slider({
					
					min: 0,
					max: 1,
					value : <?php echo $banners[$key]['Banners']['publish'];?>,
					step:1,
					create: function( event, ui ) {
					  var value=$(this).slider('value');
					  $("#slider_<?php echo $currentid;?>").css("cursor","pointer");
                                          
					if(value==1)
		{
                    $("#slider_<?php echo $currentid;?>").css("background-color","green");
                    $("#slider_<?php echo $currentid;?>").css("border-radius","10px");
                }else
		{
                    $("#slider_<?php echo $currentid;?>").css("background-color","red");
                    $("#slider_<?php echo $currentid;?>").css("border-radius","10px");
		}
					
					},
		
       slide: function( event, ui ) {
       var value=ui.value;
       var id=<?php echo $currentid;?>;
       var locale="<?php echo $lang['Language']['locale']; ?>";
       $.ajax({
		url:ajax_url+'admin/Users/publishbanner',
		data:{value:value,id:id,locale:locale},
			type:'POST',
		success:function(resp)
		{
		
		if(resp!="1")
		{
		if(value==1)
		{
		$( "#slider_<?php echo $currentid;?>" ).css("background-color","green");
		}else
		{
		$( "#slider_<?php echo $currentid;?>" ).css("background-color","red");
		}
		}
		else
		{
		$( "#slider_<?php echo $currentid;?>" ).slider({ value: 0});
		alert("No More than five banner can be active");
		

		}
		}		
		});
		}
					
					});
				  });
				  </script>
				<div>
                                    <div style="padding-top:100px;"> 
                                        <div class="col-sm-3">Hide</div>
                                       <div class="col-sm-2" id="slider_<?php echo $currentid;?>"></div>
                                       <div class="col-sm-3">Show</div>
                                    </div>
				</div>
				</div>
				</li>
				
				<?php
				
				}  
				?>
				
				<label class="desc" >File input</label>
				<div > <?php echo $this->Form->input('image_'.$lang['Language']['locale'],array('id'=>'image_'.$lang['Language']['locale'].'_'.($banners[0]['Banners']['order']+1),'type'=>'file','div'=>false,'label'=>false,'class'=>'field text required imageuploadify upload-image')); ?> </div>
				<p class="university-add-Error" id="err_image">
				<?php if(isset($error['image'][0])) echo $error['image'][0]; ?>
				</p>
				<?php
				}
				else
				{
				?>
				<input type="hidden" id="lang_order" value="0" name="data[Banners][order_<?php echo $lang['Language']['locale']; ?>]"/>	
				
				<?php if($lang['Language']['locale']!='en')
				{
				?>
				
				<div style="float:left;">
				<?php
				echo $this->Form->input('image_'.$lang['Language']['locale'].'_upload',array('class'=>'imageupload','type'=>'checkbox','id'=>'image_'.$lang['Language']['locale'].'_upload','value'=>true,'checked'=>'checked','label'=>false));
				?>
				</div>
				<label>Show default locale banners</label>
				
				<?php } ?>
				
				<li <?php echo ($lang['Language']['locale']=='en') ? 'style="display:block;' : 'style="display:none;'?>" id="imageupload_<?php echo $lang['Language']['locale'];?>_input">
				<label class="desc" >Image</label>
				<div > <?php echo $this->Form->input('image_'.$lang['Language']['locale'],array('id'=>'image_'.$lang['Language']['locale'].'_0','type'=>'file','div'=>false,'label'=>false,'class'=>'field text required imageuploadify upload-image')); ?> </div>
				<p class="university-add-Error" id="err_image">
				<?php if(isset($error['image'][0])) echo $error['image'][0]; ?>
				</p>
				</li>
				
				
				<?php
				}
				
				?>
					
                                </ul>	
					
				
				</div>
			 <?php  $i++; }  ?>   
                            </div>
			<?php echo $this->form->end(); ?>  
          </div>
		</div>
		
     </div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul>
					<?php */?>
    
   
