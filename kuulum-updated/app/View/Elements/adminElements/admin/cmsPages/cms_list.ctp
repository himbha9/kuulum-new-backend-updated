<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'cmspages','admin'=>true))); ?>
 <div id="content" class="content">

	<div class="block">

		<div class="">
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                                          
						<th >Page Title</th>
						<th class="hidden-xs">Description</th>
                        <th >Modified</th>
                       <!--	<th width="5%">Action</th> -->
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $pages)
							{ 
						?>
                        		
                                <tr>
                                    
                                    <td><a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_cms/".$pages['CmsPage']['page']; ?>" ><?php echo $pages['CmsPage']['title']?></a></td>
                                    <td class="hidden-xs"><?php echo $this->Text->truncate(strip_tags($pages['CmsPage']['description']),150,array('ending'=>'...','exact'=>false));?></td>            
                                    <td><?php echo date('d/m/Y',$pages['CmsPage']['date_modified']);?></td>                     
                                  <!--  <td>
										<?php $id = base64_encode(convert_uuencode($pages['CmsPage']['id'])); ?>
										
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_cms/".$pages['CmsPage']['page']; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-pencil"></span>
                                        </a>										
                                        
                                    </td> -->
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<!--<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div> -->
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
