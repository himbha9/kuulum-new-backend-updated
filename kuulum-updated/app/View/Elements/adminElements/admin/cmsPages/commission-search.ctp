<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'commission','admin'=>true))); ?>
<div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
		
		
		<div id="search_commission">
		<?php
		$options=array('0'=>'By Author Name',
		'1'=>'By Author Email',
		'2'=>'By Commission Rate');
		?>
		<form action="<?= $this->Html->url('search'); ?>" method="POST" onsubmit="return checksearchvalidation();">
		<?php
		echo $this->Form->label('Search Option');
		
		echo $this->Form->input('CommissionSearch.search_by',array('options'=>$options,'type'=>'select','empty'=>'Select Option','label'=>false));
	
		echo $this->Form->input('CommissionSearch.search',array('type'=>'text','label'=>false));
		?>
        <div id="search-error"></div>
		<?php
		echo $this->Form->end('submit');
		?>
</form>
		</div>
			<table id="sort-table"> 
				<thead> 
					<tr>
						<th width="5%">S.No.</th>
                        <th>Select</th>
						<th width="10%" >Commission</th>
                        <th width="10%" >Duration</th>
                        <th width="10%" >Date From</th>
                        <th width="10%" >Date To</th>
                        	<th width="20%" >Author</th>
                            	<th width="20%" >Course</th>
                       	<th width="20%">Action</th> 
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $commission)
							{ 
						?>
                        		
                                <tr>
                                    <td><?php echo $i; ?></td>
                                     <td><input type="checkbox" name="checkboxes[]" class="checkboxes" value="<?php echo $commission['Commission']['id'];?>"></td>
									<td><?php echo $commission['Commission']['commission'].'%';?></td>
                                    
                                      <td><?php echo ($commission['Commission']['duration']==1)? 'Unlimited' : 'Limited';?></td>
                                    
                                    <td><?php echo ($commission['Commission']['duration']==1)? '' : $commission['Commission']['date_from'];?></td>
                                     <td><?php echo ($commission['Commission']['duration']==1)? '' : $commission['Commission']['date_to'];?></td>
                                    <td><?php 
									
									$membername=$this->Utility->getAuthorname($commission['Commission']['author_name']);
									echo $membername['Member']['given_name'];
									?></td>
                                               <td><?php
											   $coursename=$this->Utility->getCoursename($commission['Commission']['particular-course']);
											    echo ($commission['Commission']['particular-course']!='') ?  $coursename['Course']['title'] : 'All';?>
                                                </td>
                                    
                                    <td>
										<?php 
										
										$id = base64_encode(convert_uuencode($commission['Commission']['id'])); 
										
												$table= base64_encode('Commission');
												$renderPath=base64_encode('admin/cmsPages');
												$renderElement=base64_encode('commission_list');
												
												
												
										?>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_commission/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-pencil"></span>
                                        </a>										
                                        <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteCommission("admin/Users/delete_commission","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>	
                                    </td>
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7"><?php echo __('No Record Found.');?></td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div>
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
               <script>
			$(document).ready(function(){			
			$(".inline").colorbox({inline:true, width:"50%"});
			$("#delete").click(function(){
			$(this).addClass("active");
			var selected=[];
			var formhtml='';
			$(".checkboxes").each(function(index){
			if($(this).is(":checked")==true)
			{
			formhtml+="<input type='hidden' name='deleteid[]' value='"+$(this).val()+"'/>";
			selected.push($(this).val());
			}
							
						
						});
	
			if(selected.length>0)
			{
				$("#deletenumber").html(selected.length);
					$("#deletform").prepend(formhtml);
					return true;
			}
			else
			{
			alert("Please select atleast one of the course");
			return false;
			}
						
					});
					
					
				$("#modify").click(function(){
					$(this).addClass("active");
					var selected=[];
					var formhtml='';
					$(".checkboxes").each(function(index){
						if($(this).is(":checked")==true)
						{							
						
						selected.push($(this).val());
						}
							
						
						});
						
			
			if(selected.length>0)
			{
			$(".errorcheck").html("");
			document.getElementById("modifyform").reset();
			document.getElementById("modifyform1").reset();
			document.getElementById("modifyform2").reset();
			$("#durationdateselection").css("display","none");
			$("#durationdateselection1").css("display","none");
			$.datepicker._clearDate('#datepicker');
			$.datepicker._clearDate('#datepicker1');
			$.datepicker._clearDate('#datepicker2');
			$.datepicker._clearDate('#datepicker3');
			$("input[name='modifyid']").val(JSON.stringify(selected));
			return true;
			}
			else
			{
			alert("Please select atleast one of the course");
			return false;
			}
			});
					
					
		/*$("#duration").change(function(){
			if($("#duration").val()=='0')
			{
			
			$("#durarationselect").css("display","block");	
			}
			else if($("#duration").val()=='1')
			{
			$("#durarationselect").css("display","none");	
			
			}
			
			});
				*/
		
				});
				
				
				$(document).ready(function(){
				if($("input[name='only-commission']").is(':checked')==true)
					{
					$("#form1").css("display","block");	
					}
					$("input[name='only-commission']").click(function(){
					$(".errorcheck").html("");
							$("input[name='only-commission']").prop('checked',true);
							$("input[name='only-duration']").prop('checked',false);
						$("input[name='both']").prop('checked',false);
						
						if($("input[name='only-commission']").is(':checked')==true)
					{
				$("#form1").css("display","block");	
						$("#form2").css("display","none");	
					$("#form3").css("display","none");	
					}
						
						});
				
						$("input[name='only-duration']").click(function(){
							$(".errorcheck").html("");
							$("input[name='only-duration']").prop('checked',true);
							
							$("input[name='only-commission']").prop('checked',false);
						$("input[name='both']").prop('checked',false);
						
						if($("input[name='only-duration']").is(':checked')==true)
					{
					$("#form1").css("display","none");	
						$("#form2").css("display","block");	
					$("#form3").css("display","none");	
					}
						
						});
					
						$("input[name='both']").click(function(){
					$(".errorcheck").html("");
						if($("input[name='both']").is(':checked')==true)
					{
							$("input[name='both']").prop('checked',true);
							$("input[name='only-commission']").prop('checked',false);
						$("input[name='only-duration']").prop('checked',false);
						$("#form1").css("display","none");	
						$("#form2").css("display","none");	
					$("#form3").css("display","block");	
					}
						
						});
						
					});
				function deleteno()
				{
					
				$("#delete").removeClass("active");
				$("#alert").html("");	
				}
				function modifyno()
				{
					
				$("#delete").removeClass("active");
				$("#alert").html("");	
				}
            </script>
