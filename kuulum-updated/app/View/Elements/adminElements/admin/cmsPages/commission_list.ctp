<link rel="stylesheet" href="/kuulum/js/admin/colorbox.css" />
<script src="/kuulum/js/admin/jquery.colorbox.js"></script>
<div class="row">
<div class="row" style="margin-top: 10px !important;"></div>	        
<div class="col-sm-12">
<div class="btn-group">
<button type="button" class="btn btn-default">Actions</button>
<div class="btn-group">
<button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
<span class="caret"></span>
</button>
<ul class="dropdown-menu">
<li>
<a href="<?php echo HTTP_ROOT.'admin/users/add_commission'; ?>" tabindex="-1">Add</a>
</li>
<li>
<a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="row" style="margin-top: 10px !important;"></div>	
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
			
        <thead>
		<tr>
                        <th class="text-center" style="width: 70px;">
                        <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                        <input id="check-all" type="checkbox" name="check-all">
                        <span></span>
                        </label>
                        </th>
			<th class="col-sm-2">Author</th>		
                        <th>Commission</th>
                        <th class="hidden-xs">Duration</th>
                        <th class="hidden-xs">From</th>
                        <th class="hidden-xs">To</th>
                        <th class="hidden-xs">Courses</th>
                        </tr> 
                        </thead>				
                        <tbody> 
			<?php


						if(!empty($info))
						{
						foreach($info as $row=>$commission)
                                                { 
						?>
                        		
                                    <tr>
                                        <td class="text-center"> <label class="css-input css-checkbox css-checkbox-primary">
                                        <input type="checkbox" name="row_<?php echo $row;?>" value="<?php echo $commission['Commission']['package_id']; ?>" class="rows checkboxs">
                                        <span></span></label></td>
                                        <td>
                                        <?php									
                                        $membername=$this->Utility->getAuthorname($commission['Commission']['author_name']);

                                        echo $this->Html->link(
                                                                            $membername['Member']['given_name'],
                                                                            array('controller' => 'users', 'action' => 'edit_commission', base64_encode(convert_uuencode($commission['Commission']['package_id']))),
                                                                            array('escape' => false)
                                                                            
                                                                        ); 

                                        ?>
                                        </td>
                                        <td>
                                        <?php echo $commission['Commission']['commission'].'%';?>
                                        </td>
                                        <td class="hidden-xs">
                                        <?php echo ($commission['Commission']['duration']==1)? 'Unlimited' : 'Limited';?>
                                        </td>                                    
                                        <td class="hidden-xs">
                                        <?php echo ($commission['Commission']['duration']==1)? '' : date('d/m/Y',strtotime($commission['Commission']['date_from']));?>
                                        </td>
                                        <td class="hidden-xs">
                                        <?php echo ($commission['Commission']['duration']==1)? '' : date('d/m/Y',strtotime($commission['Commission']['date_to'])); ?>
                                        </td>
                                        <td class="hidden-xs">
                                        <?php
                                        //$coursename=$this->Utility->getCoursename($commission['Commission']['particular-course']);
                                        //echo ($commission['Commission']['courses']==1) ?  $commission['Commission']['coursestitle_selected'] : 'All';

                                        if($commission['Commission']['courses']==1)
                                        {
                                            $courseids=explode(",",$commission['Commission']['coursesid_selected']);
                                            $courses=array();
                                            foreach($courseids as $courseid)
                                            {
                                            $coursename=$this->Utility->getCoursename($courseid);

                                            $courses[]=$coursename['Course']['title'];
                                            }
                                            echo implode(",",$courses);
                                        }
                                        else
                                        {
                                            echo 'All';
                                        }
                                        ?></td>
                                        </tr> 
                                        <?php	
						
                                        }
                                        }
                                        else
                                        {
					?>
					<tr>
					<td colspan="7"><?php echo __('No Record Found.');?></td>
					</tr>
					<?php		
					}
					?>							
					</tbody>
                                        </table>
                                     
  
          
        	
		<div class="clear"></div>

            
            
            <script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/deletecommissions'; ?>",
                            type:'POST',
                            data:{selectedids:selected},
                            success:function(result){
                                if(result)
                                {
                                    
                                    window.location.href="<?php echo HTTP_ROOT.'admin/users/commission'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one of the course");
			
			}
						
					});
					
					
//				$("#modify").click(function(){
//					$(this).addClass("active");
//					var selected=[];
//					var formhtml='';
//					$(".checkboxes").each(function(index){
//						if($(this).is(":checked")==true)
//						{							
//						
//						selected.push($(this).val());
//						}
//							
//						
//						});
//						
//			
//			if(selected.length>0)
//			{
//			$(".errorcheck").html("");
//			document.getElementById("modifyform").reset();
//			document.getElementById("modifyform1").reset();
//			document.getElementById("modifyform2").reset();
//			$("#durationdateselection").css("display","none");
//			$("#durationdateselection1").css("display","none");
//			$.datepicker._clearDate('#datepicker');
//			$.datepicker._clearDate('#datepicker1');
//			$.datepicker._clearDate('#datepicker2');
//			$.datepicker._clearDate('#datepicker3');
//			$("input[name='modifyid']").val(JSON.stringify(selected));
//			return true;
//			}
//			else
//			{
//			alert("Please select atleast one of the course");
//			return false;
//			}
//			});
					
					
		/*$("#duration").change(function(){
			if($("#duration").val()=='0')
			{
			
			$("#durarationselect").css("display","block");	
			}
			else if($("#duration").val()=='1')
			{
			$("#durarationselect").css("display","none");	
			
			}
			
			});
				*/
		
				});
//				
//				
//				$(document).ready(function(){
//				if($("input[name='only-commission']").is(':checked')==true)
//					{
//					$("#form1").css("display","block");	
//					}
//					$("input[name='only-commission']").click(function(){
//					$(".errorcheck").html("");
//							$("input[name='only-commission']").prop('checked',true);
//							$("input[name='only-duration']").prop('checked',false);
//						$("input[name='both']").prop('checked',false);
//						
//						if($("input[name='only-commission']").is(':checked')==true)
//					{
//				$("#form1").css("display","block");	
//						$("#form2").css("display","none");	
//					$("#form3").css("display","none");	
//					}
//						
//						});
//				
//						$("input[name='only-duration']").click(function(){
//							$(".errorcheck").html("");
//							$("input[name='only-duration']").prop('checked',true);
//							
//							$("input[name='only-commission']").prop('checked',false);
//						$("input[name='both']").prop('checked',false);
//						
//						if($("input[name='only-duration']").is(':checked')==true)
//					{
//					$("#form1").css("display","none");	
//						$("#form2").css("display","block");	
//					$("#form3").css("display","none");	
//					}
//						
//						});
//					
//						$("input[name='both']").click(function(){
//					$(".errorcheck").html("");
//						if($("input[name='both']").is(':checked')==true)
//					{
//							$("input[name='both']").prop('checked',true);
//							$("input[name='only-commission']").prop('checked',false);
//						$("input[name='only-duration']").prop('checked',false);
//						$("#form1").css("display","none");	
//						$("#form2").css("display","none");	
//					$("#form3").css("display","block");	
//					}
//						
//						});
//						
//					});
//				function deleteno()
//				{
//					
//				$("#delete").removeClass("active");
//				$("#alert").html("");	
//				}
//				function modifyno()
//				{
//					
//				$("#delete").removeClass("active");
//				$("#alert").html("");	
//				}
            </script>
            

            
            
            
            
