<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
               <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">

                                                        <li>
                                                            <a href="<?php echo HTTP_ROOT.'admin/users/add_discount'; ?>" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                             </div>
                                            </div>

            <div class="row" style="margin-top: 10px !important;"></div>	

                        <!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
                                <thead> 
                                        <tr>
                                            <th class="text-center" style="width: 70px;">
                                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                                            <input id="check-all" type="checkbox" name="check-all">
                                            <span></span>
                                            </label>
                                            </th>

                                                <th >Title</th>
                                                <th class="hidden-xs">Description</th>
                                                <th>Value</th>
                                                <th class="hidden-xs">Code</th>
                                                <th class="hidden-xs col-sm-1">Status</th> 
                                        </tr> 
                                </thead> 
                                <tbody> 

                                <?php 
                                                if(!empty($info))
                                                {
                                                        $i = $this->Paginator->counter('%start%');
                                                        foreach($info as $row=>$price)
                                                        { 
                                                ?>

                                <tr>
                                     <td class="text-center">
                                            <label class="css-input css-checkbox css-checkbox-primary">
                                                <input type="checkbox" name="row_<?php echo $row;?>" value="<?php echo base64_encode(convert_uuencode($price['Discount']['discount_code'])); ?>" class="rows checkboxs">
                                                <span>
                                                </span>
                                            </label>
                                        </td>
                                        
                                        
                                        
                                    <td><?php 
                                                                            echo $this->Html->link(
                                                                            $price['Discount']['name_en'],
                                                                            array('controller' => 'users', 'action' => 'edit_discount', base64_encode(convert_uuencode($price['Discount']['discount_code']))),
                                                                            array('escape' => false)
                                                                            
                                                                        ); ?></td>
                                    <td class="hidden-xs"><?php echo $this->Text->truncate(strip_tags($price['Discount']['description_en']),150,array('ending'=>'...','exact'=>false));?></td>            
                                    <td><?php echo $price['Discount']['initial_value'];?></td>

                                                                        <td class="hidden-xs"><?php echo $price['Discount']['discount_code'];?></td>

 <td class="hidden-xs" style="padding:15px"><?php if(@$price['Discount']['status']==0)
                                        {
                                                echo "<span class='label label-danger'>Inactive</span>";
                                        }
                                       else
                                        {
                                            echo "<span class='label label-success'>Active</span>";
                                        } 
                                        ?></td>
 <!-- <td>
                                                                                <?php $id = base64_encode(convert_uuencode($price['Discount']['id'])); 
                                                                                                $table= base64_encode('Discount');
                                                                                                $renderPath=base64_encode('admin/cmsPages');
                                                                                                $renderElement=base64_encode('discounts_list');
                                                                                ?>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_discount/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-pencil"></span>
                                        </a>										
                                        <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
                                                                                        <span class="ui-icon ui-icon-circle-close"></span>
                                                                                </a>	
                                    </td> -->
                                </tr> 
                        <?php	
                                                                $i++;	
                                                        }
                                                } else {
                                        ?>
                                                        <tr>
                                                                <td colspan="7">No Record Found.</td>
                                                        </tr>
                                        <?php		
                                                }
                                        ?>							
                                        </tbody>
                                </table>
                                <div class="clear"></div>
                        <!--	<div id="pager">					

                                        <?php echo $this->element('adminElements/table_head'); ?>

                                </div> -->
                        </div>
                <div class="clear"></div>			
                </div>
                <div class="clear"></div>
        </div> 


        <script>
        $("#remove_row").click(function()
        {
            var discount_codes=new Array();
            $(".rows").each(function()
            {
                if($(this).is(":checked")==true)
                {
                    discount_codes.push($(this).val());
                }                
            })
            
            if(discount_codes.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/delete_discounts'; ?>",
                type:'POST',
                data:{discounts:discount_codes},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/discounts'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one discount to delete");
            }
           
        });
        $("#toggle_row").click(function()
        {
            var discount_codes=new Array();
            $(".rows").each(function()
            {
                if($(this).is(":checked")==true)
                {
                    discount_codes.push($(this).val());
                }                
            })
            
            if(discount_codes.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_discount_status'; ?>",
                type:'POST',
                data:{discounts:discount_codes},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/discounts'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one discount to change status");
            }
           
        });
        </script>
