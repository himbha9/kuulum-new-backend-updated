
<div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
						<th width="15%">Featured Courses</th>
						<th width="30%" >Featured By</th>
                        <th width="30%" >Date Modified</th>
                        <th width="30%" >Options</th>
                         
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($feature))
						{
								switch ($feature['FeatureCourse']['featured_by'])
								{
									case 1:
										$feature_by="Most frequently watched";
										break;
									case 2:
										$feature_by="Most recently published";
										break;
									case 3:
										$feature_by="Alphabetical title";
										break;
									case 4:
										$feature_by="Randomly";
										break;
									
								}
						?>
                        		
                                <tr>
                                	<td>Featured Courses</td>
                                    <td><?php echo $feature_by;?></td>
                                    <td><?php echo $feature['FeatureCourse']['modified'];?></td>                     
                                    <td>
									<?php 
												$id = base64_encode(convert_uuencode($feature['FeatureCourse']['id'])); 
												
										?>
										
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_featured_course/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
																		
                                        
                                    </td>
                                </tr> 
                	<?php	
								
							
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<?php /*?><div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div><?php */?>
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
