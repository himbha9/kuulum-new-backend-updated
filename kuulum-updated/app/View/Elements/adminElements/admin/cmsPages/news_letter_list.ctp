<?php  echo $this->Html->script('newadmin/tablesorter.js');?>

<script type="text/javascript">
$(document).ready(function() {
	///* Table Sorter */
//	$("#sort-table")
//	.tablesorter({
//		widgets: ['zebra'],
//		headers: { 
//		            // assign the secound column (we start counting zero) 
//		            /*1: { 
//		                // disable it by setting the property sorter to false 
//		                sorter: false 
//		            }, */
//		            // assign the third column (we start counting zero) 
//		            4: { 
//		                // disable it by setting the property sorter to false 
//		                sorter: false 
//		            } 
//		        } 
//	})
//	
//	$(".header").append('<span class="ui-icon ui-icon-carat-2-n-s"></span>');
	
	$('.delRec').click(function(){
				
				if(confirm('Do you really want to delete this Newsletter?'))
				{
					$this = $(this);
					var id = $this.attr('id');
					
					$.post(ajax_url+'/admin/users/delete_newsletter/', { id: id }, function(resp){
						
						$this.parents('tr').fadeOut(2,function(){
	
							$(this).remove();
							$('#success').hide();
							$('#resp_msg').show();
							
						});
					
					});
				}
				return false;					
		});

	
});
</script>

<div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
					    <th width="4%" >S.No</th>
						<th width="15%">Title</th>
						<th width="30%">Description</th>
						<th width="8%">Action</th>
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $info)
							{ 
						?>
                                <tr>
									<td> <?php echo $i; ?> </td>
									<td> <?php echo $info['Newsletter']['title']; ?> </td>
									
									<td><?php echo $this->Text->truncate($info['Newsletter']['description'],200,array('ellipsis'=>'...','exact'=>false));?></td>
									<td>
										<?php $id = base64_encode(convert_uuencode($info['Newsletter']['id']));	?>
																			
										<a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_news_letter/".$id;?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-pencil"></span>
                                        </a>
																				
										<a href="javascript:void(0);" title="Delete" class="btn_no_text btn ui-state-default ui-corner-all tooltip delRec" id="<?php echo $id; ?>">
                                            <span class="ui-icon ui-icon-circle-close"></span>
                                        </a>
										
										<a title="Send Newsletter" href="<?php echo HTTP_ROOT."admin/users/send_newsletter/".$id;?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-mail-closed"></span>
                                        </a>
										
									</td>
										                                    
                                </tr> 
                	<?php	
							$i++;		
							}
						} else {
					?>
							<tr>
								<td colspan="8">No Record Found</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				
				<div class="clear"></div>
				<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div>
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            