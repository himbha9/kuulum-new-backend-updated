<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'news','admin'=>true))); ?>
 <div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
            
         
                                        
            
            <?php echo $this->Form->create('search',array('id'=>'searchNews',"class"=>"form-inline")); ?>
            <div class="form-group">
                <div class="btn-group">
                    <button type="button" class="btn btn-default">Actions</button>
                        <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                        <li><a href="<?php echo HTTP_ROOT.'admin/users/add_news'; ?>" tabindex="-1">Add</a></li>
                        <li><a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a></li>
                        <li><a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a></li>
                        </ul>
                        </div>
                </div>
            </div>    
            <div class="form-group">
                <div class="btn-group">
                    <button type="button" class="btn btn-default">Filters</button>
                    <div class="btn-group">
                      <?php echo $this->Form->input('type',array('type'=>'select','id'=>'member_email','div'=>false, 'style'=>'width:148px; padding:4px; border:1px solid #abadb3;' ,'label'=>false,'class'=>'field text full required','options'=>array('name'=>'By Title','date_register'=>'By Date','status'=>'By Status'))); ?>
                    </div>
                </div>                              
             </div>                                    
            <div class="form-group">
                <div class="showHideCoursetText"> 
                    
                 <?php echo $this->Form->input('text',array('type'=>'text','id'=>'email_search','div'=>false,'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input')); ?>    
                </div>  
            </div>
            
            <div class="form-group">
            <div class="hideStartEndDate"> 
<?php echo $this->Form->input('register_date',array('type'=>'text','id'=>'register_date','readonly'=>'readonly','div'=>false, 'label'=>false,'class'=>'datepicker form-control field text full required col-sm-3')); ?></div>
            </div>
            <div class="form-group">
                <div class="hideStatusType">
                    <?php echo $this->Form->input('status',array('type'=>'select','id'=>'mem_status','div'=>false, 'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input','options'=>array('1'=>'Active','0'=>'In-active'))); ?> </div>
            </div>    
            <div class="form-group">    
            <div class="box_padding_left moveLeft">                        
            <?php echo $this->Form->submit('Search',array('div'=>false,'label'=>false,'id'=>'searchMemberList','class'=>'btn btn-sm btn-primary')); ?></div>  
            </div>
           
            <?php echo $this->Form->end(); ?>                
                                                    
  
                                    
   </div>
                                            </div>
        
	<div class="row" style="margin-top: 10px !important;"></div>	
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                                            <th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
						<th>News Title</th>
						<th class="hidden-xs">Description</th>
						<th class="hidden-xs">Author</th>
			                        <th class="col-sm-1 hidden-xs">added </th>	
                       				<th class="col-sm-1 hidden-xs">Status</th> 
					</tr> 
				</thead> 
				<tbody> 
				
				<?php //pr($info);die;
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=> $pages)
							{ 
						?>
                        		
                                <tr>
                                     <td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                                    <input id="row_<?php echo $row; ?>" type="checkbox" name="row_1" class="rows checkboxs" value="<?php echo $pages['News']['id']; ?>">
                                                                    <span></span>
                                                                    </label>
                                                                    </td>
                                    <td>
                                      <?php  $id = base64_encode(convert_uuencode($pages['News']['id'])); ?>
                                   <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_news/".$id; ?>"><?php echo $pages['News']['title_en'];?> </a></td>
                                    <td class="hidden-xs"><?php echo $this->Text->truncate(strip_tags($pages['News']['description_en']),150,array('ending'=>'...','exact'=>false));?></td>
									<td class="hidden-xs"><?php echo $pages['Admin']['username'];?></td>
                           <!--         <td><?php echo date('d M Y, h:i e',$pages['News']['date_added']);?></td> -->
                                                                        <td class="hidden-xs"> <?php echo date('d/m/Y',$pages['News']['date_added']); ?></td>
                                    <td style="padding:15px;" class="hidden-xs"><?php 
                                    if($pages['News']['status']==0)
                                    {
                                    echo "<span class='label label-danger'>InActive</span>";
                                        }
                                        else
                                        {
                                                echo "<span class='label label-success'>Active<span>";
                                        }
                                    
                                    ?></td>  
                                  <!--  <td>
									<?php 
												$id = base64_encode(convert_uuencode($pages['News']['id'])); 
												$table= base64_encode('News');
												$renderPath=base64_encode('admin/cmsPages');
												$renderElement=base64_encode('news_list');
										?>
										<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_news/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_news/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($pages['News']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                  
										
										<a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>								
                                        
                                    </td> -->
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<!--<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div> -->
			</div>
        	<div class="clear"></div>			
	
		<div class="clear"></div>
	
      <script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"News"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/news'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one of the news");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"News"},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/news'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one news to change status");
            }
           
        });
 });
</script>      
