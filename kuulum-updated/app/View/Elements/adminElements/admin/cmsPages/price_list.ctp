<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'price','admin'=>true))); ?>
<div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
						<th width="7%">S.No.</th>
						<th width="10%">Name (English)</th>
						<th width="17%" >Description (English)</th>
						<th width="10%">Name (German)</th>
						<th width="17%" >Description (German)</th>
						<th width="7%" >Number of Months</th>
                        <th width="13%" >Initial Value</th>
                       	<th width="20%">Action</th> 
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $price)
							{ 
						?>
                        		
                                <tr>
									<td><?php echo $i;?></td>
                                    <td><?php echo $price['Price']['name_en'];?></td>
									
                                    <td><?php echo $this->Text->truncate(strip_tags($price['Price']['description_en']),150,array('ending'=>'...','exact'=>false));?></td>
									<td><?php echo $price['Price']['name_de'];?></td>
									
                                    <td><?php echo $this->Text->truncate(strip_tags($price['Price']['description_de']),150,array('ending'=>'...','exact'=>false));?></td>            
                                    <td><?php echo $price['Price']['no_of_months']; ?></td>
									<td><?php echo "USD ".$price['Price']['initial_value'];?></td>                     
                                    <td>
										<?php $id = base64_encode(convert_uuencode($price['Price']['id'])); 
												$table= base64_encode('Price');
												$renderPath=base64_encode('admin/cmsPages');
												$renderElement=base64_encode('price_list');
										?>
										
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_price/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-pencil"></span>
                                        </a>
                                        <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>										
                                        
                                    </td>
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div>
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
