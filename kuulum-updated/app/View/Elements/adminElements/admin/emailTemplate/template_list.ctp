<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'email_templates','admin'=>true))); ?>
<div class="row" style="margin-top: 10px !important;"></div>
<div class="row" style="margin-top: 10px !important;"></div>	
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info"> 
				<thead> 
					<tr>
                                            <th class="text-center" style="width: 3%">#</th>
                        <th>Title</th>
						<th>Subject</th>
						<!--<th>Description</th>
						<th>Action</th> -->
					</tr> 
				</thead> 
				<tbody> 
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $info)
							{ 
						?>
								<tr>
                                                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td>
                                        <?php $email_id = base64_encode(convert_uuencode($info['EmailTemplate']['main_id'])); ?>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_email_template/".$email_id; ?>" ><?php echo $info['EmailTemplate']['title'] ; ?></a></td>
									<td><?php echo $info['EmailTemplate']['subject'] ; ?></td>
									<!--<td><?php echo $this->Text->truncate(strip_tags($info['EmailTemplate']['description']),200,array('ending'=>'...','exact'=>false,'html'=>true));?></td>                                 	
									<td>
										<?php $email_id = base64_encode(convert_uuencode($info['EmailTemplate']['main_id'])); ?>
										
										<a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_email_template/".$email_id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
		
									</td> -->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>					
				</tbody>
			</table>
			<div class="clear"></div>
			
			<!--<div id="pager">					
				
				<?php echo $this->element('adminElements/table_head'); ?>
				
			</div> -->
			
		
		<div class="clear"></div>

	<div class="clear"></div>

