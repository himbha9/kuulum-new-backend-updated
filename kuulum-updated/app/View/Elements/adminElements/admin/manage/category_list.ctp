
<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
 <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
                                                            <a href="<?php echo HTTP_ROOT; ?>admin/users/add_category" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>
<li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a>
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                       
   </div>
                                            </div>
        
	<div class="row" style="margin-top: 10px !important;"></div>	        
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                        <th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
                        <th>Title</th> 
			<th class="hidden-xs" width="20%">Date Added</th>
                        <th class="hidden-xs">Date Modified</th>
                        <th class="hidden-xs col-sm-1">Status</th>
			<!--<th width="15%">Action</th> -->
			</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$info)
							{ 
						?>
								<tr>
                                                                  <td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                                    <input id="row_<?php echo $row; ?>" type="checkbox" name="row_1" class="checkboxs rows" value="<?php echo $info['CourseCategory']['id']; ?>">
                                                                    <span></span>
                                                                    </label>
                                                                    </td>
                                                                    <td><?php 
												$id = base64_encode(convert_uuencode($info['CourseCategory']['id']));  ?>
                                                                  <!--      <a title="View Sub-Categories" href="<?php echo HTTP_ROOT."admin/users/course_subcategory/".$id; ?>" >-->
 <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_category/".$id; ?>" ><?php echo $info['CourseCategory']['en']; ?></a></td>
									<td class="hidden-xs"><?php echo date('d/m/Y',strtotime($info['CourseCategory']['created'])); ?></td> 
									<td class="hidden-xs"><?php echo date('d/m/Y',strtotime($info['CourseCategory']['created'])); ?></td>
									<td style='padding:15px' class="hidden-xs">
									<?php 
										if($info['CourseCategory']['status']==0)
										{
											echo "<span class='label label-danger'> Inactive</span>";
										}
										elseif($info['CourseCategory']['status']==1)
										{
											echo "<span class='label label-success'>Active</span>";
										}
										
									  ?>
                                      </td> 
									<!--<td>
										<?php 
												$id = base64_encode(convert_uuencode($info['CourseCategory']['id'])); 
												$table= base64_encode('CourseCategory');
												$renderPath=base64_encode('admin/manage');
												$renderElement=base64_encode('category_list');
												//$memberid=base64_encode(convert_uuencode($info['CourseCategory']['m_id']));
												
										?>
										
										<a title="View Sub-Categories" href="<?php echo HTTP_ROOT."admin/users/course_subcategory/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_category/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($info['CourseCategory']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                        <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
										<?php /*?><a title="View Lesson"href="<?php echo HTTP_ROOT."admin/users/lessons/".$id."/".$memberid; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-note"></span>
										</a><?php */?>
										
                                       
									</td>-->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
			<!--<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"CourseCategory"},
                            success:function(result){
                                if(result)
                                {                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/course_categories'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one record");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"CourseCategory"},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/course_categories'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one record.");
            }
           
        });
 });
</script>
