
<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
 <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
                                                            <a href="<?php echo HTTP_ROOT; ?>admin/users/add_interest" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>
<li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a>
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                       
   </div>
                                            </div>
        
	<div class="row" style="margin-top: 10px !important;"></div>	        
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                        <th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
						<th >Name</th>
						<th class='col-sm-3 hidden-xs'>Date Added</th>
          <th class="col-sm-1 hidden-xs">Status</th>
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$interest)
							{ 
						?>
                        		
                                <tr>
								<?php 
												$id = base64_encode(convert_uuencode($interest['Interest']['id'])); 
												$table= base64_encode('Interest');
												$renderPath=base64_encode('admin/manage');
												$renderElement=base64_encode('interest_list');												
												
										?>
                                    <td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                                    <input id="row_<?php echo $row; ?>" type="checkbox" name="row_1" class="rows checkboxs" value="<?php echo $interest['Interest']['id']; ?>">
                                                                    <span></span>
                                                                    </label>
                                                                    </td>
                                                                    <td> <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_interest/".$id; ?>" ><?php echo $interest['Interest']['interest']; ?></a></td>
                                    <td class="hidden-xs"><?php echo date('d/m/Y',$interest['Interest']['date_added']); ?></td>   
<td style="padding:15px;" class="hidden-xs">
<?php
                             
                                
                                if( $interest['Interest']["status"]==0)
                                    {
                                            echo "<span class='label label-danger'>Inactive</span>";
                                    }
                                else{
                                        echo "<span class='label label-success'>Active<span>";
                                }
?>
</td>                  
                                   <!-- <td>
										<?php $id = base64_encode(convert_uuencode($interest['Interest']['id'])); ?>
										
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_interest/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-pencil"></span>
                                        </a>
										 <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($interest['Interest']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>										
                                        
                                    </td> -->
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="3">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<!--<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div> -->
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
<script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"Interest"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/interest'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one record");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"Interest"},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/interest'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one record.");
            }
           
        });
 });
</script>
