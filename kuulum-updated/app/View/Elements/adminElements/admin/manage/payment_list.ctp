<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'payments','admin'=>true))); ?>
<div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
						<th width="20%">Name</th>
						<th width="15%" >Payment Amount</th>
						<th width="15%" >Payment Status</th>
						<th width="15%" >Transaction Id</th>
						<th width="15%" >Date Purchesed</th>
                       	<th >Action</th> 
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $payment)
							{ 
						?>
                        		
                                <tr>
								<?php 
												$id = base64_encode(convert_uuencode($payment['CoursePurchase']['id'])); 
												$table= base64_encode('CoursePurchase');
												$renderPath=base64_encode('admin/manage');
												$renderElement=base64_encode('payment_list');												
												
										?>
                                    <td><?php echo $payment['Member']['final_name']?></td>
									<td><?php echo $payment['CoursePurchase']['payment_amount']; ?></td>
                                    <td><?php echo $payment['CoursePurchase']['payment_status']; ?></td>
									<td><?php echo $payment['CoursePurchase']['txn_id']; ?></td>
									<td><?php echo date('d M Y, h:i e',$payment['CoursePurchase']['date_added']); ?></td>
                                    <td>
										<?php $id = base64_encode(convert_uuencode($payment['CoursePurchase']['id'])); ?>
										
										<a title="View Purchesed Courses" href="<?php echo HTTP_ROOT."admin/users/view_course_purchesed/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
										 <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
										
                                    </td>
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div>
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
