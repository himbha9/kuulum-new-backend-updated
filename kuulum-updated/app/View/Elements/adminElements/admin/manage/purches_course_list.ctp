<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'CoursePurchaseList','admin'=>true))); ?>
<div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
						<th width="20%">Title</th>
						<th width="50%" >Author Name</th>
						<th >Date Added</th>
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $course)
							{ 
						?>
                        		
                                <tr>
                                    <td><?php echo $course['Course']['title']?></td>
                                    <td><?php echo $course['Course']['Member']['final_name']; ?></td>
                                    <td><?php echo date('d M Y, h:i e',$course['Course']['date_added']); ?></td>
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div>
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
