<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
         <div class="col-sm-12">
        <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                     <li>
                                                            <a href="<?php echo HTTP_ROOT.'admin/users/add_reason'; ?>" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
                                        
        
                </div>
    </div>

        
	<div class="row" style="margin-top: 10px !important;"></div>	        
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped js-table-checkable js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
				
					<tr>
						<th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
						<th >Reason</th>
						<th class="col-sm-2">Date</th>
                   <!--    	<th >Action</th>  -->
					</tr> 
				</thead> 
				<tbody> 
				
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$info)
							{
					?>
								<tr>                								<td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                                    <input type="checkbox" name="row_<?php echo $row;?>" value="<?php echo $info['Reason']['id']; ?>" class="rows checkboxs" >
                                                                    <span></span>
                                                                    </label>
                                                                    </td>	
									
                                    <td>
<?php $id = base64_encode(convert_uuencode($info['Reason']['id']));  ?><a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_reason/".$id; ?>" >
<?php echo $info['Reason']['en']; ?></a>
</td>
                                    <td><?php echo date('d/m/Y',strtotime($info['Reason']['date'])); ?></td>                     
                                <!--    <td>									
										<?php $id = base64_encode(convert_uuencode($info['Reason']['id'])); 
											
												$table= base64_encode('Reason');
												$renderPath=base64_encode('admin/manage');
												$renderElement=base64_encode('reasons_list');
										?>
										
										
										<a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_reason/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
                                    </td> -->
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="3">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<!--<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div> -->
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
<script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"Reason"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/reasons'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one reason.");
			
			}
						
					});
});
</script>