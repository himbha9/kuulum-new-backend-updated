<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_courses'))); ?>
  <div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
 <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
                                                            <a href="<?php echo HTTP_ROOT; ?>admin/users/add_role_responsibility" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>

                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                   
   </div>
                                            </div>
        
	<div class="row" style="margin-top: 10px !important;"></div>	        
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                        <th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>				
                        <th>Role</th>
						<th class="hidden-xs">Permissions</th> 						
					<!--	<th>Action</th> -->
					</tr> 
				</thead> 
				<tbody> 
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');							
							foreach($info as $row=>$info)
							{ 
								$res = array();
						?>
								<tr>									 <td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                                    <input id="row_<?php echo $row;?>" type="checkbox" class="rows checkboxs" name="row_1" value="<?php echo $info['Role']['id']; ?>">
                                                                    <span></span>
                                                                    </label>
                                                                    </td>
									<td>  <?php $id = base64_encode(convert_uuencode($info['Role']['id']));  ?> <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_role_responsibility/".$id; ?>" >
											<?php echo $info['Role']['role']; ?>
										</a> </td>
									<td class="hidden-xs"><?php if($info['Role']['BC'] == '1') {  
													$res[] = 'Browse Catalogue';
                                                  ?> 
                                                <?php } ?> 
                                                <?php if($info['Role']['EAP'] == '1') {  
													$res[] = 'Enter authenticated Partition';
                                                  ?>
                                                <?php } ?> 
                                                <?php if($info['Role']['EOM'] == '1') {  
												$res[] = 'Edit own membership';
												?>
                                                <?php } ?> 
                                                <?php if($info['Role']['PC'] == '1') {  
												$res[] = 'Purchase course';
												?>
                                                <?php } ?> 
                                                <?php if($info['Role']['PS'] == '1') { 
												$res[] = 'Purchase subscription';
												 ?>
                                                <?php } ?> 
                                                <?php if($info['Role']['CC'] == '1') { 
												$res[] = 'Contribute courses';
                                                  ?>
                                                <?php } ?> 
                                                <?php if($info['Role']['PN'] == '1') { 
												$res[] = 'Publish news';
                                                 ?>   
                                                <?php } ?> 
                                                <?php if($info['Role']['MM'] == '1') {  
												$res[] = 'Manage members';
                                                  ?> 
                                                <?php } ?>
                                                 <?php if($info['Role']['MC'] == '1') {  
												 $res[] = 'Manage courses';
                                                  ?> 
                                                <?php } ?>
                                                 <?php if($info['Role']['MP'] == '1') { 
												 $res[] = 'Manage programmes';
                                                ?>
                                                <?php } ?> 
                                                <?php if($info['Role']['MA'] == '1') {  
												$res[] = 'Manage administrators';
												?>                                                  
                                                <?php } ?>
                                                <?php if($info['Role']['VCFR'] == '1') {  
												$res[] = 'View company financial reports';
												?>
                                                   
                                                <?php } ?>
												<div class="hastable_td_inner_content">   <?php  echo  implode(',',$res);?></div>
												</td>
								<!--	<td> 
										<?php 
												$id = base64_encode(convert_uuencode($info['Role']['id'])); 
												$table= base64_encode('Role');
												$renderPath=base64_encode('admin/manage');
												$renderElement=base64_encode('role');
										?>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_role_responsibility/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										
									</td>-->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
		<!--	<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	
	<div class="clear"></div>
 <script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"Role"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/role_responsibility'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one record");
			
			}
						
					});
                                        });
                                        </script>
