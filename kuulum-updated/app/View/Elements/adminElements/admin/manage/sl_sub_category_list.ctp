
<script type="text/javascript">
	
	$(document).ready(function() {

		$('.updateStatus').live('click',function(){
			
			$this = $(this);
			var id = $this.attr('id');
			var chk = jQuery.trim($this.attr('class'));
			
			$(".loading").show();
			
			if(chk.match('ui-state-hover'))
			{
				oldClass = 'ui-state-hover';
				newClass = 'ui-state-default';
			}
			else 
			{
				oldClass = 'ui-state-default';
				newClass = 'ui-state-hover';
			}
			
			$.post(ajax_url+'/admin/users/setStatus/CourseSlSubcategory/', { id: id }, function(resp){
				
				if($.trim(resp) == 'done')
				{
					$this.removeClass(oldClass).addClass(newClass);
					$(".loading").hide();
				}
			});		
		
			return false;
		
		});

		$('.delRec').click(function(){

			if(confirm('Do you really want to delete this record?'))
			{				
				$(".loading").show();
				$this = $(this);
				var id = $(this).attr('id');
				$.post(ajax_url+'/admin/users/common_delete/CourseSlSubcategory', {id: id}, function(resp){
					
					if(resp == 'delete')
					{
						$this.parents('tr').fadeOut(2000,function(){
							$(this).remove();
							$(".loading").hide();							
						});
					}
				});
			}
			return false;					
		});		
		
	});
</script>

 <div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
						<th width="5%">S.No.</th> 
                        <th width="20%">Name</th> 
						<th width="15%" class="hidden-xs">Date Added</th>
                        <th width="15%" class="hidden-xs">Date Modified</th>						
						<th width="15%" class="hidden-xs">Action</th> 
					</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $info)
							{ 
						?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $info['CourseSlSubcategory']['en']; ?></td>
									<td class="hidden-xs"><?php echo $info['CourseSlSubcategory']['created']; ?></td> 
									<td class="hidden-xs"><?php echo $info['CourseSlSubcategory']['modified']; ?></td>									 
									<td class="hidden-xs">
										<?php 
											$id = base64_encode(convert_uuencode($info['CourseSlSubcategory']['id'])); 
											$cat_id = base64_encode(convert_uuencode($info['CourseSlSubcategory']['category_id'])); 
											$table= base64_encode('CourseSlSubcategory');
											$renderPath=base64_encode('admin/manage');
											$renderElement=base64_encode('sl_sub_category_list');											
												
										?>
																				
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_sl_subcategory/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" id="<?php echo $id; ?>" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo $info['CourseSlSubcategory']['status']==0 ? 'ui-state-hover' : 'ui-state-default'; ?>" >
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                        <a title="Delete" href="javascript:void(0);" id="<?php echo $id; ?>" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>																			
                                       
									</td>
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
			<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div>
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
