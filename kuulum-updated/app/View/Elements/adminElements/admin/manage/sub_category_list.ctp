
<script type="text/javascript">
	
	$(document).ready(function() {

		$('.updateStatus').on('click',function(){
			
			$this = $(this);
			var id = $this.attr('id');
			var chk = jQuery.trim($this.attr('class'));
			
			$(".loading").show();
			
			if(chk.match('ui-state-hover'))
			{
				oldClass = 'ui-state-hover';
				newClass = 'ui-state-default';
			}
			else 
			{
				oldClass = 'ui-state-default';
				newClass = 'ui-state-hover';
			}
			
			$.post(ajax_url+'/admin/users/setStatus/CourseSubcategory/', { id: id }, function(resp){
				
				if($.trim(resp) == 'done')
				{
					$this.removeClass(oldClass).addClass(newClass);
					$(".loading").hide();
				}
			});		
		
			return false;
		
		});

		$('.delRec').click(function(){

			if(confirm('Do you really want to delete this record?'))
			{				
				$(".loading").show();
				$this = $(this);
				var id = $(this).attr('id');
				$.post(ajax_url+'/admin/users/common_delete/CourseSubcategory', {id: id}, function(resp){
					
					if(resp == 'delete')
					{
						$this.parents('tr').fadeOut(2000,function(){
							$(this).remove();
							$(".loading").hide();							
						});
					}
				});
			}
			return false;					
		});		
		
	});
</script>	

<?php //$this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_course_subcategory/'.$Id))); ?>

 <div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
 <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
                                                            <a href="<?php echo HTTP_ROOT.'admin/users/add_subcategory/'.base64_encode(convert_uuencode($category_id)); ?>" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>
<li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a>
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                       
   </div>
                                            </div>
        
	<div class="row" style="margin-top: 10px !important;"></div>	        
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                        <th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
						
                        <th class="col-sm-4">Title</th> 
						<th class="hidden-xs col-sm-3">Date Added</th>
                        <th class="hidden-xs col-sm-3">Date Modified</th>						
						<th class="hidden-xs col-sm-1" >Status</th> 
					</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$info)
							{ 
						?>
								<tr>
									 <td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                                    <input id="row_<?php echo $row; ?>" type="checkbox" name="row_1" class="checkboxs rows" value="<?php echo $info['CourseSubcategory']['id'];  ?>">
                                                                    <span></span>
                                                                    </label>
                                                                    </td>
									<td>
<?php $id = base64_encode(convert_uuencode($info['CourseSubcategory']['id'])); ?>
 <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_subcategory/".$id; ?>" >
<?php echo $info['CourseSubcategory']['en']; ?></a></td>
									<td class="hidden-xs"><?php echo date('d/m/Y',strtotime($info['CourseSubcategory']['created'])); ?></td> 
									<td class="hidden-xs"><?php echo date('d/m/Y',strtotime($info['CourseSubcategory']['modified'])); ?></td>
									<td style="padding:15px" class="hidden-xs">
									<?php 
										if($info['CourseSubcategory']['status']==0)
										{
											echo "<span class='label label-danger'>Inactive</span>";
										}
										elseif($info['CourseSubcategory']['status']==1)
										{
											echo "<span class='label label-success'>Active</span>";
										}
										
									?>
                                    </td>
									<!--<td>
										<?php 
												$id = base64_encode(convert_uuencode($info['CourseSubcategory']['id'])); 
												$cat_id = base64_encode(convert_uuencode($info['CourseSubcategory']['category_id'])); 
												$table = base64_encode('CourseSubcategory');
												$renderPath = base64_encode('admin/manage');
												$renderElement = base64_encode('sub_category_list');
												//$memberid=base64_encode(convert_uuencode($info['CourseCategory']['m_id']));
												
										?>
										
										<a title="View SL Subcategory" href="<?php echo HTTP_ROOT."admin/users/sl_subcategories/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>										
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_subcategory/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" id="<?php echo $id; ?>" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($info['CourseSubcategory']['status']==0?'ui-state-hover':'ui-state-default') ?>" >
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                        <a title="Delete" href="javascript:void(0);" id="<?php echo $id; ?>" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>										
                                       
									</td> -->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
		<!--	<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"CourseSubcategory"},
                            success:function(result){
                                if(result)
                                {                                    
                               window.location.href="<?php echo HTTP_ROOT.'admin/users/course_subcategory/'.$Id; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one record");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"CourseSubcategory"},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/course_subcategory/'.$Id; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one record.");
            }
           
        });
 });
</script>
