<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div>


	  <div class="row" style="margin-top: 10px !important;"></div>	
			<table id="DataTables_Table_1" class="table table-bordered table-striped js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info"> 
                                <thead> 
                                        <tr>
					        <th class="text-center" width="3%">#</th> 
                                                <th>Name</th> 
                                                <th width="15%" class="hidden-xs">Gross Income</th> 
                                                <th width="15%" class="hidden-xs">Commission</th>
                                                <th width="15%">Net Income</th>
                                        </tr> 
                                </thead> 
                                <tbody> 

                                        <?php 
							$x=0;
                                                        $grnt_total ='0'; 
                                                        $total_commision='0';
                                                        $net_total_income='0';
                                                if(!empty($all_author_income))
                                                {
						foreach($all_author_income as $key=>$income)
                                                        { 
                                                            if($income['gross_income']!=0)
                                                            {
								$x++;
                                                            ?>
                                                                <tr>											
								<td class="text-center"><?php echo $x ;?></td>
                                                                <td>
                             					<?php 	
                                                                            echo $this->Html->link(
                                                                            $income['name'],
                                                                            array('controller' => 'users', 'action' => 'author_income', base64_encode(convert_uuencode($income['member_id']))),
                                                                            array('escape' => false)
                                                                            
                                                                        ); ?>
								        </td>	
                                                                        <td class="hidden-xs">$<?php echo $income['gross_income'];?></td>	
                                                                        <td class="hidden-xs">$<?php echo $income['commission'];?></td> 		
                                                                        <td>$<?php echo $income['net_revenue'];?></td> 
                                                                </tr> 
                                                                <?php	

                                                            }
                                                        }  
                                                } else { ?>
                                                        <tr>
                                                                <td colspan="4">No Record Found.</td>
                                                        </tr>
                                                <?php } ?>	

                                </tbody>
                        </table>

                        <div>
</div>

			<div class="clear"></div>
			
		<!--	<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
