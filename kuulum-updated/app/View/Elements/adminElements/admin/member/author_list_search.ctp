<?php 
if(!empty($info))
{
?>
<a href="<?php echo HTTP_ROOT.'admin/Users/author_report/'.$qryStr; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:10px; margin-bottom:10px; margin-right:6px;">Download Excel</a>
<?php } ?>
<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_members'))); ?>
 <div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
                        <th>Name</th> 
						<th>Email</th> 
						<th>Current Status</th>
						<th width="29%">Action</th> 
					</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $member_detail)
							{ 
						?>
								<tr>
									<td><?php echo $member_detail['Member']['final_name']; ?></td> 
									<td><?php echo $member_detail['Member']['email']; ?></td> 
									<td>
									<?php 
										if($member_detail['Member']['status']==0)
										{
											echo "In-Active";
										}
										elseif($member_detail['Member']['status']==1)
										{
											echo "Active";
										}
										
									  ?>
                                      </td> 
									<td>
										<?php 
												$id = base64_encode(convert_uuencode($member_detail['Member']['id'])); 
												$table= base64_encode('Member');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('member_list');
										?>
										
										<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_author/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_author/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($member_detail['Member']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
										 <?php if($member_detail['Member']['status']!=0) { ?>
                                         <a title="Active" style="padding:0px !important;" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>"  onclick='activate("admin/Users/activate","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
										</a>
                                        <?php } ?>
										<?php /*?><a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a><?php */?>
                                       
									</td>
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
