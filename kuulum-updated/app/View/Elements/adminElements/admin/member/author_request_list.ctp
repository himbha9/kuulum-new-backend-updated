<div>
     
<div class="row" style="margin-top: 10px !important;"></div>	
		
               
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer js-table-checkable" role="grid" aria-describedby="DataTables_Table_1_info">

				<thead> 
					<tr>
                        <th class="text-center" width="3%">
                        <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                        <input id="check-all" type="checkbox" name="check-all">
                        <span></span>
                        </label>
                        </th>
                                                <th width="43%">Name</th> 
						<th width="43%" class="hidden-xs">Email</th> 
						<th width="10%" class="hidden-xs">Status</th>
					</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$member_detail)
							{ 
						?>
								<tr>
                                                                        <td class="text-center">
<label class="css-input css-checkbox css-checkbox-primary">
  <input type="checkbox" name="row_<?php echo $row;?>" value="<?php echo $member_detail['Member']['id']; ?>" class="rows checkboxs" >
<span></span>
</label>
</td>
									<td><?php echo $member_detail['Member']['final_name']; ?></td> 
									<td class="hidden-xs"><?php echo $member_detail['Member']['email']; ?></td> 
									<td style="padding:15px;" class="hidden-xs">
									<?php 
										if($member_detail['Member']['status']==0)
										{
											echo "<span class='label label-danger'>In-Active</span>";
										}
										elseif($member_detail['Member']['status']==1)
										{
											echo "<span class='label label-success'>Active</span>";
										}
										
									  ?>
                                    </td>
									
								<!--<td><?php echo date('d M Y, h:i e',$member_detail['Member']['date_request']); ?></td> 									  
									
								<td>
										<?php 
												$id = base64_encode(convert_uuencode($member_detail['Member']['id'])); 
												$table= base64_encode('Member');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('author_request_list');
										?>
										
										<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_author/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
										<a title="Make Author" href="<?php echo HTTP_ROOT."admin/users/accept_author_request/".$id; ?>" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" >
											<span class="ui-icon ui-icon-check"></span>
										</a>
										<a title="Reject Author Request" href="<?php echo HTTP_ROOT."admin/users/reject_author_request/".$id; ?>" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" >
											<span class="ui-icon ui-icon-cancel"></span>
										</a>
                                       
									</td> -->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="4">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			<!--
			<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div>-->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>

<script>
			$(document).ready(function(){			
			$("#reject_req").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/reject_author_requests'; ?>",
                            type:'POST',
                            data:{selectedids:selected},
                            success:function(data){
                    
                    if(data=="true")
                    { 
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/author_requests'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast record.");
			return false;
			}
						
					});
 $("#approve_req").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
       
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/accept_author_requests'; ?>",
                type:'POST',
                data:{selectedids:selected},
                success:function(data){
                    
                    if(data=="true")
                    {

                    window.location.href="<?php echo HTTP_ROOT.'admin/users/author_requests'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one record.");
            return false;
            }
           
        });
 });
</script>
