<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
   
        
	<div class="row" style="margin-top: 10px !important;"></div>	        
	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped js-table-checkable js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					
					<tr>
						<th style="width:3%">#</th>
                        			<th> Member Name</th>
						<th class="col-sm-3">Member Email</th>
						<th class="hidden-xs col-sm-2">Reason</th>
						<th class="hidden-xs col-sm-3">Description</th>
						<th class=" hidden-xs ">Cancel Date</th>
					</tr> 
				</thead> 
				<tbody>			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $info)
							{ 
						?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $info['Member']['final_name']; ?></td> 
									<td><?php echo $info['Member']['email']; ?></td>
									<td class="hidden-xs">
										<?php 
											if(!empty($info['UserReason']))
											{
												foreach($info['UserReason'] as $reason)
												{
													echo $reason['Reason']['en'].'<br/>'; 
												}
											}
										?>
									</td>
                                    					<td class="hidden-xs"><?php echo nl2br($info['CancelReason']['reason']); ?></td>
									<td class="hidden-xs"><?php echo date('d/m/Y',strtotime($info['CancelReason']['date'])); ?></td>
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
		<!--	<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
