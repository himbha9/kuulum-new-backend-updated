

  <div>
<div>
    
    

			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                                            <th class="text-center" width="3%">
        <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
        <input id="check-all" type="checkbox" name="check-all">
        <span></span>
        </label>
        </th>
        <th width="30%">Course Title</th> 
	<th width="37%" class="hidden-xs">Course Description</th> 
	<th width="10%">Author</th>
        <th width="10%" class="hidden-xs">Added</th>
	<th width="10%" class="col-sm-1 hidden-xs">Status</th>
	</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$info)
							{ 
						?>
								<tr>
                                                                    <td class="text-center">
                                            <label class="css-input css-checkbox css-checkbox-primary">
                                            <input type="checkbox" class="checkboxs rows" name="row_<?php echo $row;?>" value="<?php echo $info['Course']['id']; ?>"><span></span>
                                            </label>
                                        </td>
					<td>
                                        <?php 
                                        $id = base64_encode(convert_uuencode($info['Course']['id'])); 


                                        ?>
                                      <!--  <a title="View Lesson"href="<?php echo HTTP_ROOT."admin/users/lessons/".$id; ?>" > --><a title="View" href="<?php echo HTTP_ROOT."admin/users/view_course/".$id; ?>">
<?php echo $info['Course']['title']; ?></a></td>
									<td  class="hidden-xs"><?php echo substr($info['Course']['description'], 0, 50); ?></td>
									<td><?php echo $info['Member']['final_name']; ?></td>
                                  <!--  <td><?php echo $info['Language']['language']; ?></td> -->
									<td  class="hidden-xs"><?php echo date('d/m/Y',$info['Course']['date_added']); ?></td>
                                                                        <td style="padding:15px;"  class="hidden-xs">
									<?php 
										if($info['Course']['status']==0)
										{
											echo "<span class='label label-danger'>Inactive</span>";
										}
										elseif($info['Course']['status']==1)
										{
											echo "<span class='label label-success'>Active</span>";
										}else if($info['Course']['status']==2)
										{
											echo "<span class='label label-info'>Retired</span>";
										}
										
									  ?>
                                      </td> 
									<!--<td>
										<?php 
												$id = base64_encode(convert_uuencode($info['Course']['id'])); 
												$table= base64_encode('Course');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('cours_list');
												$memberid=base64_encode(convert_uuencode($info['Course']['m_id']));
												
										?>
										
										<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_course/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
										<a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_course/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($info['Course']['status']!=1?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                        <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
										<a title="View Lesson"href="<?php echo HTTP_ROOT."admin/users/lessons/".$id."/".$memberid; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-note"></span>
										</a>
										
                                       
									</td> -->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
			<!--<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>


<script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"Course"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/courses'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one course.");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"Course"},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/courses'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one course to change status");
            }
           
        });
 });
</script>

