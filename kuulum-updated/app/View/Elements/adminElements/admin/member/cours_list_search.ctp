<?php 
if(!empty($info))
{
?>
<a href="<?php echo HTTP_ROOT.'admin/Users/course_report/'.$qryStr; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:10px; margin-bottom:10px; margin-right:6px;">Download Excel</a>
<?php } ?>
<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_courses'))); ?>


 <div id="page-content">

	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">

		<div class="hastable">
	
			<table id="sort-table"> 
				<thead> 
					<tr>
                        <th>Title</th> 
						<th>Description</th>
                        <th>Author</th>
						<th>Primary language</th>						
						<th width="20%">Date Added</th>
						<th>Status</th>
						<th width="15%">Action</th> 
					</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$rec=0;
							$i = $this->Paginator->counter('%start%');
							foreach($info as $info)
							{ 
								if($info['Member']['id']!=''){
									$rec=1;
						?>
								<tr>
									<td><?php echo $info['Course']['title']; ?></td>
									<td><?php echo substr($info['Course']['description'], 0, 50); ?></td>
                                    <td><?php echo $info['Member']['final_name']; ?></td>
									<td><?php echo $info['Language']['language']; ?></td> 
									<td><?php echo date('d M Y, h:i e',$info['Course']['date_added']);  ?></td>
									<td>
									<?php 
										if($info['Course']['status']==0)
										{
											echo "In-Active";
										}
										elseif($info['Course']['status']==1)
										{
											echo "Active";
										}
										
									  ?>
                                      </td> 
									<td>
										<?php 
												$id = base64_encode(convert_uuencode($info['Course']['id'])); 
												$table= base64_encode('Course');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('cours_list');
												$memberid=base64_encode(convert_uuencode($info['Course']['m_id']));
												
										?>
										
										<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_course/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_course/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($info['Course']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                        <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
										<a title="View Lesson"href="<?php echo HTTP_ROOT."admin/users/lessons/".$id."/".$memberid; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-note"></span>
										</a>
										
                                       
									</td>
								</tr> 
					<?php	
								$i++;	
							}
							}
							if(!$rec)
							{
								echo "<tr><td colspan='7'>No Record Found.</td></tr>";
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
			<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div>
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
