<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'course_prices','admin'=>true))); ?>
<div id="page-content">
	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
		<div class="hastable">
			<table id="sort-table"> 
				<thead> 
					<tr>
						<th width="20%">S.No.</th>
						<th width="30%" >Price</th>
                       	<th width="15%">Action</th> 
					</tr> 
				</thead> 
				<tbody> 
				
				<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $price)
							{ 
								
						?>
                        		
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo "USD ".$price['CoursePrice']['price'];?></td>
                                    <td>
										<?php $id = base64_encode(convert_uuencode($price['CoursePrice']['id'])); 
												$table= base64_encode('CoursePrice');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('course_price_list');
										?>
										
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_course_price/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                            <span class="ui-icon ui-icon-pencil"></span>
                                        </a>
                                        <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>										
                                        
                                    </td>
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>							
					</tbody>
				</table>
				<div class="clear"></div>
				<div id="pager">					
				
					<?php echo $this->element('adminElements/table_head'); ?>
				
				</div>
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
            
