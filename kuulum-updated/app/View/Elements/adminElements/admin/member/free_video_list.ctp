<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'free_video','admin'=>true))); ?>

<script type="text/javascript">
	
	$(document).ready(function() {

		/*$('.updateStatus').live('click',function(){
			
			$this = $(this);
			var id = $this.attr('id');
			var chk = jQuery.trim($this.attr('class'));
			
			$(".loading").show();
			
			if(chk.match('ui-state-hover'))
			{
				oldClass = 'ui-state-hover';
				newClass = 'ui-state-default';
			}
			else 
			{
				oldClass = 'ui-state-default';
				newClass = 'ui-state-hover';
			}
			
			$.post(ajax_url+'/admin/users/setStatus/Member/', { id: id }, function(resp){
				
				if($.trim(resp) == 'done')
				{
					$this.removeClass(oldClass).addClass(newClass);
					$(".loading").hide();
				}
			});		
		
			return false;
		
		});			
		*/
	});
</script>
<div>

    
    
	<div class="row" style="margin-top: 10px !important;"></div>	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
    <thead>
					<tr>
                      <!--  <th width="1%">S.No.</th> -->
                                            <th class="text-center" width="3%">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
						<th width="24%">Video Title</th>
						<th class="hidden-xs" width="20%">Description</th>
						<th class="hidden-xs" width="23%">URL</th> 
						<th class="hidden-xs" width="10%">Published</th>
					<!--	<th width="20%">Modified Date</th> -->
						<th class="hidden-xs col-sm-1" width="10%">Views</th> 
						<th class="hidden-xs col-sm-1" width="10%">Status</th>
						<!--<th width="29%">Action</th>  -->
					</tr> 
</thead>
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=> $video_detail)
							{ 
						?>
								<tr>
                                                                    <td class="text-center">
                                            <label class="css-input css-checkbox css-checkbox-primary">
                                                <input type="checkbox" class="rows checkboxs" name="row_2" id="row_<?php echo $row; ?>" value="<?php echo $video_detail['FreeVideo']['id']; ?>"><span></span>
                                            </label>
                                        </td>
									<!-- <td><?php echo $i; ?></td> -->
                                        <td>
                                           <?php $id = base64_encode(convert_uuencode($video_detail['FreeVideo']['id'])); ?>
                                            <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_free_video/".$id; ?>">
                                                <?php echo $video_detail['FreeVideo']['title_en']; ?></a>
                                                                </td> 
									<td  class="hidden-xs"><?php echo $this->Text->truncate($video_detail['FreeVideo']['description_en'],50,array('ellipsis'=>'...','exact'=>false));?></td> 
									<td  class="hidden-xs"><?php 
									if(empty($video_detail['FreeVideo']['free_video_url']))
									{
										echo $video_detail['FreeVideo']['own_video_url']; 
									}else
									{
										echo $video_detail['FreeVideo']['free_video_url']; 
									}?>
									</td> 
									<td  class="hidden-xs"><?php echo date('d/m/Y',$video_detail['FreeVideo']['date_added']); ?></td> 
									<!--<td><?php echo date('d M Y, h:i e',$video_detail['FreeVideo']['date_modified']);?></td> -->
									<td  class="hidden-xs"><?php echo $video_detail['FreeVideo']['viewer']; ?></td> 
                                                                        <td  class="hidden-xs" style="padding:15px;">
									<?php 
										if($video_detail['FreeVideo']['status']==0)
										{
											echo "<span class='label label-danger'>InActive</span>";
										}
										elseif($video_detail['FreeVideo']['status']==1)
										{
											echo "<span class='label label-success'>Active</span>";
										}
										
									  ?>
                                      </td>
									
									<!--<td>
										<?php 
												$id = base64_encode(convert_uuencode($video_detail['FreeVideo']['id'])); 
												$table= base64_encode('FreeVideo');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('free_video_list');
										?>
										
										
										<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_free_video/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-search"></span>
										</a>
										<a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_free_video/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
                                       
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($video_detail['FreeVideo']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                         
										<a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
										
									</td> -->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
			<!--<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
 <script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"FreeVideo"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/free_video'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one record.");
                        return false;
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"FreeVideo"},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/free_video'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one record.");
            return false;
            }
           
        });
 });
</script>
