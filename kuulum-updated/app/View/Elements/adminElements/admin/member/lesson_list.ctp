<?php

$this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_courses'))); ?>
<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">

            <div class="btn-group">
                <button type="button" class="btn btn-default">Actions</button>
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">


                        <li>
                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a>
                        </li>


                    </ul>
                </div>
            </div></div>
    </div>

    <div class="row" style="margin-top: 10px !important;"></div>	



                <!--<table id="sort-table"> -->
    <table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
        <thead> 
            <tr>
                <th class="text-center" style="width: 70px;">
                    <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                        <input id="check-all" type="checkbox" name="check-all">
                        <span></span>
                    </label>
                </th>
                <th>Lesson Title</th> 
                <th class="hidden-xs">Lesson Description</th> 
                <th class='col-sm-1 hidden-xs'>Added</th>
                <th class="col-sm-1 hidden-xs">Status</th>
                <!--<th width="29%">Action</th> -->
            </tr> 
        </thead> 
        <tbody> 
					<?php
						if(!empty($info))
						{
                                                    
                                                    
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$info)
							{
						?>
            <tr>
                <td class="text-center">
                    <label class="css-input css-checkbox css-checkbox-primary">
                        <input type="checkbox" name="row_2" id="row_<?php echo $row;?>" class='checkboxs rows' value="<?php echo $info['CourseLesson']['id']; ?>"><span></span>
                    </label>
                </td>
                <td><?php echo $info['CourseLesson']['title']; ?></td>
                <td class="hidden-xs"><?php echo $info['CourseLesson']['description']; ?></td>
                <td class="hidden-xs"><?php echo  date('d/m/Y',$info['Course']['date_added']); ?></td>
                <td style="padding:15px;" class="hidden-xs">
									<?php 
										if($info['CourseLesson']['status']==0)
										{
											echo "<span class='label label-danger'>Inactive</span>";
										}
										elseif($info['CourseLesson']['status']==1)
										{
											echo "<span class='label label-success'>Active</span>";
										}
									  ?>
                </td> 
                                                  <!--<td>
										<?php 
												$id = base64_encode(convert_uuencode($info['CourseLesson']['id'])); 
												$table= base64_encode('CourseLesson');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('lesson_list');
												$memberId=$this->params['pass'][1];
												$courseId=$info['CourseLesson']['course_id'];
										?>
                                                          
                                                          <a title="View" href="<?php echo HTTP_ROOT."admin/users/view_lesson/".$id."/".$memberId; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                                                  <span class="ui-icon ui-icon-search"></span>
                                                          </a>
                  <!-- <a title="Edit" href="<?php //echo HTTP_ROOT."admin/users/edit_lesson/".$id."/".$memberId; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip"> 
                                                                  <span class="ui-icon ui-icon-pencil"></span>
                                                          </a> 
                                                          <a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($info['CourseLesson']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_lesson_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement; ?>","<?php echo $courseId; ?>")'>
                                                                  <span class="ui-icon ui-icon-lightbulb"></span>
                                                          </a>
                                                  </td> -->
            </tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
            <tr>
                <td colspan="7">No Record Found.</td>
            </tr>
					<?php		
						}
					?>						
        </tbody>
    </table>
    <div class="clear"></div>
    <!--<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
    </div> -->
</div>

<div class="clear"></div>
 <script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"CourseLesson"},
                            success:function(result){
                                if(result)
                                {
                                  
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/lessons/'.$course_id; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one lesson");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"CourseLesson"},
                success:function(data){
                    
                    if(data=="true")
                    {
            window.location.href="<?php echo HTTP_ROOT.'admin/users/lessons/'.$course_id; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one lesson to change status");
            }
           
        });
 });
</script>
