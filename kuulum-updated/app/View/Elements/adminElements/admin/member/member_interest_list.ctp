
<script type="text/javascript">
	
	$(document).ready(function() {

		/*$('.updateStatus').on('click',function(){
			
			$this = $(this);
			var id = $this.attr('id');
			var chk = jQuery.trim($this.attr('class'));
			
			$(".loading").show();
			
			if(chk.match('ui-state-hover'))
			{
				oldClass = 'ui-state-hover';
				newClass = 'ui-state-default';
			}
			else 
			{
				oldClass = 'ui-state-default';
				newClass = 'ui-state-hover';
			}
			
			$.post(ajax_url+'/admin/users/setStatus/Member/', { id: id }, function(resp){
				
				if($.trim(resp) == 'done')
				{
					$this.removeClass(oldClass).addClass(newClass);
					$(".loading").hide();
				}
			});		
		
			return false;
		
		});			
		*/
	});
</script>

<?php //$this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_view_member_interests'))); ?>

<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
<div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
<?php $id = base64_encode(convert_uuencode($info['Member']['id']));  ?>
                                                            <a href="<?php echo HTTP_ROOT; ?>admin/users/add_member_interest/<?php echo $info['Member']['role_id']; ?>/<?php echo  $id; ?>" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a>
                                                        </li>
                                                       
                                                        
                                                    </ul>
                                                </div>
                                            </div>  </div>
                                            </div>

	  <div class="row" style="margin-top: 10px !important;"></div>	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info"> 
			<thead> 
			<tr>
                         <th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
                        <th>name</th> 
<th class="hidden-xs">date added</th> 
<th class="col-sm-1 hidden-xs">Status</th>
			</tr> 
			</thead> 
				<tbody> 
			
					<?php
						if(!empty($intrest))
						{
							
							foreach($intrest as $row=> $intrest1 )
							{ 
if(!empty($intrest1)){

						?>
                                                <tr>
                                                  <td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
<?php $id= ($role_id==3)?$intrest1["MemberAuthorKeywords"]["id"]:$intrest1["MemberKeywords"]["id"]; ?>
                                                                    <input id="row_<?php echo $row; ?>" type="checkbox"  class="checkboxs rows" name="row_1" value="<?php echo $id; ?>">
                                                                    <span></span>
                                                                    </label>
                                                                    </td>

                                                <?php if($role_id==3){
        $id=base64_encode(convert_uuencode($intrest1['MemberAuthorKeywords']['id'])); 
}
        else{
        $id=base64_encode(convert_uuencode($intrest1['MemberKeywords']['id'])); 
} ?>
                                                <td><?php if($role_id==3)
                                                echo "<a href='".HTTP_ROOT."/admin/users/edit_author_interest/".$id."'>".$intrest1['MemberAuthorKeywords']['keyword']."</a>"; 
                                                else
                                                 echo "<a href='".HTTP_ROOT."/admin/users/edit_member_interest/".$id."'>".$intrest1['MemberKeywords']['keyword']."</a>" ?></td> 
 <td class="hidden-xs"><?php if($role_id==3)echo date_format(date_create($intrest1['MemberAuthorKeywords']['date_added']),'d/m/Y'); else echo date_format(date_create($intrest1['MemberKeywords']['date_added']),'d/m/Y'); ?></td> 
<td class="hidden-xs">
<?php
                              $status=($role_id==3)? $intrest1['MemberAuthorKeywords']['status']:$intrest1['MemberKeywords']['status'];
                                
                                if($status==0)
                                    {
                                            echo "<span class='label label-danger'>Inactive</span>";
                                    }
                                else{
                                        echo "<span class='label label-success'>Active<span>";
                                }
?>
</td>

                                                 </tr> 
                                                <?php	
}
								
							}
						} else {
					?>
							<tr>
								<td colspan="3">No keywords Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
                                <tfoot>
                                </tfoot>
			</table>
			<div class="clear"></div>
			
			
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
 <script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_interests'; ?>",
                            type:'POST',
                            data:{selectedids:selected,"model_type":<?php echo $role_id; ?>},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/view_member_interests/'.$role_id.'/'.base64_encode(convert_uuencode($info['Member']['id'])); ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one of the interest");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_interest_status'; ?>",
                type:'POST',
                data:{selectedids:selected,"model_type":<?php echo $role_id; ?>},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/view_member_interests/'.$role_id.'/'.base64_encode(convert_uuencode($info['Member']['id'])); ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one interest to change status");
            }
           
        });
 });
</script>
