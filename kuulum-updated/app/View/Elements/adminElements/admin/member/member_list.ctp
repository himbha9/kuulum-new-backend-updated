
<script type="text/javascript">
	
	$(document).ready(function() {

		/*$('.updateStatus').on('click',function(){
			
			$this = $(this);
			var id = $this.attr('id');
			var chk = jQuery.trim($this.attr('class'));
			
			$(".loading").show();
			
			if(chk.match('ui-state-hover'))
			{
				oldClass = 'ui-state-hover';
				newClass = 'ui-state-default';
			}
			else 
			{
				oldClass = 'ui-state-default';
				newClass = 'ui-state-hover';
			}
			
			$.post(ajax_url+'/admin/users/setStatus/Member/', { id: id }, function(resp){
				
				if($.trim(resp) == 'done')
				{
					$this.removeClass(oldClass).addClass(newClass);
					$(".loading").hide();
				}
			});		
		
			return false;
		
		});			
		*/
	});
</script>

<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_members'))); ?>

 <div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
                                        
            
            <?php echo $this->Form->create('search',array('id'=>'searchAdminUser',"class"=>"form-inline")); ?>
            <div class="form-group">
                <div class="btn-group">
                    <button type="button" class="btn btn-default">Actions</button>
                        <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                        <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo HTTP_ROOT; ?>admin/users/add_member" tabindex="-1">Add</a></li>
                            <li><a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a></li>
                            <li><a href="javascript:void(0)" id="toggle_row" tabindex="-1">Toggle Active/Inactive</a></li>
                        </ul>
                        </div>
                </div>
            </div>    
            <div class="form-group">
                <div class="btn-group">
                    <button type="button" class="btn btn-default">Filters</button>
                    <div class="btn-group">
                    <?php echo $this->Form->input('type',array('type'=>'select','id'=>'member_email','div'=>false,'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input','options'=>array('name'=>'By name','email'=>'By email','date_register'=>'Date registered','status'=>'By status'))); ?>
                    </div>
                </div>                              
             </div>                                    
            <div class="form-group">
                <div class="showHideCoursetText"> 
                <?php echo $this->Form->input('text',array('type'=>'text','id'=>'email_search','div'=>false, 'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input')); ?> 
                </div>  
            </div>
            
            <div class="form-group">
            <div class="hideStartEndDate">                                        <?php echo $this->Form->input('register_date',array('type'=>'text','id'=>'register_date','div'=>false, 'label'=>false,'class'=>'datepicker form-control field text full required col-sm-3 ')); ?></div>
            </div>
            <div class="form-group">
                <div class="hideStatusType">
                    <?php echo $this->Form->input('status',array('type'=>'select','id'=>'mem_status','div'=>false, 'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input','options'=>array('1'=>'Active','0'=>'In-active'))); ?> </div>
            </div>    
            <div class="form-group">    
            <div class="box_padding_left moveLeft">                        
            <?php echo $this->Form->submit('Search',array('div'=>false,'label'=>false,'id'=>'searchMemberList','class'=>'btn btn-sm btn-primary')); ?></div>  
            </div>
           
            <?php echo $this->Form->end(); ?>                
                                                    
           
            
   </div>
                                            </div>
        
	<div class="row" style="margin-top: 10px !important;"></div>	        
	
<table id="DataTables_Table_1" class="table table-bordered table-striped js-table-checkable js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                        <th class="text-center" style="width: 3%;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                            <input id="check-all" type="checkbox" name="check-all">
                            <span></span>
                             </label>
                        </th>
                        <th width="36%">Name</th> 
			<th  class="hidden-xs"  width="35%">Email</th> 
                        <th  width="16%">Type</th>
                        <th class="hidden-xs col-sm-1"  width="10%">Status</th>
                    	</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$member_detail)
							{ 
						?>
								<tr>
                                                                    <td class="text-center">
                                                                    <label class="css-input css-checkbox css-checkbox-primary">
                                                                    <input type="checkbox" class="checkboxs rows" name="row_<?php echo $row;?>" value="<?php echo $member_detail['Member']['id']; ?>"  >
                                                                    <span></span>
                                                                    </label>
                                                                    </td>
									<td> <?php $id = base64_encode(convert_uuencode($member_detail['Member']['id'])); ?>
<a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_member/".$id; ?>" class="">
											
										
<?php echo $member_detail['Member']['final_name']; ?></a></td> 
									<td  class="hidden-xs"><?php echo $member_detail['Member']['email']; ?></td> 
                                                                        <td>
                                                                       
                                                                        <?php
                                                                         echo $role[$member_detail['Member']['role_id']];
                                                                       /*  if($member_detail['Member']['role_id']==3)
										{
											echo "Author";
										}
										else
										{
											echo "Member";
										}  */
                                                                                ?>
                                                                        </td>
									<td style="padding:15px;"  class="hidden-xs">
									<?php 
										if($member_detail['Member']['status']==0)
										{
											echo "<span class='label label-danger'>Inactive</span>";
										}
										elseif($member_detail['Member']['status']==1)
										{
											echo "<span class='label label-success'>Active<span>";
										}
										if($member_detail['Member']['status']==2)
										{
											echo "<span class='label label-danger'>Cancelled</span>";
										}
										
									  ?></span>
                                      </td>
                                                               
									
								<!--	<td><?php echo date('d M Y, h:i e',$member_detail['Member']['date_added']); ?></td> 
									
									<td>
										<?php 
												$id = base64_encode(convert_uuencode($member_detail['Member']['id'])); 
												$table= base64_encode('Member');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('member_list');
										?>
										
										<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_member/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_member/".$id; ?>" class="btn_no_text btn ui-state-default ">
											<span class="ui-icon ui-icon-search"></span>
										</a>
                                      <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_member/".$id; ?>" class="btn_no_text btn ui-state-default "> 
<a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_member/".$id; ?>" class="">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
<a title="View" href="<?php echo HTTP_ROOT."admin/users/view_member_interests/".$id; ?>" class="">
View Interest </a>
										
										
										<a title="Toggle Status" href="javascript:void(0);" class="updateStatus btn_no_text btn ui-corner-all tooltip <?php echo($member_detail['Member']['status']==0?'ui-state-hover':'ui-state-default') ?>" onclick='updateStatus("admin/Users/update_status","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement	 ?>")'>
											<span class="ui-icon ui-icon-lightbulb"></span>
										</a>
                                         
										
                                  		
										<?php if($this->Session->read('LocTrainAdmin.role_id')==8) { ?>
											<a title="Assign Permissions" href="<?php echo HTTP_ROOT."admin/users/assign_role/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
												<span class="ui-icon ui-icon-bookmark"></span>
											</a>										         
										<?php } ?>
										
										<a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_member","<?php echo $page; ?>","<?php echo $id; ?>","Member","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
										
									</td> -->
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
			<!--<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div> -->
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
 <script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"Member"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/members'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one of the member");
			
			}
						
					});
 $("#toggle_row").click(function()
        {
          var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
            
            if(selected.length>0)
            {
            $.ajax({
                url:"<?php echo HTTP_ROOT.'admin/users/change_records_status'; ?>",
                type:'POST',
                data:{selectedids:selected,model:"Member"},
                success:function(data){
                    
                    if(data=="true")
                    {
                    window.location.href="<?php echo HTTP_ROOT.'admin/users/members'; ?>";
                    }
                    }
                
            })
            }
            else
            {
            alert("Please select atleast one member to change status");
            }
           
        });
 });

</script>
