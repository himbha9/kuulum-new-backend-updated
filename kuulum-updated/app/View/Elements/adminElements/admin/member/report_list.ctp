<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'report_on_remarks','admin'=>true))); ?>
<div>  
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	        
        <div class="col-sm-12">
<div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
                                                            <a href="" tabindex="-1">Approve Removal</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1">Reject Removal</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1">Disable Review</a>
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
             </div>
                                            </div>
		    <div class="row" style="margin-top: 10px !important;"></div>	
	
<table id="DataTables_Table_1" class="table table-bordered table-striped js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
<thead> 
<tr>
<th class="text-center" width="3%"><label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b"><input id="check-all" type="checkbox" name="check-all"><span></span></label>
</th>
<th width="35%">Course Title</th>
<th width="32%"class="hidden-xs">Review</th>
<th class="hidden-xs" width="15%">Author</th>
<th width="15%">Reporter</th>
</tr> 
</thead> 
<tbody> 
<?php

    if(!empty($info))
        {
	$i = $this->Paginator->counter('%start%');
	foreach($info as $report)
	{ 
        ?>
        <tr>
        <td class="text-center"><label class="css-input css-checkbox css-checkbox-primary"><input type="checkbox" name="row_2" id="row_2"><span></span></label></td>

        <td>
        <?php 
        $remark_id = base64_encode(convert_uuencode($report['CourseRemark']['id']));
	$id = base64_encode(convert_uuencode($report['RemarkReport']['id'])); 
        ?>
        <a title="view Report" href="<?php echo HTTP_ROOT.'admin/users/view_reports_review/'.$id;?>">
        <?php echo $report['CourseRemark']['Course']['title'];?>
        </a>
        </td>
        <td class="hidden-xs"><?php echo $report['CourseRemark']['comment'];?></td>
        <td  class="hidden-xs"><?php 
        // Load Member Model
        $member_modal = ClassRegistry::init('Member');
        $author_data=$member_modal->find('first',array('fields'=>array('given_name'),'contain'=>array(false),'conditions'=>array('Member.id'=>$report['CourseRemark']['member_id'])));
        echo !empty($author_data) ? $author_data['Member']['given_name'] : "" ?>
        </td>
        <td><?php echo $report['Member']['given_name'];?></td>
        
        </tr> 
        <?php	
	$i++;	
	}
	}
        else
         {
            ?>
	<tr><td colspan="7">No Record Found.</td></tr>
            <?php		
	}
	?>							
	</tbody>
</table>

				<div class="clear"></div>
				<!--<div id="pager">					
				
					<?php /*echo $this->element('adminElements/table_head'); */?>
				
				</div> -->
			</div>
        	<div class="clear"></div>			
		</div>
		<div class="clear"></div>
	</div> 
 
