<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_user_roles'))); ?>


 <div id="page-content">

	<div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">

		<div class="hastable">
	
			<table id="sort-table"> 
				<thead> 
					<tr>
                        <th>Name</th> 
						<th>Email</th> 
						<th>Role Type</th>
						<th width="29%">Action</th> 
					</tr> 
				</thead> 
				<tbody> 
			
					<?php
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $member_detail)
							{ 
						?>
								<tr>
									<td><?php echo $member_detail['Admin']['username']; ?></td> 
									<td><?php echo $member_detail['Admin']['email']; ?></td> 
									<td>
									<?php 
										if($member_detail['Admin']['role_id']==4)
										{
											echo "News Manager";
										}
										if($member_detail['Admin']['role_id']==5)
										{
											echo "Membership Manager";
										}
										if($member_detail['Admin']['role_id']==6)
										{
											echo "Course Manager";
										}
										if($member_detail['Admin']['role_id']==7)
										{
											echo "Administrator";
										}
										if($member_detail['Admin']['role_id']==8)
										{
											echo "Partner";
										}
									  ?>
                                      </td> 
									<td>
										<?php 
												$id = base64_encode(convert_uuencode($member_detail['Admin']['id'])); 
												$table= base64_encode('Admin');
												$renderPath=base64_encode('admin/member');
												$renderElement=base64_encode('subadmin_list');
										?>
										
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_role/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
											<span class="ui-icon ui-icon-pencil"></span>
										</a>
										<a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a>
 										<?php /*?><a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_record","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
											<span class="ui-icon ui-icon-circle-close"></span>
										</a><?php */?>
                                       
									</td>
								</tr> 
					<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="7">No Record Found.</td>
							</tr>
					<?php		
						}
					?>						
				</tbody>
			</table>
			<div class="clear"></div>
			
			<div id="pager" class="pagination">
				<?php echo $this->element('adminElements/table_head'); ?>
			</div>
				
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
