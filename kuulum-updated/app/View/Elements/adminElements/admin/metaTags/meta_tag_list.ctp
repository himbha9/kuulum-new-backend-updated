<?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'meta_tags','admin'=>true))); ?>
<div>
<div>
    
    <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	
        <div class="col-sm-12">
        <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
                                                            <a href="<?php echo HTTP_ROOT.'admin/users/add_meta_tags'; ?>" tabindex="-1">Add</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a>
                                                        </li>
                                                       
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                       
        
                </div>
    </div>
<div class="row" style="margin-top: 10px !important;"></div>	
			<!--<table id="sort-table"> -->
<table id="DataTables_Table_1" class="table table-bordered table-striped  js-dataTable-full dataTable no-footer" role="grid" aria-describedby="DataTables_Table_1_info">
				<thead> 
					<tr>
                                            <th class="text-center" style="width:3%">
        <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
        <input id="check-all" type="checkbox" name="check-all">
        <span></span>
        </label>
        </th>
                        <th style="width:15%">Page Title</th>
                        <th class="hidden-xs" style="width:22%" >Page URL</th>
                        <th class="hidden-xs" style="width:19%" >Meta Title</th>
                        <th class="hidden-xs" style="width:19%">Meta keywords</th>
                        <th class="hidden-xs" style="width:">Meta Description</th>                                                
                     <!--   <th >Action</th> -->
                    </tr> 
                </thead> 
                <tbody> 
                        <?php //pr($info);die;
						if(!empty($info))
						{
							$i = $this->Paginator->counter('%start%');
							foreach($info as $row=>$pages)
							{ 
						?>
                        		
                                <tr>
                                    <td class="text-center">
                                            <label class="css-input css-checkbox css-checkbox-primary">
                                                <input type="checkbox" class="checkboxs rows" name="row_2" id="row_<?php echo $row; ?>" value="<?php echo $pages["MetaTag"]["id"]; ?>"><span></span>
                                            </label>
                                        </td>
                                    <td><?php  $id = base64_encode(convert_uuencode($pages['MetaTag']['id'])); ?> 
                                        <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_metatag/".$id; ?>" >
                                        <?php echo $pages['MetaTag']['page_title'];?>
                                        </a>
                                    </td>
                                    <td class="hidden-xs"><?php echo $this->Html->url('/', true).$pages['MetaTag']['page_url'];?></td>
                                    <td class="hidden-xs"><?php echo $pages['MetaTag']['meta_keywords_en'];?></td>
                                    <td class="hidden-xs"><?php echo $pages['MetaTag']['meta_title_en'];?></td>
                                    <td class="hidden-xs"><?php echo $this->Text->truncate(strip_tags($pages['MetaTag']['meta_description_en']),150,array('ending'=>'...','exact'=>false)); ?></td>
                                    
                                 <!--   <td>
                                        <?php 
                                                                $id = base64_encode(convert_uuencode($pages['MetaTag']['id'])); 
                                                                $table= base64_encode('MetaTag');
                                                                $renderPath=base64_encode('admin/metaTags');
                                                                $renderElement=base64_encode('meta_tag_list');
                                                ?>
                                                <a title="View" href="<?php echo HTTP_ROOT."admin/users/view_metatag/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                                        <span class="ui-icon ui-icon-search"></span>
                                                </a>
                                                <a title="Edit" href="<?php echo HTTP_ROOT."admin/users/edit_metatag/".$id; ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip">
                                                        <span class="ui-icon ui-icon-pencil"></span>
                                                </a>
                                                <a title="Delete" href="javascript:void(0);" class="delRec btn_no_text btn ui-state-default ui-corner-all tooltip" id="<?php echo $id; ?>" onclick='deleteUser("admin/Users/delete_meta","<?php echo $page; ?>","<?php echo $id; ?>","<?php echo $table ?>","<?php echo $renderPath ?>","<?php echo $renderElement ?>")'>
                                                        <span class="ui-icon ui-icon-circle-close"></span>
                                                </a>								

                                    </td> -->
                                </tr> 
                	<?php	
								$i++;	
							}
						} else {
					?>
							<tr>
								<td colspan="5">No Record Found.</td>
							</tr>
					<?php		
						}
					?>	
                </tbody>
            </table>
            <div class="clear"></div>
         <!--   <div id="pager">
                <?php echo $this->element('adminElements/table_head'); ?>
            </div> -->
        </div>
        <div class="clear"></div>			
    </div>
    <div class="clear"></div>
</div> 
            
<script>
			$(document).ready(function(){			
			$("#remove_row").click(function(){
			var selected=[];
			$(".rows").each(function(index)
                        {
			if($(this).is(":checked")==true)
			{
			selected.push($(this).val());
			}
			});
                    
			if(selected.length>0)
			{
                            $.ajax({
                            url:"<?php echo HTTP_ROOT.'admin/users/delete_selected_records'; ?>",
                            type:'POST',
                            data:{selectedids:selected,model:"MetaTag"},
                            success:function(result){
                                if(result)
                                {
                                    
                                   window.location.href="<?php echo HTTP_ROOT.'admin/users/meta_tags'; ?>";
                                }
                                }
                            })
			}
			else
			{
			alert("Please select atleast one record");
			
			}
						
					});
 
 });
</script>
