<div id="page-header-wrapper">
	<div id="top">
		<div style="float:left; padding:10px;">
			<span class="logo" style="padding:10px;color:#FFF; font-size:32px;">
				<a href="<?php echo HTTP_ROOT;?>admin/users/dashboard" style="color:#FFF;">Localization Training LLC</a>
			</span>
		</div>
		<div class="welcome">
			<span class="note">
				Welcome 
					<a href="<?php echo HTTP_ROOT;?>admin/users/dashboard" title="Welcome <?php echo $this->Session->read('LocTrainAdmin.username');?>">
						<?php echo $this->Session->read('LocTrainAdmin.username');?>
					</a>
			</span>
			<?php /*?><a class="btn ui-state-default ui-corner-all" href="<?php echo HTTP_ROOT;?>admin/users/change_password">
							<span class="ui-icon ui-icon-wrench"></span>
							Settings

			</a>	<?php */?>		
		
			<a class="btn ui-state-default ui-corner-all" href="<?php echo HTTP_ROOT;?>admin/users/change_password">

				<span class="ui-icon ui-icon-person"></span>

				My account

			</a>
	
			<a class="btn ui-state-default ui-corner-all" href="<?php echo HTTP_ROOT;?>admin/users/logout">
				<span class="ui-icon ui-icon-power"></span>
				Logout
			</a>					
		</div>
	</div>
	<ul id="navigation">
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/dashboard" class="sf-with-ul">Dashboard</a>
		</li>
		<li>
			<a href="javascript:void(0)">Members</a>
			<ul>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/author_requests">Author Requests List</a>
				</li>	
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/members">Member List</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/authors">Author List</a>
				</li>
               
			</ul>
		</li>
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/courses">Courses</a>
		</li>
		<li>
			<a href="javascript:void(0)" class="sf-with-ul">CMS</a>
			<ul>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/email_templates">Email Templates</a>
				</li>	
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/cmspages">Cms Pages</a>
				</li>				
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/news">News</a>
				</li>
               
			</ul>
		</li>
        <li>
			<a href="javascript:void(0)" class="sf-with-ul">Manage</a>
			<ul>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/interest">Interest</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/payments">Payments</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/price" class="sf-with-ul">Price</a>
		</li>
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/discounts" class="sf-with-ul">Discounts</a>
		</li>
		

	</ul>
	
</div>
 