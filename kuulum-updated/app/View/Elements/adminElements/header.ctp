<style>
.head_len
{
	width:50% !important;
}
</style>
<div id="page-header-wrapper">
	<div id="top">
		<div class="head_len" style="float:left; padding:10px; ">
			<span class="logo" style="padding:10px;color:#FFF; font-size:32px;">
				<a href="<?php echo HTTP_ROOT;?>admin/users/dashboard" style="color:#FFF;">Localization Training LLC</a>
			</span>
		</div>
		<div class="welcome">
			<span class="note">
				Welcome 
					<a href="<?php echo HTTP_ROOT;?>admin/users/dashboard" title="Welcome <?php echo $this->Session->read('LocTrainAdmin.username');?>">
						<?php echo $this->Session->read('LocTrainAdmin.username');?>
					</a>
			</span>
			<?php /*?><a class="btn ui-state-default ui-corner-all" href="<?php echo HTTP_ROOT;?>admin/users/change_password">
							<span class="ui-icon ui-icon-wrench"></span>
							Settings

			</a>	<?php */?>		
		
			<a class="btn ui-state-default ui-corner-all" href="<?php echo HTTP_ROOT;?>admin/users/change_password">

				<span class="ui-icon ui-icon-person"></span>

				My account

			</a>
	
			<a class="btn ui-state-default ui-corner-all" href="<?php echo HTTP_ROOT;?>admin/users/logout">
				<span class="ui-icon ui-icon-power"></span>
				Logout
			</a>					
		</div>
	</div>
	<ul id="navigation">
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/dashboard" class="sf-with-ul">Dashboard</a>
		</li>
		<li>
			<a href="javascript:void(0)">Members</a>
			<ul>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/author_requests">Author Requests List</a>
				</li>	
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/members">Member List</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/authors">Author List</a>
				</li>
               
			</ul>
		</li>
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/courses">Courses</a>
		</li>
		<li>
			<a href="javascript:void(0)" class="sf-with-ul">CMS</a>
			<ul>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/email_templates">Email Templates</a>
				</li>	
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/cmspages">Cms Pages</a>
				</li>				
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/news">News</a>
				</li>
               	<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/featured_courses">Featured Courses</a>
				</li>
			</ul>
		</li>
        <li>
			<a href="javascript:void(0)" class="sf-with-ul">Manage</a>
			<ul>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/role_responsibility">Roles and Responsibilities</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/user_roles">User Roles</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/interest">Interest</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/payments">Payments</a>
				</li>
				
                <li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/course_categories">Categories</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/cancelled_users">Cancelled Account</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/reasons">Reasons</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/time">Logout Time</a>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/commission" >Commission</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/free_video" >Free Videos</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/subscription">Subscription On/Off</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/languages_status">Languages On/Off</a>
				</li>
                                 <li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/paypal">PayPal</a>
				</li>
                                 <li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/email">E-mail Options</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/banners">Banners</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/meta_tags">Meta Tags</a>
				</li>
				<li>
					<a href="<?php echo HTTP_ROOT;?>admin/users/meta_data_settings">Meta Data Settings</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="javascript:void(0)" class="sf-with-ul">Price</a>
			<ul>
				<li>
				<a href="<?php echo HTTP_ROOT;?>admin/users/price" >Subscription Prices</a>
				</li>
				<li>
				<a href="<?php echo HTTP_ROOT;?>admin/users/course_prices" >Course Prices</a>
				</li>
				<li>
				<a href="<?php echo HTTP_ROOT;?>admin/users/lesson_prices" >Lesson Prices</a>
				</li>
				
				
				
			</ul>
		</li>
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/discounts" class="sf-with-ul">Discounts</a>
		</li>
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/reports_on_reviews" class="sf-with-ul">Reports</a>
		</li>
		<li>
			<a href="<?php echo HTTP_ROOT;?>admin/users/news_letter" class="sf-with-ul">Newsletter</a>
		</li>
		

	</ul>
	
</div>
