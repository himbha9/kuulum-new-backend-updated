<!-- Footer section -->
<footer>
  <div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-md-push-4 col-sm-push-4 social_icon">
       
        <ul>
            <li>                         	
            <?php echo $this->Html->link($this->Html->image('front/twitter_img.svg',array('width'=>'50','height'=>'50','alt'=>'Twitter')),'https://twitter.com/L10nTrain', array('escape' => false));?>
            </li>   
            <li>   
            <?php echo $this->Html->link($this->Html->image('front/facebook_img.svg',array('width'=>'50','height'=>'50','alt'=>'Facebook')),'https://www.facebook.com/l10ntrain', array('escape' => false));?>
            </li>
            <li>  
            <?php echo $this->Html->link($this->Html->image('front/linked-in_img.svg',array('width'=>'50','height'=>'50','alt'=>'Linkedin')),'https://www.linkedin.com/company/localization-training-llc', array('escape' => false));?>
            </li>   
            <li>
            <?php echo $this->Html->link($this->Html->image('front/google_plus_img.svg',array('width'=>'50','height'=>'50','alt'=>'Google Plus')),'https://google.com/+L10ntrain', array('escape' => false));?>
            </li>
        </ul>
 <?php if($this->Session->read("LocTrain.member_type")== 3){?>
              <div class="PoweredBy PoweredBy1 powerByMember">
<?php } else{?>
<div class="PoweredBy PoweredBy1">
<?php } ?>
<?php echo __('powered_by');?> 
                  <?php echo $this->Html->image('front/powered_by_kuulum.svg',array('width'=>'80','height'=>'40','alt'=>'Kuulum'));?>
                   <?php echo __('kuullum');?></div>  
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-md-pull-4 col-sm-pull-4">
        <h2><?php echo __('learner_and_teacher');?></h2>
        <ul>
            <li> <?php echo $this->Html->link(__('news'),array('controller'=>'Home','action'=>'news'));?></li>
            <li><?php echo $this->Html->link(__('support'),array('controller'=>'Home','action'=>'help'),array());?></li>
            <li> <a href="javascript:void(0);"><?php echo $this->Html->link(__('membership_details'),array('controller'=>'Home','action'=>'membership_detail')); ?></a></li>
            <?php if($subs_function==1)
            { ?>
            <li> <a href="javascript:void(0);"> <?php echo $this->Html->link(__('subscription_plans'),array('controller'=>'Members','action'=>'subscription')); ?> </a></li>
            <?php } if($m_role_id!=3){
		if($this->Session->read('LocTrain.login')=='1'){ 
		?>		
            <li><?php echo $this->Html->link(__('becoming_an_author'),array('controller'=>'Members','action'=>'upgrade_membership'),array());?>
		</li> <?php }else{?>
<li><?php echo $this->Html->link(__('becoming_an_author'),'javascript:void(0)',array('class'=>'becoming_an_author'));?>
	<?php } } ?>
            <li> <a href="javascript:void(0);"> <?php echo $this->Html->link(__('author_terms_and_conditions'),array('controller'=>'Home','action'=>'author_terms'),array());?></a></li>
            <?php if($this->Session->read("LocTrain.member_type")== 3){ ?>
            <li> <a href="javascript:void(0);"> <?php echo $this->Html->link(__('send_us_a_training_proposal'),array('controller'=>'Home','action'=>'training_proposal'),array());?></a></li>
            <li> <a href="javascript:void(0);"> <?php echo $this->Html->link(__('best_practices_for_trainers'),array('controller'=>'Home','action'=>'best_practices'),array());?></a></li>
            <li> <a href="javascript:void(0);"><?php echo $this->Html->link(__('style_guide_for_trainers'),array('controller'=>'Home','action'=>'training_style'),array());?> </a></li>
<?php } ?>
        </ul>
      </div>
     
         <div class="col-lg-4 col-md-4 col-sm-4">
        <h2><?php echo __('company_and_legal');?></h2>
        <ul>                 
            <li><?php echo $this->Html->link(__('terms_of_use'),array('controller'=>'Home','action'=>'terms_of_use'),array());?> </li>
            <li><?php echo $this->Html->link(__('cookie_policy'),array('controller'=>'Home','action'=>'cookie_policy'),array());?></li>
            <li><?php echo $this->Html->link(__('privacy_policy'),array('controller'=>'Home','action'=>'privacy_policy '),array());?></li>
            <li><?php echo $this->Html->link(__('about_localization_training'),array('controller'=>'Home','action'=>'about_company'),array());?> </li>
            <li><?php echo $this->Html->link(__('contact_us'),array('controller'=>'Home','action'=>'contact_us'),array());?> </li>
        </ul>
      </div>
 <?php if($this->Session->read("LocTrain.member_type")== 3){?>
         <div class="PoweredBy PoweredBy2 powerByMember">
<?php } else{ ?>
  <div class="PoweredBy PoweredBy2">
<?php } ?>
<?php echo __('powered_by');?> 
                  <?php echo $this->Html->image('front/powered_by_kuulum.svg',array('width'=>'80','height'=>'40','alt'=>'Kuulum'));?>
                   <?php echo __('kuullum');?></div>
    </div>
  </div>
  <hr> </hr>
  <div class="container-fluid">
    <div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 copywrite">
<p><?php
        $n_text = String::insert(__("copyright_date_localization_training_llc_all_rights_reserved"),array('date' => date('Y')));
         echo $n_text; ?>
    </p>
</div>
    </div>
  </div>
</footer>
<!-- End Footer section -->
