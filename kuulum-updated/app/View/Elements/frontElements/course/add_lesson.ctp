<div id="login-box" class="add-lesson-popup">
    <a href="#" class="close"><img src="../img/dlt.png" class="btn_close" title="Close Window" alt="Close" /></a>
    <div class="rd_rv_otr">
        <h2><?php echo __('add_new_lesson'); ?></h2>
        <div class="rd_rv_cntnr">
      		<div class="breadcrumb_botcont">
                <div class="crtcrs_popup_otr">
                <div class="reg_flds">
                  <label class="regfld_label_crt"><?php echo __('lesson_title'); ?>:</label>
                  <span class="regfld_txtcont_crt">
                   <input class="regfld_txt gradient" value="Lorem ipsum dolor sit amet, consectetur adipisicing" /> 
                  </span> 
                </div>
                <div class="reg_flds">
                  <label class="regfld_label_crt"><?php echo __('description'); ?>:</label>
                  <span class="regfld_txtcont_crt">
                   <textarea class="regfld_txtera_dsc">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aperiam, eaque ipsa totam rem aperiam, eaque ipsa quae ab illo Sed ut perspiciatis unde omnis 
                   </textarea>
                  </span> 
                </div>
                <div class="reg_flds">
                  <label class="regfld_label_crt"><?php echo __('video'); ?>:</label>
                  <span class="regfld_txtcont_crt">
                    <span class="lsn_vd">
                        <iframe width="545" height="300" src="http://www.youtube.com/embed/M_pIK7ghGw4" frameborder="0" allowfullscreen></iframe> 
                    </span>
                    <span class="lsn_vd_btn">
                        <a class="crt_pop <?php /*?>crt_pop_upld<?php */?>" href="javascript:void(0);"> <img src="../img/upld.png" /><?php echo __('upload_video'); ?> </a>
                        <a class="crt_pop <?php /*?>crt_pop_upld<?php */?>" href="javascript:void(0);"> <img src="../img/rmv.png" /> <?php echo __('remove_video'); ?> </a>
                        <p class="mrgn_ryt_crtpp"  <?php /*?>class="crt_popup_tm"<?php */?>> 3 minutes 12 seconds'</p>
                    </span>
                    <span class="crtsv_btn">
                    	<input type="button" value="Save" />
                    </span>
                  </span> 
                </div>
              </div>       
      <?php echo $this->Form->end(); ?>
    </div>
    	</div>
    </div>    
 </div> 