<?php echo $this->Html->script('/jwplayer/jwplayer'); $locale = $this->Session->read('LocTrain.locale');?>
<div class="lsn_flds">
  <label class="lsn_label"><?php echo __('lesson_title:'); ?></label>
    <span class="lsn_para">
        <p><?php echo $lesson_highlight['CourseLesson']['title'];?></p>	
    </span>
</div>
<div class="lsn_flds">
  <label class="lsn_label"> <?php echo __('description'); ?></label>
    <span class="lsn_para">
        <p> <?php echo $lesson_highlight['CourseLesson']['description'];?></p>	
    </span>	
</div>

</div>
<div class="lsn_flds">
  <label class="lsn_label"> <?php echo __('video:'); ?></label>
    <span class="lsn_para" id="render_video">
        <?php
		  if($lesson_highlight['CourseLesson']['video'] != ''){
			  $video = $lesson_highlight['CourseLesson']['video'];
		  }else{
			  $video='';
		  }
         if($video !="" && $lesson_highlight['CourseLesson']['extension']=="swf")
		  {
			if($lesson_highlight['CourseLesson']['image'])
			{
				if(file_exists('files/members/course_lesson/'.$lesson_highlight['CourseLesson']['image']))
				{ 
					echo $this->Html->image('/files/members/course_lesson/'.$lesson_highlight['CourseLesson']['image'],array( 'class'=>'img_bddr','width'=>480,'height'=>271));
				}else {
					echo $this->Html->image('swf_thumb.png',array( 'class'=>'img_bddr','width'=>469,'height'=>250));  
				}	
			}else {
					echo $this->Html->image('swf_thumb.png',array( 'class'=>'img_bddr','width'=>469,'height'=>250));  
			}  
			
		  } 
		  else if($video !="" && $lesson_highlight['CourseLesson']['extension']=="zip")
		  {
			if($lesson_highlight['CourseLesson']['image'])
			{
				if(file_exists('files/members/course_lesson/'.$lesson_highlight['CourseLesson']['image']))
				{ 
					
					echo $this->Html->image('/files/members/course_lesson/'.$lesson_highlight['CourseLesson']['image'],array( 'class'=>'img_bddr','width'=>469,'height'=>250));
				}else {
					echo $this->Html->image('attractive_big.png',array( 'class'=>'img_bddr','width'=>469,'height'=>250));
				}	
			}else {
					echo $this->Html->image('attractive_big.png',array( 'class'=>'img_bddr','width'=>469,'height'=>250));  
			} 
			
		  }
		  else {
		  if($video != '' && file_exists('files/members/'.$lesson_highlight['Course']['m_id']."/".$video."_smallThumb.png")) {  
				echo $this->Html->image('/files/members/'.$lesson_highlight['Course']['m_id']."/".$video."_smallThumb.png",array( 'class'=>'img_bddr','width'=>469,'height'=>250));   
			} else if ($video != '' && file_exists('files/members/'.$lesson_highlight['Course']['m_id']."/".$video."_thumb.png")) {              
				echo $this->Html->image('/files/members/'.$lesson_highlight['Course']['m_id']."/".$video."_thumb.png",array('class'=>'img_bddr','width'=>469,'height'=>250));  
			} else{
				echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>469,'height'=>250,'class'=>'img_bddr'));
			} 
            }
  
		?>
		<?php 
			 $extension = $lesson_highlight['CourseLesson']['extension'] ; 	
			 $owner_id = base64_encode(convert_uuencode($lesson_highlight['Course']['m_id']));
			 $video = $lesson_highlight['CourseLesson']['video'] ; 	
			 $m_id = $this->Session->read('LocTrain.id');
			
			if($m_id == $lesson_highlight['Course']['m_id']) 
			{
			 
				if($extension=="swf" || $extension=="zip")
			    { 
		?>		
					<div class="vwcrsply_btn" >
						<a class="ply_btn_anchr " href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"></a>
					</div>
		<?php	}else { ?>	
					
					<div class="vwcrsply_btn">
						<a class="ply_btn_anchr ply_btn_anchr1" href="javascript:void(0);"></a>
					</div>	

		<?php
				}
			}else if(trim($lesson_highlight['CourseLesson']['price'])==0.00)
			{  
				if($extension!="swf" && $extension!="zip"){
		?>
				<div class="vwcrsply_btn">
						<a class="ply_btn_anchr ply_btn_anchr1" href="javascript:void(0);"></a>
				</div>
				
		<?php	}else {  ?>
		
				<div class="vwcrsply_btn">
						<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"></a>
				</div>
		
		<?php    }
			}else {   
				if(!$subscribed)
				{
					if($video != '' && $extension!="swf" && $extension!="zip")
					{
		?>
						<div class="vwcrsply_btn">
								<a class="ply_btn_anchr ply_btn_anchr1" href="javascript:void(0);"></a>
						</div>
				
		<?php		}else
					{
						if(!empty($subscription))
						{
		?>	
						<div class="vwcrsply_btn">
							<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"></a>
						</div>
						
		<?php			}else
						{
		?>
						<div class="vwcrsply_btn">
								<a class="ply_btn_anchr ply_btn_anchr1" href="javascript:void(0);"></a>
						</div>	
		<?php			}
					}
				}else {
					
					if($extension!="swf" && $extension!="zip"){
		?>
					<div class="vwcrsply_btn">
						<a class="ply_btn_anchr ply_btn_anchr1" href="javascript:void(0);"></a>
					</div>	
				
		<?php 	   }else {
		?>
					<div class="vwcrsply_btn">
							<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"></a>
					</div>
					
		<?php		
			      }
			   }
			}		
		?>
		
		
    </span>	
</div>