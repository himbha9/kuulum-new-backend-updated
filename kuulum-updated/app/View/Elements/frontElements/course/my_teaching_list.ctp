<?php 
	//print "<pre>" ; print_r($course_list);die; 
	$locale = $this->Session->read('LocTrain.locale');
?>
<?php 
   /* Rating Script [Start] */
        echo $this->Html->script('front/star-rating.js');
?>

<script type="text/javascript">

	<?php if($update_record != '1') { ?>
		$(".tchng_cont_container").attr('id','<?php echo $last_record_id;?>');
		<?php if(!empty($course_list)){ ?>
			$(".tchng_cont_content").attr('id','<?php echo $last_id;?>');
		<?php }else { ?>
			$(".tchng_cont_content").attr('id','0');
		<?php } ?>	
	<?php } ?>
            $('document').ready(function(){
                $(".rating").rating({
                    starCaptions: function(val) {
                        if (val < 3) {
                            return val;
                        } else {
                            return 'high';
                        }
                    },
                    starCaptionClasses: function(val) {
                        if (val < 3) {
                            return 'label label-danger';
                        } else {
                            return 'label label-success';
                        }
                    }
                });
                
            });
$(function(){
			
			$('.read_review').on('click',function() {

				$(".load_top").show();
				var course_id = $.trim($(this).attr('rel'));
				
				$(".login-popup").hide();
				var loginBox = $(this).attr('href');
				
				$.post(ajax_url+'Members/read_reviews', {course_id: course_id}, function(resp){
					
						$(".load_top").hide();
						
						$('.render_course').html(resp);
						
						/*$(loginBox).fadeIn(300);
						
						var popMargTop = ($(loginBox).height() + 24) / 2; 
						var popMargLeft = ($(loginBox).width() + 24) / 2; 
						
						$(loginBox).css({ 
							'margin-top' : -popMargTop,
							'margin-left' : -popMargLeft
						});
						
						$('body').append('<div id="mask"></div>');
						$('#mask').fadeIn(300);		*/
					$('#review-box').modal('show');				
					
				});
				
				return false;
			});
			$( document ).ajaxStart(function() {
				$('.read_review').removeClass('read_review').addClass('pause_review');
			});
			
			$( document ).ajaxStop(function() {
				$('.pause_review').removeClass('pause_review').addClass('read_review');
			});
	});
              
</script>
<?php

		
foreach($course_list as $list){?>	

<div class="row video_sec video_sec2 video_sec4 video_sec_teaching" rel="<?php echo base64_encode(convert_uuencode($list['Course']['id']));?>"  id="tab_cnt_<?php echo $list['Course']['id'];?>">
    <div class="col-lg-4 col-md-4 col-sm-4 VideoBoxLeft">
        <?php 	$video = ''; $extension=''; $image='';$less_image='';
        $m_id = $this->Session->read('LocTrain.id');
        $decoded_id = $list['Course']['id'];
                if($this->Session->read('LocTrain.id'))
		{
			
			 $is_rated = $this->Utility->getCourseRating($m_id,$decoded_id);
			$rate=''; 
			   for($i=0;$i<count($is_rated);$i++)
			   {
			   
			   $rate+=$is_rated[$i]['CourseRating']['rating'];
			   }
			  $is_rated1=$rate/count($is_rated);
			   if(!empty($is_rated1))
			   {
			   if(count($is_rated)>1)
			   {
			   $rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
			   }
			   else
			   {
			   $rating=$is_rated1;
			   }
			    
			   }
			else
			{
				$rating= 0;
			}
			$login=1;
		}
		else
		{
			$rating=0;
			$login=0;
		}
        
					if(!empty($list['CourseLesson'])){
						foreach($list['CourseLesson'] as $lesson){

							if($lesson['video'] != ''){
								$video = $lesson['video'];
                                                                $extension = $lesson['extension'];
                                                                $image = $lesson['image'];
								break;
							}
						}
			  foreach($list['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                        $less_image=$lesson['image'];
                        break;
                    }
                }	
					}
		
					$gross_income = 0;
					$total_income = 0;
					$total_inc = 0;
					$commission_amount=0;
					$total_commission=0;
					$member_exists = array();
					$net_total_income=0;
					foreach($list['CoursePurchaseList'] as $purchase_income)
					{//print_r($purchase_income);
 					$discount_p='0.00';
                           		$price =$purchase_income['lesson_id']!="" ? $purchase_income['CourseLesson']['price']:$purchase_income['purchasedprice'];  
                             		$discount=$purchase_income['CoursePurchase']['discount'];
                            		if(!empty($discount)){
							$pr=($discount*$price)/100;
							$discount_p=round($pr,2);
					}
					$c_l_price=$price-$discount_p;
					$comm_percent=$purchase_income['commission'];
					$comm_amount=$c_l_price*($comm_percent/100);
					$net_income = $c_l_price-$comm_amount;
					$net_total_income=$net_total_income+$net_income;
						/*if($purchase_income['type']==0)
						{
							if($purchase_income['lesson_id']!= "" || $purchase_income['lesson_id']!=NULL)
							{ 
								$total_inc = $purchase_income['CourseLesson']['price'];
								$commission_amount =  $total_inc * ($purchase_income['commission']/100) ;
								
							}else {
								$total_inc = $purchase_income['Course']['price'];
								$commission_amount =  $total_inc * ($purchase_income['commission']/100) ;
							}
							$gross_income += $total_inc;
							$total_commission += $commission_amount;	
							
							if(!empty($purchase_income['mem_id']))
							{
								if(!in_array($purchase_income['mem_id'],$member_exists))
								{
									$member_exists[] = $purchase_income['mem_id'];
								}	
							}
						}*/
					}
					
					$total_customer = count($member_exists);
					$total_income = $gross_income-$total_commission;
					$total_actual_income =	 0.01 *($total_income*100);
					//pr($total_income);
					
			?>
			<?php 

    $course_image = $list['Course']['image'];
 
$path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$course_image);
                    if(!empty($course_image) && $path){
                       echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'thumb/'.$course_image),array('width'=>'','height'=>'','class'=>'img-responsive')),array('controller'=>'Members','action'=>'course_edit',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'course_link','escape'=>false));        
                    }else if(!empty($less_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image)){
                       echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$less_image),array( 'class'=>'img-responsive'));
                    }else if(!empty($image) &&  $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$image)){
                       echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$image),array( 'class'=>'img-responsive'));     
                    }else
                    { 
                       echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('url'=>array('controller'=>'Members','action'=>'course_edit',base64_encode(convert_uuencode($list['Course']['id'])))),array('class'=>'img-responsive'));   
                    }   
				
              
			?>
			
          
        <h2><?php echo $this->Html->link($list['Course']['title'],array('action'=>'course_edit',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'course_link')); ?> </h2>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 RatingBox">
            <div class="col-xs-6  no-padding">
            <form>
                 <?php if($m_id != $list['Course']['m_id']) {?>
                <input id="input-21a"   data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
              <?php  }else{?>
                    <input id="input-21a" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                <?php } ?>
            </form>
        </div>
        <div class="col-xs-6 Price  no-padding"><?php echo  $this->Utility->externlizePrice($language,$locale,$list['Course']['price']);?></div>
        <p><?php echo $this->Text->truncate($list['Course']['description'],'170',array('exact'=>false)); ?>	</p>
    </div>        
    <div class="col-lg-4 col-md-4 col-sm-4 TeachCourseInfo">
            <div class="Link"><?php echo __('students');?>: <span><?php echo $total_customer?$total_customer:"0"; ?></span></div>
        <div class="Link"> <?php echo $this->Html->link(__('income').': ',array('action'=>'view_course_income',base64_encode(convert_uuencode($list['Course']['id']))),array()); ?><span><?php echo $net_total_income ?__('usd').'&nbsp;'.$this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$net_total_income):"0"; ?></span></div>
        <div class="Link"><a class="read_review" rel="<?php echo base64_encode(convert_uuencode($list['Course']['id']));?>"><?php echo __('review');?>:</a> <span> <?php echo $list['Course']['review']; ?> </span> </div>
        <div class="Link"> <?php echo $this->Html->link(__('edit'),array('action'=>'course_edit',base64_encode(convert_uuencode($list['Course']['id']))),array('id'=>'tc_two')); ?> </div>
        <div class="Link"><?php echo $this->Html->link(__('retire_this_course'),'javascript:void(0);',array('class'=>'retire_c','id'=>'tc_three','value'=>$list['Course']['id'],'rel'=>base64_encode(convert_uuencode($list['Course']['id'])))); ?> </div>
    </div>
</div>
<?php }?>
<div class="render_course">
	 <?php echo $this->element('frontElements/course/review'); ?>   
</div>