<?php $locale = $this->Session->read('LocTrain.locale');?>
<div class="row">
            	<div class="col-lg-12">
                <table data-filter="#filter" class="footable PurchaseHistoryTable" > 
                    <thead>
                      <tr> 
                        <th width="16%"  data-hide="phone,tablet" data-sort-initial="true" class="text_left"><a href="javascript:;"><?php echo __('date_and_time');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="20%" data-hide="phone,tablet" data-sort-initial="true" class="text_left"><a><?php echo __('transaction_number');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="49%" class="text_left "><a><?php echo __('item');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="15%" data-class="expand" class="exp_price"><a><?php echo __('price')?><i class="fa fa-caret-up"></i></a></th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $gross_total=0;
				$pay_amount=0;
$pay_amount_arr=array();
			   $i='0';?>
                        <?php  //echo '<pre>' ;print_r($info);echo '</pre>' ;
					
                        if(!empty($info))
			{
				foreach($info as $row)
					{ ?>
                        
                                    <tr><?php 
									$formatter = IntlDateFormatter::create($this->Session->read('LocTrain.locale'),
									IntlDateFormatter::SHORT,
									IntlDateFormatter::SHORT,
									$this->Session->read('Timezone'));
                                                                        
							if(!empty($row['CoursePurchase']['date_added']))
							{
$dt_en=$row['CoursePurchase']['date_added'];
							$dt = new DateTime(date('Y-m-d H:i:s',$row['CoursePurchase']['date_added']));
							$date_show= $formatter->format($dt)."\n"; 
							}
							else
							{$dt_en=$row['CoursePurchaseList']['date_created'];
							$dt = new DateTime(date('Y-m-d H:i:s',$row['CoursePurchaseList']['date_created']));
							$date_show= $formatter->format($dt)."\n"; 
                                                        }
								//$income_time = $this->Time->convert($row['CoursePurchase']['date_added'],$this->Session->read('Timezone'));
								//echo date('d-m-Y',$income_time); 
								//echo date('d-m-Y',$row['CoursePurchase']['date_added'])


$date1=date('Y-m-d H:i:s',$dt_en);
$date1= strtotime($date1);
if(empty($date1)){$date1=strtotime('1990-01-01 00:00:00');}

								?>

                                            <td class="text_left"  data-value="<?php echo $date1; ?>" ><?php echo $date_show;?></td>

                                            <td class="text_left" ><?php echo $row['CoursePurchase']['txn_id']?></td>
                                            <td class="text_left"><?php if(!empty($row['CourseLesson']['id'])){ ?>
										<?php 
//echo $row['CourseLesson']['title'];
echo $this->Html->link($row['CourseLesson']['title'],array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($row['Course']['id']))),array('class'=>''));
?>
									<?php }
									else
									{ ?>
										<?php 
 echo $this->Html->link($row['Course']['title'],array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($row['Course']['id']))),array('class'=>''));
?>
									<?php } ?></td>
                                            <td><?php
											 $purchasedprice=$row['CoursePurchaseList']['purchasedprice']-$row['CoursePurchaseList']['discount'];
											echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$purchasedprice);//echo $row['CoursePurchaseList']['purchasedprice'] ;
											/*if(!empty($row['CourseLesson']['id'])){ ?>
							<?php  echo  number_format($row['CourseLesson']['price'],'2','.',''); ?><?php }
									else
									{ ?>
									<?php echo  number_format($row['Course']['price'],'2','.',''); ?>
							
							<?php }*/ ?>

<?php /*
// discount logic strats here
$trans_id=$row['CoursePurchase']['txn_id'];
$item_price=0;
 if(!empty($info))
{
	foreach($info as $info1)
	{
		if($trans_id==$info1['CoursePurchase']['txn_id']){
			if(!empty($info1['CourseLesson']['id'])){
			$price=number_format($info1['CourseLesson']['price'],'2','.','');
			}else{
			$price=number_format($info1['Course']['price'],'2','.','');
			}
			$item_price=$item_price+$price;
		}
	}
}
?>
<?php //echo '-'. number_format($row['CoursePurchase']['payment_amount'],'2','.','').'-'.$item_price;
if(number_format($item_price,'2','.','')==number_format($row['CoursePurchase']['payment_amount'],'2','.','')){
	if(!empty($row['CourseLesson']['id'])){
	echo number_format($row['CourseLesson']['price'],'2','.','');
	}else{
	echo number_format($row['Course']['price'],'2','.','');
	}
}else{

// If a discount is available for this item, then show price with discount
 $discount=number_format((100-(($row['CoursePurchase']['payment_amount']/$item_price)*100)),'2','.','');
	if(!empty($row['CourseLesson']['id'])){
			$t_price=number_format($row['CourseLesson']['price'],'2','.','');
			}else{
			$t_price=number_format($row['Course']['price'],'2','.','');
			}
		echo $pay_price=number_format(($t_price-($t_price*($discount/100))),'2','.','');

}
*/
?>
		</td>
                                        </tr>     
                                       <?php 
                                       
                                       if(!empty($row['CourseLesson']['id'])){
                                                $gross_total=$gross_total+$row['CourseLesson']['price'];
                                                }else{

                                                        $gross_total=$gross_total+$row['Course']['price'];
                                                }
                                              //echo  $pay_amount=$pay_amount+$row['CoursePurchase']['payment_amount'];echo'--';
$pay_amount_arr[$row['CoursePurchase']['txn_id']]=$row['CoursePurchase']['payment_amount'];

                                           $i++;
                                        }
                                }else{
                        ?>
                        <tr class="noRecordCls">
                        <td colspan="5"><?php echo __('no_records_found');?></td>
                         
                    	</tr>
                                <?php } ?>
                    </tbody>      
                  </table>
             </div>
            </div>  
            <?php
			if(!empty($info))
			{ 	
		//$result_pay = array_unique($pay_amount_arr);
			$result_pay =$pay_amount_arr;
			if(!empty($result_pay)){
				foreach($result_pay as $row){
				 $pay_amount=$pay_amount+$row;
					}
				}


?>
            <div class="row">
                <div class="col-lg-12">    
                    <table class="no-border PurchaseHistoryTable" width="100%">                    		
                     <!--   <tr>
                            <?php  $discount=$pay_amount-$gross_total;?>
                            <th colspan="2" class="text_right TotalAmount"><?php echo __('total_discount');?><span><?php echo __('usd').'&nbsp;-';?><?php echo  number_format(abs($discount),'2','.',''); ?></span></td>     
                            
                        </tr> -->
                        <tr>                            
                            <th colspan="2" class="text_right TotalAmount"><?php echo __('total');?> <span><?php echo __('usd').'&nbsp;';?><?php echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$pay_amount); ?></span></td>                          
                        </tr>
                    </table>                      
                </div>                               
            </div>
                        <?php } ?>

<!-- Table Sort [Start] -->
<?php
 echo $this->Html->script('front/footable.js');
echo $this->Html->script('front/footable.sortable.js');
?>
<script type="text/javascript">
			$(function() {
					$('table').footable();
			});
</script>
<!-- Table Sort [End] -->
