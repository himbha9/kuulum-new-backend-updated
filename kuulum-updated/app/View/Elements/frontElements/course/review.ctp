<?php

?>
<style>
.rd_rv_data_btm { float: left; display:block; text-align:right; margin:-27px 0px 0px 0px;  width: 100%; }
.rb_rv_snp {
    float: left;
    width: 100%;
}
.review_auh_nam{ color: #a5a5a5;
    float: left;
    margin-bottom: -25px;
    text-align: right;
    width: 100%;}
</style>
<script type="text/javascript">
	
	$(function(){
		
		$('.show_full_review').click(function(){
				
				var rel = $.trim($(this).attr('rel'));
				
				if(rel == 'show_full')
				{
					$(this).attr('rel','show_less');
					$(this).html('<?php echo __('show_less'); ?>');
					$(this).parents('.rd_rv_data_btm').prev().show().prev().hide();
					
				} else {
					
					$(this).attr('rel','show_full');
					$(this).html('<?php echo __('show_more'); ?>');
					$(this).parents('.rd_rv_data_btm').prev().hide().prev().show();
				}
				
		});
		
		$('.pro_review').on('click',function(){
				
				$this = $(this);
				var id = $.trim($this.attr('id'));
				
				if($this.hasClass('promote_disable'))
				{
					var old_cls = 'promote_disable';
					var new_cls = 'promote';					
				} else {					
					var old_cls = 'promote';
					var new_cls = 'promote_disable';
				}
				
				if(id != '')
				{
					$.post(ajax_url+'Members/promote_review',{id: id},function(resp){
							
							if($.trim(resp) == 'done')
							{
								$this.removeClass(old_cls).addClass(new_cls);
							}
							
					});
				}
		});
		
		$('.block_review').on('click',function(){
				
				$this = $(this);
				var id = $.trim($this.attr('id'));
				
				if($this.hasClass('block_disable'))
				{
					var old_cls = 'block_disable';
					var new_cls = 'block';					
				} else {					
					var old_cls = 'block';
					var new_cls = 'block_disable';
				}
				
				if(id != '')
				{
					$.post(ajax_url+'Members/block_review',{id: id},function(resp){
							
							if($.trim(resp) == 'done')
							{
								$this.removeClass(old_cls).addClass(new_cls);
							}
							
					});
				}
		});
		
		

showHideReview();
        $('#show_hide_reviews').click(function(){
            var type=$(this).attr('rel');            
            if(type=='hide'){
               $(this).attr('rel','show'); 
               if($(this).attr('value') ==1){
                   $(this).html("<?php echo __('hide_review');?>");
               }else{
               $(this).html("<?php echo __('hide_reviews'); ?>");
           }
            }else{
               $(this).attr('rel','hide'); 
               if($(this).attr('value') ==1){
                   $(this).html("<?php echo String::insert(__('show_numc_more_reviews'),array('numc' => '1')); ?>");
               }else{
$(this).html($(this).attr('data-info'));
		//$(this).html("<?php echo String::insert(__('show_numc_more_reviews'),array('numc' => '')); ?>");
           //    $(this).html('<?php echo __('show_all').'&nbsp;';?>'+ $(this).attr('value')+' <?php echo '&nbsp;'.__('reviews');?>');
           }
            }
            showHideReview();
            
        });
        
    });
    function showHideReview(){
        if($('.rd_rv_blk').length>3){
            $('.rd_rv_blk').eq(2).nextAll('.rd_rv_blk').toggle();
        }
    }
	
</script>
<!-- Modal -->
                <div class="modal fade" id="review-box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            	<h4 class="modal-title" id="myModalLabel"><?php echo __('reviews'); ?></h4>
                            </div>
                            <div class="modal-body">
                               <?php //echo '<pre>';print_r($course_info['CourseRemark']);echo '</pre>';
				if(isset($course_info) && !empty($course_info['CourseRemark'])) 
				{$i=1;
					foreach($course_info['CourseRemark'] as $remark)
					{
						$id = base64_encode(convert_uuencode($remark['id']));
					
			?><div class="rd_rv_blk">
                               <div class="form-group rd_rv_data " id="show_<?php echo $i;?>" >
                                    <p><?php echo $this->Text->truncate($remark['comment'],200,array('ellipses'=>'...','exact'=>false)); ?>
<br>
				<!-- <span style="color:#A5A5A5;text-align:right; float: left; width: 100%;"><?php echo $remark['anonymous'] == 0 ? $remark['Member']['given_name'].' '.$remark['Member']['family_name'] : __('anonymous_user'); ?></span>-->
				</p>
								</div>
				<div class="form-group rd_rv_data"  style="display:none;" >
				<p>
					<?php  echo nl2br($remark['comment']); ?><br>
				
				</p>
				</div>
<div class="form-group rd_rv_data_btm"  style="text-align:left">
<?php if(strlen($remark['comment']) > 200){ ?>
								
									<span class="rb_rv_snp">
										<?php echo $this->Html->link(__('show_more'),'javascript:void(0);',array('class'=>"read_more_review show_full_review",'rel'=>'show_full')); ?>
									</span>
									
								<?php } ?>
								<span class="review_auh_nam"><?php echo $remark['anonymous'] == 0 ? $remark['Member']['given_name'].' '.$remark['Member']['family_name'] : __('anonymous_user'); ?></span>
								<span class="rb_rv_snp2" >
									<?php
										$promote = $remark['promote'] == 0 ? 'promote_disable' : 'promote';
										echo $this->Html->link(__('promote'),'javascript:void(0)',array('class'=>'ryt_flt pro_review '.$promote,'id'=>$id,'style'=>'text-align:left'));
									?>
								</span>&nbsp;
								<span class="rb_rv_snp2">
									<?php
										$block = $remark['block'] == 0 ? 'block_disable' : 'block';
			//echo $this->Html->link(__('block'),'javascript:void(0)',array('class'=>'ryt_flt block_review '.$block,'id'=>$id));
									?> 
								</span>
</div>
</div>
<?php 	$i++;}
				} else {
					echo __("no_reviews_received");
				}
			?>
   <?php if(isset($course_info)){if(count($course_info['CourseRemark'])>3){?>
                <div class="Link"><a href="javascript:;" id="show_hide_reviews" rel="hide" data-info="<?php echo String::insert(__('show_numc_more_reviews'),array('numc' =>count($course_info['CourseRemark'])-3)); ?>" value="<?php echo count($course_info['CourseRemark'])-3; ?>"><?php if(count($course_info['CourseRemark'])-3 ==1){ ?><?php echo String::insert(__("show_numc_more_reviews"),array('numc' => '1')); ?>
<?php } 
else {
?>
<?php echo String::insert(__("show_numc_more_reviews"),array('numc' =>count($course_info['CourseRemark'])-3)); ?>
<?php } ?></a></div>              
                <?php } } ?>
								
                            </div>                            
                        </div>
                    </div>
                </div>



