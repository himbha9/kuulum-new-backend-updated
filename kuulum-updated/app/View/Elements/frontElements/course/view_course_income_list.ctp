<?php $locale = $this->Session->read('LocTrain.locale');?>
 <div class="row">
    <div class="col-lg-12">  
 <?php $class = $course_purchase ? '':'borderClass'; ?>
<table data-filter="#filter" class="footable" data-page-size="5">
                    <thead>
                      <tr>
                        <th width="52%" data-class="expand" data-sort-initial="true" class=""><a><?php echo __('date_and_time');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="13%" data-hide="phone" ><a><?php echo __('transaction-id');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="12%" data-hide="phone,tablet" class=""><a><?php echo __('customer');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="12%" data-hide="phone,tablet" class=""><a><?php echo __('lesson');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="12%" data-hide="phone,tablet" class=""><a><?php echo __('price');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="12%" data-hide="phone,tablet" data-type="numeric"><a href="javascript:;"><?php echo __('commission');?><i class="fa fa-caret-up"></i></a></th>
                        <th width="12%"><a href="javascript:;"><?php echo __('net');?><i class="fa fa-caret-up"></i></a></th>
                      </tr>
                    </thead>
                    <tbody  class="overf_scroll  <?php echo $class; ?>">
                     <?php
			if(!empty($course)) 
			{
				$i=1;
                                $total=0;
                                $lesson_total=0;
                                $total_commision =0;
                                $net_total_income = 0;
                                foreach($course as $course)
                                {//echo '<pre>';print_r($course); echo '</pre>';
                                         ?>
                        <tr>
                            <td width="52%"><?php 
					$formatter = IntlDateFormatter::create($this->Session->read('LocTrain.locale'),
							IntlDateFormatter::SHORT,
							IntlDateFormatter::SHORT,
							$this->Session->read('Timezone'));
							
							 echo $formatter->format($course['CoursePurchase']['date_added']), "\n"; 
				?></td>
                            <td width="13%"><?php echo $course['CoursePurchase']['txn_id']; ?>
                            </td>
                            <td width="12%">
                                <?php if($course['CoursePurchaseList']['status']=="1")
										  {
											$course['Member']['given_name']=__('not_a_member_now');
										
										  }
									?>
				<?php echo $course['Member']['given_name'] ? $course['Member']['given_name'] : __('not_a_member_now'); ?></td>
                            <td width="12%"><?php echo $course['CourseLesson']['title']? $course['CourseLesson']['title'] :__('all_lessons'); ?></td>
                            <td width="12%"><?php //$price =$course['CoursePurchaseList']['lesson_id']!="" ? $course['CourseLesson']['price']:$course['Course']['price'];  
                           
                            $discount_p='0.00';
                            $price =$course['CoursePurchaseList']['lesson_id']!="" ? $course['CourseLesson']['price']:$course['CoursePurchaseList']['purchasedprice'];  
                             $discount=$course['CoursePurchase']['discount'];
                            if(!empty($discount)){
								$pr=($discount*$price)/100;
							$discount_p=round($pr,2);
							}
                            ?>
                            <?php echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,($price-$discount_p));  ?>
                            </td>
                            <td width="12%" data-value="78025368997">
                                <?php //pr($course);die;
                                $price_n=$price-$discount_p;
                               $lesson_total=$lesson_total+$price_n;
					         $comm_percent=$course['CoursePurchaseList']['commission'];
					         $c_l_price_1 =$course['CoursePurchaseList']['lesson_id']!="" ? $course['CourseLesson']['price']:$course['CoursePurchaseList']['purchasedprice'];
					         $c_l_price=$c_l_price_1-$discount_p;
							 $comm_amount=$c_l_price*($comm_percent/100);
							  $a=$comm_amount;  
echo  '-'.$this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$a);
							?>
									
                            </td>
                            <td width="12%">
   <?php   
						  	
							//$net_income = $price-$comm_amount;
							$net_income = $c_l_price-$comm_amount; 
							echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$net_income);
							//echo number_format($net_income,'2','.','');	
					 ?>
</td>
                        </tr>
			<?php 	 $i++;
			$total_commision=$total_commision+$comm_amount;
			$net_total_income=$net_total_income+$net_income;
			
		}?>
                    <?php  } else { ?>
                    <tr class="noRecordCls">
                        <td colspan="5"><?php echo __('no_records_found');?></td>
                         
                    </tr>
          
            <?php } ?>
                    </tbody>
                    
</table>
    </div></div>
<div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-md-push-6 col-sm-push-6 IncomeBalanceWrapper">                			
                    <?php
			if(!empty($course)) 
			{ ?>
                      <table class="no-border" width="100%">
                    <tr>
                        <td width="50%"><?php echo __('lessons_sold');?></td>
                        <td width="50%"><?php // echo  number_format($lesson_total,'2','.',''); 
                        echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$lesson_total);
                        
                        ?></td>
                      </tr>
                      <tr>
                        <td><?php echo __('gross_income');?></td>
                          <td><?php //echo  number_format($total,'2','.',''); 
                          echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$total);                       
                          
                          ?></td>
                      </tr>
                      <tr>
                        <td><?php echo __('commission');?></td>
                          <td>-<?php // echo  number_format($total_commision,'2','.','');
                           echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$total_commision);  
                          
                           ?></td>
                      </tr>
                      <tr>
                        <th><?php echo __('your_net_income');?></th>
                          <th><?php echo __('usd');?><?php echo '&nbsp;&nbsp;';
                         /* number_format($net_total_income,'2','.','')*/; 
                           echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$net_total_income); 
                          
                          ?></th>
                      </tr>
                   </table>
                      <?php }?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-md-pull-6 col-sm-pull-6 IncomeDescWrapper">
                    <p>
                        <?php echo __('the_income_for_your_course_is_summarized_below_at_the_end_of_each_month_you_receive_a_payment_from_localization_training_llc_for_the_net_sale_in_the_current_month_please_contact_us_immediately_if_there_is_a_discrepancy_between_what_you_see_and_what_you_received');?> 
                    </p>
																</div>
            </div>      	                                
        	
 <!-- Table Sort [Start] -->
<?php
 echo $this->Html->script('front/footable.js');
echo $this->Html->script('front/footable.sortable.js');
?>
<script type="text/javascript">
			$(function() {
					$('table').footable();
			});
</script>
<!-- Table Sort [End] -->
