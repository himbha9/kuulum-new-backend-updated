        <?php
			$locale = $this->Session->read('LocTrain.locale');
			if(!empty($alpha_courses)){
			 foreach($alpha_courses as $courses) { ?>
             
             
        <span class="alpha_dscrtp_ryt_para">
            <span class="alpha_dscrtp_ryt_vdo">
                <h4><?php echo $this->Html->link(wordwrap($courses['Course']['title'],55,"\n",TRUE),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link')); ?></h4>	
                <p><?php echo $this->Text->truncate(strip_tags($courses['Course']['description']),200,array('ellipsis'=>'...','exact'=>false));?></p>
            </span>
            <span class="dscrtp_ryt_vdo_ryt">
               
                <?php 		$video = ''; $extension =''; $image='';
			
                        if(!empty($courses['CourseLesson'])){
                            foreach($courses['CourseLesson'] as $lesson){
                                if($lesson['video'] != ''){
                                    $video = $lesson['video'];
									$extension = $lesson['extension'];
									$image = $lesson['image'];
                                    break;
                                }
                            }
                       }								
             ?>
    
						  <?php 
							
							if($video != '' && $extension=="swf")
							{
								if($image)
								{ 
									if(file_exists('files/members/course_lesson/'.$image))
									{ 
										echo $this->Html->image('/files/members/course_lesson/thumb/'.$image,array( 'class'=>'img_bddr','width'=>135,'height'=>86));
									}else {
										echo $this->Html->image('swf_thumb.png',array( 'class'=>'img_bddr','width'=>135,'height'=>86));
									}	
								}else {
										echo $this->Html->image('swf_thumb.png',array( 'class'=>'img_bddr','width'=>135,'height'=>86));
								}
								
							}else if($video != '' && $extension=="zip")
							{
								if($image)
								{ 
									if(file_exists('files/members/course_lesson/'.$image))
									{ 
										echo $this->Html->image('/files/members/course_lesson/thumb/'.$image,array( 'class'=>'img_bddr','width'=>135,'height'=>86));
									}else {
										echo $this->Html->image('attractive.png',array( 'class'=>'img_bddr','width'=>135,'height'=>86));
									}	
								}else {
										echo $this->Html->image('attractive.png',array( 'class'=>'img_bddr','width'=>135,'height'=>86));
								}
								
							}else {
						  
                              if($video != '' && file_exists('files/members/'.$courses['Course']['m_id']."/".$video."_smallThumb.png")) {  
                                  echo  $this->Html->link($this->Html->image('/files/members/'.$courses['Course']['m_id']."/".$video."_smallThumb.png",array('width'=>141,'height'=>86 ,'class'=>'ctlg_lndng_vdo')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link img_bddr','escape'=>false)); 
                                  
                              } else if ($video != '' && file_exists('files/members/'.$courses['Course']['m_id']."/".$video."_thumb.png")) {              
                              echo $this->Html->link($this->Html->image('/files/members/'.$courses['Course']['m_id']."/".$video."_thumb.png",array('width'=>141,'height'=>86 ,'class'=>'ctlg_lndng_vdo')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link img_bddr','escape'=>false));  
                                
                              } else{
                                  echo $this->Html->link($this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>141,'height'=>86 ,'class'=>'ctlg_lndng_vdo')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link img_bddr','escape'=>false));
                              } 
							}  
                          ?>
                         <div class="vwcrsply_btn_thmb">
							<?php echo $this->Html->link('',array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'ply_btn_anchr_thmb')); ?>
						</div>       
            </span>
        </span>
        <?php }}else{
			
			echo '<div class="mytch_rcd">'.__('no_records_found_').'</div>';
			} ?>