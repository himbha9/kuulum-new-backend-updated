<?php

$cookie_lessons = array();
		if(!empty($getCoursesindex)){
			
			foreach($getCoursesindex as $key=>$val){
				if(strlen($key)>8)
				{
					$cookie_lessons[] = convert_uudecode(base64_decode($val));
				}
				
			}
		}

$cookie_courses = array();
		if(!empty($getCoursesindex)){
			foreach($getCoursesindex as $course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
			}
			//pr($cookie_courses); die;
		}

$locale = $this->Session->read('LocTrain.locale');
if(!empty($course_subcategory)){
         foreach($course_subcategory as $courses) { ?>
<div class="row video_sec video_sec2 video_sec5 video_sec_click" id="<?php echo base64_encode(convert_uuencode($courses['Course']['id']));?>">
<div class="col-lg-6 col-md-6 col-sm-6 VideoBoxLeft">
          <?php 	$video = ''; $extension =''; $image='';$less_image='';
			
                        if(!empty($courses['CourseLesson'])){
                            foreach($courses['CourseLesson'] as $lesson){
                                if($lesson['video'] != ''){
                                    $video = $lesson['video'];
                                    $extension = $lesson['extension'];
                                    $image = $lesson['image'];
                                    break;
                                }
                            }
  foreach($courses['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                        $less_image=$lesson['image'];
                        break;
                    }
                }
                       }								
             ?>
    
                        <?php 

                       $course_image = $courses['Course']['image'];
               $path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$course_image);
                    if(!empty($course_image) && $path){
                         echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'thumb/'.$course_image),array('width'=>'262','height'=>'138','class'=>'img-responsive')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link','escape'=>false));        
                    }else if(!empty($less_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$less_image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));
                    }else if(!empty($image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$image),array('width'=>'262','height'=>'138','class'=>'img-responsive'));     
                    }else
                    { 
                       echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>'262','height'=>'138','class'=>'img-responsive','url'=>array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id'])))),array('class'=>'img-responsive'));   
                    }
                          ?>
						  
   
    <h2><?php
$title_r=$this->Text->truncate(strip_tags($courses['Course']['title']),50,array( 'ellipsis' => '...','exact' => false,'html' => false)); 
 echo $this->Html->link($title_r,array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link')); ?></h2>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 RatingBox">
       <?php
                                        $m_id = $this->Session->read('LocTrain.id');
                                       $decoded_id = $courses['Course']['id'];
                                            $is_rated=$courses['CourseRating'];
                                                                      $rate=''; 
							   for($i=0;$i<count($is_rated);$i++)
							   {
							   
							   $rate+=$is_rated[$i]['rating'];
							   }
							if(count($is_rated)>0){ $is_rated1=$rate/count($is_rated);}else{ $is_rated1=0;}
			   if(!empty($is_rated1))
			   {
			   if(count($is_rated)>1)
			   {
				$rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
			   }
			   else
			   {
			   $rating=$is_rated1;
			   }
			    
			   }
                                                                   else
                                                                   {
                                                                           $rating= 0;
                                                                   }$login=0;
                                                               if($this->Session->read('LocTrain.id'))
			{
			$login=1;
			}
                                         $first_lesson=isset($courses['CourseLesson'])?$courses['CourseLesson']['0']['id']:0;                                         
$subscription = $this->Utility->getCoursesubscriptionIndex($m_id,$decoded_id,@$first_lesson,$courses['CoursePurchaseList']);

                                       ?>
                                                   <div class="col-xs-6 Rating no-padding">
                                                       <?php if($m_id != $courses['Course']['m_id'] && $login==1) {?>
                                                          <?php if(!empty($subscription)  || $courses['Course']['price']==0.00) { ?>

                                                          <input id="input-21a-<?php echo $courses['Course']['id'];?>" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                          <?php    } else {?>
                                                           <input id="input-21a-<?php echo $courses['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                           <?php } }else{?>
                                                           <input id="input-21a-<?php echo $courses['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                           <?php } ?>
<!-- <div class="RatingNum"><span class="to_rating_<?php echo $courses['Course']['id'];?>"><?= $rating; ?></span> Rating</div> -->
                                                           </div>
  <div class="col-xs-6 Price no-padding"><?php 


if($courses['Course']['price']=='0.00'){ 
$course_details=$this->Utility->getCourseLearningStatusIndex($m_id,$decoded_id,$courses['CoursePurchaseList']);
$class = !empty($course_details)?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
if($class != 'c_added_learning_dsbl'){
echo __('free');
}else{
echo __('purchased');
}

}else{

$subscription_price= $this->Utility->getCoursesubscription_priceLeasson($courses['CoursePurchaseList'],$courses['CourseLesson'],$cookie_lessons);

if($login==1 && ($subscription_price || in_array($decoded_id,$cookie_courses) )){

echo __('purchased');
}else{
echo $this->Utility->externlizePrice($language,$locale,$courses['Course']['price']);
}
}
?></div>
                                     
        <?php if(isset($courses['CourseLesson'][0]['extension']) && $courses['CourseLesson'][0]['extension']!='swf' &&$courses['CourseLesson'][0]['extension']!='zip') {?>
        <div class="Price no-padding"><?php 
                $hours=intval(($courses['Course']['duration_seconds'])/(60*60));
                $minutes=intval(($courses['Course']['duration_seconds'])/60);
                $secs=($courses['Course']['duration_seconds'])%60;
                if($hours)
                {
                        if($hours >= 10){
                                echo $hours.':';
                        } else {
                                echo '0'.$hours.':';
                        }
                        //echo $hours.' '. __('hours');
                }
                if($minutes<60)
                {
                         if($locale =='de'){
                           echo $minutes.':';										
                        }else{
                            if($minutes >= 10){
                                echo $minutes.':';
                        } else {
                                echo '0'.$minutes.':';
                        }
                        }
                      
                }
                if($secs >= 10 ){
                        echo $secs;
                } else {
                        echo '0'.$secs;
                }
                //echo ' '.$secs.' '.__("seconds");
                ?></div>
                <?php } else { ?>
               <div class="Price no-padding"><?php echo __('duration_unknown'); ?> </div>
                <?php } ?>
   <p><?php echo $this->Text->truncate(strip_tags($courses['Course']['description']),100,array('ellipsis'=>'...','exact'=>false));?></p>
</div>        
</div>

  <?php }
            }else{
                    echo '<div class="mytch_rcd">'.__('No record found').'</div>';	
            }
?>
 <?php 
   /* Rating Script [Start] */
        echo $this->Html->script('front/star-rating.js');
?>
<script>
    jQuery(document).ready(function () {
        
    
        $(".rating").rating({
            starCaptions: function(val) {
                if (val < 3) {
                    return val;
                } else {
                    return 'high';
                }
            },
            starCaptionClasses: function(val) {
                if (val < 3) {
                    return 'label label-danger';
                } else {
                    return 'label label-success';
                }
            }
        });
          $('.rating').on('rating.change', function(event, value, caption) {
                var attr_id=$(this).attr('id');
                var arr = attr_id.split('input-21a-');
                var id =arr[1];
                var rating=value;
               $.ajax({
                           url: ajax_url+"Home/update_rating",
                           type: "POST",
                           data: {course_id : id, rating: rating},
                           dataType: "html",
success :function(){
                                
  				 $('.to_rating_'+id).text(rating);


                           }
                     }); 
             
                });
           
	
	
    });
</script>
