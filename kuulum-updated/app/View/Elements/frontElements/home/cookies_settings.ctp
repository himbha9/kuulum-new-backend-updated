<script type="application/javascript">
$(document).ready(function(){
	$(".blank_slide").addClass('pos-left');
	
	$(".click").on('click',function(){
		var getId = $(this).attr('id');
		var width = $('#'+getId).width();
		var margin = $('#'+getId).css('margin-right');
		var totalWidth = parseInt(width) + parseInt(margin)+5;
		if(getId == 'on_style'){
			$(this).parent().prev().children(":input").val('0');	
			$(this).parent().prev().children().css({'width':totalWidth});		
			$(this).parent().prev().children().removeClass('pos-right');
			$(this).parent().prev().children().addClass('pos-left');
		}
		else if(getId == 'off_style'){
			$(this).parent().prev().children(":input").val('1');
			$(this).parent().prev().children().css({'width':totalWidth});
			$(this).parent().prev().children().removeClass('pos-left');
			$(this).parent().prev().children().addClass('pos-right');
		}
	});
});
</script>

<div id="about-us-window" class="login-popup new_popups">
<?php //echo $this->Html->link($this->Html->image('dlt.png',array('width'=>20,'height'=>20,'class'=>'btn_close')),'#reset-password-box',array('class'=>'close','title'=>'Close Window','alt'=>'Close','escape'=>false)); ?>
<div class="rd_rv_otr">

	<h2 class="contctus_hds">My Cookies Settings</h2>
	<div class="rd_rv_cntnr_cookie">
    	<div class="inner_contentauthoring_net">
        	<h3> Cookie Type </h3>
            <h4> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.  </h4>
      		<div class="ryton_off" id="0">
            	<div class="inner_rytonof" id="inner_rytonof">
                	<?php echo $this->Form->input('type1',array('type'=>'hidden','id'=>'type1'));?>
                	<a class="blank_slide" id="blank_slide" href="javascript:void(0);"> </a>

                </div>
                <div class="inner_rytonof" id="inner_rytonof">
                	<a class="on_style click gradient" id="on_style" href="javascript:void(0);"> On </a>
                	<a class="off_style click gradient" id="off_style" href="javascript:void(0);"> Off </a>                    
                </div>
            </div>
        </div>
        <div class="inner_contentauthoring_net">
        	<h3> Cookie Type </h3>
            <h4>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</h4>
           <div class="ryton_off" id="0">
            	<div class="inner_rytonof" id="inner_rytonof">
                	<?php echo $this->Form->input('type2',array('type'=>'hidden','id'=>'type2'));?>
                	<a class="blank_slide" id="blank_slide" href="javascript:void(0);"> </a>
                </div>
                <div class="inner_rytonof" id="inner_rytonof">
                	<a class="on_style click" id="on_style" href="javascript:void(0);"> On </a>
                	<a class="off_style click" id="off_style" href="javascript:void(0);"> Off </a>                    
                </div>
            </div>
        </div>
        <div class="inner_contentauthoring_net no_bottom">
        	<h3>Cookie Type </h3>
            <h4> 
           		Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites.
            </h4>
          <div class="ryton_off" id="0">
            	<div class="inner_rytonof" id="inner_rytonof">
                	<?php echo $this->Form->input('type3',array('type'=>'hidden','id'=>'type3'));?>
                	<a class="blank_slide" id="blank_slide" href="javascript:void(0);"> </a>
                </div>
                <div class="inner_rytonof" id="inner_rytonof">
                	<a class="on_style click" id="on_style" href="javascript:void(0);"> On </a>
                	<a class="off_style click" id="off_style" href="javascript:void(0);"> Off </a>                    
                </div>
            </div>
        </div>
        <span class="lng_itm_lft">
            <input type="submit" class="changeLanguage" value="Save and close" onclick="window.location.reload();">
        </span>
    </div>

 </div>
</div>

</div>	
</div>