<script type="text/javascript">
<?php if($update_record != '1') { ?>
	$(".tchng_cont_container").attr('id','<?php echo $last_record_id;?>');
		<?php if(!empty($course_list)){ ?>
			$(".tchng_cont_content").attr('id','<?php echo $last_id;?>');
		<?php }else { ?>
			$(".tchng_cont_content").attr('id','0');
		<?php } ?>	
	<?php } ?>
</script>

<?php
	$locale = $this->Session->read('LocTrain.locale'); 
	foreach($course_list as $courses) { ?>
	<div class="tchng_content">
		<div class="tchng_coursepic">
			<?php 	$video = '';
			
                        if(!empty($courses['CourseLesson'])){
                            foreach($courses['CourseLesson'] as $lesson){
                                 if($lesson['video'] != '')
								{
									$video = $lesson['video'];
									$extension = $lesson['extension'];
									$image = $lesson['image'];
									break;
								}
                            }
                       }								
             ?>
				<?php
					if($video != '' && $extension=="swf")
					  {
						if($image)
						{ 
							if(file_exists('files/members/course_lesson/'.$image))
							{ 
								echo $this->Html->image('/files/members/course_lesson/thumb/'.$image,array( 'class'=>'img_bddr','width'=>178,'height'=>119));
							}else {
								echo $this->Html->image('swf_thumb.png',array( 'class'=>'img_bddr','width'=>178,'height'=>119));
							}	
						}else {
								echo $this->Html->image('swf_thumb.png',array( 'class'=>'img_bddr','width'=>178,'height'=>119));
						}
						
					}else if($video != '' && $extension=="zip")
					{
						if($image)
						{ 
							if(file_exists('files/members/course_lesson/'.$image))
							{ 
								echo $this->Html->image('/files/members/course_lesson/thumb/'.$image,array( 'class'=>'img_bddr','width'=>178,'height'=>119));
							}else {
								echo $this->Html->image('attractive.png',array( 'class'=>'img_bddr','width'=>178,'height'=>119));
							}	
						}else {
								echo $this->Html->image('attractive.png',array( 'class'=>'img_bddr','width'=>178,'height'=>119));
						}
						
					}else {	
					
						if($video != '' && file_exists('files/members/'.$courses['Course']['m_id']."/".$video."_smallThumb.png")) { ?> 
                        <?php echo $this->Html->link($this->Html->image('/files/members/'.$courses['Course']['m_id']."/".$video."_smallThumb.png",array('width'=>178,'height'=>119)),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link','escape'=>false)); ?>  
                    <?php }else if($video != '' && file_exists('files/members/'.$courses['Course']['m_id']."/".$video."_thumb.png")) { ?>             
                        <?php echo $this->Html->link($this->Html->image('/files/members/'.$courses['Course']['m_id']."/".$video."_thumb.png",array('width'=>178,'height'=>119)),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link','escape'=>false)); ?>  
                    <?php } else{?>
                        <?php echo $this->Html->link($this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>178,'height'=>119)),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link','escape'=>false)); ?>                                   
                    <?php } } ?>
            
            
				
        </div>
		<div class="tchng_picrgt">
			<h3 id="section_subtitle"> <?php echo $this->Html->link(wordwrap($courses['Course']['title'],49,"\n",TRUE),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('class'=>'course_link'));?> </h3>
			<p id="section_desc" class="short_desc"> <?php echo $this->Text->truncate($courses['Course']['description'],'200',array('exact'=>false)); ?> </p>		 
			<p class="long_desc sliding"> <?php echo $courses['Course']['description']; ?> </p>
			<div class="tc_links_ctlg">
			<?php $rl= __('show_less'); ?>
            	<?php $class = strlen($courses['Course']['description']) > 200 ? 'read_more_review':'read_review_disable'; ?>
				<?php echo $this->Html->link(__('show_more'),'javascript:void(0);',array('class'=>$class,'rel'=>$rl));?>
				<?php 
				
						if(!empty($courses['CoursePurchaseList'])){
							$class = in_array($courses['CoursePurchaseList'][0]['type'],array('0','1','2'))?'added_learning_dsbl':'add_learning';
							echo $this->Html->link(__('add_to_learning_plan'),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($courses['Course']['id']))));
						}else{
							echo $this->Html->link(__('add_to_learning_plan'),'javascript:void(0);',array('class'=>'add_learning','rel'=>base64_encode(convert_uuencode($courses['Course']['id']))));
						}
				
				?>
				
				<?php if(!empty($courses['Subscription_course']))
						{
							
							$option="watch";
							$full_course=1;
						}
						else if(!empty($courses['Subscription_lesson']))
						{
							$full_course=0;
							$option="watch";
						}
						else
						{
							$option="preview";
							$full_course=0;
						}
				?>
							<?php /*		
                                $class ='';	
                                $class = in_array($list['Course']['id'],$cookie_courses)?'vw_scn_link bg_ad_basket added_learning_sub':'vw_scn_link bg_ad add_to_cart';
                                $url = in_array($list['Course']['id'],$cookie_courses)?'javascript:void(0);':array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($list['Course']['id'])));						
                                if(!$full_course)
								{
									echo $this->Html->link(__('Add to shopping basket'),$url,array('class'=>$class));
								}*/
                            ?>
                <?php 
				if(!$subscribed)
				{
				
				echo $this->Html->link(__($option),'javascript:void(0);',array('id'=>base64_encode(convert_uuencode($courses['Course']['id'])),'class'=>'tc_four open_video','rel'=>$option)); 
				
				}
				else
				{
					echo $this->Html->link(__('watch'),'javascript:void(0);',array('id'=>base64_encode(convert_uuencode($courses['Course']['id'])),'class'=>'tc_four open_video','rel'=>'watch')); 
				}
				?>
				
				<?php 			
					//echo $this->Html->link(__('Watch now'),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($courses['Course']['id']))),array('id'=>'tc_three_ctlg'));
				?>               
			</div>
		</div>      
	</div>
<?php } ?>