<?php echo '';?><script type="text/javascript">
	$(document).ready(function(){
	
	
	
	
			$("#countText").bind('paste',function(e){					
				setTimeout(function(e) {  
					var countVal = $("#countText").val().length;
					var text = $("#countText").val();
					var getval = $("#showCount").text();
					var total = 1024 - countVal;
					if(total >= 0){
						$('#showCount').text(total);
					}else{				
						 $("#countText").val($("#countText").val().substring(0, 1024));						
					}
				
				}, 15)
			});
			$("#countText").bind('cut',function(e){					
				e.preventDefault();
			});
			$("#countText").on('keyup',function(e){	
		  		var countVal = $(this).val().length;
				var text = $(this).val();
				var getval = $("#showCount").text();
				var total = 1024 - countVal;
				if(total >= 0){
		  			$('#showCount').text(total);
				}else{					
					 $(this).val($(this).val().substring(0, 1024));
				}
		});
		$("#countText").on('focusout',function(e){			
			 $(this).val($(this).val().substring(0, 1024));
			 var countVal = $(this).val().length;			
			 var total = 1024 - countVal;
			 $('#showCount').text(total);
		});
	});
	
</script>
 <div class="modal fade" id="add_review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            	<h4 class="modal-title" id="myModalLabel"><?php echo __('write_a_review_'); ?></h4>
                            </div>
                            <div class="modal-body">
                              <?php echo  $this->Form->create('CourseRemark',array('id'=>'remarks','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'add_reviews'))); ?>
                                    <p>
                                <?php echo $this->Form->input('course_id',array('type'=>'hidden','div'=>false,'id'=>'h_id','value'=>$course_details['Course']['id'])); ?>
                                <?php echo __("did_this_course_meet_your_expectations?_please_share_your_experience_with_us"); ?>
                                   </p>
                                <div class="form-group">
                                      
                                    <?php echo $this->Form->input('comment',array('type'=>'textarea','div'=>false,'onkeyup'=>'commentcheck(this)','id'=>'countText',"placeholder"=>__("comment"),'class'=>"form-control",'label'=>false)) ?>
                                    <div id="err" class="register_error alert-danger" style="font-size:11px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('anonymous',array('type'=>'checkbox','class'=>"form-control",'div'=>false,'label'=>false)) ?>
                                        <?php echo __('i_wish_to_remain_anonymous'); ?>                                    
                                    </div>    
                                   <div class="form-group">
                                       <p> <?php echo __('characters_remaining'); ?> <span class="showcnt" id="showCount">1024</span></p>
                                    </div>
                                 	  
                                    <span class="Link"><button href="" id="submit_review"> <?php echo __('save_review'); ?> </button></span>
								<?php echo $this->Form->end(); ?>
                            </div>                            
                        </div>
                    </div>
                </div>


