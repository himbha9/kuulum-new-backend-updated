<?php echo '';?><script type="text/javascript">
	$(document).ready(function(){
			$("#edit_countText").bind('paste',function(e){					
				setTimeout(function(e) {  
					var countVal = $("#edit_countText").val().length;
					var text = $("#edit_countText").val();
					var getval = $("#edit_showCount").text();
					var total = 1024 - countVal;
					if(total >= 0){
						$('#edit_showCount').text(total);
					}else{				
						 $("#edit_countText").val($("#edit_countText").val().substring(0, 1024));						
					}
				
				}, 15)
			});
			$("#edit_countText").bind('cut',function(e){					
				e.preventDefault();
			});
			$("#edit_countText").on('keyup',function(e){	
		  		var countVal = $(this).val().length;
				var text = $(this).val();
				var getval = $("#edit_showCount").text();
				var total = 1024 - countVal;
				if(total >= 0){
		  			$('#edit_showCount').text(total);
				}else{					
					 $(this).val($(this).val().substring(0, 1024));
				}
		});
		$("#edit_countText").on('focusout',function(e){			
			 $(this).val($(this).val().substring(0, 1024));
			 var countVal = $(this).val().length;			
			 var total = 1024 - countVal;
			 $('#edit_showCount').text(total);
		});
	});
</script>
<div class="modal fade" id="edit_review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            	<h4 class="modal-title" id="myModalLabel"><?php echo __('edit_a_review'); ?></h4>
                            </div>
                            <div class="modal-body">
                              <?php echo  $this->Form->create('CourseRemark',array('id'=>'edit_remarks','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'edit_reviews'))); ?>
                                    <p>
                                <?php echo $this->Form->input('course_id',array('type'=>'hidden','id'=>'h_id','value'=>$course_details['Course']['id'])); ?>
		<?php echo $this->Form->input('id',array('type'=>'hidden','id'=>'crId','value'=>'')); ?>
                                <?php echo __("did_this_course_meet_your_expectations?_please_share_your_experience_with_us"); ?>
                                   </p>
                                <div class="form-group">
                                      
                                    <?php echo $this->Form->input('comment',array('type'=>'textarea','div'=>false,'id'=>'edit_countText',"placeholder"=>__("comment"),'class'=>"form-control",'label'=>false,'value'=>'')) ?>
                                    <div id="err" class="register_error alert-danger" style="font-size:11px;"></div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('anonymous',array('type'=>'checkbox','class'=>"form-control",'div'=>false,'label'=>false)) ?>
                                        <?php echo __('i_wish_to_remain_anonymous'); ?>                                    
                                    </div>    
                                   <div class="form-group">
                                       <p> <?php echo __('characters_remaining'); ?> </p><p class="showcnt" id="edit_showCount">1024</p>
                                    </div>
                                 	  
                                    <span class="Link"><button href="" id="edit_submit_review"> <?php echo __('save_review'); ?> </button></span>
								<?php echo $this->Form->end(); ?>
                            </div>                            
                        </div>
                    </div>
                </div>



