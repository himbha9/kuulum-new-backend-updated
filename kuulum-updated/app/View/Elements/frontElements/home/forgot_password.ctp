<div id="forgot-window" class="login-popup">
<?php echo $this->Html->link($this->Html->image('dlt.png',array('width'=>20,'height'=>20,'class'=>'btn_close')),'#review-box',array('class'=>'close','title'=>'Close Window','alt'=>'Close','escape'=>false)); ?>
<div class="forgotpassword_con">
	<h2> <?php echo __('Forgotten Password'); ?> </h2>
    <p> 
    	<?php echo __('You will be able to set a new password after following a few simple steps. Please confirm your e-mail address is correct, then select Submit. We will send an e-mail to the provided address. The e-mail will contain a unique link that will bring you back to the training portal.'); ?>
    </p>
    <p> 
    	<?php echo __('When you return, we will ask you to answer a few questions before you can reset your password. We do this for your peace of mind, to ensure this is your account. The safekeeping of your details is very important to us.'); ?>
    </p>
</div>
	
<div class="contaienr_maildd">
	<div class="inner_mailads">
    	<label>
        <?php echo __('e-mail_address');?>:
        </label>
		<div class="input_cont">
        <input type="text" id="forgotemail" />
		<div id="err_reset_email" class="register_error" style="width: 400px;"> <?php if(isset($error['password'][0])) echo $error['password'][0];?> </div>
    	</div>
	</div>
    <div class="submit_forgotpass">
    	<img src="../img/msg.png" class="img_mssgs" />        
        <?php echo $this->Html->link(__('submit'),'javascript:void(0);',array('class'=>'submit_button'));?>
    	<!--<input type="submit" class="sc_btns" value="Submit" />-->
    </div>
</div>
</div>

<style type="text/css">
.input_cont{
	float:left;
	width:80%;
}
.submit_button
{
	background: none repeat scroll 0 0 transparent;
    border: medium none;
    color: #2D66B1;
    cursor: pointer;
    float: left;
    font-family: verdana;
    font-size: 13px;
    font-weight: bold;
    margin: 5px 2px;
    text-align: left;
    text-decoration: none;
    width: auto;
}
</style>