<?php $locale = $this->Session->read('LocTrain.locale');
$courseid=$this->request->params['pass'];
$course_id=convert_uudecode(base64_decode($courseid[0]));

?>

<script>
    function showLesson(c_id,l_id,m_id){
       
        req=0;
        req = $.ajax({
			url:ajax_url+'Members/lesson_detail/'+c_id+'/'+l_id,
			type: 'get',
			
			success: function(resp){
			$('#myModal_lesson_'+m_id).find('.modal-body').html(resp);
			}
		});
    }
	  function showLesson1(c_id,l_id,m_id,less_yn){
       
        req=0;
        req = $.ajax({
			url:ajax_url+'Members/lesson_detail/'+c_id+'/'+l_id+'/'+less_yn,
			type: 'get',
			
			success: function(resp){
			$('#myModal_lesson_'+m_id).find('.modal-body').html(resp);
			$('#myModal_lesson_'+m_id).modal('show');
			}
		});
    }
    $('document').ready(function(){


		<?php if(!empty($less_id)){ ?>

showLesson1("<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",'<?php echo $less_id;?>','<?php echo convert_uudecode(base64_decode($less_id));?>','<?php echo $less_yn;?>');

<?php }?>

	 var arrValuesForOrder =[];
<?php if(isset($course_details['Course']['lessons_order']) && !empty($course_details['Course']['lessons_order'])){
	$order_array=explode(',',$course_details['Course']['lessons_order']);
	foreach($order_array as $row){ ?>
		  arrValuesForOrder.push('<?php echo $row;?>');
	<?php }	 ?>
<?php if(isset($course_details['CourseLesson']) && !empty($course_details['CourseLesson'])){
	
	foreach($course_details['CourseLesson'] as $row){
			if(!in_array($row['id'],$order_array)){
 ?>
		  arrValuesForOrder.push('<?php echo $row['id'];?>');
	<?php } }	} ?>
	 var len=arrValuesForOrder.length;
            var $ul = $(".table-responsive111").find('tbody');
	for (var i =0; i<len; i++) {
                 $ul.append($("#lesson_"+arrValuesForOrder[i]));                 
            }
		var numbering = 1;
		$ul.find('tr').each(function(){
			$(this).find('td.CourseNum').html(numbering+'.');			
			numbering++;
		});
<?php }?>

		
            var  req='';
        checkBoxLength=$("input[type='checkbox'][name='add_lessons']").length;
           if ($("input[type='checkbox'][name='add_lessons']:checked").length == checkBoxLength ){
if($(".checkLessonZero").length== checkBoxLength && $('.Price').attr('id')!='0.00'){
$('#check_lesson_basket').addClass('added_learning_sub');
               $('#check_lesson_basket').hide();
               $('#check_lesson_basket').attr('id','');
return false;
}else{
               $('#check_lesson_basket').addClass('added_learning_sub');
               $('#check_lesson_basket').hide();
               $('#check_lesson_basket').attr('id','');
		$(".SmallLinkBoxWrapper2 .SmallLinkBox .add_to_cart").attr("id","class-check");
		$(".SmallLinkBoxWrapper2 .Link .add_to_cart").attr("id","class-check1");
		$("#class-check").removeClass("add_to_cart");
		$("#class-check").addClass("added_learning_sub");
		$("#class-check1").removeClass("add_to_cart");
		$("#class-check1").addClass("added_learning_sub");
if($('.Price').attr('id')!='0.00' && $('.save_this_course_later').length ==0){
$('.Price').hide();
$('.RatingStar').css('margin-right','-3px');
}

		$("#class-check").unbind("click");
		$("#class-check1").unbind("click");
		$("#class-check").attr("href",'javascript:void(0);');
		$("#class-check1").attr("href",'javascript:void(0);');}
           }
		   //var counter=0;
		   //var counter1=0;
		   $(".vw_scn_link.bg_ad.add_to_cart").click(function(e){      
 			if($(this).attr('disabled')=='disabled')
			{
			   e.preventDefault();    
			}
					$(this).attr('disabled','disabled');
			//counter=counter+1;
			/*if(counter>1){
			$(this).attr("href","javascript:void(0);");
			}*/
			});
		
       $('#check_lesson_basket').on("click",function(event){
		  
		  if($('#check_lesson_basket').attr('disabled')=='disabled')
			{
			   e.preventDefault();    
			}        
		
        var myid=[];
         var chk=true;
	
            if ($("input[type='checkbox'][name='add_lessons']:checked").not(":disabled").length == 0 ){
                <?php if($this->Session->read('LocTrain.login') != '1'){
                
                ?>
                $.ajax({
                url:ajax_url+'Home/access_denied',
                success: function(resp){                   
                      if(resp=='index'){
                        alert('<?php echo __("please_select_at_least_one_lesson");?>');
							   
			//window.location.href="<?php echo HTTP_ROOT ;?>Home/register";  
                      }else{
                    
					 	
			window.location.href=ajax_url+"Home/register"; 
                }
	
                }
                });   
            <?php } else {?>
            alert('<?php echo __("please_select_at_least_one_lesson");?>');
		$('#check_lesson_basket').removeAttr('disabled');
            <?php } ?>
               return false;
             }else{$('#check_lesson_basket').attr('disabled','disabled');
			/*	 
			counter1=counter1+1;
          if(counter1==1)
		   {*/
			
             $("input[type='checkbox'][name='add_lessons']:checked").each(
                function() {           
					if($(this).is(":disabled")==false)
										{
                  if($(this).val() !=''){                     
                      myid.push($(this).val());
                    }
              }
                });
	
		if($("input[type='checkbox'][name='add_lessons']").length ==$("input[type='checkbox'][name='add_lessons']:checked").length ){


				//Check if ant lessons of this is sold or not if sold any one please add the lessons indivaully or add whole course for purchase
				req = $.ajax({
				url:ajax_url+'Home/check_lessons_course/',
				type: 'post',
				data:{"course_id":'<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>'},
				success: function(resp){
						if($.trim(resp)=='1'){
						//one or more lessons are already buy so add indiviually
							req = $.ajax({
				url:ajax_url+'Home/add_lesson_to_cart_ajax/',
				type: 'post',
				data:{"array":myid},
				success: function(resp){
				       
				         if(resp=='index'){
						
				       window.location.href=ajax_url+"Home/register";   
				      }else{
						  var login="<?php echo $this->Session->read('LocTrain.login')?>";
				 if(login==1)
						{	 
							      window.location.href=ajax_url+"Home/purchase";   
						}else
						{
				  $.ajax({
							url:ajax_url+"Home/findcookiepurchase/",
							
							success:function(index){
									var array=$.parseJSON(index);
									var values=[];
										console.log(array);
									$("input[type='checkbox'][name='add_lessons']:checked").each(function(index){
									if($(this).is(":disabled")==false)
										{
										var val=$(this).val();
										values.push(val);
										$(this).prop("disabled",true);
										}
										});
																		
										$("#title-cart").html("");
										$.each(values,function(index1){
											if(values.length==1)
											{
											$.ajax({
										url:ajax_url+"Home/coursetitle/"+values[index1],
										success:function(response){
										$("#title-cart").html("<p>"+response+" was added to your shopping basket. Go to the shopping basket to complete your order.</p>");
										
										}
										});
											}
											else
											{
										$("#title-cart").html("<p>Lessons were added to your shopping basket. Go to the shopping basket to complete your order.</p>");
											}
										
									});									
								
								$(".counter_txt").html(array.countcart);
								$("#register-cart").modal('show');
								
									if($("input[type='checkbox'][name='add_lessons']:checked").length==$("input[type='checkbox'][name='add_lessons']").length)
								{
									$("#check_lesson_basket").addClass("added_learning_sub");
									$("#check_lesson_basket").unbind( event );
									//$("#check_lesson_basket").removeAttr("id");
										//$(".Link.addShopingViewBasket").html("<a href='javascript:void(0);' class='added_learning_sub'><?php echo __('add_selected_to_basket');?></a>");
										$(".SmallLinkBoxWrapper2 .SmallLinkBox .add_to_cart").attr("id","class-check");
								$(".SmallLinkBoxWrapper2 .Link .add_to_cart").attr("id","class-check1");
								$("#class-check").removeClass("add_to_cart");
								$("#class-check").addClass("added_learning_sub");
								$("#class-check1").removeClass("add_to_cart");
								$("#class-check1").addClass("added_learning_sub");
								$("#class-check").unbind("click");
								$("#class-check1").unbind("click");
								$('#check_lesson_basket').removeAttr('disabled');
								
								
									}	
								}
							});
					
						}	
				  }
				}
				});
						}else{
						//add whole lesson
					if($("input[type='checkbox'][name='add_lessons']:checked").not(":disabled").length != $("input[type='checkbox'][name='add_lessons']:checked").not('.checkLessonZero').length){
				req = $.ajax({
				url:ajax_url+'Home/add_lesson_to_cart_ajax/',
				type: 'post',
				data:{"array":myid},
				success: function(resp){
				       
				         if(resp=='index'){
						
				       window.location.href=ajax_url+"Home/register";   
				      }else{
						  var login="<?php echo $this->Session->read('LocTrain.login')?>";
				 if(login==1)
						{	 
							      window.location.href=ajax_url+"Home/purchase";   
						}else
						{
				  $.ajax({
							url:ajax_url+"Home/findcookiepurchase/",
							
							success:function(index){
									var array=$.parseJSON(index);
									var values=[];
										console.log(array);
									$("input[type='checkbox'][name='add_lessons']:checked").each(function(index){
									if($(this).is(":disabled")==false)
										{
										var val=$(this).val();
										values.push(val);
										$(this).prop("disabled",true);
										}
										});
																		
										$("#title-cart").html("");
										$.each(values,function(index1){
											if(values.length==1)
											{
											$.ajax({
										url:ajax_url+"Home/coursetitle/"+values[index1],
										success:function(response){
										$("#title-cart").html("<p>"+response+" was added to your shopping basket. Go to the shopping basket to complete your order.</p>");
										
										}
										});
											}
											else
											{
										$("#title-cart").html("<p>Lessons were added to your shopping basket. Go to the shopping basket to complete your order.</p>");
											}
										
									});									
								
								$(".counter_txt").html(array.countcart);
								$("#register-cart").modal('show');
								
									if($("input[type='checkbox'][name='add_lessons']:checked").length==$("input[type='checkbox'][name='add_lessons']").length)
								{
									$("#check_lesson_basket").addClass("added_learning_sub");
									$("#check_lesson_basket").unbind( event );
									//$("#check_lesson_basket").removeAttr("id");
										//$(".Link.addShopingViewBasket").html("<a href='javascript:void(0);' class='added_learning_sub'><?php echo __('add_selected_to_basket');?></a>");
										$(".SmallLinkBoxWrapper2 .SmallLinkBox .add_to_cart").attr("id","class-check");
								$(".SmallLinkBoxWrapper2 .Link .add_to_cart").attr("id","class-check1");
								$("#class-check").removeClass("add_to_cart");
								$("#class-check").addClass("added_learning_sub");
								$("#class-check1").removeClass("add_to_cart");
								$("#class-check1").addClass("added_learning_sub");
								$("#class-check").unbind("click");
								$("#class-check1").unbind("click");
								$('#check_lesson_basket').removeAttr('disabled');
								
								
									}	
								}
							});
					
						}	
				  }
				}
				});
			}else{
				var login="<?php echo $this->Session->read('LocTrain.login')?>";
			
		if(login==1)
		{
		window.location.href=ajax_url+"Home/add_to_cart/<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>";	
		}else
		{
		        
		        $.ajax({
					url:ajax_url+"Home/add_to_cart/<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
					type: 'post',
					success:function(){
						$.ajax({
							url:ajax_url+"Home/findcookiepurchase/",
							
							success:function(index){
								var array=$.parseJSON(index);
								$("#title-cart").html(array['courseslist'][0]['Course']['title']+" was added to your shopping basket. Go to the shopping basket to complete your order.");
								$(".counter_txt").html(array.countcart);
								$("#register-cart").modal('show');
									$("#check_lesson_basket").addClass("added_learning_sub");
									$("#check_lesson_basket").unbind(event);
								//	$("#check_lesson_basket").removeAttr("id");
									//$(".Link.addShopingViewBasket").html("<a href='javascript:void(0);' class='added_learning_sub'><?php echo __('add_selected_to_basket');?></a>");
									$(".SmallLinkBoxWrapper2 .SmallLinkBox .add_to_cart").attr("id","class-check");
								$(".SmallLinkBoxWrapper2 .Link .add_to_cart").attr("id","class-check1");
								$("#class-check").removeClass("add_to_cart");
								$("#class-check").addClass("added_learning_sub");
								$("#class-check1").removeClass("add_to_cart");
								$("#class-check1").addClass("added_learning_sub");
								$("#class-check").unbind("click");
								$("#class-check1").unbind("click");
							$("input[type='checkbox'][name='add_lessons']:checked").not(":disabled").attr("disabled",true);
								
								}
							});
					
						}
					});
		
		}
			}
						}
					}
				});

		/*	*/
		}else{


			req = $.ajax({
		        url:ajax_url+'Home/add_lesson_to_cart_ajax/',
		        type: 'post',
		        data:{"array":myid},
		        success: function(resp){
				
				var login="<?php echo $this->Session->read('LocTrain.login')?>";

		                //$(this).attr('disabled',false);
						
						//Commented on 7th jan 2014   
		                // if(resp=='index'){
						
						if(login==1)
						{	 
							      window.location.href=ajax_url+"Home/purchase";   
						}
						else
						{
						
						$.ajax({
							url:ajax_url+"Home/findcookiepurchase/",
							
							success:function(index){
								var array=$.parseJSON(index);
								var values=[];
									$("input[type='checkbox'][name='add_lessons']:checked").each(function(index){
									if($(this).is(":disabled")==false)
										{
										var val=$(this).val();
											values.push(val);
											$(this).attr("disabled",true);
										}
										});
									
											$("#title-cart").html("");
										$.each(values,function(index1){
											
											if(values.length==1)
											{
												$.ajax({
										url:ajax_url+"Home/coursetitle/"+values[index1],
									
										
										success:function(response){
											
										$("#title-cart").html("<p>"+response+" was added to your shopping basket. Go to the shopping basket to complete your order.</p>");
										
											
											}
										});	
											}
											else
											{											
											
										    $("#title-cart").html("<p>Lessons were added to your shopping basket. Go to the shopping basket to complete your order.</p>");
										
											}
									
									});
							
									
										
								
								$(".counter_txt").html(array.countcart);
								$("#register-cart").modal('show');
								
								$('#check_lesson_basket').removeAttr('disabled');
								}
							});
							
						}	          

				  
				}
		        });
		}
                
            

       
	   }
	    });
	    

    });
   $(document).on('hide.bs.modal',"div[id*='myModal_lesson_']",function() {
var str=' <div class="loading" style="margin-left: -12px"><div class="loading_inner"><?php echo $this->Html->image("front/ajax-loader.gif",array("width"=>"197","height"=>"22","class"=>"loading_image")); ?></div></div>';
	 $('.VideoLessonBox_'+$(this).attr("data-data")).html(str);

});
    
      $(document).on('show.bs.modal',"div[id*='myModal_lesson_']",function() {
//alert($('#render_video img.img-responsive').length);
	if($('#render_video img.img-responsive').length==0){
	 jwplayer('render_video').stop(); jwplayer().stop(); 	
	}
  });
</script>
<div class="table-responsive111">
                <table class="table no-border">
                  <tbody>
                      <?php 
                      if(isset($course_details['CourseLesson']) && !empty($course_details['CourseLesson'])){$i=1;
                          foreach($course_details['CourseLesson'] as  $lesson){ 
                       //   echo '<pre>';print_r($lesson);
                            $video='';
                            $c_id = base64_encode(convert_uuencode($lesson['course_id']));
                            $l_id = base64_encode(convert_uuencode($lesson['id']));
                            ?>
                            <tr id="lesson_<?php echo $lesson['id']; ?>">
                            <td class="CourseNum" width="2%"><?php echo $i;?>.</td>
<?php if(!empty($course_details)){ 
                                    $course_m_id=$course_details['Course']['m_id'];
                                    $course_id=base64_encode(convert_uuencode($course_details['Course']['id']));
                                   
                                    $cookie_lessons = array();
                                    $course_in_cart=0;
                                    if(!empty($getCoursesindex)){

                                            foreach($getCoursesindex as $key=>$val){
                                                    if(strlen($key)>8)
                                                    {
                                                            $cookie_lessons[] = convert_uudecode(base64_decode($val));
                                                    }
                                                    else
                                                    {
                                                            if($course_id==$val)
                                                            {
                                                                    $course_in_cart=1;
                                                            }
                                                    }
                                            }
                                    }
                                   /* $less_subscription= $this->Utility->getLessonSubscription($m_id,$course_details['Course']['id'],$lesson['id']);*/ $less_subscription=array();
foreach($course_details['CoursePurchaseList'] as $list){
if(in_array($course_details['Course']['id'],$list) && (in_array($lesson['id'],$list) || $list['lesson_id']==''||$list['lesson_id']=='NULL' ))
$less_subscription[]=$list;
}

                                    $less_val="";
                                    if($course_m_id==$m_id)
                                    {
                                            $my_course=1;
                                    }
                                    else
                                    {
                                            $my_course=0;
                                    }
                                    if($my_course){
                                        $less_val='';
                                    }else{
                                        if(!$less_subscribed)
                                        {
                                               
                                            $url = (in_array($lesson['id'],$cookie_lessons)|| $course_in_cart==1)?'javascript:void(0);':array('controller'=>'Home','action'=>'add_lesson_to_cart',base64_encode(convert_uuencode($lesson['id'])));
                                            $less_val= (in_array($lesson['id'],$cookie_lessons)|| $course_in_cart==1)?'':base64_encode(convert_uuencode($lesson['id']));
                                            if(!empty($less_subscription) || trim($lesson['price'])==0.00)
                                            {

                                                    $url='';
                                                    $less_val='';
                                            }
									
                                            //echo $this->Html->link(__('add_to_shopping_basket'),$url,array('class'=>$class));
                                        }
                                    }
                                }

 ?>


<?php if( $this->Session->read('LocTrain.id')){
if($lesson['price']=='0.00'){
$class = !empty($course_details['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
		if($class != 'c_added_learning_dsbl'){ ?>
		<td class="CourseCheckbox" width="2%" >				                
				                <input type="checkbox" rel="<?php echo $lesson['price'];?>" name="add_lessons" class="checkLesson <?php if(empty($less_val)){echo 'checkLessonZero';}?>" value="<?php echo $less_val;?>" <?php if(empty($less_val)){echo 'checked="checked"';}?>  <?php if(empty($less_val)){echo 'disabled';}?> >
								
				            </td>
		<?php }else{
		
		}

}else{
		if(empty($less_val)){?>
				           
		<?php } else{ ?>
		 <td class="CourseCheckbox" width="2%" >
				                
				                
				                <input type="checkbox" rel="<?php echo $lesson['price'];?>" name="add_lessons" class="checkLesson" value="<?php echo $less_val;?>" <?php if(empty($less_val)){echo 'checked="checked"';}?>  <?php if(empty($less_val)){echo 'disabled';}?> >
								
				            </td>
		<?php }
	}

 }else{?>
 <td class="CourseCheckbox" width="2%" >
                                
                                
                                <input type="checkbox" rel="<?php echo $lesson['price'];?>" name="add_lessons" class="checkLesson" value="<?php echo $less_val;?>" <?php if(empty($less_val)){echo 'checked="checked"';}?>  <?php if(empty($less_val)){echo 'disabled';}?> >
								
                            </td>
<?php }?>
<?php
$colspan='';
if($this->Session->read('LocTrain.id')){
 if($lesson['price']=='0.00'){
	$class = !empty($course_details['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
		if($class != 'c_added_learning_dsbl'){
		$colspan='';
		}else{
		$colspan='2';
		}
 }else{
	if(empty($less_val)){
		$colspan='2';
	}
  }
}

?>
                            <td colspan="<?php echo $colspan;?>"  width="54%"><a href="javascript:;" data-toggle="modal" data-target="#myModal_lesson_<?php echo $lesson['id'];?>" onclick="showLesson('<?php echo $c_id;?>','<?php echo $l_id;?>','<?php echo $lesson['id'];?>');">

                                <?php 
                               
                                //echo $this->Html->link($lesson['title'],'javascript:void(0)');
                                //echo $this->Html->link(wordwrap($lesson['title'],25,"\n",TRUE),'/Members/lesson_detail/'.$c_id.'/'.$l_id);
                                echo $lesson['title'];
                                ?>
</a>
                            </td>
                            <td class="CoursePrice"  width="30%" align="right"><?php 

	if($lesson['price']=='0.00'){
        $class = !empty($course_details['CoursePurchaseList']) ? 'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
		if($class != 'c_added_learning_dsbl'){
		echo __('free');
		}
		else
		{
		echo __('purchased');
		}
		
	}else{
		if(empty($less_val)){
		//if less_val is empty->check the lesson is in cart or sold
			//print_r($cookie_lessons);
		if(in_array($lesson['id'],$cookie_lessons) ||  $course_in_cart==1){
			$pur_less_id=array();
			foreach($course_details['CoursePurchaseList'] as $list){
				$pur_less_id[]=$list['lesson_id'];
			}
			if(!in_array($lesson['id'],$pur_less_id)){			
			echo __('in_shopping_basket');
			}else{
			echo __('purchased');}
			
		}else{
			echo __('purchased');}
		}else{
			echo $this->Utility->externlizePrice($language,$locale,$lesson['price']);
		}
	}	


?></td>
                            <td  width="10%" align="right">  <?php if($lesson['mins'] > 0 || $lesson['secs'] > 0) { ?>                                           
                                                    <?php //echo  ($lesson['mins'] != NULL && $lesson['mins'] > 0)?$lesson['mins']." ".__('minutes'):' ';?>
                                                    <?php //echo  ($lesson['secs'] != NULL && $lesson['secs'] > 0)?$lesson['secs']." ".__('seconds'):'00 '.__('seconds');?>

                                                    <?php /*echo  ($lesson['mins'] != NULL && $lesson['mins'] > 1 || $lesson['mins'] == 0)?$lesson['mins']." ".__('minutes'):$lesson['mins']." ".__('minute');
                                                    echo  ($lesson['secs'] != NULL && $lesson['secs'] > 1 || $lesson['secs'] == 0)?$lesson['secs']." ".__('seconds'):$lesson['secs']." ".__('second');*/?>
                                                <?php $secs='00';
                                             if($lesson['mins']== null || $lesson['mins']==0){
                                                    $mins='00';
                                                }
                                                else if($lesson['mins'] !=null && ($lesson['mins'] >0 && $lesson['mins'] <10)){
                                                    $mins='0'.$lesson['mins'];
                                                }else{
                                                    $mins=$lesson['mins'];
                                                }

                                                if($lesson['secs'] !=null && ($lesson['secs'] >0 && $lesson['secs'] <10)){
                                                    $secs='0'.$lesson['secs'];
                                                }else if($lesson['secs'] ==0 || $lesson['secs']== null ){
                                                   $secs='00';
                                                }else{
                                                   $secs=$lesson['secs'];
                                                }
                                                echo trim($mins).':'.trim($secs);
                                                
                            }else { echo __('duration_unknown'); } ?></td>
                            <td class="CourseInfo"  width="2%" style="padding-right:0px;"><a href="javascript:;" data-toggle="modal" data-target="#myModal_lesson_<?php echo $lesson['id'];?>" onclick="showLesson('<?php echo $c_id;?>','<?php echo $l_id;?>','<?php echo $lesson['id'];?>');">
                                    <?php echo $this->Html->image('front/icons/icon_info.png',array('width'=>'25','height'=>'25',"alt"=>''));?></a>
                     
                      	
                            <!-- Modal -->
                            <div class="modal fade" data-data="<?php echo $lesson['id'];?>" id="myModal_lesson_<?php echo $lesson['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog model-lesson">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><?php echo $lesson['title'];?></h4>
                                    </div>
                                    <div class="modal-body">
                                    	  <div class="loading" style="margin-left: -12px">
                                                <div class="loading_inner">
                                                    <?php echo $this->Html->image('front/ajax-loader.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                                                </div>
                                            </div>
                                        
                                    </div>                            
                                </div>
                            </div>
                        </div>
                       </td>
                          </tr>
                            
                         <?php $i++;}                          
                      }else{ ?>
                          <tr>
                          <td colspan="6"><?php echo __('no_lesson_found'); ?></td>
                            </tr>
                    <?php  }
                     ?>                    
                  </tbody>
                </table>            
			</div>
			
			
<div class="modal fade" id="register-cart"" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <div class="modal-body">    
                       <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<p id="title-cart"></p>
		
		 <!--<span class="Link"><a onclick="window.location.href='<?php echo HTTP_ROOT ;?>Home/register'" href="javascript:void(0);"><?php echo __('yes'); ?></a>&nbsp;&nbsp;<a  data-dismiss="modal"><?php echo __('no'); ?></a></span>
	-->
                           
                       
                    </div>                            
                </div>
            </div>
        </div>
<?php 
       if(isset($course_details['CourseLesson']) && !empty($course_details['CourseLesson']) && $course_details['Course']['price'] !='0.00'){ ?>
    <div class="Link addShopingViewBasket"><a href="javascript:void(0);" class="" id="check_lesson_basket"><?php echo __('add_selected_to_basket');?></a></div>
    <!--<div class="Link"><a href="javascript:;"><?php echo __('add_selected_to_wish_list');?></a></div>-->
<?php } ?>

