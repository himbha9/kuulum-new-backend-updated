<?php echo ''; ?>

<script type="text/javascript">
function changeTooltipColorTo(color,border_c) {
    $('.tooltip-inner').css('background', border_c);
	$('.tooltip-inner').css('color', color);
	//$('.tooltip-inner').css('border','2px solid #000');
	$('.tooltip-inner').css({'border-radius':'50%'});
 	// $('.tooltip-arrow').css({'z-index':'9999','position':'absolute','bottom':'2px'});
    $('.tooltip.top .tooltip-arrow').css('border-top-color', border_c);
    $('.tooltip.right .tooltip-arrow').css('border-right-color', border_c);
    $('.tooltip.left .tooltip-arrow').css('border-left-color', border_c);
    $('.tooltip.bottom .tooltip-arrow').css('border-bottom-color', border_c);
}

$(function () { $("[data-toggle='tooltip']").tooltip(); });
$('body').on('shown.bs.tooltip', function () {
 // changeTooltipColorTo('#fff','#000');
});
	<?php if(!empty($news_list)){ ?>
		$(".tchng_cont_content").attr('id','<?php echo $last_id;?>');
	<?php }else { ?>
		$(".tchng_cont_content").attr('id','0');
	<?php } ?>	
</script>
<?php  foreach($news_list as $news) { ?>
    	<div class="filter_news">
        <div class="col-lg-3 col-md-3 col-sm-3 filter_img">
            <?php 
			$info = new SplFileInfo($this->Utility->getAWSImgUrl(NEWS_BUCKET,'thumbnails/'.$news['News']['image']));
				$info1 = new SplFileInfo($this->Utility->getAWSImgUrl(NEWS_BUCKET,'original/'.$news['News']['image']));

            if($news['News']['image'] != '' && $info->getExtension()) { 
                    echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'thumbnails/'.$news['News']['image']),array('alt'=>$news['News']['image'],"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$news['News']['id']));
                }else if($news['News']['image'] != '' && $info1->getExtension()){
                   echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'original/'.$news['News']['image']),array('alt'=>$news['News']['image'],"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$news['News']['id']));
		 }else{
                    echo $this->Html->image('front/no_img_small.png',array('alt'=>'no_img_small.png',"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$news['News']['id']));
                }
             ?>
        </div> 
         <div class="col-lg-9 col-md-9 col-sm-9 filter_mob">
         <div class="filter_right">
<?php      
	$date_news=$this->Timezone->dateFormatAccoringLocaleLangNew($this->Session->read('Config.language'),$news['News']['date_added']);                           
                    $date_month=$this->Timezone->dateFormatAccoringLocaleLangNewsMonth($this->Session->read('Config.language'),$news['News']['date_added']);
                    $date_day=date('d',$news['News']['date_added']);
                    ?>
         <div class="date_month" data-toggle="tooltip" data-placement="right" title="<?php echo $date_news;?>">
              
       <div class="date" > <?php echo $date_day;?></div><?php echo $date_month;?></div>
         <?php $link_text = $this->Text->truncate(strip_tags($news['News']['title_'.$locale]),100,array( 'ellipsis' => '...','exact' => false,'html' => false)); ?>
             <h5> <a data-toggle="modal" data-target="#news_<?php echo $news['News']['id']; ?>"><?php echo $link_text;?></a></h5>
        <div class="clearfix"></div>
          <p> <?php echo $this->Text->truncate(strip_tags($news['News']['description_'.$locale]),250,array( 'ellipsis' => '...','exact' => false,'html' => false)); ?> </p>
         
         </div>
    </div>
    </div>     
<!-- model -->
            <div class="modal fade newsPopupWidth" id="news_<?php echo $news['News']['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                     <div class="date_month" data-toggle="tooltip" data-placement="top" title="<?php echo $date_news;?>">                    
                                        <div class="date"> <?php echo $date_day; ?></div><?php echo $date_month ?> </div>
                                    <h4 class="modal-title" id="myModalLabel">                                       
                                        <?php echo $link_text = strip_tags($news['News']['title_'.$locale]); ?></h4>  <div class="clearfix"></div>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12 rawmodal">
                                            <div class="embed-responsive embed-responsive-16by9 pull-left NewsArticleImg">
                                                        <?php 
                                                        
                                      
                                                        
                                                      if($news['News']['image'] && $info->getExtension()) { 
                                                             echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'thumbnails/'.$news['News']['image']),array('alt'=>$news['News']['image'],'width'=>262,'height'=>'138','class'=>'img-responsive'));
                                                          }else if($news['News']['image'] != '' && $info1->getExtension()){
                                                             echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'original/'.$news['News']['image']),array('alt'=>$news['News']['image'],'width'=>262,'height'=>'138','class'=>'img-responsive'));

                                                         }else{
                                                             echo $this->Html->image('front/no_img_small.png',array('alt'=>'no_img_small.png','width'=>262,'height'=>'138','class'=>'img-responsive'));
                                                         }
                                                         ?>
                                                </div>
                                           
                                             <div class="Title"> <?php echo $news['News']['sub_title_'.$locale];?></div>
                                              <p> <?php echo $news['News']['description_'.$locale];?></p>
                                        </div>
                                        </div>
                                    
                                    <div>
                                       
                                       
                                    </div>
                                    
                                </div>                            
                            </div>
                        </div>
            </div>
<?php } ?> 
