<?php  $locale = $this->Session->read('LocTrain.locale'); ?>

 <div class="row">
    	<div class="col-lg-12  col-md-12 col-sm-12">
            <div class="table-responsive111">
                <table class="table no-border table-striped111">
                  <tbody>
                      <?php $total_price=0; $i=1;foreach($course_list as $key=>$courses) {
                           	$video = '';$extension='';$image='';
								 
								if(strlen($key)>8)
								{	//calculating the price for lessons			
									if(!empty($courses['CourseLesson']['video']))
									{
										if(in_array($courses['CourseLesson']['extension'],array('zip','swf')) && !empty($courses['CourseLesson']['image']))
										{
											$video=$courses['CourseLesson']['video'];
											$image = $courses['CourseLesson']['image'];
										}else{
											$video=$courses['CourseLesson']['video'];
										}
									}
									$folder=$courses['Course']['m_id'];
									$remove_id=$courses['CourseLesson']['id'];
									$course_of_lesson=$courses['CourseLesson']['course_id'];
									$title=$courses['CourseLesson']['title'];
									$price=$courses['CourseLesson']['price'];
									$total_price+=$price;
									//$url='/Members/lesson_detail/'.base64_encode(convert_uuencode($course_of_lesson)).'/'.base64_encode(convert_uuencode($remove_id));
 $url='/Home/view_course_details/'.base64_encode(convert_uuencode($courses['Course']['id']));
								}else{//calculating the price for courses		
								
									  if(!empty($courses['CourseLesson'])){
										  foreach($courses['CourseLesson'] as $lesson){
											  if($lesson['video'] != ''){
												  $video = $lesson['video'];
												  $extension=$lesson['extension'];
												  $image = $lesson['image'];
												  //pr($video); die;
												  break;
											  }
										  }
									  }
									  $folder=$courses['Course']['m_id'];
									  $remove_id=$courses['Course']['id'];
									  $title=$courses['Course']['title'];
									  $minus_price=0;
									  if(!empty($courses['CoursePurchaseList'])){// If previously purchased a lesson of a course,then  minus the cost  have already paid.
										  
										  foreach($courses['CoursePurchaseList'] as $list){
											  if($list['lesson_id']){
										//  $key=array_search($list['lesson_id'], $courses['CourseLesson']);
									foreach($courses['CourseLesson'] as $k=>$les){	
if($les['id'] ==$list['lesson_id']){ $key=$k;$minus_price+=$courses['CourseLesson'][$key]['price'];}}										
													 
											  }
										  }
									  }

									  $price=$courses['Course']['price']-$minus_price;
									  $total_price += $price;
									  $url='/Home/view_course_details/'.base64_encode(convert_uuencode($remove_id));
								}
                            $class = !empty($courses['CoursePurchaseList'])?'added_learning_dsbl':'add_learning';
   $les_id= !empty($courses['CoursePurchaseList'])?'':base64_encode(convert_uuencode($courses['Course']['id']));		
  //$les_id= base64_encode(convert_uuencode($remove_id));
                          ?>
                    <tr>
                      <td width="2%"><?php echo $i;?>.</td>
                      <td width="2%" style="padding-left: 5px;" class="CourseCheckbox">
                      <input type="checkbox" title="<?php echo $title;?>" name="chklist" data-id="<?php echo $les_id; ?>" id="<?php echo $courses['Course']['pro_id'];?>" rel="<?php echo base64_encode(convert_uuencode($remove_id));?>" class="delte_conted <?php echo $class;?>">
                      </td>
                      <td><?php echo $this->Html->link(wordwrap($title,100,"\n",TRUE),$url,array('rel'=>base64_encode(convert_uuencode($remove_id)),'class'=>'course_link add_learning_discount','escape'=>false)); ?> </td>
                      <td width="14%" class="CoursePrice actualprice" data-val="<?php echo $price;?>" align="right" ><?php  echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$price);  ?></td>
                  
                    </tr>
                      <?php $i++;} ?>
                  </tbody>
                </table>            
			</div>
            
            
            
            
            
            
        </div>
    </div> 
    
    
    <div class="row">
    
<div class="col-lg-6 col-md-6 col-sm-6 col-sm-push-6 shoping_pice_sec ">
               <div class="table-responsive111">
                <table class="table no-border table-striped111">
                  <tbody>
                    <tr>
                      <td><?php echo __('sub_total');?></td>
                      <td class="CoursePrice sub_total" width="29%" id="<?php echo $total_price; ?>"><?php 
                      $tot_pr= $total_price;                      
                      echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,$tot_pr); 
                      ?></td>
                    </tr>
                    
                    <tr>
                      <td><a  data-toggle="modal" class="disable_apply_code" data-target="#shopping_apply_discount"><?php echo __('apply_discount');?></a></td>
                      <td class="CoursePrice discount_amt">-<?php echo $this->Utility->externlizePriceWithoutCurrencySign($language,$locale,0.00); ?></td>
                    </tr>
                    
                    <tr>
                      <td><?php echo __('total_to_pay');?></td>
                      <td class="CoursePrice ryt_contcoursget"  id="<?php echo $total_price; ?>"> <?php echo $this->Utility->externlizePrice($language,$locale,$total_price); ?></td>
                    </tr>
                    
                     <tr>
                      <td colspan="2" class="CoursePrice paypalimg">
                          	<?php if(!empty($course_list))
						{
				?>
                                  
					<?php if($locale =='ko' || $locale =='zh' || $locale =='ar'  )
					{?>	
					 <?php $image='https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif';    ?>
                    <!--   <a id='submit' href="javascript:void(0)" > <?php echo $this->Html->image($image,array('class'=>'sc_btns_img', 'src'=>$image,'alt'=>'PayPal - The safer, easier way to pay online!')); ?> </a> -->
 <a id='submit' href="javascript:void(0)" > <?php echo $this->Html->image('front/paypal_img.png',array('class'=>'sc_btns_img','alt'=>'PayPal - The safer, easier way to pay online!')); ?> </a> 
				<?php } else {?>
							 <?php $image='https://www.sandbox.paypal.com/'.$language_folder.'/i/btn/btn_buynowCC_LG.gif';    ?>
                     <!--  <a id='submit' href="javascript:void(0)" > <?php echo $this->Html->image($image,array('class'=>'sc_btns_img', 'src'=>$image,'alt'=>'PayPal - The safer, easier way to pay online!')); ?> </a> -->
				  <a id='submit' href="javascript:void(0)" > <?php echo $this->Html->image('front/paypal_img.png',array('class'=>'sc_btns_img','alt'=>'PayPal - The safer, easier way to pay online!')); ?> </a>
				<?php } ?>
             <?php  } 
				?>
<?php   echo  $this->Html->link(__('add_to_learning_plan'),'javascript:void(0);',array('class'=>'add_learning_plan','style'=>'display:none;')); ?>

                      </td>
                    </tr>
                    </table>
                    </div>
            
        </div>
    
     <div class="col-lg-6 col-md-6 col-sm-6 col-sm-pull-6 shopingbasketLinks">
            <div class="Link"><a class="remove_delte_conted"><?php echo __('remove_selected_from_basket');?></a></div>
            <div class="Link"><a class="move_to_learning_plan"><?php echo __('move_selected_to_wish_list');?></a></div>
            <div class="Link"><?php echo $this->Html->link(__('continue_shopping_'),array('controller'=>'Home','action'=>'catalogue_landing'),array('class'=>'sc_btns disRegisBut','div'=>false)); ?></div>
            <div class="Link"><?php echo $this->Html->link(__('about_refunds'),array('controller'=>'Home','action'=>'about_refund'),array('class'=>'sc_btns disRegisBut','div'=>false)); ?></div>
            
        </div>  
        
                       
    
    
    </div>
    
    
    <div class="row">
    
    <div class="col-lg-12 visa_img">
    <?php echo $this->Html->image('front/visa_img.png',array('alt'=>'Visa'));?>
    </div>
    
    
    </div>


<!-- Modal -->
    <div class="modal fade" id="shopping_apply_discount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo __('enter_discount_code');?></h4>
                </div>
                <div class="modal-body aply_dscnt_otr" rel="" id="">
                        <div class="form-group "  >
                            <input placeholder="<?php echo __('enter_discount_code');?>" class="form-control TextDiscount aply_dscnt_ipt"  type="text"></input>
                            <div id="err_code" class="register_error alert-danger"></div>
                        </div>
<input type="hidden" id="discount_val"><input type="hidden" id="discount_amt">
                        <span class="Link"><button type="submit" class="btn btn-default link aply_dscnt_sbmt"><?php echo __('validate_discount_code'); ?></button></span>   
                        <div id="shop_condition" style="display:none;"><p><?php echo __('this_code_entitles_you_to_a_discount_of');?><span id="discount_percent"></span></p>
                        <span class="Link text-right"> <button type='submit' class="btn btn-default link aply_dscnt_sbmt_step2"><?php echo $this->Html->image('front/icons_contrast/icon_tag_sales.png',array('class'=>'','alt'=>'')); ?><br><?php echo __('apply_discount');?></button></span>
</div>
                 
                </div>                            
            </div>
        </div>
    </div>
    
