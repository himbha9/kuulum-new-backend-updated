<script>
jwplayer("render_video").setup({
	 playlist: [
		<?php foreach($video['CourseLesson'] as $key=>$video_lesson) { ?>
			{
				
				file: "<?php echo HTTP_ROOT.'files/members/'.$video['Course']['m_id'].'/'.$video_lesson['video'].'.'.$video_lesson['extension']; ?>",
				image: "<?php echo HTTP_ROOT.'files/members/'.$video['Course']['m_id'].'/'.$video_lesson['video'].'_thumb.png'; ?>",
				title: "<?php echo $video_lesson['title']; ?>"
			} <?php if($key < count($video['CourseLesson'])-1){echo ',';}?>
		<?php } ?>     
	 ],
	 listbar: {
		position: 'right',
		size: 300
	}                
});
 
</script>
<div id="render_video" style="width:1024px;height:100%;" >
                
</div>