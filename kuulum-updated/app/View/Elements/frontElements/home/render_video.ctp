<?php $videoCdnURL = 'https://'.$this->Utility->bucketsCloudFront(LESSON_VIDEOS_BUCKET); ?>
<script>
jwplayer("render_video").setup({
	 playlist: [
		<?php foreach($new['CourseLesson'] as $key=>$video_lesson) {
			
			if(trim($video_lesson['purchased'])=='_preview')
			{
				$access = '_preview.';
			}
			else
			{
				$access = '.';
			}
			
		
		
		?>
			{
				
				file: "<?php echo $videoCdnURL.'/'.$new['Course']['m_id'].'/'.$video_lesson['video'].$access.$video_lesson['extension']; ?>",
				image: "<?php echo $videoCdnURL.'/'.$new['Course']['m_id'].'/'.$video_lesson['video'].'_thumb.png'; ?>",
				title: "<?php echo $video_lesson['title']; ?>"
			} <?php if($key < count($new['CourseLesson'])-1){echo ',';}?>
		<?php } ?>     
	 ],
	 listbar: {
		position: 'right',
		size: 300
	}                
});
 
</script>
<div id="render_video" style="width:1024px;height:100%;" >
                
</div>
