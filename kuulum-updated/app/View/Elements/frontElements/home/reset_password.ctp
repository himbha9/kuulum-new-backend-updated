<script type="text/javascript">
$(document).ready(function(){

	var array = {'old_pass':'<?php echo __("what_is_your_current_password");?>','new_password':'<div class="password_instructions"><ul class="instruction_ul"><li style="color:#A5A5A5;"><span class="length invalid"><?php echo __("a_minimum_of_8_and_maximum_of_20_characters");?></span></li><li style="color:#A5A5A5;"><span class="upperCase invalid"><?php echo __("at_least_one_upper_case_character");?></span></li><li style="color:#A5A5A5;"><span class="lowerCase invalid"><?php echo __("at_least_one_lower_case_character");?></span></li><li style="color:#A5A5A5;"><span class="number invalid"><?php echo __("at_least_one_number");?></span></li><li style="color:#A5A5A5;"><span class="alpha invalid"><?php echo __("at_least_one_non_alphanumeric_character");?></sapn></li></ul></div>','confirmPassword':'<?php echo __("please_type_your_password_again_to_confirm");?>'};
	$(':input').on('focus',function(){
		var getId = $(this).attr('id');
		switch(getId)
		{
			case 'old_pass':				
				$(".main_strngth").html(array['old_pass']);
			break;
			case 'new_password':
				$(".main_strngth").html(array['new_password']);
				checkVal();
			break;
			case 'confirmPassword':				
				$(".main_strngth").html(array['confirmPassword']);
				
			break;
			
				
		}	
			
	});
	
	$('#new_password').on('keyup',function(){ checkVal();	});	
	
	$('#old_pass').on('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#old_pass').val()	
	
		if(inputValPass == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_old_password").text(msg);
		}
		else {
			$("#err_old_password").text('');
		}
		  
	});
	$('#new_password').on('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#new_password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		if(inputValPass == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_new_password").text(msg);
		}
		else {
			$("#err_new_password").text('');
		}
		if((inputValPass != '' && inputValConPass != '') && (inputValPass != inputValConPass)){
			msg = '<?php echo __(PASSWORD_MISTACHMATCH);?>';
			$("#err_confirm_password").text(msg);
		}
		else {
			$("#err_confirm_password").text('');
		}
		  
		  
	});
	$('#confirmPassword').on('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#new_password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		  if(inputValConPass == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_confirm_password").text(msg);
		  }
		 else if((inputValPass != '') && (inputValPass != inputValConPass)){
			  msg = '<?php echo __(PASSWORD_MISTACHMATCH);?>';
			  $("#err_confirm_password").text(msg);
		  }
		  else {
			$("#err_confirm_password").text('');
		  }
	});

function checkVal()
{
	if($('#new_password').val().length < 8 || $('#new_password').val().length > 20){
	$(".length").removeClass("valid").addClass('invalid');
	}else{
		$(".length").removeClass("invalid").addClass('valid');
	}
	
	if($('#new_password').val().match(/[A-Z]/)){
		$(".upperCase").removeClass("invalid").addClass('valid');
	}else{
		$(".upperCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[a-z]/)){
		$(".lowerCase").removeClass("invalid").addClass('valid');
	}else{
		$(".lowerCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[0-9]/)){
		$(".number").removeClass("invalid").addClass('valid');
	}else{
		$(".number").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[!@?#$%^&*()\-_=+{};:,<.>~]/)){
		$(".alpha").removeClass("invalid").addClass('valid');
	}else{
		$(".alpha").removeClass("valid").addClass('invalid');
	}
}
});
</script>

<div id="reset-password-box" class="login-popup new_popups">
<?php echo $this->Html->link($this->Html->image('dlt.png',array('width'=>20,'height'=>20,'class'=>'btn_close')),'#reset-password-box',array('class'=>'close','title'=>'Close Window','alt'=>'Close','escape'=>false)); ?>
<div class="forgotpassword_con">
	<h2><?php echo __('reset_password'); ?> </h2>
    <p> 
    	<?php echo __('enter_your_current_password_then_choose_a_new_one'); ?>
    </p>
</div>	
<div class="contaienr_maildd">
	<?php echo $this->Form->create('change_password',array('id'=>'resetpassword','div'=>false,'method'=>'post','url'=>array('controller'=>'Members','action'=>'change_password')));?>
	<div class="main_lftpswrd">
    	<div class="inner_lftpswrd">
        	<label> 
            	<?php echo __('current_password'); ?>
            </label>
           <div class="box_cont">
			   <?php echo $this->Form->input('old_password',array('type'=>'password','id'=>'old_pass','class'=>'passrd_fld','label'=>false,'div'=>false)); ?> 
               <div id="err_old_password" class="register_error"> <?php if(isset($error['old_password'][0])) echo $error['old_password'][0];?> </div>
           </div>
        </div>
        <div class="inner_lftpswrd">
        	<label> 
            	<?php echo __('new_password'); ?>
            </label>
           <div class="box_cont">
			   <?php echo $this->Form->input('new_password',array('type'=>'password','id'=>'new_password','class'=>'passrd_fld','label'=>false,'div'=>false)); ?> 
               <div id="err_new_password" class="register_error"> <?php if(isset($error['new_password'][0])) echo $error['new_password'][0];?> </div>
           </div>
        </div>
        <div class="inner_lftpswrd">
        	<label> 
            	<?php echo __('confirm_new_password'); ?>
            </label>
           <div class="box_cont">
			   <?php echo $this->Form->input('confirm_password',array('type'=>'password','id'=>'confirmPassword','class'=>'passrd_fld','label'=>false,'div'=>false)); ?> 
               <div id="err_confirm_password" class="register_error"> <?php if(isset($error['confirm_password'][0])) echo $error['confirm_password'][0];?> </div>
           </div>
        </div>
    </div>
    <div class="main_rytpswrd">
    	<div class="main_strngth">   </div>         
    </div>
	 <div class="reset_strngth">
        <input type="submit" value="<?php echo __('reset'); ?>" onclick="return ajax_form('resetpassword','Members/validate_changePassword_ajax','load_top','disRegisBut')" class="submit_button disRegisBut">
     </div>
    <?php echo $this->Form->end(); ?>
</div>
</div>

<style type="text/css">
.submit_button
{
	background: none repeat scroll 0 0 transparent;
    border: medium none;
    color: #2D66B1;
    cursor: pointer;
	<?php if($this->Session->read('LocTrain.locale')=='ar'){ ?>
    float: left;
	<?php } else { ?>
	float: right;
	<?php } ?>
    font-size: 14pt;
    font-weight: normal;
    margin: 5px 0;
    text-align: left;
    text-decoration: none;
    width: auto;
}
.box_cont
{
	float:left;
	width:54%;
}
</style>