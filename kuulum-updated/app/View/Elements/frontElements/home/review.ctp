<script type="text/javascript">
	
	$(function(){
		
		$('.show_full_review').click(function(){
				
				var rel = $.trim($(this).attr('rel'));
				
				if(rel == 'show_full')
				{
					$(this).attr('rel','show_less');
					$(this).html('<?php echo __('show_less'); ?>');
					$(this).parents('.rd_rv_data_btm').prev().show().prev().hide();
					
				} else {
					
					$(this).attr('rel','show_full');
					$(this).html('<?php echo __('show_more'); ?>');
					$(this).parents('.rd_rv_data_btm').prev().hide().prev().show();
				}
				
		});
		
		$('.report_review').live('click',function(){
				
				$('.report_review').parent().show();
				$('.rly_inner').hide();
				$this = $(this);
				$this.parent().hide(500);
				
				var $candidiate=$this.parent().parent().next();
				rid = $.trim($candidiate.attr('id'));
				$candidiate.css('display','block');
				$candidiate.children('textarea').focus();
								
		});
		
			cancel_the_reporting();
			
		$('.report_now').click(function(){
			$this=$(this);
			reason=$this.parent().prev().val();
			if(reason != '')
			{
				
				old_cls='report_review';
				new_cls='report_done';
				remark_id = $this.parent().parent().attr('id');
				
				if(remark_id != '')
					{
						$.post(ajax_url+'Home/report_remark',{remark_id: remark_id, reason: reason},function(resp){
								
								if($.trim(resp) == 'reported')
								{
									$this.parent().parent().prev().children('.rb_rv_snp2').children().removeClass(old_cls).addClass(new_cls);
									$this.parent().parent().prev().children('.rb_rv_snp2').show();
									$this.parent().parent().prev().children('.rb_rv_snp2').children().html('<?php echo __('reported'); ?>');
									$this.parent().parent().hide();
								}
								
						});
					}

			}
			else
			{
				alert('Please give a reason why you want to report this review');
				return false;
			}
		});
		
	});
	
	function cancel_the_reporting()
	{
		$('.cancel_reporting').click(function(){
			
			$reason_form = $(this).parent().parent('.rly_inner');
			$(this).parent().prev().val('');
			$reason_form.hide(500);
			$reason_form.prev().children('.rb_rv_snp2').show(500);
			
		});

	}
	
</script>

<div id="review-box" class="login-popup">
<?php echo $this->Html->link($this->Html->image('dlt.png',array('width'=>20,'height'=>20,'class'=>'btn_close')),'#review-box',array('class'=>'close','title'=>'Close Window','alt'=>'Close','escape'=>false)); ?>

	<div class="rd_rv_otr">
    <h2> <?php echo __('reviews'); ?> </h2> 
	<div class="revie_popup">
     <?php if($this->Session->read('LocTrain.id')!= $course_details['Course']['m_id']) { ?>
    	<a href="#add-review" class="login-window add_rev"></a>
<?php }  ?>
    </div>	
    <div class="rd_rv_cntnr">
        <div class="rd_rv_maincntnr">
        <?php 
        	if(!empty($course_details['CourseRemark'])) 
			{$i=1;
				foreach($course_details['CourseRemark'] as $remark)
				{  
					$id = base64_encode(convert_uuencode($remark['id']));			
							
					
			 ?>
            <div class="rd_rv_blk" id="show_<?php echo $i;?>">
                <span class="rd_rv_data" rel="<?php echo $remark['id']; ?>">
                    <p>
						<?php echo $this->Text->truncate($remark['comment'],200,array('ellipses'=>'...','exact'=>false)); ?>
						<span style="color:#A5A5A5;">(<?php echo $remark['anonymous'] == 0 ? $remark['Member']['given_name'].' '.$remark['Member']['family_name'] : __('anonymous_user'); ?>)</span>
<?php if($this->Session->read('LocTrain.id')==$remark['Member']['id']) { ?> 

<a  style="text-decoration:none;" class="login-window" href="#edit_review"><span class="show_review" style="color:#2D66B1;"> (<?php echo __('edit'); ?>) </span></a> <?php } ?>
					</p>
                </span>
				
				<span class="rd_rv_data" style="display:none;" >
                    <p>
						<?php  echo nl2br($remark['comment']); ?>
						<span style="color:#A5A5A5;">(<?php echo $remark['anonymous'] == 0 ? $remark['Member']['given_name'].' '.$remark['Member']['family_name'] : __('anonymous_user'); ?>)</span>
<?php if($this->Session->read('LocTrain.id')==$remark['Member']['id']) { ?> 

<a style="text-decoration:none;" class="login-window" href="#edit_review"><span class="show_review" style="color:#2D66B1;"> (<?php echo __('edit'); ?>) </span></a>
 <?php } ?>
					</p>
                </span>
				
                <span class="rd_rv_data_btm">
					
					<?php if(strlen($remark['comment']) > 200){ ?>
					
						<span class="rb_rv_snp">
							<?php echo $this->Html->link(__('show_more'),'javascript:void(0);',array('class'=>"read_more_review show_full_review",'rel'=>'show_full')); ?>
						</span>
					
					<?php } ?>					
                   	
                   	<?php if($remark['member_id'] != $this->Session->read('LocTrain.id')){ ?>
                   		<?php if(empty($remark['RemarkReport'])) { ?>
								<span class="rb_rv_snp2">
									<?php echo $this->Html->link(__('report'),'javascript:void(0)',array('class'=>'ryt_flt report report_review','id'=>$id));?> 
								</span>
						<?php } else {?>
								<span class="rb_rv_snp2">
									<?php echo $this->Html->link(__('reported'),'javascript:void(0)',array('class'=>'ryt_flt report_done','id'=>$id));?> 
								</span>
						<?php } ?>
                    <?php } ?>
                    	
                </span>
                
                	<div class="rly_inner" style="display:none;" id="<?php echo $id; ?>" >
                    	<label><?php echo __('reason'); ?>:</label>
                    	<textarea placeholder = "<?php echo __('give_a_reason_why_you_want_to_report_this_review'); ?>"></textarea>
                    	<div class="rly_inner_anchor">
	                        	<a  class="ryt_flt report_now" href="javascript:void(0)"><?php echo __('report_now'); ?>:</a>
								<a  class="ryt_flt cancel_reporting" href="javascript:void(0)"><?php echo __('cancel'); ?></a>
                    	</div>
                    </div>
            </div>  
           <?php
				 $i++;}
		    }else
				{
					echo __("no_reviews_received");
			}
		?>
        </div>
    </div>
</div>    
</div>    
<?php echo $this->element('frontElements/home/edit_review'); ?>
<?php echo $this->element('frontElements/home/edit_user_review'); ?>
