<?php /*echo $countnotification; 
echo 'mem :'.$this->Session->read('LocTrain.id');
echo 'visitor :'.$this->Session->read('LocTrain.visitor_id');*/



?>

<?php 

$locale = $this->Session->read('LocTrain.locale');  
	$my_teaching = array('course','course_create','course_edit','lesson_add','lesson_edit');
	$catalogue = array('catalogue','view_course_details','catalogue_landing');
	$cart = array('purchase');
        
	echo $this->Html->css('front/notification.min.css');      
	echo $this->Html->script('front/mustache.min.js');
        echo $this->Html->script('front/notification.min.js');
// echo '<pre>';print_r($notification);echo '</pre>';
/* echo $this->Html->css($this->Utility->getAWSImgUrl(STATIC_BUCKET,'css/front/notification.min.css'));
echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/front/mustache.min.js'));
echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/front/notification.min.js'));*/
?>
 
              
<script>
  var  jsonObj = [];
    
    <?php 
                $noftication_arr=array();
				if(!empty($notification))
				{  
					$heading="";
					$n_text="";
                    $i=0;
					foreach($notification as $notification)
					{
						if($notification['Notification']['notification_type']=='like' && !empty($notification['CourseLesson']['id']))
						{
							$heading=__('like_lesson');
							
							
							$n_text = String::insert(__('member_liked_your_lesson_lesson'),array('member' => $notification['Member']['given_name'],'lesson' => $notification['CourseLesson']['title']));
						}
						if($notification['Notification']['notification_type']=='like' && empty($notification['CourseLesson']['id']))
						{
							$heading=__('like_course');
							
							$n_text = String::insert(__(':member liked your course :course'),array('member' => $notification['Member']['given_name'],'course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='unlike' && !empty($notification['CourseLesson']['id']))
						{
							$heading=__('unlike_lesson');
						
							$n_text = String::insert(__('member_unliked_your_lesson_lesson'),array('member' => $notification['Member']['given_name'],'lesson' => $notification['CourseLesson']['title']));
							
						}
						if($notification['Notification']['notification_type']=='unlike' && empty($notification['CourseLesson']['id']))
						{
							$heading= __('unlike_course');
							
							
							$n_text = String::insert(__('member_unliked_your_course_course'),array('member' => $notification['Member']['given_name'],'course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='course_purchased')
						{
							$heading= __('course_purchased');
							
							
							$n_text = String::insert(__('member_has_purchased_your_course_course'),array('member' => $notification['Member']['given_name'],'course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='lesson_purchased')
						{
							$heading= __('lesson_purchased');
							
							
							$n_text = String::insert(__('member_has_purchased_your_lesson_lesson'),array('member' => $notification['Member']['given_name'],'lesson' => $notification['CourseLesson']['title']));
						}
						if($notification['Notification']['notification_type']=='become_author')
						{
							$heading= __('author_request_accepted');
							$n_text=__('you_have_been_accepted_as_an_author');
						 }
						 if($notification['Notification']['notification_type']=='reject_author')
						{
							$heading= __('author_request_rejected');
							$n_text=__('your_request_to_become_an_author_has_been_rejected');
						 }
						if($notification['Notification']['notification_type']=='become_member')
						{
							$heading= __('member');
							$n_text=__('you_are_a_member_now');
						}
						if($notification['Notification']['notification_type']=='course_saved')
						{
							$heading= __('course_saved');
							
							$n_text = String::insert(__('course_saved_'),array('course' => $notification['Course']['title']));
							
						}
						if($notification['Notification']['notification_type']=='delete_lesson')
						{
							$heading= __('delete_lesson');
							$n_text=__('lesson_deleted');
						}
						 if($notification['Notification']['notification_type']=='video_is_being_deleted')
						{
							$heading= __('video_is_being_deleted');
							$n_text=__('video_is_being_deleted');
						}
						if($notification['Notification']['notification_type']=='course_update')
						{
							$heading= __('course_updated');
							
							$n_text = String::insert(__('course_updated_'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='lesson_save')
						{
							$heading= __('lesson_saved');
							
							
							$n_text = String::insert(__('lesson_has_been_saved_you_will_receive_an_e-mail_when_it_is_ready_to_be_viewed'),array('lesson' => $notification['CourseLesson']['title']));
													}
if($notification['Notification']['notification_type']=='upload_progress')
						{
							$heading= __('upload_progress');
							
							 $n_text = String::insert(__("lesson_Video_upload_is_in_process_we_will_let_you_know_once_the_video_has_been_successfully_uploaded"),array('lesson' => $notification['CourseLesson']['title']));

							
						}
						if($notification['Notification']['notification_type']=='retire_course')
						{
							$heading= __('retire_course');
							$n_text = String::insert(__('course_is_retired'),array('course' => $notification['Course']['title']));
						}

						if($notification['Notification']['notification_type']=='lesson_update')
						{
							$heading= __('lesson_updated');
														
							$n_text = String::insert(__('lesson_has_been_updated'),array('lesson' => $notification['CourseLesson']['title']));
						}
						if($notification['Notification']['notification_type']=='update_profile')
						{
							$heading= __('profile_updated');
							$n_text=__('profile_updated');
						}
						if($notification['Notification']['notification_type']=='password_changed')
						{
							$heading= __('password_changed');
							$n_text=__('your_password_has_been_changed');
						}
						if($notification['Notification']['notification_type']=='request_course_author')
						{
							$heading= __('author_request');
							$n_text=__('your_request_to_become_an_author_has_been_sent_to_a_membership_manager_for_review');
						}
						if($notification['Notification']['notification_type']=='already_author')
						{
							$heading= __('already_author');
							$n_text=__('you_are_an_author_already');
						}
						if($notification['Notification']['notification_type']=='add_course_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_has_been_added_to_your_shopping_basket'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='course_not_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_was_not_added_to_the_shopping_basket_please_try_again'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='lesson_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							
							$n_text = String::insert(__('lesson_has_been_added_to_your_shopping_basket'),array('lesson' => $notification['CourseLesson']['title']));
						}
						if($notification['Notification']['notification_type']=='lesson_present_shopping_basket')
						{
							if($notification['Notification']['c_id']==NULL)
							{
									$heading= __('shopping_basket');
							$n_text = String::insert(__('a_course_in_the_shopping_basket_already_contains_lesson'),array('lesson' => $notification['CourseLesson']['title']));
							}
							else
							{
								$heading= __('shopping_basket');
							$n_text = String::insert(__('a_course_in_the_shopping_basket_already_contains_lesson'),array('lesson' => $notification['Course']['title']));
							}
						
						}
						if($notification['Notification']['notification_type']=='lesson_not_added_shopping_basket')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('lesson_was_not_added_to_the_shopping_basket_please_try_again'),array('lesson' => $notification['CourseLesson']['title']));
						}
						if($notification['Notification']['notification_type']=='course_removed_shopping_basket' && !empty($notification['CourseLesson']['id']) )
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('lesson_was_removed_from_the_shopping_basket'),array('lesson' => $notification['CourseLesson']['title']));
						}else if($notification['Notification']['notification_type']=='course_removed_shopping_basket' && !empty($notification['Course']['id']) )
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_was_removed_from_the_shopping_basket'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='course_not_removed_shopping_basket' && !empty($notification['CourseLesson']['id']))
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('lesson_not_removed_from_cart_please_try_again'),array('lesson' => $notification['CourseLesson']['title']));
						}
						if($notification['Notification']['notification_type']=='course_not_removed_shopping_basket' && $notification['CourseLesson']['id']=='')
						{
							$heading= __('shopping_basket');
							
							$n_text = String::insert(__('course_was_not_removed_from_the_shopping_basket_please_try_again'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='course_added_learning_plan')
						{
							$heading= __('learning_plan');
							
							$n_text = String::insert(__('course_was_added_to_your_learning_plan'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='course_not_added_learning_plan')
						{
							$heading= __('learning_plan');
							
							$n_text = String::insert(__('course_was_not_added_to_your_learning_plan_please_try_again'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='already_added_learning_plan')
						{
							$heading= __('learning_plan');
							
							$n_text = String::insert(__('course_is_already_in_your_learning_plan'),array('course' => $notification['Course']['title']));
						}
						if($notification['Notification']['notification_type']=='review')
						{
							$heading= __('review');
							$n_text=__('thank_you_for_your_review');
						}
						if($notification['Notification']['notification_type']=='course_not_purchased')
						{
							$heading= __('course_not_purchased');
							//	$n_text=String::insert(__('the_order_has_been_cancelled_and_you_have_not_been_charged'));
								$n_text=__('the_order_has_been_cancelled_and_you_have_not_been_charged');
						}
						if($notification['Notification']['notification_type']=='course_purchased_sucessfully')
						{
							$heading= __('your_transaction_is_complete');
							$n_text=__('a_confirmation_message_has_been_sent_to_your_registered_e-mail_address');
						}
						if($notification['Notification']['notification_type']=='subsptn_account_reactivate')
						{
							$heading= __('account_activation');
							$n_text=__('your_account_has_been_activated');
						}
						if($notification['Notification']['notification_type']=='subscription_success')
						{
							$heading= __('your_subscription_is_complete');
							$n_text=__('a_confirmation_message_has_been_sent_to_your_registered_e-mail_address');
						}
						if($notification['Notification']['notification_type']=='subscription_failure')
						{
							$heading= __('subscription_not_purchased');
							$n_text= String::insert(__('the_order_has_been_cancelled_and_you_have_not_been_charged'));
						}
						if($notification['Notification']['notification_type']=='member_not_login')
						{
							$heading= __('access_denied');
							$n_text=__('please_sign_in_to_access_this_section');
						}	
						if($notification['Notification']['notification_type']=='account_activation')
						{
							$heading= __('account_activation');
							$n_text=__('your_account_has_been_activated_you_may_change_your_membership_details_any_time_by_selecting_your_e-mail_address_in_the_header');
						}
						if($notification['Notification']['notification_type']=='account_reactivate')
						{
							$heading= __('account_reactivate');
							$n_text=__('account_has_been_reactivated');
						}
						if($notification['Notification']['notification_type']=='permission_denied')
						{
							$heading= __('permission_denied');
							$n_text=__('permission_denied');
						}
						if($notification['Notification']['notification_type']=='account_cancel_success')
						{
							$heading= __('account_cancelled');
							$n_text=__('your_account_has_been_cancelled_but_we_will_be_happy_to_see_you_again');
						}
						if($notification['Notification']['notification_type']=='register_success')
						{
							$heading= __('registration');
							$n_text=__('a_confirmation_message_has_been_sent_to_the_e-mail_address_you_entered_please_select_activate_my_membership_when_the_e-mail_arrives');
						}
						if($notification['Notification']['notification_type']=='unable_account_activation')
						{
							$heading= __('unable_to_activate_membership');
							$n_text=__('the_activate_my_membership_link_has_already_been_used_or_it_has_passed_its_expiry_date_and_time_if_your_membership_has_already_been_activated_please_sign_in_using_your_e-mail_address_and_the_password_you_entered_during_registration_if_your_membership_was_not_activated_within_24-hours_of_registration_you_will_need_to_register_again');
						}
					
                                                    
                                                    
					?>
                                        
                        item = {}
                        item ["id"] = "<?php echo $i;?>";
                        item ["title"] = "<?php echo strip_tags($heading);?>";
                        item ["msg"] = "<?php echo strip_tags($n_text);?>";
                        item ["h_id"] ="<?php echo base64_encode(convert_uuencode($notification['Notification']['id'])); ?>";
                        jsonObj.push(item);
				
			<?php $i++;		 }
				} else{
                                  
			?> 
			<?php } ?>  
var _prum = [['id', '5416cd59abe53d10160cf5ee'],
             ['mark', 'firstbyte', (new Date()).getTime()]];
(function() {
    var s = document.getElementsByTagName('script')[0]
      , p = document.createElement('script');
    p.async = 'async';
    p.src = '//rum-static.pingdom.net/prum.min.js';
    s.parentNode.insertBefore(p, s);
})();
</script>
<?php  if($this->Session->read('LocTrain.login') == '1') {
$calss='LogoutWrapper';
}else{
$calss='';
}?>
<input type="hidden" id="notify_header" value="<?php echo __("notifications");?>">
<input type="hidden" id="notify_no_record" value="<?php echo __("you_don_t_have_notifications");?>">
<!-- Header1 [Start] -->
<header class="Header1">
  <div class="container-fluid">
    <div class="row">

      <div class="top_head <?php echo $calss;?>">
        
        <div class="col-lg-12 top_login">
          <ul>
                      <li class="Notification"> <button type="button" class="button-default show-notifications js-show-notifications <?php if($countnotification >0){echo 'NotifyFull';}?>">
                  
                              <div class="notifications-count js-count"><?php echo $countnotification; ?></div>
                  </button>
                      
            
            </li>
                 <?php  if($this->Session->read('LocTrain.login') == '1' && $this->Session->read('LocTrain.member_type') == '3') { ?>
          <li class="myTeaching"> <?php echo $this->Html->link('<span class="headerTitle">'.__('my_teaching').'</span>',array('controller'=>'Members','action'=>'course'),array('class'=>in_array(trim($action),$my_teaching)?'chngbg':'','escape'=>false)); } ?> </li>
        <?php  if($this->Session->read('LocTrain.login') == '1') { ?>
          <li class="myLearning"> <?php echo $this->Html->link('<span class="headerTitle">'.__('my_learning').'</span>',array('controller'=>'Members','action'=>'my_learning'),array('class'=>'','escape'=>false));?> </li>
        <?php } ?>

   
            <?php  if($this->Session->read('LocTrain.login')!='1'){ ?>
 <li class="register"><?php echo $this->Html->link('<span class="headerTitle">'.__('register').'</span>',array('controller'=>'Home','action'=>'register'),array('escape'=>false));?> </li>
<?php }else{?>
<li class="signout"><?php echo $this->Html->link('<span class="headerTitle">'.__('sign_out').'</span>',array('controller'=>'Home','action'=>'logout'),array('escape'=>false)); ?></li>
<?php } ?>
            <li class="SignIn"> <?php 
                if($this->Session->read('LocTrain.login')=='1'){
                echo $this->Html->link('<span class="headerTitle">'.__($this->Session->read('LocTrain.email')).'</span>',array('controller'=>'Members','action'=>'update_profile'),array('escape'=>false)); 
                }else{ ?>
                 <!-- Button trigger modal -->
                <a class="SignInBtn" data-toggle="modal" data-target="#myModal1"><span class="headerTitle"><?php echo __('sign_in');?></span></a>
                
                <!-- Modal -->
                <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            	<h4 class="modal-title" id="myModalLabel"><?php echo __("sign_in_to_learn_and_teach");?></h4>
                            </div>
                            <div class="modal-body">                                
                                <?php echo $this->Form->create('Login',array('role'=>'form', 'id'=>'memberLogin','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'member_login')));?>
                                    <div class="form-group">
                                    <?php echo $this->Form->input('email',array('type'=>'text','id'=>'emailId1','class'=>'form-control TextEmail','placeholder'=> __('e-mail_address_'),'div'=>false,'label'=>false,'value'=>isset($cookieVal)?$cookieVal:'')); ?>
		  
                                    <div id="err_email" class="register_error  alert-danger"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>
                                   
                                    </div>
                                    <div class="form-group">
                                     <?php echo $this->Form->input('password',array('type'=>'password','id'=>'password1','class'=>'form-control TextPwd','placeholder'=>__('password'),'div'=>false,'label'=>false)); ?>
                                    <div id="err_password" class="register_error  alert-danger" > <?php if(isset($error['password'][0])) echo $error['password'][0];?> </div>
                                    
                                    </div>                                    
                                    <?php echo $this->Form->submit(__('sign_in'),array('class'=>'btn btn-primary btn-lg ButtonBlue','onclick'=>"return ajax_form('memberLogin','Home/login_check_ajax','load_top')")); ?>
                                  
                                    <span class="Link link_reg_not">
                                        <?php echo $this->Html->link(__('i_havent_registered_yet'),array('controller'=>'Home','action'=>'register'),array('class'=>'memberLogin_a'));?> 
                                    </span>
                                    
				<?php echo $this->Form->end(); ?>
                                <span class="Link link_forgot_pwd"><a href="javascript:;" onclick="return openForgotForm(1);"><?php echo __('i_forgot_my_password');?></a></span>
                                    <p class="form-group openForgotForm" id="openForgotForm_1">
                                          <?php echo __("we_will_send_an_e_mail_to_you_with_a_password_reset_link_before_we_send_it_please_confirm_your_e_mail_address_above_is_correct");?>
                                    </p>
                                    <span class="Link show_link link_send_pwd" id="show_link_1"><a href="javascript:;" onclick="return forgot_ajax_form('emailId',1,'myModal1');"><?php echo __('send_the_password_reset_e-mail_to_me');?></a></span>
                            </div>                            
                        </div>
                    </div>
                </div>
                
                <?php   }?> 
            </li> 
<li class="preferences"><?php
echo $this->Html->link('<span class="headerTitle">'.__('preferences').'</span>',array('controller' => 'Home', 'action' => 'user_preferences',($this->Session->read('LocTrain.preference'))?$this->Session->read('LocTrain.preference'):'normal'),array('escape'=>false));            
?></li> 
            <li class="dropdown Language" id="">
                <a data-toggle="dropdown" class="dropdown-toggle" role="button" id="dropdown1" href="#"><span class="headerTitle"><?php echo __('language'); ?></span></a>
                 
<?php   $locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';?>
                <ul aria-labelledby="dropdown1" role="menu" class="dropdown-menu dropdownLang_<?php echo $locale;?>" id="formLang">
<li class="LanguageTitle"><i class="fa fa-caret-up"></i><?php echo __('language_options');?></li>
                    <?php echo $this->Form->create('Language',array('id'=>'changeLanguage','url'=>'/Home/change_language')); ?>                        
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_rendered','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_1','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_id_1','value'=>$this->Session->read('LocTrain.LangId'))); ?>
                    <?php
                          

                            // $s_lang = array('en'=>__('english_united_states'),'de'=>__('german_germany'),'ar'=>__('arabic'),'fr'=>__('french_france'),'it'=>__('italian_italy'),'ja'=>__('japanese'),'ko'=>__('korean'),'pt'=>__('portuguese_portugal'),'ru'=>__('russian_russian_federation'),'zh'=>__('chinese_china'),'es'=>__('spanish_spain'));

$s_lang = array('en'=>__('english_united_states_'),'de'=>__('deutsch_deutschland'),'ar'=>__('ar_lang'),'fr'=>__('fr_lang'),'it'=>__('it_lang'),'ja'=>__('ja_lang'),'ko'=>__('ko_lang'),'pt'=>__('pt_lang'),'ru'=>__('ru_lang'),'zh'=>__('zh_lang'),'es'=>__('es_lang'));

                            if(!empty($language))
                            {
                            foreach($language as $lang)
                            {
                            ?>	
                    <li role="presentation" class="<?php if($locale==$lang['Language']['locale']){echo 'activeLanguage';}?>" ><a onclick="changeLanguage('<?php echo $lang['Language']['id']; ?>','<?php echo $lang['Language']['locale']; ?>','changeLanguage','lan_1','lan_id_1');" tabindex="-1" role="menuitem"  id="<?php echo $lang['Language']['id']; ?>" ><?php echo $s_lang[$lang['Language']['locale']]; ?></a></li>
                    <?php }} ?>   
                    <?php echo $this->Form->end(); ?> 
                </ul>
                
                
            </li>                      
          <!--  <li class="Support"><span class="headerTitle"><?php echo $this->Html->link(__('support'),array('controller'=>'Home','action'=>'help'),array());?></span></li> -->
<?php    // if($this->Session->read('LocTrain.login')=='1'){ ?>
            <li class="Last ShoppingCart">
               
            <?php  $cookPurchase = ($cookPurchase!=0)? $cookPurchase:0;?>
			<?php $action=($this->Session->read('LocTrain.login') == '1')? '/home/purchase':'/home/index/'.'few';
			 $action=($this->Session->read('LocTrain.login') == '1')? '/home/purchase':'javascript:void(0)';
					$shopCls='';
					if(!empty($cookPurchase)){
						$shopCls='fullShoppingCart';
					}
			?>
			<?php
					$one=__('shopping_basket');
			$msg='<span class="counter_txt" >' ?>
			<?php 
			
			if($this->Session->read('LocTrain.login')=='1'){ 
			
			echo $this->Html->link($msg. $cookPurchase.' </span>',$action,array('class'=>in_array(trim($action),$cart)?'new_nchor chngbg prinav_li_a gradient '.$shopCls:'new_nchor prinav_li_a gradient '.$shopCls,'escape'=>false));?>
            <?php } 
            else
            {
		echo $this->Html->link($msg. $cookPurchase.' </span>',$action,array('data-target'=>'#myModal1','data-toggle'=>'modal','class'=>in_array(trim($action),$cart)?'new_nchor chngbg prinav_li_a gradient '.$shopCls:'new_nchor prinav_li_a gradient '.$shopCls,'escape'=>false));
			?>
	
			<?php
			}
            ?>
            </li>    
<?php //} ?>                                
          </ul>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  
  <div class="main_header">
    <div class="container-fluid container-header1">
      <div class="row">
 
        <div class="logo">             
            <?php echo $this->Html->image('front/logo.svg',array('url'=>array('controller'=>'Home','action'=>'index'),'class'=>'img-responsive','width'=>'598','height'=>'218','alt'=>'Localization Training','title'=> __('home'))); ?> 
            <!--<a href="index.html"><img src="images/logo.png" class="img-responsive" alt=""/></a>-->
        </div>
      



  <?php echo $this->Form->create('Search',array('type'=>'GET',"role"=>"search","onmousedown"=>" e.preventDefault()","class"=>"",'url'=>array('controller'=>'Home','action'=>'search','.')));?>  
        <div>
    <div class="form-element textfield">
        <?php echo $this->Form->input('q',array('type'=>'text',"placeholder"=>__('Search_or_browse_the_catalogue'),'class'=>'form-control srch_input','label'=>false,'div'=>false,"ondragstart"=>"return false"," ondragenter"=>"event.dataTransfer.dropEffect='none'; event.stopPropagation(); event.preventDefault();" , 
        "ondragover"=>"event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();"  ,"ondrop"=>"event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();","draggable"=>"false",'autocomplete'=>'off',"onmousedown"=>" e.preventDefault()","value"=>isset($q)?$q:'')); ?> 
      	<button type="submit" class="btn btn-default home_submit_btn">
                    <?php if(isset($css) && $css=='style'){?>
                <?php // echo $this->Html->image('front/icons/search_icon.svg',array('class'=>'img-responsive search_normal','alt'=>'Search','title'=> __('search'))); 
                    }else{
              //  echo $this->Html->image('front/icons_contrast/search_icon.svg',array('class'=>'img-responsive search_contrast','alt'=>'Search','title'=> __('search')));
                    }
                ?> 

                </button>
    </div>
  
  <div class="form-element submit-btn">

               
            <div class="Catalouge">
                <?php echo $this->Html->link('<i class="fa fa-bars"></i><span class="catalogueText">'.__('catalogue').'</span>',array('controller'=>'Home','action'=>'catalogue_landing'),array('escape' => FALSE,'title'=>__('catalogue')));?> 
                
            </div>
                
   
    
  </div>
  
  </div>
 
<?php echo $this->Form->end(); ?>  











            
              
            <div class="clearfix"></div>
        </div>
      </div>
   
  </div>
</header>
<!-- Header1 [End] -->

<!-- Header2 [Start] -->
<header class="Header2">
  <div class="container-fluid">
    <div class="row">
     <div class="col-lg-12 top_head  <?php echo $calss;?>">
            	
        	
          
            <?php echo $this->Html->link($this->Html->image('front/logo-responsive.svg',array('class'=>'img-responsive','width'=>'218','height'=>'218','alt'=>'Localization Training')),array('controller'=>'Home','action'=>'index'), array('escape' => false,'class'=>"LogoResponsive"));?>
         <div class="top_login">
   <ul>
                      <li class="Notification"> <button type="button" class="button-default show-notifications js-show-notifications <?php if($countnotification >0){echo 'NotifyFull';}?>">
                  
                              <div class="notifications-count js-count"><?php echo $countnotification; ?></div>
                  </button>
                      
            
            </li>
                 <?php  if($this->Session->read('LocTrain.login') == '1' && $this->Session->read('LocTrain.member_type') == '3') { ?>
          <li class="myTeaching"> <?php echo $this->Html->link('<span class="headerTitle">'.__('my_teaching').'</span>',array('controller'=>'Members','action'=>'course'),array('class'=>in_array(trim($action),$my_teaching)?'chngbg':'','escape'=>false)); } ?> </li>
        <?php  if($this->Session->read('LocTrain.login') == '1') { ?>
          <li class="myLearning"> <?php echo $this->Html->link('<span class="headerTitle">'.__('my_learning').'</span>',array('controller'=>'Members','action'=>'my_learning'),array('class'=>'','escape'=>false));?> </li>
        <?php } ?>

              <?php  if($this->Session->read('LocTrain.login')!='1'){ ?>
 <li class="register"><?php echo $this->Html->link('<span class="headerTitle">'.__('register').'</span>',array('controller'=>'Home','action'=>'register'),array('escape'=>false));?> </li>
<?php }else{?>
<li class="signout"><?php echo $this->Html->link('<span class="headerTitle">'.__('sign_out').'</span>',array('controller'=>'Home','action'=>'logout'),array('escape'=>false)); ?></li>
<?php } ?>
            
            <li class="SignIn"> <?php 
                if($this->Session->read('LocTrain.login')=='1'){
                echo $this->Html->link('<span class="headerTitle">'.__($this->Session->read('LocTrain.email')).'</span>',array('controller'=>'Members','action'=>'update_profile'),array('escape'=>false)); 
                }else{ ?>
                 <!-- Button trigger modal -->
                <a class="SignInBtn" data-toggle="modal" data-target="#myModal2"><span class="headerTitle"><?php echo __('sign_in');?></span></a>
                
                <!-- Modal -->
                <!-- Modal -->
                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?php echo __("sign_in_to_learn_and_teach");?></h4>
                                </div>
                                <div class="modal-body">
                                    <?php echo $this->Form->create('Login',array('role'=>'form', 'id'=>'memberLogin2','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'member_login')));?>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('email',array('type'=>'text','id'=>'emailId2','class'=>'form-control TextEmail','placeholder'=>__('e-mail_address_'),'div'=>false,'value'=>isset($cookieVal)?$cookieVal:'')); ?>
                                            <div id="err2_email" class="register_error  alert-danger"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>
                                         </div>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('password',array('type'=>'password','id'=>'password2','class'=>'form-control TextPwd','placeholder'=>__('password'),'div'=>false)); ?>
                                            <div id="err2_password" class="register_error  alert-danger" > <?php if(isset($error['password'][0])) echo $error['password'][0];?> </div>
                                        </div> 
                                        <?php echo $this->Form->submit(__('sign_in'),array('class'=>'btn btn-primary btn-lg ButtonBlue','onclick'=>"return ajax_form('memberLogin2','Home/login_check_ajax','load_top')")); ?>
                                         
                                        <span class="Link link_reg_not">
                                        <?php echo $this->Html->link(__('i_havent_registered_yet'),array('controller'=>'Home','action'=>'register'),array('class'=>'memberLogin_a2'));?> 
                                    </span>
                                        
                                        
                                    <?php echo $this->Form->end(); ?>
                                    <span class="Link link_forgot_pwd"><a href="javascript:;" onclick="return openForgotForm(2);"><?php echo __('i_forgot_my_password');?></a></span>
                                    <p class="form-group openForgotForm" id="openForgotForm_2">
                                      <?php echo __("we_will_send_an_e_mail_to_you_with_a_password_reset_link_before_we_send_it_please_confirm_your_e_mail_address_above_is_correct");?>
                                    </p>
                                    <span class="Link show_link link_send_pwd" id="show_link_2"><a href="javascript:;" onclick="return forgot_ajax_form('emailId',2,'myModal2');">	<?php echo __('send_the_password_reset_e-mail_to_me');?></a></span>
                                </div>                            
                            </div>
                        </div>
                    </div>
                
                <?php   }?> 
            </li> 
<li class="preferences"><?php
echo $this->Html->link('<span class="headerTitle">'.__('preferences').'</span>',array('controller' => 'Home', 'action' => 'user_preferences',($this->Session->read('LocTrain.preference'))?$this->Session->read('LocTrain.preference'):'normal'),array('escape'=>false));            
?></li> 
            <li class="dropdown Language" id="">
                <a data-toggle="dropdown" class="dropdown-toggle" role="button" id="dropdown1" href="#"><span class="headerTitle"><?php echo __('language'); ?></span></a>
                 
<?php   $locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';?>
                
                <ul aria-labelledby="dropdown1" role="menu" class="dropdown-menu dropdownLang_<?php echo $locale;?>" id="formLang">
<li class="LanguageTitle"><i class="fa fa-caret-up"></i><?php echo __('language_options');?></li>
                    <?php echo $this->Form->create('Language',array('id'=>'changeLanguage','url'=>'/Home/change_language')); ?>                        
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_rendered','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_1','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_id_1','value'=>$this->Session->read('LocTrain.LangId'))); ?>
                    <?php
                            

                             //$s_lang = array('en'=>__('english_united_states'),'de'=>__('german_germany'),'ar'=>__('arabic'),'fr'=>__('french_france'),'it'=>__('italian_italy'),'ja'=>__('japanese'),'ko'=>__('korean'),'pt'=>__('portuguese_portugal'),'ru'=>__('russian_russian_federation'),'zh'=>__('chinese_china'),'es'=>__('spanish_spain'));
$s_lang = array('en'=>__('english_united_states_'),'de'=>__('deutsch_deutschland'),'ar'=>__('ar_lang'),'fr'=>__('fr_lang'),'it'=>__('it_lang'),'ja'=>__('ja_lang'),'ko'=>__('ko_lang'),'pt'=>__('pt_lang'),'ru'=>__('ru_lang'),'zh'=>__('zh_lang'),'es'=>__('es_lang'));

                            if(!empty($language))
                            {
                            foreach($language as $lang)
                            {
                            ?>	
                    <li role="presentation" class="<?php if($locale==$lang['Language']['locale']){echo 'activeLanguage';}?>"><a onclick="changeLanguage('<?php echo $lang['Language']['id']; ?>','<?php echo $lang['Language']['locale']; ?>','changeLanguage','lan_1','lan_id_1');" tabindex="-1" role="menuitem"  id="<?php echo $lang['Language']['id']; ?>" ><?php echo $s_lang[$lang['Language']['locale']]; ?></a></li>
                    <?php }} ?>   
                    <?php echo $this->Form->end(); ?> 
                </ul>
                
                
            </li>                      
          <!--  <li class="Support"><span class="headerTitle"><?php echo $this->Html->link(__('support'),array('controller'=>'Home','action'=>'help'),array());?></span></li> -->
<?php    /* if($this->Session->read('LocTrain.login')=='1'){*/ ?>
            <li class="Last ShoppingCart">
               
            <?php  $cookPurchase = ($cookPurchase!=0)? $cookPurchase:0;?>
			<?php $action=($this->Session->read('LocTrain.login') == '1')? '/home/purchase':'/home/index/'.'few';
					$shopCls='';
					if(!empty($cookPurchase)){
						$shopCls='fullShoppingCart';
					}
			?>
			<?php
					$one=__('shopping_basket');
			$msg='<span class="counter_txt" >' ?>
			
<?php 
			
			if($this->Session->read('LocTrain.login')=='1'){ 
			
			echo $this->Html->link($msg. $cookPurchase.' </span>',$action,array('class'=>in_array(trim($action),$cart)?'new_nchor chngbg prinav_li_a gradient '.$shopCls:'new_nchor prinav_li_a gradient '.$shopCls,'escape'=>false));?>
            <?php } 
            else
            {
		echo $this->Html->link($msg. $cookPurchase.' </span>',$action,array('data-target'=>'#myModal2','data-toggle'=>'modal','class'=>in_array(trim($action),$cart)?'new_nchor chngbg prinav_li_a gradient '.$shopCls:'new_nchor prinav_li_a gradient '.$shopCls,'escape'=>false));
			?>
	
			<?php
			}
            ?>
            </li>  
<?php /*}*/ ?>                                  
          </ul>   
</div>                   
        <div class="clearfix"></div>
<div class="Catalouge"> <?php echo $this->Html->link('<i class="fa fa-bars"></i><span class="catalogueText">'.__('catalogue').'</span>',array('controller'=>'Home','action'=>'catalogue_landing'),array('escape' => FALSE,'title'=>__('catalogue')));?> </div>
        <?php echo $this->Form->create('Search',array('type'=>'GET',"role"=>"search","class"=>"navbar-form",'url'=>array('controller'=>'Home','action'=>'search','.')));?>  
                <div class="form-group">                    
                    <?php echo $this->Form->input('q',array('type'=>'text',"placeholder"=>__('Search_or_browse_the_catalogue'),'class'=>'form-control srch_input','label'=>false,'div'=>false,"ondragstart"=>"return false"," ondragenter"=>"event.dataTransfer.dropEffect='none'; event.stopPropagation(); event.preventDefault();" , 
        "ondragover"=>"event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();"  ,"ondrop"=>"event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();","draggable"=>"false",'autocomplete'=>'off',"onmousedown"=>" e.preventDefault()","value"=>isset($q)?$q:'')); ?> 
                </div>     
            <button type="submit" class="btn btn-default">                
                  <?php if(isset($css) && $css=='style'){?>
                <?php //echo $this->Html->image('front/search_icon.png',array('class'=>'img-responsive search_normal','alt'=>'Search','title'=> __('search'))); 
                    }else{
                //echo $this->Html->image('front/search_icon_contrast.png',array('class'=>'img-responsive search_contrast','alt'=>'Search','title'=> __('search')));
                    }
                ?> 
            </button>
         <?php echo $this->Form->end(); ?>   
      </div>
    </div>
  </div>
</header>
<!-- Header2 [End] -->

<!-- Header3 [Start] -->

<header class="Header3">
  <div class="container-fluid">
    <div class="row">
     <div class="col-lg-12 top_head  <?php echo $calss;?>">
            	
        	
          
            <?php echo $this->Html->link($this->Html->image('front/logo-responsive.svg',array('class'=>'img-responsive','width'=>'218','height'=>'218','alt'=>'Localization Training')),array('controller'=>'Home','action'=>'index'), array('escape' => false,'class'=>"LogoResponsive"));?>
         <div class="top_login">
   <ul>
                      <li class="Notification"> <button type="button" class="button-default show-notifications js-show-notifications <?php if($countnotification >0){echo 'NotifyFull';}?>">
                  
                              <div class="notifications-count js-count"><?php echo $countnotification; ?></div>
                  </button>
                      
            
            </li>
                 <?php  if($this->Session->read('LocTrain.login') == '1' && $this->Session->read('LocTrain.member_type') == '3') { ?>
          <li class="myTeaching"> <?php echo $this->Html->link('<span class="headerTitle">'.__('my_teaching').'</span>',array('controller'=>'Members','action'=>'course'),array('class'=>in_array(trim($action),$my_teaching)?'chngbg':'','escape'=>false)); } ?> </li>
        <?php  if($this->Session->read('LocTrain.login') == '1') { ?>
          <li class="myLearning"> <?php echo $this->Html->link('<span class="headerTitle">'.__('my_learning').'</span>',array('controller'=>'Members','action'=>'my_learning'),array('class'=>'','escape'=>false));?> </li>
        <?php } ?>
  <?php  if($this->Session->read('LocTrain.login')!='1'){ ?>
 <li class="register"><?php echo $this->Html->link('<span class="headerTitle">'.__('register').'</span>',array('controller'=>'Home','action'=>'register'),array('escape'=>false));?> </li>
<?php }else{?>
<li class="signout"><?php echo $this->Html->link('<span class="headerTitle">'.__('sign_out').'</span>',array('controller'=>'Home','action'=>'logout'),array('escape'=>false)); ?></li>
<?php } ?>
            
            <li class="SignIn"> <?php 
                if($this->Session->read('LocTrain.login')=='1'){
                echo $this->Html->link('<span class="headerTitle">'.__($this->Session->read('LocTrain.email')).'</span>',array('controller'=>'Members','action'=>'update_profile'),array('escape'=>false)); 
                }else{ ?>
                 <!-- Button trigger modal -->
                <a class="SignInBtn" data-toggle="modal" data-target="#myModal3"><span class="headerTitle"><?php echo __('sign_in');?></span></a>
                
                <!-- Modal -->
              <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?php echo __("sign_in_to_learn_and_teach");?></h4>
                                </div>
                                <div class="modal-body">
                                    <?php echo $this->Form->create('Login',array('role'=>'form', 'id'=>'memberLogin3','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'member_login')));?>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('email',array('type'=>'text','id'=>'emailId3','class'=>'form-control TextEmail','placeholder'=>__('e-mail_address_'),'div'=>false,'value'=>isset($cookieVal)?$cookieVal:'')); ?>
                                            <div id="err3_email" class="register_error  alert-danger"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('password',array('type'=>'password','id'=>'password3','class'=>'form-control TextPwd','placeholder'=>__('password'),'div'=>false)); ?>
                                            <div id="err3_password" class="register_error  alert-danger" > <?php if(isset($error['password'][0])) echo $error['password'][0];?> </div>
                                        </div>                                    
                                        <?php echo $this->Form->submit(__('sign_in'),array('class'=>'btn btn-primary btn-lg ButtonBlue','onclick'=>"return ajax_form('memberLogin3','Home/login_check_ajax','load_top')")); ?>
                                        <span class="Link link_reg_not">
                                        <?php echo $this->Html->link(__('i_havent_registered_yet'),array('controller'=>'Home','action'=>'register'),array('class'=>'memberLogin_a3'));?> 
                                    </span>
                                        
                                    <?php echo $this->Form->end(); ?>
                                    <span class="Link link_forgot_pwd"><a href="javascript:;" onclick="return openForgotForm(3);"><?php echo __('i_forgot_my_password');?></a></span>   
                                    <p class="form-group openForgotForm" id="openForgotForm_3">
                                        <?php echo __("we_will_send_an_e_mail_to_you_with_a_password_reset_link_before_we_send_it_please_confirm_your_e_mail_address_above_is_correct");?>
                                    </p>
                                    <span class="Link show_link link_send_pwd" id="show_link_3"><a href="javascript:;" onclick="return forgot_ajax_form('emailId',3,'myModal3');">	<?php echo __('send_the_password_reset_e-mail_to_me');?></a></span>
                                </div>                            
                            </div>
                        </div>
                    </div>
                
                <?php   }?> 
            </li> 
<li class="preferences"><?php
echo $this->Html->link('<span class="headerTitle">'.__('preferences').'</span>',array('controller' => 'Home', 'action' => 'user_preferences',($this->Session->read('LocTrain.preference'))?$this->Session->read('LocTrain.preference'):'normal'),array('escape'=>false));            
?></li> 
            <li class="dropdown Language" id="">
                <a data-toggle="dropdown" class="dropdown-toggle" role="button" id="dropdown1" href="#"><span class="headerTitle"><?php echo __('language'); ?></span></a>
                 <?php   $locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';?>
                
                <ul aria-labelledby="dropdown1" role="menu" class="dropdown-menu dropdownLang_<?php echo $locale;?>" id="formLang">

              
<li class="LanguageTitle"><i class="fa fa-caret-up"></i><?php echo __('language_options');?></li>
                    <?php echo $this->Form->create('Language',array('id'=>'changeLanguage','url'=>'/Home/change_language')); ?>                        
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_rendered','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_1','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_id_1','value'=>$this->Session->read('LocTrain.LangId'))); ?>
                    <?php
                           

                           //  $s_lang = array('en'=>__('english_united_states'),'de'=>__('german_germany'),'ar'=>__('arabic'),'fr'=>__('french_france'),'it'=>__('italian_italy'),'ja'=>__('japanese'),'ko'=>__('korean'),'pt'=>__('portuguese_portugal'),'ru'=>__('russian_russian_federation'),'zh'=>__('chinese_china'),'es'=>__('spanish_spain'));

$s_lang = array('en'=>__('english_united_states_'),'de'=>__('deutsch_deutschland'),'ar'=>__('ar_lang'),'fr'=>__('fr_lang'),'it'=>__('it_lang'),'ja'=>__('ja_lang'),'ko'=>__('ko_lang'),'pt'=>__('pt_lang'),'ru'=>__('ru_lang'),'zh'=>__('zh_lang'),'es'=>__('es_lang'));

                            if(!empty($language))
                            {
                            foreach($language as $lang)
                            {
                            ?>	
                    <li role="presentation" class="<?php if($locale==$lang['Language']['locale']){echo 'activeLanguage';}?>"><a onclick="changeLanguage('<?php echo $lang['Language']['id']; ?>','<?php echo $lang['Language']['locale']; ?>','changeLanguage','lan_1','lan_id_1');" tabindex="-1" role="menuitem"  id="<?php echo $lang['Language']['id']; ?>" ><?php echo $s_lang[$lang['Language']['locale']]; ?></a></li>
                    <?php }} ?>   
                    <?php echo $this->Form->end(); ?> 
                </ul>
                
                
            </li>                      
          <!--  <li class="Support"><span class="headerTitle"><?php echo $this->Html->link(__('support'),array('controller'=>'Home','action'=>'help'),array());?></span></li> -->
<?php     /*if($this->Session->read('LocTrain.login')=='1'){*/ ?>
            <li class="Last ShoppingCart">
               
            <?php  $cookPurchase = ($cookPurchase!=0)? $cookPurchase:0;?>
			<?php $action=($this->Session->read('LocTrain.login') == '1')? '/home/purchase':'/home/index/'.'few';
					$shopCls='';
					if(!empty($cookPurchase)){
						$shopCls='fullShoppingCart';
					}

			?>
			<?php
					$one=__('shopping_basket');
			$msg='<span class="counter_txt" >' ?>
			<?php 
			
			if($this->Session->read('LocTrain.login')=='1'){ 
			
			echo $this->Html->link($msg. $cookPurchase.' </span>',$action,array('class'=>in_array(trim($action),$cart)?'new_nchor chngbg prinav_li_a gradient '.$shopCls:'new_nchor prinav_li_a gradient '.$shopCls,'escape'=>false));?>
            <?php } 
            else
            {
		echo $this->Html->link($msg. $cookPurchase.' </span>',$action,array('data-target'=>'#myModal1','data-toggle'=>'modal','class'=>in_array(trim($action),$cart)?'new_nchor chngbg prinav_li_a gradient '.$shopCls:'new_nchor prinav_li_a gradient '.$shopCls,'escape'=>false));
			?>
	
			<?php
			}
            ?>
            </li>    
<?php /* }*/ ?>                                
          </ul>   
</div>                   
        <div class="clearfix"></div>
<div class="Catalouge"> <?php echo $this->Html->link('<i class="fa fa-bars"></i><span class="catalogueText">'.__('catalogue').'</span>',array('controller'=>'Home','action'=>'catalogue_landing'),array('escape' => FALSE,'title'=>__('catalogue')));?> </div>
        <?php echo $this->Form->create('Search',array('type'=>'GET',"role"=>"search","class"=>"navbar-form",'url'=>array('controller'=>'Home','action'=>'search','.')));?>  
                <div class="form-group">                    
                    <?php echo $this->Form->input('q',array('type'=>'text',"placeholder"=>__('Search_or_browse_the_catalogue'),'class'=>'form-control srch_input','label'=>false,'div'=>false,"ondragstart"=>"return false"," ondragenter"=>"event.dataTransfer.dropEffect='none'; event.stopPropagation(); event.preventDefault();" , 
        "ondragover"=>"event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();"  ,"ondrop"=>"event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();","draggable"=>"false",'autocomplete'=>'off',"onmousedown"=>" e.preventDefault()","value"=>isset($q)?$q:'')); ?> 
                </div>     
            <button type="submit" class="btn btn-default">                
                  <?php if(isset($css) && $css=='style'){?>
                <?php //echo $this->Html->image('front/search_icon.png',array('class'=>'img-responsive search_normal','alt'=>'Search','title'=> __('search'))); 
                    }else{
              //  echo $this->Html->image('front/search_icon_contrast.png',array('class'=>'img-responsive search_contrast','alt'=>'Search','title'=> __('search')));
                    }
                ?> 
            </button>
         <?php echo $this->Form->end(); ?>   
      </div>
    </div>
  </div>
</header>
<!-- Header3 [End] -->

<script type="text/javascript">
function checkpassword(id,error_id){

var msg='<?php echo  __("please_choose_a_password_consisting_of")."<br>".__("a_minimum_of_8_and_maximum_of_20_characters").'<br>'.__("at_least_one_upper_case_character").'<br>'. __("at_least_one_lower_case_character").'<br>'. __("at_least_one_number").'<br>'.__("at_least_one_non_alphanumeric_character");?>';
		var inputValPass = $('#'+id).val();			
		if(inputValPass == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#"+error_id).text(msg);
		}
		/*else if($('#'+id).val().length < 8 || $('#'+id).val().length > 20){
	$("#"+error_id).html(msg);
	}else if(!$('#'+id).val().match(/[A-Z]/)){
		$("#"+error_id).html(msg);
	}else if(!$('#'+id).val().match(/[a-z]/)){
		$("#"+error_id).html(msg);
	}else if(!$('#'+id).val().match(/[0-9]/)){
		$("#"+error_id).html(msg);
	}else if(!$('#'+id).val().match(/[!@?#$%^&*()\-_=+{};:,<.>~]/)){
		$("#"+error_id).html(msg);
	}*/else{
		$("#"+error_id).html('');
	}
}
	
    $('document').ready(function(){
	if($('.Header1').is(':visible')){
					$('<input>').attr({
						    type: 'hidden',
						    class:'redirectPage',
						    value:"",
						    name: 'redirectPage'
						}).appendTo('form#memberLogin');

		}else if($('.Header2').is(':visible')){
					$('<input>').attr({
						    type: 'hidden',
						    class:'redirectPage',
						    value:"",
						    name: 'redirectPage'
						}).appendTo('form#memberLogin2');

		}else if($('.Header3').is(':visible')){
					$('<input>').attr({
						    type: 'hidden',
						    class:'redirectPage',
						    value:"",
						    name: 'redirectPage'
						}).appendTo('form#memberLogin3');

		}
	
	//login pop for logout case
	$('.ShoppingCart').click(function(){
		$('.redirectPage').val('purchase');
	});
	$('.becoming_an_author').click(function(){
		$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
		if($('.Header1').is(':visible')){$('#myModal1').modal('show');
		}else if($('.Header2').is(':visible')){$('#myModal2').modal('show');
		}else if($('.Header3').is(':visible')){$('#myModal3').modal('show');
		}
		$('.redirectPage').val('becoming_an_author');
	});
 	$('#myModal1').on('hide.bs.modal',function(){
		$('.redirectPage').val('');
	});
	$('#myModal2').on('hide.bs.modal',function(){
		$('.redirectPage').val('');
	});
	$('#myModal3').on('hide.bs.modal',function(){
		$('.redirectPage').val('');
	});
		
	$('#emailId1').css({'border-color':'#66afe9','box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 175, 233, 		0.6)','outline':'0 none'});
 	
	$('#emailId1').focusout(function(){
		$(this).attr('style','');
	});
$('#emailId2').css({'border-color':'#66afe9','box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 175, 233, 0.6)','outline':'0 none'});
	$('#emailId2').focusout(function(){
		$(this).attr('style','');
	});
$('#emailId3').css({'border-color':'#66afe9','box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 175, 233, 0.6)','outline':'0 none'});
	$('#emailId3').focusout(function(){
		$(this).attr('style','');
	});
	$('#emailId1').on('keyup',function(){ 	
		var msg = '';
		var inputValEmail = $('#emailId1').val();	
		
		//var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_email").text(msg);
		}
		else {
			$("#err_email").text('');
		}
	
		
		
	});
	$('#password1').on('focus',function(){
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
			var inputValEmail = $('#emailId1').val();	
			if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_email").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		});
		
		$('#password2').on('focus',function(){
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
			var inputValEmail = $('#emailId2').val();	
			if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err2_email").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		});
		
		
		$('#password3').on('focus',function(){
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
			var inputValEmail = $('#emailId3').val();	
			if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err3_email").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		});
$('#emailId2').on('keyup',function(){ 	
		var msg = '';
		var inputValEmail = $('#emailId2').val();	
		
		//var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err2_email").text(msg);
		}
		else {
			$("#err2_email").text('');
		}
		
		
		
	});
$('#emailId3').on('keyup',function(){ 	
		var msg = '';
		var inputValEmail = $('#emailId3').val();	
		
		//var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err3_email").text(msg);
		}
		else {
			$("#err3_email").text('');
		}
		
		
		
	});

$('#password1').on('focusout',function(){ checkpassword('password1','err_password');	});
$('#password2').on('focusout',function(){ checkpassword('password2','err2_password');	});
$('#password3').on('focusout',function(){ checkpassword('password3','err3_password');	});
	var w=$( window ).width();
	if(w>='768'){
	$('.navbar-form').css('width', '100%').css('width', '-=165px');
	$('.navbar-form .form-group').css('width', '100%').css('width', '-=67px');
	}else{
	
	$('.navbar-form').css('width', '100%').css('width', '-=44px');
	$('.navbar-form .form-group').css('width', '100%').css('width', '-=42px');
	}
	$(window).resize(function() {
	var w=$( window ).width();
	if(w>='768'){
	$('.navbar-form').css('width', '100%').css('width', '-=165px');
	$('.navbar-form .form-group').css('width', '100%').css('width', '-=67px');
	}else{
	$('.navbar-form').css('width', '100%').css('width', '-=44px');
	$('.navbar-form .form-group').css('width', '100%').css('width', '-=42px');
	}
	});
//$('.navbar-form .form-group').css('padding-right', '56px').css('width', '93%');
         //focus 

	$( ".srch_input" ).focusout(function() {
		//var form_id = $(this).closest('form').attr('id');
	 	//$( "#"+form_id ).removeClass('active');
		$(this).closest('form').removeClass('active');
	}).focus(function() {
	 //var form_id = $(this).closest('form').attr('id');	
 		//$( "#"+form_id ).addClass('active');
$(this).closest('form').addClass('active');
	});

        $('#header2_lang').click(function(){
            if($('#formLang2').hasClass('headerLang')){
                $('#formLang2').removeClass('headerLang');
            }else{
               $('#formLang2').addClass('headerLang'); 
            }
            
        });
          $('#header3_lang').click(function(){
            if($('#formLang3').hasClass('headerLang')){
                $('#formLang3').removeClass('headerLang');
            }else{
               $('#formLang3').addClass('headerLang'); 
            }
            
        });
        $(document.body).on('click','.delete_notify',function(){
		
		var Id = $(this).next().val();
	var dataid=$(".notifications-list li:last-child").attr("data-id");
		
	
		if($('.js-count').html()){
		$no = $('.js-count').html();
		$count = $('.js-count').html();
		
			
		}else{
		$no=0;$count =0;
		}//console.log($no+'-'+$count);	//alert($count);
		$.post(ajax_url+'Home/delete_notify',{'id':Id,'dataid':dataid}, function(resp){
			console.log(resp);
				$no--;
				$count--;
				
				//console.log('1--'.$no+'-'+$count);
                               // $('.js-count').attr('data-count',$no);
                                //$('.js-count').html($count);	
								
								    if($('.js-count').length == 0 ){
                                                                   $('.js-show-notifications').append('<div class="notifications-count js-count" data-count="'+resp.countN+'"></div>'); 
                                                                   $('li.Notification').find('.js-notifications').removeClass('empty');
                                                                }
                                                                $('.js-count').attr('data-count',resp.countN);
								$('.js-count').html(resp.countN);	
								
                                                               
                                                                //$('ul.notifications-list').append(resp.msg);
				//console.log( $('.js-count').attr('data-count')+'-'+$('.js-count').html());
				if($.trim(resp.process)=='save')
				{	
					
                 $('ul.notifications-list').append(resp.msg);
														 
				}else if($.trim(resp.process)=='last_record')
				{
					return false	
						
				}
				else if($.trim(resp.process)=='last')
				{ 
					$('.js-count').remove();
					$('.js-notifications').addClass('empty');
					$('ul li.Notification button').removeClass('NotifyFull');
					$('ul li.Notification button').removeClass('active');
					/*if(resp.msg==''){
					$('ul.notifications-list').append(resp.msg);}else{
						$('.js-notifications').addClass('empty');
					}
					*/
					
				}	

			
			},'json');
	});
  <?php if($this->Session->read('LocTrain.preference')=='largest'){ ?>
             defaultzommHead('1.2','preference_largest');
     <?php   }else if($this->Session->read('LocTrain.preference')=='large'){ ?>
          defaultzommHead('1.1','preference_large');         
    <?php } else{ ?>
         defaultzommHead('1','preference_normal');         
    <?php } ?>
    });
  
                	var zv1h=0;
function zoom_value_userhead(z_v)
	{
		if(z_v==0)
		{
			return zv1h;
		}
		else
		{
			zv1h = z_v;
		}
	}
 function defaultzommHead(zoom,cls){
        zoom_value_userhead(zoom);
         zoomer_userHead(zoom,cls);		
    }
function zoomer_userHead(zoom,cls)
{
	$('.bodyContentWrapper').css({
            "-moz-transform":"scale("+zoom+")",
            "-webkit-transform":"scale("+zoom+")",
            "-o-transform":"scale("+zoom+")",
            "-ms-transform":"scale("+zoom+")",
			"transform":"scale("+zoom+")",
            
        });
       // alert($('#bodyContentWrapper').attr('style'));
        if(zoom == 1){
          $('.bodyContentWrapper').attr('style','');  
        }
          $('.bodyContentWrapper').attr('class','bodyContentWrapper container');
        $('.bodyContentWrapper').addClass(cls);
}
    function changeLanguage(id,lang,form,field_lang,field_id){
        /*$.ajax({
			url:ajax_url+'Home/change_language/',
			type: 'post',
			data: {'data[Language][id]':id,'data[Language][lang]':lang},
			success:function(resp){
			// location.reload(); 
			}
		});*/
        $('#'+field_lang).val(lang);
        $('#'+field_id).val(id);
        $('#'+form).submit();
        
    }
     $('.show_link').hide();
         $('.openForgotForm').hide();
    function openForgotForm(id){
        $('.show_link').hide();
         $('.openForgotForm').hide();
        $('#openForgotForm_'+id).show();
        $('#show_link_'+id).show();
    }
    function forgot_ajax_form(id,no,model){
        var userEmail = $("#"+id+no).val();
        $('.register_error').html('');
        $.ajax({
			url:ajax_url+'Home/forgot_password/',
			type: 'post',
			data: {'data[Member][email]':userEmail},
			success:function(resp){
			var msg= resp.split('-');
				if(msg[0] == 'sucess'){
                                    $('#'+model).modal('hide');
                                    if(no ==1){
                                         $("#err_email").html("");
                                    }else{
				    $("#err"+no+"_email").html("");
                                    }
					$('.account-activated').html(msg[1]);
					$('.account-activated').show();
					$('.account-activated').fadeOut(10000);
				}else{
					
                                    if(no ==1){
                                    $("#err_email").html(resp);
                                    }else{
				    $("#err"+no+"_email").html(resp);
                                    }
				}
			}
		});	
    }
    
    </script>
    <script>

//console.log( $('.js-count').attr('data-count')+'-'+$('.js-count').html());
	var req; var $number;var $count;//console.log($count)
jQuery(window).load(function () {
setInterval(function(){
		if($('.js-count').html()){
		$no = $('.js-count').html();
		$count = $('.js-count').html();
		
		
		}else{
		$no=0;$count =0;
		}//console.log($count);

				req = $.ajax({
						url:ajax_url+'Home/check_notification/'+$count,
						dataType:"json",
						success:function(resp)
						{ 

						//console.log(resp);
						 if(resp && $.trim(resp.process)=="save")
							{
								$number++

                                                                if($('.js-count').length == 0 ){
                                                                   $('.js-show-notifications').append('<div class="notifications-count js-count" data-count="'+resp.countN+'"></div>'); 
                                                                   $('li.Notification').find('.js-notifications').removeClass('empty');

                                                                }
                                                                $('.js-count').attr('data-count',resp.countN);
								$('.js-count').html(resp.countN);	
								$('.js-count').show();

                                                                $('ul.notifications-list').html('');
                                                                $('ul.notifications-list').append(resp.msg);
								
							}

						}
					});
			
	},30000);

if($('.notifications-count').length==0){
                   $('.show-notifications').addClass('hideNotification');
               }else{
                   $('.show-notifications').removeClass('hideNotification');
               }
   

});
	/*$(document).ready(function(){
		
		
	});*/

</script>
