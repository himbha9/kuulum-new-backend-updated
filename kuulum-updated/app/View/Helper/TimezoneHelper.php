<?php  
class TimeZoneHelper extends AppHelper { 
     
    public $helpers = array('Form'); 
    var $timezones = array( 
    '-12.0' => '(GMT -12:00 hours) Eniwetok, Kwajalein', 
    '-11.0' => '(GMT -11:00 hours) Midway Island, Somoa', 
    '-10.0' => '(GMT -10:00 hours) Hawaii', 
    '-9.0' => '(GMT -9:00 hours) Alaska', 
    '-8.0' => '(GMT -8:00 hours) Pacific Time (US & Canada)', 
    '-7.0' => '(GMT -7:00 hours) Mountain Time (US & Canada)', 
    '-6.0' => '(GMT -6:00 hours) Central Time (US & Canada), Mexico City', 
    '-5.0' => '(GMT -5:00 hours) Eastern Time (US & Canada), Bogota, Lima, Quito', 
    '-4.0' => '(GMT -4:00 hours) Atlantic Time (Canada), Caracas, La Paz', 
    '-3.5' => '(GMT -3:30 hours) Newfoundland', 
    '-3.0' => '(GMT -3:00 hours) Brazil, Buenos Aires, Georgetown', 
    '-2.0' => '(GMT -2:00 hours) Mid-Atlantic', 
    '-1.0' => '(GMT -1:00 hours) Azores, Cape Verde Islands', 
    '0.0' => '(GMT) Western Europe Time, London, Lisbon, Casablanca, Monrovia', 
    '+1.0' => '(GMT +1:00 hours) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris', 
    '+2.0' => '(GMT +2:00 hours) EET(Eastern Europe Time), Kaliningrad, South Africa', 
    '+3.0' => '(GMT +3:00 hours) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi', 
    '+3.5' => '(GMT +3:30 hours) Tehran', 
    '+4.0' => '(GMT +4:00 hours) Abu Dhabi, Muscat, Baku, Tbilisi', 
    '+4.5' => '(GMT +4:30 hours) Kabul', 
    '+5.0' => '(GMT +5:00 hours) Ekaterinburg, Islamabad, Karachi, Tashkent', 
    '+5.5' => '(GMT +5:30 hours) Bombay, Calcutta, Madras, New Delhi', 
    '+6.0' => '(GMT +6:00 hours) Almaty, Dhaka, Colombo', 
    '+7.0' => '(GMT +7:00 hours) Bangkok, Hanoi, Jakarta', 
    '+8.0' => '(GMT +8:00 hours) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei', 
    '+9.0' => '(GMT +9:00 hours) Tokyo, Seoul, Osaka, Sapporo, Yakutsk', 
    '+9.5' => '(GMT +9:30 hours) Adelaide, Darwin', 
    '+10.0' => '(GMT +10:00 hours) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok', 
    '+11.0' => '(GMT +11:00 hours) Magadan, Solomon Islands, New Caledonia', 
    '+12.0' => '(GMT +12:00 hours) Auckland, Wellington, Fiji, Kamchatka, Marshall Island'    
    ); 

    function select($fieldname, $label="Please Choose a timezone") { 
    
        $list = $this->Form->input($fieldname, array("type"=>"select", "label"=>$label, "options"=>$this->timezones, "error"=>"Please choose a timezone")); 
        return $this->output($list); 
    } 

    function display($index) { 
        return $this->output($this->timezones[$index]); 
    } 
    
    function dateAccoringLocale($locale,$date){
       switch ($locale)
        {
           case 'en': return date('d-m-Y',$date);
           case 'de': return date('d.m.Y',$date);
           case 'ar': return date('d/m/Y',$date);
           case 'fr': return date('d/m/Y',$date);
           case 'it': return date('d/m/Y',$date);
           case 'ja': return date('d/m/Y',$date);
           case 'ko': return date('d-m-Y',$date);
           case 'pt': return date('d/m/Y',$date);
           case 'ru': return date('d.m.Y',$date);
           case 'zh': return date('d-m-Y',$date);
           case 'es': return date('d/m/Y',$date);          
           default:return date('d-m-Y',$date);
        }
       
       
    }
    function convertData($locale,$date){
    
       switch ($locale)
        {
           case 'en': return str_replace('-','-',$date);
           case 'de': return str_replace('.','-',$date);
           case 'ar': return str_replace('/','-',$date);
           case 'fr': return str_replace('/','-',$date);
           case 'it': return str_replace('/','-',$date);
           case 'ja': return str_replace('/','-',$date);
           case 'ko': return str_replace('-','-',$date);
           case 'pt': return str_replace('/','-',$date);
           case 'ru': return str_replace('.','-',$date);
           case 'zh': return str_replace('-','-',$date);
           case 'es': return str_replace('/','-',$date);      
           default:return str_replace('-','-',$date);
        }
       
       
    }
    
    function dateFormatAccoringLocale($locale){
        switch ($locale)
        {
           case 'en': return 'yy-mm-dd';
          // case 'de': return 'yy.mm.dd';
           case 'de': return 'dd.mm.yy';
           case 'ar': return 'yy/mm/dd';
           case 'fr': return 'yy/mm/dd';
           case 'it': return 'yy/mm/dd';
           case 'ja': return 'yy/mm/dd';
           case 'ko': return 'yy-mm-dd';
           case 'pt': return 'yy/mm/dd';
           case 'ru': return 'yy.mm.dd';
           case 'zh': return 'yy-mm-dd';
           //case 'es': return 'yy/mm/dd';       
           case 'es': return 'dd/mm/yy';       
           default:return 'yy-mm-dd';
        }
    }
 function dateFormatAccoringLocaleSearch($locale){
        switch ($locale)
        {
           case 'en': return "['YYYY','MM','DD'],separator:'-'";
           case 'de': return "['DD','MM','YYYY'],separator:'.'";
           case 'ar': return "['YYYY','MM','DD'],separator:'/'";
           case 'fr': return "['YYYY','MM','DD'],separator:'/'";
           case 'it': return "['YYYY','MM','DD'],separator:'/'";
           case 'ja': return "['YYYY','MM','DD'],separator:'/'";
           case 'ko': return "['YYYY','MM','DD'],separator:'-'";
           case 'pt': return "['YYYY','MM','DD'],separator:'/'";
           case 'ru': return "['YYYY','MM','DD'],separator:'.'";
           case 'zh': return "['YYYY','MM','DD'],separator:'-'";
           case 'es': return "['DD','MM','YYYY'],separator:'/'";       
           default:return "['YYYY','MM','DD'],separator:'-'";
        }
    }
    
     function dateFormatAccoringLocaleLang($lang_locale,$date){
         $dt = new DateTime(date('Y-m-d H:i:s',strtotime('-0 hours -0 minutes',$date)));
         $dt1=date('Y-m-d H:i:s',$date);
         $formatter = new IntlDateFormatter($lang_locale, IntlDateFormatter::LONG, IntlDateFormatter::SHORT);
         return $formatter->format($dt);       
    }
function dateFormatAccoringLocaleLangNew($lang_locale,$date){
         $dt = new DateTime(date('d F y',strtotime('-0 hours -0 minutes',$date)));
       //  $dt1=date('Y-m-d H:i:s',$date);
         $formatter = new IntlDateFormatter($lang_locale, IntlDateFormatter::LONG, IntlDateFormatter::NONE);	
	$formatter->setPattern($dt);
	//echo  $formatter->getPattern();
//echo '2--'.$formatter->format(0);
         return $formatter->format($dt);       
    }
function dateFormatAccoringLocaleLangNewsMonth($lang_locale,$date){
         $dt = new DateTime(date('d F y',strtotime('-0 hours -0 minutes',$date)));
         $dt1=date('Y-m-d H:i:s',$date);
         $formatter = new IntlDateFormatter($lang_locale, IntlDateFormatter::FULL, IntlDateFormatter::FULL);
	$formatter->setPattern('MMM');
	//echo  $formatter->getPattern();
         return $formatter->format($dt);       
    }
} 
?>
