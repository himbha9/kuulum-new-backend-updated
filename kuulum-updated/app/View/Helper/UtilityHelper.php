<?php  
class UtilityHelper extends AppHelper { 
     
    
    function getCourseRating($m_id,$decoded_id){
        App::import('Model', "Course");  
        $this->Course = new Course();
       // pr($this->Course->CourseRating);



       return $this->Course->CourseRating->find('all',array('conditions'=>array('CourseRating.course_id'=>$decoded_id)));
      
    }
function getCourseSearch($login_m_id,$decoded_id){
        App::import('Model', "Course");  
        $this->Course = new Course();
$contain=array('CourseLesson'=>array('order'=>'CourseLesson.order ASC'),'Subscription_course'=>array('conditions'=>array('Subscription_course.mem_id'=>$login_m_id,'Subscription_course.purchase_id !='=>'','Subscription_course.lesson_id'=>'','Subscription_course.type'=>0)),'Subscription_lesson'=>array('conditions'=>array('Subscription_lesson.mem_id'=>$login_m_id,'Subscription_lesson.purchase_id !='=>'','Subscription_lesson.lesson_id !='=>'','Subscription_lesson.type'=>0)),'CoursePurchaseList'=>array('conditions'=>array('CoursePurchaseList.mem_id'=>$login_m_id)));
       $courses= $this->Course->find('first',array('conditions'=>array('Course.status'=>1,'Course.isPublished'=>1,'Course.id'=>$decoded_id),'fields'=>array('id','date_added','title','description','date_modified','m_id','price','rating','image'),'contain'=>$contain));
      $search_arr = array();
		if(!empty($courses)){
			if(!empty($courses['CourseLesson']))
					{
					foreach($courses['CourseLesson'] as $course_lesson)
						{
							if(!empty($course_lesson['video']))
							{
								$search_arr['video'] = $course_lesson['video'];
							}
							$search_arr['extension'] = $course_lesson['extension'];
							if(!empty($course_lesson['video']))
							{
								
								if(in_array($course_lesson['extension'],array('zip','swf')) && !empty($course_lesson['image']))
								{   
									
									$search_arr['image']= $course_lesson['image'];
								}else if($course_lesson['extension']!='zip' && $course_lesson['extension']!='swf') 
								{
									$search_arr['image']= $course_lesson['video'];
                                }else if(in_array($course_lesson['extension'],array('zip','swf')) && empty($course_lesson['image']))
								{
									$search_arr['image'] = '';
								}								
								break;
							}
							else
							{
								$search_arr['image'] = '';
								//break;
							}
						}

				}else
					{
						$search_arr['image'] = '';
						$search_arr['extension'] ='';
					}
					
					$search_arr['date_modified'] = $courses['Course']['date_modified'];
					$search_arr['CoursePurchaseList'] = $courses['CoursePurchaseList'];
					$search_arr['CourseLesson'] = $courses['CourseLesson'];
					$search_arr['Subscription_course'] = $courses['Subscription_course'];
					$search_arr['Subscription_lesson'] = $courses['Subscription_lesson'];
					$search_arr['Course'] = $courses['Course'];
		}
	return $search_arr;
    }
  function getCoursesubscription($m_id,$decoded_id,$first_lesson){
        App::import('Model', "CoursePurchaseList");
         $this->CoursePurchaseList = new CoursePurchaseList();
$this->CoursePurchaseList->unbindModel(
        array('belongsTo'=>array('Course','CoursePurchase','Member','CourseLesson'))

    );
        return  $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.lesson_id'=>@$first_lesson,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0),array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)))));
    }
function getCoursesubscriptionIndex($m_id,$decoded_id,$first_lesson,$purchase_list){
        
$list=array();
	if(!empty($purchase_list)){
			foreach($purchase_list as $purchase_list){
				if($decoded_id==$purchase_list['c_id'] ||$first_lesson ==$purchase_list['lesson_id']){
			$list[]=$purchase_list;
				}
			}
		}

return $list;

    }
function getCoursesubscription_price_old($m_id,$decoded_id,$cookie_lessons,$first_lesson){
        App::import('Model', "CourseLesson");
         $this->CourseLesson = new CourseLesson();
$this->CourseLesson->recursive=-1;			
        
$subscription=array();$count_lesson=0;
			if(!empty($first_lesson)){ $count_lesson=count($first_lesson);

			foreach($first_lesson as $first_lesson){$this->CoursePurchaseList->unbindModel(
        array('belongsTo'=>array('Course','CoursePurchase','Member','CourseLesson'))

    );
$subscription_old = $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.lesson_id'=>@$first_lesson['id'],'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0),array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)))));
						if(!empty($subscription_old)){
							$subscription[]=$subscription_old;
							}else{
 			$less_subscription= $this->getLessonSubscription($m_id,@$first_lesson['Course']['id'],@$first_lesson['CourseLesson']['id']);
		if(!empty($less_subscription)){
		$subscription[]=$less_subscription;
		}else {
		if(in_array(@$first_lesson['CourseLesson'],$cookie_lessons)){
		$subscription[]=$less_subscription;
		}
		}
}
						}
			}
//echo $count_lesson.'-'.count($subscription);
if($count_lesson==count($subscription)){
return true;
}else{
return false;
}

    }
function getCoursesubscription_price($m_id,$decoded_id,$cookie_lessons){
        App::import('Model', "CourseLesson");
         $this->CourseLesson = new CourseLesson();			
        $first_lesson=$this->CourseLesson->find('all',array('conditions'=>array('CourseLesson.status'=>1,'CourseLesson.course_id'=>$decoded_id),'order'=>array('CourseLesson.date_added ASC')));
$subscription=array();$count_lesson=0;
			if(!empty($first_lesson)){ $count_lesson=count($first_lesson);
				foreach($first_lesson as $first_lesson){
						$subscription_old = $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.lesson_id'=>@$first_lesson['CourseLesson']['id'],'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0),array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.status'=>0)))));//print_r($subscription_old);
						if(!empty($subscription_old)){
							$subscription[]=$subscription_old;
							}else{
 			$less_subscription= $this->getLessonSubscription($m_id,@$first_lesson['Course']['id'],@$first_lesson['CourseLesson']['id']);
		if(!empty($less_subscription)){
		$subscription[]=$less_subscription;
		}else {
		if(in_array(@$first_lesson['CourseLesson'],$cookie_lessons)){
		$subscription[]=$less_subscription;
		}
		}
}
						}
			}
//echo $count_lesson.'-'.count($subscription);
if($count_lesson==count($subscription)){
return true;
}else{
return false;
}

    }

function getCoursesubscription_priceLeasson($CoursePurchaseList,$CourseLesson,$cookie_lessons){
$subscription_lessions=array();$count_lesson=0;
$purcahse_list=$CoursePurchaseList;//print_r($purcahse_list);
if(!empty($CourseLesson)){
$count_lesson=count($CourseLesson);
	if(!empty($purcahse_list)){
	foreach($purcahse_list as $purcahse_list){
	$subscription_lessions[]=$purcahse_list;
	}
	}else{
	foreach($CourseLesson as $lesson){
		if(in_array($lesson['id'],$cookie_lessons)){
		$subscription_lessions[]=$lesson;
		}
	}

	}	
}
if($count_lesson==count($subscription_lessions)){
return true;
}else{
return false;
}
}
function getcookie_courses($m_id){
App::import('Model', "PurchaseItem");
 $this->PurchaseItem = new PurchaseItem();	
 $getCourses=array();
if($m_id){
                    $order_ids_mem=array();
            $get_array = $this->PurchaseItem->find('all',array('conditions'=>array('PurchaseItem.m_id'=>$m_id),'contain'=>array('PurchaseItem'=>array('fields'=>array('item_name','item_val')))));
                                    if(!empty($get_array)){
                                    foreach($get_array as $row){

                                    $order_ids_mem[$row['PurchaseItem']['item_name']]=$row['PurchaseItem']['item_value'];
                                    }
                                    } 
$getCourses = $order_ids_mem;//echo '<pre>';print_r($get_array);
}else{
App::uses('CakeSession', 'Model/Datasource');
//print_r(CakeSession::read('LocTrainPurchase'));
	$getCourses = CakeSession::read('LocTrainPurchase');

		
}
$cookie_courses = array();
		if(!empty($getCourses)){
			foreach($getCourses as $course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
			}
			//pr($cookie_courses); die;
		}

return $cookie_courses;
}
function getCourseLearningStatusIndex($m_id,$decoded_id,$purchase_list){

$list=array();
	if(!empty($purchase_list)){
			foreach($purchase_list as $purchase_list){
				if($decoded_id==$purchase_list['c_id']){
			$list[]=$purchase_list;
				}
			}
		}

return $list;
}
function getCourseLearningStatus($m_id,$decoded_id){

 App::import('Model', "CoursePurchaseList");
         $this->CoursePurchaseList = new CoursePurchaseList();
//$this->CoursePurchaseList->find('first',array('conditions'=>array('CoursePurchaseList.c_id'=>$course_id,,'CoursePurchaseList.status'=>0)));
$this->CoursePurchaseList->unbindModel(
        array('belongsTo'=>array('Course','CoursePurchase','Member','CourseLesson'))

    );
   return $this->CoursePurchaseList->find('first',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0),array('CoursePurchaseList.c_id'=>$decoded_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$m_id,'CoursePurchaseList.type'=>0)))));
}
    function getCoursesubscribed($m_id,$decoded_id){
         App::import('Model', "Member");
         $this->Member = new Member();
         $date = date("Y-m-d H:i:s");
   return $this->Member->MemberSubscription->find('count',array('conditions'=>array('MemberSubscription.m_id'=>$m_id, 'MemberSubscription.txn_id !='=>'','MemberSubscription.status'=>1, 'MemberSubscription.date_expired >='=>strtotime($date))));
    }
    function getFirstLesson($decoded_id){
        App::import('Model', "CourseLesson");
         $this->CourseLesson = new CourseLesson();
$this->CourseLesson->recursive=-1;
        return $this->CourseLesson->find('first',array('conditions'=>array('CourseLesson.status'=>1,'CourseLesson.course_id'=>$decoded_id),'order'=>array('CourseLesson.date_added ASC'),'limit'=>1));
    }
    function getLessonSubscription($login_mem_id,$course_id,$lesson_id){
        App::import('Model', "CoursePurchaseList");
         $this->CoursePurchaseList = new CoursePurchaseList();
$this->CoursePurchaseList->unbindModel(
        array('belongsTo'=>array('Course','CoursePurchase','Member','CourseLesson'))

    );
       return $this->CoursePurchaseList->find('all',array('conditions'=>array('OR'=>array(array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>$lesson_id,'CoursePurchaseList.mem_id'=>$login_mem_id,'CoursePurchaseList.type'=>0),array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.lesson_id'=>NULL,'CoursePurchaseList.mem_id'=>$login_mem_id,'CoursePurchaseList.type'=>0)))));
        
    }
	
	
	  function getCourselearningRating($m_id,$decoded_id){
        App::import('Model', "Course");  
        $this->Course = new Course();
       // pr($this->Course->CourseRating);


       return  $this->Course->CourseRating->find('first',array('conditions'=>array('CourseRating.member_id'=>$m_id,'CourseRating.course_id'=>$decoded_id)));
      
    }
	
	
	function commissions(){
		 App::import('Model', "Commission");  
		  $this->Commission = new Commission();
		 return  $this->Commission->find('all');
		}
		
		function getpurchaselistexistence($course_id,$m_id){
		App::import('Model', "CoursePurchaseList");  
		$this->CoursePurchaseList = new CoursePurchaseList();
		return $this->CoursePurchaseList->find('all',array('conditions'=>array('CoursePurchaseList.c_id'=>$course_id,'CoursePurchaseList.type'=>0,'CoursePurchaseList.mem_id'=>$m_id),'order'=>array('CoursePurchaseList.id DESC'),'limit'=>1));
		  
			}
			
			function getAuthorname($m_id){
		   App::import('Model', "Member");
         $this->Member = new Member();
		 return  $this->Member->find('first',array('conditions'=>array('Member.id'=>$m_id),'fields'=>array('given_name')));
		}
		function getCoursename($c_id){
		   App::import('Model', "Course");
         $this->Course = new Course();
		 return  $this->Course->find('first',array('conditions'=>array('Course.id'=>$c_id),'fields'=>array('title')));
		}
	function externlizePrice($language,$locale,$price){
	  foreach($language as $lang) {if($lang['Language']['locale']==$locale){  $current_loc= $lang['Language']['lang_code'];}}
		$fmt = new NumberFormatter( $current_loc, NumberFormatter::DECIMAL_ALWAYS_SHOWN );
		//return  $fmt->format($price);
		return str_replace('US','',$fmt->formatCurrency($price, 'USD'));
	  }  
	  
	  function externlizePriceWithoutCurrencySign($language,$locale,$price){
	  foreach($language as $lang) {if($lang['Language']['locale']==$locale){  $current_loc= $lang['Language']['lang_code'];}}
		$fmt = new NumberFormatter( $current_loc, NumberFormatter::DECIMAL_ALWAYS_SHOWN );
		return trim(str_replace(array('$','€','¤'),array('','',''),$fmt->format($price)));
		
	  }

		function bucketsCloudFront($bucket){
			/*$buckets = array(
				'dev-kuulum-uploaded-media' => 'd3c0u8eauytko.cloudfront.net',
				'dev-kuulum-members' => 'd2y0jikem0oaqr.cloudfront.net',
				'dev-kuulum-news' => 'd1banggjot84vi.cloudfront.net',
				'dev-kuulum-lessons' => 'dwmp668t2ux90.cloudfront.net',
				'dev-kuulum-course-lesson' => 'd81cqbq1fesz.cloudfront.net',
				'dev-kuulum-banners' => 'd3qykfcesg88t1.cloudfront.net',
				'dev-kuulum-courses' => 'dtyfjbvakfxt7.cloudfront.net',
				'dev-kuulum-static-content' => 'dmmnknitg2mi7.cloudfront.net'
			);*/
			$buckets = array(
				UPLOADED_MEDIA_BUCKET => UPLOADED_MEDIA_BUCKET_CDN,
				MEMBERS_BUCKET => MEMBERS_BUCKET_CDN,
				NEWS_BUCKET => NEWS_BUCKET_CDN,
				LESSON_VIDEOS_BUCKET => LESSON_VIDEOS_BUCKET_CDN,
				LESSON_BUCKET => LESSON_BUCKET_CDN,
				BANNER_BUCKET => BANNER_BUCKET_CDN,
				COURSES_BUCKET => COURSES_BUCKET_CDN,
				STATIC_BUCKET => STATIC_BUCKET_CDN
			);
			if(!empty($bucket)){
				return $buckets[$bucket];
			}else{
				return $buckets;
			}
		}

		function getAWSImgUrl($bucket, $key){
			App::import('Component', 'Aws');
		    	$this->Aws = new AwsComponent();
		    	$fileURL = $this->Aws->getFileURL($bucket, $key);
			if(IS_CLOUDFRONT == '1'){
				$cdnURL = $this->bucketsCloudFront($bucket);
			// http://dev-kuulum-banners.s3.amazonaws.com/zh_ZH/1425726654-curious-1140x272-8bit.gif
				$cdnURL = preg_replace("/^(https:\/\/)([^\/]+)(.*)$/", "$1$cdnURL$3", $fileURL);
				return $cdnURL;
			}else{
				return $fileURL;
			}
		}
  
	/*function getAWSImgUrl($bucket, $key){
	App::import('Component', 'Aws');
    	$this->Aws = new AwsComponent();
	return  $this->Aws->getFileURL($bucket, $key);
	}*/
	function getAWSDisplayUrl($bucket,$key){
		if($key){
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();
		$url=  $this->Aws->getFileURL($bucket, $key);		
		
		if(!empty($url))
		{
		return   $file_exists = true;
		} else {
		return   $file_exists = false;
		}
		}else{
		return $file_exists = false;
		}
		//return $file_exists;
	}
function getAWSUrl($bucket,$key){
		if($key){
		App::import('Component', 'Aws');
		$this->Aws = new AwsComponent();
		$url=  $this->Aws->getFileURL($bucket, $key);			
		return $url;
}else{
return false;
}
	}
	function getAWSFileUrl($url){


//	    $fp = @fsockopen($url, 80);
//	    if($fp === false) {
if (@fopen($url, "r")) {
 return   $url;
		}else{
			return false;
		}  
		//return $file_exists;
	}


	function getbanners($locale){ 
        App::import('Model', "Banners");
         $this->Banners = new Banners();
        return  $this->Banners->find('all',array('conditions'=>array('locale'=>$locale),'order'=>'order desc'));
    }
    
    function getbannerdefaults($locale){ 
        App::import('Model', "Bannerdefaults");
         $this->Bannerdefaults = new Bannerdefaults();
        return  $this->Bannerdefaults->find('first',array('conditions'=>array('language'=>$locale),'fields'=>'default'));
    }
    function manage_metadata_info($website_url){
        App::import('Model', "MetaTag");
        $this->MetaTag = new MetaTag();
        return  $this->MetaTag->find('first',array('conditions'=>array('MetaTag.page_url'=>$website_url)));
    }
    
    function manage_metadata_default_info(){
        App::import('Model', "MetatagSetting");
        $this->MetatagSetting = new MetatagSetting();
        return  $this->MetatagSetting->find('first',array('conditions'=>array(),'limit'=>'1','order'=>'MetatagSetting.id DESC'));
    }
         function getCoursekeywords($id){
             App::import('Model', "Course");  
            $this->Course = new Course();
            $encoded_id = @convert_uudecode(base64_decode($id));
            return $this->Course->CourseKeyword->find('first',array('conditions'=>array('CourseKeyword.course_id'=>$encoded_id),'fields'=>array('CourseKeyword.course_id','CourseKeyword.keyword')));
         }
         
         function getCourseMetaInfo($id){
             App::import('Model', "Course");  
            $this->Course = new Course();
            $encoded_id = @convert_uudecode(base64_decode($id));
            return $this->Course->find('first',array('conditions'=>array('Course.id'=>$encoded_id),'contain'=>array('CourseKeyword'),'fields'=>array('Course.id','Course.meta_title','Course.meta_description','Course.meta_keyword')));
         }
         function pageinfo($current_page_url,$locale){
            $parse = parse_url($current_page_url);
            $replace_str=$parse['host'];
            $website_url=str_replace(array('https://'.$replace_str.'/kuulum/','http://'.$replace_str.'/kuulum/'),array('',''),$current_page_url);
            $meta_title=$meta_keywords=$meta_desc='';   
            $url_meta_info = $this->manage_metadata_info($website_url);            
            
            $default_meta_info = $this->manage_metadata_default_info();
            if (strpos($website_url,'view_course_details') !== false || strpos($website_url,'course_edit') !== false) {
                //meta tags for courses
                $id=str_replace(array('Home/view_course_details/','Members/course_edit/'),array('',''),$website_url);
                $course_info = $this->getCourseMetaInfo($id); 
               
               if(!empty($course_info['Course']['meta_title'])){
                   $meta_title=$course_info['Course']['meta_title'];}
                   else if(!empty($url_meta_info['MetaTag']['meta_title_'.$locale])){
                    $meta_title=$url_meta_info['MetaTag']['meta_title_'.$locale];
                   }else{
                    $meta_title=$default_meta_info['MetatagSetting']['meta_title_'.$locale];               
                   }
                   if(!empty($course_info['Course']['meta_keyword'])){
                    $meta_keywords=$course_info['Course']['meta_keyword'];
                   }else if(!empty($course_info['CourseKeyword'])){
                       $keywords=array();
                       foreach($course_info['CourseKeyword'] as $row){
                          $keywords[]=$row['keyword'];
                       }
                       $meta_keywords=implode(',',$keywords);
                   }else if(!empty($url_meta_info['MetaTag']['meta_keywords_'.$locale])){
                       $meta_keywords=$url_meta_info['MetaTag']['meta_keywords_'.$locale];
                   }else{
                      $meta_keywords=$default_meta_info['MetatagSetting']['meta_keywords_'.$locale]; 
                   }
                    if(!empty($course_info['Course']['meta_description'])){
                    $meta_desc=$course_info['Course']['meta_description'];
                    }else if(!empty($url_meta_info['MetaTag']['meta_description_'.$locale])){
                    $meta_desc=$url_meta_info['MetaTag']['meta_description_'.$locale]; 
                    }else{
                    $meta_desc=$default_meta_info['MetatagSetting']['meta_description_'.$locale];
                    }
            }else{                               
                if($url_meta_info){
                    if(!empty($url_meta_info['MetaTag']['meta_title_'.$locale])){
                        $meta_title=$url_meta_info['MetaTag']['meta_title_'.$locale];
                    }else{
                        $meta_title=$default_meta_info['MetatagSetting']['meta_title_'.$locale];
                    }
                    if(!empty($url_meta_info['MetaTag']['meta_keywords_'.$locale])){
                    $meta_keywords=$url_meta_info['MetaTag']['meta_keywords_'.$locale];
                    }else{
                    $meta_keywords=$default_meta_info['MetatagSetting']['meta_keywords_'.$locale];
                    }
                    if(!empty($url_meta_info['MetaTag']['meta_description_'.$locale])){
                    $meta_desc=$url_meta_info['MetaTag']['meta_description_'.$locale];
                    }else{
                        $meta_desc=$default_meta_info['MetatagSetting']['meta_description_'.$locale];
                    }
                }else {
                    
                    $meta_title=$default_meta_info['MetatagSetting']['meta_title_'.$locale];               
                    $meta_desc=$default_meta_info['MetatagSetting']['meta_description_'.$locale];
                     //check url is of course or not
                    if (strpos($website_url,'view_course_details') !== false) {
                        $id=str_replace('Home/view_course_details/','',$website_url);
                        $course_info = $this->getCoursekeywords($id);                    
                        if(!empty($course_info['CourseKeyword']['keyword'])){
                           $meta_keywords=$course_info['CourseKeyword']['keyword']; 
                        }else{
                           $meta_keywords=$default_meta_info['MetatagSetting']['meta_keywords_'.$locale];
                        }
                    }else{
                         $meta_keywords=$default_meta_info['MetatagSetting']['meta_keywords_'.$locale];
                    }
                }
                
            }
            return array('meta_title'=>$meta_title,'meta_desc'=>$meta_desc,'meta_keywords'=>$meta_keywords);
        }
} 
?>

