<?php echo '';?>
<!-- main container section -->

    <section>
		<div class="container InnerContent CMS_Common_Pages">
        	<div class="row">
            	<div class="col-lg-12">
                	<h4 class="modal-title"><?php echo $about_refund['CmsPage']['page_title']; ?></h4>
                   <?php echo $about_refund['CmsPage']['description']; ?>
                                        
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-6 Wrapper_Return_To">
                    <a href="javascript:void(0);" class="return_to">Return to top</a>                    
                </div>
                <div class="col-xs-6 Wrapper_Addthis">
	      <!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
						<a class="addthis_button_email eml_bg"></a>						
						<a style="margin-left:10px;" class="addthis_button_print print_bg"></a>
						<a style="margin-left:10px;" class="addthis_button_compact share_bg"></a> 
						<!--<a class="addthis_button_preferred_2"></a><a class="addthis_button_preferred_3"></a>-->
						<!--<a class="addthis_counter addthis_bubble_style"></a>-->
						 </div>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-51e508fe2ccf7da5"></script>
						<!-- AddThis Button END -->
                                </div>
            </div>
            
        </div>    
      
    </section>

<!--End  main container section -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.return_to').on('click',function(){
			$('body,html').animate({
					scrollTop: 0
				}, 500);
				return false;
		})
	});
        function PrintDiv()
 {
	 window.open('<?php echo HTTP_ROOT?>Home/print_content/<?php echo base64_encode(convert_uuencode($cookie_policy['CmsPage']['id'])); ?>', '_blank', 'width=700,height=700');
 }   

	
</script>