<?php echo $this->Html->css('autoSuggest');?>
<?php echo $this->Html->script('jquery.autoSuggest');?>

<script type="text/javascript">
$(document).ready(function(){
	

	
var array = {'emailId':'<?php echo __("you_will_use_your_e-mail_address_to_sign-in_next_time");?>','confirmEmailId':'<?php echo __("please_confirm_your_e-mail_address_you_will_use_your_e-mail_address_to_sign-in_next_time");?>','password':'<div class="password_instructions"><div class="password_instruction_inner"><?php echo __("please_choose_a_password_consisting_of");?><br/><br/></div><ul class="instruction_ul"><li style="color:#A5A5A5;"><span class="length invalid"><?php echo __("a_minimum_of_8_and_maximum_of_20_characters");?></span></li><li style="color:#A5A5A5;"><span class="upperCase invalid"><?php echo __("at_least_one_upper_case_character");?></span></li><li style="color:#A5A5A5;"><span class="lowerCase invalid"><?php echo __("at_least_one_lower_case_character");?></span></li><li style="color:#A5A5A5;"><span class="number invalid"><?php echo __("at_least_one_number");?></span></li><li style="color:#A5A5A5;"><span class="alpha invalid"><?php echo __("at_least_one_non_alphanumeric_character");?></sapn></li></ul></div>','confirmPassword':'<?php echo __("please_type_your_password_again_to_confirm");?>','givenName':'<?php echo __("a_name_that_distinguishes_you_from_other_family_members");?>','familyName':'<?php echo __("a_name_that_distinguishes_you_from_other_people_not_in_your_family");?>','otherName':'<?php echo __("do_you_have_any_other_names_after_your_surname_that_are_important_to_you");?>','finalName':'<?php echo __("a_name_that_distinguishes_you_from_other_people_not_in_your_family");?>','interest':'<?php echo __("enter_important_keywords_that_describe_the_topics_you_are_interested_in_learning_we_may_use_these_to_let_you_know_about_new_courses");?>','Notification1':'<?php echo __("would_you_like_to_receive_news_or_information_about_new_courses_when_they_become_available");?>',};
	$(':input').live('focus',function(){
		var getId = $(this).attr('id');
		switch(getId)
		{
			case 'emailId':				
				$(".guide_info").text(array['emailId']);
			break;
			case 'confirmEmailId':				
				$(".guide_info").text(array['confirmEmailId']);
			break;
			case 'password':				
				$(".guide_info").html(array['password']);
				checkVal();
			break;
			case 'confirmPassword':				
				$(".guide_info").text(array['confirmPassword']);
			break;
			case 'givenName':				
				$(".guide_info").text(array['givenName']);
			break;
			case 'familyName':				
				$(".guide_info").text(array['familyName']);
			break;
			case 'otherName':				
				$(".guide_info").text(array['otherName']);
			break;
			case 'finalName':				
				$(".guide_info").text(array['finalName']);
			break;			
		}				
	});
	$('.as-input').live('focus',function(){
		$(".guide_info").text(array['interest']);
	});
	$('#password').live('keyup',function(){ checkVal();	});
	
	$('#emailId').live('focusout',function(){ 	
		var msg = '';
		var inputValEmail = $('#emailId').val();		
		var inputValConEmail = $('#confirmEmailId').val();	
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo FIELD_REQUIRED;?>';
			$("#err_email").text(msg);
		}
		else {
			$("#err_email").text('');
		}
		if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo FIELD_REQUIRED;?>';
			$("#err_email").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		if((inputValEmail != '' && inputValConEmail != '') && (inputValEmail != inputValConEmail)){
			msg = '<?php echo EMAIL_MISMATCH;?>';
			$("#err_confirm_email").text(msg);
		}
		else {
			$("#err_confirm_email").text('');
		}
		
	});
	$('#confirmEmailId').live('focusout',function(){ 	
		var msg = '';
		var inputValEmail = $('#emailId').val();	
		var inputValConEmail = $('#confirmEmailId').val();		
		 if(inputValConEmail == ''){
			  msg = '<?php echo FIELD_REQUIRED;?>';
		  	  $("#err_confirm_email").text(msg);
		  }		
		 else if((inputValEmail != '') && (inputValEmail != inputValConEmail)){
			  msg = '<?php echo EMAIL_MISMATCH;?>';
			  $("#err_confirm_email").text(msg);
		  }
		  else {
			$("#err_confirm_email").text('');
		  }		 
		    
	});
	
	$('#password').live('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		if(inputValPass == ''){
			msg = '<?php echo FIELD_REQUIRED;?>';
			$("#err_password").text(msg);
		}
		else {
			$("#err_password").text('');
		}
		if((inputValPass != '' && inputValConPass != '') && (inputValPass != inputValConPass)){
			msg = '<?php echo PASSWORD_MISTACHMATCH;?>';
			$("#err_confirm_password").text(msg);
		}
		else {
			$("#err_confirm_password").text('');
		}
		  
		  
	});
	$('#confirmPassword').live('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		  if(inputValConPass == ''){
			  msg = '<?php echo FIELD_REQUIRED;?>';
		  	  $("#err_confirm_password").text(msg);
		  }
		 else if((inputValPass != '') && (inputValPass != inputValConPass)){
			  msg = '<?php echo PASSWORD_MISTACHMATCH;?>';
			  $("#err_confirm_password").text(msg);
		  }
		  else {
			$("#err_confirm_password").text('');
		  }
	});
	
	$('#finalName').live('focusout',function(){ 	
		var msg = '';
		var finalNameVal = $('#finalName').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo FIELD_REQUIRED;?>';
		  	  $("#err_final_name").text(msg);
		  }
		  else {
			$("#err_final_name").text('');
		  }
	});
	$('.rdo_yesno').live('change',function(){ 			
		var inputVal = $(this).val();
		 if(inputVal != '1' && inputVal != '2'){
			  msg = '<?php echo FIELD_REQUIRED;?>';
		  	  $("#err_notification").text(msg);
		 }	
		 else{
			 $("#err_notification").text('');
		  }		
	});
	$('.accept_chk').live('change',function(){ 			
		var inputVal = $(this).val();
		 if(inputVal != '1' && inputVal != '2'){
			  msg = '<?php echo FIELD_REQUIRED;?>';
		  	  $("#err_term_condition").text(msg);
		 }	
		 else{
			 $("#err_term_condition").text('');
		  }		
	});
	
	$("#termNcondition").click(function(){
		window.open(ajax_url+'Home/terms',"","menubar=0,location=0,height=500,width=900,scrollbars=1,resizable=1" );	
	});
	
	$(".radio_image_no").live("click",function(){
		$(".guide_info").text(array['Notification1']);
		var getId = $(this).attr('id');	
		$(".radio_image_no").removeClass('rdo_chkd');
		$(this).addClass('rdo_chkd');
		if($.trim(getId) == 'yes'){
			$("#notificationInput").val('1');
		}else{
			$("#notificationInput").val('0');
		}		
	});	
	$(".term_chk_box").live("click",function(){
		var getId = $("#termConditionInput").val();
		if(getId == '1'){
			$(this).css({'background-position':'0 0px'});
			$("#termConditionInput").val('0');
		}
		else if(getId == '0'){
			$(this).css({'background-position':'0 -30px'});			
			$("#termConditionInput").val('1');
		}
	});		
});

function checkVal()
{
	if($('#password').val().length < 8 || $('#password').val().length > 20){
	$(".length").removeClass("valid").addClass('invalid');
	}else{
		$(".length").removeClass("invalid").addClass('valid');
	}
	
	if($('#password').val().match(/[A-Z]/)){
		$(".upperCase").removeClass("invalid").addClass('valid');
	}else{
		$(".upperCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#password').val().match(/[a-z]/)){
		$(".lowerCase").removeClass("invalid").addClass('valid');
	}else{
		$(".lowerCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#password').val().match(/[0-9]/)){
		$(".number").removeClass("invalid").addClass('valid');
	}else{
		$(".number").removeClass("valid").addClass('invalid');
	}
	
	if($('#password').val().match(/[!@#$%^&*()\-_=+{};:,<.>~]/)){
		$(".alpha").removeClass("invalid").addClass('valid');
	}else{
		$(".alpha").removeClass("valid").addClass('invalid');
	}
}
</script>

<div id="wrapper"> 
  <div id="middle">
    <div class="breadcrumb_botcont">
      <?php echo $this->Form->create('Member',array('id'=>'memberRegister','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'register')));?>
          <div class="formfld_cont">
          	<div class="inner_parafrm">
            	<p>
                	Because the safekeeping of your details are very important to us we need to you to answer a few questions about yourself before you can reset your password. We do this to confirm that this is indeed your account. When you have finished, please select Verify my answers.
                </p>
            </div>
            <div class="reg_flds">
              <label class="regfld_label"><span class="mandatory">*</span> <?php echo __('what_should_we_call_you');?></label>
              <span class="regfld_txtcont">
                <?php echo $this->Form->input('email',array('type'=>'text','id'=>'emailId','class'=>'regfld_txt gradient','label'=>false,'div'=>false)); ?> 
                <div id="err_email" class="register_error"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>         
              </span> 
            </div>
            <div class="reg_flds">
              <label class="regfld_label"><span class="mandatory">*</span> <?php echo __('what_is_your_forename');?></label>
              <span class="regfld_txtcont">
                <?php echo $this->Form->input('confirm_email',array('type'=>'text','id'=>'confirmEmailId','class'=>'regfld_txt gradient','label'=>false,'div'=>false)); ?>
                <div id="err_confirm_email" class="register_error"> <?php if(isset($error['confirm_email'][0])) echo $error['confirm_email'][0];?> </div> 
              </span>
            </div>            
          </div>
          <div class="formfld_rgt">
            <div class="section_guide"> <span class="guide_upper"> <?php echo __('in_this_section');?> </span>
              <h2 class="guide_head"> <?php echo __('member_verification'); ?> </h2>
              <div class="guide_info"> <?php echo __('enter_your_answer');?>  </div>
              <div class="guide_bot"> <span class="option"> <?php echo __('options');?></span> 
              	<?php echo $this->element('frontElements/home/reset_password'); ?>  
              	<?php echo $this->Html->link(__('reset_password'),'#reset-password-box',array('class'=>'login-window scn_link scn_margn')); ?> 
                <?php echo $this->Form->submit(__('verify_my_answers'),array('class'=>'sc_btns disRegisBut','div'=>'section_links','onclick'=>"return ajax_form('memberRegister','Home/validate_register_ajax','load_top','disRegisBut')")); ?>
                <?php echo $this->Form->submit(__('cancel'),array('class'=>'sc_btns disRegisBut', 'div'=>'section_links', 'onclick'=>'memberRegister.action="'. HTTP_ROOT.'Home/index"')); ?>
              </div>
            </div>
          </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('a.login-window').click(function() {
		
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').live('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});

</script>
