<?php echo $this->Html->script('Scripts/jquery.msgBox.js');?>
<?php echo $this->Html->css('front/msgBoxLight.css');
$lang=$this->Session->read('LocTrain.locale');
//$type='purchaser';
?>
<script type="text/javascript">
    $('document').ready(function(){
$('.chk_reason').on('click',function(){
 	if($(this).is(':checked'))
	{
		$(this).val('1');
	}else{
		$(this).val('0');
	}
});

        $('#do_not_cancel').click(function(){
             $(".disRegisBut").attr('disabled',false);
            $('#myModal19').modal('hide');
        });
        
        	
        $("#membercancel").submit(function()
        {   	
            $('#myModal19').modal('show');
            return false;
        });
    });
    function yes_cancel(opt){
        											
        if(opt=='author'){
            var tempValues = {};
                        $("input.chk_reason").each(function(){
                        var th= $(this);
                        tempValues[th.attr('value')] = th.val();
            });
            var text_data_second;
            text_data_second=$("#givenName").val();

            var Id = $("#membercancel").attr('rel');
            $("#membercancel").keydown(function(event) {
                    if(event.keyCode ==27)
                    {
                            $('.msgBox').fadeOut().remove();
                            $('.msgBoxBackGround').fadeOut(300).remove();
                    }
            });
            $(".load_top").hide();
            <?php $root=HTTP_ROOT; ?>
													
            req = $.ajax({
                    url:ajax_url+'Home/author_cancel_account',
                    type: 'get',
            data: {'id':Id,'resn':tempValues,'text_data_second':text_data_second},
                    success: function(resp){					
                            $(".disRegisBut").attr('disabled',false);
                            if($.trim(resp)=='done')
                            {  

                                    window.location.href = ajax_url+'Home/index';

                            }

                    }


            });
											
										
        }else if (opt == 'subscriber'){
            var tempValues = {};
                $("input.chk_reason").each(function(){
                var th= $(this);
                tempValues[th.attr('value')] = th.val();
                });
                var text_data;
                text_data=$("#givenName").val();

                 var Id = $("#membercancel").attr('rel');
                $("#membercancel").keydown(function(event) {
                        if(event.keyCode ==27)
                        {
                                $('.msgBox').fadeOut().remove();
                                $('.msgBoxBackGround').fadeOut(300).remove();
                        }
                });
                $(".load_top").hide();
                req = $.ajax({

                url:ajax_url+'Home/expire_subscription',
                type: 'get',
                data: {'id':Id,'resn':tempValues,'text_data':text_data},
                success: function(resp){					
                        $(".disRegisBut").attr('disabled',false);
                        if($.trim(resp)=='done')
                        {  
                                window.location.href = ajax_url+'Home/index';

                        }

                }
        });
							
		
        }else if (opt == 'purchaser'){
            var tempValues = {};
						$("input.chk_reason").each(function(){
						var th= $(this);
						tempValues[th.attr('value')] = th.val();
						
					});
						var text_data_first;
						text_data_first=$("#givenName").val();
						
						var Id = $("#membercancel").attr('rel');
						
						
						$("#membercancel").keydown(function(event) {
							if(event.keyCode ==27)
							{
								$('.msgBox').fadeOut().remove();
								$('.msgBoxBackGround').fadeOut(300).remove();
							}
						});
						$(".load_top").hide();
						<?php $root=HTTP_ROOT; ?>
						req = $.ajax({
											
                                                url:ajax_url+'Home/purchaser_cancel_account',
                                                type: 'get',
                                                data: {'id':Id,'resn':tempValues,'text_data_first':text_data_first},
                                                success: function(resp){					
                                                        $(".disRegisBut").attr('disabled',false);
                                                        if($.trim(resp)=='done')
                                                        {  

                                                                window.location.href = ajax_url+'Home/index';

                                                        }

                                                }


                                        });

    }
    }
    
	
	
</script>
<?php 


$locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en'; ?>
<section class="mainContent">
  <div class="container InnerContent">
      <?php if($this->Session->read("LocTrain.flashMsg") != ''){ ?>
    	<div class="account-activated">	<?php echo $this->Session->read("LocTrain.flashMsg"); ?></div>
	<?php $this->Session->delete("LocTrain.flashMsg"); }  ?>
         <?php echo $this->Form->create('CancelReason',array('id'=>'membercancel','rel'=>$mid,'div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'cancel_account')));?>
    <div class="row">
    
    <div class="col-lg-12">
    				
        <div class="modal-title"><?php echo __('we_re_sad_to_see_you_go');?></div><br>
        <p><?php echo __('but_before_you_do_go_please_let_us_know_why_you_want_to_leave');?></p><br>
   <?php  
                if(!empty($reasons))
                {
                        $j = count($reasons);
                        for($i = 0; $i < $j; $i++)
                        {
    ?>	
        <div class="checkbox">
          <label>
              <?php echo $this->Form->input('reason_val',array('name'=>'data[CancelReason][reason_val][]','type'=>'checkbox','class'=>'chk_reason','value'=>'0','label'=>false,'div'=>false,'id'=>$reasons[$i]['Reason']['id'])); ?> 
             <p><?php echo $reasons[$i]['Reason'][$locale]; ?></p>
          </label>
          </div>
      
<?php 	}
        }
?>	
						
       <div id="err_reason_val" class="register_error alert-danger"> <?php if(isset($error['reason_id'][0])) echo $error['reason_id'][0];?> </div>
        <div class="form-group">
          <label for="givenName"><?php echo __('would_you_like_to_share_a_few_words_about_your_experience_with_us');?></label>
          <?php echo $this->Form->input('reason',array('type'=>'textarea','placeholder'=>__('your_leaving_thoughts'),'id'=>'givenName','class'=>'form-control','label'=>false,'div'=>false)); ?>
        <div id="err_reason" class="register_error alert-danger"> <?php if(isset($error['reason'][0])) echo $error['reason'][0];?> </div>
        </div>
        
        <p><?php echo __('thank_you_we_will_use_your_contribution_to_deliver_better_training_for_everyone');?></p><br>
        
        
        <span class="Link"><?php echo $this->Html->link(__('i_have_changed_my_mind_please_don_t_cancel_my_account'),array('controller'=>'Members','action'=>'update_profile'),array('class'=>'sc_btns btn_sbmt disRegisBut')); ?></span> 
        <?php if($cancel_request==1)
            {?>
                    <span class="Link">
                        <a href="javascript:;"> <?php echo __('account_was_cancelled');?> </a>
                    </span>			
                    <span class="Link">
                            <?php echo $this->Html->link(__('reactivate_account'),array('controller'=>'Home','action'=>'reactivate_account_again'),array('class'=>'sc_btns btn_sbmt disRegisBut')); ?> 
                    </span>	

        <?php }
        else{ ?>
                   
         <span class="Link">
             <?php echo $this->Form->submit(__('cancel_my_account'),array('class'=>'sc_btns btn_sbmt disRegisBut','div'=>false,'onclick'=>"return ajax_form('membercancel','Home/validate_cancel_account_ajax','load_top','disRegisBut')"));?>
              <?php echo __('we_will_check_to_see_if_you_have_any_training_obligations');?></span> 
        <?php } ?>
        
        <?php echo $this->Form->end(); ?>
        
        <?php if($type=='subscriber') { ?>
        <!-- Modal -->
                <div class="modal fade" id="myModal19" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog CancelConfirmPopup">
                <div class="modal-content">
                    <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                     <h4 class="modal-title" id="myModalLabel"><?php echo __('confirmation');?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo __('by_cancelling_your_account_you_agree_that_courses_sold_or_are_subscribed_to_members_may_continue_to_be_offered_on_Kuulum_courses_that_were_never_sold_will_be_removed_from_Kuulum_immediately_you_agree_to_continue_to_abide_by_the');?> <?php echo $this->Html->link(__('author_terms_and_conditions'),array('controller'=>'Home','action'=>'author_terms'),array());?>.</p><br>
                        <p><?php  echo String::insert(__("if_you_cancel_your_account_now_you_will_no_longer_have_access_to_your_account_or_the_training_materials_purchased_please_kindly_note_that_we_do_not_offer_refunds"),array('date' => $exp_date)); ?>
                        </p><br>
                        
                        <p><?php echo __('are_you_really_sure_you_want_to_cancel_your_account');?></p><br>
                        
                        <div class="Link BigLink"><a href="javascript:;" id="do_not_cancel"><?php echo __('no_dont_cancel_my_account');?></a></div>
                        <div class="Link BigLink"><a href="javascript:;" onclick="yes_cancel('subscriber');"><?php echo __('yes_cancel_my_account_now');?></a></div>
                    </div>                            
                </div>
            </div>
        </div>
        <?php } if($type=='purchaser' )
			{ ?>
        
        <!-- Modal -->
                <div class="modal fade" id="myModal19" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog CancelConfirmPopup">
                <div class="modal-content">
                    <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                     <h4 class="modal-title" id="myModalLabel"><?php echo __('confirmation');?></h4>
                    </div>
                    <div class="modal-body">
                         <p><?php echo __('by_cancelling_your_account_you_agree_that_courses_sold_or_are_subscribed_to_members_may_continue_to_be_offered_on_Kuulum_courses_that_were_never_sold_will_be_removed_from_Kuulum_immediately_you_agree_to_continue_to_abide_by_the');?> <?php echo $this->Html->link(__('author_terms_and_conditions'),array('controller'=>'Home','action'=>'author_terms'),array());?>.</p><br>
                                            
                       
                        <?php if(!empty($purchase_info)) { ?>
                            <p><?php echo __('your_purchased_courses_and_lessons');?></p><br>   
                        <?php }?>
                            
                            <table data-filter="#filter" class="footable PurchaseHistoryTable" data-page-size="5">
                           <thead>
                             <tr>
                                 <th width="16%" data-hide="phone,tablet" data-sort-initial="true" class="text_left "><a href="javascript:;"><?php echo __('date_and_time');?><i class="fa fa-caret-up"></i></a></th>
                                <th width="20%" data-hide="phone, tablet" class="text_left "><a href="javascript:;"><?php echo __('transaction_number');?><i class="fa fa-caret-up"></i></a></th>
                                <th width="49%" data-class="expand" class="text_left "><a><?php echo __('course_or_lesson');?><i class="fa fa-caret-up"></i></a></th>
                                <th width="15%"  data-hide="phone, tablet"><a><?php echo __('price')?><i class="fa fa-caret-up"></i></a></th>
                              
                             </tr>
                           </thead>
                           <tbody>
                               <?php if(!empty($purchase_info)) {
                                       foreach($purchase_info as $purchase_info){ 
                                           if(!empty($purchase_info['CourseLesson']['id']))
                                               {$item_title=$purchase_info['CourseLesson']['title'];
                                               
                                               }else{$item_title=$purchase_info['Course']['title'];}                                       
                                ?>
                             <tr>
                                 <td width="" class="text_left"><?php echo $this->Timezone->dateAccoringLocale( $this->Session->read('LocTrain.locale'),$purchase_info['CoursePurchase']['date_added']);?></td>
                                 <td width="" class="text_left"><?php echo $purchase_info['CoursePurchase']['txn_id']; ?></td>
                                 <td width="" class="text_left"><?php echo $item_title;?></td>
                                 <td width=""><?php if(!empty($purchase_info['CourseLesson']['id'])){ 
                                                       echo number_format($purchase_info['CourseLesson']['price'],'2','.','');
                                                       }else{ echo number_format($purchase_info['Course']['price'],'2','.',''); 
                                                       
                                                       } ?></td>
                             </tr>           
                                       <?php } }?>
                            </tbody>      
                            </table>
                             <?php echo __("<a class='add-trnsctn' style='display:none;' href='{$root}Members/purchase_course'>"); 
                                                                   echo String::insert(__("plus_trans_additional_transaction_s"),array('trans' => $purchase_count)); echo __("</a>"); ?>
                               
                                <?php if(!empty($purchase_info)) { ?>
                                    <p><?php echo __('if_you_cancel_your_account_now_you_will_no_longer_have_access_to_your_account_or_the_training_materials_purchased_please_kindly_note_that_we_do_not_offer_refunds');?></p><br>                  
                            <?php  }?>
                        
                        <p><?php echo __('are_you_really_sure_you_want_to_cancel_your_account');?></p><br>
                        
                        <div class="Link BigLink"><a href="javascript:;" id="do_not_cancel"><?php echo __('no_dont_cancel_my_account');?></a></div>
                        <div class="Link BigLink"><a href="javascript:;" onclick="yes_cancel('purchaser');"><?php echo __('yes_cancel_my_account_now');?></a></div>
                        
                    </div>                            
                </div>
            </div>
        </div>
            <?php }  if($type=='author')
            { ?>
<!-- Modal -->
                <div class="modal fade" id="myModal19" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog CancelConfirmPopup">
                <div class="modal-content">
                    <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                     <h4 class="modal-title" id="myModalLabel"><?php echo __('confirmation');?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo __('by_cancelling_your_account_you_agree_that_courses_sold_or_are_subscribed_to_members_may_continue_to_be_offered_on_Kuulum_courses_that_were_never_sold_will_be_removed_from_Kuulum_immediately_you_agree_to_continue_to_abide_by_the');?> <?php echo $this->Html->link(__('author_terms_and_conditions'),array('controller'=>'Home','action'=>'author_terms'),array());?>.</p><br>
                                            
                       
                        <?php if(!empty($purchase_info)) { ?>
                            <p><?php echo __('your_purchased_courses_and_lessons');?></p><br>   
                        <?php }?>
                            
                            <table data-filter="#filter" class="footable PurchaseHistoryTable" data-page-size="5">
                           <thead>
                             <tr>
                                 <th width="16%" data-hide="phone,tablet" data-sort-initial="true" class="text_left "><a href="javascript:;"><?php echo __('date_and_time');?><i class="fa fa-caret-up"></i></a></th>
                                <th width="20%" data-hide="phone, tablet" class="text_left "><a href="javascript:;"><?php echo __('transaction_number');?><i class="fa fa-caret-up"></i></a></th>
                                <th width="49%" data-class="expand" class="text_left "><a><?php echo __('course_or_lesson');?><i class="fa fa-caret-up"></i></a></th>
                                <th width="15%"  data-hide="phone, tablet"><a><?php echo __('price')?><i class="fa fa-caret-up"></i></a></th>
                              
                             </tr>
                           </thead>
                           <tbody>
                               <?php if(!empty($purchase_info)) {
                                       foreach($purchase_info as $purchase_info){ 
                                           if(!empty($purchase_info['CourseLesson']['id']))
                                               {$item_title=$purchase_info['CourseLesson']['title'];
                                               
                                               }else{$item_title=$purchase_info['Course']['title'];}                                       
                                ?>
                             <tr>
                                 <td width="" class="text_left"><?php echo $this->Timezone->dateAccoringLocale( $this->Session->read('LocTrain.locale'),$purchase_info['CoursePurchase']['date_added']);?></td>
                                 <td width="" class="text_left"><?php echo $purchase_info['CoursePurchase']['txn_id']; ?></td>
                                 <td width="" class="text_left"><?php echo $item_title;?></td>
                                 <td width=""><?php if(!empty($purchase_info['CourseLesson']['id'])){ 
                                                       echo number_format($purchase_info['CourseLesson']['price'],'2','.','');
                                                       }else{ echo number_format($purchase_info['Course']['price'],'2','.',''); 
                                                       
                                                       } ?></td>
                             </tr>           
                                       <?php } }?>
                            </tbody>      
                            </table>
                             <?php echo __("<a class='add-trnsctn' style='display:none;' href='{$root}Members/purchase_course'>"); 
                                                                   echo String::insert(__("plus_trans_additional_transaction_s"),array('trans' => $purchase_count)); echo __("</a>"); ?>
                               
                                <?php if(!empty($purchase_info)) { ?>
                                    <p><?php echo __('if_you_cancel_your_account_now_you_will_no_longer_have_access_to_your_account_or_the_training_materials_purchased_please_kindly_note_that_we_do_not_offer_refunds');?></p><br>                  
                            <?php  }?>
                        
                        <p><?php echo __('are_you_really_sure_you_want_to_cancel_your_account');?></p><br>
                        
                        <div class="Link BigLink"><a href="javascript:;" id="do_not_cancel"><?php echo __('no_dont_cancel_my_account');?></a></div>
                        <div class="Link BigLink"><a href="javascript:;" onclick="yes_cancel('author');"><?php echo __('yes_cancel_my_account_now');?></a></div>
                        
                    </div>                            
                </div>
            </div>
        </div>
        
        <?php } ?>
    
        
    </div>
    
    
    
    </div>
  </div>
</section>
<?php
 echo $this->Html->script('front/footable.js');
echo $this->Html->script('front/footable.sortable.js');
?>
<script type="text/javascript">
			$(function() {
					$('table').footable();
			});
</script>
<!--End  main container section -->
