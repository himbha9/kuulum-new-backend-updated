<?php echo $this->Html->css('autoSuggest');?>
<?php echo $this->Html->script('jquery.autoSuggest');?>
<?php echo $this->Html->script('Scripts/jquery.raty.js');?>

<style type="text/css">
	.regfld_txtcont_search
	{
		float:left;
		width:92%;
		margin-left:10px;
		position:relative;
	}
	span.selectArrow
	{
		width:13%;
	}
	div.selectOptions
	{
		height:156px;
	}
	ul.as-selections
	{
		width:90%;
	}
	.new_outer_selectradius
	{
		float: left;
		width: 100%;
		
	}
	.select_options_scr_bar {
    height: 65px;!important;
    overflow-y: scroll;!important;
}

	
</style>
<script type="text/javascript">
	var req = '';
	
	var data = {items: [<?php echo $interest_list;?>]};
// Function FOR Select Box
		
	function enableSelectBoxes1()
	{
		$('div.selectBox').each(function(){
			$(this).children('span.selected').html($(this).children().children('div.new_outer_selectradius').children('span.selectOption:first').html());		
			
			$(this).children('span.selected,span.selectArrow').click(function(){
				
				$(this).parent().css({'border-radius':'10px 0 0 0px'});
				if($(this).parent().children('div.selectOptions').css('display') == 'none'){
					$(this).parent().children('div.selectOptions').css('display','block');
				}
				else
				{
					$(".selectBox").css({'border-radius':'10px 0 0 10px'});
					$(this).parent().children('div.selectOptions').css('display','none');
				}
			});
			
			$(this).find('span.selectOption').click(function(){
				var currSelected = $(this).attr('id');
				var div_id = $(this).parent().parent().attr('id');
				$(this).parent().parent().parent().css({'border-radius':'10px 0 0 10px'});
				if(currSelected == '0'){
					$("#"+div_id).val('');	
					$(this).parent().parent().parent().css({'color':'#888'});
				}else{
					$("#"+div_id).val(currSelected);
					$(this).parent().parent().parent().css({'color':'#333'});
				}							
				$(this).parent().parent().css('display','none');
				$(this).closest('div.selectBox').attr('value',$(this).attr('value'));
				$(this).parent().parent().siblings('span.selected').html($(this).html());
				
			});
		});				
	}

$(document).ready(function(){
	var duration=0;
	var rating = null;
	var num_records=$('.tchng_content').length ;
	enableSelectBoxes1();
	$("#keywords").autoSuggest(data.items, {selectedItemProp: "name", searchObjProps: "value",preFill: [],fieldName: "keyword"});
	
	$('.return_to').live('click',function(){
		$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
	});
		
	$(".open_video").live('click',function(){
		var getUrl = $(this).attr('id');	
		var w = 700;
		var h = 400;
		var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/1.5);	
		window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
	});	
	$(".read_more_review").live('click',function(){
		var tabText = $(this).text();
		var tabNewText = $(this).attr('rel');
		$(this).parent().prev().show();
		$(this).parent().prev().prev().hide();
		$(this).attr('class','read_less_review');
		$(this).attr('rel',tabText);
		$(this).text(tabNewText);		
		return false;
		
	});
	$(".read_less_review").live('click',function(){
		var tabText = $(this).text();
		var tabNewText = $(this).attr('rel');	
		$(this).parent().prev().hide();
		$(this).parent().prev().prev().show();
		$(this).attr('class','read_more_review');		
		$(this).attr('rel',tabText);
		$(this).text(tabNewText);	
		return false;		
	});
// FOCUSOUT SEARCH 
	$(".regfld_txt_search").live('focusout',function(){
		var getId = $(".tchng_cont_content").attr('id');
		var totalRecord = $(".tchng_cont_container").attr('id');	
		var order = $("#sort").val();
		
		var d_option = store_duration(0);
		if(d_option ==0)
		{
			duration_option='all';
		}
		else
		{
			duration_option=d_option;
		} 
		
		score=store_rating(null);
		if(req  && req.readystate  != '4'){
			req.abort();
		}			
		$(".load_top").show();	
		req = $.ajax({
			url:ajax_url+'Home/catalogue/',
			type: 'get',
			data: {'type':'3','limit':getId,'keyword':$(".as-values").val(),'author':$("#author").val(),'order':order,'total_record':totalRecord,'duration_opt':duration_option,'score':score},
			success: function(resp){				
				$("#searchType").val('1');	
				$(".tchng_cont_content").html(resp);	
				$(".load_top").hide();
			}
		});
	});
	
	 function store_duration(d)
	{
		if(d==0)
		{
			return duration;
		}
		else
		{
			duration=d;
			//alert(duration);
		}
	}
	 function store_rating(r)
	{
		if(r==null)
		{
			return rating;
		}
		else
		{
			rating=r;
			//alert(duration);
		}
	}
	
//sort by duration
$('.sop').click(function(){
		$this=$(this);
		var duration_option = $this.attr('id');
		store_duration(duration_option);
		var getId = $(".tchng_cont_content").attr('id');
		var totalRecord = $(".tchng_cont_container").attr('id');	
		var order = $("#sort").val();
		score=store_rating(null);
		if(req  && req.readystate  != '4'){
			req.abort();
		}				
		$(".load_top").show();	
		req = $.ajax({
			url:ajax_url+'Home/catalogue/',
			type: 'get',
			data: {'type':'7','limit':getId,'keyword':$(".as-values").val(),'order':order,'author':$("#author").val(),'total_record':totalRecord, 'duration_opt':duration_option,'score':score},
			success: function(resp){				
				$("#searchType").val('1');	
				$(".tchng_cont_content").html(resp);	
				$(".load_top").hide();
			}
		});
		
	});

	$('.sb_auth').click(function(){
			var getId = $(".tchng_cont_content").attr('id');	
			var totalRecord = $(".tchng_cont_container").attr('id');
			var order = $("#sort").val()
			var d_option = store_duration(0);
			if(d_option ==0)
			{
				duration_option='all';
			}
			else
			{
				duration_option=d_option;
			}
			if(req  && req.readystate  != '4'){
				req.abort();
			}
			score=store_rating(null);
			$(".load_top").show();	
				req = $.ajax({
				url:ajax_url+'Home/catalogue/',
				type: 'get',
				data: {'type':'3','limit':getId,'keyword':$(".as-values").val(),'author':$("#author").val(),'order':order,'total_record':totalRecord , 'duration_opt':duration_option,'score':score},
						success: function(resp){
							$("#searchType").val('1');	
							$(".tchng_cont_content").html(resp);	
							$(".load_top").hide();
						}
					});
	});

// SORT RECORDS
	$(".filter_options_rdo").click(function(){	
		var order = $(this).attr('id');
		$("#sort").val(order);
		var getId = $(".tchng_cont_content").attr('id');
		var totalRecord = $(".tchng_cont_container").attr('id');		
		var searchType = $("#searchType").val();
		var d_option = store_duration(0);
		score=store_rating(null);
		if(d_option ==0)
		{
			duration_option='all';
		}
		else
		{
			duration_option=d_option;
		}
		if(req  && req.readystate  != '4'){
			req.abort();
		}
		if(req  && req.readystate  != '4'){
			req.abort();
		}
		$(".load_top").show();	
		if(searchType == '1'){			
			req = $.ajax({
				url:ajax_url+'Home/catalogue/',
				type: 'get',
				data: {'type':'4','limit':getId,'keyword':$(".as-values").val(),'author':$("#author").val(),'order':order,'total_record':totalRecord,'duration_opt':duration_option,'score':score},
				success: function(resp){
					flag = 0;	
					$("#searchType").val('1');	
					$(".tchng_cont_content").html(resp);	
					$(".load_top").hide();
				}
			});
		}
		else{				
			req = $.ajax({
				url:ajax_url+'Home/catalogue/',
				type: 'get',
				data: {'type':'5','limit':getId,'order':order,'total_record':totalRecord,'duration_opt':duration_option},
				success: function(resp){														
					$(".tchng_cont_content").html(resp);	
					$(".load_top").hide();
				}
			});
		}
	});
// RESET SEARCH FILTER	
	$(".scn_link").click(function(){			
		if(req  && req.readystate  != '4'){
			req.abort();
		}
		$(".load_top").show();	
		/*req = $.ajax({
			url:ajax_url+'Home/catalogue/',
			type: 'get',
			data: {'type':'6','limit':0},
			success: function(resp){	
				$("#searchType").val('0');
				$("#sort").val('');
				$(".filter_options_rdo").removeClass("rdo_chkd");
				$(".as-selection-item").remove();
				$(".as-values").val('');
				$(".tchng_cont_content").html(resp);	
				$(".load_top").hide();
			}
		}); */
		window.location = ajax_url+'Home/catalogue';
	});	
	$(".add_learning").live('click',function(){
		var getId = $(this).attr('rel');
		$(".load_top").show();	
		var a =$(this);	
		req = $.ajax({
				url:ajax_url+'Home/add_to_learning/'+getId,
				dataType: 'json',
				success: function(resp){
					if(resp.status == 'ok'){
						$(".load_top").hide();
						a.addClass('added_learning');	
						a.removeClass('add_learning');
						$(".ajax-success").html(resp.msg).show();
						$('.ajax-success').fadeOut(10000);
						//$('#middle').prepend('<div class="ajax-success"> </div>');
					}
					else if(resp.status == 'error'){
						window.location.href = resp.url;
					}
				}
		});
	});
	
	$('#star').raty({
			
				starOn  : 'star-32-gold.png',
				starOff : 'star-32-grey-2.png',
				cancel	: false,
				path 	: '<?php echo HTTP_ROOT.'img/'; ?>',
			cancelHint	: 'Remove this filter.',
				click	: function(score, evt) {
					$(this).fadeOut(function() { $(this).fadeIn(); });
							store_rating(score);
							var getId = $(".tchng_cont_content").attr('id');
							var totalRecord = $(".tchng_cont_container").attr('id');
							var d_option = store_duration(0);
							if(d_option ==0)
							{
								duration_option='all';
							}
							else
							{
								duration_option=d_option;
							} 	
							var order = $("#sort").val();	 	
							if(req  && req.readystate  != '4'){
								req.abort();
							}				
							$(".load_top").show();	
							req = $.ajax({
							url:ajax_url+'Home/catalogue/',
							type: 'get',
							data: {'type':'8','limit':getId,'keyword':$(".as-values").val(),'author':$("#author").val(),'order':order,'total_record':totalRecord , 'duration_opt':duration_option,'score':score},
							success: function(resp){
							$("#searchType").val('1');	
							$(".tchng_cont_content").html(resp);	
							$(".load_top").hide();
						}
					});
					
				},
				targetKeep	: true,
				
				score		: 0		
		}); 

});
<?php if(!empty($course_list)) { ?>
$(document).scroll(function(){
	if ($(window).scrollTop() == $(document).height() - $(window).height()){		
		var getId = $(".tchng_cont_content").attr('id');
		var totalRecord = $(".tchng_cont_container").attr('id');	
		var searchType = $("#searchType").val();
		if(req  && req.readystate  != '4'){
			req.abort();
		}
		if(getId != '0'){		
			$(".loading_inner").show();	
			if(searchType != 1){	
				$(".tchng_cont_content").attr('id','0');		
				req = $.ajax({				
					url:ajax_url+'Home/catalogue/',
					type: 'get',
					data: {'type':'1','limit':getId,'order':$("#sort").val(),'total_record':totalRecord},
					success: function(resp){	
						if($.trim(resp) == 'error'){
							window.location.href = ajax_url+'Home/index';
						}
						$(".tchng_cont_content").append(resp);	
						$(".loading_inner").hide();
					}
				});
			}
			else{					
				req = $.ajax({					
					url:ajax_url+'Home/catalogue/',
					type: 'get',
					data: {'type':'2','limit':getId,'keyword':$(".as-values").val(),'author':$("#author").val(),'order':$("#sort").val()},
					success: function(resp){
						if($.trim(resp) == 'error'){
							window.location.href = ajax_url+'Home/index';
						}
						$(".tchng_cont_content").append(resp);	
						$(".loading_inner").hide();
					}
				});
			}
		}	
	}
});
<?php } ?>
</script>

<div id="wrapper"> 
  <div id="middle">
  	<div class="ajax-success">	</div>
    <div class="breadcrumb_botcont">
    	<div class="tchng_cont">
            <div class="tchng_cont_container"  id="<?php echo $last_id;?>">     
              <div class="tchng_cont_content" id="<?php echo $last_id;?>">
                  <?php if(!empty($course_list)) { ?>
                    <?php echo $this->element('frontElements/home/courses_list'); ?>
                  <?php }else{ ?>
                    <div class="mytch_rcd">
                        <?php echo __('no_courses_match_the_filter'); ?>
                    </div>
                  <?php } ?>
              </div>
              <div class="loading">
                    <div class="loading_inner">
                        <?php echo $this->Html->image('front/loading.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                    </div>
              </div>
            </div>
        </div>
     	 <div class="formfld_rgt tchng">
            <div class="section_guide"> <span class="guide_upper"> <?php echo __('in_this_section');?> </span>
              <h2 class="guide_head"> <i class="rstrt_rslt_i"><?php echo __('restrict_results'); ?></i><p class="rstrt_rslt_p"></p> </h2>             
              <div class="guide_bot"> 
				  <?php //echo $this->Form->create('SearchCourse',array('id'=>'filterCourses','url'=>'/Member/catalogue_filter','type'=>'get')); ?> 
                    <div class="section-linkcover">                   
                     <div class="vw_guide_bot keyword_btm"> 
                        <span class="vw_option"> <?php echo __('filter_by_keyword');?></span>
                         <span class="regfld_txtcont_search">
                         	<?php echo $this->Form->input('type',array('type'=>'hidden','id'=>'searchType','value'=>'0')); ?>                           
                            <?php echo $this->Form->input('keywords',array('type'=>'text','id'=>'keywords','class'=>'regfld_txt_search gradient','label'=>false,'div'=>false)); ?>
                         </span>   
                      </div>    
                     <div class="vw_guide_bot keyword_btm"> 
                        <span class="vw_option"> <?php echo __('filter_by_course_duration');?></span>
                         <span class="regfld_txtcont_search">
                           <div class='selectBox regfld_txtera_slct'>
                                    <?php echo $this->Form->input('duration',array('type'=>'hidden','id'=>'duration','class'=>'','label'=>false,'div'=>false,'value'=>'0')); ?> 
                                <span class='selected wdthincrs_selected_srch' ></span>
                                <span class='selectArrow'> <?php echo $this->Html->image('ae.png',array('class'=>'img_arw','width'=>16,'height'=>16)); ?> </span>
                                <div class="selectOptions wdthincrs_selectedopt" id="duration">
                                    <div class="new_outer_selectradius select_catalogue">            
                                    	<span class="selectOption sop" id="all" > <?php echo __('all_durations'); ?> </span>      
                                        <span class="selectOption sop" id='<15'> <?php echo __('less_than_15_minutes'); ?> </span>
                                        <span class="selectOption sop" id='15>30<'> <?php echo __('between_15_and_30_minutes'); ?> </span>
                                        <span class="selectOption sop" id='30>60<'> <?php echo __('between_30_and_60_minutes'); ?> </span>
                                        <span class="selectOption sop" id='60>'> <?php echo __('more_than_60_minutes'); ?> </span>
                                    </div>
                                </div>
                                <!--<div class="selectOptions wdthincrs_selectedopt" >
                                    <div class="new_outer_selectradius">            
                                    	<span class="selectOption" id="all" > <?php echo __('All durations'); ?> </span>      
                                        <span class="selectOption" id='<15'> <?php echo __('Less than 15 minutes'); ?> </span>
                                        <span class="selectOption" id='15>30<'> <?php echo __('Between 15 and 30 minutes'); ?> </span>
                                        <span class="selectOption" id='30>60<'> <?php echo __('Between 30 and 60 minutes'); ?> </span>
                                        <span class="selectOption" id='60>'> <?php echo __('More than 60 minutes'); ?> </span>
                                    </div>
                                </div>    -->              
                            </div>  
                        </span>
                      </div>
                       <div class="vw_guide_bot keyword_btm"> 
                        <span class="vw_option"> <?php echo __('filter_by_author');?></span>
                         <span class="regfld_txtcont_search">
                           <div class='selectBox regfld_txtera_slct'>
                                    <?php echo $this->Form->input('author',array('type'=>'hidden','id'=>'author','class'=>'','label'=>false,'div'=>false,'value'=>'0')); ?> 
                                <span class='selected wdthincrs_selected_srch' ></span>
                                <span class='selectArrow'> <?php echo $this->Html->image('ae.png',array('class'=>'img_arw','width'=>16,'height'=>16)); ?> </span>
                                <div class="selectOptions select_options_scr_bar wdthincrs_selectedopt" id='author'>
                                    <div class="new_outer_selectradius select_catalogue">
                                    	<?php if(!empty($author)) {  ?>
                                        	<span class="selectOption sb_auth" id="0"> <?php echo __('all_authors'); ?> </span>
                                        	<?php foreach($author as $key=>$author) { ?>                            
                                               		<span class="selectOption sb_auth" id="<?php echo $key;?>"> <?php echo $author; ?> </span>
                                            <?php }?>
                                        <?php }else{ ?> 
                                        	<span class="selectOption sb_auth" > <?php echo __('no_records_found'); ?> </span>
										<?php } ?>           
                                    	
                                    </div>
                                </div>                   
                            </div>  
                        </span>
                      </div>
                     <div class="vw_guide_bot keyword_btm"> 
                        <span class="vw_option"> <?php echo __('filter_by_average_rating');?>  </span>
                        <span class="rt_img_tp">
						
							<div id="star"></div>
                            <!--<img alt="" =""="" class="img_topmain" src="/kuulum/img/rt.png"> -->                
                        </span>                    
                        <div class="section_links new_sctionlnk"> 
                            <?php echo $this->Html->link(__('reset_filter'),'javascript:void(0)',array('class'=>'scn_link filter_new')); ?>
                        </div>                  
                      </div>
                    </div>
                   <?php echo $this->Form->end(); ?>
                
                <div class="guide_bot"> <span class="option"><?php echo __('sort_by'); ?></span>
                    <div class="sctn_radiolinks">
                         <?php echo $this->Form->input('sort',array('type'=>'hidden','id'=>'sort','value'=>'0')); ?>
                         <div class="filter_cont">
                            <div class="section_links filter_options_rdo" id="title" rel="sort">  </div>
                            <span class="filter_options_lbl"> <?php echo __('course_title');?></span>
                         </div>
                         <div class="filter_cont">
                            <div class="section_links filter_options_rdo" id="date" rel="sort">  </div>
                            <span class="filter_options_lbl"> <?php echo __('published_date');?></span>
                         </div>
                         <div class="filter_cont">
                            <div class="section_links filter_options_rdo" id="rating" rel="sort">  </div>
                            <span class="filter_options_lbl"> <?php echo __('average_rating');?></span>
                         </div>
                         <div class="filter_cont">
                            <div class="section_links filter_options_rdo" id="reviews" rel="sort">  </div>
                            <span class="filter_options_lbl"> <?php echo __('most_reviews');?></span>
                         </div>
                  </div>
              </div> 
            </div>
          </div>
    	</div>
    </div>
  </div>
</div>
