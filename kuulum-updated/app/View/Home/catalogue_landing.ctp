
<!-- Carousel [Start] -->
<?php 
$cookie_lessons = array();
		if(!empty($getCoursesindex)){
			
			foreach($getCoursesindex as $key=>$val){
				if(strlen($key)>8)
				{
					$cookie_lessons[] = convert_uudecode(base64_decode($val));
				}
				
			}
		}

$cookie_courses = array();
		if(!empty($getCoursesindex)){
			foreach($getCoursesindex as $course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
			}
			//pr($cookie_courses); die;
		}


    echo $this->Html->css('front/jcarousel.responsive.css');
    echo $this->Html->script('front/jquery.jcarousel.min.js');
    echo $this->Html->script('front/jcarousel.responsive.js');
?>
<script type="text/javascript">
$('document').ready(function(){

$('body').on('click','.CarouselBox',function(e){
if( $(e.target).hasClass("rating-container")){
//console.log('yes');
return false;
}else if( $(e.target).hasClass("rating-stars")){
//console.log('yes');
return false;
}else{
//console.log('no');
window.location=ajax_url+'Home/view_course_details/'+$(this).attr('id');
}
});
$('body').on('click','.video_sec_click',function(e){
if( $(e.target).hasClass("rating-container")){
//console.log('yes');
return false;
}else if( $(e.target).hasClass("rating-stars")){
//console.log('yes');
return false;
}else{
//console.log('no');
window.location=ajax_url+'Home/view_course_details/'+$(this).attr('id');
}
});

$('.video_sec_video_click').on('click',function(e){
if( $(e.target).hasClass("rating-container")){
//console.log('yes');
return false;
}else if( $(e.target).hasClass("rating-stars")){
//console.log('yes');
return false;
}else{
//console.log('no');
window.location=ajax_url+'Home/view_free_video/'+$(this).attr('id');
}
});

});
</script>
<?php echo $this->Html->script('jquery.tabify.js');?>
<!-- Carousel [End] -->

<!--Start Tab Drop section --> 
    <?php echo '';?>
 <!-- Start Carousel section -->

    <section class="CarauselBg catalogue_carausel_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="jcarousel-wrapper">
                        <div class="jcarousel" data-jcarousel="true">
                            <ul style="left: 0px; top: 0px;">
                                <?php if(!empty($courses)) { ?>
                                <?php foreach($courses as $course) { ?>
                                <li style="width: 200px;" class="CarouselBox" id="<?php echo base64_encode(convert_uuencode($course['Course']['id']));?>">
                                    <div class="video_sec video_sec_click" id="<?php echo base64_encode(convert_uuencode($course['Course']['id']));?>"> 
                                        <?php 	$video = '';$extension = ''; $image='';	 $less_image='';				
								if(!empty($course['CourseLesson'])){
									foreach($course['CourseLesson'] as $lesson){
										if($lesson['video'] != ''){
											$video = $lesson['video'];
                                            $extension = $lesson['extension'];
 											$image = $lesson['image'];

											break;
										}
									}
foreach($course['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                        $less_image=$lesson['image'];
                        break;
                    }
                }
								}								
						?>
							 <?php  
									

 		$course_image = $course['Course']['image'];
                // $path = 'files/members/courses/'.$course_image;
		$path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$course_image);
                    if(!empty($course_image) && $path){
                         echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'thumb/'.$course_image),array('width'=>'262','height'=>'138','class'=>'img-responsive')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($course['Course']['id']))),array('class'=>'course_link','escape'=>false));        
                    }else if(!empty($less_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$less_image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));
                    }else if(!empty($image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));     
                    }else
                    { 
                       echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>'262','height'=>'138','class'=>'img-responsive','url'=>array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($course['Course']['id'])))),array('class'=>'img-responsive'));   
                    }
									
								?>
                                        
                                        
                                       
                                        <h2><?php
$title=$this->Text->truncate(strip_tags($course['Course']['title']),50,array( 'ellipsis' => '...','exact' => false,'html' => false));
 echo $this->Html->link($title,array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($course['Course']['id']))),array()); ?></h2>
                                        <div class="row RatingBox">
                                          <?php 
                                                        $m_id = $this->Session->read('LocTrain.id');
                                                       $decoded_id = $course['Course']['id'];
                                                        
                                                                   
$is_rated=$course['CourseRating'];
                                                                      $rate=''; 
							   for($i=0;$i<count($is_rated);$i++)
							   {
							   
							   $rate+=$is_rated[$i]['rating'];
							   }
							   //$is_rated1=$rate/count($is_rated);
if(count($is_rated)>0){ $is_rated1=$rate/count($is_rated);}else{ $is_rated1=0;}
							   if(!empty($is_rated1))
							   {
							   if(count($is_rated)>1)
								{
								$rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
								}
								else
								{
								$rating=$is_rated1;
								}
								}
                                                                   else
                                                                   {
                                                                           $rating= 0;
                                                                   }$login=0;
                                                               if($this->Session->read('LocTrain.id'))
			{
			$login=1;
			}
                                                        
                                                
						$first_lesson=isset($course['CourseLesson'])?$course['CourseLesson']['0']['id']:0;
$subscription = $this->Utility->getCoursesubscriptionIndex($m_id,$decoded_id,@$first_lesson,$course['CoursePurchaseList']);
                

                                                    ?>
                                                     <div class="col-xs-6 Rating ">
                                                    <?php if($m_id != $course['Course']['m_id'] && $login==1) {?>
                                                       <?php if(!empty($subscription)  || $course['Course']['price']==0.00) { ?>

                                                       <input id="input-21a-<?php echo $course['Course']['id'];?>" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $course['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                       <?php    } else {?>
                                                        <input id="input-21a-<?php echo $course['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $course['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                        <?php } }else{?>
                                                        <input id="input-21a-<?php echo $course['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $course['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                        <?php } ?>
<!-- <div class="RatingNum"><span class="to_rating_<?php echo $course['Course']['id'];?>"><?= $rating; ?></span> Rating</div> -->

                                            </div>
                                        <div class="col-xs-6 Price"><?php 

if($course['Course']['price']=='0.00'){ 
$course_details=$this->Utility->getCourseLearningStatusIndex($m_id,$decoded_id,$course['CoursePurchaseList']);
$class = !empty($course_details)?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
if($class != 'c_added_learning_dsbl'){
echo __('free');
}else{
echo __('purchased');
}

}else{


$subscription_price= $this->Utility->getCoursesubscription_priceLeasson($course['CoursePurchaseList'],$course['CourseLesson'],$cookie_lessons);

if($login==1 && ($subscription_price || in_array($decoded_id,$cookie_courses) )){

echo __('purchased');
}else{
echo $this->Utility->externlizePrice($language,$locale,$course['Course']['price']);
}
}

?></div>
                                        </div>
                                        <!--<div class="vwcrsply_btn_thmb-large">
							
							<?php echo $this->Html->link('<i class="fa fa-play"></i>',array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($course['Course']['id']))),array('escape'=>false,'class'=>'ply_btn_anchr_thmb')); ?>
						</div>-->
                                    </div>
                                </li> 
                                <?php } } ?>                                
                                
                            </ul>
                        </div>
                        
                        <a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true"><i class="fa fa-angle-left"></i></a>
                        <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true"><i class="fa fa-angle-right"></i></a>                                        
                
                        <!--<p class="jcarousel-pagination" data-jcarouselpagination="true"><a href="#" class="active">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a></p>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
                
    <!--End Carousel section -->

    <div class="bodyContentWrapper">
    <section class="catalogue_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="tabbable Tabbable1 margintop20">
                    
                    	<ul class="nav nav-tabs TabDropdownBox ">
                        	<li class="dropdown pull-right tabdrop">
                                <a id="dropdown6" class="dropdown-toggle" data-toggle="dropdown" href="#">Select <b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-menu-cata TabDropdown" aria-labelledby="dropdown6">
                                    <li class=""><a href="#news" data-toggle="tab"><?php echo __('show_new_courses'); ?></a></li>
                                    <li class=""><a href="#description" data-toggle="tab"><?php echo __('show_most_popular_courses'); ?></a></li>
                                    <li class=""><a href="#usage" data-toggle="tab"><?php echo __('show_courses_by_category'); ?></a></li>
                                    <li><a href="#download" data-toggle="tab"><?php echo __('show_courses_by_author'); ?></a></li>
                                    <!--<li><a href="#alphabetical" data-toggle="tab"><?php echo __('show_courses_alphabetcal'); ?></a></li>-->
                                    <li><a href="#freevideo" data-toggle="tab"><?php echo __('show_free_videos'); ?></a></li>
                                </ul>
                            </li>
                        </ul>
                        
                            <ul class="nav nav-tabs">                                                                                   	                                            
                                                        
                                <li class="active"><a href="#news" data-toggle="tab"><i class="fa fa-caret-up"></i><?php echo __('new'); ?></a></li>
                                <li class=""><a href="#description" data-toggle="tab"><i class="fa fa-caret-up"></i><?php echo __('most_popular'); ?></a></li>
                                <li class=""><a href="#usage" data-toggle="tab"><i class="fa fa-caret-up"></i><?php echo __('category'); ?></a></li>
                                <li><a href="#download" data-toggle="tab"><i class="fa fa-caret-up"></i><?php echo __('author'); ?></a></li>
                                <!--<li><a href="#alphabetical" data-toggle="tab"><i class="fa fa-caret-up"></i><?php echo __('alphabetical'); ?></a></li>-->
                                <li><a href="#freevideo" data-toggle="tab"><i class="fa fa-caret-up"></i><?php echo __('free_videos'); ?></a></li>
                               
                            </ul>
                        
                            <div class="tab-content">
                            
                                <div class="tab-pane active" id="news">
                                <?php foreach($new as $list){ ?>
						  <?php 	$video = ''; $extension='';		$image='';	$less_image='';	
                                      if(!empty($list['CourseLesson'])){
                                          foreach($list['CourseLesson'] as $lesson){
                                              if($lesson['video'] != ''){
                                                    $video = $lesson['video'];
                                                    $extension=$lesson['extension'];
                                                    $image=$lesson['image'];
                                                  //pr($video); die;
                                                  break;
                                              }
                                          }
foreach($list['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                        $less_image=$lesson['image'];
                        break;
                    }
                }
                                      }								
                          ?>	
                                	<div class="row video_sec video_sec2 video_sec_click" id="<?php echo base64_encode(convert_uuencode($list['Course']['id']));?>">
                                        <div class="col-lg-6 col-md-6 col-sm-6 VideoBoxLeft">
                                        	
                                                  <?php
				$course_image = $list['Course']['image'];
                $path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$course_image);
                    if(!empty($course_image) && $path){
                         echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'thumb/'.$course_image),array('width'=>'262','height'=>'138','class'=>'img-responsive')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'course_link','escape'=>false));        
                    }else if(!empty($less_image) &&  $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$less_image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));
                    }else if(!empty($image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));     
                    }else
                    { 
                       echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>'262','height'=>'138','class'=>'img-responsive','url'=>array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id'])))),array('class'=>'img-responsive'));   
                    }	?>
                                               
                                            <h2><?php 
$title_n=$this->Text->truncate(strip_tags($list['Course']['title']),50,array( 'ellipsis' => '...','exact' => false,'html' => false));
echo $this->Html->link($title_n,array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'')); ?></h2>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 RatingBox">
                                        	<?php
                                        $m_id = $this->Session->read('LocTrain.id');
                                       $decoded_id = $list['Course']['id'];$login=0;
                                       if($this->Session->read('LocTrain.id'))
			{
			$login=1;
			}

                                                   $is_rated=$list['CourseRating'];
                                                      $rate=''; 
							   for($i=0;$i<count($is_rated);$i++)
							   {
							   
							   $rate+=$is_rated[$i]['rating'];
							   }
							   if(count($is_rated)>0){ $is_rated1=$rate/count($is_rated);}else{ $is_rated1=0;}
							   if(!empty($is_rated1))
							   {
							   if(count($is_rated)>1)
								{
								$rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
								}
								else
								{
								$rating=$is_rated1;
								}
								}
                                                   else
                                                   {
                                                           $rating= 0;
                                                   }
                                                  
                                           $first_lesson=isset($list['CourseLesson'])?$list['CourseLesson']['0']['id']:0;
                                         
                                          $subscription = $this->Utility->getCoursesubscriptionIndex($m_id,$decoded_id,@$first_lesson,$list['CoursePurchaseList']);
                                         

                                       ?>
                                                   <div class="col-xs-6 Rating no-padding">
                                                       <?php if($m_id != $list['Course']['m_id'] && $login==1) {?>
                                                          <?php if(!empty($subscription)  || $list['Course']['price']==0.00) { ?>

                                                          <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $list['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                          <?php    } else {?>
                                                           <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $list['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                           <?php } }else{?>
                                                           <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $list['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                           <?php } ?>
<!--<div class="RatingNum"><span class="to_rating_<?php echo $list['Course']['id'];?>"><?= $rating; ?></span> Rating</div>-->
                                               </div>
                                            <div class="col-xs-6 Price no-padding"><?php



if($list['Course']['price']=='0.00'){ 
$course_details=$this->Utility->getCourseLearningStatusIndex($m_id,$decoded_id,$list['CoursePurchaseList']);
$class = !empty($course_details)?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
if($class != 'c_added_learning_dsbl'){
echo __('free');
}else{
echo __('purchased');
}

}else{


$subscription_price= $this->Utility->getCoursesubscription_priceLeasson($list['CoursePurchaseList'],$list['CourseLesson'],$cookie_lessons);

if($login==1 && ($subscription_price || in_array($decoded_id,$cookie_courses) )){

echo __('purchased');
}else{
echo $this->Utility->externlizePrice($language,$locale,$list['Course']['price']);
}
}
?></div>
                                            <p>
<?php echo $this->Text->truncate(strip_tags($list['Course']['description'],"<br>"),100,array('ellipsis'=>'...','exact'=>false));?></p>
                                        </div>        
                                    </div>
                                    
                                <?php } ?>
                                    
                                </div>
                                
                                <div class="tab-pane" id="description">
                                       <?php foreach($course_most_popular as $list){ ?>
						  <?php 	$video = '';	$extension='';$image='';	$less_image='';		
                                      if(!empty($list['CourseLesson'])){
                                          foreach($list['CourseLesson'] as $lesson){
                                              if($lesson['video'] != ''){
                                                  $video = $lesson['video'];
                                                 $extension =  @$lesson['extension'];	
                                                 $image= @$lesson['image'];
                                                  break;
                                              }
                                          }
                        foreach($list['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                        $less_image=$lesson['image'];
                        break;
                    }
                }
                                      }								
                          ?>			
                        
                                    <div class="row video_sec video_sec2 video_sec_click" id="<?php echo base64_encode(convert_uuencode($list['Course']['id']));?>">
                                        <div class="col-lg-6 col-md-6 col-sm-6 VideoBoxLeft">
                                        
                                                <?php 
           
							$course_image = $list['Course']['image'];
                 $path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$course_image);
                    if(!empty($course_image) && $path){
                         echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'thumb/'.$course_image),array('width'=>'262','height'=>'138','class'=>'img-responsive')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'course_link','escape'=>false));        
                    }else if(!empty($less_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$less_image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));
                    }else if(!empty($image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));     
                    }else
                    { 
                       echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>'262','height'=>'138','class'=>'img-responsive','url'=>array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id'])))),array('class'=>'img-responsive'));   
                    }
								?>
                                                           
                                            <h2><?php 
$title1=$this->Text->truncate(strip_tags($list['Course']['title']),50,array( 'ellipsis' => '...','exact' => false,'html' => false));
 echo $this->Html->link($title1,array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'')); ?></h2>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 RatingBox">
                                        	<?php 
                                                        $m_id = $this->Session->read('LocTrain.id');
                                                       $decoded_id = $list['Course']['id'];$login=0;
                                                        if($this->Session->read('LocTrain.id'))
			{
			$login=1;
			}

                                                                 $is_rated=$list['CourseRating'];
                                                                   $rate=''; 
							   for($i=0;$i<count($is_rated);$i++)
							   {
							   
							   $rate+=$is_rated[$i]['rating'];
							   }
							   if(count($is_rated)>0){ $is_rated1=$rate/count($is_rated);}else{ $is_rated1=0;}
							   
			   if(!empty($is_rated1))
			   {
			   if(count($is_rated)>1)
			   {
			  $rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
			   }
			   else
			   {
			   $rating=$is_rated1;
			   }
			    
			   }
                                                                   else
                                                                   {
                                                                           $rating= 0;
                                                                   }
                                                              
                                                        
                                                            $first_lesson=isset($list['CourseLesson'])?$list['CourseLesson']['0']['id']:0;
                                                     
                                                       
                                                       $subscription = $this->Utility->getCoursesubscriptionIndex($m_id,$decoded_id,@$first_lesson,$list['CoursePurchaseList']);

                                                    ?>
                                                     <div class="col-xs-6 Rating no-padding">
                                                    <?php if($m_id != $list['Course']['m_id'] && $login==1) {
													
													
													?>
                                                       <?php if(!empty($subscription) || $list['Course']['price']==0.00) { ?>

                                                       <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $list['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                       <?php    } else {?>
                                                        <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $list['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                        <?php } }else{
														
														?>
                                                        <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating update_<?php echo $list['Course']['id'];?>" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                                                        <?php } ?>
<!-- <div class="RatingNum"><span class="to_rating_<?php echo $list['Course']['id'];?>"><?= $rating; ?></span> Rating</div> -->
                                            </div>
                                            <div class="col-xs-6 Price no-padding"><?php 
if($list['Course']['price']=='0.00'){ 
$course_details=$this->Utility->getCourseLearningStatusIndex($m_id,$decoded_id,$list['CoursePurchaseList']);
$class = !empty($course_details)?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
if($class != 'c_added_learning_dsbl'){
echo __('free');
}else{
echo __('purchased');
}

}else{


$subscription_price= $this->Utility->getCoursesubscription_priceLeasson($list['CoursePurchaseList'],$list['CourseLesson'],$cookie_lessons);

if($login==1 && ($subscription_price || in_array($decoded_id,$cookie_courses) )){

echo __('purchased');
}else{
echo $this->Utility->externlizePrice($language,$locale,$list['Course']['price']);
}
}
?></div>
                                            <p><?php echo $this->Text->truncate(strip_tags($list['Course']['description'],"<br>"),100,array('ellipsis'=>'...','exact'=>false));?></p>
                                        </div>        
                                    </div>
                                       <?php } ?>
                                </div>
                                <div class="tab-pane" id="usage">
                                    <div class="row">
                                        <div class="col-lg-12">
                                    <div class="col-lg-4 col-md-4 col-sm-4 ctgry_dscrtp_lft">
						<ul>
							<?php
								if(!empty($categories))
								{
									foreach($categories as $category)
									{ 		
						if($category['CourseCategory']['id'] == 11){ ?>
                                                                             
							<li class="categoryOther">
                                                                             <?php  }else{ ?>
										<li>
										<?php } ?>                             	
											<?php 
				
echo $this->Html->link($category['CourseCategory'][$locale],'javascript:void(0)',array('class'=>'plus','rel'=>$category['CourseCategory']['id'],'escape'=>false)); 

?>
											<span class="level_first" style="display:none;">
												<ul>
													<?php
														if(count($category['CourseSubcategory']))
														{
															foreach($category['CourseSubcategory'] as $sub_category)
															{
													?>
																<li>
																	<?php echo $this->Html->link($sub_category[$locale],'javascript:void(0)',array('class'=>'sub_category plus1','rel'=>base64_encode(convert_uuencode($sub_category['id'])))); ?>
																	
																	<?php if(!empty($sub_category['CourseSlSubcategory'])){ ?>
																	
																		<span class="ctgry_dscrp_itm" style="display:none">
																			<ul class="sl_sc_ul">
																				<?php foreach($sub_category['CourseSlSubcategory'] as $sl_sub_cat){ ?>
																						
																						<li>
																							<?php echo $this->Html->link($sl_sub_cat[$locale],'javascript:void(0)',array('class'=>'sl_sub_category','rel'=>base64_encode(convert_uuencode($sl_sub_cat['id'])))); ?>
																						</li>
																						
																				<?php } ?>
																			</ul>
																		</span>
																		
																	<?php }	?>	
																</li>													
													<?php	}	

														}
													?>
												</ul>
											</span>
										</li>
							<?php 	}
								}
							?>							
						</ul>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8 dscrtp_ryt">
						<?php echo $this->element('frontElements/home/category_course'); ?>   
					</div>
                                         </div></div>
                                </div>
                                <div class="tab-pane" id="download">
                                     <div class="row">
                                        <div class="col-lg-12">
                                    		<div class="col-lg-4 col-md-4 col-sm-4 ctgry_dscrtp_lft athr_dscrtp_lft">
                        <ul>
                        
                        	<?php 
							
							if(!empty($author))
							{
								foreach($author as $author_id=>$author_name)
								{
							 ?>
                            		<li><?php echo $this->Html->link($author_name,'javascript:void(0)',array('class'=>'author_mem','rel'=>base64_encode(convert_uuencode($author_id)))); ?></li>
                            <?php }
							}
							else{
								echo __("No authors added yet");   
							   }
							?>
                           
                        </ul>
                    </div>
                   <div class="col-lg-8 col-md-8 col-sm-8 dscrtp_ryt author_wala_element">
                    <?php echo $this->element('frontElements/home/author_course'); ?>
                    </div>
                                    </div></div>
                        </div>
                                
                                <div class="tab-pane" id="freevideo">
                                   <?php if(!empty($freevideo))
				   {
				   foreach($freevideo as $list)
				   {// pr($list);
						if(!empty($list['FreeVideo']['own_video_url']))
						{
							$video = $list['FreeVideo']['own_video_url'];
							
						}else
						{
							$video = $list['FreeVideo']['free_video_url'];
							//echo $video;die;
						}
				   ?>
                                    <div class="row video_sec video_sec2 video_sec_video_click" id="<?php echo base64_encode(convert_uuencode($list['FreeVideo']['id'])); ?>">
                                        <div class="col-lg-6 col-md-6 col-sm-6 VideoBoxLeft">
                                        
                                                 <?php 
									if(!empty($list['FreeVideo']['free_video_url']))
									{
										$count = strlen($video)-11;
										$embed = substr($video,$count); 
									
										echo $this->Html->link($this->Html->image('https://img.youtube.com/vi/'.$embed.'/0.jpg',array('class'=>'img-responsive', 'width'=>'480','height'=>'360', 'alt'=>'')),array('controller'=>'Home','action'=>'view_free_video',base64_encode(convert_uuencode($list['FreeVideo']['id']))),array('style'=>'','escape'=>false));
										
										   
									}
									else 
									if ($video != '' && file_exists('files/admin_free_video/'.$video."_smallThumb.png")) 
									{ 
										
										
										echo  $this->Html->link($this->Html->image('/files/admin_free_video/'.$video."_smallThumb.png",array('class'=>'img-responsive','width'=>'480','height'=>'360')),array('controller'=>'Home','action'=>'view_free_video',base64_encode(convert_uuencode($list['FreeVideo']['id']))),array('class'=>'','escape'=>false));  
									} else{
									
										echo $this->Html->link($this->Html->image('front/no_thumbnail_'.$locale.'.png',array('class'=>'img-responsive','width'=>'480','height'=>'360')),array('controller'=>'Home','action'=>'view_free_video',base64_encode(convert_uuencode($list['FreeVideo']['id']))),array('class'=>'','escape'=>false));  
										
									} 
								?>
                        
						<div class="vwcrsply_btn_thmb">
							
							<?php echo $this->Html->link('<i class="fa fa-play"></i>',array('controller'=>'Home','action'=>'view_free_video',base64_encode(convert_uuencode($list['FreeVideo']['id']))),array('escape'=>false,'class'=>'ply_btn_anchr_thmb')); ?>
						</div>
                                            <h2><?php
$title_v=$this->Text->truncate(strip_tags($list['FreeVideo']['title_'.$locale]),50,array( 'ellipsis' => '...','exact' => false,'html' => false));
 echo $this->Html->link($title_v,array('controller'=>'Home','action'=>'view_free_video',base64_encode(convert_uuencode($list['FreeVideo']['id']))),array('class'=>'course_link')); ?></h2>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 RatingBox">

                                            <p><?php echo $this->Text->truncate(strip_tags($list['FreeVideo']['description_'.$locale]),100,array('ellipsis'=>'...','exact'=>false));?></p>
                                        </div>        
                                    </div>
                                   <?php }} else {?>
                    
					<?php echo __('No free video available'); }?> 
                                </div>                                
                            </div>
                        
                        </div>                                               
                </div>
            </div>
        </div>
    </section>
       
    <!--End Tab Drop section --> 
     </div>
    <?php 
   /* Rating Script [Start] */
        echo $this->Html->script('front/star-rating.js');
?>
<script>
    jQuery(document).ready(function () {
        var locale_text=$('ul.dropdown-menu-cata li:first-child').text();                        
            $("#dropdown6:first-child").text(locale_text).append('<span class="caret"></span>' );;
            
       
        
        $(".dropdown-menu li a").click(function(){
            $("#dropdown6:first-child").text($(this).text()).append('<span class="caret"></span>' );
            $("#dropdown6:first-child").val($(this).attr('value'));
          });
        $(".rating").rating({
            starCaptions: function(val) {
                if (val < 3) {
                    return val;
                } else {
                    return 'high';
                }
            },
            starCaptionClasses: function(val) {
                if (val < 3) {
                    return 'label label-danger';
                } else {
                    return 'label label-success';
                }
            }
        });
          $('.rating').on('rating.change', function(event, value, caption) {
                var attr_id=$(this).attr('id');
                var arr = attr_id.split('input-21a-');
             
                var id =arr[1];
                var rating=value;
                if(req && req.readystate != 4){
			req.abort();
		}
               req=$.ajax({
                           url: ajax_url+"Home/update_rating",
                           type: "POST",
                           data: {course_id : id, rating: rating},
                           dataType: "html",
                           success :function(){
                                 $('.update_'+id).rating('update', rating);
  				// $('.to_rating_'+id).text(rating);


                           }
                     }); 
//                console.log(id);
//                console.log(value);
//                console.log(caption);
                });
           
	
	$(".plus, .minus").click(function(){
		var category = $(this).attr('rel');
    	if($(this).attr('class') == 'plus')
		{
			$(this).attr('class', 'minus');
			//$(this).removeClass( "plus" ).addClass( "minus" );
			//$(this).children().children().attr('src',"../img/remove-new.png");
		}
		else
		{
			$(this).attr('class', 'plus');
			//$(this).children().children().attr('src',"../img/add-item.png");
		}
		$(this).parent().children('.level_first').slideToggle("slow");
		
		$(".load_top").show();
		req = $.ajax({
			url:ajax_url+'Home/catalogue_landing/',
			type: 'get',
			data: {category: category},
			success: function(resp){
			
				$(".dscrtp_ryt").html(resp);	
				$(".load_top").hide();
			}
		});
		
	});
        
	$('.alpha_content span a').click(function(){
		var alphabet = $(this).attr('rel');
		
		if( !isNaN(alphabet))
		{
			$(this).parent().siblings().children().removeAttr('class');
		$(this).attr('class','actv_l');
		}else{
			$(this).parent().siblings().children().removeAttr('class');
		$(this).attr('class','actv_l');
		}
		$(".load_top").show();
		req = $.ajax({
			url:ajax_url+'Home/catalogue_landing/',
			type: 'get',
			data: {alphabet: alphabet},
			success: function(resp){				
				$(".alpha_dscrtp_ryt").html(resp);
				//alert(resp);	
				$(".load_top").hide();
			}
		});
	
	});
	
	$('.author_mem').click(function(){
		var authorId=$(this).attr('rel');
		$(".load_top").show();
		req = $.ajax({
			url:ajax_url+'Home/catalogue_landing/',
			type: 'get',
			data: {authorId: authorId},
			success: function(resp){				
				$(".author_wala_element").html(resp);	
				$(".load_top").hide();
			}
		});
	});
	
	$('.sub_category').click(function(){
		var sub_category=$(this).attr('rel');
		if($(this).attr('class')=='sub_category plus1')
		{
			
			$(this).removeClass( "plus1" ).addClass( "minus1" );
			$(this).next().slideToggle('slow');
		}
		else
		{
			$(this).removeClass( "minus1" ).addClass( "plus1" );
			$(this).next().slideToggle('slow'); 
		}
		$(".load_top").show();
		req = $.ajax({
			url:ajax_url+'Home/catalogue_landing/',
			type: 'get',
			data: {sub_category: sub_category},
			success: function(resp){				
				$(".dscrtp_ryt").html(resp);	
				$(".load_top").hide();
			}
		});
	});
	
	$('.sl_sub_category').click(function(){
		
		var sl_sub_category = $.trim($(this).attr('rel'));
		var sub_category = $.trim($(this).attr('id')); // sub category id only for other case
		$(".load_top").show();
		$.get(ajax_url+'Home/catalogue_landing/',{sl_sub_category: sl_sub_category,sub_category: sub_category}, function(resp){
			
			$(".dscrtp_ryt").html(resp);	
			$(".load_top").hide();
		});
	});
	
	$('.clk_ctgry_others').click(function(){
		var others_category=$(this).attr('rel');
		//alert(sub_category); return false;
		$(".load_top").show();
		req = $.ajax({
			url:ajax_url+'Home/catalogue_landing/',
			type: 'get',
			data: {others_category: others_category},
			success: function(resp){				
				$(".dscrtp_ryt").html(resp);	
				$(".load_top").hide();
			}
		});
	});
    });
</script>
