<?php 
$cookie_lessons = array();
		if(!empty($getCoursesindex)){
			
			foreach($getCoursesindex as $key=>$val){
				if(strlen($key)>8)
				{
					$cookie_lessons[] = convert_uudecode(base64_decode($val));
				}
				
			}
		}

$cookie_courses = array();
		if(!empty($getCoursesindex)){
			foreach($getCoursesindex as $course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
			}
			//pr($cookie_courses); die;
		}
?>

<!-- Start Banner section -->
<section class="banner-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="banner"><div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
  </ol>


  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    
    	
		<?php
		
		foreach($banners as $key=>$banner)
		{
		$bannerimage=$banner['Banners']['image'];
		$bannerlangcode=$banner['Banners']['lang_code'];
		?>
		<div class="item <?php echo ($key==0) ? 'active' : '' ; ?>">
		<?php
		if(!empty($bannerimage))
		{
		echo $this->Html->image($this->Utility->getAWSImgUrl(BANNER_BUCKET,'/'.$bannerlangcode."/".$bannerimage),array('class'=>'img-responsive','alt'=>'','width'=>'1140','height'=>'272'));
		}
		?>
		</div>
		<?php } ?>
  </div>

  <!-- Controls -->
  <?php 
  if(count($banners)>1)
  {
	?>
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
<?php } ?>
</div></div>
      </div>
    </div>
  </div>
</section>
<!--End  Banner section --> 

<!-- main container section -->
<section>
  <div class="container home_container">
    <div class="row home_landing_row">
    <!-- Needs to be optimized - 1st May -->
    <?php  
        foreach($courses as $list){ 
            $video = ''; $extension='';		$image='';	 $less_image='';		
            if(!empty($list['CourseLesson'])){ 
                foreach($list['CourseLesson'] as $lesson){
                    if($lesson['video'] != ''){
                        $video = $lesson['video'];
                        $extension=$lesson['extension'];
                        $image=$lesson['image'];
                        //pr($video); die;
                        break;
                    }
                }
        foreach($list['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                        $less_image=$lesson['image'];
                        break;
                    }
                }
            }								
    ?>	
    <!-- Needs to be optimized - 1st May -->

    <div class="video_sec " id="<?php echo base64_encode(convert_uuencode($list['Course']['id'])); ?>"> 
          <?php

           		$course_image = $list['Course']['image'];

          		$path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$course_image);
                 if(!empty($course_image) && $path){
                         echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'thumb/'.$course_image),array('width'=>'262','height'=>'138','class'=>'img-responsive')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'course_link','escape'=>false));        
                    }else if(!empty($less_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image)){ 
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$less_image),array( 'width'=>'262','height'=>'138','class'=>'img-responsive'));
                    }else if(!empty($image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$image),array('class'=>'img-responsive','width'=>'262','height'=>'138'));     
                    }else
                    {
                       echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('class'=>'img-responsive','width'=>'262','height'=>'138','url'=>array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id'])))),array());   
                    }
        ?>
        <h2><?php 
$title=$this->Text->truncate(strip_tags($list['Course']['title']),30,array( 'ellipsis' => '...','exact' => false,'html' => false));
echo  $this->Html->link($title,array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'')); ?></h2>
        <div class="row RatingBox">
         <?php
             $m_id = $this->Session->read('LocTrain.id');
             $decoded_id = $list['Course']['id'];		
			$is_rated=$list['CourseRating'];
			  $rate=''; 
			   for($i=0;$i<count($is_rated);$i++)
			   {			   
				 $rate+=$is_rated[$i]['rating'];
			   }							   
                         if(count($is_rated)>0){ $is_rated1=$rate/count($is_rated);}else{ $is_rated1=0;}
			   if(!empty($is_rated1))
			   {
			   if(count($is_rated)>1)
			   {
			 $rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
			   }
			   else
			   {
			   $rating=$is_rated1;
			   }
			    
			   }
			else
			{
				$rating= 0;
			}$login=0;
			if($this->Session->read('LocTrain.id'))
			{
			$login=1;
			}
		
		$first_lesson=isset($list['CourseLesson'])?$list['CourseLesson']['0']['id']:0;
                $subscription = $this->Utility->getCoursesubscriptionIndex($m_id,$decoded_id,@$first_lesson,$list['CoursePurchaseList']);
                /*$subscribed=$this->Utility->getCoursesubscribed($m_id,$decoded_id);*/
                
            ?>
        	<div class="col-xs-6 Rating">
                    <?php if($m_id != $list['Course']['m_id'] && $login==1) {?>
                       <?php if(!empty($subscription)  || $list['Course']['price']==0.00) { ?>
                          
                       <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                       <?php    } else {?>
                        <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                        <?php } }else{
						?>
                        <input id="input-21a-<?php echo $list['Course']['id'];?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                        <?php } ?>
               <!-- <div class="RatingNum"><span id="to_rating_<?php echo $list['Course']['id'];?>"><?= $rating; ?></span> Rating</div>-->
            </div>
            <div class="col-xs-6 Price"><?php /*echo $fmt->format($list['Course']['price']); */ 

if($list['Course']['price']=='0.00'){ 
$course_details=$this->Utility->getCourseLearningStatusIndex($m_id,$decoded_id,$list['CoursePurchaseList']);
$class = !empty($course_details)?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
if($class != 'c_added_learning_dsbl'){
echo __('free');
}else{
echo __('purchased');
}

}else{

/*$cookie_courses=$this->Utility->getcookie_courses($m_id);*/
$subscription_price= $this->Utility->getCoursesubscription_priceLeasson($list['CoursePurchaseList'],$list['CourseLesson'],$cookie_lessons);
if($login==1 && ($subscription_price || in_array($decoded_id,$cookie_courses) )){
echo __('purchased');
}else{
echo $this->Utility->externlizePrice($language,$locale,$list['Course']['price']);
}
}


?></div>
        </div>
        
       </div>
  
        <?php } ?>
    </div>
  </div>
</section>
<!--End  main container section -->
<?php 
   /* Rating Script [Start] */
    echo $this->Html->script('front/star-rating.min.js');
//echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/front/star-rating.min.js'));
?>
<script>
    jQuery(document).ready(function () {
	
        $('.video_sec').on('click',function(e){
        if( $(e.target).hasClass("rating-container")){

        }else if( $(e.target).hasClass("rating-stars")){

        }else{
        window.location=ajax_url+'Home/view_course_details/'+$(this).attr('id');
        }
        });
        $('body').addClass('homeBody');
                $(".rating").rating({
                    starCaptions: function(val) {
                        if (val < 3) {
                            return val;
                        } else {
                            return 'high';
                        }
                    },
                    starCaptionClasses: function(val) {
                        if (val < 3) {
                            return 'label label-danger';
                        } else {
                            return 'label label-success';
                        }
            }
        });
        $('.rating').on('rating.change', function(event, value, caption) {
                var attr_id=$(this).attr('id');
                var arr = attr_id.split('input-21a-');
                var id =arr[1];
                var rating=value;
                $.ajax({
                           url: ajax_url+"Home/update_rating",
                           type: "POST",
                           data: {course_id : id, rating: rating},
                           dataType: "html",
                });
        });
   });
</script>
