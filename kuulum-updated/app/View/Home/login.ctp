<script type="text/javascript">
var array = {'emailId':'<?php echo __("please_enter_your_registered_e-mail_address");?>','password':'<?php echo __("please_enter_your_password");?>'};
$(document).ready(function(){	

	$(".term_chk_box").click(function(){
		var getId = $("#remeberInput").val();		
		if(getId == '1'){
			$(this).css({'background-position':'0 0px'});
			$("#remeberInput").val('0');
		}
		else if(getId == '0'){	
		
			$(this).css({'background-position':'0 -30px'});
			
			$("#remeberInput").val('1');
		}
	});
	$(':input').live('focus',function(){
		var getId = $(this).attr('id');
		switch(getId)
		{
			case 'emailId':				
				$(".guide_info").text(array['emailId']);
			break;			
			case 'password':				
				$(".guide_info").html(array['password']);			
			break;			
		}			
	});

});
</script>
<!--Wrapper div Start (AD)-->
<div id="wrapper"> 
  <!--Div Middle Start (AD)-->
  <div id="middle">
  	<?php if($this->Session->read("LocTrain.flashMsg")!=""){ ?>
    	<div class="account-activated">	<?php echo $this->Session->read(__("LocTrain.flashMsg")); ?></div>
	<?php $this->Session->delete("LocTrain.flashMsg");  }?>
    
    <div class="breadcrumb_botcont">
	<?php echo $this->Form->create('Login',array('id'=>'memberLogin','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'member_login')));?>
      <div class="formfld_cont">
        <div class="reg_flds">
          <label class="regfld_label"><?php echo __('e-mail_address');?>: </label>
          <span class="regfld_txtcont">
      		<?php echo $this->Form->input('email',array('type'=>'text','id'=>'emailId','class'=>'regfld_txt gradient','label'=>false,'div'=>false,'value'=>$cookieVal)); ?>
		  
		   <div id="err_email" class="register_error"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>
          </span> </div>
        <div class="reg_flds">
          <label class="regfld_label"><?php echo __('password');?>:</label>
          <span class="regfld_txtcont">
		  <?php echo $this->Form->input('password',array('type'=>'password','id'=>'password','class'=>'regfld_txt gradient','label'=>false,'div'=>false)); ?>
		  <div id="err_password" class="register_error" style="width: 400px;"> <?php if(isset($error['password'][0])) echo $error['password'][0];?> </div>
           <div class="login_fldfrgt"> <?php echo $this->Html->link(__('help_i_forgot_my_password'),'#forgot-window',array('id'=>'popUp','class'=>'forgot-window'));?></div>
           <div class="login_fldfrgt"> <?php echo $this->Html->link(__('not_registered_yet'),array('controller'=>'Home','action'=>'register'));?></div>    
          </span> 
          </div>
        <div class="reg_flds">
          <label class="regfld_label">&nbsp;</label>
          <span class="regfld_txtcont">
              <div class="login_fldrmmbr"> 
              
              </div>        
            
          </span> 
         </div>
      
      </div>
 
      <div class="formfld_rgt">
            <div class="section_guide"> <span class="guide_upper"> <?php echo __('in_this_section');?> </span>
              <h2 class="guide_head"> <?php echo __('sign_in'); ?> </h2>
              <div class="guide_info"> <?php echo __('enter_your_e-mail_address_and_password_to_sign_in');?>  </div>
              <div class="guide_bot"> <span class="option"> <?php echo __('options');?> </span>
                <div class="accept_terms">          
                	<?php $class = ($cookieVal != '')?'chk_chkd':''; ?>      	   
                    <div id="remeberInput_1" class="term_chk_box chkbox_class <?php echo $class; ?>">                  		
                    	<?php echo $this->Form->input('remember',array('type'=>'hidden','id'=>'remeberInput','value'=>$cookieVal != NULL ? '1':'0')); ?>
                    </div>
                    <span class="accepted"> <?php echo __('remember_my_e-mail_address');?></span> 
                    <div id="err_term_condition" class="register_error"> <?php if(isset($error['term_condition'][0])) echo $error['term_condition'][0];?> </div>
                </div>               
                <?php echo $this->Form->submit(__('sign_in'),array('class'=>'sc_btns disRegisBut','div'=>'section_links','onclick'=>"return ajax_form('memberLogin','Home/login_check_ajax','load_top')")); ?>
                <?php echo $this->Form->submit(__('cancel'),array('class'=>'sc_btns disRegisBut', 'div'=>'section_links', 'onclick'=>'memberLogin.action="'. HTTP_ROOT.'Home/index"')); ?>
              </div>
            </div>
          </div>
    
      <?php echo $this->Form->end(); ?>
	  <?php echo $this->element('frontElements/home/forgot_password'); ?>  
    </div>
  </div>
  <!--Div Middle End (AD)--> 
  
  
</div>
<!--Wrapper div End (AD)--> 



<script type="text/javascript">
$(document).ready(function() {
	$('a.forgot-window').click(function() {
		
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');
		var email =$('#emailId').val();
		$('#forgotemail').val(email);
		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').live('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
	
	
	$(".submit_button").live('click',function(){
		var userEmail = $("#forgotemail").val();
			$.ajax({
			url:ajax_url+'Home/forgot_password/',
			type: 'post',
			data: {'data[Member][email]':userEmail},
			success:function(resp){
			var msg= resp.split('-');
				if(msg[0] == 'sucess'){
				    $("#err_reset_email").html("");
					$('#mask , .login-popup').fadeOut(300 , function() {
						$('#mask').remove();  					
					}); 
					$('.account-activated').html(msg[1]);
					$('.account-activated').show();
					$('.account-activated').fadeOut(10000);
				}else{
					$("#err_reset_email").html(resp);
				}
			}
		});	
	});
});

</script>