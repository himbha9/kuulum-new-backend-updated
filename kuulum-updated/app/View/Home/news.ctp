<?php $locale=$this->Session->read('LocTrain.locale');?>
<!-- main container section -->
 <?php 
  
        echo $this->Html->css('front/BeatPicker.min.css');
        echo $this->Html->script('front/BeatPicker.min.js');
?>
<script type="text/javascript">
function changeTooltipColorTo(color,border_c) {
    $('.tooltip-inner').css('background', border_c);
	$('.tooltip-inner').css('color', color);
	//$('.tooltip-inner').css('border','2px solid #000');
	$('.tooltip-inner').css({'border-radius':'50%'});
 	// $('.tooltip-arrow').css({'z-index':'9999','position':'absolute','bottom':'2px'});
    $('.tooltip.top .tooltip-arrow').css('border-top-color', border_c);
    $('.tooltip.right .tooltip-arrow').css('border-right-color', border_c);
    $('.tooltip.left .tooltip-arrow').css('border-left-color', border_c);
    $('.tooltip.bottom .tooltip-arrow').css('border-bottom-color', border_c);
}

$(function () { $("[data-toggle='tooltip']").tooltip(); });
$('body').on('shown.bs.tooltip', function () {
  //changeTooltipColorTo('#fff','#000');
})
var req = '';
function getDate(inputVal){
  var locale='<?php echo $this->Session->read('LocTrain.locale'); ?>';
  var d = inputVal.getDate(); 
  var m=inputVal.getMonth()+1; 
  var y=inputVal.getFullYear();   
  if(d<10)
  {
    var d_n= '0'+d;  
  }else{
      var d_n= d;  
  }
  
  if(m <10)
  {
    var m_n= '0'+m;  
  }else{
      var m_n= m;  
  }
  switch (locale)
        {
           case 'en': return  y+'-'+m_n+'-'+d_n;
           case 'de': return  d_n+'.'+m_n+'.'+y;
           case 'ar': return  y+'/'+m_n+'/'+d_n;
           case 'fr': return  y+'/'+m_n+'/'+d_n;
           case 'it': return  y+'/'+m_n+'/'+d_n;
           case 'ja': return  y+'/'+m_n+'/'+d_n;
           case 'ko': return  y+'-'+m_n+'-'+d_n;
           case 'pt': return  y+'/'+m_n+'/'+d_n;
           case 'ru': return  y+'.'+m_n+'.'+d_n;
           case 'zh': return  y+'-'+m_n+'-'+d_n;    
           case 'es': return  d_n+'/'+m_n+'/'+y;
           default:   return  y+'-'+m_n+'-'+d_n;
        }
  
  
  
}
 function convertData(locale,date){  		
	switch (locale)
        {
           case 'en': return date.replace(/-/gi,'-');
           case 'de': return date.replace(/\./gi,'-');
           case 'ar': return date.replace(/\//gi,'-');
           case 'fr': return date.replace(/\//gi,'-');
           case 'it': return date.replace(/\//gi,'-');
           case 'ja': return date.replace(/\//gi,'-');
           case 'ko': return date.replace(/-/gi,'-');
           case 'pt': return date.replace(/\//gi,'-');
           case 'ru': return date.replace(/\./gi,'-');
           case 'zh': return date.replace(/-/gi,'-');
           case 'es': return date.replace(/\//gi,'-');          
           default:return date.replace(/-/gi,'-');
        }
		
		
}
$(document).ready(function(){
	author_id=0;
	$('#news_filter').slideUp(500);
	
        $("#dropdown7:first-child").text('<?php echo __("all_authors"); ?>').append('<span class="caret"></span>' );
        $("#dropdown7:first-child").val('0');
        $(".dropdown-menu li a").click(function(){    
        $("#dropdown7:first-child").text($(this).text()).append('<span class="caret"></span>' );
	/*$('.df').attr('id','');
    	$('.dt').attr('id','');
	 $('#from_date').val('');
        $('#to_date').val('');*/
        $("#dropdown7:first-child").val($(this).attr('value'));
	$('#author').attr('value',$(this).attr('value'));
			DateFrom = $('.df').attr('id');
                        DateTo = $('.dt').attr('id');
                        $(".loading_inner").show();
			var auth_id = $('#author').attr('value');
			store_auth_id(auth_id);                       
                      	req = $.ajax({
			url:ajax_url+'Home/news/',
			type: 'get',
			data: {'type':'1','author':auth_id,'DateFrom':DateFrom, 'DateTo':DateTo},
			success: function(resp){				
				
				$(".tchng_cont_content").html(resp);	
				$(".load_top").hide();
			}
		});
        
    
    });
from_date.on("show", function () {
//from_date.today;
});
	
	 from_date.on("select", function (data) {
                $("#from_date").parent().parent('.date_selected').attr('id',data.string);
			$("#to_date").val('');$('.dt').attr('id','');
			DateFrom = $('.df').attr('id');
                        DateTo = $('.dt').attr('id');
                        $(".loading_inner").show();
                        auth_id	= store_auth_id(0);
			 $(".loading_inner").show();
                        req = $.ajax({
                                        url:ajax_url+'Home/news/',
                                        type: 'get',
                                        data: {'type':'2','author':auth_id,'DateFrom':DateFrom,'DateTo':DateTo},
                                        success: function(resp){
						
                                                $(".tchng_cont_content").html(resp);	
						if($(".tchng_cont_content").attr('id')==0){
						$(".tchng_cont_content").html('<div class="mytch_rcd"><?php echo __("no_news_matches_the_filters"); ?></div>');	
						}
                                                $(".loading_inner").hide();
                                        }
                        });
               
                });

	     to_date.on("select", function (data) {
                    $("#to_date").parent().parent('.date_selected').attr('id',data.string);
                        DateFrom = $('.df').attr('id');
                        DateTo = $('.dt').attr('id');
                        $(".loading_inner").show();
                        auth_id	= store_auth_id(0);
			 $(".loading_inner").show();
                        req = $.ajax({
                                        url:ajax_url+'Home/news/',
                                        type: 'get',
                                        data: {'type':'2','author':auth_id,'DateFrom':DateFrom,'DateTo':DateTo},
                                        success: function(resp){
						
                                                $(".tchng_cont_content").html(resp);	
						if($(".tchng_cont_content").attr('id')==0){
						$(".tchng_cont_content").html('<div class="mytch_rcd"><?php echo __("no_news_matches_the_filters"); ?></div>');	
						}
                                                $(".loading_inner").hide();
                                        }
                        });
                     
               	
                    });


	$('.return_to').on('click',function(){
		$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
	});
	
	
	
	 function store_auth_id(d)
	{
		if(d==0)
		{
			return $('#author').attr('value');
		}
		else
		{
			author_id=d;
		}
	}
	
	
$('#turn_on_off_chk').click(function(event){	
	event.preventDefault();
    var current_filter=$(this).val(); 
    var   on_off_div= $('#on_off_div').val();    
    if(current_filter=='turn_filter_on' || current_filter=='')
    {
        $(this).val('turn_filter_off');
        $(this).text('<?php echo __("turn_filter_off");?>');
        //slides the respective content to show
        $('#news_filter').slideDown(500);
       auth_id	= store_auth_id(0);
	DateFrom = $('.df').attr('id');
	DateTo = $('.dt').attr('id');
        
    }else{
        $(this).val('turn_filter_on');
        $(this).text('<?php echo __("turn_filter_on");?>');
        //slides the respective content to show   
 	$('#news_filter').slideUp('slow');
	auth_id	=0;
    	DateFrom ='';
	DateTo='';	
	
    }
	
 	$(".tchng_cont_content").attr('id','0');	
	//$(".tchng_cont_content").html('');
	$(".loading_inner").show();
req = $.ajax({
			url:ajax_url+'Home/news/',
			type: 'get',
			data: {'type':'3','limit':'0','author':auth_id,'DateFrom':DateFrom, 'DateTo':DateTo},
			success: function(resp){				
				
				$(".tchng_cont_content").html(resp);	
				$(".loading_inner").hide();
			}
		});

});
    $('#reset_filter').click(function(){
        $('#from_date').val('');
        $('#to_date').val('');
	 $(".tchng_cont_content").attr('id','0');
	$('#author').attr('value','0');
	 $('.df').attr('id','');
            $('.dt').attr('id','');
	auth_id	=0;	
	 $("#dropdown7:first-child").text('<?php echo __("all_authors"); ?>').append('<span class="caret"></span>' );
        $("#dropdown7:first-child").val('0');
        //$(".tchng_cont_content").html('');
		req = $.ajax({
			url:ajax_url+'Home/news/',
			type: 'get',
			data: {'type':'3','limit':'0','author':auth_id},
			success: function(resp){				
				
				$(".tchng_cont_content").html(resp);	
				$(".loading_inner").hide();
			}
		});

     });
<?php if(!empty($news_list)) { ?>
$(document).on('scroll',function(){
 
	if ($(window).scrollTop() == $(document).height() - $(window).height()){		
		var getId = $(".tchng_cont_content").attr('id');
		auth_id	= store_auth_id(0);
		DateFrom = $('.df').attr('id');
               	DateTo = $('.dt').attr('id');
		/*if(req && req.readystate != 4){
			req.abort();
		}*/		
		if(getId != '0'){
                      // $('.mytch_rcd').remove();
			$(".loading_inner").show();	
			/*req = $.ajax({
				url:ajax_url+'Home/news/'+getId,
				success: function(resp){	
					$(".tchng_cont_content").append(resp);	
					$(".loading_inner").hide();
				}
			});*/
 var current_filter=$('#turn_on_off_chk').val();// alert(current_filter);
    var   on_off_div= $('#on_off_div').val();    
	    if(current_filter=='turn_filter_off')
	    {
	req = $.ajax({
			url:ajax_url+'Home/news/',
			type: 'get',
			data: {'type':'3','limit':getId,'author':auth_id,'DateFrom':DateFrom, 'DateTo':DateTo},
			success: function(resp){				
				
				$(".tchng_cont_content").append(resp);	
				$(".loading_inner").hide();
			}
		});
	}else{
		req = $.ajax({
			url:ajax_url+'Home/news/',
			type: 'get',
			data: {'type':'3','limit':getId,'author':'0','DateFrom':'', 'DateTo':''},
			success: function(resp){				
				
				$(".tchng_cont_content").append(resp);	
				$(".loading_inner").hide();
			}
		});
	}
			
          
		}	
	}
});
<?php } ?>
});
</script>
<section>
<div class="newsContent">
  <div class="container">
    <div class="row">
    <div class="col-lg-12">
    
    <div class="recent_news">
<?php //pr($latest_news_list);

?>
        <?php if(isset($latest_news_list) && !empty($latest_news_list)){
            foreach($latest_news_list as $row){ ?>
                <div class="r_news1">
                <?php 
                
                $info = new SplFileInfo($this->Utility->getAWSImgUrl(NEWS_BUCKET,'thumbnails/'.$row['News']['image']));
				$info1 = new SplFileInfo($this->Utility->getAWSImgUrl(NEWS_BUCKET,'original/'.$row['News']['image']));

            if($row['News']['image'] != '' && $info->getExtension()) { 
                    echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'thumbnails/'.$row['News']['image']),array('alt'=>$row['News']['image'],"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$row['News']['id']));
                }else if($row['News']['image'] != '' && $info1->getExtension()){
                   echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'original/'.$row['News']['image']),array('alt'=>$row['News']['image'],"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$row['News']['id']));
		 }else{
                    echo $this->Html->image('front/no_img_small.png',array('alt'=>'no_img_small.png',"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$row['News']['id']));
                }
                
                
               
                ?>
                    <?php //echo $this->Session->read('Config.language');
			$date_news=$this->Timezone->dateFormatAccoringLocaleLangNew($this->Session->read('Config.language'),$row['News']['date_added']);                  
                    $date_month=$this->Timezone->dateFormatAccoringLocaleLangNewsMonth($this->Session->read('Config.language'),$row['News']['date_added']);
                    $date_day=date('d',$row['News']['date_added']);

                    ?>
                <div class="date_month recentNewsDateMonth" data-toggle="tooltip" data-placement="top" title="<?php echo $date_news;?>">                    
                <div class="date"> <?php echo $date_day; ?></div><?php echo $date_month ?> </div>
                <h5><?php $link_text = $this->Text->truncate(strip_tags($row['News']['title_'.$locale]),60,array( 'ellipsis' => '...','exact' => false,'html' => false)); ?>
            
                    <a data-toggle="modal" data-target="#resent_news_<?php echo $row['News']['id']; ?>"><?php echo $link_text;?></a>
                </h5>
                <div class="clearfix"></div>
                <p><?php echo $this->Text->truncate(strip_tags($row['News']['description_'.$locale]),100,array( 'ellipsis' => '...','exact' => false,'html' => false)); ?> </p>

                </div> 
        <!-- model -->
            <div class="modal fade newsPopupWidth" id="resent_news_<?php echo $row['News']['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                   <div class="date_month" data-toggle="tooltip" data-placement="top" title="<?php echo $date_news;?>">                    
                                        <div class="date"> <?php echo $date_day; ?></div><?php echo $date_month ?> </div>
                                    <h4 class="modal-title" id="myModalLabel">
                                       
                                        <?php echo $link_text = strip_tags($row['News']['title_'.$locale]); ?></h4>
                                      <div class="clearfix"></div>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12 rawmodal">
                                            <div class="embed-responsive embed-responsive-16by9 pull-left NewsArticleImg">
                                                <?php 
 if($row['News']['image'] != '' && $info->getExtension()) { 
                    echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'thumbnails/'.$row['News']['image']),array('alt'=>$row['News']['image'],"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$row['News']['id']));
                }else if($row['News']['image'] != '' && $info1->getExtension()){
                   echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'original/'.$row['News']['image']),array('alt'=>$row['News']['image'],"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$row['News']['id']));
		 }else{
                    echo $this->Html->image('front/no_img_small.png',array('alt'=>'no_img_small.png',"class"=>"img-responsive",'width'=>'262','height'=>'138','data-toggle'=>"modal",'data-target'=>"#news_".$row['News']['id']));
                }
                                                ?>
                                        </div>
                                           
                                             <div class="Title"><?php echo $row['News']['sub_title_'.$locale];?></div>
                                        <p> <?php echo $row['News']['description_'.$locale];?></p>
                                    </div>
                                </div>
                                </div>                            
                            </div>
                        </div>
            </div>
          <?php  }
        }else{ ?>
        <div class="r_news1">
   
          <p><?php echo __('no_courses_found');?></p>

      </div>
      <?php  }
        ?>
      

    </div>
    
    
    </div>
    
    </div>
  </div>
  </div>
</section >

<section class="section-news">
  <div class="container">  
    <div class="row">
            	<div class="col-lg-12">
                	<div class="tabbable margintop20">
                            <ul class="nav nav-tabs">   
                                <li class="active"><a data-toggle="tab"  id="turn_on_off_chk" value="turn_filter_on"><?php echo __('turn_filter_on');?></a></li>
                            </ul>
                        </div>                                               
                </div>
            </div> 
            
            <div class="FilterBox" id="news_filter">
                                    
                                    	<div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1"><?php echo str_replace(':','',__('filter_by_author'));?></label>
 <?php echo $this->Form->input('author',array('type'=>'hidden','id'=>'author','class'=>'','label'=>false,'div'=>false,'value'=>'0')); ?> 
                                                    <div class="dropdown DropdownField" id="">
                                                        <button data-toggle="dropdown" id="dropdown7" type="button" class="btn btn-default dropdown-toggle Textname">Dropdown<span class="caret"></span></button>
                                                        <ul aria-labelledby="dropdown7" role="menu" class="dropdown-menu">
<?php if(!empty($news_author)) {  ?>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="0" class="sb_auth" id="0"><?php echo __('all_authors'); ?></a></li>
<?php foreach($news_author as $key=>$author) { ?>  
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;"  class="sb_auth" value="<?php echo $key;?>" id="<?php echo $key;?>"><?php echo $author; ?></a></li>
 <?php }?>
                                        <?php }else{ ?> 
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" class="sb_auth"><?php echo __('no_records_found_'); ?></a></li>
<?php } ?>       
                                                        </ul>
                                                    </div>   
                                                </div>
                                            </div>
                                        </div>
                                    <div id="date_div">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                                <div class="form-group  date_selected df" id="">
                                                    <label for="published_from"><?php echo __('filter_by_date_published_from');?></label>
                                                    <?php
                                                    $str_date=date('Y,n,j', strtotime("+1 days")); 
                                                    $from_disable_date="{from:[".$str_date."],to:'>'}";
                                                    ?>
                                                    <input  type="text" name="from_date" data-beatpicker-id="from_date" data-beatpicker="true" placeholder="" id="from_date" data-beatpicker-module="gotoDate" data-beatpicker-disable="<?php echo $from_disable_date;?>" data-beatpicker-format="<?php echo $this->Timezone->dateFormatAccoringLocaleSearch($this->Session->read('LocTrain.locale'))?>"  class="form-control DatePicker">
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                               <div class="form-group  date_selected dt" id="">
                                                    <label for="published_from"><?php echo __('filter_by_date_published_to');?></label>
                                                    <input  type="text" name="to_date" data-beatpicker-id="to_date"  data-beatpicker="true" placeholder="" id="to_date" data-beatpicker-module="gotoDate" data-beatpicker-disable="<?php echo $from_disable_date;?>"  data-beatpicker-format="<?php echo $this->Timezone->dateFormatAccoringLocaleSearch($this->Session->read('LocTrain.locale'))?>"  class="form-control DatePicker">
                                                </div>                                            
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                    	<div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                            <span class="Link"><a id="reset_filter"><?php echo __('reset_filter');?></a></span>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
  
    <div class="row">
    <div class="col-lg-12">
            <div class="tchng_cont_content" id="<?php echo $last_id;?>">
            	<?php if(!empty($news_list)) { ?>
                	<?php echo $this->element('frontElements/home/news_list'); ?>
                <?php }else{ ?>
                    <div class="mytch_rcd">
                        <?php echo __('no_records_found_'); ?>
                    </div>
                <?php } ?>                       	
            </div>
            <div class="loading">
                   <div class="loading_inner">
                       <?php echo $this->Html->image('front/ajax-loader.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                   </div>
             </div>
          
    </div>
    
    </div>
  </div>
</section>
<!--End  main container section -->