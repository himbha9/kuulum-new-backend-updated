<script type="text/javascript">
$(document).ready(function(){
	$('.return_to').live('click',function(){
		$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
	})
});
</script>

<div id="wrapper"> 
  <div id="middle">
    <div class="breadcrumb_botcont">
        <div class="tchng_cont">
        	<?php if(!empty($news)) { ?>
                <div class="vw_tchng_content">
                    <div class="vw_cntn_ilft"><h2> <?php echo $news['News']['title_'.$locale];?> </h2> <p> <?php echo date('d F Y, H:i e',$news['News']['date_modified']);?></p> </div>
                    <div class="vwcntnt_hydng_ryt">
                         <?php if($news['News']['image'] != '' && file_exists('img/news/original/'.$news['News']['image'])) { 
                                echo $this->Html->image('news/original/'.$news['News']['image'],array('alt'=>$news['News']['image'],'width'=>467,'height'=>'262'));
                            }else if($news['News']['image'] != '' && file_exists('img/news/thumbnails/'.$news['News']['image'])){
                                echo $this->Html->image('news/original/'.$news['News']['image'],array('alt'=>$news['News']['image'],'width'=>467,'height'=>'262'));
                            }else{
                                echo $this->Html->image('front/no_image.png',array('alt'=>'no_img_small.png','width'=>300,'height'=>'180'));
                            }
                         ?>                	
                    </div>
                </div>
                <div class="vw_ftct">
                    <span class="vw_tp_ryt">
                        <h3 id="vwsection_subtitle"> <?php echo $news['News']['sub_title_'.$locale];?> </h3>
                        <div class="vwsection_desc">
                            <?php echo $news['News']['description_'.$locale];?>
                        </div>                   
                    </span>  
                    <span class="vw_mn_btmnd">
                        <span class="vw_ls_btn">
                             <?php echo $this->Html->link(__('return_to_top'),'javascript:void(0);',array('class'=>'return_to')); ?> 
                        </span>
                        <span class="vw_ls_btn_ryt">
                        <!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
							<a class="addthis_button_email eml_bg"></a>						
							<a style="margin-left:10px; " class="addthis_button_print print_bg"></a>
							<a style="margin-left:10px; " class="addthis_button_compact share_bg"></a>
						<!--<a class="addthis_button_preferred_2"></a><a class="addthis_button_preferred_3"></a>-->
						<!--<a class="addthis_counter addthis_bubble_style"></a>-->
						</div>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-51e508fe2ccf7da5"></script>
						<!-- AddThis Button END -->
                        </span>
                      </span>	
                </div>
			<?php }else{ ?>
                <div class="mytch_rcd">
                    <?php echo __('no_records_found'); ?>
                </div>
            <?php } ?>
        </div>
        <div class="vw_formfld_rgt">
        <div class="vw_section_guide"> 
            <span class="vw_guide_upper"> <?php echo __('in_this_section');?>
            </span>
            <h2 class="vw_guide_head border-top"> <?php echo __('archive'); ?> </h2>
            <div class="vw_guide_bot"> 
                <span class="vw_option"> <?php echo __('related_links');?> 
                </span>
                <div class="vw_section_links">
                   <?php  if(!empty($news_others))
							{
								foreach($news_others as $n_other)
								{
										$id=base64_encode(convert_uuencode($n_other['News']['id']));
				   ?>
					
					<span class="sct_lft">
                        <?php echo $this->Html->link($n_other['News']['title_'.$locale],'/Home/news_landing/'.$id,array('class'=>'vw_scn_link_new')); ?>
                        <p class="dated_january"><?php echo date('d F Y, h:i e',$n_other['News']['date_modified']); ?></p>
                    </span>
                    <?php
							}
						}
					?>                    
                    <span class="sct_lft1">  
                        <?php echo $this->Html->link(__('view_all_news'),array('controller'=>'Home','action'=>'news'),array('class'=>'vw_scn_link_new'));?>
                    </span>
					
					
                </div>                
          </div>
        </div>
        </div>
    </div>
  </div>
</div>
<style>
.at15t_compact
{
	background:none;
}
.at16nc
{
	background:none;
}	
</style>