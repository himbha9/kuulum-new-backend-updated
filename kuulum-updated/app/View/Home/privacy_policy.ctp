<!-- main container section -->

    <section>
		<div class="container InnerContent CMS_Common_Pages">
        	<div class="row">
            	<div class="col-lg-12">
                	<h4 class="modal-title"><?php echo $privacy_policy['CmsPage']['page_title']; ?></h4>
                   <?php echo $privacy_policy['CmsPage']['description']; ?>
                                        
                </div>
            </div>
            
          
            
        </div>    
     
    </section>

<!--End  main container section -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.return_to').on('click',function(){
			$('body,html').animate({
					scrollTop: 0
				}, 500);
				return false;
		});
                $('a[rel=shareit], #shareit-box').mouseenter(function() {		
		
		//get the height, top and calculate the left value for the sharebox
		var height = $(this).height();
		var top = $(this).offset().top;
		
		//get the left and find the center value
		var left = $(this).offset().left + ($(this).width() /2) - ($('#shareit-box').width() / 2);		
		
		//grab the href value and explode the bar symbol to grab the url and title
		//the content should be in this format url|title
		var value = $(this).attr('href').split('|');
		
		//assign the value to variables and encode it to url friendly
		var field = value[0];
		var url = encodeURIComponent(value[0]);
		var title = encodeURIComponent(value[1]);
		
		//assign the height for the header, so that the link is cover
		$('#shareit-header').height(height);
		
		//display the box
		$('#shareit-box').show();
		
		//set the position, the box should appear under the link and centered
		$('#shareit-box').css({'top':top, 'left':left});
		
		//assign the url to the textfield
		$('#shareit-field').val(field);
		
		//make the bookmark media open in new tab/window
		$('a.shareit-sm').attr('target','_blank');
		
		//Setup the bookmark media url and title
		$('a[rel=shareit-mail]').attr('href', 'http://mailto:?subject=' + title);
		$('a[rel=shareit-linkedin]').attr('href', 'http://del.icio.us/post?v=4&amp;noui&amp;jump=close&amp;url=' + url + '&title=' + title);
		$('a[rel=shareit-facebook]').attr('href', 'http://www.facebook.com/submit.php?url='  + url + '&amp;title=' + title);
		$('a[rel=shareit-twitter]').attr('href', 'http://twitter.com/home?status=' + title + '%20-%20' + title);
		
	});

	//onmouse out hide the shareit box
	$('#shareit-box').mouseleave(function () {
		$('#shareit-field').val('');
		$(this).hide();
	});
	
	//hightlight the textfield on click event
	$('#shareit-field').click(function () {
		$(this).select();
	});
	});

function PrintDiv()
 {
	 window.open('<?php echo HTTP_ROOT?>Home/print_content/<?php echo base64_encode(convert_uuencode($cookie_policy['CmsPage']['id'])); ?>', '_blank', 'width=700,height=700');
 }   

	
</script>
