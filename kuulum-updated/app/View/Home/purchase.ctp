<?php echo $this->Html->script('Scripts/jquery.msgBox.js');?>
<?php echo $this->Html->css('front/msgBoxLight.css');?>
<script type="text/javascript">
	function to_numbers (str,limit)
			{var is_number = /[0-9]/;
				var formatted = '';
				for (var i=0;i<(str.length);i++)
				{
					char_ = str.charAt(i);
					if (formatted.length==0 && char_==0) char_ = false;

					if (char_ && char_.match(is_number))
					{
						formatted = formatted+char_;
					}
				}

				return formatted;
			}
			function fill_with_zeroes (str)
			{var centsLimit=2;
			 while (str.length<(centsLimit+1)) str = str+'0';
								return str;
			}
			function fill_with_decimal(str){
				var centsLimit=2;
				while (str.length<(centsLimit)) str = str+'0';
				return str;
				
				}
var format = function(num,locale,pre,suf,cenSep,thouSep){	var centsLimit=2;	
	if(pre == ''){
		var str = num.toString().replace(suf, ""), parts = false, output = [], i = 1, formatted = null;
		}else{
			var str = num.toString().replace(pre, ""), parts = false, output = [], i = 1, formatted = null;
		}
		
	/*	if(str.indexOf(cenSep) > 0){
			partsw = str.split(cenSep);
			str=((to_numbers(partsw[0]))?to_numbers(partsw[0]):0)+cenSep+fill_with_decimal(to_numbers(partsw[1]));
		}else{		
		partsw=(to_numbers(str,6))?to_numbers(str,6):0;
		str=partsw+cenSep+'00';
		}	*/
	if(str.indexOf(cenSep) > 0) {
		parts = str.split(cenSep);
		str = parts[0];
	}
	str = str.split("").reverse();
	for(var j = 0, len = str.length; j < len; j++) {
		if(str[j] != thouSep) {
			output.push(str[j]);
			if(i%3 == 0 && j < (len - 1)) {
				output.push(thouSep);
			}
			i++;
		}
	}
	formatted = output.reverse().join("");
	if(pre == ''){
		return(formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : "")+' '+suf);
	}else {
	return(pre + formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : ""));
	}
};
function externalizePrice(price){
	var locale='<?php echo $this->Session->read('LocTrain.locale');?>';
					if(locale=='en'){						
							return format(price,locale,'$','','.',',');		
						}else if (locale=='de'){
							price=price.replace(".", ","); 
							return format(price,locale,'',' $',',','.');		
						}else if (locale=="es"){
							price=price.replace(".", ","); 
							return format(price,locale,'$','',',','.');
						}else if (locale=='zh'){
							price=price.replace(".", "."); 	
							return format(price,locale,'$','','.',',');
						}else {
							price=price.replace(".", "."); 
							return format(price,locale,'$','','.',',');			
						}
	}
$(document).ready(function(){
  
	
	$('.aply_dscnt_otr').show();
	
		
	$('.aply_dscnt_sbmt').click(function(){
		
		var discount_code = $('.aply_dscnt_ipt').val();
		if(discount_code=='' || discount_code==null)
		{
			$('#err_code').html("<?php echo __('field_is_required');?>");
			return false;
		}
		$(this).parent('div').attr('rel',discount_code);
		$(".load_top").show();
		req = $.ajax({
		
			url:ajax_url+'Home/find_discount/',
			type: 'post',
			data: {'discount_code':discount_code},
			success: function(resp){
				
				$(".load_top").hide();
				if(isNaN(resp))
				{
					$('#err_code').html(resp+". <?php echo __('try_again');?>.");
				$('#discount_percent').html('');
                                        $('#shop_condition').hide();
				}
				else
				{         //$('#shopping_apply_discount').modal('hide');
                                        $('.disable_apply_code').attr('data-target','');
					$('#err_code').html('');
					//var price = $.trim($('.ryt_contcoursget').attr('id'));
					
					$('#discount_val').val($.trim(resp))
					var discount = $.trim(resp);
					//var net_discount=(discount*price)/100;
					//net_discount=net_discount.toFixed(2);
					
				/*	if(discount>=10 && discount%parseInt(discount)==0)
					{
					var discount_percent=(discount/100).toFixed(2);
					}
					else if (discount<10 || discount%parseInt(discount)!=0)
					{
					var discount_percent=(discount/100);
					}*/
					var discount_percent=discount;
					$('#discount_percent').html(discount_percent+'%');
                                        $('#shop_condition').show();
					/*$('.aply_dscnt_sbmt').parent('div').attr('id',net_discount);
					var new_total = price - net_discount;
					
					new_total =new_total.toFixed(2);
					$('.discount_amt').html('-'+net_discount);
					$('.ryt_contcoursget').attr('id',new_total);
					
					$('.ryt_contcoursget').html("$"+new_total);*/
					//$('.aply_dscnt_btn,.disable_apply_code, .aply_dscnt_otr, .aply_dscnt_sbmt, .aply_dscnt_ipt').attr('disabled', true);
					
				}	
			}
		});
	});
$('.aply_dscnt_sbmt_step2').click(function(){
	var resp=$('#discount_val').val();
	
	$('#err_code').html('');
	var price = $.trim($('.ryt_contcoursget').attr('id'));
	var discount = $.trim(resp);
	
	var totaldiscount=0;
	var netdiscount=0; 
		$(".actualprice").each(function(index){
	//var actualprice=$(this).text();
var actualprice=$(this).attr('data-val');
	$.ajax({		
			url:ajax_url+'Home/round_discount/',
			type: 'post',
			data: {'discount':discount,'price':actualprice},
			success: function(resp){				
			netdiscount=parseFloat(netdiscount)+parseFloat(resp);
			netdiscount=netdiscount.toFixed(2);
			totaldiscount=parseFloat(totaldiscount)+parseFloat(actualprice-resp);
			totaldiscount =totaldiscount.toFixed(2);
			$('.aply_dscnt_sbmt').parent('div').attr('id',netdiscount);
			$('#discount_amt').val(netdiscount);
			$('.discount_amt').html('-'+externalizePrice(netdiscount));
			$('.ryt_contcoursget').attr('id',totaldiscount);	
			$('.ryt_contcoursget').html(externalizePrice(totaldiscount));
			$('#shopping_apply_discount').modal('hide');
			$('.aply_dscnt_btn,.disable_apply_code, .aply_dscnt_otr, .aply_dscnt_sbmt, .aply_dscnt_ipt,.aply_dscnt_sbmt_step2').attr('disabled', true);
			if(parseInt(discount)==100)
			{
			$(".paypalimg #submit").css("display","none");
			$(".visa_img").css("display","none");
			$(".add_learning_plan").css("display","block");
			}
			
	}
/*var net_discount=(discount*price)/100;
	net_discount=net_discount.toFixed(2);
	$('.aply_dscnt_sbmt').parent('div').attr('id',net_discount);
$('#discount_amt').val(net_discount);
	var new_total = price - net_discount;
	
	new_total =new_total.toFixed(2);

	$('.discount_amt').html('-'+externalizePrice(net_discount));
	$('.ryt_contcoursget').attr('id',new_total);	
	$('.ryt_contcoursget').html(externalizePrice(new_total));
	$('#shopping_apply_discount').modal('hide');
$('.aply_dscnt_btn,.disable_apply_code, .aply_dscnt_otr, .aply_dscnt_sbmt, .aply_dscnt_ipt,.aply_dscnt_sbmt_step2').attr('disabled', true);*/
});

});
});


$(".add_learning_plan").click(function()
{
	var allids=new Array();
	$(".add_learning_discount").each(function(index)
	{
		allids.push($(this).attr('rel'));
		var getId = $(this).attr('rel');
		$(".load_top").show();	
		var a =$(this);	
		req = $.ajax(
		{
			url:ajax_url+'Home/add_to_my_learning1/'+getId,
			dataType: 'json',
			success: function(resp)
			{
				if(resp.status == 'ok')
				{
					var myarr=[];
					var myid=[];
					$("input[type='checkbox'][name='chklist']").each(function()
					{
						myarr.push($(this).attr('title'));
						myid.push($(this).attr('id')+'-'+$(this).attr('rel'));
					});
					// remove from the list
					var   my_str='';
					$.each(myarr,function(key,value)
					{
						my_str+='<p>'+(key+1)+'. '+value+'</p>' ; 
					});
					$('#removeListArray').val(myid);			              
					var  myid= ($('#removeListArray').val()).split(',');
					req = $.ajax(
					{
						url:ajax_url+'Home/remove_product_ajax/',
						type: 'post',
						data:{"array":myid},
						success: function(resp)
						{
						window.location=ajax_url+'Members/my_learning';
						}
					});
				}
			}
		});
	})
});

	$('#submit').click(function(){
		var resp=$('#discount_val').val();
		var discount = $.trim(resp);
		$price=$.trim($('.ryt_contcoursget').attr('id'));
		
		//$discount_amount=$.trim($('.aply_dscnt_sbmt').parent('div').attr('id'));
$discount_amount =$('#discount_amt').val();
		$.ajax({
				url:ajax_url+'Members/course_purchase/'+$price+'/'+$discount_amount+'/'+discount,	
				success: function(resp){  //alert(resp);
					if(resp == 'error'){
						window.location.href = ajax_url;
					}else{
						
						window.location=resp;
							
							
					}
				}
		});
	});
	$('.remove_delte_conted').on("click",function(){ 
            var myarr=[];
            var myid=[];
            var cnt = $("input[type='checkbox'][name='chklist']:checked").length;
            if(cnt == 0){
                 alert('<?php echo __('please_select_at_least_one');?>');
            }else{
                 $("input[type='checkbox'][name='chklist']").each(function() {
                     if($(this).prop("checked") == true){
                         //alert($(this).attr('id') +'--'+$(this).attr('rel'));
                         myarr.push($(this).attr('title'));
                         myid.push($(this).attr('id')+'-'+$(this).attr('rel'));
                    }
               
            });
                // remove from the list
                
              var   my_str='';
              $.each(myarr,function(key,value){
                    my_str+='<p>'+(key+1)+'. '+value+'</p>' ; 
              });
              $('#removeListArray').val(myid);
               $('#removeList').html(my_str);
               $('#confirmModel').modal('show');            
            }
		
	});
    $('#no_remove').click(function(){
        $('#confirmModel').modal('hide');
    });
        $('#yes_remove').click(function(){
            var  myid= ($('#removeListArray').val()).split(',');
            req = $.ajax({
                url:ajax_url+'Home/remove_product_ajax/',
                type: 'post',
                data:{"array":myid},
                success: function(resp){
                         $('#confirmModel').modal('hide');
                        window.location=ajax_url+'Home/purchase';
                }
            });
						  
        });
        //move to learning plan
        
        $('.move_to_learning_plan').on('click',function(){
            
            var myid=[];
            var avlList='';
            var addList='';
            var cnt = $("input[type='checkbox'][name='chklist']:checked").length;
            if(cnt == 0){
                 alert('<?php echo __('please_select_at_least_one');?>');
            }else{
                 $("input[type='checkbox'][name='chklist']").each(function() {
                     if($(this).prop("checked") == true){                       
                         if(!$(this).hasClass('added_learning_dsbl')){                            
                            addList+='<p>'+$(this).attr('title')+'</p>';
                            myid.push($(this).attr('id')+'-'+$(this).attr('data-id')+'-'+$(this).attr('rel'));
                         }else{
                             avlList+='<p>'+$(this).attr('title')+'</p>';
                         }
                    }
                    
               
            });
            if(myid.length>0){
         if(avlList==''){
                $('.avlListMsg').hide();
                $('#availableList').html(avlList);
                }else{
$('.avlListMsg').show();
                $('#availableList').html(avlList);
                }
                $('#addLearnList').html(addList);
                $('#learnListArray').val(myid);
                $('#confirmLearningModel').modal('show');
            }else{
                 alert('<?php echo __('already_available_in_learning_plan');?>');      
            }
            
           }
        });
        $('#yes_add').click(function(){
        var myid=($('#learnListArray').val()).split(',');
            //alert(mid);
            req = $.ajax({
                url:ajax_url+'Home/add_learning_cart_ajax/',
                type: 'post',
                data:{"array":myid},
                success: function(resp){
                         $('#confirmLearningModel').modal('hide');
                         if(resp =='index'){
                             window.location=ajax_url+'Home/index';
                         }else{
		window.location=ajax_url+'Home/purchase';
                          
                         }
                }
            });
        });
        $('#no_add').click(function(){
         $('#confirmLearningModel').modal('hide');
        });
   
});

</script>
<!-- main container section -->
<section>
  <div class="container InnerContent">
    <div class="row">
    <?php if($this->Session->read("LocTrain.flashMsg")!=""){ ?>
    	<div class="account-activated">	<?php echo $this->Session->read("LocTrain.flashMsg"); ?></div>
	<?php $this->Session->delete("LocTrain.flashMsg");  }?>
    <div class="col-lg-12">
    	<?php if(!empty($course_list)) {?>
    <div class="row">
    	<div class="col-lg-12 CourseVideo">
        	<div class="Title shoppingContentTitle" ><?php echo __('your_shopping_basket_contents');?></div>
           
            </div>
    </div>
<?php } ?>
    	<?php if(!empty($course_list)) {?>
             <?php  echo $this->element('frontElements/home/purchase_list'); ?>
            
     <?php   }else{ ?>
        <div class="row">
    	<div class="col-lg-12  col-md-12 col-sm-12 shoppingEmptyDiv">
            <span><?php echo __('shopping_basket_is_empty'); ?></span>
	<?php echo $this->Html->link(__('continue_shopping_in_the_catalogue'),array('controller'=>'Home','action'=>'catalogue_landing'),array('class'=>'small','escape'=>false)); ?>
             </div>
         </div>
       <?php } ?>
        
   
    <!-- Modal -->
        <div class="modal fade" id="confirmModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo __('confirmation');?></h4>
                    </div>
                    <div class="modal-body">    
                        <p class="Title margintop20"><?php echo __('are_you_sure_you_want_to_remove_this_course_from_your_shopping_basket'); ?></p>
                            <div id="removeList">
                                
                            </div>
                            <input type="hidden" id="removeListArray" value="">
                            <span class="Link"><a id="yes_remove"><?php echo __('yes'); ?></a>&nbsp;&nbsp;<a id="no_remove"><?php echo __('no'); ?></a></span>
                          
                       
                    </div>                            
                </div>
            </div>
        </div>
    
    <div class="modal fade" id="confirmLearningModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo __('confirmation');?></h4>
                    </div>
                    <div class="modal-body"> 
                        <p class="avlListMsg Title"><?php echo __('the_following_courses_already_available_in_your_learning_plan'); ?></p>
                            <div id="availableList">
                                
                            </div>
                        <p class="Title margintop20"><?php echo __('are_you_sure_you_want_to_move_this_course_to_your_learning_plan'); ?></p>
                            <div id="addLearnList">
                                
                            </div>
                            <input type="hidden" id="learnListArray" value="">
                            <span class="Link"><a id="yes_add"><?php echo __('yes'); ?></a>&nbsp;&nbsp;<a id="no_add"><?php echo __('no'); ?></a></span>
                            
                       
                    </div>                            
                </div>
            </div>
        </div>
    
    
      </div>  
    </div>
  </div>
</section>
<!--End  main container section -->
