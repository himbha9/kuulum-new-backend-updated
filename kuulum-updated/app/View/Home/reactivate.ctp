<?php echo $this->Html->css('autoSuggest');?>
<?php echo $this->Html->script('jquery.autoSuggest');?>
<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'clean'
 };
$('document').ready(function(){


$('#termCond').on('click',function(){
 	if($(this).is(':checked'))
	{
		$(this).val('1');
	}else{
		$(this).val('0');
	}
});
$('#notification').on('click',function(){
 	if($(this).is(':checked'))
	{
		$(this).val('1');
	}else{
		$(this).val('0');
	}
});
    var data = {items: [
		<?php echo $interest_list;?>
	]};
	
	<?php // if cancel account case and user want to reactivate
		$selectinterest_list = '';
		if(!empty($data['MemberKeyword'])){
			foreach($data['MemberKeyword'] as $interestVal):	
				$selectinterest_list .= '{value:'."'".$interestVal['keyword']."'".", name:"."'".$interestVal['keyword']."'},";	
			endforeach;
		}
	?>	
	
$("#interest").autoSuggest(data.items, {selectedItemProp: "name", searchObjProps: "name",preFill: [<?php echo $selectinterest_list; ?>],fieldName: "data[Member][keywords]"});	
	
});
 </script>
<!-- main container section -->
<section>
  <div class="container InnerContent">
    <div class="row">
    
    <div class="col-lg-6 col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 register">
    
    
  
       
		<?php echo $this->Form->create('Member',array('id'=>'memberRegister','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'reactivate')));?>
			
			<?php $mem_id = base64_encode(convert_uuencode($data['Member']['id'])); ?>
		
			<?php echo $this->Form->input('id',array('type'=>'hidden','id'=>'id','class'=>'regfld_txt gradient','label'=>false,'div'=>false,'value'=>$mem_id,'readonly'=>'readonly')); ?> 
  <div class="form-group">
    <label for="fullName">*<?php echo __('preferred_name');?></label>
    <?php echo $this->Form->input('given_name',array('type'=>'text','id'=>'givenName','class'=>'form-control Textname',"placeholder"=>"John Doe",'label'=>false,'div'=>false,'value'=>$data['Member']['given_name'])); ?>
    <div id="err_given_name" class="register_error  alert-danger"> <?php if(isset($error['given_name'][0])) echo $error['given_name'][0];?> </div>
    
  </div>
  <div class="form-group">
    <label for="emailId">*<?php echo __('e-mail_address');?></label>    
    <?php echo $this->Form->input('rea_email',array('type'=>'text','id'=>'emailId','placeholder'=>"jdoe@example.com",'class'=>'form-control TextEmail','label'=>false,'div'=>false,'value'=>$data['Member']['email'])); ?> 
    <div id="err_rea_email" class="register_error  alert-danger"> <?php if(isset($error['rea_email'][0])) echo $error['rea_email'][0];?> </div>         
  </div>
  <div class="form-group">
     <label><?php echo __('type_some_keywords_or_phrases_that_describe_your_learning_interests');?></label>
   <?php echo $this->Form->input('interests',array('type'=>'text','id'=>'interest','class'=>'form-control','label'=>false,'div'=>false)); ?>
    <div id="err_interests" class="register_error alert-danger"> <?php if(isset($error['interests'][0])) echo $error['interests'][0];?> </div>
 
  </div>  
  
  <div class="form-group captcha">
        <label for="password">*<?php echo __('because_we_care_about_people_not_bots_please_enter_the_text_shown_below');?></label>    
            <?php echo recaptcha_get_html($publickey, null, true); ?>
        <div id="err_captcha_chk" class="register_error  alert-danger"> <?php if(isset($error['captcha_chk'][0])) echo $error['captcha_chk'][0];?> </div>
  </div>
  
  
  <div class="checkbox">
    <label>
        <?php echo $this->Form->input('term_condition',array('type'=>'checkbox','id'=>'termCond','value'=>'0','label'=>false,'div'=>false)); ?> 
        <p><?php echo __('i_accept_the'); ?>
	<?php echo $this->Html->link(__('terms_of_use'),array('controller'=>'Home','action'=>'terms_of_use'),array('target'=>'_new')); ?>
        </p>
    </label>
    <div id="err_term_condition" class="register_error  alert-danger"> <?php if(isset($error['term_condition'][0])) echo $error['term_condition'][0];?> </div>
  </div>
  
  <div class="checkbox">
    <label>
        <?php echo $this->Form->input('notification',array('type'=>'checkbox','id'=>'notification','value'=>'0','label'=>false,'div'=>false)); ?> 
      <p><?php echo __('we_would_like_to_keep_you_informed_of_featured_courses_and_news_that_may_be_relevant_to_you_tick_box_if_you_do_not_wish_to_receive_this');?></p>
    </label>
  </div>
<?php echo $this->Form->submit(__('save_my_details_and_register'),array('class'=>'btn btn-default link disRegisBut','div'=>false,'onclick'=>"return ajax_form('memberRegister','Home/validate_reactivate_ajax','load_top','disRegisBut')")); ?>
   <div class="link" id="sign_link"><?php echo $this->Form->submit(__('cancel'),array('class'=>'btn btn-default link disRegisBut', 'div'=>false, 'onclick'=>'memberRegister.action="'. HTTP_ROOT.'Home/index"')); ?></div> 
  <?php echo $this->Form->end(); ?>

    
    
    
    
    </div>
    
    
    
    </div>
  </div>
</section>
<!--End  main container section -->
