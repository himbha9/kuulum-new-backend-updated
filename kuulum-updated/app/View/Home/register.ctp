<style>

<?php if(isset($css) && $css=='contrast'){ ?>
#recaptcha_response_field {
    border-color:#000 !important; // Text input field border color  
  }
<?php }else { ?>
#recaptcha_response_field {
    border-color:#7f7f7f !important; // Text input field border color  
  }
<?php } ?>

</style>
<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'clean'
 };
$('document').ready(function(){

///show login popup
/*var show_or_hide_login='<?php echo $show_or_hide_login;?>';
if(show_or_hide_login=='Yes'){
 $('#myModal4').modal('show');
}*/
    $('.show_link').hide();
    $('.openForgotForm').hide();

//
$('#emailId').on('focusout',function(){ 	
		var msg = '';
		var inputValEmail = $('#emailId').val();	
		
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_reg_email").text(msg);
		}
		else {
			$("#err_reg_email").text('');
		}
		if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_reg_email").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		
		
	});
$('#emailId4').css({'border-color':'#66afe9','box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 175, 233, 		0.6)','outline':'0 none'});
 	
	$('#emailId4').focusout(function(){
		$(this).attr('style','');
	});
$('#emailId4').on('focusout',function(){ 	
		var msg = '';
		var inputValEmail = $('#emailId4').val();	
		
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err4_email").text(msg);
		}
		else {
			$("#err4_email").text('');
		}
		if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err4_email").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		
		
	});
	$('#password4').on('focusout',function(){ 	
		var msg='<?php echo  __("please_choose_a_password_consisting_of")."<br>".__("a_minimum_of_8_and_maximum_of_20_characters").'<br>'.__("at_least_one_upper_case_character").'<br>'. __("at_least_one_lower_case_character").'<br>'. __("at_least_one_number").'<br>'.__("at_least_one_non_alphanumeric_character");?>';
		var inputValPass = $('#password4').val();			
		if(inputValPass == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err4_password").text(msg);
		}
		/*else if($('#password4').val().length < 8 || $('#password4').val().length > 20){
	$("#err4_password").html(msg);
	}else if(!$('#password4').val().match(/[A-Z]/)){
		$("#err4_password").html(msg);
	}else if(!$('#password4').val().match(/[a-z]/)){
		$("#err4_password").html(msg);
	}else if(!$('#password4').val().match(/[0-9]/)){
		$("#err4_password").html(msg);
	}else if(!$('#password4').val().match(/[!@?#$%^&*()\-_=+{};:,<.>~]/)){
		$("#err4_password").html(msg);
	}*/else{
		$("#err4_password").html('');
	}
		
		
		  
	});

$('body').on('focusout','#recaptcha_response_field',function(){//alert('hello');
		var msg = '';
		var captchaVal = $('#recaptcha_response_field').val();
		  if(captchaVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_reg_captcha_chk").text(msg);
		  }else if(captchaVal != ''){
			$.ajax({
			url:ajax_url+'Home/checkCaptchaCode',
			type:'POST',
			data:$( "#memberRegister" ).serialize(),
			success:function(html){
				if(html==''){
				 $("#err_reg_captcha_chk").text('');
				}else{Recaptcha.reload();
					$("#err_reg_captcha_chk").text(html);
				}
			}
		});
			} 
		  else {
			$("#err_reg_captcha_chk").text('');
		  }
	});
$('#termCond').on('click',function(){
var msg='';
 if($(this).is(':checked')){
$(this).val('1');
$('#err_reg_term_condition').text('');
	}else{$(this).val('0');
msg='<?php echo __("please_accept_the_terms_of_use");?>';
$('#err_reg_term_condition').text(msg);
	}
});
$('#notification').on('click',function(){
if($(this).is(':checked')){
$(this).val('1');
	}else{$(this).val('0');
}
});
$('#fullName').on('keyup',function(){
		var msg = '';
		var finalNameVal = $('#fullName').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_reg_full_name").text(msg);
		  }
		  else {
			$("#err_reg_full_name").text('');
		  }
	});


	$('#reg_password').on('focusout',function(){ 	
		var msg='<?php echo  __("please_choose_a_password_consisting_of")."<br>".__("a_minimum_of_8_and_maximum_of_20_characters").'<br>'.__("at_least_one_upper_case_character").'<br>'. __("at_least_one_lower_case_character").'<br>'. __("at_least_one_number").'<br>'.__("at_least_one_non_alphanumeric_character");?>';
		var inputValPass = $('#reg_password').val();			
		if(inputValPass == ''){
			msg1 = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_reg_password").text(msg1);
		}
		else if($('#reg_password').val().length < 8 || $('#reg_password').val().length > 20){
	$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[A-Z]/)){
		$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[a-z]/)){
		$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[0-9]/)){
		$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[!@?#$%^&*()\-_=+{};:,<.>~]/)){
		$("#err_reg_password").html(msg);
	}else{
		$("#err_reg_password").html('');
	}
		
		
		  
	});
$('#reg_password').on('focusout',function(){ 	 checkVal();	});

});
function checkVal()
{
var msg='<?php echo  __("please_choose_a_password_consisting_of")."<br>".__("a_minimum_of_8_and_maximum_of_20_characters").'<br>'.__("at_least_one_upper_case_character").'<br>'. __("at_least_one_lower_case_character").'<br>'. __("at_least_one_number").'<br>'.__("at_least_one_non_alphanumeric_character");?>';
	if($('#reg_password').val().length < 8 || $('#reg_password').val().length > 20){
	$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[A-Z]/)){
		$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[a-z]/)){
		$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[0-9]/)){
		$("#err_reg_password").html(msg);
	}else if(!$('#reg_password').val().match(/[!@?#$%^&*()\-_=+{};:,<.>~]/)){
		$("#err_reg_password").html(msg);
	}else{
		$("#err_reg_password").html('');
	}
}
    function openForgotForm(id){
        $('.show_link').hide();
         $('.openForgotForm').hide();
        $('#openForgotForm_'+id).show();
        $('#show_link_'+id).show();
    }
 </script>
<!-- main container section -->
<section>
  <div class="container  InnerContent">
    <div class="row">
    
    <div class="col-lg-6 col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 register">
    
    
  
       <?php echo $this->Form->create('Member',array("role"=>"form",'id'=>'memberRegister','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'register',$course_id)));?>
  <div class="form-group">
    <label for="fullName">*<?php echo __('preferred_name');?></label>
    <?php echo $this->Form->input('reg_full_name',array('type'=>'text','id'=>'fullName','class'=>'form-control Textname',"placeholder"=>__(""),'label'=>false,'autofocus'=>'','div'=>false)); ?>
    <div id="err_reg_full_name" class="register_error  alert-danger"> <?php if(isset($error['reg_full_name'][0])) echo $error['reg_full_name'][0];?> </div>
    
  </div>
  <div class="form-group">
    <label for="emailId">*<?php echo __('e-mail_address');?></label>    
    <?php echo $this->Form->input('reg_email',array('type'=>'text','id'=>'emailId','placeholder'=>__(""),'class'=>'form-control TextEmail','label'=>false,'div'=>false,'autocomplete'=>'off')); ?> 
    <div id="err_reg_email" class="register_error  alert-danger"> <?php if(isset($error['reg_email'][0])) echo $error['reg_email'][0];?> </div>         
  </div>
  <div class="form-group">
    <label for="password">*<?php echo __('password');?></label>    
    <?php echo $this->Form->input('reg_password',array('type'=>'password','id'=>'reg_password','class'=>'form-control TextPwd',"placeholder"=>__(''),'label'=>false,'div'=>false,'autocomplete'=>'off')); ?>
    <div id="err_reg_password" class="register_error  alert-danger"> <?php if(isset($error['reg_password'][0])) echo $error['reg_password'][0];?> </div>
  </div>
  
  
  <div class="form-group captcha">
        <label for="password">*<?php echo __('because_we_care_about_people_not_bots_please_enter_the_text_shown_below');?></label>    
      <?php echo recaptcha_get_html($publickey, null, true); ?>
        <div id="err_reg_captcha_chk" class="register_error  alert-danger"> <?php if(isset($error['reg_captcha_chk'][0])) echo $error['reg_captcha_chk'][0];?> </div>
  </div>
  
  
  <div class="checkbox">
    <label>
        <?php echo $this->Form->input('reg_term_condition',array('type'=>'checkbox','id'=>'termCond','value'=>'0','label'=>false,'div'=>false)); ?> 
        <p><?php echo __('i_accept_the'); ?>
	<?php echo $this->Html->link(__('terms_of_use'),array('controller'=>'Home','action'=>'terms_of_use'),array('target'=>'_new')); ?>
        </p>
    </label>
    <div id="err_reg_term_condition" class="register_error  alert-danger"> <?php if(isset($error['reg_term_condition'][0])) echo $error['reg_term_condition'][0];?> </div>
  </div>
  
  <div class="checkbox checkNotificationTick">
    <label>
        
      <p><?php echo __('we_would_like_to_keep_you_informed_of_featured_courses_and_news_that_may_be_relevant_to_you_tick_box_if_you_do_not_wish_to_receive_this');?>
<?php echo $this->Form->input('notification',array('type'=>'checkbox','id'=>'notification','value'=>'0','label'=>false,'div'=>false)); ?> 
</p>
    </label>
  </div>

   <?php echo $this->Form->submit(__('register'),array('class'=>'btn btn-default link ','div'=>'false registerDiv','onclick'=>"return ajax_form('memberRegister','Home/validate_register_ajax','load_top','disRegisBut')")); ?>
  <?php echo $this->Form->end(); ?>

    
<div class="link" id="sign_link"><?php echo __('have_you_registered_already');?> <a href="javascript:;" data-toggle="modal" data-target="#myModal4"> <?php echo __('sign_in');?></a></div> 
    
      <!-- Modal -->
              <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?php echo __("sign_in_to_learn_and_teach");?></h4>
                                </div>
                                <div class="modal-body">
                                    <?php echo $this->Form->create('Login',array('role'=>'form', 'id'=>'memberLogin4','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'member_login')));?>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('email',array('type'=>'text','id'=>'emailId4','class'=>'form-control TextEmail','placeholder'=>__('e-mail_address'),'autofocus'=>'','div'=>false,'value'=>isset($cookieVal)?$cookieVal:'')); ?>
                                            <div id="err4_email" class="register_error  alert-danger"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('password',array('type'=>'password','id'=>'password4','class'=>'form-control TextPwd','placeholder'=>__('password'),'div'=>false)); ?>
                                            <div id="err4_password" class="register_error  alert-danger" > <?php if(isset($error['password'][0])) echo $error['password'][0];?> </div>
                                        </div>                                    
                                        <?php echo $this->Form->submit(__('sign_in'),array('class'=>'btn btn-primary btn-lg ButtonBlue','onclick'=>"return ajax_form('memberLogin4','Home/login_check_ajax','load_top')")); ?>
                                        <span class="Link">
                                        <?php echo $this->Html->link(__('i_havent_registered_yet'),array('controller'=>'Home','action'=>'register'),array('class'=>''));?> 
                                    </span>
                                        
                                    <?php echo $this->Form->end(); ?>
                                    <span class="Link"><a href="javascript:;" onclick="return openForgotForm(4);"><?php echo __('i_forgot_my_password');?></a></span>   
                                    <p class="form-group openForgotForm" id="openForgotForm_4">
                                        <?php echo __("we_will_send_an_e_mail_to_you_with_a_password_reset_link_before_we_send_it_please_confirm_your_e_mail_address_above_is_correct");?>
                                    </p>
                                    <span class="Link show_link" id="show_link_4"><a href="javascript:;" onclick="return forgot_ajax_form('emailId',4,'myModal4');"><?php echo __('send_the_password_reset_e-mail_to_me');?></a></span>
                                </div>                            
                            </div>
                        </div>
                    </div>
    
    </div>
    
    
    
    </div>
  </div>
</section>
<!--End  main container section -->
