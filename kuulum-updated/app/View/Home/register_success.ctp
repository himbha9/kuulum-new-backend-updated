<script>
$(document).ready(function(){
	$('.register_success').fadeOut(10000);
});

</script>
<div id="wrapper"> 
  <div id="middle">
    <div class="breadcrumb_botcont">
      <div class="register_success">
      	<div class="reg_scleft">
        	<?php echo $this->Html->image('front/success_new.png',array('width'=>'50','height'=>'62')); ?>        	
        </div>
        <div class="reg_scrgt">
      		<p class="reg_msg_head"> <?php echo __('your_registration_is_almost_complete');?> </p>
        	<span class="reg_msg"> <?php echo __('a_confirmation_message_has_been_sent_to_the_e-mail_address_you_entered_to_complete_your_registration_please_select_activate_my_membership_when_the_e-mail_arrives_if_you_suspect_the_e-mail_has_not_arrived_take_a_look_in_the_spam_folder');?> </span>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
.mandatory{	
	color:#FD2600;
}
.radio_image
{
	float:left;
	width:auto;
}
.radio_text{
	float:left;
	width:auto;
	margin-left:10px;
	margin-top:4px;
}
.term_chk_box
{
	float:left;
	width:auto;
}
.radioBox_container
{
	float:left;
	width:100%;
}
</style>