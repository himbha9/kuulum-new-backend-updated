<script>
$(document).ready(function(){
	$('.register_unsuccess').fadeOut(10000);
});

</script>
<div id="wrapper"> 
  <div id="middle">
    <div class="breadcrumb_botcont">
      <div class="register_unsuccess">
      	<div class="reg_scleft">
        	<?php echo $this->Html->image('front/unsuccess_new.png',array('width'=>'50','height'=>'62')); ?>

        </div>
        <div class="reg_scrgt">
      		<p class="reg_msg_head"> <?php echo __('unable_to_activate_membership');?> </p>
        	<span class="reg_msg"> <?php echo __('the_activate_my_membership_link_has_already_been_used_or_it_has_passed_its_expiry_date_and_time_if_your_membership_has_already_been_activated_please_sign_in_using_your_e-mail_address_and_the_password_you_entered_during_registration_if_your_membership_was_not_activated_within_24-hours_of_registration_you_will_need_to_register_again');?> </span>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
.mandatory{	
	color:#FD2600;
}
.radio_image
{
	float:left;
	width:auto;
}
.radio_text{
	float:left;
	width:auto;
	margin-left:10px;
	margin-top:4px;
}
.term_chk_box
{
	float:left;
	width:auto;
}
.radioBox_container
{
	float:left;
	width:100%;
}
</style>