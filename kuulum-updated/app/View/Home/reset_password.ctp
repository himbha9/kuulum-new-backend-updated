<script type="text/javascript">
 $('document').ready(function(){

$('#email_r').on('focusout',function(){ 	
		var msg = '';
		var inputValEmail = $('#email_r').val();	
		
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_ques2").text(msg);
		}
		else {
			$("#err_ques2").text('');
		}
		if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_ques2").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		
		
	});
$('#quest1').on('keyup',function(){
		var msg = '';
		var finalNameVal = $('#quest1').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_ques1").text(msg);
		  }
		  else {
			$("#err_ques1").text('');
		  }
	});
});
</script>
<section>
  <div class="container InnerContent">
 <div class="row">  
        <div class="col-lg-12">
            <p>
                <?php echo __('because_the_safekeeping_of_your_details_are_very_important_to_us_we_need_to_you_to_answer_a_few_questions_about_yourself_before_you_can_reset_your_password_we_do_this_to_confirm_that_this_is_indeed_your_account_when_you_have_finished_please_select_verify_my_answers');?>
            </p> 
            <br><br><br>
        </div>
    </div>
    <div class="row">
    
    <div class="col-lg-6 col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 register formWrapper">    
    
  
      <?php echo $this->Form->create('Member',array('id'=>'memberRegister','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'reset_password')));?>
  
       
        <div class="form-group">
            <label for="fullName">*<?php echo __('what_is_your_preferred_name');?></label>
            <?php echo $this->Form->input('id',array('type'=>'hidden','value'=>base64_encode(convert_uuencode($reset_member_password['Member']['id'])))); ?>
            <?php echo $this->Form->input('ques1',array('type'=>'text','id'=>'quest1','autofocus'=>'','class'=>'form-control Textname',"placeholder"=>"John Doe",'label'=>false,'div'=>false)); ?> 
            <div id="err_ques1" class="register_error  alert-danger"> <?php if(isset($error['ques1'][0])) echo $error['ques1'][0];?> </div>         
        </div>
         <div class="form-group">
            <label for="fullName">*<?php echo __('what_e_mail_address_did_you_register_with_us');?></label>
            <?php echo $this->Form->input('id',array('type'=>'hidden','value'=>base64_encode(convert_uuencode($reset_member_password['Member']['id'])))); ?>
            <?php echo $this->Form->input('ques2',array('type'=>'text','id'=>'email_r','class'=>'form-control TextEmail',"placeholder"=>"jode@example.com",'label'=>false,'div'=>false)); ?> 
            <div id="err_ques2" class="register_error  alert-danger"> <?php if(isset($error['ques1'][0])) echo $error['ques1'][0];?> </div>         
        </div>
        <?php echo $this->Form->submit(__('verify_my_answers'),array('class'=>'btn btn-default link','div'=>'false','onclick'=>"return ajax_form('memberRegister','Home/validate_reset_password_ajax','load_top','disRegisBut')")); ?>
           <div class="link" id="sign_link">     <?php echo $this->Form->submit(__('cancel'),array('class'=>'btn btn-default link', 'div'=>'false', 'onclick'=>'memberRegister.action="'. HTTP_ROOT.'Home/index"')); ?>
               </div>
   
  <?php echo $this->Form->end(); ?>
    
    </div>
    
    
    
    </div>
  </div>
</section>
