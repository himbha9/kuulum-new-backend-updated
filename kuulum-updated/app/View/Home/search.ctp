<?php $ulogin=0;
						if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}
?>
<style>
.optDisble{
color:#f2f2f2;
}
.optEnble{
color:#0070c0;
}
</style>
  <?php 
   /* Rating Script [Start] */
        echo $this->Html->script('front/star-rating.js');       
        echo $this->Html->css('front/BeatPicker.min.css');
        echo $this->Html->script('front/BeatPicker.min.js');
?>
<script type="text/javascript">
      
    var req='';
    function convertData(locale,date){  		
	switch (locale)
        {
           case 'en': return date.replace(/-/gi,'-');
           case 'de': return date.replace(/\./gi,'-');
           case 'ar': return date.replace(/\//gi,'-');
           case 'fr': return date.replace(/\//gi,'-');
           case 'it': return date.replace(/\//gi,'-');
           case 'ja': return date.replace(/\//gi,'-');
           case 'ko': return date.replace(/-/gi,'-');
           case 'pt': return date.replace(/\//gi,'-');
           case 'ru': return date.replace(/\./gi,'-');
           case 'zh': return date.replace(/-/gi,'-');
           case 'es': return date.replace(/\//gi,'-');          
           default:return date.replace(/-/gi,'-');
        }
		
		
}
    
 $('document').ready(function(){


$('body').on('click','.video_sec',function(e){
 var id=$(this).attr('id');
 var type=$(this).attr('rel');	
 var news_id=$(this).attr('data-val');	
	if(type=='News'){
		if($(e.target).hasClass("recent_news")){
			return false;
		}
	//$('#news_'+news_id).modal('show');
	}else if(type =='Free Video'){
	window.location=ajax_url+'Home/view_free_video/'+id;
	}
	else{
		if( $(e.target).hasClass("rating-container")){
		//console.log('yes');
		return false;
		}else if( $(e.target).hasClass("rating-stars")){
		//console.log('yes');
		return false;
		}else if( $(e.target).hasClass("add_learning")){
		//console.log('yes');
		return false;
		}else if( $(e.target).hasClass("tc_four")){
		//console.log('yes');
		return false;
		} else{
		//console.log('no');
		window.location=ajax_url+'Home/view_course_details/'+id;
		}
	} 
});


$('.FilterBox').hide();
     $('#date_div').hide();
$('#tab2').hide();
        $(".loading_inner").hide();
        $("#dropdown7:first-child").text('<?php echo __("published_this_week");?>').append('<span class="caret"></span>' );
    $("#dropdown7:first-child").val('week');
    $(".dropdown-menu li a").click(function(){    
    $("#dropdown7:first-child").text($(this).text()).append('<span class="caret"></span>' );
    $("#dropdown7:first-child").val($(this).attr('value'));
    if($(this).attr('value') =='month'){
        $('#date_div').hide();
        showSearchResults();
    }else if($(this).attr('value') =='between'){
        $('#date_div').show();
        
    }else{
       $('#date_div').hide(); 
       showSearchResults();
    }
    
  }); 

//
$('.tabOpt').click(function(){
var current_filter=$('#turn_on_off_chk').val(); // alert(current_filter);

	$('#filertOpt').val($(this).attr('rel'));
	if($(this).find('.fa:first').hasClass('optEnble')){
	$('#sortInput').val('2');
	$(this).find('.fa:first').removeClass('optEnble');
	$(this).find('.fa:first').addClass('optDisble');
	$(this).find('.fa:last').addClass('optEnble');
	$(this).find('.fa:last').removeClass('optDisble');
	}else{
	$('#sortInput').val('1');
	$(this).find('.fa:last').removeClass('optEnble');
	$(this).find('.fa:last').addClass('optDisble');
	$(this).find('.fa:first').removeClass('optDisble');
	$(this).find('.fa:first').addClass('optEnble');
	
	}

	showSearchResults();

});

  ////call showSearchResults when from_date and to_date is filled
  
 from_date.on("select", function (data) {
             $("#from_date").parent().parent('.date_selected').attr('id',data.string);
             $('#to_date').val('');$('.dt').attr('id','');
showSearchResults();
                 
	});
  to_date.on("select", function (data) {
                    $("#to_date").parent().parent('.date_selected').attr('id',data.string);
showSearchResults();
		});

  /////onclick of date tab show the results by date
  $('#date_tab').click(function(){

       $('#title_tab').find('.fa:first').removeClass('optDisble');
	$('#title_tab').find('.fa:first').removeClass('optEnble');
	$('#title_tab').find('.fa:first').addClass('optDisble');

	$('#title_tab').find('.fa:last').removeClass('optEnble');
	$('#title_tab').find('.fa:last').removeClass('optDisble');
	$('#title_tab').find('.fa:last').addClass('optEnble');


	$('#filertOpt').val('date');	
        $('#on_off_div').val('date');
        var current_filter=$('#turn_on_off_chk').val(); // alert(current_filter);
        if(current_filter=='turn_filter_on' || current_filter=='')
        {
            $('#tab1').hide();
            $('#tab2').hide();
        }else{
            $('#tab1').show();
            $('#tab2').show();

        }

      
  });
$('#title_tab').click(function(){
	
	$('#date_tab').find('.fa:first').removeClass('optDisble');
	$('#date_tab').find('.fa:first').removeClass('optEnble');
	$('#date_tab').find('.fa:first').addClass('optDisble');

	$('#date_tab').find('.fa:last').removeClass('optEnble');
	$('#date_tab').find('.fa:last').removeClass('optDisble');
	$('#date_tab').find('.fa:last').addClass('optEnble');


   
	$('#filertOpt').val('title');	
    $('#on_off_div').val('title');
   
    var current_filter=$('#turn_on_off_chk').val();  
    if(current_filter=='turn_filter_on' || current_filter=='')
    {
         $('#tab1').show();
         $('#tab2').hide();
    }else{
	
        $('#tab1').hide();
          $('#tab2').hide();

    }

    
});
$('#turn_on_off_chk').click(function(){
    var current_filter=$(this).val();
    var   on_off_div= $('#on_off_div').val();  d_from='';d_to='';

    if(current_filter=='turn_filter_on' || current_filter=='')
    {
	$('.FilterBox').slideDown(500);
	$(this).val('turn_filter_off');//alert($(this).val());
        $(this).text('<?php echo __("turn_filter_off");?>');
      fileropt=on_off_div;sort_option=$('#sortInput').val();
  var option =  $("#dropdown7").val();
	 switch(option){

            case 'week':
                    var date_to = new Date();
                    d_to = Math.floor(date_to.getTime()/1000);
                    var date_from = new Date();
                    date_from.setDate(date_from.getDate()-7);
                    d_from= Math.floor(date_from.getTime()/1000);


                    break;
            case 'month':
                    var date_to = new Date();
	
                    d_to = Math.floor(date_to.getTime()/1000);
                    var date_from = new Date();
                    //alert(date_from.getMonth());
                    date_from.setMonth(date_from.getMonth()- 1);
                    //d_from=date_from.getTime()/1000;
                    d_from= Math.floor(date_from.getTime()/1000);
			
                    break;
            case 'between':
                    var date_f=$('#from_date').val();
                    var date_t=$('#to_date').val();
                   if(date_f=='' && date_t=='')
                    {
		    d_from='';
	            d_to='';
                            /*alert('<?php echo __('please_select_the_dates_first') ?>'); 
                            //alert('Please select the dates first.');
                            return false;*/
			
                    }  else if(date_f !='' && date_t==''){			
	            var date_f_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_f);	          
	            var date_from = new Date(date_f_n);	           
	            d_from= Math.floor(date_from.getTime()/1000);
	            d_to='';
	            break;
			}   else if(date_f =='' && date_t !='') {
                    var date_t_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_t);	          
	            var date_to = new Date(date_t_n);	         
	            d_to = Math.floor(date_to.getTime()/1000);
	            d_from='';
	            d_to=d_to+23455;
	            break;
			}else{
	 		var date_t_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_t);
                    var date_f_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_f);
                    var date_to = new Date(date_t_n);
                    var date_from = new Date(date_f_n);
                    d_to = Math.floor(date_to.getTime()/1000);
                    d_from= Math.floor(date_from.getTime()/1000);
                    d_to=d_to+23455;
                    break;
		}                                  
                   
    }

	/* if(on_off_div =='title'){
	
	$('#tab1').slideDown(500);
   	 $('#tab2').hide();
        }else{

	 $('#tab1').hide();
    	
	$('#tab2').slideDown(500);
            
        }*/
   }else{
$('.FilterBox').slideUp('slow');
	fileropt=on_off_div;sort_option=$('#sortInput').val();d_from='';d_to='';
	 $(this).val('turn_filter_on');
        $(this).text('<?php echo __("turn_filter_on");?>');
	/* if(on_off_div =='title'){
	
	$('#tab1').hide();
   	$('#tab2').hide();
        }else{	
	
             $('#tab1').hide();
         
          $('#tab2').hide();
        }*/
 }


        //$('#date_div').hide();
	$(".loading_inner").show();	
	var keyword = $("#search_text").val();
	//$('#filertOpt').val('title');
	req = $.ajax({
		url:ajax_url+'Home/search/',
		data: {'q':keyword,'records':'0','type':2 ,'date_from':d_from ,'date_to':d_to, 'sort_option':sort_option,'filter_opt':fileropt},
		type: 'get',
		success: function(resp){	
			$(".tchng_cont_content").html(resp);
		if($(".tchng_cont_content").attr('id') == '0')
		{
			$(".tchng_cont_content").prepend('<div class="no_rec">No result maches the chosen filters.</div>');
		}
	
			$(".loading_inner").hide();
		}
	});




});

    $('#reset_filter').click(function(){
        $('#from_date').val('');
        $('#to_date').val('');
        $(".tchng_cont_content").html('');

     });
 });   
 
 
 
    
var req = '';
var from_date='0';
function showSearchResults(){
	$('.tchng_cont_content').html('');
    var option =  $("#dropdown7").val();
    var sort_option = $('#sortInput').val();
	
    var filertOpt=$('#filertOpt').val();

	if( $("#dropdown7").val()=='')
    	{option='week';
	
	}else{
	option =  $("#dropdown7").val();
	}


  // var getId = $(".tchng_cont_content").attr('id');//alert(getId);
var getId = 0;

    var keyword = $("#search_text").val();
    if(keyword=='')
    {	
            alert('<?php echo __("please_fill_a_keyword") ?>');
            return false;
    }
	if(req && req.readystate != 4){
    req.abort();
    }		

    $(".loading_inner").show();	
	if(option ==''){

		//if(getId != '0'){		
					$(".loading_inner").show();	
					var keyword = $("#search_text").val();
					req = $.ajax({
						url:ajax_url+'Home/search/',
						data: {'q':keyword,'records':getId,'type':2 , 'sort_option':sort_option,'filter_opt':filertOpt},
						type: 'get',
						success: function(resp){	
							$(".tchng_cont_content").html(resp);
						if($(".tchng_cont_content").attr('id') == '0')
						{
							$(".tchng_cont_content").prepend('<div class="no_rec">No result maches the chosen filters.</div>');
						}
					
							$(".loading_inner").hide();
						}
					});
			//	}	
	}else{
 
    switch(option){

            case 'week':
                    var date_to = new Date();
                    d_to = Math.floor(date_to.getTime()/1000);
                    var date_from = new Date();
                    date_from.setDate(date_from.getDate()-7);
                    d_from= Math.floor(date_from.getTime()/1000);


                    break;
            case 'month':
                    var date_to = new Date();
	
                    d_to = Math.floor(date_to.getTime()/1000);
                    var date_from = new Date();
                    //alert(date_from.getMonth());
                    date_from.setMonth(date_from.getMonth()- 1);
                    //d_from=date_from.getTime()/1000;
                    d_from= Math.floor(date_from.getTime()/1000);
			
                    break;
            case 'between':
                    var date_f=$('#from_date').val();
                    var date_t=$('#to_date').val();
                    if(date_f=='' && date_t=='')
                    {
		    d_from='';
	            d_to='';
                           
			
                    }  else if(date_f !='' && date_t==''){		
	            var date_f_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_f);	          
	            var date_from = new Date(date_f_n);	           
	            d_from= Math.floor(date_from.getTime()/1000);
	            d_to='';
	            break;
			} else if(date_f =='' && date_t !='') {
                    var date_t_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_t);	          
	            var date_to = new Date(date_t_n);	         
	            d_to = Math.floor(date_to.getTime()/1000);
	            d_from='';
	            d_to=d_to+23455;
	            break;
			}else{
	 		var date_t_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_t);
                    var date_f_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_f);
                    var date_to = new Date(date_t_n);
                    var date_from = new Date(date_f_n);
                    d_to = Math.floor(date_to.getTime()/1000);
                    d_from= Math.floor(date_from.getTime()/1000);
                    d_to=d_to+23455;
                    break;
		}
    }
var current_filter=$('#turn_on_off_chk').val(); 
 if(current_filter=='turn_filter_off')
    {

	}else{
d_to='';d_from='';
	}
   
//if(getId != '0'){	
    req = $.ajax({
            url:ajax_url+'Home/search/',
            data: {'q':keyword,'records':getId,'type':2,'date_from':d_from ,'date_to':d_to, 'sort_option':sort_option,'filter_opt':filertOpt },
            type: 'get',
            success: function(resp){
              $(".tchng_cont_content").html(resp);
                if($(".tchng_cont_content").attr('id') == '0')
                {
                    $(".tchng_cont_content").prepend('<div class="no_rec"><?php echo __("no_result_matches_the_chosen_filter");?></div>');
                }
                $(".loading_inner").hide();
            }
    });
//}
}
 }
$(document).ready(function(){
	$(".regfld_txt_search").attr('disabled',true);
	$('.return_to').on('click',function(){
		$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
	});
	
	
	//open the video popup if v is set
	<?php if(isset($v) && !empty($v)){ ?>
		var getUrl = "<?php echo $v; ?>";	
		var w = 700;
		var h = 400;
		var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/1.5);
		window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
	<?php } ?>
	
	<?php if(isset($nv) && !empty($nv)){ ?>
		var lesson_id='<?php echo convert_uudecode(base64_decode($nv)); ?>';
		<?php 
		$course_sub=$this->Utility->getCourseSearch($this->Session->read('LocTrain.id'),convert_uudecode(base64_decode($nv)));
			//print_r($course_sub);
			if(!empty($course_sub)){
			$m_id = $this->Session->read('LocTrain.id');
			$owner_id = base64_encode(convert_uuencode($course_sub['Course']['m_id']));
			$extension = $course_sub['extension'];
			$video = isset($course_sub['video'])?$course_sub['video']:'';
			$decoded_id = $course_sub['Course']['id'];	   
			$first_lesson=$this->Utility->getFirstLesson($decoded_id);
			$subscription = $this->Utility->getCoursesubscription($m_id,$decoded_id,@$first_lesson['CourseLesson']['id']);
			$subscribed=$this->Utility->getCoursesubscribed($m_id,$decoded_id);
			if((!empty($subscription) || $subscribed)&& $video !='')
						{
							if($extension!='swf' && $extension!='zip')
							{ ?>
							var getUrl = "<?php echo $nv; ?>";	
							var w = 700;
							var h = 400;
							var left = (screen.width/2)-(w/2);
							var top = (screen.height/2)-(h/1.5);
							window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
							<?php }else {?>
								
								window.location.href = ajax_url+'home/play_video_swf/<?php echo $video.'/'.$extension.'/'.$owner_id.'/'.$course_sub['Course']['title'];?>';
							<?php }						
						
							
						}else if($m_id == $course_sub['Course']['m_id']){
							if($extension!='swf' && $extension!='zip' && $video !='')
							{ ?>
								var getUrl = "<?php echo $nv; ?>";	
								var w = 700;
								var h = 400;
								var left = (screen.width/2)-(w/2);
								var top = (screen.height/2)-(h/1.5);
								window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
							<?php }else if($video !=''){ ?>
								window.location.href = ajax_url+'home/play_video_swf/<?php echo $video.'/'.$extension.'/'.$owner_id.'/'.$course_sub['Course']['title'];?>';
							<?php }	
							}
			
				
			}else{
		?>
		//alert($('.lesson_'+lesson_id).length);
		if($('.lesson_'+lesson_id).length ==1 && $('.lesson_'+lesson_id).hasClass('open_video')){
			
			var getUrl = "<?php echo $nv; ?>";	
			var w = 700;
			var h = 400;
			var left = (screen.width/2)-(w/2);
			var top = (screen.height/2)-(h/1.5);
			window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
			
			}
	<?php } } ?>
	
	 <?php if($ulogin==0){ ?>
	$('body').on('click','.open_video_search',function(e){ e.preventDefault();
		var str=$(this).attr('href');
		var getUrl = str.replace("<?php echo ROOT_FOLDER;?>home/play_video_swf/",'');	
		//$(this).attr('href','');		
			$('#searchFooV').remove();
			$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
							if($('.Header1').is(':visible')){
								$('#myModal1').modal('show');
								$('<input>').attr({
								    type: 'hidden',
								    id: 'searchFooV',
								    value:getUrl,
								    name: 'redirectSearchZip'
								}).appendTo('form#memberLogin');
							}else if($('.Header2').is(':visible')){
								$('<input>').attr({
								    type: 'hidden',
								    id: 'searchFooV',
								    value:getUrl,
								    name: 'redirectSearchZip'
								}).appendTo('form#memberLogin2');
								$('#myModal2').modal('show');
							}else if($('.Header3').is(':visible')){
								$('<input>').attr({
								    type: 'hidden',
								    id: 'searchFooV',
								    value:getUrl,
								    name: 'redirectSearchZip'
								}).appendTo('form#memberLogin3');
							     $('#myModal3').modal('show');
							}else{}
							return false;
			
			
			
	});<?php }?>
$('body').on('click','.open_video',function(){
		var getUrl = $(this).attr('id');	
		var w = 700;
		var h = 400;
		var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/1.5);
        <?php if($ulogin==0){ ?>
			$('#searchFoo').remove();
			$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
							if($('.Header1').is(':visible')){
								$('#myModal1').modal('show');
								$('<input>').attr({
								    type: 'hidden',
								    id: 'searchFoo',
								    value:getUrl,
								    name: 'redirectSearch'
								}).appendTo('form#memberLogin');
							}else if($('.Header2').is(':visible')){
								$('<input>').attr({
								    type: 'hidden',
								    id: 'searchFoo',
								    value:getUrl,
								    name: 'redirectSearch'
								}).appendTo('form#memberLogin2');
								$('#myModal2').modal('show');
							}else if($('.Header3').is(':visible')){
								$('<input>').attr({
								    type: 'hidden',
								    id: 'searchFoo',
								    value:getUrl,
								    name: 'redirectSearch'
								}).appendTo('form#memberLogin3');
							     $('#myModal3').modal('show');
							}else{}
							
			
			<?php }else{ ?>
			 window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
			<?php } ?>	
		
	});	
	$('body').on('click','.noSubcribe',function(){
		var getUrl = $(this).attr('id');	
		var w = 700;
		var h = 400;
		var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/1.5);
        <?php if($ulogin==0){ ?>
			$('#noSubcribe').remove();
			$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
							if($('.Header1').is(':visible')){
								$('#myModal1').modal('show');
								$('<input>').attr({
								    type: 'hidden',
								    id: 'noSubcribe',
								    value:getUrl,
								    name: 'redirectSearchnoSubcribe'
								}).appendTo('form#memberLogin');
							}else if($('.Header2').is(':visible')){
								$('<input>').attr({
								    type: 'hidden',
								    id: 'noSubcribe',
								    value:getUrl,
								    name: 'redirectSearchnoSubcribe'
								}).appendTo('form#memberLogin2');
								$('#myModal2').modal('show');
							}else if($('.Header3').is(':visible')){
								$('<input>').attr({
								    type: 'hidden',
								    id: 'noSubcribe',
								    value:getUrl,
								    name: 'redirectSearchnoSubcribe'
								}).appendTo('form#memberLogin3');
							     $('#myModal3').modal('show');
							}else{}
							
			
			<?php }else{ ?>
			// window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
			<?php } ?>	
		
	});	
	
		
	
	$(".add_learning").on('click',function(){
		var getId = $(this).attr('rel');
		$(".load_top").show();	
		var a =$(this);	
		req = $.ajax({
				url:ajax_url+'Home/add_to_learning/'+getId,
				dataType: 'json',
				success: function(resp){
				
					if(resp.status == 'ok'){
						$(".load_top").hide();
						a.addClass('added_learning added_learning_sub');	
						a.removeClass('add_learning');		
						a.attr('title','<?php echo __('this_course_is_already_in_my_learning'); ?>');
						/*$(".ajax-success").html(resp.msg).show();
						$('.ajax-success').fadeOut(10000);*/
						//$('#middle').prepend('<div class="ajax-success"> </div>');
					}
					else if(resp.status == 'error'){
						//window.location.href = resp.url;
					$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
							if($('.Header1').is(':visible')){
								$('#myModal1').modal('show');
							}else if($('.Header2').is(':visible')){
								$('#myModal2').modal('show');
							}else if($('.Header3').is(':visible')){
							     $('#myModal3').modal('show');
							}else
							{
							window.location.href = resp.url;}
					}
				}
		});
	});
	
});
<?php if(!empty($search_arr)) { ?>
$(document).scroll(function(){
	if ($(window).scrollTop()  == $(document).height() - $(window).height()){
 
	var option =  $("#dropdown7").val();
    var sort_option = $('#sortInput').val();
    var filertOpt=$('#filertOpt').val();
	if( $("#dropdown7").val()=='')
    	{option='week';
	
	}else{
	option =  $("#dropdown7").val();
	}
  

    var getId = $(".tchng_cont_content").attr('id');
    var keyword = $("#search_text").val();
    if(keyword=='')
    {	
            alert('<?php echo __("please_fill_a_keyword") ?>');
            return false;
    }
if(req && req.readystate != 4){
    req.abort();
    }		

    
	if(option ==''){

		if(getId != '0'){		
					$(".loading_inner").show();	
					var keyword = $("#search_text").val();
					req = $.ajax({
						url:ajax_url+'Home/search/',
						data: {'q':keyword,'records':getId,'type':1 , 'sort_option':sort_option,'filter_opt':filertOpt},
						type: 'get',
						success: function(resp){	
							$(".tchng_cont_content").append(resp);	
							$(".loading_inner").hide();
						}
					});
				}
    		

	}else{
    switch(option){

            case 'week':
                    var date_to = new Date();
                    d_to = Math.floor(date_to.getTime()/1000);
                    var date_from = new Date();
                    date_from.setDate(date_from.getDate()-7);
                    d_from= Math.floor(date_from.getTime()/1000);


                    break;
            case 'month':
                    var date_to = new Date();

                    d_to = Math.floor(date_to.getTime()/1000);
                    var date_from = new Date();
                    //alert(date_from.getMonth());
                    date_from.setMonth(date_from.getMonth()- 1);
                    //d_from=date_from.getTime()/1000;
                    d_from= Math.floor(date_from.getTime()/1000);

                    break;
            case 'between':
                  var date_f=$('#from_date').val();
                    var date_t=$('#to_date').val();
                    if(date_f=='' && date_t=='')
                    {
		    d_from='';
	            d_to='';
                            /*alert('<?php echo __('please_select_the_dates_first') ?>'); 
                            //alert('Please select the dates first.');
                            return false;*/
			
                    }  else if(date_f !='' && date_t==''){		
	            var date_f_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_f);	          
	            var date_from = new Date(date_f_n);	           
	            d_from= Math.floor(date_from.getTime()/1000);
	            d_to='';
	            break;
			} else if(date_f =='' && date_t !='') {
                    var date_t_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_t);	          
	            var date_to = new Date(date_t_n);	         
	            d_to = Math.floor(date_to.getTime()/1000);
	            d_from='';
	            d_to=d_to+23455;
	            break;
			}else{
	 		var date_t_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_t);
                    var date_f_n=convertData('<?php echo $this->Session->read('LocTrain.locale'); ?>',date_f);
                    var date_to = new Date(date_t_n);
                    var date_from = new Date(date_f_n);
                    d_to = Math.floor(date_to.getTime()/1000);
                    d_from= Math.floor(date_from.getTime()/1000);
                    d_to=d_to+23455;
                    break;
		}
    }
var current_filter=$('#turn_on_off_chk').val(); 
	if(current_filter=='turn_filter_off')
    {

	}else{
d_to='';d_from='';
	}
    if(getId != '0'){
$(".loading_inner").show();
	    req = $.ajax({
		    url:ajax_url+'Home/search/',
		    data: {'q':keyword,'records':getId,'type':1,'date_from':d_from ,'date_to':d_to, 'sort_option':sort_option,'filter_opt':filertOpt },
		    type: 'get',
		    success: function(resp){
		  	$(".tchng_cont_content").append(resp);	
			$(".loading_inner").hide();
		        $(".loading_inner").hide();
		    }
	    });	
	}
}
}
});
<?php } ?>

</script>
    <!--Start Tab Drop section --> 
    <?php if($this->Session->read("LocTrain.flashMsg") != ''){ ?>
	<div class="account-activated">	<?php echo __($this->Session->read("LocTrain.flashMsg")); ?></div>
<?php $this->Session->delete("LocTrain.flashMsg");  } ?>
        <?php echo $this->Form->input('q',array('type'=>'hidden','id'=>'search_text','class'=>'','label'=>false,'div'=>false,'value'=>$q)); ?> 
 <?php echo $this->Form->input('sortby',array('type'=>'hidden','id'=>'sortInput','value'=>'1')); ?>
 <?php echo $this->Form->input('sortby',array('type'=>'hidden','id'=>'filertOpt','value'=>'title')); ?>
    <section class="searchsection">
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="tabbable Tabbable2 searchTabable">                                        	
                        
                            <ul class="nav nav-tabs">                                                                                   	                                            
                                                        
                                <li class="active" id="title_li"><a href="#tab1" rel="title" class="tabOpt" id="title_tab" data-toggle="tab"><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i><?php echo __('title');?></a></li>
                                <li class="" id="date_li"><a href="#tab2" rel="date" id="date_tab"  class="tabOpt" data-toggle="tab"><i class="fa fa-caret-up optDisble "></i><i class="fa fa-caret-down optEnble "></i><?php echo __('date');?></a></li>
                                <li class=""><a href="#tab3" class="search_turn_on_off_chk"  id="turn_on_off_chk" value="turn_filter_on"><i class="fa fa-caret-up"></i><?php echo __('turn_filter_on');?></a></li>
                            </ul>
                        
                            <div class="tab-content">
                             <input type="hidden" value="title" id="on_off_div" />
                                <div class="tab-pane active" id="tab1">
                                	
                                </div>
                                
                                <div class="tab-pane" id="tab2">
                                                                	                                    
                                  
                                       <!-- <div class="tchng_cont_content" id="<?php echo $last_id;?>">
                                            
                                        </div>-->
                                   
                                    </div>       
					  <div class="FilterBox">
                                    
                                    	<div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1"><?php echo __('filter_by_date');?></label>
                                                    <div id="" class="dropdown DropdownField">
                                                        <button class="btn btn-default dropdown-toggle Textname" type="button" id="dropdown7" data-toggle="dropdown">Dropdown<span class="caret"></span></button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdown7">
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="week"><?php echo __('published_this_week');?></a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="month"><?php echo __('published_this_month');?></a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="between"><?php echo __('published_between_two_dates');?></a></li>
                                                        </ul>
                                                    </div>   
                                                </div>
                                            </div>
                                        </div>
                                        <div id="date_div">
                                        <div class="row">
											  <?php
                                                    $str_date=date('Y,n,j', strtotime("+1 days")); 
                                                    $from_disable_date="{from:[".$str_date."],to:'>'}";
                                                    ?>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                                <div class="form-group  date_selected df" id="">
                                                    <label for="published_from"><?php echo __('filter_by_date_published_from');?></label>
                                                    <input  type="text" name="from_date" data-beatpicker-id="from_date" data-beatpicker="true" placeholder="" id="from_date" data-beatpicker-disable="<?php echo $from_disable_date;?>" data-beatpicker-format="<?php echo $this->Timezone->dateFormatAccoringLocaleSearch($this->Session->read('LocTrain.locale'))?>"  class="form-control DatePicker">


                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                                <div class="form-group date_selected dt">
                                                    <label for="published_from"><?php echo __('filter_by_date_published_to');?></label>
                                                    <input  type="text" name="to_date" data-beatpicker-id="to_date" data-beatpicker="true" placeholder="" id="to_date" data-beatpicker-disable="<?php echo $from_disable_date;?>" data-beatpicker-format="<?php echo $this->Timezone->dateFormatAccoringLocaleSearch($this->Session->read('LocTrain.locale'))?>"  class="form-control DatePicker">
                                                </div>                                            
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                    	<div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                                            <span class="Link"><a id="reset_filter"><?php echo __('reset_filter');?></a></span>
                                        </div>
                                    </div>
                                    </div>
                                    </div>                             
                                    <?php if(!empty($search_arr)) { ?>
				 <div class="tchng_cont_content"  id="<?php echo $last_id;?>">
                                            <?php echo $this->element('frontElements/home/search_list'); ?>
         
                                        </div>
                                          <?php }else{ ?>
                                            <div class="mytch_rcd">
                                                <?php if($q == ''){ ?>
                                                        <?php echo __('enter_search_text_in_the_field_above'); ?>

                                                <?php }else {  ?>
                                                        <?php echo __("no_result_matches_the_chosen_filter");?>
                                                <?php } ?>
                                            </div>
                                          <?php } ?>
                               
<!--                                <div class="tab-pane" id="tab3">
                                    <p>Filter</p>
                                </div>                                                               -->
                            </div>
                        
                        </div>                                               
                </div>
            </div>
        </div>
             <div class="loading" style="margin-left: -12px">
                    <div class="loading_inner">
                        <?php echo $this->Html->image('front/ajax-loader.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                    </div>
              </div>
    </section>     
    
    <!--End Tab Drop section --> 

<script>
    jQuery(document).ready(function () {
        $(".rating").rating({
            starCaptions: function(val) {
                if (val < 3) {
                    return val;
                } else {
                    return 'high';
                }
            },
            starCaptionClasses: function(val) {
                if (val < 3) {
                    return 'label label-danger';
                } else {
                    return 'label label-success';
                }
            }
        });
            $('.rating').on('rating.change', function(event, value, caption) {
                var attr_id=$(this).attr('id');
                var arr = attr_id.split('input-21a-');
                var id =arr[1];
                var rating=value;
                $.ajax({
                           url: ajax_url+"Home/update_rating",
                           type: "POST",
                           data: {course_id : id, rating: rating},
                           dataType: "html"
                     }); 
//                console.log(id);
//                console.log(value);
//                console.log(caption);
                });
    });
</script>
