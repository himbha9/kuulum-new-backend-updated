 <?php echo $this->Html->css('jquery-ui.css'); ?>
<?php echo $this->Html->script('jquery-ui.js')?>
<?php $locale=$this->Session->read('LocTrain.locale');
		
	if($locale=='ar')
	{
		echo $this->Html->script('ui.datepicker-ar.js');
	}
	if($locale=='zh')	{
		echo $this->Html->script('ui.datepicker-zh.js'); 
	}
	if($locale=='de')	{
		echo $this->Html->script('ui.datepicker-de.js'); 
	}
	if($locale=='fr')
	{ 
		echo $this->Html->script('ui.datepicker-fr.js');
	}
	if($locale=='ru')
	{ 
		echo $this->Html->script('ui.datepicker-ru.js');
		
	}
	if($locale=='it')
	{ 
		echo $this->Html->script('ui.datepicker-ru.js');
		
	}
	if($locale=='ja')
	{ 
		echo $this->Html->script('ui.datepicker-ru.js');
		
	}
	if($locale=='ko')
	{ 
		echo $this->Html->script('ui.datepicker-ko.js');
		
	}
	if($locale=='pt')
	{ 
		echo $this->Html->script('ui.datepicker-pt.js');
		
	}
	if($locale=='es')
	{ 
		echo $this->Html->script('ui.datepicker-es.js');
		
	}
?>

<script type="text/javascript">
var req = '';
var from_date='0';
$(document).ready(function(){
	$(".regfld_txt_search").attr('disabled',true);
	$('.return_to').live('click',function(){
		$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
	});
	
	$(".filter_options_chk").live("click",function(){
		var getId = $(this).prev().val();
		if(getId == '1'){
			$(this).css({'background-position':'0 0px'});
			$(this).prev().val('0');
		}
		else if(getId == '0'){
			$(this).css({'background-position':'0 -30px'});			
			$(this).prev().val('1');
		}
	});
	$(".filter_options_rdo").live("click",function(){
		var getId = $(this).attr('id');	
		$(".filter_options_rdo").removeClass('rdo_chkd');
		$(".tchng_cont_content").attr('id','0');
		$(this).addClass('rdo_chkd');		
		switch(getId){
			case 'opt1':
				$("#radioInput").val('1');
				$(".keyword_btm").css('display','none');
				$(".regfld_txt_search").attr('disabled',true);
				
				
			break;
			case 'opt2':
				$("#radioInput").val('2');
				$(".keyword_btm").css('display','none');
				$(".regfld_txt_search").attr('disabled',true);
				
			break;
			case 'opt3':
				$("#radioInput").val('3');
				$(".keyword_btm").css('display','block');
				$(".regfld_txt_search").removeAttr('disabled');
			break;
		}
	});
	
	$(".filter_options_rdo1").live("click",function(){
		var getId = $(this).attr('id');	
		$(".filter_options_rdo1").removeClass('rdo_chkd');
		$(this).addClass('rdo_chkd');		
		switch(getId){
			
			case 'oldest':
			
				$("#sortInput").val('1');
				
				
			break;
			
			case 'newest':
				$("#sortInput").val('2');
				
				
				
			break;
			
		}
	});
	
	
	
	
$(".open_video").live('click',function(){
		var getUrl = $(this).attr('id');	
		var w = 700;
		var h = 400;
		var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/1.5);	
		window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
	});		
	$(".read_more_review").live('click',function(){
		var tabText = $(this).html();
		var tabNewText = $(this).attr('rel');
		$(this).parent().prev().show();
		$(this).parent().prev().prev().hide();
		$(this).attr('class','read_less_review');
		$(this).attr('rel',tabText);
		$(this).html(tabNewText);		
		return false;
		
	});
	$(".read_less_review").live('click',function(){
		var tabText = $(this).html();
		var tabNewText = $(this).attr('rel');	
		$(this).parent().prev().hide();
		$(this).parent().prev().prev().show();
		$(this).attr('class','read_more_review');		
		$(this).attr('rel',tabText);
		$(this).html(tabNewText);	
		return false;		
	});
	
	
	
	$("#datepicker").datepicker({	
			
			dateFormat : 'yy-mm-dd',
			changeMonth : true,
			changeYear : true,		
			yearRange : '1930:2030',
			inline: true,
			maxDate: new Date(),
			
			onSelect: function(dateSelected){
								
								$(this).parent('.date_selected').attr('id',dateSelected);
								var getId = $(".tchng_cont_content").attr('id');
								$("#datepicker1").parent('.date_selected').attr('id','');
								$("#datepicker1").val('');
								$( "#datepicker1" ).datepicker( "destroy");
								if(req && req.readystate != 4){
									req.abort();
								}
								$("#datepicker1").datepicker({	
			
									dateFormat : 'yy-mm-dd',
									changeMonth : true,
									changeYear : true,		
									yearRange : '1930:2030',
									inline: true,
									minDate: new Date(dateSelected),
									maxDate: new Date(),
									
									onSelect: function(dateSelected1){
											$("#datepicker1").parent('.date_selected').attr('id',dateSelected1);
											var getId = $(".tchng_cont_content").attr('id');
												
											if(req && req.readystate != 4){
													req.abort();
											}					
									}
									
								});
				
			}
			
		});
		
		$('.scn_link').on('click',function(){
			$(".tchng_cont_content").attr('id','0');
			var option = $('#radioInput').val();
			var sort_option = $('#sortInput').val();
			
			if(option=='')
			{
				alert('<?php echo __('select_one_option') ?>'); 
				return false;
			}
			var getId = $(".tchng_cont_content").attr('id');
			var keyword = $("#search_text").val();
			if(keyword=='')
			{	
				alert('<?php echo __('please_fill_a_keyword') ?>');
				
				return false;
			}
			switch(option){
				
				case '1':
					var date_to = new Date();
					d_to = Math.floor(date_to.getTime()/1000);
					var date_from = new Date();
					date_from.setDate(date_from.getDate()-7);
					d_from= Math.floor(date_from.getTime()/1000);
					
					
					break;
				case '2':
					var date_to = new Date();
					
					d_to = Math.floor(date_to.getTime()/1000);
					var date_from = new Date();
					//alert(date_from.getMonth());
					date_from.setMonth(date_from.getMonth()- 1);
					//d_from=date_from.getTime()/1000;
					d_from= Math.floor(date_from.getTime()/1000);
						
					break;
				case '3':
					var date_f=$('.df').attr('id');
					var date_t=$('.dt').attr('id');
					if(date_f=='' || date_t=='')
					{
					
						alert('<?php echo __('please_select_the_dates_first') ?>'); 
						//alert('Please select the dates first.');
						return false;
					}
					var date_to = new Date(date_t);
					var date_from = new Date(date_f);
					d_to = Math.floor(date_to.getTime()/1000);
					d_from= Math.floor(date_from.getTime()/1000);
					d_to=d_to+23455;
					break;
			}
			
			if(req && req.readystate != 4){
			req.abort();
			}		
					
			$(".loading_inner").show();	
				
			req = $.ajax({
				url:ajax_url+'Home/search/',
				data: {'q':keyword,'records':getId,'type':2,'date_from':d_from ,'date_to':d_to, 'sort_option':sort_option },
				type: 'get',
				success: function(resp){
				
						$(".tchng_cont_content").html(resp);
						if($(".tchng_cont_content").attr('id') == '0')
						{
							$(".tchng_cont_content").prepend('<div class="no_rec">No result maches the chosen filters.</div>');
						}
					
					$(".loading_inner").hide();
				}
			});
			
		});
	$(".add_learning").live('click',function(){
		var getId = $(this).attr('rel');
		$(".load_top").show();	
		var a =$(this);	
		req = $.ajax({
				url:ajax_url+'Home/add_to_learning/'+getId,
				dataType: 'json',
				success: function(resp){
				
					if(resp.status == 'ok'){
						$(".load_top").hide();
						a.addClass('added_learning');	
						a.removeClass('add_learning');
						/*$(".ajax-success").html(resp.msg).show();
						$('.ajax-success').fadeOut(10000);*/
						//$('#middle').prepend('<div class="ajax-success"> </div>');
					}
					else if(resp.status == 'error'){
						window.location.href = resp.url;
					}
				}
		});
	});
	
});
<?php if(!empty($search_arr)) { ?>
$(document).live('scroll',function(){
	if ($(window).scrollTop()  == $(document).height() - $(window).height()){		
		var getId = $(".tchng_cont_content").attr('id');	
		if(req && req.readystate != 4){
			req.abort();
		}	
			var option = $('#radioInput').val();
			var sort_option = $('#sortInput').val();
			//alert(option); 
			if(option=='')
			{
				if(getId != '0'){		
					$(".loading_inner").show();	
					var keyword = $("#search_text").val();
					req = $.ajax({
						url:ajax_url+'Home/search/',
						data: {'q':keyword,'records':getId,'type':1 , 'sort_option':sort_option},
						type: 'get',
						success: function(resp){	
							$(".tchng_cont_content").append(resp);	
							$(".loading_inner").hide();
						}
					});
				}	
			}
			else
			{
				switch(option){
						
						case '1':
							var date_to = new Date();
							d_to = Math.floor(date_to.getTime()/1000);
							var date_from = new Date();
							date_from.setDate(date_from.getDate()-7);
							d_from= Math.floor(date_from.getTime()/1000);
							
							
							break;
						case '2':
							var date_to = new Date();
							
							d_to = Math.floor(date_to.getTime()/1000);
							var date_from = new Date();
							date_from.setDate(date_from.getMonth()- 1);
							d_from= Math.floor(date_from.getTime()/1000);
								
							break;
						case '3':
							
							var date_f=$('.df').attr('id');
							var date_t=$('.dt').attr('id');
							if(date_f=='' || date_t=='')
							{
							
								alert('<?php echo __('please_select_the_dates_first') ?>'); 
								//alert('Please select the dates first.');
								return false;
							}
							var date_to = new Date(date_f);
							var date_from = new Date(date_t);
							d_to = Math.floor(date_to.getTime()/1000);
							d_from= Math.floor(date_from.getTime()/1000);
								
							break;
					}
					
					if(getId != '0'){		
						$(".loading_inner").show();	
						var keyword = $("#search_text").val();
						req = $.ajax({
							url:ajax_url+'Home/search/',
							data: {'q':keyword,'records':getId,'type':1,'date_from':d_from ,'date_to':d_to , 'sort_option':sort_option},
							type: 'get',
							success: function(resp){	
								$(".tchng_cont_content").append(resp);	
								$(".loading_inner").hide();
							}
						});
					}	
			}
	}
});
<?php } ?>
</script>

<div id="wrapper"> 
  <div id="middle">
  <div class="ajax-success">	</div>
<?php if($this->Session->read("LocTrain.flashMsg") != ''){ ?>
	<div class="account-activated">	<?php echo __($this->Session->read("LocTrain.flashMsg")); ?></div>
<?php $this->Session->delete("LocTrain.flashMsg");  } ?>
    <div class="breadcrumb_botcont">
    	<div class="tchng_cont">
            <div class="tchng_cont_container"> 
              <div class="search_field gradient">
              	<?php echo $this->Form->create('Search',array('type'=>'GET','url'=>array('controller'=>'Home','action'=>'search','.')));?>
                	<?php echo $this->Form->input('q',array('type'=>'text','id'=>'search_text','class'=>'','label'=>false,'div'=>false,'value'=>$q)); ?> 
                    <?php echo $this->Form->submit(__('search'),array('class'=>'search_submit gradient')); ?>
                 <?php echo $this->Form->end(); ?>
                
                <div id="err_email" class="register_error"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>         
              </div>     
              <div class="tchng_cont_content" id="<?php echo $last_id;?>">
                  <?php if(!empty($search_arr)) { ?>
                    <?php echo $this->element('frontElements/home/search_list'); ?>
                  <?php }else{ ?>
                    <div class="mytch_rcd">
                    	<?php if($q == ''){ ?>
                        	<?php echo __('enter_search_text_in_the_field_above'); ?>
                        	
                    	<?php }else {  ?>
                        	<?php echo __('no_records_found_'); ?>
                        <?php } ?>
                    </div>
                  <?php } ?>
              </div>
              <div class="loading" style="margin-left: -12px">
                    <div class="loading_inner">
                        <?php echo $this->Html->image('front/loading.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                    </div>
              </div>
            </div>	
        </div>
          <div class="formfld_rgt tchng">
            <div class="section_guide"> <span class="guide_upper"> <?php echo __('your_search_results');?> </span>
              <h2 class="guide_head"> <i class="rstrt_rslt_i"><?php echo __('restrict_results'); ?> </i><p class="rstrt_rslt_p"></p> </h2>             
              <div class="guide_bot">  <span class="option"> <span class="div_cont"> <?php echo __('filter_by');?></span>  </span> 
              	
                <div class="sctn_radiolinks">
                	 <?php echo $this->Form->input('radio',array('type'=>'hidden','id'=>'radioInput','value'=>'')); ?>
                	 <div class="filter_cont">
                     	<div class="section_links filter_options_rdo" id="opt1">  </div>
                        <span class="filter_options_lbl"> <?php echo __('posted_this_week');?></span>
                     </div>
                     <div class="filter_cont">
                     	<div class="section_links filter_options_rdo" id="opt2">  </div>
                        <span class="filter_options_lbl"> <?php echo __('posted_this_month');?></span>
                     </div>
                     <div class="filter_cont">
                     	<div class="section_links filter_options_rdo" id="opt3">  </div>
                        <span class="filter_options_lbl"> <?php echo __('posted_between');?></span>
                     </div>
                	
                </div>
				<div class="vw_guide_bot keyword_btm" style="display:none;"> 
                        <span class="vw_option"> <?php echo __('from_date');?></span>
                         <span class="regfld_txtcont_search date_selected df" id="">
							
                         	<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'regfld_txt_search gradient','id'=>'datepicker','div'=>false,'label'=>false)); ?>
                         </span>
						 <span class="vw_option"> <?php echo __('to_date');?></span>
                         <span class="regfld_txtcont_search date_selected dt" id="">
							
                         	<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'regfld_txt_search gradient','id'=>'datepicker1','div'=>false,'label'=>false)); ?>
                         </span>
						
                </div>
				
				
				<div class="sctn_radiolinks uper_sps">
                	 <?php echo $this->Form->input('sortby',array('type'=>'hidden','id'=>'sortInput','value'=>'1')); ?>
                	 <div class="filter_cont">
                     	<div class="section_links filter_options_rdo1" id="oldest">  </div>
                        <span class="filter_options_lbl"> <?php echo __('sorted_by_oldest_first');?></span>
                     </div>
                     <div class="filter_cont">
                     	<div class="section_links filter_options_rdo1" id="newest">  </div>
                        <span class="filter_options_lbl"> <?php echo __('sorted_by_newest_first');?></span>
                     </div>
                     
                </div>
				
				<div class="section_links">
                    	 <?php echo $this->Html->link(__('apply_filter'),'javascript:void(0);',array('class'=>'scn_link')); ?> 
                </div>
				
              </div>
            </div>
          </div>
    </div>
  </div>
</div>