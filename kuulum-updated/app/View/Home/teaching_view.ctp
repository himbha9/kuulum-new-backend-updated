<script type="text/javascript">
$(document).ready(function(){
	$('.return_to').on('click',function(){
		$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
	})
});
</script>

<div id="wrapper"> 
  <div id="middle">
    <div class="breadcrumb_botcont">
      <?php echo $this->Form->create('Member',array('id'=>'memberRegister','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'register')));?>
          <div class="tchng_cont">
            <div class="tchng_content">
            	<div class="tchng_coursepic"><img src="../img/front/training_pik_old.png" width="200" height="125" /></div>
                <div class="tchng_picrgt">
                    <h3 id="section_subtitle">Course Title</h3>
                    <p id="section_desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <div class="tchng_coursedesc">
                        <span class="desc_span">Number of customers: 45 <a href="javascript:void(0);">View history</a></span>
                        <span class="desc_span">Income: $123.45 <a href="javascript:void(0);">View income</a></span>
                        <span class="desc_span">Number of reviews: 17 <a href="javascript:void(0);">Read reviews</a></span>
                    </div>
                    <p class="ltstdtm">Last watched on 06 March 2013, 14:32 GMT</p>.
                    <div class="tc_links">
                        <a id="tc_one" href="javascript:void(0);">Show less..</a>
                        <a id="tc_two" href="javascript:void(0);">Update</a>
                        <a id="tc_three" href="javascript:void(0);">Retire</a>
                        <a id="tc_four" href="javascript:void(0);">Preview</a>
                    </div>
                </div>
			</div>
			<div class="tchng_content">
            	<div class="tchng_coursepic"><img src="../img/front/training_pik_old.png" width="200" height="125" /></div>
                <div class="tchng_picrgt">
                    <h3 id="section_subtitle">Course Title</h3>
                    <p id="section_desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <div class="tchng_coursedesc">
                        <span class="desc_span">Number of customers: 45 <a href="javascript:void(0);">View history</a></span>
                        <span class="desc_span">Income: $123.45 <a href="javascript:void(0);">View income</a></span>
                        <span class="desc_span">Number of reviews: 17 <a href="javascript:void(0);">Read reviews</a></span>
                    </div>
                    <p class="ltstdtm">Last watched on 06 March 2013, 14:32 GMT</p>.
                    <div class="tc_links">
                        <a id="tc_five" href="javascript:void(0);">Read more..</a>
                        <a id="tc_two" href="javascript:void(0);">Update</a>
                        <a id="tc_three" href="javascript:void(0);">Retire</a>
                        <a id="tc_four" href="javascript:void(0);">Preview</a>
                    </div>
                </div>
			</div>            
          </div>
          <div class="formfld_rgt tchng">
            <div class="section_guide"> <span class="guide_upper"> <?php echo __('my_teaching');?></span>
              <h2 class="guide_head"> <?php echo __('restrict_results'); ?> <img src="../img/front/read_morearw.png" /> </h2>
              <h2 class="guide_head border-top"> <?php echo __('terms_of_use'); ?> </h2>
              <div class="guide_bot"> <span class="option"> <?php echo __('filter_by');?></span>
              	<div class="section-linkcover">
                    <div class="section_links">
                        <input class="scn-chkbox" type="checkbox" /><?php echo $this->Html->link(__('Item1'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                    </div>
                    <div class="section_links">
                        <input class="scn-chkbox" type="checkbox" /><?php echo $this->Html->link(__('Item2'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                    </div>
                    <div class="section_links">
                        <input class="scn-chkbox" type="checkbox" /><?php echo $this->Html->link(__('Item3'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                    </div>
				</div>
                <div class="sctn_radiolinks">
                	<div class="section_links">
                        <input class="scn-chkbox" type="radio" /><?php echo $this->Html->link(__('Option1'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                    </div>
                    <div class="section_links">
                        <input class="scn-chkbox" type="radio" /><?php echo $this->Html->link(__('Option2'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                    </div>
                    <div class="section_links">
                        <input class="scn-chkbox" type="radio" /><?php echo $this->Html->link(__('Option3'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                    </div>
                    <div class="section_links">
                    	<a class="scn_link" href="javascript:void(0);">Apply filter</a>
                    </div>
                </div>
              </div>
              <div class="guide_bot"> <span class="option"> <?php echo __('options');?></span>
			  <div class="section_links">
                  <a class="scn_link scn_margn" href="javascript:void(0);">Create a new course</a>
              </div>                
              </div>
            </div>
          </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<style>
.mandatory{	
	color:#FD2600;
}
</style>