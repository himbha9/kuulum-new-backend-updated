<script type="text/javascript">
$(document).ready(function(){
	$('.return_to').on('click',function(){
		$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
	})
});
</script>

<div id="wrapper"> 
  <div id="middle">
    <div class="breadcrumb_botcont">
          <div class="tchng_cont">
            <div class="vw_tchng_content">
            	<div class="vw_cntn_ilft"><h2>Labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h2></div>
                <div class="vwcntnt_hydng_ryt">
                	<iframe width="490" height="330" src="http://www.youtube.com/embed/h04_6GxIZhgwmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="vw_ftct">
                <span class="vw_tp_ryt">
                    <h3 id="vwsection_subtitle">Sunt in culpa qui officia deserunt mollit anim id est </h3>
                    <p class="vwsection_desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p class="vwsection_desc">quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
                  <i class="vwup_info"> Updated on 3 January 2013, 14:32 GMT </i>
                  </span>  
                <span class="vw_mn_btmnd">
                    <span class="vw_ls_btn">
                        <a href="javascript:void(0);"> Return to top</a>
                    </span>
                    <span class="vw_ls_btn_ryt">
                        <a class="bg1" href="javascript:void(0);"></a>
                        <a class="bg2" href="javascript:void(0);"></a>
                        <a class="bg3" href="javascript:void(0);"></a>
                    </span>
                  </span>	
            </div>
	      </div>
          <div class="vw_formfld_rgt">
          	<div class="vw_section_guide"> 
            	<span class="vw_guide_upper"> <?php echo __('In this section:');?> 
                </span>
              	<h2 class="vw_guide_head border-top"> <?php echo __('Your Options'); ?> </h2>
              	<div class="vw_guide_bot"> 
              		<span class="vw_option"> <?php echo __('Related Links');?> : 
              		</span>
                  	<div class="vw_section_links">
                        <span class="sct_lft">
                      <a class="vw_scn_link bg_ad" href="javascript:void(0);">Add to learning plan</a>
                    </span>
                        <span class="sct_lft">  
                      <a class="vw_scn_link bg_wch" href="javascript:void(0);">Watch Now </a>
                    </span>  
                        <span class="sct_lft">  
                      <a class="vw_scn_link bg_lyk" href="javascript:void(0);">Like</a>
                    </span>
                  	</div>                
              </div>
              <div class="vw_guide_bot"> 
                <span class="vw_option"> <?php echo __('Rate this content');?> : 
                </span>
                <span class="rt_img_tp"> 
                    <img src="../img/rt.png" />              
                </span>
              </div>
              <div class="vw_info_hdng_ryt">
              	<h2 class="vw_guide_head border-top"> <?php echo __('More Information'); ?> </h2>
              	<div class="vw_guide_bot"> 
              		<span class="vw_option"> <?php echo __('Specific to this content ');?> : 
              		</span>
                  	<div class="vw_section_links">
                        <span class="sct_lft">
                          <a class="vw_scn_link bg_ad_athr login-window" href="#login-box-athor">About the author</a>
                         
                        </span>
                        <span class="sct_lft">  
                          <a class="vw_scn_link bg_ad_athr" href="javascript:void(0);">17 reviews</a>
                        </span>  
                 	</div> 
                    <div id="login-box-athor" class="login-popup">
                    <a href="#" class="close"><img src="../img/dlt.png" class="btn_close" title="Close Window" alt="Close" /></a>
                  	<div class="vwcntnt_rv_otr">
                    	<h2>About the author</h2>
                        <div class="vwcntnt_rv_blk">
                            <span class="vwcntnt_rv_data">
                                <p><img src="../img/athr.png" />Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque iaudaan tium, totam rem aperiam, eaque ipsa quae ab illo 
                                inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut 
                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque iaudaan tium, totam rem aperiam, eaque ipsa quae ab illo 
                                inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                               
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut 
                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque iaudaan tium, </p>
                                <p> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut 
                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Sed ut perspiciatis unde .</p>
                                <a href="javascript:void(0);"> Contact</a>
                            </span>
                        </div>
                    </div>    
                  </div>
              	</div>
                <div class="vw_slctlng">
                	<span class="vw_option"> <?php echo __('Select your prefered language ');?> :</span>
                	<span class="vw_inpt_otritm">
                    	<span class="vw_inpt_otritm_btm">
                        	<input type="radio" name="nmb" /><p>English (United Kingdom)</p>
                        </span>
                        <span class="vw_inpt_otritm_btm">
                        	<input type="radio" name="nmb" /><p>German (Germany)</p>
                        </span>
                        <span class="vw_inpt_otritm_btm">
                        	<input type="radio" name="nmb" /><p>Chinese (China)</p>
                        </span>
                         <span class="vw_inpt_otritm_btm">
                        	<input type="radio" name="nmb" /><p>Japnese (Japan)</p>
                        </span>
                    
                    </span>
                </div>
              </div>
            </div>
          </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<style>
.mandatory{	
	color:#FD2600;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('a.login-window').click(function() {
		
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').on('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});
<?php /*?>	var select = document.getElementById('mySelect');
	select.onchange = function () {
		select.className = this.options[this.selectedIndex].className;
	}
});<?php */?>
</script>