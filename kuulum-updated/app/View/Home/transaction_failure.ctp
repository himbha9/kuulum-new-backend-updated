<script>
$(document).ready(function(){
	$('.register_success').fadeOut(10000);
});

</script>
<div id="wrapper"> 
  <div id="middle">
    <div class="breadcrumb_botcont">
      <div class="register_unsuccess">
      	<div class="reg_scleft">
        	<?php echo $this->Html->image('front/unsuccess_new.png',array('width'=>'50','height'=>'62')); ?>

        </div>
        <div class="reg_scrgt">
      		<p class="reg_msg_head"> <?php echo __('course_not_purchased');?> </p>
        	<span class="reg_msg"> <?php echo __('the_order_has_been_cancelled_and_you_have_not_been_charged');?> </span>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
.mandatory{	
	color:#FD2600;
}
.radio_image
{
	float:left;
	width:auto;
}
.radio_text{
	float:left;
	width:auto;
	margin-left:10px;
	margin-top:4px;
}
.term_chk_box
{
	float:left;
	width:auto;
}
.radioBox_container
{
	float:left;
	width:100%;
}
</style>