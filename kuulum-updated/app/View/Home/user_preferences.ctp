<?php $s_lang = array('en'=> __('english_united_states'),'de'=> __('german_germany'),'ar'=> __('arabic'),'fr'=> __('french_france'),'it'=> __('italian_italy'),'ja'=> __('japanese'),'ko'=> __('korean'),'pt'=> __('portuguese_portugal'),'ru'=> __('russian_russian_federation'),'zh'=> __('chinese_china'),'es'=> __('spanish_spain')); 
$t_lang = array('Mr'=>__('mr'),'Miss'=>__('miss'),'Mrs'=>__('mrs'));

?>
<?php $locale=$this->Session->read('LocTrain.locale');
		if($locale == 'ar') 
		{
			echo $this->Html->css('autoSuggest_arabic');
		}
		else{
			echo $this->Html->css('autoSuggest'); 
		}
		?>
<section>
  <div class="container InnerContent">
    <!-- <div class="row">    
        <div class="col-lg-12">  
            <div class="Title">Text Size</div>
            <div class="row PreferenceBox TextSizeBox">
                <div class="col-xs-3 Box Normal">
                    <div class="Text">
                        <?php echo $this->Html->image('front/text_img_normal.jpg',array('class'=>'img-responsive ','alt'=>'')); ?> 
                        <span rel='1' class="userzoom <?php if(isset($preference) && $preference=='normal'){echo 'selected';}?>" >
                            <?php echo $this->Html->link(__('normal'),array('controller' => 'Home', 'action' => 'user_preferences','normal'),array('style'=>'text-decoration:none;color:#7f7f7f !important')); ?>
                            </span>
                    </div>                    
                </div>
                <div class="col-xs-3 Box Large">
                    <div class="Text">
                        <?php echo $this->Html->image('front/text_img_large.jpg',array('class'=>'img-responsive ','alt'=>'')); ?> 
                        <span rel='1.1' class="userzoom <?php if(isset($preference) && $preference=='large'){echo 'selected';}?>">
                            <?php  echo $this->Html->link(__('large'),array('controller' => 'Home', 'action' => 'user_preferences','large'),array('style'=>'text-decoration:none;color:#7f7f7f !important'));?>
                            </span>
                    </div>                    
                </div>
                <div class="col-xs-3 Box Largest Last">
                    <div class="Text">
                        <?php echo $this->Html->image('front/text_img_largest.jpg',array('class'=>'img-responsive ','alt'=>'')); ?> 
                        <span rel='1.2' class="userzoom <?php if(isset($preference) && $preference=='largest'){echo 'selected';}?>">
                            <?php echo $this->Html->link(__('largest'),array('controller' => 'Home', 'action' => 'user_preferences','largest'),array('style'=>'text-decoration:none;color:#7f7f7f !important'));?>
                            </span>
                    </div>                    
                </div> 
                <div class="clearfix"></div>           
            </div>    	        
        </div>    
    </div>-->
    
    <div class="row">    
        <div class="col-lg-6 col-md-6 col-sm-10 col-md-offset-3 col-sm-offset-2">  
            <div class="Title"><?php echo __('contrast');?></div>
            <div class="row PreferenceBox TextSizeBox">
                <?php if($this->Session->read('LocTrain.locale')=='ar'){ ?>
                    <div class="col-xs-4 Box Normal text-center">
                        <div class="Text">
                           A 
                            
                        </div> 
<span class="<?php if(isset($css) && $css=='style_arabic'){echo 'selected';}?> userzoom">
                                <?php                     
                                echo $this->Html->link(__('normal'),array('controller' => 'home', 'action' => 'cookie_for_css','style_arabic'),array('class'=>'','style'=>' text-decoration: none;')); 
                                  ?>
                            </span>                   
                    </div>
                    <div class="col-xs-4 col-xs-offset-1 Box Large text-center">
                        <div class="Text">
                           A
                           
                        </div>  
 <span class="<?php if(isset($css) && $css=='contrast_arabic'){echo 'selected';}?> userzoom">
                                <?php                     
                                echo $this->Html->link(__('high'),array('controller' => 'home', 'action' => 'cookie_for_css','contrast_arabic'),array('class'=>'','style'=>' text-decoration: none;')); 
                                  ?>
                            </span>                  
                    </div>  
                <?php } else {?>
                <div class="col-xs-4 Box Normal text-center">
                    <div class="Text">
                       A
                        
                    </div>   
			<span class="<?php if(isset($css) && $css=='style'){echo 'selected';}?> userzoom">
                            
                             <?php                     
                                echo $this->Html->link(__('normal'),array('controller' => 'home', 'action' => 'cookie_for_css','style'),array('class'=>'','style'=>' text-decoration: none;')); 
                                  ?>
                            </span>                 
                </div>
                <div class="col-xs-4 col-xs-offset-1 Box Large text-center">
                    <div class="Text">
                        A
                        
                    </div> 
<span class="<?php if(isset($css) && $css=='contrast'){echo 'selected';}?> userzoom">
                        <?php                     
                                echo $this->Html->link(__('high'),array('controller' => 'home', 'action' => 'cookie_for_css','contrast'),array('class'=>'','style'=>' text-decoration: none;')); 
                                  ?>
                        </span>                   
                </div>  
                <?php } ?>
                <div class="clearfix"></div>     
            </div>    	        
        </div>    
    </div>
    
    <div class="row">
    	<div class="col-lg-6 col-md-6 col-sm-8 col-md-offset-3 col-sm-offset-2 margintop20">
            <div class="form-group">
                <label for="exampleInputPassword1"><?php echo __('language');?></label>
                <div class="dropdown DropdownField" id="">
                    <button data-toggle="dropdown" id="dropdown_lang" type="button" class="btn btn-default dropdown-toggle LanguageRegion">
                        <?php foreach($language as $lang) {if($lang['Language']['locale']==$locale){echo $s_lang[$lang['Language']['locale']];}}?>
                        <span class="caret"></span></button>
                    <ul aria-labelledby="dropdown_lang" role="menu" class="dropdown-menu">
                         

<?php echo $this->Form->create('Language',array('id'=>'changeLanguage','url'=>'/Home/change_language')); ?>                        
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_rendered','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('lang',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_1','value'=>$this->Session->read('LocTrain.locale'))); ?>
                    <?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'inp_radio','id'=>'lan_id_1','value'=>$this->Session->read('LocTrain.LangId'))); ?>
                    <?php
                            $locale = $this->Session->read('LocTrain.locale') ? $this->Session->read('LocTrain.locale') : 'en';

                          /*   $s_lang = array('en'=>__('english_united_states'),'de'=>__('german_germany'),'ar'=>__('arabic'),'fr'=>__('french_france'),'it'=>__('italian_italy'),'ja'=>__('japanese'),'ko'=>__('korean'),'pt'=>__('portuguese_portugal'),'ru'=>__('russian_russian_federation'),'zh'=>__('chinese_china'),'es'=>__('spanish_spain'));*/
$s_lang = array('en'=>__('english_united_states_'),'de'=>__('deutsch_deutschland'),'ar'=>__('ar_lang'),'fr'=>__('fr_lang'),'it'=>__('it_lang'),'ja'=>__('ja_lang'),'ko'=>__('ko_lang'),'pt'=>__('pt_lang'),'ru'=>__('ru_lang'),'zh'=>__('zh_lang'),'es'=>__('es_lang'));


                            if(!empty($language))
                            {
                            foreach($language as $lang)
                            {
                            ?>	
                    <li role="presentation"><a onclick="changeLanguage('<?php echo $lang['Language']['id']; ?>','<?php echo $lang['Language']['locale']; ?>','changeLanguage','lan_1','lan_id_1');" tabindex="-1" role="menuitem" style=" text-decoration: none;"   id="<?php echo $lang['Language']['id']; ?>"><?php echo $s_lang[$lang['Language']['locale']]; ?></a></li>
                    <?php }} ?>   
                    <?php echo $this->Form->end(); ?> 


                       
                    </ul>
                </div>                
            </div>
        </div>
    </div>
    
  </div>
</section>
<script type="text/javascript">

$('document').ready(function(){
    
    $(".dropdown-menu li a").click(function(){
    $("#dropdown_lang:first-child").text($(this).text()).append('<span class="caret"></span>' );
    $("#dropdown_lang:first-child").val($(this).attr('value'));
  });
   
        

});
     

</script>
