<?php $videoCdnURL = $this->Utility->bucketsCloudFront(LESSON_VIDEOS_BUCKET); ?>
<?php
$cookie_courses = array();
		if(!empty($getCoursesindex)){
			foreach($getCoursesindex as $course){
				$cookie_courses[] = convert_uudecode(base64_decode($course));
			}
		}
$cookie_lessons = array();
		$course_in_cart=array();
		if(!empty($getCoursesindex)){
			
			foreach($getCoursesindex as $key=>$val){
				if(strlen($key)>8)
				{
					$cookie_lessons[] = convert_uudecode(base64_decode($val));
				}
				else
				{
					if($id==$val)
					{
						$course_in_cart[]=$val;
					}
				}
			}
		}
$s_lang = array('en'=> __('english_united_states'),'de'=> __('german_germany'),'ar'=> __('arabic'),'fr'=> __('french_france'),'it'=> __('italian_italy'),'ja'=> __('japanese'),'ko'=> __('korean'),'pt'=> __('portuguese_portugal'),'ru'=> __('russian_russian_federation'),'zh'=> __('chinese_china'),'es'=> __('spanish_spain')); 

if(isset($css) && $css=='style'){
$img_icon='icons';
}else{
$img_icon='icons_contrast';
}
?>

<?php echo $this->Html->script('/jwplayer/jwplayer');?>
<?php //echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'jwplayer/jwplayer.js')); ?>

<script type="text/javascript">jwplayer.key="NCsSCSxmmK/Kp06tPL4+X17g7+t8VkvEPG9v6g==";</script>

<?php echo $this->Html->script('Scripts/jquery.raty.js');?>
<?php echo $this->Html->script('Scripts/jquery.msgBox.js');?>
<?php
//echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/Scripts/jquery.raty.js'));
//echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/Scripts/jquery.msgBox.js'));
?>

<?php /* echo $this->Html->css('Styles/msgBoxLight.css');*/?>
<?php $locale = $this->Session->read('LocTrain.locale'); ?>
<?php //echo $this->Html->css('captivate.css'); ?>
<?php //echo $this->Html->css($this->Utility->getAWSImgUrl(STATIC_BUCKET,'css/captivate.css')); ?>

<?php echo $this->Html->script('standard.js');
  $locale = $this->Session->read('LocTrain.locale');
?>
<?php //echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/standard.js')); ?>

<style type="text/css">
    .CourseKeywords .SearchKeywords {
    word-break: normal;
}
#CaptivateContent { 
    text-align:center;
    height:100%;
}
.Link {
   
    word-break:normal;
}
.vwcrsply_btn {
    border: 1px solid #3f3f3f;
    border-radius: 32px;
    float: left;
    left: 45%;
    position: absolute;
    top: 40%;
    width: auto;
}

.ply_btn_anchr {
    /*background: url("../../img/play-32-black.png") no-repeat scroll 12px 10px transparent;*/
    float: left;
    padding: 26px;
    width: auto;
}
</style>
<!-- main container section -->
<section>
    <?php $add_edit_chk=0;
       if(!empty($course_details['CourseRemark'])) 
			{$add_edit_chk=0;
				foreach($course_details['CourseRemark'] as $remark)
				{
                                    if($this->Session->read('LocTrain.id')==$remark['Member']['id']){
                                       $add_edit_chk=1; 
                                    }
                                }


			
			
                        }   
		$rating=0; $rate='';
		$is_rated = $course_details['CourseRating'];			 
			   for($i=0;$i<count($is_rated);$i++)
			   {
			   
			   $rate+=$is_rated[$i]['rating'];
			   }
			 
		if(count($is_rated)>0){ $is_rated1=$rate/count($is_rated);}else{ $is_rated1=0;}
			   if(!empty($is_rated))
			   {
			   if(count($is_rated)>1)
			   {
			   $rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
			   }
			   else
			   {
			   $rating=$is_rated1;
			   }
			    
			   }
			else
			{
				$rating= 0;
			}
    
    $video = '';
							$lesson_ID='';$less_image='';$lesson_image ='';
 //echo '<pre>';print_r($course_details['CourseLesson']);echo '</pre>';
								if(!empty($course_details['CourseLesson'])){
									$no_of_lessons = count($course_details['CourseLesson']);
									
									foreach($course_details['CourseLesson'] as $lesson){
										if($lesson['video'] != ''){
											$video = $lesson['video'];
											$lesson_ID=$lesson['id'];
											$extension=$lesson['extension'];
											$lesson_image = $lesson['image'];
											break;
										}

									}
foreach($course_details['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                       $less_image=$lesson['image'];
                        break;
                    }
                }
								} //pr($extension);die;								
					?>
  <div class="container InnerContent">
    <div class="row">
    
    <div class="col-lg-7 col-md-7 col-sm-7">
	
    <div class="SmallLinkBoxWrapper SmallLinkBoxWrapper1  SmallLinksWrapper">   
        <div class="SmallLinkBox">
                
              
                    <?php
                        $class ='';
                         $url='';
                        if($this->Session->read('LocTrain.id')=="" && $course_details['Course']['price']!=0.00)
                        {
                                $class = in_array($course_details['Course']['id'],$cookie_courses)?'vw_scn_link bg_ad_basket added_learning_sub':'vw_scn_link bg_ad add_to_cart';
                                $url = (in_array($course_details['Course']['id'],$cookie_courses) || ($this->Session->read('LocTrain.login')!=1))?'javascript:void(0);':array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($course_details['Course']['id'])));	
                                echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_add_to_cart.svg',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class,'escape'=>false));
                                echo '<span class="Link">'.$this->Html->link(__('add_to_basket'),$url,array('class'=>$class,'escape'=>false)).' </span>';
                        }
                        else if(!$subscribed)
                        { 
							$ulogin=0;
							$uclass='';
							if($this->Session->read('LocTrain.id'))
								{
								$ulogin=1;
								}if($ulogin==0 && $course_details['Course']['price']==0.00)
								{
								$uclass='free_course_play';

									}
							
                                if($m_id == $course_details['Course']['m_id']) {
                                        $class = 'vw_scn_link bg_ad_basket added_learning_sub';
                                        $url='javascript:void(0);';
                                }
                                else
                                {
                                        $class = in_array($course_details['Course']['id'],$cookie_courses)?'vw_scn_link bg_ad_basket added_learning_sub':'vw_scn_link bg_ad add_to_cart';
                                        $url = in_array($course_details['Course']['id'],$cookie_courses)?'javascript:void(0);':array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($course_details['Course']['id'])));					
                                       /* if( isset($set) && $set =='unlink')
                                        {
                                                $class = 'vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
                                                $url='javascript:void(0);';

                                        }									
                                        else*/
                                        $lesson_pur=isset($subscription['0']['lesson_id'])?$subscription['0']['lesson_id']:$subscription['lesson_id'];
                                         if((!empty($subscription) && $lesson_pur =="") || trim($course_details['Course']['price'])==0.00)
                                        { 
                                                $class = 'vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
                                                $url='javascript:void(0);';
                                        }else if(!empty($subscription)){
											
											foreach($subscription as $subscription){
												if($subscription['lesson_id']==""){
												$class = 'vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
                                                $url='javascript:void(0);';}
												}
											}
                                }
                                if($url != 'javascript:void(0);'){
                                 echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_add_to_cart.svg',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class,'escape'=>false));    
                                echo '<span class="Link">'.$this->Html->link(__('add_to_basket'),$url,array('class'=>$class,'escape'=>false)).' </span>';
                                }else{
                                    
                              //  echo $this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>''));    
                              ?><?php // echo $this->Html->link(__('add_to_basket'),$url,array('class'=>$class)).' </span>';
                                 
                                if($video != '' && $extension!="swf" && $extension!="zip")
                                {
                                        if($m_id == $course_details['Course']['m_id']) 
                                        {

                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                        ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                       ?></span>
                                       
                                       <?php } 
                                       /*else if(isset($set) && $set =='unlink')
                                        {
                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                          ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php      echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                    ?></span>
                                    
                                    <?php   }	*/
                                        else
                                        {
                                                if($course_details['Course']['price']!= 0.00)
                                                {
                                                        if(!$subscribed && !empty($subscription))
                                                        {	

                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>(!empty($subscription))?'watch':'preview','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php    echo $this->Html->link(__((!empty($subscription))?'watch_now_':'play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>(!empty($subscription))?'watch':'preview','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                       ?></span><?php }
                                                        else
                                                        {
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                              ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?></span><?php }
                                                }else
                                                {

                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                               ?> </span> <?php }
                                        }
                                }else 
                                { 
                                        if($video != '' && (isset($extension)  && $extension=="swf" ))
                                        { 
                                                if($m_id == $course_details['Course']['m_id']) 
                                                { 
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php      echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                              ?></span><?php  } else if($set =='unlink')
                                                {
													if($uclass){
														
														 echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('class'=>'vw_scn_link bg_wch free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('class'=>'vw_scn_link bg_wch free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php 
														}else{
															  echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php 
															
															
															}
													
													
                                                         }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        { 
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        {
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                       ?> </span> <?php }
                                                                        else
                                                                        { 
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                        ?></span ><?php }
                                                                }else
                                                                { 
                                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                                      ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                             ?> </span><?php   }
                                                        }else{
															if($uclass){ 
																
																echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span>
																
																<?php }else{

                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span>
                                                        
                                                        <?php } }
                                                }
                                        }elseif($video != '' && (isset($extension) &&  $extension=="zip"))
                                        { 
                                                if($m_id == $course_details['Course']['m_id']) 
                                                { 
                                                                /*echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'https://s3.amazonaws.com/'.LESSON_VIDEOS_BUCKET.'/'.convert_uudecode(base64_decode($owner_id)).'/'.$video.'/index.html',array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));*/
echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php      /*echo $this->Html->link(__('watch_now_'),'https://s3.amazonaws.com/'.LESSON_VIDEOS_BUCKET.'/'.convert_uudecode(base64_decode($owner_id)).'/'.$video.'/index.html',array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));*/
echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

                                              ?></span><?php  } else if($set =='unlink')
                                                {
													if($uclass)
													{
													echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php  	
													}else{
													
                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php   } }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        { 
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        {
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                       ?> </span> <?php }
                                                                        else
                                                                        { 
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                        ?></span ><?php }
                                                                }else
                                                                { 
                                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                                      ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                             ?> </span><?php   }
                                                        }else{
															if($uclass){
																echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span><?php
																
															}else{

                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span><?php } }
                                                }
                                        }else {

                                               /* echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                               ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

?></span><?php */				                if($m_id == $course_details['Course']['m_id']) 
                                                { 
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

												?></span><?php  } else if($set =='unlink')
                                                {
													if($uclass)
													{
													echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php  	
													}else{
													
                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

?></span><?php   } }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        { 
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        {
                                                                               echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

?></span><?php }
                                                                        else
                                                                        { 
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                        ?></span ><?php }
                                                                }else
                                                                {
                                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                                      ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                             ?> </span><?php   }
                                                        }else{
															if($uclass){
																echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span><?php
																
															}else{
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

											?></span><?php } }  }
                                        }
                                        						

                                }?>
                                
                    <?php   }
                        }
			
                           $rel_val='';
                        if($video != '' && $extension!="swf" && $extension!="zip")
                                {
                                        if($m_id == $course_details['Course']['m_id']) 
                                        {
                                            $rel_val='watch';
                                        
                                        } else if($set =='unlink')
                                        { 
                                            $rel_val='watch';                                              
                                        }	
                                        else
                                        {
                                                if($course_details['Course']['price']!= 0.00)
                                                {
                                                        if(!$subscribed)
                                                        {	 $rel_val=(!empty($subscription))?'watch':'preview';

                                                             
                                                        }
                                                        else
                                                        { $rel_val='watch';
                                                              
                                                        }
                                                }else
                                                { $rel_val='watch';

                                                        
                                                }
                                        }
                                }else 
                                { 
                                        if($video != '' && (isset($extension)  && $extension=="swf" )||(isset($extension) &&  $extension=="zip"))
                                        { 
                                                if($m_id == $course_details['Course']['m_id']) 
                                                {  $rel_val='watch';
                                                         
                                                } else if($set =='unlink')
                                                { $rel_val='watch';
                                                  
                                                }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        {  
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        { $rel_val='watch';
                                                                              
                                                                        }
                                                                        else
                                                                        {  $rel_val='swf';
                                                                            
                                                                        }
                                                                }else
                                                                {  $rel_val='watch';
                                                                   $action='javascript:void(0);';
                                                                      
                                                                }
                                                        }else{ $rel_val='watch';
                                                               $action='play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'];
                                                                
                                                        }
                                                }
                                        }else { $rel_val='swf';
                                                $action='javascript:void(0);';
                                                


                                        }							

                                }
                    ?>
                
                <input type="hidden" class="bg_wch1 bg_wch" rel="<?php echo $rel_val;?>" id="<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>">
                  
            </div>
               
            <div class="SmallLinkBox">
                   <?php 	
       
                   $class = !empty($course_details_wish['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
                   $class1 = !empty($course_details_wish['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad save_this_course_later';
                                    if($m_id == $course_details['Course']['m_id']) {
                                            $class = 'c_added_learning_dsbl';
                                            $class1 = 'c_added_learning_dsbl';
                                    }
                                    
                                      //add free course to my learning tab
                                    if(isset($course_details['Course']['price']) && $course_details['Course']['price']==0.00){
										
										if($class1 != 'c_added_learning_dsbl'){
											echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_learning_grey.svg',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>$class1,'title'=>__('save_this_course_for_later'),'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
											echo '<span class="Link">'.$this->Html->link(__('add_to_my_learning'),array('controller'=>'Home','action'=>'add_to_my_learning',base64_encode(convert_uuencode($course_details['Course']['id']))),array('class'=>$class1,'title'=>__('save_this_course_for_later'),'escape'=>false)).'</span>';
										}else{                                              
											echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_learning_grey.svg',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>$class1,'title'=>__('this_course_is_already_in_my_learning'),'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
											echo '<span class="Link">'.$this->Html->link(__('add_to_my_learning'),'javascript:void(0);',array('class'=>$class1,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'title'=>__('this_course_is_already_in_my_learning'),'escape'=>false)).'</span>';
										}
										
									}else{
								
									
                                    if($class != 'c_added_learning_dsbl'){
                                    echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_wishlist.svg',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                    echo '<span class="Link">'.$this->Html->link(__('add_to_wish_list'),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)).'</span>';
                                    }else if($this->Session->read('LocTrain.id')!= $course_details['Course']['m_id'] && $add_edit_chk ==0) { ?>
                                       <a  data-toggle="modal"   data-target="#add_review" >  <?php echo $this->Html->image('front/'.$img_icon.'/icon_write.svg',array('width'=>'57','height'=>'57','alt'=>''));?></a>
                                         <span class="Link">   <a  data-toggle="modal"   data-target="#add_review" ><?php echo __("write_a_review");?></a></span> 
                                         <?php } else{ 
                                             
                                    echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_wishlist.svg',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                    echo '<span class="Link">'.$this->Html->link(__('add_to_wish_list'),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)).'</span>';
                                             
                                         }
                                          }
                            ?> 
               
            </div>
            <div class="SmallLinkBox">
                <a data-toggle="modal" data-target="#login-box-athor"><?php echo $this->Html->image('front/'.$img_icon.'/icon_user_person.svg',array('width'=>'57','height'=>'57','alt'=>''));?></a>
              
                <span class="Link"><a data-toggle="modal" data-target="#login-box-athor"><?php echo __('about_the_author_'); ?></a></span>
            </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="row">
       
    	<div class="col-lg-12 CourseVideo">
            <div class="Title"><?php echo $course_details['Course']['title']; ?></div>
            <div  id="render_video">
             
                
					
					
                    <?php
   $image = $course_details['Course']['image'];
      
$path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$image);
if(!empty($image) && $path)
                                        {
                                        echo $this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'medium/'.$image),array('width'=>'','height'=>'', 'class'=>'img-responsive'));
                                        }else if(!empty($less_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image))
								{ 
									
									echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'medium/'.$less_image),array('style'=>"border:1px solid #7f7f7f; width: 99%;", 'class'=>'img-responsive','width'=>'','height'=>''));
								}else if(!empty($lesson_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$lesson_image) )
								{ 
									
									echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'medium/'.$lesson_image),array('style'=>"border:1px solid #7f7f7f; width: 99%;", 'class'=>'img-responsive','width'=>'','height'=>''));
								}else{
                                        echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('style'=>"border:1px solid #7f7f7f; width: 99%;",'class'=>"img-responsive",'width'=>'','height'=>''));
                                        }


?> 

					<?php if(isset($extension) && $extension=='swf'){	
					
							$owner_id = base64_encode(convert_uuencode($course_details['Course']['m_id']));
							if($m_id == $course_details['Course']['m_id']) 
							{
					?>
								<div class="vwcrsply_btn" >
									<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
								</div>
					
					<?php	} else if($set =='unlink'){ 

						$ulogin=0;

					if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}if($ulogin==0 && $course_details['Course']['price']==0.00)
						{ ?>

<div class="vwcrsply_btn" >
									<a class="ply_btn_anchr free_course_play"><i class="fa fa-play"></i></a>
								</div>

<?php }else{

?>
								<div class="vwcrsply_btn" >
									<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
								</div>
					<?php	} } else 
							{
									if($course_details['Course']['price']!= 0.00)
									{
										if(!$subscribed && !empty($subscription))
										{
					?>
											<div class="vwcrsply_btn" >
												<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
											</div>
								
					<?php           	}else{ 	?>
											 <div class="vwcrsply_btn ">
												<a class="ply_btn_anchr free_course_play" href="javascript:void(0);" ><i class="fa fa-play"></i></a>
											</div>	
					<?php 
										}
									}else {

$ulogin=0;

					if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}if($ulogin==0 && $course_details['Course']['price']==0.00)
						{ ?>
<div class="vwcrsply_btn" >
												<a class="ply_btn_anchr free_course_play" ><i class="fa fa-play"></i></a>
											</div>

						<?php	}else{
					?>
											<div class="vwcrsply_btn" >
												<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
											</div>
					<?php	}	
									}
							}
						}
						elseif(isset($extension) && $extension=='zip'){	
					
							$owner_id = base64_encode(convert_uuencode($course_details['Course']['m_id']));
							if($m_id == $course_details['Course']['m_id']) 
							{
					?>
								<div class="vwcrsply_btn" >
									<a class="ply_btn_anchr " href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
									</div>
					
					<?php	} else if($set =='unlink'){ $ulogin=0;
if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}if($ulogin==0 && $course_details['Course']['price']==0.00)
						{
							
							?>
								<div class="vwcrsply_btn" >
									<a class="ply_btn_anchr free_course_play"><i class="fa fa-play"></i></a>
									</div>
					<?php	}else{ ?>
			<div class="vwcrsply_btn" >
									<a class="ply_btn_anchr " href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
									</div>

			<?php }


} else 
							{
									if($course_details['Course']['price']!= 0.00)
									{$show=0;
										if(!empty($subscription) && $subscription['lesson_id'] ==''){
											$show=1;
											}else{
												foreach($subscription as $p){
													if(isset($p['lesson_id']) && $p['lesson_id']==''){
														$show=1;
														}
													}
												
												}
										if(!$subscribed && !empty($subscription) && $show==1)
										{
					?>
											<div class="vwcrsply_btn" >
												<a class="ply_btn_anchr " href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
												</div>
								
					<?php           	}else{ 	?>
											 <div class="vwcrsply_btn ">
												
<a class="ply_btn_anchr free_course_play"  href="javascript:void(0);"><i class="fa fa-play"></i></a>
											</div>	
					<?php 
										}
									}else {$ulogin=0;
						if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}if($ulogin==0 && $course_details['Course']['price']==0.00)
						{
					?>
											<div class="vwcrsply_btn" >
												
<a class="ply_btn_anchr free_course_play" ><i class="fa fa-play"></i></a>
												</div>
					<?php		}else{ ?>
<div class="vwcrsply_btn" >
												<a class="ply_btn_anchr " href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'] ?>"><i class="fa fa-play"></i></a>
												</div>

						<?php }




									}
							}
						}else {
							if($m_id == $course_details['Course']['m_id']) 
							{?>
								
								<div class="vwcrsply_btn play_video_original">
								<a class="ply_btn_anchr" href="javascript:void(0);"><i class="fa fa-play"></i></a>
							</div>
							<?php }else if($course_details['Course']['price']!= 0.00)
									{//echo 'im here';print_r($subscribed);print_r($subscription);
										$show=0;
										if(!empty($subscription) && $subscription['lesson_id'] ==''){
											$show=1;
											}else{
												foreach($subscription as $p){
													if(isset($p['lesson_id']) && $p['lesson_id']==''){
														$show=1;
														}
													}
												
												}
										if(!$subscribed && !empty($subscription) && $show==1)
											{?>
										
										<div class="vwcrsply_btn play_video_original">
								<a class="ply_btn_anchr" href="javascript:void(0);"><i class="fa fa-play"></i></a>
							</div>
										
									<?php }else{ ?>
										<div class="vwcrsply_btn play_video_original" >
								<a class="ply_btn_anchr free_course_play"   href="javascript:void(0);"><i class="fa fa-play"></i></a>
							</div>
										<?php }
										
										
										
										}else{
							
							
						$ulogin=0;
						if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}
						if($ulogin==0 && $course_details['Course']['price']==0.00)
						{
					?>
						 <div class="vwcrsply_btn play_video_original" >
								<a class="ply_btn_anchr free_course_play"   href="javascript:void(0);"><i class="fa fa-play"></i></a>
							</div>
					<?php     }else{ ?>

 				<div class="vwcrsply_btn play_video_original">
								<a class="ply_btn_anchr" href="javascript:void(0);"><i class="fa fa-play"></i></a>
							</div>
					<?php } }
					
					} ?>
					
					
					
            
            
            
            
            </div>
             <div class="CourseRatingWrapper">            	            
                <div class="CourseRating CourseRating1">
                    <div class="RatingStar">
                        <form>
                         <?php 
						$login=0;
						if($this->Session->read('LocTrain.id'))
						{
						$login=1;
						}
						if($m_id != $course_details['Course']['m_id'] && $login==1) {?>
                       <?php if(!empty($subscription) || $subscribed || $course_details['Course']['price']==0.00) { ?>
                          
                       <input id="input-21a" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                       <?php    } else {?>
                        <input id="input-21a" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                        <?php } }else{
						?>
                     <input id="input-21a" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false"> 
                        <?php } ?>
                        </form>
                    </div>
                    <div class="Price"><?php 
if($course_details['Course']['price']=='0.00'){
          if($login==1){
           $class = !empty($course_details['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
if($class != 'c_added_learning_dsbl'){
echo __('free');
}else{
//echo __('purchased');
}
}else{
echo __('free');
}
}else{
	if($login==1){$show=0;
		if(!empty($subscription) && $subscription['lesson_id'] ==''){
		$show=1;
		}else{
		foreach($subscription as $p){
		if(isset($p['lesson_id']) && $p['lesson_id']==''){
		$show=1;
		}
		}

		}
	if(!empty($subscription) && $show==1)
	{ 
		}else{
			echo $this->Utility->externlizePrice($language,$locale,$course_details['Course']['price']);
			
			
			}}else{
				echo $this->Utility->externlizePrice($language,$locale,$course_details['Course']['price']);
				}

}

?></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-lg-12 CourseOffer">
        	<div class="Title CourseRatingWrapper"><?php echo __('this_course_offers');?>
                <div class="CourseRating CourseRating2">
                    <div class="RatingNum">


<!-- <span id="to_rating"><?php echo $rating;?></span><?php echo __('rating') ;?> -->
			<?php if(strpos($rating, ".") != false) {?>
		 <span id="to_rating"><?php printf(ngettext("%1\$.1f ".__('rating'), "%1\$.1f ".__("rating"), $rating),$rating);?></span> 
			<?php } else{ ?>
			<span id="to_rating"><?php printf(ngettext("%d ".__('rating'), "%d ".__("rating"), $rating),$rating);?></span> 
			<?php }?>
			</div>
                    <div class="RatingStar">
                        <form>
                        <?php 
						$login=0;
						if($this->Session->read('LocTrain.id'))
						{
						$login=1;
						}
						if($m_id != $course_details['Course']['m_id'] && $login==1) {?>
                       <?php if(!empty($subscription) || $subscribed || $course_details['Course']['price']==0.00) { ?>
                          
                       <input id="input-21a" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                       <?php    } else {
					   
					   
					   ?>
                        <input id="input-21a" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                        <?php } }else{
						
						
						?>
                        <input id="input-21a" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
                        <?php } ?>
                        </form>
                    </div>
                    <div class="Price" id="<?php echo $course_details['Course']['price']; ?>"><?php 


if($course_details['Course']['price']=='0.00'){
if($login==1){
           $class = !empty($course_details['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
if($class != 'c_added_learning_dsbl'){
echo __('free');
}else{
//echo __('purchased');
}
}else{
echo __('free');
}
}else{
if($login==1){
		$show=0;
		if(!empty($subscription) && $subscription['lesson_id'] ==''){
		$show=1;
		}else{
		foreach($subscription as $p){
		if(isset($p['lesson_id']) && $p['lesson_id']==''){
		$show=1;
		}
		}

		}
	if((!empty($subscription) && $show==1) || in_array($course_details['Course']['id'],$cookie_courses))
	{ 
		}else{
			echo $this->Utility->externlizePrice($language,$locale,$course_details['Course']['price']);
			
			
			}}else{
				echo $this->Utility->externlizePrice($language,$locale,$course_details['Course']['price']);
				}
}
 ?></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            
            <?php echo $this->element('frontElements/home/lesson_detail');?>
            
            
            
        </div>
    </div>                  
    
    
    </div>
    
    <div class="col-lg-5 col-md-5 col-sm-5 ProfileSection"> 
    	<div class="SmallLinkBoxWrapper SmallLinkBoxWrapper2 text-center">   
            <div class="SmallLinkBox">
                
              
                    <?php
                        $class ='';
                         $url='';
                        if($this->Session->read('LocTrain.id')=="" && $course_details['Course']['price']!=0.00)
                        {
                                $class = in_array($course_details['Course']['id'],$cookie_courses)?'vw_scn_link bg_ad_basket added_learning_sub':'vw_scn_link bg_ad add_to_cart';
                                $url = (in_array($course_details['Course']['id'],$cookie_courses) || ($this->Session->read('LocTrain.login')!=1))?'javascript:void(0);':array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($course_details['Course']['id'])));	
                                echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_add_to_cart.svg',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class,'escape'=>false));
                                echo '<span class="Link">'.$this->Html->link(__('add_to_basket'),$url,array('class'=>$class,'escape'=>false)).' </span>';
                        }
                        else if(!$subscribed)
                        { 
							$ulogin=0;
							$uclass='';
							if($this->Session->read('LocTrain.id'))
								{
								$ulogin=1;
								}if($ulogin==0 && $course_details['Course']['price']==0.00)
								{
								$uclass='free_course_play';

									}
							
                                if($m_id == $course_details['Course']['m_id']) {
                                        $class = 'vw_scn_link bg_ad_basket added_learning_sub';
                                        $url='javascript:void(0);';
                                }
                                else
                                {									
									
                                        $class = in_array($course_details['Course']['id'],$cookie_courses)?'vw_scn_link bg_ad_basket added_learning_sub':'vw_scn_link bg_ad add_to_cart';
                                        $url = in_array($course_details['Course']['id'],$cookie_courses)?'javascript:void(0);':array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($course_details['Course']['id'])));						
                                      /*  if( isset($set) && $set =='unlink')
                                        {
                                                $class = 'vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
                                                $url='javascript:void(0);';

                                        }									
                                        else */
                                         $lesson_pur=isset($subscription['0']['lesson_id'])?$subscription['0']['lesson_id']:$subscription['lesson_id'];
                                        if((!empty($subscription) &&  $lesson_pur =="") || trim($course_details['Course']['price'])==0.00)
                                        {
                                                $class = 'vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
                                                $url='javascript:void(0);';
                                        }else if(!empty($subscription)){
											
											foreach($subscription as $subscription){
												if($subscription['lesson_id']==""){echo 'im here';
												$class = 'vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
                                                $url='javascript:void(0);';}
												}
											}
                                }
                                if($url != 'javascript:void(0);'){
                                 echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_add_to_cart.svg',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class,'escape'=>false));    
                                echo '<span class="Link">'.$this->Html->link(__('add_to_basket'),$url,array('class'=>$class,'escape'=>false)).' </span>';
                                }else{
                                    
                              //  echo $this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>''));    
                              ?><?php // echo $this->Html->link(__('add_to_basket'),$url,array('class'=>$class)).' </span>';
                                 
                                if($video != '' && $extension!="swf" && $extension!="zip")
                                {
                                        if($m_id == $course_details['Course']['m_id']) 
                                        {

                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                        ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                       ?></span>
                                       
                                       <?php } 
                                       /*else if(isset($set) && $set =='unlink')
                                        {if($uclass){
                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                          ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php      echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                    ?></span>
                                    
                                    <?php  }else{
										
										 echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                              ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?></span>
										
										<?php } }	*/
                                        else
                                        {
                                                if($course_details['Course']['price']!= 0.00)
                                                {
                                                          if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        {	

                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>(!empty($subscription))?'watch':'preview','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php    echo $this->Html->link(__((!empty($subscription))?'watch_now_':'play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>(!empty($subscription))?'watch':'preview','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                       ?></span><?php }else{
														   echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                              ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?></span><?php
														   } }
                                                        else
                                                        {
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                              ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?></span><?php }
                                                }else
                                                {
                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                               ?> </span> <?php }
                                        }
                                }else 
                                { 
                                        if($video != '' && (isset($extension)  && $extension=="swf" ))
                                        { 
                                                if($m_id == $course_details['Course']['m_id']) 
                                                { 
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php      echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                              ?></span><?php  } else if($set =='unlink')
                                                {
													if($uclass){
	echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch free_course_play','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php 
														}else{
															  echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php 
															
															
															}
													
													
                                                         }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        { 
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        {
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                       ?> </span> <?php }
                                                                        else
                                                                        { 
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                        ?></span ><?php }
                                                                }else
                                                                { 
                                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                                      ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                             ?> </span><?php   }
                                                        }else{
															if($uclass){ 
																
																echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span>
																
																<?php }else{

                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span>
                                                        
                                                        <?php } }
                                                }
                                        }elseif($video != '' && (isset($extension) &&  $extension=="zip"))
                                        {
                                                if($m_id == $course_details['Course']['m_id']) 
                                                { 
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php      echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                              ?></span><?php  } else if($set =='unlink')
                                                {
													if($uclass)
													{
													echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php  	
													}else{
													
                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php   } }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        { 
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        {
                                                                               /* echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'https://s3.amazonaws.com/'.LESSON_VIDEOS_BUCKET.'/'.convert_uudecode(base64_decode($owner_id)).'/'.$video.'/index.html',array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); */
echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                       ?> </span> <?php }
                                                                        else
                                                                        { 
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                        ?></span ><?php }
                                                                }else
                                                                {
                                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                                      ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                             ?> </span><?php   }
                                                        }else{
															if($uclass){
																echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span><?php
																
															}else{
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),array('controller' => 'home', 'action' => 'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title']),array('class'=>'vw_scn_link bg_wch','rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span><?php } }
                                                }
                                        }else {

                                               /* echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                               ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

?></span><?php */				                if($m_id == $course_details['Course']['m_id']) 
                                                { 
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

												?></span><?php  } else if($set =='unlink')
                                                {
													if($uclass)
													{
													echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                       ?> <span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                            ?></span><?php  	
													}else{
													
                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

?></span><?php   } }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        { 
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        {
                                                                               echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

?></span><?php }
                                                                        else
                                                                        { 
                                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                             ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php   echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)); 
                                                                        ?></span ><?php }
                                                                }else
                                                                {
                                                                        echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                                      ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php  echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch vw_scn_link_disable bg_wch1 bg_wch_preview','rel'=>'swf','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                             ?> </span><?php   }
                                                        }else{
															if($uclass){
																echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                               ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('watch_now_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch '.$uclass,'rel'=>'watch','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                        ?></span><?php
																
															}else{
                                                                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                                           ?><span class="Link sct_lft" id="<?php echo base64_encode(convert_uuencode($lesson_ID)); ?>"><?php echo $this->Html->link(__('play_this_course'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch1 vw_scn_link_disable bg_wch_preview','data-val'=>'no','title'=>__('preview_is_not_available_for_interactive_videos'),'rel'=>'swf','id'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));

											?></span><?php } }  }

                                        }
                                        						

                                }?>
                                
                    <?php   }
                        }
			
                           $rel_val='';
                        if($video != '' && $extension!="swf" && $extension!="zip")
                                {
                                        if($m_id == $course_details['Course']['m_id']) 
                                        {
                                            $rel_val='watch';
                                        
                                        } else if($set =='unlink')
                                        { 
                                            $rel_val='watch';                                              
                                        }	
                                        else
                                        {
                                                if($course_details['Course']['price']!= 0.00)
                                                {
                                                        if(!$subscribed)
                                                        {	 $rel_val=(!empty($subscription))?'watch':'preview';

                                                             
                                                        }
                                                        else
                                                        { $rel_val='watch';
                                                              
                                                        }
                                                }else
                                                { $rel_val='watch';

                                                        
                                                }
                                        }
                                }else 
                                { 
                                        if($video != '' && (isset($extension)  && $extension=="swf" )||(isset($extension) &&  $extension=="zip"))
                                        { 
                                                if($m_id == $course_details['Course']['m_id']) 
                                                {  $rel_val='watch';
                                                         
                                                } else if($set =='unlink')
                                                { $rel_val='watch';
                                                  
                                                }else
                                                {
                                                        if($course_details['Course']['price']!= 0.00)
                                                        {  
                                                                if(!$subscribed)
                                                                { 
                                                                        if(!empty($subscription))
                                                                        { $rel_val='watch';
                                                                              
                                                                        }
                                                                        else
                                                                        {  $rel_val='swf';
                                                                            
                                                                        }
                                                                }else
                                                                {  $rel_val='watch';
                                                                   $action='javascript:void(0);';
                                                                      
                                                                }
                                                        }else{ $rel_val='watch';
                                                               $action='play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$course_details['Course']['title'];
                                                                
                                                        }
                                                }
                                        }else { $rel_val='swf';
                                                $action='javascript:void(0);';
                                                


                                        }							

                                }
                    ?>
                
                <input type="hidden" class="bg_wch1 bg_wch" rel="<?php echo $rel_val;?>" id="<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>">
                  
            </div>
            <div class="SmallLinkBox">
                      <?php 	
                                    $class = !empty($course_details_wish['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
                                     $class1 = !empty($course_details_wish['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad save_this_course_later';
                                    if($m_id == $course_details['Course']['m_id']) {
                                            $class = 'c_added_learning_dsbl';
                                            $class1 = 'c_added_learning_dsbl';
                                    }
                                   
                                //add free course to my learning tab
                                    if(isset($course_details['Course']['price']) && $course_details['Course']['price']==0.00){
					 $class_l = !empty($course_details['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
                                     $class1_l = !empty($course_details['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad save_this_course_later';
                                    if($m_id == $course_details['Course']['m_id']) {
                                            $class_l = 'c_added_learning_dsbl';
                                            $class1_l = 'c_added_learning_dsbl';
                                    }	
										if($class_l != 'c_added_learning_dsbl'){
											echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_learning_grey.svg',array('width'=>'57','height'=>'57','alt'=>'')),array('controller'=>'Home','action'=>'add_to_my_learning',base64_encode(convert_uuencode($course_details['Course']['id']))),array('class'=>$class1_l,'title'=>__('save_this_course_for_later'),'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
											echo '<span class="Link">'.$this->Html->link(__('add_to_my_learning'),array('controller'=>'Home','action'=>'add_to_my_learning',base64_encode(convert_uuencode($course_details['Course']['id']))),array('class'=>$class1_l,'title'=>__('save_this_course_for_later'),'escape'=>false)).'</span>';
										}else{                                              
											echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_learning_grey.svg',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>$class1_l,'title'=>__('this_course_is_already_in_my_learning'),'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
											echo '<span class="Link">'.$this->Html->link(__('add_to_my_learning'),'javascript:void(0);',array('class'=>$class1_l,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'title'=>__('this_course_is_already_in_my_learning'),'escape'=>false)).'</span>';
										}
										
									}else{
										
                                    if($class != 'c_added_learning_dsbl'){
                                    echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_wishlist.svg',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                    echo '<span class="Link">'.$this->Html->link(__('add_to_wish_list'),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)).'</span>';
                                    }else if($this->Session->read('LocTrain.id')!= $course_details['Course']['m_id'] && $add_edit_chk ==0) { ?>
                                        <a  data-toggle="modal"   data-target="#add_review" ><?php echo $this->Html->image('front/'.$img_icon.'/icon_write.svg',array('width'=>'57','height'=>'57','alt'=>''));?></a>
                                          <span class="Link">   <a  data-toggle="modal"   data-target="#add_review" ><?php echo __("write_a_review");?></a></span> 
                                         <?php } else{ 
                                             
                                             echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_wishlist.svg',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false));
                                    echo '<span class="Link">'.$this->Html->link(__('add_to_wish_list'),'javascript:void(0);',array('class'=>$class,'rel'=>base64_encode(convert_uuencode($course_details['Course']['id'])),'escape'=>false)).'</span>';
                                             
                                         }
									 }
                            ?> 
               
            </div>
            <div class="SmallLinkBox">
                <a data-toggle="modal" data-target="#login-box-athor"><?php echo $this->Html->image('front/'.$img_icon.'/icon_user_person.svg',array('width'=>'57','height'=>'57','alt'=>''));?></a>
                 <span class="Link"><a data-toggle="modal" data-target="#login-box-athor"><?php echo __('about_the_author_'); ?></a></span>
            </div>
            <div class="clearfix"></div>
        </div>        
    
    	<div class="row CourseDesc">
            <div class="col-lg-12">
                <div class="Title"><?php echo __('description');?></div>
                <p><?php echo $course_details['Course']['description']; ?><!--<a class="ViewMore" href="javascript:;">more</a>--></p>                                   
            </div>
        </div>
        
        <div class="row CourseKeywords">
            <div class="col-lg-12">
                <div class="Title"><?php echo __('keywords');?></div>
                <div class="SearchKeywords">
                     <?php foreach($course_details['CourseKeyword'] as $keyword) { 
 echo $this->Html->link($keyword['keyword'],'/Home/search?q='.urlencode($keyword['keyword']));
 } ?>
                	
                </div>                
            </div>
        </div>
        
        <div class="row CourseKeywords ReviewWrapper">
            <div class="col-lg-12">
                <div class="Title"><?php echo __('reviews');?></div>
                <?php   
        	if(!empty($course_details['CourseRemark'])) 
			{$i=1;
				foreach($course_details['CourseRemark'] as $remark)
				{ //print_r($remark);// if(!empty($remark['Member'])){
					$id = base64_encode(convert_uuencode($remark['id']));										
					
			 ?>
                            <div class="ReviewBox rd_rv_blk" id="show_<?php echo $i;?>">
                                <span class="rd_rv_data" rel="<?php echo $remark['id']; ?>">
                                    
                                                <p>
						<?php echo $this->Text->truncate($remark['comment'],200,array('ellipses'=>'...','exact'=>false)); ?>
						<span class="Author" style="color:#A5A5A5;"><?php echo $remark['anonymous'] == 0 ? $remark['Member']['given_name'].' '.$remark['Member']['family_name'] : __('anonymous_user'); ?></span>
                                                <?php if(isset($remark['Member']['id']) && $this->Session->read('LocTrain.id')==$remark['Member']['id']) { ?> 

                                                <a    data-toggle="modal" data-target="#edit_review" ><span class="show_review" style="color:#2D66B1;"> <?php echo __('edit'); ?> </span></a> <?php } ?>
                                                </p>
                                </span>
				
				<span class="rd_rv_data" style="display:none;" >
                    <p>
						<?php  echo nl2br($remark['comment']); ?>
						<span class="Author"  style="color:#A5A5A5;"><?php echo $remark['anonymous'] == 0 ? $remark['Member']['given_name'].' '.$remark['Member']['family_name'] : __('anonymous_user'); ?></span>
<?php if(isset($remark['Member']['id']) && $this->Session->read('LocTrain.id')==$remark['Member']['id']) { ?> 

<a style="text-decoration:none;"  data-toggle="modal" data-target="#edit_review" ><span class="show_review" style="color:#2D66B1;"> <?php echo __('edit'); ?> </span></a>
 <?php } ?>
					</p>
                </span>
				
                <span class="rd_rv_data_btm">
					
					<?php if(strlen($remark['comment']) > 200){ ?>
					
						<span class="rb_rv_snp">
							<?php echo $this->Html->link(__('show_more'),'javascript:void(0);',array('class'=>"read_more_review show_full_review",'rel'=>'show_full')); ?>
						</span>
					
					<?php } ?>					
                   	
                   	<?php if(!empty($this->Session->read('LocTrain.id')) && $remark['member_id'] != $this->Session->read('LocTrain.id')){ ?>
                   		<?php if(empty($remark['RemarkReport'])) { ?>
								<span class="rb_rv_snp2">
                                                                    
									<?php echo $this->Html->link(__('report'),'javascript:void(0)',array('class'=>'rpt_'. $remark['id'],"data-toggle"=>"modal" ,"data-target"=>"#report_".$remark['id'],'id'=>$id));?> 
								</span>
						<?php } else {?>
								<span class="rb_rv_snp2">
									<?php echo $this->Html->link(__('reported'),'javascript:void(0)',array('class'=>'rptd_'. $remark['id'],'id'=>$id));?> 
								</span>
						<?php } ?>
                    <?php } ?>
                    	
                </span>
                 <div class="modal fade" id="report_<?php echo $remark['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            	<h4 class="modal-title" id="myModalLabel4"><?php echo __('reviews'); ?></h4>
                            </div>
                            <div class="modal-body">
                                                              
                                    <div class="form-group">
                                        <textarea  class="form-control" id="text_report_<?php echo $remark['id'];?>" placeholder ="<?php echo __('give_a_reason_why_you_want_to_report_this_review'); ?>"></textarea></div>                                    
                                 	
                                <span class="Link"><a  class="ryt_flt report_now" onclick="reportNow('<?php echo $remark['id']; ?>','<?php echo $id;?>');" href="javascript:void(0)"><?php echo __('report_now'); ?>:</a></span>
                                    <span class="Link"><a  class="ryt_flt cancel_reporting"  onclick="cancel_the_reporting('<?php echo $remark['id']; ?>');" href="javascript:void(0)"><?php echo __('cancel'); ?></a></span>
				
                            </div>                            
                        </div>
                    </div>
                </div>
                	
            </div>  
           <?php
                               // }
                                $i++;
                                                }
		    }else{ ?>
		<div class="ReviewBox"><?php	echo __("no_reviews_received"); ?></div>
		<?php }
		?>
                <?php if(count($course_details['CourseRemark'])>3){?>
                <div class="Link"><a href="javascript:;" id="show_hide_reviews" rel="hide" data-info="<?php echo String::insert(__('show_numc_more_reviews'),array('numc' =>count($course_details['CourseRemark'])-3)); ?>" value="<?php echo count($course_details['CourseRemark'])-3; ?>"><?php if(count($course_details['CourseRemark'])-3 ==1){ ?><?php echo String::insert(__("show_numc_more_reviews"),array('numc' => '1')); ?>
<?php } 
else {
?>
<?php echo String::insert(__("show_numc_more_reviews"),array('numc' =>count($course_details['CourseRemark'])-3)); ?>
<?php } ?></a></div>              
                <?php } ?>
            </div>
        </div> 
    
        
    </div>
    
    
    
    </div>
  </div>
    
    
    
            <!-- Modal -->
                    <div class="modal fade" id="login-box-athor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel6"><?php echo __('about_the_author'); ?></h4>
                                </div>
                                <div class="modal-body">
                                   <div class="vwcntnt_rv_blk">
                                        <div class="row vwcntnt_rv_data">
                                                <div class="col-sm-6" id="p_anchor">
                                                   <?php
                                                   
                                                   
                                                           if($course_details['Member']['image'])
                                                           {
                                                                   $image = $course_details['Member']['image'];
                                                                 
                                                                   
                                                                    if($course_details['Member']['image'] && $this->Utility->getAWSDisplayUrl(MEMBERS_BUCKET,$course_details['Member']['image']))
                        {
                             echo $this->Html->image($this->Utility->getAWSImgUrl(MEMBERS_BUCKET,'thumb/'.$course_details['Member']['image']),array('class'=>'img-responsive')); 
                        }else{

			 echo $this->Html->image('front/about_author.png',array("class"=>"img-responsive",'alt'=>'Profile Picture'));
							}                   
                                                                   
															   }
                                                                   
                                                                else {
                                                                   echo $this->Html->image('front/about_author.png',array("class"=>"img-responsive",'alt'=>'Profile Picture'));
                                                           }
                                                   ?>
                                                                           </div>
                                             <div class="col-sm-6">
                                                   <?php 
                                                   $about_loc=$course_details['Member']['about_me'];
                                                     if(trim($about_loc) !=''){
                                                            $about_loc_new=json_decode($about_loc);
                                                            if(!empty($about_loc_new)){
                                                                 foreach($about_loc_new as $row){ ?>
                                                                   <div class="abt_cls_div">
                                                                        <span class="lft_loc"><?php
                                                                        foreach($s_lang as $k=>$v) { 
                                                                        if($k == $row->lang_locale ){
                                                                             echo $s_lang[$k];
                                                                        }
                                                                        }
                                                                        
                                                                         ?></span>
                                                                       <span class-="rht_loc">  <?php 
                                                                      
                                                                        $string = preg_replace('#(^|\s)([a-z]+://([^\s\w/]?[\w/])*)#is', '\\1<a  href="\\2">\\2</a>', $row->about_me);
                                                                        $string = preg_replace('#(^|\s)((www|ftp)\.([^\s\w/]?[\w/])*)#is', '\\1<a href="http://\\2">\\2</a>',$string);
                                                                        $string = preg_replace('#(^|\s)(([a-z0-9._%+-]+)@(([.-]?[a-z0-9])*))#is', '\\1<a href="mailto:\\2">\\2</a>',$string);
                                                                        echo nl2br($string); 
                                                                       ?></span>
                                                                    </div>
                                                           <?php      }
                                                                
                                                            }else{
                                                                $string = preg_replace('#(^|\s)([a-z]+://([^\s\w/]?[\w/])*)#is', '\\1<a  href="\\2">\\2</a>', $course_details['Member']['about_me']);
                                                                $string = preg_replace('#(^|\s)((www|ftp)\.([^\s\w/]?[\w/])*)#is', '\\1<a href="http://\\2">\\2</a>',$string);
                                                                $string = preg_replace('#(^|\s)(([a-z0-9._%+-]+)@(([.-]?[a-z0-9])*))#is', '\\1<a href="mailto:\\2">\\2</a>',$string);
                                                                echo nl2br($string); 
                                                            }
                                                         
                                                     }else{
                                                        $string = preg_replace('#(^|\s)([a-z]+://([^\s\w/]?[\w/])*)#is', '\\1<a  href="\\2">\\2</a>', $course_details['Member']['about_me']);
                                                        $string = preg_replace('#(^|\s)((www|ftp)\.([^\s\w/]?[\w/])*)#is', '\\1<a href="http://\\2">\\2</a>',$string);
                                                        $string = preg_replace('#(^|\s)(([a-z0-9._%+-]+)@(([.-]?[a-z0-9])*))#is', '\\1<a href="mailto:\\2">\\2</a>',$string);
                                                         echo nl2br($string); 
                                                        
                                                     }
                                                      ?>

                                            
                                            <div class="bc_holder">
                                                <span class="Link"><a id="book_author" href="<?php echo 'mailto:info@l10ntrain.com?subject=Request%20for%20more%20training%20from%20'.$course_details['Member']['final_name'];?>"> <?php echo __('book_this_author_for_more_training'); ?></a></span>
                                                    <span class="Link"><a href="<?php echo 'mailto:info@l10ntrain.com?subject=Contact%20request%20to%20'.$course_details['Member']['final_name'];?>"> <?php echo __('contact'); ?></a></span>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>



<!-- Modal -->
            <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?php echo __("sign_in_to_learn_and_teach");?></h4>
                                </div>
                                <div class="modal-body">
                                    <?php echo $this->Form->create('Login',array('role'=>'form', 'id'=>'memberLogin4','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'member_login')));?>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('email',array('type'=>'text','id'=>'emailId4','class'=>'form-control TextEmail','placeholder'=>__('e-mail_address'),'autofocus'=>'','div'=>false,'value'=>isset($cookieVal)?$cookieVal:'')); ?>
                                            <div id="err4_email" class="register_error  alert-danger"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('password',array('type'=>'password','id'=>'password4','class'=>'form-control TextPwd','placeholder'=>__('password'),'div'=>false)); ?>
                                            <div id="err4_password" class="register_error  alert-danger" > <?php if(isset($error['password'][0])) echo $error['password'][0];?> </div>
                                        </div>                                    
                                        <?php echo $this->Form->submit(__('sign_in'),array('class'=>'btn btn-primary btn-lg ButtonBlue','onclick'=>"return ajax_form('memberLogin4','Home/login_check_ajax','load_top')")); ?>
                                        <span class="Link">
                                        <?php echo $this->Html->link(__('i_havent_registered_yet'),array('controller'=>'Home','action'=>'register'),array('class'=>'memberLogin_a4'));?> 
                                    </span>
                                        
                                    <?php echo $this->Form->end(); ?>
                                    <span class="Link"><a href="javascript:;" onclick="return openForgotForm(4);"><?php echo __('i_forgot_my_password');?></a></span>   
                                    <p class="form-group openForgotForm" id="openForgotForm_4">
                                        <?php echo __("we_will_send_an_e_mail_to_you_with_a_password_reset_link_before_we_send_it_please_confirm_your_e_mail_address_above_is_correct");?>
                                    </p>
                                    <span class="Link show_link" id="show_link_4"><a href="javascript:;" onclick="return forgot_ajax_form('emailId',4,'myModal4');"><?php echo __('send_the_password_reset_e-mail_to_me');?></a></span>
                                </div>                            
                            </div>
                        </div>
                    </div>




</section>
<?php  echo $this->element('frontElements/home/edit_review');?>
<?php  echo $this->element('frontElements/home/edit_user_review');?>

<!--End  main container section -->
<?php 
   /* Rating Script [Start] */
        echo $this->Html->script('front/star-rating.js');
?>

<script type="text/javascript">

var id = "";
id = "<?php echo $mem_acc_no; ?>"; 

	var c_id = '<?php echo base64_encode(convert_uuencode($course_details['Course']['id'])); ?>';
	

	function can_review()
	{
		var request = $.ajax({
			  url: ajax_url+"Home/review_permit/",
			  type: "POST",
			  data: {course_id : c_id},
			  dataType: "html"
		});
		request.done(function(msg) {
			msg = msg.split('*&');
			if(msg[0]!='not')
			{
				$('a.add_rev').html(msg);
			}
			else if(msg.length>1)
			{
				$('#edit_remarks #edit_countText').val(msg[2]);
				$('#edit_remarks #crId').val(msg[1]);
			}
		});
	}

	$(document).ready(function(){
	
if($('.bg_wch1').attr('data-val')=='no'){
$('.play_video_original').hide();
$('.vwcrsply_btn ').hide();
}else if($('.ply_btn_anchr').attr('href')== 'javascript:void(0);' && $('.bg_wch1').attr('rel')=='swf' ){
$('.play_video_original').hide();
}
		$('#p_anchor a').addClass('anchor_link');
		$('#p_anchor a').attr("target","_blank");
		
		//can_review();
		var l_video='';
		var no_of_lessons=$('.noOfLessons').attr('id');
		
		if(no_of_lessons>2)
		{
			$('.tchng_cont_scrl').css('height','720px');
			$('.tchng_cont_scrl').css('overflow-y','scroll');
			$('.tchng_cont_scrl').css('position','absolute');
		}
		else
		{
			$('.vw_ls_btn').hide();
		}
	
		$('#show').on('click',function(){
			$('.container_btns').show('fast',function(){
				$("#show").text('<?php  echo __('hide_keywords'); ?>');
				
				$("#show").attr('id','hide');
			});
		});
		
		$("#hide").on('click',function(){
			$('.container_btns').hide('fast',function(){
				$("#hide").text('<?php echo __('show_keywords'); ?>');
			
				$("#hide").attr('id','show');
			});
		});
	
		$(".add_learning_sub").on('click',function(){
			var getId = $(this).attr('rel');
			$(".load_top").show();	
			var a =$(this);	
			
			req = $.ajax({
				url:ajax_url+'Home/add_to_learning/'+getId,
				dataType: 'json',
				success: function(resp){
					if(resp.status == 'ok'){
						$(".load_top").hide();	
						a.css({'color':'#A5A5A5'});	
						a.addClass('added_learning');	
						a.removeClass('add_learning');
                                                $('.added_learning').removeClass('add_learning_sub ');
                                                $('.added_learning').addClass('c_added_learning_dsbl'); window.location.reload();
					}
					else if(resp.status == 'error'){


$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
			if($('.Header1').is(':visible')){
				$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'redirect'
								}).appendTo('form#memberLogin');
$('<input>').attr({
								    type: 'hidden',
								    id: 'add_l',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'course_to_learning'
								}).appendTo('form#memberLogin');
				$('#myModal1').modal('show');
$('.memberLogin_a').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
			}else if($('.Header2').is(':visible')){
				$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'redirect'
								}).appendTo('form#memberLogin2');
$('.memberLogin_a2').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');$('<input>').attr({
								    type: 'hidden',
								    id: 'add_l',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'course_to_learning'
								}).appendTo('form#memberLogin');
				$('#myModal2').modal('show');
			}else if($('.Header3').is(':visible')){
			$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'redirect'
								}).appendTo('form#memberLogin3');
$('<input>').attr({
								    type: 'hidden',
								    id: 'add_l',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'course_to_learning'
								}).appendTo('form#memberLogin');
$('.memberLogin_a3').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
			     $('#myModal3').modal('show');
			}else
							{
							window.location.href = resp.url;}


					}
				}
			});
		});
		$('.save_this_course_later').click(function(){

		<?php if($this->Session->read('LocTrain.id')){?>
			//nothing to do
		<?php } else{?>
$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
			$(this).attr('href','#');
			if($('.Header1').is(':visible')){
				$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'redirect'
								}).appendTo('form#memberLogin');
$('<input>').attr({
								    type: 'hidden',
								    id: 'add_l',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'course_to_learning'
								}).appendTo('form#memberLogin');
$('.memberLogin_a').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
				$('#myModal1').modal('show');
			}else if($('.Header2').is(':visible')){
				$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'redirect'
								}).appendTo('form#memberLogin2');
$('<input>').attr({
								    type: 'hidden',
								    id: 'add_l',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'course_to_learning'
								}).appendTo('form#memberLogin');
$('.memberLogin_a2').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
				$('#myModal2').modal('show');
			}else if($('.Header3').is(':visible')){
			$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'redirect'
								}).appendTo('form#memberLogin3');
$('<input>').attr({
								    type: 'hidden',
								    id: 'add_l',
									value:"<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
								    name: 'course_to_learning'
								}).appendTo('form#memberLogin');
$('.memberLogin_a3').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
			     $('#myModal3').modal('show');
			}
		<?php } ?>

		});
		$(".bg_wch1, .play_video_original").on('click',function(){
				if($(this).hasClass('free_course_play')){
				return false;
				}
			var getId = $('.bg_wch1').attr('id');
			var a =$('.bg_wch1');
			
			var lesson_ID=a.next().attr('id');
			
			if(a.attr('rel')=='swf')
			{
				return false;
			}	
			$(".load_top").show();
			
			
			if(a.attr('rel')=='watch')
			{			
				// logic to find the lesson purchased or not;
				var rt = $.ajax({
							url: ajax_url+"Home/is_course_subscribed/",
							type: "POST",
							data: {cors_id : getId,less_id : lesson_ID},
							dataType: "html"
						});

						rt.done(function(msg){
						
							if(msg)
							{
								//alert(msg); return false;
									l_video='preview';
							}
							else
							{
								l_video ='watermark';
							
							}
							
							req = $.ajax({
								url:ajax_url+'Home/play_video/'+getId,
								dataType: 'json',
								success: function(resp){
								 
									if(resp.status == 'error'){
										window.location.href = resp.url;
									}
									else if(resp.status == 'true'){	
										if(l_video=='watermark')
										{	
											var request = $.ajax({
												url: ajax_url+"Home/course_watched/",
												type: "POST",
												data: {course_id : getId},
												dataType: "html"
											});
										}
						
										var id1 = "";
						
										id1 = "<?php  echo $mem_acc_no; ?>"; 
										if(resp.extension == "zip")
										{
											 $.ajax({
												url:ajax_url+'Home/ext_file/'+resp.video+'/'+resp.container,
												success:function(msg)
												{
													if($.trim(msg)!='')
													{
														//window.open(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/index.html','HTML5video','status=1');
									                    window.open('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'/index.html','HTML5video','status=1');
									                    return false; 
														/*$('#render_video').html('<iframe style="border:none;margin-left:36px;" width=425  height=322 src='+ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/index.html />');*/
													}
												}
											});
											
										} 	
										else if(resp.extension != "swf" && resp.extension != "zip")
										{				
											//*****   For water mark text****************										
										jwplayer("render_video").setup({
											
											
											file: "https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'_'+l_video+'.'+resp.extension,
											//file: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'.'+resp.extension,
									width: '100%',	height:'360',
									image: "https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'_thumb.png',
											//image: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'_thumb.png',
											plugins:{
													"<?php echo ROOT_FOLDER;?>jwplayer/view_course.js":
														{				
															texto: id1 
														}
													}
											});
											
											 jwplayer("render_video").play();
											setTimeout(function(){$('#render_video').css('border','1px solid #BEDAFF')},2000);
										}else if(resp.extension == "swf")
										{
											var strURLFull = window.document.location.toString();
											var intTemp = strURLFull.indexOf("?");
											var	strURLParams = "";
											if(intTemp != -1)
											{
												strURLParams = strURLFull.substring(intTemp + 1, strURLFull.length);
											}
												//	$("#render_video").css({'width':'75%'});
											//var so = new SWFObject(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'.'+resp.extension, "Captivate", "425px", "335px", "10", "#CCCCCC");
											var so = new SWFObject('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'.'+resp.extension, "Captivate", "100%", "338px", "10", "#CCCCCC");
											so.write("render_video");
										}			
//*****   For water mark text *****************						
										// Prevent form submission in IE AND CHROME
										//$("#render_video").css('margin-left','227px');	
										//jQuery(document).on('click', '.jwplayer button', function(event) { event.preventDefault(); });
										
										//a.removeClass('bg_wch');	
										
									}
									else if(resp.status == 'nolesson'){
										alert(resp.error);
									}
									$(".load_top").hide();	
								}
							});		
						});
					return false;	
			}
			else if(a.attr('rel')=='preview')
			{			
			l_video='preview';
		
			req = $.ajax({
				url:ajax_url+'Home/play_video/'+getId,
				dataType: 'json',
				success: function(resp){
					if(resp.status == 'error'){
						window.location.href = resp.url;
					}
					else if(resp.status == 'true'){	
						
						if(l_video=='watermark')
						{
								var request = $.ajax({
									url: ajax_url+"Home/course_watched/",
									type: "POST",
									data: {course_id : getId},
									dataType: "html"
								  });
						}if(resp.extension == "zip")
                        {
							 $.ajax({
								url:ajax_url+'Home/ext_file/'+resp.video,
								success:function(msg)
								{
									if($.trim(msg)!='')
									{
										//window.open(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/index.html','HTML5video','status=1');
										
										window.open('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'/index.html','HTML5video','status=1');
									    return false;
										/*$('#render_video').html('<iframe style="border:none;margin-left:36px;" width=425  height=322 src='+ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/'+msg+'/index.htm />');*/
									}
								}
						    });
							
						} 	
						else if(resp.extension != "swf" && resp.extension != "zip")
						{
							console.log(resp);
//*****   For water mark text *****************						
							jwplayer("render_video").setup({
								file: "https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'_'+l_video+'.'+resp.extension,
								//file: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'_'+l_video+'.'+resp.extension,	
								width: '100%',	
								height:'360',
								image: "https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'_thumb.png',				
								//image: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'_thumb.png',
								logo:
							{
							file:'<?php echo SITEURL;?>img/front/logo.svg',
							hide: true
							},
								plugins:{
										"<?php echo ROOT_FOLDER;?>jwplayer/view_course.js":
											{				
												texto: id 
											}
										}
								});
								jwplayer("render_video").play();
	                        //*****   For water mark text *****************	
							//Prevent form submission in IE AND CHROME
							//$("#render_video").css('margin-left','227px');	
							//jQuery(document).on('click', '.jwplayer button', function(event) { event.preventDefault(); });
							setTimeout(function(){$('#render_video').css('border','1px solid #BEDAFF')},2000);
						}else if(resp.extension == "swf") {
							var strURLFull = window.document.location.toString();
							var intTemp = strURLFull.indexOf("?");
							var	strURLParams = "";
							if(intTemp != -1)
							{
								strURLParams = strURLFull.substring(intTemp + 1, strURLFull.length);
							}
	                        $("#render_video").css({'width':'75%'});
							//var so = new SWFObject(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'.'+resp.extension, "Captivate", "425px", "335px", "10", "#CCCCCC");

							var so = new SWFObject('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'.'+resp.extension, "Captivate", "425px", "338px", "10", "#CCCCCC");
							so.write("render_video");
							}
//*****   For water mark text *****************	

						//Prevent form submission in IE AND CHROME
						//$("#render_video").css('margin-left','227px');	
						//jQuery(document).on('click', '.jwplayer button', function(event) { event.preventDefault(); });
						

							
					

					}
					else if(resp.status == 'nolesson'){
						alert(resp.error);
					}
					$(".load_top").hide();	
				}
			});
		}
	});
	
	$('.show_review').on("click",function(){
		can_review();
	});
	 
	 
	

	
	
});
</script>
<script>
    $(document).ready(function(){
       showHideReview();
        $('#show_hide_reviews').click(function(){
            var type=$(this).attr('rel');            
            if(type=='hide'){
               $(this).attr('rel','show'); 
               if($(this).attr('value') ==1){
                   $(this).html("<?php echo __('hide_review');?>");
               }else{
               $(this).html("<?php echo __('hide_reviews'); ?>");
           }
            }else{
               $(this).attr('rel','hide'); 
               if($(this).attr('value') ==1){
                   $(this).html("<?php echo String::insert(__('show_numc_more_reviews'),array('numc' => '1')); ?>");
               }else{
$(this).html($(this).attr('data-info'));
		//$(this).html("<?php echo String::insert(__('show_numc_more_reviews'),array('numc' => '')); ?>");
           //    $(this).html('<?php echo __('show_all').'&nbsp;';?>'+ $(this).attr('value')+' <?php echo '&nbsp;'.__('reviews');?>');
           }
            }
            showHideReview();
            
        });
        
    });
	
	
	function commentcheck(abc)
	{
	var id=abc.getAttribute("id");
	if($("#"+id).val()=='')
	{
	msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err").text(msg);
	}
	else
	{
	$("#err").text('');
	}
	}
    function showHideReview(){
        if($('.rd_rv_blk').length>3){
            $('.rd_rv_blk').eq(2).nextAll('.rd_rv_blk').toggle();
        }
    }
    jQuery(document).ready(function () {
        $(".rating").rating({
            starCaptions: function(val) {
                if (val < 3) {
                    return val;
                } else {
                    return 'high';
                }
            },
            starCaptionClasses: function(val) {
                if (val < 3) {
                    return 'label label-danger';
                } else {
                    return 'label label-success';
                }
            }
        });
  $('.rating').on('rating.change', function(event, value, caption) {
    var c_id = '<?php echo base64_encode(convert_uuencode($course_details['Course']['id'])); ?>';
    var to_rating=$('#to_rating').html();   
    var rating=value;  
            //   $(this).rating('update', 3);
    <?php if($m_id != $course_details['Course']['m_id']) {?>
                       <?php if(!empty($subscription) || $subscribed || $course_details['Course']['price']==0.00) {  ?>
       var request = $.ajax({
                           url: ajax_url+"Home/give_rating",
                           type: "POST",
                           data: {course_id : c_id, rating: rating},
                           dataType: "html",
			   success : function(e){
				//$("#to_rating").text(rating);
			   	if(e=='no_login' || e=="error"){
					$("#to_rating").text(rating+'<?php echo __("ratings"); ?>');
				}else{
			         $("#to_rating").text(e);
				}
				}	
                     });
                      <?php } }?>
       });

    });
    
    $(function(){
		
		$('.show_full_review').click(function(){
				
				var rel = $.trim($(this).attr('rel'));
				
				if(rel == 'show_full')
				{
					$(this).attr('rel','show_less');
					$(this).html('<?php echo __('show_less'); ?>');
					$(this).parents('.rd_rv_data_btm').prev().show().prev().hide();
					
				} else {
					
					$(this).attr('rel','show_full');
					$(this).html('<?php echo __('show_more'); ?>');
					$(this).parents('.rd_rv_data_btm').prev().hide().prev().show();
				}
				
		});
	
		$(".SmallLinkBoxWrapper2 .add_to_cart").click(function(event){
			
	if($(this).attr("href")=="javascript:void(0);")
	{	
			$.ajax({
					url:ajax_url+"Home/add_to_cart/<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
					type: 'post',
					success:function(){
						$.ajax({
							url:ajax_url+"Home/findcookiepurchase/",
							
							success:function(index){
								var array=$.parseJSON(index);
								var text1="<?php echo __('was_added_to _your_shopping basket._Go_to_the_shopping_basket_to_complete_your_order.')?>";
								$("#title-cart").html(array['courseslist'][0]['Course']['title']+" "+text1); 
								$(".counter_txt").html(array.countcart); 
								$("#register-cart").modal('show');
								$("#check_lesson_basket").addClass("added_learning_sub");
								$("#check_lesson_basket").unbind( "click" );
								$(".SmallLinkBoxWrapper2 .SmallLinkBox .add_to_cart").attr("id","class-check");
								$(".SmallLinkBoxWrapper2 .Link .add_to_cart").attr("id","class-check1");
								$("#class-check").removeClass("add_to_cart");
								$("#class-check").addClass("added_learning_sub");
								$("#class-check1").removeClass("add_to_cart");
								$("#class-check1").addClass("added_learning_sub");
								$("#class-check").unbind("click");
								$("#class-check1").unbind("click");
								
								$("input[type='checkbox'][name='add_lessons']").not(":disabled").attr("disabled",true);
								$("input[type='checkbox'][name='add_lessons']").attr("checked",true);
								}
							});
					
						}
					});
		}
			});
		
		  
		
	});
	function reportNow(id,remark_id){                
            var field_id='text_report_'+id;
            var reason=$('#'+field_id).val();
            if(reason != '')
            {
                if(remark_id != '')
                        {
                            $.post(ajax_url+'Home/report_remark',{remark_id: remark_id, reason: reason},function(resp){

                                if($.trim(resp) == 'reported')
                                {   $('#report_'+id).modal('hide');
                                       $('.rpt_'+id).attr('data-target','');
                                   // $('#report_'+id).remove();
                                    $('.rpt_'+id).html('<?php echo __('reported'); ?>');
                                    
                                }

                            });
                        }
            }else{
                alert('Please give a reason why you want to report this review');
                return false;
            }

        }
	function cancel_the_reporting(id)
	{
            $('#report_'+id).modal('hide');
           $('#text_report_'+id).val('');
            
        /*$('.cancel_reporting').click(function(){
			
			$reason_form = $(this).parent().parent('.rly_inner');
			$(this).parent().prev().val('');
			$reason_form.hide(500);
			$reason_form.prev().children('.rb_rv_snp2').show(500);
			
		});*/

	}
	$('document').ready(function(){

		$('.free_course_play').click(function(){ 

			<?php
$value=HTTP_ROOT.'home/view_course_details/'.base64_encode(convert_uuencode($course_details['Course']['id']));
if(!empty($course_details['CourseLesson'])){				
				foreach($course_details['CourseLesson'] as $lesson){
					if($lesson['video'] != '' && ($lesson['extension']=='zip' || $lesson['extension']=='swf') ){
						$video = $lesson['video'];
						$lesson_ID=$lesson['id'];
						$extension=$lesson['extension'];
						$lesson_image = $lesson['image'];
						$value=HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.base64_encode(convert_uuencode($course_details['Course']['m_id'])).'/'.$course_details['Course']['title']; 
						break;
					}

				}

			} 
				?>
			//$('.memberLogin_a').attr('href','');$('.memberLogin_a2').attr('href','');$('.memberLogin_a3').attr('href','');
			$('#myModal1').modal('hide');$('#myModal2').modal('hide');$('#myModal3').modal('hide');
							if($('.Header1').is(':visible')){
								$('#myModal1').modal('show');
								$('.memberLogin_a').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
								$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
								    value:"<?php echo $value;?>",
								    name: 'redirectLesson'
								}).appendTo('form#memberLogin');
							}else if($('.Header2').is(':visible')){
								$('.memberLogin_a2').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
								$('#myModal2').modal('show');
									$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
								    value:"<?php echo $value;?>",
								    name: 'redirectLesson'
								}).appendTo('form#memberLogin2');
							}else if($('.Header3').is(':visible')){
								$('.memberLogin_a3').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($course_details["Course"]["id"]));?>');
							     $('#myModal3').modal('show');
								$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
									value:"<?php echo $value;?>",
								    name: 'redirectLesson'
								}).appendTo('form#memberLogin');
							}else
							{
							}
			return false;
		});
		$('.free_course_play').each(function(i, obj) {
		  $(this).removeClass('bg_wch');
		$(this).removeClass('bg_wch1');
		});
		

	});
</script>

