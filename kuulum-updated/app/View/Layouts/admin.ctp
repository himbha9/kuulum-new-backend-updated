<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo HTTP_ROOT?>favicon.ico" />
        <title>Admin Panel</title>
        <noscript>    
            <style type="text/css">
                #page-content{display:none;}
                #sub-nav{display:none;}
                .noscript{display:block !important;}

            </style>  
        </noscript>   


        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

            <!--NEW THEME CSS FILES--->
            <!-- Page JS Plugins CSS -->



<?php
                echo $this->Html->css('admin_new_theme/bootstrap.min.css');
                echo $this->Html->css('admin_new_theme/oneui.css');
                echo $this->Html->css('admin_new_theme/dropdown/jquery.selectbox.css');
          
//   
//       
		echo $this->Html->css('admin/admin.css');
		echo $this->Html->css('admin/developer.css');
//	  	echo $this->Html->css('ui/ui.base.css');
	   	//echo $this->Html->css('themes/black_rose/ui.css','stylesheet',array('title'=>'style','media'=>'all'));
                //echo $this->Html->css('themes/black_rose/ui.css');	   
	   	echo $this->Html->css('ui/ui.datepicker.css');
		echo $this->Html->css('ui/ui.messages.css');
		echo $this->Html->css('admin/sample.css');
                
              
		
?>
            <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
            <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
            <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/slick/slick.min.css">
         <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/slick/slick-theme.min.css">
                            <!-- Page JS Plugins CSS -->
          <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/datatables/jquery.dataTables.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/select2/select2-bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/dropzonejs/dropzone.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT; ?>js/admin_new_theme/plugins/jquery-tags-input/jquery.tagsinput.min.css">

        <!-- <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script> -->
<?php  
	//echo $this->Html->script('jquery-1.8.1.min.js');
        echo $this->Html->script('admin_new_theme/core/jquery.min.js');
	echo $this->Html->script('admin/common.js');
        echo $this->Html->script('admin/cookie.js');
        echo $this->Html->script('admin/superfish.js');
    	echo $this->Html->script('admin/live_search.js');	
        echo $this->Html->script('admin/custom.js');
        echo $this->Html->script('admin/sidebar_position.js');
	
//	echo $this->Html->script('ui/ui.core.js');
//	echo $this->Html->script('ui/ui.widget.js');
//	echo $this->Html->script('ui/ui.mouse.js');
//	echo $this->Html->script('ui/ui.sortable.js');
//	echo $this->Html->script('ui/ui.draggable.js');
//	echo $this->Html->script('ui/ui.resizable.js');
//	echo $this->Html->script('ui/ui.position.js');
//	echo $this->Html->script('ui/ui.button.js');
//	echo $this->Html->script('ui/ui.dialog.js');
//	echo $this->Html->script('ui/ui.datepicker.js');	
//	echo $this->Html->script('ui/ui.tabs.js');
//	
	echo $this->Html->script('jquery.validate.js');
//	
//	//echo $this->Html->script('fckeditor/fckeditor.js');
//	echo $this->Html->script('admin/js/ckeditor/ckeditor');
?>
                                </head>


                                <body>
<?php
$strFunction = trim($this->request->params['action']);
?>
                                    <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
                                        <!-- Sidebar -->
                                        <nav id="sidebar">
                                            <!-- Sidebar Scroll Container -->
                                            <div id="sidebar-scroll">
                                                <!-- Sidebar Content -->
                                                <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                                                <div class="sidebar-content">
                                                    <!-- Side Header -->
                                                    <div class="side-header side-content2 bg-white-op">
                                                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                                                        <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                                                        <div class="btn-group pull-right">

                                                        </div>
                                                        <a class="h5 text-white" href="<?php echo HTTP_ROOT;?>admin/users/dashboard">
                                               <?php echo $this->HTML->image('powered_by_kuulum.png');?> 
                                                        </a>
                                                    </div>
                                                    <!-- END Side Header -->

                                                    <!-- Side Content -->
                                                    <div class="side-content">
                                                        <ul class="nav-main">
                                                            <li>
                                                                <a <?php if($strFunction=="admin_dashboard"){ echo "class='active'";} ?> href="<?php echo HTTP_ROOT;?>admin/users/dashboard"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Admin Dashboard</span></a>
                                                            </li>
                                                            <li class="nav-main-heading"><span class="sidebar-mini-hide">Users</span></li>
                                                            <li <?php if($strFunction=="admin_author_incomes" || $strFunction=="admin_author_income" || $strFunction=="admin_author_requests"){ echo "class='open'";} ?> >
                                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-book-open "></i><span class="sidebar-mini-hide">Authors</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <a href="<?php echo HTTP_ROOT;?>admin/users/author_incomes" <?php if($strFunction=="admin_author_income" || $strFunction=="admin_author_incomes"){ echo "class='active'";} ?>>Income</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_author_requests"){ echo "class='active'";} ?> href="<?php echo HTTP_ROOT;?>admin/users/author_requests">Requests</a>
                                                                    </li>

                                                                </ul>
                                                            </li>
                                                            <li <?php if($strFunction=="admin_members" || $strFunction=="admin_add_member" || $strFunction=="admin_edit_member"  || $strFunction=="admin_view_member_interests" || $strFunction=="admin_edit_member_interest" || $strFunction=="admin_add_member_interest" || $strFunction=="admin_edit_author_interest" ){ echo "class='open'";}?>>
                                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-user"></i><span class="sidebar-mini-hide">Members</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_members" || $strFunction=="admin_edit_member" || $strFunction=="admin_add_member"){ echo "class='active'";} ?> href="<?php echo HTTP_ROOT;?>admin/users/members">Members List</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_view_member_interests" || $strFunction=="admin_add_member_interest" || $strFunction=="admin_edit_member_interest" || $strFunction=="admin_edit_author_interest"  ){ echo "class='active'";} ?> href="#">Member Interests</a>
                                                                    </li>

                                                                </ul>
                                                            </li>

                                                            <li class="nav-main-heading"><span class="sidebar-mini-hide">Training</span></li>
                                                            <li>
                                                                <a <?php if($strFunction=="admin_courses" || $strFunction=="admin_edit_course" || $strFunction=="admin_view_course" || $strFunction=="admin_lessons" ){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/courses"><i class="si si-camera"></i><span class="sidebar-mini-hide">Course</span></a>
                                                            </li>
                                                            <li>
                                                                <a <?php if($strFunction=="admin_reports_on_reviews" ||$strFunction=="admin_view_reports_review"){ echo "class='active'";}?>  href="<?php echo HTTP_ROOT;?>admin/users/reports_on_reviews"><i class="si si-volume-1"></i><span class="sidebar-mini-hide">Conflict Resolution</span></a>
                                                            </li>
                                                            <li>
                                                                <a <?php if($strFunction=="admin_free_video" ||$strFunction=="admin_add_free_video" ||$strFunction=="admin_edit_free_video"){ echo "class='active'";}?>  href="<?php echo HTTP_ROOT;?>admin/users/free_video"><i class="si  si-film"></i><span class="sidebar-mini-hide">Third Party Videos</span></a>
                                                            </li>
                                                            <li <?php if($strFunction=="admin_discounts" || $strFunction=="admin_commission" || $strFunction=="admin_edit_discount" || $strFunction=="admin_add_discount" ||$strFunction=="admin_edit_commission" || $strFunction=="admin_add_commission"){ echo "class='open'";}?>>
                                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si s si-folder-alt"></i><span class="sidebar-mini-hide">Accounting</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_discounts" || $strFunction=="admin_add_discount" || $strFunction=="admin_edit_discount"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/discounts">Discounts</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_commission" ||$strFunction=="admin_edit_commission" || $strFunction=="admin_add_commission"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/commission">Commisions</a>
                                                                    </li>

                                                                </ul>
                                                            </li>


                                                            <li class="nav-main-heading"><span class="sidebar-mini-hide">Content</span></li>
                                                            <li>
                                                                <a <?php if($strFunction=="admin_cmspages" || $strFunction=="admin_edit_cms"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/cmspages"><i class="si si-paper-clip"></i><span class="sidebar-mini-hide">Pages</span></a>

                                                            </li>
                                                            <li>
                                                                <a <?php if($strFunction=="admin_news" || $strFunction=="admin_edit_news"){ echo "class='active'";}?>  href="<?php echo HTTP_ROOT;?>admin/users/news"><i class="si si-info"></i><span class="sidebar-mini-hide">News</span></a>

                                                            </li>
                                                            <li class="nav-main-heading"><span class="sidebar-mini-hide">Settings</span></li>
                                                            <li <?php if($strFunction=="admin_role_responsibility" ||$strFunction=="admin_add_role_responsibility" || $strFunction=="admin_edit_role_responsibility" || $strFunction=="admin_user_role" || $strFunction=="admin_time" || $strFunction=="admin_languages_status" || $strFunction=="admin_cancelled_users" || $strFunction=="admin_reasons" || $strFunction=="admin_add_reason" || $strFunction=="admin_edit_reason" || $strFunction=="admin_paypal" || $strFunction=="admin_banners" || $strFunction=="admin_edit_featured_course" || $strFunction=="admin_add_default_commission"){ echo "class='open'";}?>>
                                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-wrench"></i><span class="sidebar-mini-hide">Global</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_role_responsibility" || $strFunction=="admin_add_role_responsibility" ||$strFunction=="admin_edit_role_responsibility"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/role_responsibility">Roles</a>
                                                                    </li>
                                                             
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_time"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/time">Logout TIme</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_languages_status"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/languages_status">Languages</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_cancelled_users"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/cancelled_users">Cancelled Accounts</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_reasons" || $strFunction=="admin_edit_reason" || $strFunction=="admin_add_reason"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/reasons">Cancellation Reasons</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_paypal"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/paypal">Payement Gateways</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_banners"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/banners">Banners</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_edit_featured_course"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/edit_featured_course">Featured Courses</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_add_default_commission"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/add_default_commission">Default Commission Rate</a>
                                                                    </li>
                                                                    


                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <a  <?php if($strFunction=="admin_interest" || $strFunction=="admin_edit_interest" ||$strFunction=="admin_add_interest"){ echo "class='active'"; } ?> href="<?php echo HTTP_ROOT; ?>admin/users/interest"><i class="si si-key"></i><span class="sidebar-mini-hide">Keywords</span></a>

                                                            </li>
                                                            <li>
                                                                <a <?php if($strFunction=="admin_course_categories" ||$strFunction=="admin_add_category" ||$strFunction=="admin_edit_category" ||$strFunction=="admin_course_subcategory" ||$strFunction=="admin_edit_subcategory" ||$strFunction=="admin_add_subcategory"){ echo "class='active'";}?>  href="<?php echo HTTP_ROOT;?>admin/users/course_categories"><i class="si si-wrench"></i>Course Categories</a>


                                                            </li>
                                                            <li <?php if($strFunction=="admin_email_templates" || $strFunction=="admin_email"){ echo "class='open'";}?>>
                                                                <a  data-toggle="nav-submenu" href="#"><i class="si si-envelope-open"></i><span class="sidebar-mini-hide">Email</span></a>

                                        
<script>
$('#DataTables_Table_1').on( 'page.dt', function () {
    var table = $('#DataTables_Table_1').DataTable();
    var info = table.page.info();
    
    if(info.page>=1)
    {
                $("#check-all").prop('checked', false);
                $(".checkboxs").each(function (){
                    $(this).prop('checked', false);
                });
    }

    //$('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
} );
</script>
                                                                <ul>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_email_templates"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/email_templates">Templates</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_email"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/email">Default Addresses</a>
                                                                    </li>

                                                                </ul>
                                                            </li>
                                                            <li <?php if($strFunction=="admin_meta_tags" || $strFunction=="admin_meta_data_settings" ||$strFunction=="admin_edit_meta_data_settings" || $strFunction=="admin_edit_metatag"){ echo "class='open'";}?>>
                                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bar-chart"></i><span class="sidebar-mini-hide">SEO</span></a>
   <ul>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_meta_tags" || $strFunction=="admin_edit_metatag"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/meta_tags">Meta Tags</a>
                                                                    </li>
                                                                    <li>
                                                                        <a <?php if($strFunction=="admin_meta_data_settings"||$strFunction=="admin_edit_meta_data_settings"){ echo "class='active'";}?> href="<?php echo HTTP_ROOT;?>admin/users/meta_data_settings">Meta Data Settings</a>
                                                                    </li>

                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        </li>

                                                        </ul>
                                                    </div>
                                                    <!-- END Side Content -->
                                                </div>
                                                <!-- Sidebar Content -->
                                            </div>
                                            <!-- END Sidebar Scroll Container -->
                                        </nav>
                                        <!-- END Sidebar -->


                                        <!-- Header -->
                                        <header id="header-navbar" class="content-mini content-mini-full">
                                            <!-- Header Navigation Right -->
                                            <ul class="nav-header pull-right">
                                                <li>
                                                    <div class="btn-group">
                                                        <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                                  <?php echo $this->Html->image('admin_new_theme/avatars/avatar10.jpg',array('alt'=>'Avatar'));?>
                                                     <!-- <img src="assets/img/avatars/avatar10.jpg" alt="Avatar"> -->
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-right">

                                                            <li class="dropdown-header">Actions</li>
                                                            <li>
                                                                <a tabindex="-1" href="<?php echo HTTP_ROOT;?>admin/users/change_password">
                                                                    <i class="si si-settings pull-right"></i>Account Settings
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a tabindex="-1" href="<?php echo HTTP_ROOT;?>admin/users/logout">
                                                                    <i class="si si-logout pull-right"></i>Log out
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                            </ul>
                                            <!-- END Header Navigation Right -->

                                            <!-- Header Navigation Left -->
                                            <ul class="nav-header pull-left">
                                                <li class="hidden-md hidden-lg">
                                                    <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                                                    <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                                                        <i class="fa fa-navicon"></i>
                                                    </button>
                                                </li>
                                                <li class="hidden-xs hidden-sm">
                                                    <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                                                    <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                    </button>
                                                </li>
                                            </ul>
                                            <!-- END Header Navigation Left -->
                                        </header>
                                        <!-- END Header -->



                                        <!-- Main -->
                                        <div id="page_wrapper">
                                            <div class="loading"><div class='loadingText'>Loading...</div></div>

                                            <!--   <div id="page-header">
				<?php if ($this->Session->read('LocTrainAdmin.role_id')==8 )
				{
					 echo $this->element('adminElements/header');
				}
				if($this->Session->read('LocTrainAdmin.role_id')==4)
				{
					echo $this->element('adminElements/news_mng_header');
				}
				if($this->Session->read('LocTrainAdmin.role_id')==5)
				{
				 	echo $this->element('adminElements/membership_mngr_header');
				}
				if($this->Session->read('LocTrainAdmin.role_id')==6)
				{
				 	echo $this->element('adminElements/cours_manager_header');
				}
				if($this->Session->read('LocTrainAdmin.role_id')==7)
				{
				 	echo $this->element('adminElements/admin_header');
				}  
				?>
                                               </div> -->
                                            <div class="noscript"> 
                                                <div class="nonscript_changes">
                                                    <div class="nonscript_register">
                                                        <div class="course_pricing_mai"> 
                        <?php echo __('JavaScript is disabled on your browser.'); ?>
                                                        </div>
                                                        <div class="course_pricing_mai"> 
                            <?php echo __('Please enable your browser JavaScript or upgrade to a JavaScript-capable browser to enjoy Axiom Tutoring.');?>
                                                        </div>
                                                    </div>  
                                                    <div class="clear"></div>
                                                </div>  
                                            </div>  
                                            <div> <?php echo $content_for_layout ?> </div>

<!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-right">
                     <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">Powered by Kuulum</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">Localization Training LLC</a> &copy;  2015
                </div>
            </footer>
            <!-- END Footer -->

                                        </div> 

                                        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->


<?php

echo $this->Html->script('admin_new_theme/core/bootstrap.min.js');
echo $this->Html->script('admin_new_theme/core/jquery.slimscroll.min.js');
echo $this->Html->script('admin_new_theme/core/jquery.scrollLock.min.js');
echo $this->Html->script('admin_new_theme/core/jquery.appear.min.js');
echo $this->Html->script('admin_new_theme/core/jquery.countTo.min.js');
echo $this->Html->script('admin_new_theme/core/jquery.placeholder.min.js');
echo $this->Html->script('admin_new_theme/core/js.cookie.min.js');
echo $this->Html->script('admin_new_theme/app.js');
echo $this->Html->script('admin_new_theme/plugins/slick/slick.min.js');
echo $this->Html->script('admin_new_theme/plugins/chartjs/Chart.min.js');


echo $this->Html->script('admin_new_theme/plugins/datatables/jquery.dataTables.js');
echo $this->Html->script('admin_new_theme/pages/base_tables_datatables.js');

echo $this->Html->script('admin_new_theme/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');
echo $this->Html->script('admin_new_theme/plugins/bootstrap-datetimepicker/moment.min.js');
echo $this->Html->script('admin_new_theme/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');

echo $this->Html->script('admin_new_theme/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js');
echo $this->Html->script('admin_new_theme/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js');

echo $this->Html->script('admin_new_theme/plugins/select2/select2.full.min.js');
echo $this->Html->script('admin_new_theme/plugins/masked-inputs/jquery.maskedinput.min.js');
echo $this->Html->script('admin_new_theme/plugins/ion-rangeslider/js/ion.rangeSlider.min.js');
echo $this->Html->script('admin_new_theme/plugins/dropzonejs/dropzone.min.js');
echo $this->Html->script('admin_new_theme/plugins/jquery-tags-input/jquery.tagsinput.min.js');

echo $this->Html->script('admin_new_theme/plugins/summernote/summernote.min.js');
echo $this->Html->script('admin_new_theme/plugins/ckeditor/ckeditor.js');

echo $this->Html->script('admin_new_theme/dropdown/jquery.selectbox-0.2.js');
echo $this->Html->script('admin_new_theme/dropdown/dropdown.js');


?>
                                        <script>
                                            jQuery(function () {
                                                // Init page helpers (Slick Slider plugin)
                                                //   App.initHelpers('slick');
                                            });
                                        </script>

                                        <script>
                                            jQuery(function () {
                                                // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                                                App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
                                            });
                                        </script>
                                    </div>  
                                    <!-- Main -->


                                        
<script>
$('#DataTables_Table_1').on( 'page.dt', function () {
    var table = $('#DataTables_Table_1').DataTable();
    var info = table.page.info();
    
    if(info.page>=1)
    {
                $("#check-all").prop('checked', false);
                $(".checkboxs").each(function (){
                    $(this).prop('checked', false);
                });
    }

    //$('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
} );
</script>


                                </body>
                                </html>
