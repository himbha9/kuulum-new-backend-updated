
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>		
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');	
				
		echo $this->Html->css('front/main');
	
		//echo $this->Html->css('colortip-1.0-jquery');
		echo $this->Html->script('jquery-latest.pack.js');
		echo $this->Html->script('jquery-1.8.1.min.js');
		echo $this->Html->script('front/front.js');
		//echo $this->Html->script('jwplayer.js');
		
		//echo $this->Html->script('colortip-1.0-jquery');
	
		
	?>

    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none !important;
        }
      </style>
    <![endif]-->
</head>
<body>
	
	<div id="main">
    	<!--Div Header Start (AD)-->
        	
        <!--Div Header End (AD)-->

        <!--Wrapper div Start (AD)-->
       		<?php echo $this->fetch('content'); ?>
        <!--Wrapper div End (AD)-->
        
        <!--Div Footer Start (AD)-->
        	
        <!--Div Footer End (AD)--> 
    </div>

</body>
</html>
