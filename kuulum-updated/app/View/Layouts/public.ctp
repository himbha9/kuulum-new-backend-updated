<!DOCTYPE html>
<html lang="en" class=" js cssanimations csstransitions">
<head>
        <?php 
            //getting page meta info
            $locale = $this->Session->read('LocTrain.locale');  
            $current_page_url=$this->Html->url('', true);
            $pageinfo=$this->Utility-> pageinfo($current_page_url,$locale);
            //print_r($pageinfo);
            $meta_title=$pageinfo['meta_title'];
            $meta_desc=$pageinfo['meta_desc'];
            $meta_keywords=$pageinfo['meta_keywords'];
        ?>
	<?php echo $this->Html->charset(); ?>
	<title>		
                <?php echo $title_for_layout;  ?>
	</title>
	<?php        
        echo $this->Html->meta('icon');	
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'));	
        echo $this->Html->meta(array('name' => 'title', 'content' => $meta_title));	
        echo $this->Html->meta(array('name' => 'keywords', 'content' => $meta_keywords));
         echo $this->Html->meta(array('name' => 'description', 'content' => $meta_desc));	
        echo $this->Html->meta(array('name' => 'author', 'content' => ''));
        
        /* CSS Implementing Plugins */
        echo $this->Html->css('front/star-rating.css');
        echo $this->Html->css('front/font-awesome.min.css');
//echo $this->Html->css($this->Utility->getAWSImgUrl(STATIC_BUCKET,'css/front/star-rating.min.css'));
//echo $this->Html->css($this->Utility->getAWSImgUrl(STATIC_BUCKET,'css/front/font-awesome.min.css'));
        
        /* CSS Global Compulsory */
        echo $this->Html->css('front/bootstrap.min.css');
//echo $this->Html->css($this->Utility->getAWSImgUrl(STATIC_BUCKET,'css/front/bootstrap.min.css'));
         //echo $this->Html->css('front/style.css');
        if($this->Session->read('LocTrain.locale')=='ar')
        {
                echo $this->Html->css('front/'.$css);
//echo $this->Html->css($this->Utility->getAWSImgUrl(STATIC_BUCKET,'css/front/'.$css.'.min.css'));
        } else{
              echo $this->Html->css('front/'.$css);
//echo $this->Html->css($this->Utility->getAWSImgUrl(STATIC_BUCKET,'css/front/'.$css.'.min.css'));
        }

        echo $this->Html->script('front/jquery-1.11.1.min.js'); 
        echo $this->Html->script('front/bootstrap.min.js');
        echo $this->Html->script('front/front.min.js');
/*echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/front/jquery-1.11.1.min.js'));
echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/front/bootstrap.min.js'));
echo $this->Html->script($this->Utility->getAWSImgUrl(STATIC_BUCKET,'js/front/front.min.js'));*/
        
        ?>
  <!-- <script type="text/javascript" src="//use.typekit.net/ieg0rsl.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>-->
<script type="text/javascript" src="//use.typekit.net/kra2vip.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>
<body>

    <!--Div Header Start (AD)-->
        <?php echo $this->element('header'); ?>
    <!--Div Header End (AD)-->
    <!--Wrapper div Start (AD)-->
      <?php    
     $url=explode('/',Router::url($this->here, true));
    if(!in_array('catalogue_landing',$url)){
    ?>
    <div class="bodyContentWrapper container">
         <?php } ?>
       	<?php echo $this->fetch('content'); ?>
       <?php if(!in_array('catalogue_landing',$url)){
    ?>
    </div>
    <?php } ?>
    <!--Wrapper div End (AD)-->
    <!--Div Footer Start (AD)-->
        <?php echo $this->element('footer'); ?>
    <!--Div Footer End (AD)--> 

   


<!-- Rating Script [End] -->
<?php echo $this->element('sql_dump');?>
</body>
</html>
