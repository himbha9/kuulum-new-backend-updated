<?php $locale=$this->Session->read('LocTrain.locale');
		
		if($locale == 'ar') 
		{
		echo $this->Html->css('autoSuggest_arabic');
		}
		else
		{
		echo $this->Html->css('autoSuggest'); 
		}
             
	if(isset($css) && $css=='style')
	{
	$img_icon='icons';
	}
	else
	{
	$img_icon='icons_contrast';
	}
	?>
<?php echo $this->Html->script('jquery.autoSuggest');?>
<?php echo $this->Html->script('Scripts/jquery.msgBox.js');?>
<?php //echo $this->Html->css('front/Styles/msgBoxLight.css');?>
<?php echo $this->Form->input('sortby',array('type'=>'hidden','id'=>'sortInput','value'=>'1')); ?>
<!--Start Tab Drop section --> 

    <section class="myteaching_section">
    	<div class="container">                	
        
        	<div class="tabbable Tabbable1 margintop20">
                <?php if(!empty($course_list)) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs TabDropdownBox">
                        	<li class="dropdown pull-right tabdrop">
                                     <?php echo $this->Form->input('sort',array('type'=>'hidden','id'=>'sort','value'=>'0')); ?>
                                <a id="dropdown6" class="dropdown-toggle" data-toggle="dropdown" href="#">Select <b class="caret"></b></a>
                                <ul class="dropdown-menu TabDropdown" aria-labelledby="dropdown6">
                                    <li class="active"><a href="#tab1" data-toggle="tab"  onclick="showContent('tab1');"><?php echo __('show_courses_by_title');?></a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab"  onclick="showContent('tab2');"><?php echo __('show_courses_bydate');?></a></li>
                                    <li class="active"><a href="#tab3" data-toggle="tab" onclick="showContent('tab3');"><?php echo __('show_courses_by_average_rating');?></a></li>
                                    <li><a href="#tab4" data-toggle="tab" onclick="showContent('tab4');"><?php echo __('show_courses_by_number_of_reviews');?></a></li>
                                    <li><a  class="turn_on_off_chk" value="turn_filter_on" rel="turn_filter_on"><?php echo __('turn_filter_on');?></a></li>
                                </ul>
                            </li>
                        </ul>
                        
                        <ul class="nav nav-tabs">                                                                                   	
                            <li class="active" id="title_li"><a href="#tab1" data-toggle="tab" class="tabslist" id="s_t" onclick=""><?php echo __('title');?><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i></a></li>
                            <li class="" id="date_li"><a href="#tab2" data-toggle="tab" id="s_d" class="tabslist" onclick=""><?php echo __('date');?><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i></a></li>
                            <li class="" id="rating_li"><a href="#tab3" data-toggle="tab"  id="s_ar" class="tabslist" onclick=""><?php echo __('rating');?><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i></a></li>
                            <li id="reviews_li"><a href="#tab4" data-toggle="tab"  id="s_nor" class="tabslist" onclick=""><?php echo __('reviews');?><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i></a></li>
                            <li><a  class="turn_on_off_chk" value="turn_filter_on" rel="turn_filter_on"><?php echo __('turn_filter_on');?><i class="fa fa-caret-up"></i></a></li>
                        </ul>
                    </div>
                </div>
                
           
        <?php } ?>
        		<div class="row">
<?php if(!empty($course_list)) { ?>
	<div class="col-lg-7 col-md-7 col-sm-7 video_sec4_wrapper clearfix">                	    
                        <div class="tab-content">
                            
                                <div class="tab-pane active tchng_cont_container" id="<?php echo $last_id;?>">
                                <div class="tchng_cont_content" id="<?php echo $last_id;?>">
					<?php if(!empty($course_list)) { ?>
                                        <?php echo $this->element('frontElements/course/my_teaching_list'); ?>
                                    <?php }	?>
                                        
                                              
                                    <div class="mytch_rcd1">
						
                                     </div>	
                                </div>
                                    <div class="loading" >
                                <div class="loading_inner">
                                    <?php echo $this->Html->image('front/ajax-loader.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                                </div>
                                </div>
                                   
                                </div>                                            
                            </div>                        
                 	</div>                
                            
                    <div class="col-lg-5 col-md-5 col-sm-5"> 
                        <div class="row SmallLinkBoxWrapper SmallLinkBoxWrapper2 text-center myTeachingSmallLinks2">   
                            <div class="SmallLinkBox">
                                <?php echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_plus_add.svg',array('class'=>'','width'=>57,'height'=>57)),array('action'=>'course_create'),array('class'=>'scn_link scn_margn','escape'=>false)); ?> 
                                
                                <span class="Link"><?php echo $this->Html->link(__('new_course'),array('action'=>'course_create'),array('class'=>'scn_link scn_margn','escape'=>false)); ?> </span>
                            </div>
                            <div class="SmallLinkBox">
                               <?php echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_price.svg',array('url'=>array('controller'=>'Members','action'=>'view_trainer_income')),array('alt'=>'', "width"=>"57", "height"=>"57" )),array('controller'=>'Members','action'=>'view_trainer_income'),array('class'=>'small','escape'=>false)); ?>
                                <span class="Link"><?php echo $this->Html->link(__('my_income_'),array('controller'=>'Members','action'=>'view_trainer_income'),array('class'=>'small','escape'=>false)); ?> </span>
                            </div>
                   <!--         <div class="SmallLinkBox">
                                <?php echo $this->Html->image('front/'.$img_icon.'/icon_fire_esting.svg',array('class'=>'','width'=>57,'height'=>57)); ?> 
                                
                                <span class="Link"><a href="javascript:;">Settle review disputes</a></span>
                            </div> -->
                            <div class="clearfix"></div>
                        </div>        
                    	
                        
                        <div class="row">
                            <div class="col-lg-12 FilterBox2">
                                <div class="form-group">
                                    <label for=""><?php echo __('filter_by_keywords');?></label>
                                    <?php echo $this->Form->input('type',array('type'=>'hidden','id'=>'searchType','value'=>'0')); ?>
                                    <?php echo $this->Form->input('keywords',array('type'=>'text','id'=>'keywords','class'=>'regfld_txt_search form-control','label'=>false,'div'=>false)); ?>
                                    
                                </div>
                                <div class="form-group">
                                    <label for=""><?php echo __('filter_by_average_rating');?></label>
                                     <input id="input_avg_rate" value="0" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" >
                                </div>                                  
                                <div class="Link">
				<a id="reset_filter" class="scn_link1 filter_new"><?php echo __('reset_filter');?></a>
</div>              
                            </div>
                        </div>
                    </div>
<?php } else { ?>

<div class="add-firstcourse">
							<a href="<?php echo HTTP_ROOT.'members/course_create';?>"><?php echo $this->Html->image('front/'.$img_icon.'/icon_plus_add.svg',array('class'=>'','width'=>57,'height'=>57)); ?><br><span> <?php echo __("it_s_time_to_add_br_your_first_course");?> </span> 
							</a>

							
					</div>
<?php }?>
            		
                </div>
            </div>
        </div>
         
         <!-- Modal -->
                <div class="modal fade" id="retire_course" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            	<h4 class="modal-title" id="myModalLabel"><?php echo __('confirmation');?></h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="course_id_val" name="course_id_val">
                                <input type="hidden" id="course_id_attr" name="course_id_val">
                                <div class="form-group" id="retire_content" >
                                    <p><?php echo __('are_you_sure_you_want_to_retire_this_course');?></p>
                                </div>
                                <span class="Link"><a id="yes_retire_course"><?php echo __('yes');?></a>&nbsp;&nbsp;<a id="no_retire_course"><?php echo __('no');?></a></span>
                               
								
                            </div>                            
                        </div>
                    </div>
                </div>
    </section>
        <!--End  main container section -->
<?php 
   /* Rating Script [Start] */
        echo $this->Html->script('front/star-rating.js');
?>
    <!--End Tab Drop section --> 
  <script type="text/javascript">
       var req = '';
    var duration=0;
    var rating=0;
    
        jQuery('document').ready(function(){


$(".tabslist").click(function(){
	var array=[];
	$(".tabslist").each(function(index){
		array.push($(this).attr('id'));
		});
		
	var id=$(this).attr('id');
	var href=$(this).attr("href");
	href=href.split("#");
	if($(this).find('.fa:first').hasClass('optEnble')){
	$('#sortInput').val('0');
	$(this).find('.fa:first').removeClass('optEnble');
	$(this).find('.fa:first').addClass('optDisble');
	$(this).find('.fa:last').addClass('optEnble');
	$(this).find('.fa:last').removeClass('optDisble');
	}
	else{
	$('#sortInput').val('1');
	$(this).find('.fa:last').removeClass('optEnble');
	$(this).find('.fa:last').addClass('optDisble');
	$(this).find('.fa:first').removeClass('optDisble');
	$(this).find('.fa:first').addClass('optEnble');
	
	}
	
	
	array.splice($.inArray(id,array) ,1 );
	

$.each(array,function(index,item){
$('#'+item).find('.fa:first').removeClass('optDisble');
$('#'+item).find('.fa:first').removeClass('optEnble');
$('#'+item).find('.fa:first').addClass('optDisble');

$('#'+item).find('.fa:last').removeClass('optEnble');
$('#'+item).find('.fa:last').removeClass('optDisble');
$('#'+item).find('.fa:last').addClass('optEnble');

});

showContent(href[1]);
	
	});

$('body').on('click touchstart','.read_review',function(){//alert('f');
//$('.read_review').on('click',function() {

				$(".load_top").show();
				var course_id = $.trim($(this).attr('rel'));
				
				$(".login-popup").hide();
				var loginBox = $(this).attr('href');
				
				$.post(ajax_url+'Members/read_reviews', {course_id: course_id}, function(resp){
					
						$(".load_top").hide();
						
						$('.render_course').html(resp);
						
					$('#review-box').modal('show');				
					
				});
				
				//return false;
			});

			$( document ).ajaxStart(function() {
				$('.read_review').removeClass('read_review').addClass('pause_review');
			});
			
			$( document ).ajaxStop(function() {
				$('.pause_review').removeClass('pause_review').addClass('read_review');
			});
$(document).on('click touchstart','.video_sec',function(e){//alert('1'+$(e.target).attr('class'));
if( $(e.target).hasClass("Link a")){
//return false;
}else if( $(e.target).hasClass("retire_c")){
//return false;
}else if( $(e.target).hasClass("read_review")){
//return false;
}else if( $(e.target).hasClass("pause_review")){
//return false;
}else if( $(e.target).hasClass("vw_scn_link")){
//return false;
}else if( $(e.target).hasClass("rating-container")){

//return false;
}else if( $(e.target).hasClass("rating-stars")){

//return false;
}else{

window.location=ajax_url+'Members/course_edit/'+$(this).attr('rel');
}
});
/*
$('body').on('click touchstart','.video_sec2',function(e){//alert('2'+$(e.target).attr('class'));
if( $(e.target).hasClass("Link a")){
return false;
}else if( $(e.target).hasClass("retire_c")){
return false;
}else if( $(e.target).hasClass("read_review")){
return false;
}else if( $(e.target).hasClass("pause_review")){
return false;
}else if( $(e.target).hasClass("vw_scn_link")){
return false;
}else if( $(e.target).hasClass("rating-container")){

return false;
}else if( $(e.target).hasClass("rating-stars")){

return false;
}else{

window.location.href=ajax_url+'Members/course_edit/'+$(this).attr('rel');
}
});
*/
$(".loading_inner").hide();
var filter_vis='';
            if($('.FilterBox2').is(':visible')){
                filter_vis='FilterBox2';
            }else if($('.FilterBox1').is(':visible')){
                filter_vis='FilterBox1';
            }else{
                 filter_vis='FilterBox1';
            }
			$('.'+filter_vis).hide();
            var num_records=$('.video_sec_teaching').length ;
           <?php if(!empty($course_list)) { ?>
            showContent('tab1');
              <?php } ?>
            var data = {items: [<?php echo $interest_list;?>]};
            $("#keywords").autoSuggest(data.items, {selectedItemProp: "name", searchObjProps: "value",preFill: [],fieldName: "keyword"});
            
            $("#keywords_device").autoSuggest(data.items, {selectedItemProp: "name", searchObjProps: "value",preFill: [],fieldName: "keyword"});
            
			$('ul.as-selections').addClass('FilterKeyword');           
            
			$('#input_avg_rate').on('rating.change', function(event, value, caption) {
            
            var score=value;  
            store_rating(score);
            var getId = $(".tchng_cont_content").attr('id');
            var totalRecord = $(".tchng_cont_container").attr('id');
            var d_option = store_duration(0);
			var sort_order=$('#sortInput').val();
            if(d_option ==0)
            {
                    duration_option='all';
            }
            else
            {
                    duration_option=d_option;
            } 	
            var order = $("#sort").val();	 	
            if(req  && req.readystate  != '4')
            {
                    req.abort();
            }				
            $(".loading_inner").show();	
            req = $.ajax({
                    url:ajax_url+'Members/course/',
                    type: 'get',
                    data: {'type':'9','limit':10,'keyword':$(".as-values").val(),'order':order,'total_record':10,'score':score,'sort_order':sort_order},
                    success: function(resp){
                        
                            $("#searchType").val('1');	
                            $(".tchng_cont_content").html(resp);	
                            $(".loading_inner").hide();
                    }
            });
   
        });
        $('#input_avg_rate_device').on('rating.change', function(event, value, caption) {
            var score=value;  
            store_rating(score);
            var getId = $(".tchng_cont_content").attr('id');
            var totalRecord = $(".tchng_cont_container").attr('id');
            var d_option = store_duration(0);
	var sort_order=$('#sortInput').val();
            if(d_option ==0)
            {
                    duration_option='all';
            }
            else
            {
                    duration_option=d_option;
            } 	
            var order = $("#sort").val();	 	
            if(req  && req.readystate  != '4'){
                    req.abort();
            }				
            $(".loading_inner").show();	
            req = $.ajax({
                    url:ajax_url+'Members/course/',
                    type: 'get',
                    data: {'type':'9','limit':10,'keyword':$(".as-values").val(),'order':order,'total_record':10,'score':score,'sort_order':sort_order},
                    success: function(resp){				
                            $("#searchType_device").val('1');	
                            $(".tchng_cont_content").html(resp);	
                            $(".loading_inner").hide();
                    }
            });
   
        });
        $('.turn_on_off_chk').click(function(){
        var current_filter=$('.turn_on_off_chk').attr('rel');
 
    if(current_filter=='turn_filter_on' || current_filter=='')
    {

		var getId = $(".tchng_cont_content").attr('id');	
		var check_search_flag = $("#searchType").val();	
                var check_search_flag_device = $("#searchType_device").val();	
		var totalRecord = $(".tchng_cont_container").attr('id');
		var sort_order=$('#sortInput').val();
                var kewords='';
                $(".as-values").each(function(){
                    kewords+=$(this).val();
                });
		
		score=store_rating(null);
		var d_option = store_duration(0);
		if(d_option ==0)
		{
			duration_option='all';
		}
		else
		{
			duration_option=d_option;
		}
        $('.turn_on_off_chk').attr('rel','turn_filter_off');
        $('.turn_on_off_chk').text('<?php echo __('turn_filter_off');?>').append('<i class="fa fa-caret-up"></i>');
        

    $('.'+filter_vis).slideDown(500);
 $('.loading_inner').hide();
    
    }else{
		
 		var order='title';
		var getId = '10';	
		var check_search_flag =0;	
                var check_search_flag_device = 0;	
		var totalRecord = 10;
		var sort_order=1;
                var kewords='';
     		
        $('.turn_on_off_chk').attr('rel','turn_filter_on');
        $('.turn_on_off_chk').text('<?php echo __('turn_filter_on');?>').append('<i class="fa fa-caret-up"></i>');
             
  $('.'+filter_vis).hide();
 $('.loading_inner').hide();
    }
	      
    $(".loading_inner").show();	
		if(check_search_flag == '1' || check_search_flag_device=='1'){			
			req = $.ajax({
				url:ajax_url+'Members/course/',
				type: 'get',
				data: {'type':'9','limit':getId,'keyword':kewords,'order':order,'total_record':totalRecord ,'score': score,'sort_order':sort_order},
				success: function(resp){
                                    if(check_search_flag_device=='1'){
					$("#searchType_device").val('1');	
                                    }else{
                                        $("#searchType").val('1');
                                    }
					$(".tchng_cont_content").html(resp);	
					$(".loading_inner").hide();
				}
			});
		}
		else{
			req = $.ajax({
				url:ajax_url+'Members/course/',
				type: 'get',
				data: {'type':'9','limit':getId,'order':order,'total_record':totalRecord,'sort_order':sort_order},
				success: function(resp){						
					$(".tchng_cont_content").html(resp);	
					$(".loading_inner").hide();
				}
			});
		}





});
// RESET SEARCH FILTER	
	$(".scn_link").click(function(){			
		if(req  && req.readystate  != '4'){
			req.abort();
		}	
		$(".loading_inner").show();	
		
		window.location = ajax_url+'Members/course';
	});

//reset filter

$('.filter_new').click(function(){
	$('#input_avg_rate').val('0');
	$('#input_avg_rate_device').val('0');
	 $('#input_avg_rate').rating('reset');
	 $('#input_avg_rate_device').rating('reset');
	 store_rating(0);
	$('.as-values').val('');
	$(".as-values").trigger("reset");
	$('.as-selections li').each(function(){
		if(!$(this).hasClass('as-original')){
		$(this).remove();
		}
	});
		var order='title';
		var getId = '10';	
		var check_search_flag =0;	
                var check_search_flag_device = 0;	
		var totalRecord = 10;
		var sort_order=1;
                var kewords='';
			$(".loading_inner").show();
		req = $.ajax({
				url:ajax_url+'Members/course/',
				type: 'get',
				data: {'type':'9','limit':getId,'order':order,'total_record':totalRecord,'sort_order':sort_order},
				success: function(resp){						
					$(".tchng_cont_content").html(resp);	
					$(".loading_inner").hide();
				}
			});
});
        // FOCUS OUT SEARCH 	
	$(".regfld_txt_search").on('focusout',function(){
		var getId = $(".tchng_cont_content").attr('id');
		var totalRecord = $(".tchng_cont_container").attr('id');
		var d_option = store_duration(0);
                var keyword_search_parent_id=$(this).attr('id');
                var get_dynamic_id=keyword_search_parent_id.split('as-input-');
             
                var keyword_id=get_dynamic_id[1];
		if(d_option ==0)
		{
			duration_option='all';
		}
		else
		{
			duration_option=d_option;
		} 
		score=store_rating(null);
		var order = '';	 	
		if(req  && req.readystate  != '4'){
			req.abort();
		}				
		$(".loading_inner").show();	
		req = $.ajax({
			url:ajax_url+'Members/course/',
			type: 'get',
			data: {'type':'3','limit':getId,'keyword':$("#as-values-"+keyword_id).val(),'order':order,'total_record':totalRecord,'score':score},
			success: function(resp){
                            if($(this).hasClass('keywords_device')){
                                $("#searchType_device").val('1');	
                            }else{
                                $("#searchType").val('1');
                            }				
                            $(".tchng_cont_content").html(resp);	
                            $(".loading_inner").hide();
			}
		});
	});
 
        // retire the course
        $('body').on('click touchstart','.retire_c',function(){
           
           var course_id = $(this).attr('rel');           
           $('#course_id_val').val(course_id);
           $('#course_id_attr').val($(this).attr('value'));
           $('#retire_course').modal('show');
         return false;
        });
        $('#no_retire_course').click(function(){
            $('#course_id_val').val('');
            $('#course_id_attr').val('');
            $('#retire_course').modal('hide');
        });
        $('#yes_retire_course').click(function(){
           var course_id = $('#course_id_val').val();
           $(".loading_inner").show();
            req = $.ajax({
                    url:ajax_url+'Members/retire_course/',
                    type: 'get',
                    data: {'course_id': course_id},
                    success: function(resp){				
                                    $(".loading_inner").hide();
                                    $('#retire_course').modal('hide');
                                    if($.trim(resp)=='retired')
                                    {
                                            $('#tab_cnt_'+$('#course_id_attr').val()).hide();	
                                            
                                            num_records--;
                                              if(num_records==0)
                                              {
                                                  
						location.reload(); 
                                              }
                                    }
                                    else
                                    {
                                            alert(resp);
                                            return false;
                                    }

                            }
            });
        });
 $("#dropdown6").text('<?php echo __('show_courses_by_title');?>').append('<b class="caret"></b>' );
 $(".TabDropdown li a").click(function(){
          $("#dropdown6").text($(this).text()).append('<b class="caret"></b>' );
        });
    });
     function store_duration(d)
	{
		if(d==0)
		{
			return duration;
		}
		else
		{
			duration=d;
			
		}
	}
	 function store_rating(r)
	{
		
			if(r==null)
			{
				return rating;
			}
			else
			{
				rating=r;
				
			}
		
	}
 
        ///// sorting
        function showContent(tab){
			
	
	var current_filter=$('.turn_on_off_chk').attr('rel'); 
		
			<?php if(!empty($course_list)) { ?>
         
           var order = '';
		
		if(tab =='tab1'){
                   order='title' ;
                }else if(tab =='tab2'){
                    order='date' ;
                }else if(tab =='tab3'){
                    order='rating' ;
                }else if(tab =='tab4'){
                    order='reviews' ;
                }else{
                   order='title' ; 
                }

		
                $("#sort").val(order);
		var getId = $(".tchng_cont_content").attr('id');	
		var check_search_flag = $("#searchType").val();	
                var check_search_flag_device = $("#searchType_device").val();	
		var totalRecord = $(".tchng_cont_container").attr('id');
		var sort_order=$('#sortInput').val();//alert(sort_order);
                var kewords='';
                $(".as-values").each(function(){
                    kewords+=$(this).val();
                });
		
		score=store_rating(null);
		var d_option = store_duration(0);
		if(d_option ==0)
		{
			duration_option='all';
		}
		else
		{
			duration_option=d_option;
		}
		if(req  && req.readystate  != '4'){
			req.abort();
		}
		$(".loading_inner").show();	
		if(check_search_flag == '1' || check_search_flag_device=='1'){			
			req = $.ajax({
				url:ajax_url+'Members/course/',
				type: 'get',
				data: {'type':'4','limit':getId,'keyword':kewords,'order':order,'total_record':totalRecord ,'score': score,'sort_order':sort_order},
				success: function(resp){
                                    if(check_search_flag_device=='1'){
					$("#searchType_device").val('1');	
                                    }else{
                                        $("#searchType").val('1');
                                    }
					$(".tchng_cont_content").html(resp);	
					$(".loading_inner").hide();
				}
			});
		}
		else{
			req = $.ajax({
				url:ajax_url+'Members/course/',
				type: 'get',
				data: {'type':'5','limit':getId,'order':order,'total_record':totalRecord,'sort_order':sort_order},
				success: function(resp){						
					$(".tchng_cont_content").html(resp);	
					$(".loading_inner").hide();
				}
			});
		}
          <?php } ?> 
		

        }

      <?php if(!empty($course_list)) { ?>
	$(document).on('scroll',function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){		
			var getId = $(".tchng_cont_content").attr('id');
			var totalRecord = $(".tchng_cont_container").attr('id');	
			var searchType = $("#searchType").val();
		    var sort_order=$('#sortInput').val();
            var search_flag_device = $("#searchType_device").val();
            var kewords='';
                        $(".as-values").each(function(){
                            kewords+=$(this).val();
                        });
			if(req  && req.readystate  != '4'){
				req.abort();
			}			
			if(getId != 0){						
				$(".loading_inner").show();	
				if(searchType != 1){
					req = $.ajax({					
						url:ajax_url+'Members/course/',
						type: 'get',
						data: {'type':'1','limit':getId,'order':$("#sort").val(),'total_record':totalRecord,'sort_order':sort_order},
						success: function(resp){
							if($.trim(resp) == 'error'){
								window.location.href = ajax_url+'Home/index';
							}
							$(".tchng_cont_content").append(resp);	
							$(".loading_inner").hide();
						}
					});
				}else{				
					req = $.ajax({					
						url:ajax_url+'Members/course/',
						type: 'get',
						data: {'type':'2','limit':getId,'keyword':kewords,'order':$("#sort").val(),'sort_order':sort_order},
						success: function(resp){
							if($.trim(resp) == 'error'){
								window.location.href = ajax_url+'Home/index';
							}
							$(".tchng_cont_content").append(resp);	
							$(".loading_inner").hide();
						}
					});
				}
			}	
		}
	});	
<?php } ?>
        </script>
<style>
 .optDisble{
color:#f2f2f2;
}
.optEnble{
color:#0070c0;
}  
</style>
