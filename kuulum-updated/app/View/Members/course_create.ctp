<?php $s_lang = array('en'=> __('english_united_states'),'de'=> __('german_germany'),'ar'=> __('arabic'),'fr'=> __('french_france'),'it'=> __('italian_italy'),'ja'=> __('japanese'),'ko'=> __('korean'),'pt'=> __('portuguese_portugal'),'ru'=> __('russian_russian_federation'),'zh'=> __('chinese_china'),'es'=> __('spanish_spain')); ?>
<?php $locale=$this->Session->read('LocTrain.locale');
		if($locale == 'ar') 
		{
			echo $this->Html->css('autoSuggest_arabic');
		}
		else{
			echo $this->Html->css('autoSuggest'); 
		}
		
		foreach($language as $lang) {if($lang['Language']['locale']==$locale){  $current_loc= $lang['Language']['lang_code'];}}

if(isset($css) && $css=='style'){
$img_icon='icons';
}else{
$img_icon='icons_contrast';
}


		?>
<?php echo $this->Html->script('jquery.autoSuggest');?>
<?php echo $this->Html->script('front/jasny-bootstrap.js');?>
<?php echo $this->Html->css('front/jasny-bootstrap.css');?>
<?php echo $this->Html->script('front/dropdowns-enhancement.min.js');?>

<?php echo $this->Html->css('front/dropdowns-enhancement.css');?>

<?php echo $this->Html->script('front/jquery.price_format.2.0.js');?>

<style type="text/css">
.selectBoxArrow {
   cursor: pointer;
    float: left;
    margin-top: 10px;
    padding: 10px;
    position: absolute;
    right: 5px;
}
	.selectBoxInput{
		border:0px;
		padding-left:1px;
		height:16px;
		position:absolute;
		top:0px;
		left:0px; width: 100%;    padding: 0 12px;
		  background: none repeat scroll 0 0 #fff;
		  border: 1px solid #7f7f7f;
    border-radius: 4px;
      text-align: left;
    text-overflow: ellipsis;
    white-space: nowrap;
	}

	.selectBox{
		border:none;
		height:40px; width: 100% !important;
	
	}
	.selectBoxOptionContainer{
		 background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 4px;
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
    display: none;
    float: left;
		position:absolute;
	
		height:auto;
		
		left:0px;
		top:48px;
		visibility:hidden;
		overflow:auto;
		z-index:1000;
		 width: 100% !important;
	}
	.selectBoxIframe{
		position:absolute;
		background-color:#FFF;
		border:0px;
		z-index:999;
	}
	.selectBoxAnOption{
		 background: none repeat scroll 0 0 #fff ;
    color: #333 !important;
    display: block;
    font-family: "proxima-nova-n1","proxima-nova",sans-serif;
    font-size: 18px;
    font-style: normal;
    font-weight: 100;
    height: auto;
    line-height: 24px;
    margin: 0 !important;
    min-height: 24px;
    padding: 1px 15px !important;
    text-align: left;
    white-space: normal;
	}
	</style>
	<script type="text/javascript">
	
	// Path to arrow images
	var arrowImage = '<?php echo $this->webroot; ?>img/cart_arrow_down.png';	// Regular arrow
	var arrowImageOver ='<?php echo $this->webroot; ?>img/cart_arrow_down_black.png';	// Mouse over
	var arrowImageDown = '<?php echo $this->webroot; ?>img/cart_arrow_down.png';	// Mouse down

	
	var selectBoxIds = 0;
	var currentlyOpenedOptionBox = false;
	var editableSelect_activeArrow = false;
	

	
	function selectBox_switchImageUrl()
	{
		if(this.src.indexOf(arrowImage)>=0){
			this.src = this.src.replace(arrowImage,arrowImageOver);	
		}else{
			this.src = this.src.replace(arrowImageOver,arrowImage);
		}
		
		
	}
	
	function selectBox_showOptions()
	{
		if(editableSelect_activeArrow && editableSelect_activeArrow!=this){
			editableSelect_activeArrow.src = arrowImage;
			
		}
		editableSelect_activeArrow = this;
		
		var numId = this.id.replace(/[^\d]/g,'');
		var optionDiv = document.getElementById('selectBoxOptions' + numId);
		if(optionDiv.style.display=='block'){
			optionDiv.style.display='none';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='none';
			this.src = arrowImageOver;	
		}else{			
			optionDiv.style.display='block';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='block';
			this.src = arrowImageDown;	
			if(currentlyOpenedOptionBox && currentlyOpenedOptionBox!=optionDiv)currentlyOpenedOptionBox.style.display='none';	
			currentlyOpenedOptionBox= optionDiv;			
		}
	}
	
	function selectOptionValue()
	{
		var parentNode = this.parentNode.parentNode;
		var textInput = parentNode.getElementsByTagName('INPUT')[0];
		textInput.value = this.innerHTML.replace('&nbsp;',' ');	
		this.parentNode.style.display='none';	
		document.getElementById('arrowSelectBox' + parentNode.id.replace(/[^\d]/g,'')).src = arrowImageOver;
		
		if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + parentNode.id.replace(/[^\d]/g,'')).style.display='none';
		
	}
	var activeOption;
	function highlightSelectBoxOption()
	{
		if(this.style.backgroundColor=='#316AC5'){
			this.style.backgroundColor='';
			this.style.color='';
		}else{
			this.style.backgroundColor='#316AC5';
			this.style.color='#FFF';			
		}	
		
		if(activeOption){
			activeOption.style.backgroundColor='';
			activeOption.style.color='';			
		}
		activeOption = this;
		
	}
	
	function createEditableSelect(dest)
	{

		dest.className='selectBoxInput';		
		var div = document.createElement('DIV');
		div.style.styleFloat = 'left';
		div.style.width = dest.offsetWidth + 16 + 'px';
		div.style.position = 'relative';
		div.id = 'selectBox' + selectBoxIds;
		var parent = dest.parentNode;
		parent.insertBefore(div,dest);
		div.appendChild(dest);	
		div.className='selectBox';
		div.style.zIndex = 1 - selectBoxIds;

		var img = document.createElement('IMG');
		img.src = arrowImage;
		img.className = 'selectBoxArrow';
		
		img.onmouseover = selectBox_switchImageUrl;
		img.onmouseout = selectBox_switchImageUrl;
		img.onclick = selectBox_showOptions;
		img.id = 'arrowSelectBox' + selectBoxIds;

		div.appendChild(img);
		
		var optionDiv = document.createElement('DIV');
		optionDiv.id = 'selectBoxOptions' + selectBoxIds;
		optionDiv.className='selectBoxOptionContainer';
		optionDiv.style.width = div.offsetWidth-2 + 'px';
		div.appendChild(optionDiv);
		
		if(navigator.userAgent.indexOf('MSIE')>=0){
			var iframe = document.createElement('<IFRAME src="about:blank" frameborder=0>');
			iframe.style.width = optionDiv.style.width;
			iframe.style.height = optionDiv.offsetHeight + 'px';
			iframe.style.display='none';
			iframe.id = 'selectBoxIframe' + selectBoxIds;
			div.appendChild(iframe);
		}
		
		if(dest.getAttribute('selectBoxOptions')){
			var options = dest.getAttribute('selectBoxOptions').split(';');
			var optionsTotalHeight = 0;
			var optionArray = new Array();
			for(var no=0;no<options.length;no++){
				var anOption = document.createElement('DIV');
				anOption.innerHTML = options[no];
				anOption.className='selectBoxAnOption';
				anOption.onclick = selectOptionValue;
				anOption.style.width = optionDiv.style.width.replace('px','') - 2 + 'px'; 
				anOption.onmouseover = highlightSelectBoxOption;
				optionDiv.appendChild(anOption);	
				optionsTotalHeight = optionsTotalHeight + anOption.offsetHeight;
				optionArray.push(anOption);
			}
			if(optionsTotalHeight > optionDiv.offsetHeight){				
				for(var no=0;no<optionArray.length;no++){
					optionArray[no].style.width = optionDiv.style.width.replace('px','') - 22 + 'px'; 	
				}	
			}		
			optionDiv.style.display='none';
			optionDiv.style.visibility='visible';
		}
		
		selectBoxIds = selectBoxIds + 1;
	}	
	
	</script>


<script type="text/javascript">
    $(document).ready(function(){
		
		
        /////
             $(".fileinput").on("change.bs.fileinput", function(e){
                 var image_val=$('#image').val();
                 var get_extn=image_val.split('.');
                    if(get_extn[1]=='png' || get_extn[1] =='jpeg' || get_extn[1]=='jpg' || get_extn[1]=='gif'){
                        
                    }else{
                        $('.fileinput').fileinput('clear');
                        alert('Please select valid image file'); 
                        return false;
                    }
                });
            });
</script>
<script type="text/javascript">
    $(document).ready(function(){


$('#c_title').on('blur',function(){
		var msg = '';
		var finalNameVal = $('#c_title').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_title").text(msg);
		  }
		  else {
			$("#err_title").text('');
		  }
	});
$('#c_description').on('blur',function(){
		var msg = '';
		var finalNameVal = $('#c_description').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_description").text(msg);
		  }
		  else {
			$("#err_description").text('');
		  }
	});


        $('#yes_publish').on('click',function(){
	$('#actionType').val('3');
    $('#addCourse').submit();							
    $(".overlay").hide();
});
$('#no_publish').on('click',function(){
    $('#confirmPublicCourseModel').modal('hide');
});
       var lang_txt=  $("ul.dropdown_menu_locale li a:first").text();
       var lang_val=  $("ul.dropdown_menu_locale li a:first").attr('value');
        $("#dropdown4:first-child").text(lang_txt).append('<span class="caret"></span>' );
        $("#dropdown4:first-child").val(lang_val);       
        $('#c_language').val(lang_val);
        ///
      /*  var price_txt=  $("ul.dropdown-menu-price li a:first").text();
        var price_val=  $("ul.dropdown-menu-price li a:first").attr('value');
        $("#price_btn:first-child").text(price_txt).append('<span class="caret"></span>' );
        $("#price_btn:first-child").val(price_val);       
         $('#price').val(price_val);*/
        ///
        var category_txt=  $("ul.dropdown-menu-3 li a:first").text();
        var category_val=  $("ul.dropdown-menu-3 li a:first").attr('value');
         var category_id=  $("ul.dropdown-menu-3 li a:first").attr('id');
   $("#category_btn").text(category_txt).append('<b class="caret"></b>' );
        $("#category_btn").attr('value',category_val);      
        $('#category_id').val(category_val);
                var sub = $('.sl_sub_cat:first').parents('ul.dropdown-menu-2 li').children('.sub_cat');              
		var sub_cat_id = $.trim(sub.attr('id'));
		var sub_cat_txt = $.trim(sub.html());//alert(sub_cat_txt);
		
		var id = $.trim($('.sl_sub_cat:first').attr('id'));
		var txt = $.trim($('.sl_sub_cat:first').html());//alert(txt);
		
		$('#subcategory_id').val(sub_cat_id);
		$('#sl_subcategory_id').val(id);	
		
		$('#sub_cat_txt').val(sub_cat_txt+', '+txt); 
                
                
        $(".dropdown_menu_locale li a").click(function(){
          $('#c_language').val($(this).attr('value'));
          $("#dropdown4:first-child").text($(this).text()).append('<span class="caret"></span>' );
          $("#dropdown4:first-child").val($(this).attr('value'));
        });
        ///price
        
      /*  $('.dropdown-menu-price li a').click(function(){
              $('#price').val($(this).attr('value'));
          $("#price_btn:first-child").text($(this).text()).append('<span class="caret"></span>' );
          $("#price_btn:first-child").val($(this).attr('value'));
        });*/
        
        ///category
        
         $('.dropdown-menu-3 li a').click(function(){
            $('#category_id').val($(this).attr('value'));
          $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
          $("#category_btn").attr('value',$(this).attr('value'));
        });
$('.dropdown-menu-2 li a').click(function(){
            var cat_f=$(this).parents('ul.dropdown-menu-category li').children('.course_cat');              
            $('#category_id').val($.trim(cat_f.attr('id')));
          $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
          $("#category_btn").attr('value',$(this).attr('value'));
        });
   $('.dropdown-menu-category li a').click(function(){             
            $('#category_id').val($.trim($(this).attr('id')));
          $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
          $("#category_btn").attr('value',$(this).attr('value'));
        });
        var data = {items: [
					<?php echo $interest_list;?>				
				]};
	$("#keywords").autoSuggest(data.items, {selectedItemProp: "name", searchObjProps: "value",preFill: [],fieldName: "data[Course][keywords]"});
        $('ul.as-selections').addClass('FilterKeyword');
        
        $('.course_cat').click(function(){
		
		var id = $.trim($(this).attr('id'));
		
		$('#category_id').val(id);
		
		$('#subcategory_id').val('');
		$('#sl_subcategory_id').val('');			
		$('#sub_cat_txt').val('');
		
	});
	
	$('.sub_cat').click(function(){
		
		var id = $.trim($(this).attr('id'));
		var txt = $.trim($(this).html());
		
		$('#subcategory_id').val(id);		
		$('#sub_cat_txt').val(txt);
		
		//$('.selected2').html(txt);
		$('#sl_subcategory_id').val('');
		//$(".course_cat").css({'color':'#888'});

	});
	
$('.sl_sub_cat').click(function(){
		var cat_f=$(this).parents('ul.dropdown-menu-category li').children('.course_cat');              
		var sub = $(this).parents('ul.dropdown-menu-2 li').children('.sub_cat');              
		var sub_cat_id = $.trim(sub.attr('id'));
		var sub_cat_txt = $.trim(sub.html());//alert(sub_cat_txt);
		
		var id = $.trim($(this).attr('id'));
		var txt = $.trim($(this).html());//alert(txt);
               // alert($.trim(cat_f.attr('id')));
		$('#category_id').val($.trim(cat_f.attr('id')));
		$('#subcategory_id').val(sub_cat_id);
		$('#sl_subcategory_id').val(id);	
		
		$('#sub_cat_txt').val(sub_cat_txt+', '+txt);
		//$('.selected2').html(txt);
		//$(".course_cat").css({'color':'#888'});
		
	});
    });
 
</script>

<!-- main container section -->
<section>
  <div class="container InnerContent">
    <div class="row">

    <?php echo $this->Form->create('Course',array('id'=>'addCourse','name'=>'addCourse','url'=>array('controller'=>'Members','action'=>'course_create'),'enctype'=>'multipart/form-data')); ?>
    <div class="col-lg-6 col-md-6 col-sm-6">
	
    <div class="SmallLinkBoxWrapper SmallLinkBoxWrapper1 SmallLinksWrapper ">   
        <div class="SmallLinkBox">
                <button onclick="return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')" div="scn_link scn_margn" class="sc_btns disableAddCourseBut" id="saveCourse" type="submit"><?php echo $this->Html->image('front/'.$img_icon.'/icon_upload_cloud.svg',array('class'=>'','width'=>57,'height'=>57)); ?>  </button>
                <span class="Link">
                <?php echo $this->Form->button(__('save_changes_'),array('type'=>'submit','id'=>'saveCourse','escape'=>false,'class'=>'sc_btns disableAddCourseBut','div'=>'scn_link scn_margn','onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')")); ?>                       </span>

            </div>
            <div class="SmallLinkBox">
             
                <button onclick="return ajax_form('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')" div="scn_link scn_margn" class="sc_btns disableAddCourseBut" id="saveCourse" type="submit"><?php echo $this->Html->image('front/'.$img_icon.'/icon_plus_add.svg',array('class'=>'','width'=>57,'height'=>57)); ?></button>
                <span class="Link"><?php echo $this->Form->button(__('add_lesson_'),array('type'=>'submit','id'=>'addLesson','escape'=>false,'class'=>'sc_btns disAddLessonBut','div'=>'scn_link scn_margn','onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')")); ?></span>
            </div>
            <div class="SmallLinkBox">
              <button onclick="return ajax_form_publish('addCourse','Members/validate_course_ajax','load_top','disablePublishBut')" div="scn_link scn_margn" class="sc_btns disablePublishBut disablePublish" id="" type="submit"><?php echo $this->Html->image('front/'.$img_icon.'/icon_plane_paper.svg',array('class'=>'','width'=>57,'height'=>57)); ?></button>
               
            
                <span class="Link">
                    
                 <button onclick="return ajax_form_publish('addCourse','Members/validate_course_ajax','load_top','disablePublishBut')" div="scn_link scn_margn" class="sc_btns disablePublishBut disablePublish" id="" type="submit"><?php echo __('publish_course');?></button>
                       
            </div>
        <div class="clearfix"></div>
    </div>
        
 
  <div class="form-group">
    <label for="exampleInputEmail1">*<?php echo __('course_title'); ?></label>
    <?php echo $this->Form->input('title',array('type'=>'text','maxlength'=>250,'id'=>'c_title','class'=>'form-control','placeholder'=>__(''),'autofocus'=>'','label'=>false,'div'=>false)); ?> 
    <?php  echo $this->Form->input('actionType',array('type'=>'hidden','id'=>'actionType','value'=>''));?>
    <div id="err_title" class="register_error alert-danger"> <?php if(isset($error['title'][0])) echo $error['title'][0];?> </div> 
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">*<?php echo __('description'); ?></label>
    <?php echo $this->Form->input('description',array('type'=>'textarea','id'=>'c_description',"rows"=>"3",'class'=>'form-control','label'=>false,'div'=>false)); ?> 
    <div id="err_description" class="register_error alert-danger"> <?php if(isset($error['description'][0])) echo $error['description'][0];?> </div> 
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><?php echo __('language'); ?></label>
    	<?php echo $this->Form->input('primary_lang',array('type'=>'hidden','id'=>'c_language','class'=>'','label'=>false,'div'=>false,'value'=>'1')); ?>                    
    <div class="dropdown DropdownField" id="">
        <button data-toggle="dropdown" id="dropdown4" type="button" class="btn btn-default dropdown-toggle LanguageRegion">German(Germany)<span class="caret"></span></button>
        <ul aria-labelledby="dropdown4" role="menu" class="dropdown-menu dropdown_menu_locale">
              <?php foreach($language as $lang) { ?>
            <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $lang['Language']['locale'] ?>"><?php echo $s_lang[$lang['Language']['locale']];?> </a></li>
            <?php } ?>           
        </ul>
    </div>   
  </div>
    <div class="form-group">  	  
    <label for="exampleInputPassword1"><?php echo __('price'); ?></label>
      <?php $priceStr='';
    // $fmt = new NumberFormatter( $current_loc, NumberFormatter::DECIMAL_ALWAYS_SHOWN );

      foreach($course_prices as $key=>$val) { 
      $priceStr=$priceStr.$this->Utility->externlizePrice($language,$locale,$val).';';
		  } 
		  
		  ?> 
    <?php echo $this->Form->input('price',array('type'=>'text','id'=>'price','selectBoxOptions'=>$priceStr,'class'=>'form-control','value'=>$this->Utility->externlizePrice($language,$locale,'0.00'),'label'=>false,'div'=>false)); ?>                    
         <div id="err_price" class="register_error alert-danger"> <?php if(isset($error['price'][0])) echo $error['price'][0];?> </div> 
   
     
  </div>
  <!--<div class="form-group">
	   
	  
    <label for="exampleInputPassword1"><?php /* echo __('price'); ?></label>
    <?php echo $this->Form->input('price',array('type'=>'hidden','id'=>'price','class'=>'','value'=>'','label'=>false,'div'=>false)); ?>                    
    <div class="dropdown DropdownField" id="">        
        <button data-toggle="dropdown" id="price_btn" type="button" class="btn btn-default dropdown-toggle">$19.99<span class="caret"></span></button>
        <ul aria-labelledby="price_btn" role="menu" class="dropdown-menu dropdown-menu-price">
            <?php foreach($course_prices as $key=>$val) { ?>     
                <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $val;?>"><?php echo "$ ".$val;?> </a></li>            
            <?php } ?> 
        </ul>
         <div id="err_price" class="register_error alert-danger"> <?php if(isset($error['price'][0])) echo $error['price'][0];*/ ?> </div> 
    </div> 
     
  </div> -->
  <div class="form-group">
    <label for="exampleInputPassword1">*<?php echo __('category'); ?></label>
    <?php echo $this->Form->input('category_id',array('type'=>'hidden','id'=>'category_id','class'=>'','label'=>false,'div'=>false)); ?>                    
     <div class="navbar-nav MultiLevelDropdown">
        <div class="dropdown DropdownField"> <a class="DropdownCaption" data-toggle="dropdown" id="category_btn" value=""><?php echo __('dropdown');?> <b class="caret"></b></a>
          <ul class="dropdown-menu dropdown-menu-box dropdown-menu-category">
          <?php foreach($categories as $category) {?>
            
                
              <?php if(!empty($category['CourseSubcategory'])){ ?>
                <li class="dropdown-submenu">
                <a  tabindex="-1" data-toggle="dropdown" class="MultiLevelTitle course_cat" id="<?php echo $category['CourseCategory']['id']; ?>" value="<?php echo $category['CourseCategory']['id']; ?>"><?php echo $category['CourseCategory'][$locale]; ?></a>
                <ul class="dropdown-menu dropdown-menu-2">
                     <?php     foreach($category['CourseSubcategory'] as $subcat)
                { ?>                
                
                   <?php 
                        if(!empty($subcat['CourseSlSubcategory']))
                        {
                        ?>
                    <li class="dropdown-submenu">                    
                    <a  class="MultiLevelLink sub_cat" data-toggle="dropdown" id="<?php echo $subcat['id']; ?>" tabindex="-1" value="<?php echo $subcat['id']; ?>"><?php echo $subcat[$locale]; ?></a>
                    
                    <ul class="dropdown-menu dropdown-menu-3">
                          <?php
                                foreach($subcat['CourseSlSubcategory'] as $slcat)
                                {
                                ?>
                            <li>
                            <a  class="MultiSubLevelLink sl_sub_cat" id="<?php echo $slcat['id']; ?>" tabindex="-1"  value="<?php echo $slcat['id']; ?>"><?php echo $slcat[$locale]; ?></a>
                            </li>
                    
                        <?php } ?>
                    </ul>
                        <?php }else{ ?>
                       <li>                    
                    <a  class="MultiLevelLink sub_cat" id="<?php echo $subcat['id']; ?>" tabindex="-1" value="<?php echo $subcat['id']; ?>"><?php echo $subcat[$locale]; ?></a>
                         
                     <?php   } ?>
                </li>
                <?php } ?>
              </ul>
              <?php }else{?>
              <li>
                <a  tabindex="-1"  class="MultiLevelTitle course_cat" id="<?php echo $category['CourseCategory']['id']; ?>" value="<?php echo $category['CourseCategory']['id']; ?>"><?php echo $category['CourseCategory'][$locale]; ?></a>    
             <?php } ?>   
            </li>
            
            
          <?php } ?>
          </ul>
             <div class="reg_flds" style="display:none;">
        <?php echo $this->Form->input('sub_cat',array('type'=>'text','id'=>'sub_cat_txt','readonly'=>'readonly','class'=>'regfld_txt gradient','label'=>false,'div'=>false)); ?>
					
        <?php echo $this->Form->input('subcategory_id',array('type'=>'hidden','id'=>'subcategory_id','class'=>'','label'=>false,'div'=>false)); ?>
        <?php echo $this->Form->input('sl_subcategory_id',array('type'=>'hidden','id'=>'sl_subcategory_id','class'=>'','label'=>false,'div'=>false)); ?>
            </div>
        <div id="err_category_id" class="register_error alert-danger"> <?php if(isset($error['category_id'][0])) echo $error['category_id'][0];?> </div> 
     </div>   
     		</div>
       
  </div>
  
  <div class="form-group">
     <label><?php echo __('type_some_keywords_that_relevant_for_the_course_content');?></label>
    <?php echo $this->Form->input('keyword',array('type'=>'text','id'=>'keywords','class'=>'form-control','label'=>false,'div'=>false)); ?>
    <div id="err_keywords" class="register_error alert-danger"> <?php if(isset($error['keyword'][0])) echo $error['keyword'][0];?> </div>
  
  </div>
  <div class="form-group">
     <label><?php echo __('cover_image');?></label>
      <div class="fileinput fileinput-new coverImgWrapper" data-provides="fileinput" style="width: 100%; ">
            <div class="fileinput-new thumbnail" style="width: 652px; height: 329px;" style="display:none;">
            <?php echo $this->Html->image('front/cover_photo.png',array('alt'=>'','class'=>'img-responsive')); ?> 
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 652px; max-height: 329px;">
             <?php echo $this->Html->image('front/cover_photo.png',array('alt'=>'','class'=>'img-responsive')); ?> 
            </div>
            <div>
              <span class="btn btn-default btn-file"><span class="fileinput-new"><?php echo __("upload_photo");?></span><span class="fileinput-exists"><?php echo __("upload_photo");?></span>
                  <?php echo $this->Form->input('image',array('name'=>'image','type'=>'file','id'=>'image','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('picture',array('type'=>'hidden','id'=>'file_hidden','label'=>false,'div'=>false)); ?>
              </span>
              <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo __('remove_photo');?></a>
            </div>
          </div> 
  
  </div>
    
    </div>
    
    <div class="col-lg-6 col-md-6 col-sm-6 text-center ProfileSection"> 
    	<div class="SmallLinkBoxWrapper SmallLinkBoxWrapper2 smalllinkboxbg">   
             <div class="SmallLinkBox">
                <button onclick="return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')" div="scn_link scn_margn" class="sc_btns disableAddCourseBut" id="saveCourse" type="submit"><?php echo $this->Html->image('front/'.$img_icon.'/icon_upload_cloud.svg',array('class'=>'','width'=>57,'height'=>57)); ?>  </button>
                <span class="Link">
                <?php echo $this->Form->button(__('save_changes_'),array('type'=>'submit','id'=>'saveCourse','escape'=>false,'class'=>'sc_btns disableAddCourseBut','div'=>'scn_link scn_margn','onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')")); ?>                       </span>

            </div>
            <div class="SmallLinkBox">
             
                <button onclick="return ajax_form('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')" div="scn_link scn_margn" class="sc_btns disableAddCourseBut" id="saveCourse" type="submit"><?php echo $this->Html->image('front/'.$img_icon.'/icon_plus_add.svg',array('class'=>'','width'=>57,'height'=>57)); ?></button>
                <span class="Link"><?php echo $this->Form->button(__('add_lesson_'),array('type'=>'submit','id'=>'addLesson','escape'=>false,'class'=>'sc_btns disAddLessonBut','div'=>'scn_link scn_margn','onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')")); ?></span>
            </div>
            <div class="SmallLinkBox">

                 <button onclick="return ajax_form_publish('addCourse','Members/validate_course_ajax','load_top','disablePublishBut')" div="scn_link scn_margn" class="sc_btns disablePublishBut disablePublish" id="" type="submit"><?php echo $this->Html->image('front/'.$img_icon.'/icon_plane_paper.svg',array('class'=>'','width'=>57,'height'=>57)); ?></button> 
                
                
                <span class="Link">
                    
                      <button onclick="return ajax_form_publish('addCourse','Members/validate_course_ajax','load_top','disablePublishBut')" div="scn_link scn_margn" class="sc_btns disablePublishBut disablePublish" id="" type="submit"><?php echo __('publish_course');?></button> 
                
</span>
            </div>
            <div class="clearfix"></div>
        </div>
    
    <?php echo $this->Form->end(); ?>
    
    <div id="lesson" class="col-lg-12 col-md-12 col-sm-12">
    
    	<h2><?php echo __('lessons');?></h2>
    <div class="clearfix"></div>
    			
                <div class="leasson1">
                    <?php echo __('there_are_no_lessons_for_this_course'); ?> 
<!--                <div class="col-lg-6 col-md-6 col-sm-6 leasson_img">
               
                </div>
                
                <div class="right_lesson col-lg-6 col-md-6 col-sm-6 right_text">
                <h5><?php echo __('there_are_no_lessons_for_this_course'); ?> </h5>
              <div class="price_sec">
                <p style="float:left">00:01:00</p> 

                <p style="float:right">$4.99</p>        
                </div>
                
                </div>                -->
                
                </div>
                
                  
                
                 
                          
                           
                
     
    
    </div>
    
    <!--<div class="profile_pics profile_pics2">    
        <div class="Title">Profile photo</div>    
        <img src="images/img_placeholder.svg" width="165" height="162" alt=""/>    
        <div class="profile_upload">
        	<span class="Link"><a href="#">Upload photo</a></span>
        	<span class="Link"><a href="#">Remove photo</a></span>
        </div> 
        <form role="form">
            <div class="checkbox text-left">
                <label><input type="checkbox"><p>Tick this box if you are happy for us to tell you about relevant news, and new courses  that may interest you.</p></label>
            </div>
        </form>   
    </div>-->
    
        
    </div>
    
    
    
    </div>
  </div>
<div class="modal fade" id="confirmPublicCourseModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo __('publish_course_');?></h4>
                    </div>
                    <div class="modal-body"> 
                        <p class="avlListMsg Title"><?php echo __('your_course_will_be_visible_in_the_catalogue_and_search_results_do_you_want_to_continue'); ?></p>
                         
                       
                            <input type="hidden" id="course_id" value="">
                            <span class="Link"><a id="yes_publish"><?php echo __('yes'); ?></a> &nbsp;&nbsp;<a id="no_publish"><?php echo __('no'); ?></a></span>
                            
                       
                    </div>                            
                </div>
            </div>
        </div>
</section>
<script type="text/javascript">
createEditableSelect(document.forms['addCourse'].price);
function str_replace(replace, by, str) {
    for (var i=0; i<replace.length; i++) {
        str = str.replace(new RegExp(replace[i], "g"), by[i]);
    }
  
    return str;
}
function to_numbers (str,limit)
			{var is_number = /[0-9]/;
				var formatted = '';
				for (var i=0;i<(str.length);i++)
				{
					char_ = str.charAt(i);
					if (formatted.length==0 && char_==0) char_ = false;

					if (char_ && char_.match(is_number))
					{
						/*if (limit)
						{
							if (formatted.length < limit) formatted = formatted+char_;
						}
						else
						{
							formatted = formatted+char_;
						}*/
						formatted = formatted+char_;
					}
				}

				return formatted;
			}
			function fill_with_zeroes (str)
			{var centsLimit=2;
			 if(str.length >1){
				while (str.length<(centsLimit+2)) str = str+'0';
				}else{
					while (str.length<(centsLimit+1)) str = str+'0';
					}
								return str;
			}
			function fill_with_decimal(str){
				var centsLimit=2;
				while (str.length<(centsLimit)) str = str+'0';
				return str;
				
				}
var format = function(num,locale,pre,suf,cenSep,thouSep){	var centsLimit=2;	
	if(pre == ''){
		var str = num.toString().replace(suf, ""), parts = false, output = [], i = 1, formatted = null;
		}else{
			var str = num.toString().replace(pre, ""), parts = false, output = [], i = 1, formatted = null;
		}
		if(str.indexOf(cenSep) > 0){
			partsw = str.split(cenSep);
			str=((to_numbers(partsw[0]))?to_numbers(partsw[0]):0)+cenSep+fill_with_decimal(to_numbers(partsw[1]));
		}else{
		/*str=fill_with_zeroes(to_numbers(str,6));		
		var centsVal = str.substr(str.length-centsLimit,centsLimit);
		var integerVal = str.substr(0,str.length-centsLimit);
		str =integerVal+cenSep+centsVal;*/
		partsw=(to_numbers(str,6))?to_numbers(str,6):0;
		str=partsw+cenSep+'00';
		}	
	if(str.indexOf(cenSep) > 0) {
		parts = str.split(cenSep);
		str = parts[0];
	}
	str = str.split("").reverse();
	for(var j = 0, len = str.length; j < len; j++) {
		if(str[j] != thouSep) {
			output.push(str[j]);
			if(i%3 == 0 && j < (len - 1)) {
				output.push(thouSep);
			}
			i++;
		}
	}
	formatted = output.reverse().join("");
	if(pre == ''){
		return(formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : "")+' '+suf);
	}else {
	return(pre + formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : ""));
	}
};

			
var formatCheck=function(price){
		var replace = new Array("[\$]", "[\€]", "[¤]",'\,');
		var by = new Array("", "", "","");
		var new_price=$.trim(str_replace(replace, by, price));
		var msg = '';				
		if(new_price.length<3 || new_price.length>7){
			 msg = '<?php echo __("enter_a_number_between_000_and_999999");?>';
		  	  $("#err_price").text(msg);
			}else {
			$("#err_price").text('');
		  }
};

$('document').ready(function(){
	  var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
	specialKeys.push(37); //Home
	specialKeys.push(39); //Home
$("#price").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode;
                var ret = ((keyCode >= 48 && keyCode <= 57) || keyCode==190 || keyCode == 44 ||( specialKeys.indexOf(keyCode) != -1 && e.charCode != 37 && e.charCode != 39));                
                return ret;
		
            });
	
	$('#price').blur(function(){
		
		var locale='<?php echo $this->Session->read('LocTrain.locale');?>';
	
		if(locale=='en'){
			 $(this).val(format($(this).val(),locale,'$','','.',','));
					
		}else if (locale=='de'){
			$(this).val(format($(this).val(),locale,'',' $',',','.'));	
			
		}else if (locale=="es"){
			$(this).val(format($(this).val(),locale,'$','',',','.'));
			
		}else if (locale=='zh'){	
			 $(this).val(format($(this).val(),locale,'$','','.',','));	
			
		}else {
			 $(this).val(format($(this).val(),locale,'$','','.',','));	
			
		}
		 formatCheck($(this).val());	
	/*	var price=$(this).val();
		var replace = new Array("[\$]", "[\€]", "[¤]",'\,');
		var by = new Array("", "", "",'\.');
		var new_price=$.trim(str_replace(replace, by, price));
		$('#price').val(new_price);
		var locale='<?php echo $this->Session->read('LocTrain.locale');?>';
		if(locale=='en'){
			$('#price').priceFormat({
				limit:6,
				prefix: '$',
				centsSeparator: '.',
				thousandsSeparator: ','
			});
			}else if (locale=='de' || locale=="es"){
				
			$('#price').priceFormat({
				limit:6,
				prefix:'',
				suffix: ' €',
				centsSeparator: ',',
				thousandsSeparator:'.'
			}); 
				}else if (locale=='zh'){	
					$('#price').priceFormat({
						limit:6,
				prefix: '¤',
				centsSeparator: '.',
				thousandsSeparator:','
			});$(this).val().replace('US$','');
					}else{
				$('#price').priceFormat({
					limit:6,
				prefix: '$',
				centsSeparator: '.',
				thousandsSeparator: ','
			}); 
						}*/
		

		});
		<?php
	     $fmt = new NumberFormatter( $current_loc, NumberFormatter::DECIMAL_ALWAYS_SHOWN );

	?>
	var price_val='<?php echo $fmt->format($course_info['Course']['price']); ?>';
	$( ".selectBoxAnOption" ).each(function() {		
		if(price_val==$(this).html()){
		$(this).css('background','#316AC5');
	}
	});
	});
	
</script> 
<!--End  main container section -->
