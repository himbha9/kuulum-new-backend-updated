<?php echo ''; ?>
<?php $s_lang = array('en'=> __('english_united_states'),'de'=> __('german_germany'),'ar'=> __('arabic'),'fr'=> __('french_france'),'it'=> __('italian_italy'),'ja'=> __('japanese'),'ko'=> __('korean'),'pt'=> __('portuguese_portugal'),'ru'=> __('russian_russian_federation'),'zh'=> __('chinese_china'),'es'=> __('spanish_spain')); ?>
<?php $locale=$this->Session->read('LocTrain.locale');
		if($locale == 'ar') 
		{
			echo $this->Html->css('autoSuggest_arabic');
		}
		else{
			echo $this->Html->css('autoSuggest'); 
		}
		foreach($language as $lang) {if($lang['Language']['locale']==$locale){  $current_loc= $lang['Language']['lang_code'];}}

if(isset($css) && $css=='style'){
$img_icon='icons';
}else{
$img_icon='icons_contrast';
}
		?>
<?php echo $this->Html->script('jquery.autoSuggest');?>
<?php echo $this->Html->script('front/jasny-bootstrap.js');?>
<?php echo $this->Html->css('front/jasny-bootstrap.css');?>
<?php echo $this->Html->script('front/dropdowns-enhancement.min.js');?>
<?php echo $this->Html->css('front/dropdowns-enhancement.css');?>
<?php echo $this->Html->script('front/jquery.sortable.js');?>
<?php echo $this->Html->css('front/sort.css');?>

<?php echo $this->Html->script('front/jquery.price_format.2.0.js');?>

<style type="text/css">
.selectBoxArrow {
   cursor: pointer;
    float: left;
    margin-top: 10px;
    padding: 10px;
    position: absolute;
    right: 5px;
}
	.selectBoxInput{
		border:0px;
		padding-left:1px;
		height:16px;
		position:absolute;
		top:0px;
		left:0px; width: 100%;    padding: 0 12px;
		  background: none repeat scroll 0 0 #fff;
		  border: 1px solid #7f7f7f;
    border-radius: 4px;
      text-align: left;
    text-overflow: ellipsis;
    white-space: nowrap;
	}

	.selectBox{
		border:none;
		height:40px; width: 100% !important;
	
	}
	.selectBoxOptionContainer{
		 background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 4px;
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
    display: none;
    float: left;
		position:absolute;
	
		height:auto;
		
		left:0px;
		top:48px;
		visibility:hidden;
		overflow:auto;
		z-index:1000;
		 width: 100% !important;
	}
	.selectBoxIframe{
		position:absolute;
		background-color:#FFF;
		border:0px;
		z-index:999;
	}
	.selectBoxAnOption{
		 background: none repeat scroll 0 0 #fff ;
    color: #333 !important;
    display: block;
    font-family: "proxima-nova-n1","proxima-nova",sans-serif;
    font-size: 18px;
    font-style: normal;
    font-weight: 100;
    height: auto;
    line-height: 24px;
    margin: 0 !important;
    min-height: 24px;
    padding: 1px 15px !important;
    text-align: left;
    white-space: normal;
	}
	</style>
	<script type="text/javascript">
	
	// Path to arrow images
	var arrowImage = '<?php echo $this->webroot; ?>img/cart_arrow_down.png';	// Regular arrow
	var arrowImageOver ='<?php echo $this->webroot; ?>img/cart_arrow_down_black.png';	// Mouse over
	var arrowImageDown = '<?php echo $this->webroot; ?>img/cart_arrow_down.png';	// Mouse down

	
	var selectBoxIds = 0;
	var currentlyOpenedOptionBox = false;
	var editableSelect_activeArrow = false;
	

	
	function selectBox_switchImageUrl()
	{
		if(this.src.indexOf(arrowImage)>=0){
			this.src = this.src.replace(arrowImage,arrowImageOver);	
		}else{
			this.src = this.src.replace(arrowImageOver,arrowImage);
		}
		
		
	}
	
	function selectBox_showOptions()
	{
		if(editableSelect_activeArrow && editableSelect_activeArrow!=this){
			editableSelect_activeArrow.src = arrowImage;
			
		}
		editableSelect_activeArrow = this;
		
		var numId = this.id.replace(/[^\d]/g,'');
		var optionDiv = document.getElementById('selectBoxOptions' + numId);
		if(optionDiv.style.display=='block'){
			optionDiv.style.display='none';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='none';
			this.src = arrowImageOver;	
		}else{			
			optionDiv.style.display='block';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='block';
			this.src = arrowImageDown;	
			if(currentlyOpenedOptionBox && currentlyOpenedOptionBox!=optionDiv)currentlyOpenedOptionBox.style.display='none';	
			currentlyOpenedOptionBox= optionDiv;			
		}
	}
	
	function selectOptionValue()
	{
		var parentNode = this.parentNode.parentNode;
		var textInput = parentNode.getElementsByTagName('INPUT')[0];
		textInput.value = this.innerHTML.replace('&nbsp;',' ');	
		this.parentNode.style.display='none';	
		document.getElementById('arrowSelectBox' + parentNode.id.replace(/[^\d]/g,'')).src = arrowImageOver;
		
		if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + parentNode.id.replace(/[^\d]/g,'')).style.display='none';
		
	}
	var activeOption;
	function highlightSelectBoxOption()
	{
		if(this.style.backgroundColor=='#316AC5'){
			this.style.backgroundColor='';
			this.style.color='';
		}else{
			this.style.backgroundColor='#316AC5';
			this.style.color='#FFF';			
		}	
		
		if(activeOption){
			activeOption.style.backgroundColor='';
			activeOption.style.color='';			
		}
		activeOption = this;
		
	}
	
	function createEditableSelect(dest)
	{

		dest.className='selectBoxInput';		
		var div = document.createElement('DIV');
		div.style.styleFloat = 'left';
		div.style.width = dest.offsetWidth + 16 + 'px';
		div.style.position = 'relative';
		div.id = 'selectBox' + selectBoxIds;
		var parent = dest.parentNode;
		parent.insertBefore(div,dest);
		div.appendChild(dest);	
		div.className='selectBox';
		div.style.zIndex = 1 - selectBoxIds;

		var img = document.createElement('IMG');
		img.src = arrowImage;
		img.className = 'selectBoxArrow';
		
		img.onmouseover = selectBox_switchImageUrl;
		img.onmouseout = selectBox_switchImageUrl;
		img.onclick = selectBox_showOptions;
		img.id = 'arrowSelectBox' + selectBoxIds;

		div.appendChild(img);
		
		var optionDiv = document.createElement('DIV');
		optionDiv.id = 'selectBoxOptions' + selectBoxIds;
		optionDiv.className='selectBoxOptionContainer';
		optionDiv.style.width = div.offsetWidth-2 + 'px';
		div.appendChild(optionDiv);
		
		if(navigator.userAgent.indexOf('MSIE')>=0){
			var iframe = document.createElement('<IFRAME src="about:blank" frameborder=0>');
			iframe.style.width = optionDiv.style.width;
			iframe.style.height = optionDiv.offsetHeight + 'px';
			iframe.style.display='none';
			iframe.id = 'selectBoxIframe' + selectBoxIds;
			div.appendChild(iframe);
		}
		
		if(dest.getAttribute('selectBoxOptions')){
			var options = dest.getAttribute('selectBoxOptions').split(';');
			var optionsTotalHeight = 0;
			var optionArray = new Array();
			for(var no=0;no<options.length;no++){
				var anOption = document.createElement('DIV');
				anOption.innerHTML = options[no];
				anOption.className='selectBoxAnOption';
				anOption.onclick = selectOptionValue;
				anOption.style.width = optionDiv.style.width.replace('px','') - 2 + 'px'; 
				anOption.onmouseover = highlightSelectBoxOption;
				optionDiv.appendChild(anOption);	
				optionsTotalHeight = optionsTotalHeight + anOption.offsetHeight;
				optionArray.push(anOption);
			}
			if(optionsTotalHeight > optionDiv.offsetHeight){				
				for(var no=0;no<optionArray.length;no++){
					optionArray[no].style.width = optionDiv.style.width.replace('px','') - 22 + 'px'; 	
				}	
			}		
			optionDiv.style.display='none';
			optionDiv.style.visibility='visible';
		}
		
		selectBoxIds = selectBoxIds + 1;
	}	
	
	</script>
<script type="text/javascript">
$(function(){

 var course_id='<?php echo base64_encode(convert_uuencode($course_info['Course']['id'])); ?>';
            req = $.ajax({
            url:ajax_url+'Members/check_retire_course/',
            type: 'get',

            data: {'course_id': course_id},
 beforeSend : function(){
	$('.loading_inner').show();
        $('.InnerContent').hide();
    },
            success: function(resp){	
				$('.loading_inner').hide();			
                            $(".load_top").hide();
                            if($.trim(resp)=='1')
                            {
                                    window.location=ajax_url+'Members/course';
                            }
                            else
                            {  $('.InnerContent').show();
                                   // alert(resp);
                                    return false;
                            }

                    }
    });
});
    $(document).ready(function(){

          $('body').on('click','.leasson1',function(e){
window.location=ajax_url+'Members/lesson_edit/'+$(this).attr('rel');
});

$('.loading_inner').hide();

$('#c_title').on('blur',function(){
		var msg = '';
		var finalNameVal = $('#c_title').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_title").text(msg);
		  }
		  else {
			$("#err_title").text('');
		  }
	});
$('#c_description').on('blur',function(){
		var msg = '';
		var finalNameVal = $('#c_description').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_description").text(msg);
		  }
		  else {
			$("#err_description").text('');
		  }
	});

// check the course is retire or not 
     

        /////
             $(".fileinput").on("change.bs.fileinput", function(e){
                 var image_val=$('#image').val();
                 var get_extn=image_val.split('.');
                    if(get_extn[1]=='png' || get_extn[1] =='jpeg' || get_extn[1]=='jpg' || get_extn[1]=='gif'){
                        
                    }else{
                        $('.fileinput').fileinput('clear');
                        alert('Please select valid image file'); 
                        return false;
                    }
                });
 $('#remove_cover_image').click(function(){
                    $(this).hide();
			$('#old_image').val('');
                        $('.thumbnail img').attr('src','<?php echo ROOT_FOLDER;?>img/front/cover_photo.png');
                              });
//course publish
$('#yes_publish').on('click',function(){
	$(this).attr('disabled',true);
 req = $.ajax({
            url:ajax_url+"Members/saveCourse_ajax/<?php echo $course_info['Course']['id']; ?>/<?php echo base64_encode(convert_uuencode($course_info['Course']['id'])); ?>",           
 beforeSend : function(){        
	$('.loading_inner').show();
       
    },
            success: function(resp){	
			
			$('.loading_inner').hide();			
                    	$(".load_top").hide();
                           //alert(resp);
			if(resp =='success'){
			
				 location.reload();
			}else{
				$(this).attr('disabled',false);
				alert('<?php echo __("something_went_wrong_please_try_after_sometime");?>');
			}

                    }
    });
});
$('#no_publish').on('click',function(){
    $('#confirmPublicCourseModel').modal('hide');
});
$('.disablePublishCourse').click(function(){

	 $('#confirmPublicCourseModel').modal('show');
});
            });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        
        var locale_set=$('#c_language').attr('value');
        if(locale_set !==''){
            $(".dropdown_menu_locale li a").each(function(){
                if( $(this).attr('value') == locale_set ) {
                     $("#dropdown4:first-child").text($(this).text()).append('<span class="caret"></span>' );;
                }
            });       
            $("#dropdown4:first-child").val(locale_set);        
        }
        
      /*  var price_set=$('#price').attr('value');
        if(price_set !==''){
            $(".dropdown-menu-price li a").each(function(){
                if( $(this).attr('value') == price_set ) {
                     $("#price_btn:first-child").text($(this).text()).append('<span class="caret"></span>' );;
                }
            });       
            $("#price_btn:first-child").val(price_set);        
        }*/
        
      
        //var category_id=$('#sl_subcategory_id').attr('value');
        if($('#sl_subcategory_id').attr('value') !=='' && $('#sl_subcategory_id').attr('value') !=0){
        var category_id=$('#sl_subcategory_id').attr('value');
        $("a.sl_sub_cat").each(function(){            
            if( $(this).attr('value') == category_id ) {
                $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
                $("#category_btn").attr('value', $(this).attr('value'));
                
            }
        });       
         
    }else if($('#subcategory_id').attr('value') !='' && $('#subcategory_id').attr('value') !=0) {
     var category_id=$('#subcategory_id').attr('value');
        $("a.sub_cat").each(function(){            
            if( $(this).attr('value') == category_id ) {
                $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
                $("#category_btn").attr('value', $(this).attr('value'));
                
            }
        });     

	}else{ 

 var category_id=$('#category_id').attr('value');
        $("a.course_cat").each(function(){            
            if( $(this).attr('value') == category_id ) {
                $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
                $("#category_btn").attr('value', $(this).attr('value'));
                
            }
        });     

}
    
       
        $(".dropdown_menu_locale li a").click(function(){
          $('#c_language').val($(this).attr('value'));
          $("#dropdown4:first-child").text($(this).text()).append('<span class="caret"></span>' );
          $("#dropdown4:first-child").val($(this).attr('value'));
        });
        ///price
        
       /* $('.dropdown-menu-price li a').click(function(){
              $('#price').val($(this).attr('value'));
          $("#price_btn:first-child").text($(this).text()).append('<span class="caret"></span>' );
          $("#price_btn:first-child").val($(this).attr('value'));
        });*/
        
        ///category
        
         $('.dropdown-menu-3 li a').click(function(){
            $('#category_id').val($(this).attr('value'));
        $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
          $("#category_btn").attr('value',$(this).attr('value'));
        });
$('.dropdown-menu-2 li a').click(function(){
            var cat_f=$(this).parents('ul.dropdown-menu-category li').children('.course_cat');              
            $('#category_id').val($.trim(cat_f.attr('id')));
          $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
          $("#category_btn").attr('value',$(this).attr('value'));
        });
   $('.dropdown-menu-category li a').click(function(){             
            $('#category_id').val($.trim($(this).attr('id')));
          $("#category_btn").text($(this).text()).append('<b class="caret"></b>' );
          $("#category_btn").attr('value',$(this).attr('value'));
        });
        var data = {items: [<?php echo $interest_list;?>]};
	$("#keywords").autoSuggest(data.items, {selectedItemProp: "name", searchObjProps: "value",preFill: [<?php echo $pre_fill;?>],fieldName: "data[Course][keywords]"});
	
        $('ul.as-selections').addClass('FilterKeyword');
        
        $('.course_cat').click(function(){
		
		var id = $.trim($(this).attr('id'));
		
		$('#category_id').val(id);
		
		$('#subcategory_id').val('');
		$('#sl_subcategory_id').val('');			
		$('#sub_cat_txt').val('');
		
	});
	
	$('.sub_cat').click(function(){
		
		var id = $.trim($(this).attr('id'));
		var txt = $.trim($(this).html());
		
		$('#subcategory_id').val(id);		
		$('#sub_cat_txt').val(txt);
		
		//$('.selected2').html(txt);
		$('#sl_subcategory_id').val('');
		//$(".course_cat").css({'color':'#888'});

	});
	
	$('.sl_sub_cat').click(function(){
		var cat_f=$(this).parents('ul.dropdown-menu-category li').children('.course_cat');              
		var sub = $(this).parents('ul.dropdown-menu-2 li').children('.sub_cat');              
		var sub_cat_id = $.trim(sub.attr('id'));
		var sub_cat_txt = $.trim(sub.html());//alert(sub_cat_txt);
		
		var id = $.trim($(this).attr('id'));
		var txt = $.trim($(this).html());//alert(txt);
               // alert($.trim(cat_f.attr('id')));
		$('#category_id').val($.trim(cat_f.attr('id')));
		$('#subcategory_id').val(sub_cat_id);
		$('#sl_subcategory_id').val(id);	
		
		$('#sub_cat_txt').val(sub_cat_txt+', '+txt);
		//$('.selected2').html(txt);
		//$(".course_cat").css({'color':'#888'});
		
	});
        $('body').on("click",'#retire_course',function(){
		var course_id = $(this).attr('rel');
			$this=$(this);
                        $('#course_confirm_id').val(course_id);
                        $('#confirm_course_model').modal('show');
	
	});
        $('#course_yes').click(function(){
            var course_id= $('#course_confirm_id').val();
            req = $.ajax({
            url:ajax_url+'Members/retire_course/',
            type: 'get',
            data: {'course_id': course_id},
            success: function(resp){				
                            $(".load_top").hide();
                            if($.trim(resp)=='retired')
                            {
                                    window.location=ajax_url+'Members/course';
                            }
                            else
                            {
                                    alert(resp);
                                    return false;
                            }

                    }
    });
        });
        $('#course_no').click(function(){
            $('#confirm_course_model').modal('hide');
        });
 var arrValuesForOrder =[];
<?php if(isset($course_info['Course']['lessons_order']) && !empty($course_info['Course']['lessons_order'])){
	$order_array=explode(',',$course_info['Course']['lessons_order']);
	foreach($order_array as $row){ ?>
		  arrValuesForOrder.push('<?php echo $row;?>');
	<?php }	 ?>
<?php if(isset($course_info['CourseLesson']) && !empty($course_info['CourseLesson'])){
	
	foreach($course_info['CourseLesson'] as $row){
			if(!in_array($row['id'],$order_array)){
 ?>
		  arrValuesForOrder.push('<?php echo $row['id'];?>');
	<?php } }	} ?>
	 var len=arrValuesForOrder.length;
            var $ul = $(".exclude"),
                $items = $(".exclude").children();
	for (var i =0; i<len; i++) {
                // index is zero-based to you have to remove one from the values in your array
	//alert(arrValuesForOrder[i]);
//alert($("#"+arrValuesForOrder[i]));
                 $ul.append($("#lesson_"+arrValuesForOrder[i]));                 
            }
<?php }?>
$('.exclude').sortable({items: ':not(.disabled)'}).bind('sortupdate', function() {
		//Triggered when the user stopped sorting and the DOM position has changed.
		 var homeVals = [];
		$(this).find('li').each(function(){
		   // alert($(this).find('div:first').attr('id'));
		    homeVals.push($(this).find('div:first').attr('id'));
		}); 
		$('#lessonsOrder').val(homeVals.join());
	});
    });
 
</script>
<!-- main container section -->
<section>
  <div class="container InnerContent">
    <div class="row">
        <?php if($this->Session->read("LocTrain.flashMsg") != ''){ ?>
            <div class="account-activated">	<?php echo __($this->Session->read("LocTrain.flashMsg")); ?></div>
        <?php $this->Session->delete("LocTrain.flashMsg");  } ?>
    <?php echo $this->Form->create('Course',array('id'=>'addCourse','url'=>array('controller'=>'Members','action'=>'course_edit'),'enctype'=>'multipart/form-data')); ?>
    <div class="col-lg-6 col-md-6 col-sm-6 CourseLeftPanel">
	
    <div class="SmallLinkBoxWrapper SmallLinkBoxWrapper1 SmallLinkBox4 SmallLinkBox4 SmallLinksWrapper">   
        <div class="SmallLinkBox">
<?php echo $this->Form->button($this->Html->image('front/'.$img_icon.'/icon_upload_cloud.svg',array('class'=>'','width'=>57,'height'=>57)),array('class'=>'sc_btns btn_sbmt disableAddCourseBut','type'=>'submit','div'=>false,'onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')")); ?>               
                <span class="Link"> <?php echo $this->Form->button(__('save_changes_'),array('class'=>'sc_btns btn_sbmt disableAddCourseBut','type'=>'submit','div'=>false,'onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')")); ?></span>
            </div>
            <div class="SmallLinkBox">
    <?php echo $this->Form->button($this->Html->image('front/'.$img_icon.'/icon_plus_add.svg',array('class'=>'','width'=>57,'height'=>57)),array('type'=>'submit','class'=>'sc_btns btn_sbmt disAddLessonBut','div'=>false,'onclick'=>"return ajax_form_new('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')")); ?>
               
                <span class="Link">
                <?php echo $this->Form->button(__('add_lesson_'),array('type'=>'submit','class'=>'sc_btns btn_sbmt disAddLessonBut','div'=>false,'onclick'=>"return ajax_form_new('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')")); ?>
                </span>
            </div>
            <div class="SmallLinkBox">
                <?php
echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_price.svg',array('class'=>'','width'=>57,'height'=>57)),array('action'=>'view_course_income',base64_encode(convert_uuencode($course_info['Course']['id']))),array('class'=>'scn_link scn_margn','escape'=>false));
  ?> 
                 <span class="Link"> <?php echo $this->Html->link(__('view_income_'),array('action'=>'view_course_income',base64_encode(convert_uuencode($course_info['Course']['id']))),array('class'=>'scn_link scn_margn','escape'=>false)); ?></span>
            </div>
            <div class="SmallLinkBox">
 
<?php if($course_info['Course']['isPublished']=='1'){?>
  <?php echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_document_archive.svg',array('class'=>'','width'=>57,'height'=>57)),'javascript:void(0)',array('class'=>'scn_link scn_margn','escape'=>false,'id'=>'retire_course','rel'=>base64_encode(convert_uuencode($course_info['Course']['id'])))); ?>                      	
                
                <span class="Link">
                    
                <?php echo $this->Html->link(__('retire_course'),'javascript:void(0)',array('class'=>'scn_link scn_margn','escape'=>false,'id'=>'retire_course','rel'=>base64_encode(convert_uuencode($course_info['Course']['id'])))); ?>                      	
                </span>
<?php } else {?>
      <a  class="disablePublishCourse" id="<?php echo $course_info['Course']['id'];?>" rel="<?php echo base64_encode(convert_uuencode($course_info['Course']['id'])) ?>"><?php echo $this->Html->image('front/'.$img_icon.'/icon_plane_paper.svg',array('class'=>'','width'=>57,'height'=>57)); ?></a>               
                
                <span class="Link">                    
                       <a   class="disablePublishCourse" id="<?php echo $course_info['Course']['id'];?>" rel="<?php echo base64_encode(convert_uuencode($course_info['Course']['id'])) ?>"><?php echo __('publish_course');?></a>                    
</span>
<?php } ?>
 
            </div>
        <div class="clearfix"></div>
    </div>
        
  
  <div class="form-group">
    <label for="exampleInputEmail1"><?php echo __('course_title');?></label>
    <?php echo $this->Form->input('id',array('type'=>'hidden','id'=>'h_id','value'=>$course_info['Course']['id'])); ?>
    <?php echo $this->Form->input('sub_type',array('type'=>'hidden','id'=>'sub_id')); ?>
	<?php echo $this->Form->input('lessons_order',array('type'=>'hidden','id'=>'lessonsOrder','value'=>$course_info['Course']['lessons_order'])); ?>
    <?php echo $this->Form->input('title',array('type'=>'text','maxlength'=>250,'id'=>'c_title','class'=>'form-control','placeholder'=>__(''),'label'=>false,'div'=>false,'autofocus'=>'','value'=>$course_info['Course']['title'])); ?> 
    <div id="err_title" class="register_error alert-danger"> <?php if(isset($error['title'][0])) echo $error['title'][0];?> </div>
   
  </div>
  <div class="form-group">
       <label for="exampleInputPassword1">*<?php echo __('description'); ?></label>
       <?php echo $this->Form->input('description',array('type'=>'textarea','id'=>'c_description',"rows"=>"3",'class'=>'form-control','label'=>false,'div'=>false,'value'=>str_replace('<br />','',$course_info['Course']['description']))); ?> 
   
    <div id="err_description" class="register_error alert-danger"> <?php if(isset($error['description'][0])) echo $error['description'][0];?> </div> 
    
  </div>
  <div class="form-group">
   <label for="exampleInputPassword1"><?php echo __('language'); ?></label>
    	<?php echo $this->Form->input('primary_lang',array('type'=>'hidden','id'=>'c_language','class'=>'','label'=>false,'div'=>false,'value'=>$course_info['Course']['locale'])); ?>                    
    <div class="dropdown DropdownField" id="">
        <button data-toggle="dropdown" id="dropdown4" type="button" class="btn btn-default dropdown-toggle LanguageRegion"><?php echo __('dropdown');?><span class="caret"></span></button>
        <ul aria-labelledby="dropdown4" role="menu" class="dropdown-menu dropdown_menu_locale">
              <?php foreach($language as $lang) { ?>
            <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $lang['Language']['locale'] ?>"><?php echo $s_lang[$lang['Language']['locale']];?> </a></li>
            <?php } ?>           
        </ul>
    </div> 
   <div id="err_primary_lang" class="register_error alert-danger"> <?php if(isset($error['primary_lang'][0])) echo $error['primary_lang'][0];?> </div> 
  </div>
      <div class="form-group">  	  
    <label for="exampleInputPassword1"><?php echo __('price'); ?></label>
      <?php $priceStr='';    
      foreach($course_prices as $key=>$val) { 
      $priceStr=$priceStr.$this->Utility->externlizePrice($language,$locale,$val).';';
		  } 
		  
		  ?> 
    <?php echo $this->Form->input('price',array('type'=>'text','id'=>'price','selectBoxOptions'=>$priceStr,'class'=>'form-control','value'=>$this->Utility->externlizePrice($language,$locale,$course_info['Course']['price']),'label'=>false,'div'=>false)); ?>                    
         <div id="err_price" class="register_error alert-danger"> <?php if(isset($error['price'][0])) echo $error['price'][0];?> </div> 
   
     
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">*<?php echo __('category'); ?></label>
    <?php echo $this->Form->input('category_id',array('type'=>'hidden','id'=>'category_id','class'=>'','label'=>false,'div'=>false,'value'=>($course_info['Course']['category_id'] ==NULL? 'NULL' : $course_info['Course']['category_id']))); ?>                    
   <div class="navbar-nav MultiLevelDropdown">
        <div class="dropdown DropdownField"> <a class="DropdownCaption" data-toggle="dropdown" id="category_btn" value=""><?php echo __('dropdown');?> <b class="caret"></b></a>
           <ul class="dropdown-menu dropdown-menu-box dropdown-menu-category">
          <?php foreach($categories as $category) {?>
            
                
              <?php if(!empty($category['CourseSubcategory'])){ ?>
                <li class="dropdown-submenu">
                <a  tabindex="-1" data-toggle="dropdown" class="MultiLevelTitle course_cat" id="<?php echo $category['CourseCategory']['id']; ?>" value="<?php echo $category['CourseCategory']['id']; ?>"><?php echo $category['CourseCategory'][$locale]; ?></a>
                <ul class="dropdown-menu dropdown-menu-2">
                     <?php     foreach($category['CourseSubcategory'] as $subcat)
                { ?>                
                
                   <?php 
                        if(!empty($subcat['CourseSlSubcategory']))
                        {
                        ?>
                    <li class="dropdown-submenu">                    
                    <a  class="MultiLevelLink sub_cat" data-toggle="dropdown" id="<?php echo $subcat['id']; ?>" tabindex="-1" value="<?php echo $subcat['id']; ?>"><?php echo $subcat[$locale]; ?></a>
                    
                    <ul class="dropdown-menu dropdown-menu-3">
                          <?php
                                foreach($subcat['CourseSlSubcategory'] as $slcat)
                                {
                                ?>
                            <li>
                            <a  class="MultiSubLevelLink sl_sub_cat" id="<?php echo $slcat['id']; ?>" tabindex="-1"  value="<?php echo $slcat['id']; ?>"><?php echo $slcat[$locale]; ?></a>
                            </li>
                    
                        <?php } ?>
                    </ul>
                        <?php }else{ ?>
                       <li>                    
                    <a  class="MultiLevelLink sub_cat" id="<?php echo $subcat['id']; ?>" tabindex="-1" value="<?php echo $subcat['id']; ?>"><?php echo $subcat[$locale]; ?></a>
                         
                     <?php   } ?>
                </li>
                <?php } ?>
              </ul>
              <?php }else{?>
              <li>
                <a  tabindex="-1"  class="MultiLevelTitle course_cat" id="<?php echo $category['CourseCategory']['id']; ?>" value="<?php echo $category['CourseCategory']['id']; ?>"><?php echo $category['CourseCategory'][$locale]; ?></a>    
             <?php } ?>   
            </li>
            
            
          <?php } ?>
          </ul>
             <div class="reg_flds" style="display:none;">
                <?php echo $this->Form->input('sub_cat',array('type'=>'text','id'=>'sub_cat_txt','readonly'=>'readonly','class'=>'regfld_txt gradient','label'=>false,'div'=>false,'value'=>$course_info['CourseSubcategory'][$locale].', '.$course_info['CourseSlSubcategory'][$locale])); ?>
		<?php echo $this->Form->input('subcategory_id',array('type'=>'hidden','id'=>'subcategory_id','class'=>'','label'=>false,'div'=>false,'value'=>($course_info['Course']['subcategory_id'])?$course_info['Course']['subcategory_id']:0)); ?>
                <?php echo $this->Form->input('sl_subcategory_id',array('type'=>'hidden','id'=>'sl_subcategory_id','class'=>'','label'=>false,'div'=>false,'value'=>($course_info['Course']['sl_subcategory_id'])?$course_info['Course']['sl_subcategory_id']:0)); ?>
            </div>
        <div id="err_category_id" class="register_error alert-danger"> <?php if(isset($error['category_id'][0])) echo $error['category_id'][0];?> </div> 
     </div>   
     		</div>
  </div>
  
  <div class="form-group">
      <label><?php echo __('type_some_keywords_that_relevant_for_the_course_content');?></label>
    <?php echo $this->Form->input('keyword',array('type'=>'text','id'=>'keywords','class'=>'form-control','label'=>false,'div'=>false)); ?>
    <div id="err_keywords" class="register_error alert-danger"> <?php if(isset($error['keyword'][0])) echo $error['keyword'][0];?> </div>
  
  </div>
  <div class="form-group">
   <label><?php echo __('cover_image');?></label>
      <div class="fileinput fileinput-new coverImgWrapper" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 652px; height: 329px;" style="display:none;">
                <?php 
                if($course_info['Course']['image'])
                {
                    $image = $course_info['Course']['image'];
			 $path=$this->Utility->getAWSImgUrl(COURSES_BUCKET,$image);
                    //$path = 'files/members/courses/'.$image;
		?>
		<img  src="<?php echo $path; ?>" alt="Profile Picture" class='img-responsive' style="max-height: 329px;"/>
		<?php
                 /*   if(file_exists($path))
                    {
                    ?>
                <!-- <img  src="<?php echo HTTP_ROOT.'files/members/courses/'.$image; ?>" alt="Profile Picture" class='img-responsive' /> -->
		
            <?php }*/}else{ ?>
                 <?php echo $this->Html->image('front/cover_photo.png',array('alt'=>'','class'=>'img-responsive')); ?> 
            <?php } ?>
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 652px; max-height: 329px;">
            <?php 
                if($course_info['Course']['image'])
                {
                    $image = $course_info['Course']['image'];
		 $path=$this->Utility->getAWSImgUrl(COURSES_BUCKET,$image);
?>
		<img  src="<?php echo $path; ?>" alt="Profile Picture" class='img-responsive' />
<?php
                    //$path = 'files/members/courses/'.$image;
                 /*   if(file_exists($path))
                    {
                    ?>
                <!-- <img  src="<?php echo HTTP_ROOT.'files/members/courses/'.$image; ?>" alt="Profile Picture" class='img-responsive' /> -->
		
            <?php }*/}else{ ?>
                 <?php echo $this->Html->image('front/cover_photo.png',array('alt'=>'','class'=>'img-responsive')); ?> 
            <?php } ?>
            </div>
            <div>
              <span class="btn btn-default btn-file"><span class="fileinput-new"><?php echo __("upload_photo");?></span><span class="fileinput-exists"><?php echo __("upload_photo");?></span>
                  <?php echo $this->Form->input('image',array('name'=>'image','type'=>'file','id'=>'image','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('picture',array('type'=>'hidden','id'=>'file_hidden','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('old_image',array('type'=>'hidden','id'=>'old_image','label'=>false,'div'=>false,'value'=>$course_info['Course']['image'])); ?>
              </span>
<?php
                if($course_info['Course']['image'])
                {
                    $image = $course_info['Course']['image'];
                  //  $path = 'files/members/courses/'.$image;
		$path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$image);
                    if($path)
                    { ?>
<a class="fileinput-new" id="remove_cover_image"><?php echo __("remove_photo");?></a>
<?php }} ?>
              <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo __('remove_photo');?></a>
            </div>
          </div> 

    
    
    </div>
    </div>
    
    <div class="col-lg-6 col-md-6 col-sm-6 ProfileSection CourseRightPanel"> 
    	<div class="SmallLinkBoxWrapper SmallLinkBoxWrapper2 smalllinkboxbg SmallLinkBox4">   
             <div class="SmallLinkBox">
<?php echo $this->Form->button($this->Html->image('front/'.$img_icon.'/icon_upload_cloud.svg',array('class'=>'','width'=>57,'height'=>57)),array('class'=>'sc_btns btn_sbmt disableAddCourseBut','type'=>'submit','div'=>false,'onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')")); ?>               
                <span class="Link"> <?php echo $this->Form->button(__('save_changes_'),array('class'=>'sc_btns btn_sbmt disableAddCourseBut','type'=>'submit','div'=>false,'onclick'=>"return ajax_form('addCourse','Members/validate_course_ajax','load_top','disableAddCourseBut')")); ?></span>
            </div>
            <div class="SmallLinkBox">
    <?php echo $this->Form->button($this->Html->image('front/'.$img_icon.'/icon_plus_add.svg',array('class'=>'','width'=>57,'height'=>57)),array('type'=>'submit','class'=>'sc_btns btn_sbmt disAddLessonBut','div'=>false,'onclick'=>"return ajax_form_new('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')")); ?>
               
                <span class="Link">
                <?php echo $this->Form->button(__('add_lesson_'),array('type'=>'submit','class'=>'sc_btns btn_sbmt disAddLessonBut','div'=>false,'onclick'=>"return ajax_form_new('addCourse','Members/validate_course_ajax','load_top','disAddLessonBut')")); ?>
                </span>
            </div>
            <div class="SmallLinkBox">
                <?php
echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_price.svg',array('class'=>'','width'=>57,'height'=>57)),array('action'=>'view_course_income',base64_encode(convert_uuencode($course_info['Course']['id']))),array('class'=>'scn_link scn_margn','escape'=>false));
  ?> 
                 <span class="Link"> <?php echo $this->Html->link( __('view_income_'),array('action'=>'view_course_income',base64_encode(convert_uuencode($course_info['Course']['id']))),array('class'=>'scn_link scn_margn','escape'=>false)); ?></span>
            </div>
            <div class="SmallLinkBox">

<?php  if($course_info['Course']['isPublished']=='1'){?>
  <?php echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_document_archive.svg',array('class'=>'','width'=>57,'height'=>57)),'javascript:void(0)',array('class'=>'scn_link scn_margn','escape'=>false,'id'=>'retire_course','rel'=>base64_encode(convert_uuencode($course_info['Course']['id'])))); ?>                      	
                
                <span class="Link">
                    
                <?php echo $this->Html->link(__('retire_course'),'javascript:void(0)',array('class'=>'scn_link scn_margn','escape'=>false,'id'=>'retire_course','rel'=>base64_encode(convert_uuencode($course_info['Course']['id'])))); ?>                      	
                </span>
<?php } else {?>
      <a  class="disablePublishCourse" id="<?php echo $course_info['Course']['id'];?>" rel="<?php echo base64_encode(convert_uuencode($course_info['Course']['id'])) ?>"><?php echo $this->Html->image('front/'.$img_icon.'/icon_plane_paper.svg',array('class'=>'','width'=>57,'height'=>57)); ?></a>               
                
                <span class="Link">                    
                       <a   class="disablePublishCourse" id="<?php echo $course_info['Course']['id'];?>" rel="<?php echo base64_encode(convert_uuencode($course_info['Course']['id'])) ?>"><?php echo __('publish_course');?></a>                    
</span>
<?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    
    
    <div id="lesson" class="col-lg-12 col-md-12 col-sm-12">
    
    	<div class="Title"><?php echo __('lessons');?></div>
    	<?php if(!empty($course_info['CourseLesson'])) { $i = 1;
 ?>
<ul class="exclude list dragDrop">
        <?php foreach($course_info['CourseLesson'] as $lesson){   ?>
 <li id="lesson_<?php echo $lesson['id']; ?>">
            <div class="leasson1" id="<?php echo $lesson['id']; ?>" rel="<?php echo base64_encode(convert_uuencode($lesson['id']));?>">
                <div class="col-lg-6 col-md-6 col-sm-6 leasson_img">
                   
                    <?php
                    if($lesson['image'] && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$lesson['image']))
                        {
                             echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$lesson['image']),array	( 'class'=>'img-responsive','width'=>'','height'=>'')),array('action'=>'lesson_edit',base64_encode(convert_uuencode($lesson['id']))),array('escape'=>false)); 
                        }else{

 echo $this->Html->link($this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>'','height'=>'')),array('action'=>'lesson_edit',base64_encode(convert_uuencode($lesson['id']))),array('escape'=>false));
}                          
                  

 ?>
                
                
                
                </div>     
                <div class="right_lesson col-lg-6 col-md-6 col-sm-6 right_text">
                    <h2>
                        <?php

$title_v=$this->Text->truncate(strip_tags($lesson['title']),40,array( 'ellipsis' => '...','exact' => false,'html' => false));
 echo $this->Html->link($title_v,array('action'=>'lesson_edit',base64_encode(convert_uuencode($lesson['id']))))?>                                    
                    </h2>
                    <div class="row price_sec">
                        <div class="pull-left"> <small>                           			
                            <?php $hours=00;$secs=00;$min=00;
				if($lesson['hours'] > 0 || $lesson['mins'] > 0 || $lesson['secs'] > 0) {
					if($lesson['hours'] <10){
					$hours='0'.$lesson['hours'];
					}else{
					$hours=$lesson['hours'];
					}
				if( $lesson['mins'] < 10){
					$min='0'.$lesson['mins'];
					}else{
					$min=$lesson['mins'];
					}
				if($lesson['secs'] <10){
					$secs='0'.$lesson['secs'];
					}else{
					$secs=$lesson['secs'];
					}
					echo trim($hours).':'.trim($min).":".trim($secs);
				 } else { echo __('duration_unknown'); }
				?>			
                                    </small>
                        </div>      
                        <div class="pull-right text-right"><small><?php echo $this->Utility->externlizePrice($language,$locale,$lesson['price']); ?></small></div>        
                    </div>     
                </div>          
            </div>
        <?php $i++; ?></li><?php } ?>
</ul>
        <?php } else {  ?>
            <div class="leasson1">
                <?php echo __('there_are_no_lessons_for_this_course'); ?>
            </div>
        <?php } ?>
    </div>
    </div>


          <?php echo $this->Form->end(); ?>
    </div>
  </div>
<div class="modal fade" id="confirm_course_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
<h4 class="modal-title" id="myModalLabel"><?php echo __('confirmation');?></h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <input type="hidden" id="course_confirm_id" value="">
    <p><?php echo __('are_you_sure_you_want_to_retire_this_course'); ?></p>
    </div>
<span class="Link"><a id="course_yes"><?php echo __('yes');?></a>&nbsp;&nbsp;<a id="course_no"><?php echo __('no');?></a></span>

</div>
</div>
</div>
</div>
 <div class="loading" style="margin-left: -12px">
                    <div class="loading_inner">
                        <?php echo $this->Html->image('front/ajax-loader.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                    </div>
              </div>

<div class="modal fade" id="confirmPublicCourseModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo __('publish_course_');?></h4>
                    </div>
                    <div class="modal-body"> 
                        <p class="avlListMsg Title"><?php echo __('your_course_will_be_visible_in_the_catalogue_and_search_results_do_you_want_to_continue'); ?></p>
                         
                       
                            <input type="hidden" id="course_id" value="">
                            <span class="Link"><a id="yes_publish"><?php echo __('yes'); ?></a> &nbsp;&nbsp;<a id="no_publish"><?php echo __('no'); ?></a></span>
                            
                       
                    </div>                            
                </div>
            </div>
        </div>
</section>
<script type="text/javascript">
createEditableSelect(document.forms['addCourse'].price);
function str_replace(replace, by, str) {
    for (var i=0; i<replace.length; i++) {
        str = str.replace(new RegExp(replace[i], "g"), by[i]);
    }
  
    return str;
}
function to_numbers (str,limit)
			{var is_number = /[0-9]/;
				var formatted = '';
				for (var i=0;i<(str.length);i++)
				{
					char_ = str.charAt(i);
					if (formatted.length==0 && char_==0) char_ = false;

					if (char_ && char_.match(is_number))
					{
						/*if (limit)
						{
							if (formatted.length < limit) formatted = formatted+char_;
						}
						else
						{
							formatted = formatted+char_;
						}*/
						formatted = formatted+char_;
					}
				}

				return formatted;
			}
			function fill_with_zeroes (str)
			{var centsLimit=2;
			 if(str.length >1){
				while (str.length<(centsLimit+2)) str = str+'0';
				}else{
					while (str.length<(centsLimit+1)) str = str+'0';
					}
								return str;
			}
			function fill_with_decimal(str){
				var centsLimit=2;
				while (str.length<(centsLimit)) str = str+'0';
				return str;
				
				}
var format = function(num,locale,pre,suf,cenSep,thouSep){	var centsLimit=2;	
	if(pre == ''){
		var str = num.toString().replace(suf, ""), parts = false, output = [], i = 1, formatted = null;
		}else{
			var str = num.toString().replace(pre, ""), parts = false, output = [], i = 1, formatted = null;
		}
		if(str.indexOf(cenSep) > 0){
			partsw = str.split(cenSep);
			str=((to_numbers(partsw[0]))?to_numbers(partsw[0]):0)+cenSep+fill_with_decimal(to_numbers(partsw[1]));
		}else{
		/*str=fill_with_zeroes(to_numbers(str,6));		
		var centsVal = str.substr(str.length-centsLimit,centsLimit);
		var integerVal = str.substr(0,str.length-centsLimit);
		str =integerVal+cenSep+centsVal;*/
		partsw=(to_numbers(str,6))?to_numbers(str,6):0;
		str=partsw+cenSep+'00';
		}	
	if(str.indexOf(cenSep) > 0) {
		parts = str.split(cenSep);
		str = parts[0];
	}
	str = str.split("").reverse();
	for(var j = 0, len = str.length; j < len; j++) {
		if(str[j] != thouSep) {
			output.push(str[j]);
			if(i%3 == 0 && j < (len - 1)) {
				output.push(thouSep);
			}
			i++;
		}
	}
	formatted = output.reverse().join("");
	if(pre == ''){
		return(formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : "")+' '+suf);
	}else {
	return(pre + formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : ""));
	}
};
var formatCheck=function(price){
		var replace = new Array("[\$]", "[\€]", "[¤]",'\,');
		var by = new Array("", "", "","");
		var new_price=$.trim(str_replace(replace, by, price));
		var msg = '';				
		if(new_price.length<3 || new_price.length>7){
			 msg = '<?php echo __("enter_a_number_between_000_and_999999");?>';
		  	  $("#err_price").text(msg);
			}else {
			$("#err_price").text('');
		  }
};

$('document').ready(function(){
  var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
	specialKeys.push(37); //Home
	specialKeys.push(39); //Home
$("#price").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode;
                var ret = ((keyCode >= 48 && keyCode <= 57) || keyCode==190 || keyCode == 44 ||( specialKeys.indexOf(keyCode) != -1 && e.charCode != 37 && e.charCode != 39));                
                return ret;
		
            });
	var locale='<?php echo $this->Session->read('LocTrain.locale');?>';
	if(locale=='en'){
			 $('#price').val(format($('#price').val(),locale,'$','','.',','));			
		}else if (locale=='de'){
			$('#price').val(format($('#price').val(),locale,'',' $',',','.'));		
		}else if (locale=="es"){
			$('#price').val(format($('#price').val(),locale,'$','',',','.'));		
		}else if (locale=='zh'){	
			 $('#price').val(format($('#price').val(),locale,'$','','.',','));			
		}else {
			 $('#price').val(format($('#price').val(),locale,'$','','.',','));			
		}
	$('#price').blur(function(){
		
		var locale='<?php echo $this->Session->read('LocTrain.locale');?>';
	
		if(locale=='en'){
			 $(this).val(format($(this).val(),locale,'$','','.',','));
					
		}else if (locale=='de'){
			$(this).val(format($(this).val(),locale,'',' $',',','.'));	
			
		}else if (locale=="es"){
			$(this).val(format($(this).val(),locale,'$','',',','.'));
			
		}else if (locale=='zh'){	
			 $(this).val(format($(this).val(),locale,'$','','.',','));	
			
		}else {
			 $(this).val(format($(this).val(),locale,'$','','.',','));	
			
		}
		 formatCheck($(this).val());	

		});
		
		<?php
	     $fmt = new NumberFormatter( $current_loc, NumberFormatter::DECIMAL_ALWAYS_SHOWN );

	?>
	var price_val='<?php echo $fmt->format($course_info['Course']['price']); ?>';
	$( ".selectBoxAnOption" ).each(function() {		
		if(price_val==$(this).html()){
		$(this).css('background','#316AC5');
	}
	});
	});
	
</script> 
<!--End  main container section -->

