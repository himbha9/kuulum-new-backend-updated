<?php $locale=$this->Session->read('LocTrain.locale');
		


		if($locale == 'ar') 
		{
				echo $this->Html->css('uploadify_arabic');
		}
		else{
				echo $this->Html->css('uploadify');
		}
		foreach($language as $lang) {if($lang['Language']['locale']==$locale){  $current_loc= $lang['Language']['lang_code'];}}

if(isset($css) && $css=='style'){
$img_icon='icons';
}else{
$img_icon='icons_contrast';
}
		?>
<?php echo $this->Html->script('jquery.uploadify.js?ver='.rand(0,9999));?>
<?php echo $this->Html->script('/jwplayer/jwplayer');?>
<?php echo $this->Html->css('captivate.css');?>
<?php echo $this->Html->script('standard.js');?>
<script type="text/javascript">
function preventBack(){window.history.forward();}
var referrer = document.referrer;
 if(!referrer.search('course_edit')){setTimeout("preventBack()", 0);}

    window.onunload=function(){null};
    $(document).ready(function(){

$('#l_title').on('blur',function(){
		var msg = '';
		var finalNameVal = $('#l_title').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_title").text(msg);
		  }
		  else {
			$("#err_title").text('');
		  }
	});
$('#l_description').on('blur',function(){
		var msg = '';
		var finalNameVal = $('#l_description').val();
		  if(finalNameVal == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_description").text(msg);
		  }
		  else {
			$("#err_description").text('');
		  }
	});


 $(".fileinput").on("change.bs.fileinput", function(e){
                 var image_val=$('#image').val();
                 var get_extn=image_val.split('.');
                    if(get_extn[1]=='png' || get_extn[1] =='jpeg' || get_extn[1]=='jpg' || get_extn[1]=='gif'){
                        
                    }else{
                        $('.fileinput').fileinput('clear');
                        alert('Please select valid image file'); 
                        return false;
                    }
                });
        
       /* var price_txt=  $("ul.dropdown-menu-price li a:first").text();
        var price_val=  $("ul.dropdown-menu-price li a:first").attr('value');
        $("#price_btn:first-child").text(price_txt).append('<span class="caret"></span>' );
        $("#price_btn:first-child").val(price_val);       
         $('#price').val(price_val);
       
        ///price
        
        $('.dropdown-menu-price li a').click(function(){
              $('#price').val($(this).attr('value'));
          $("#price_btn:first-child").text($(this).text()).append('<span class="caret"></span>' );
          $("#price_btn:first-child").val($(this).attr('value'));
        });
        */
        
    });
 
</script>

<?php echo $this->Html->script('front/jquery.price_format.2.0.js');?>

<style type="text/css">
.selectBoxArrow {
   cursor: pointer;
    float: left;
    margin-top: 10px;
    padding: 10px;
    position: absolute;
    right: 5px;
}
	.selectBoxInput{
		border:0px;
		padding-left:1px;
		height:16px;
		position:absolute;
		top:0px;
		left:0px; width: 100%;    padding: 0 12px;
		  background: none repeat scroll 0 0 #fff;
		  border: 1px solid #7f7f7f;
    border-radius: 4px;
      text-align: left;
    text-overflow: ellipsis;
    white-space: nowrap;
	}

	.selectBox{
		border:none;
		height:40px; width: 100% !important;
	
	}
	.selectBoxOptionContainer{
		 background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 4px;
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
    display: none;
    float: left;
		position:absolute;
	
		height:auto;
		
		left:0px;
		top:48px;
		visibility:hidden;
		overflow:auto;
		z-index:1000;
		 width: 100% !important;
	}
	.selectBoxIframe{
		position:absolute;
		background-color:#FFF;
		border:0px;
		z-index:999;
	}
	.selectBoxAnOption{
		 background: none repeat scroll 0 0 #fff ;
    color: #333 !important;
    display: block;
    font-family: "proxima-nova-n1","proxima-nova",sans-serif;
    font-size: 18px;
    font-style: normal;
    font-weight: 100;
    height: auto;
    line-height: 24px;
    margin: 0 !important;
    min-height: 24px;
    padding: 1px 15px !important;
    text-align: left;
    white-space: normal;
	}
	</style>
	<script type="text/javascript">
	
	// Path to arrow images
	var arrowImage = '<?php echo $this->webroot; ?>img/cart_arrow_down.png';	// Regular arrow
	var arrowImageOver ='<?php echo $this->webroot; ?>img/cart_arrow_down_black.png';	// Mouse over
	var arrowImageDown = '<?php echo $this->webroot; ?>img/cart_arrow_down.png';	// Mouse down

	
	var selectBoxIds = 0;
	var currentlyOpenedOptionBox = false;
	var editableSelect_activeArrow = false;
	

	
	function selectBox_switchImageUrl()
	{
		if(this.src.indexOf(arrowImage)>=0){
			this.src = this.src.replace(arrowImage,arrowImageOver);	
		}else{
			this.src = this.src.replace(arrowImageOver,arrowImage);
		}
		
		
	}
	
	function selectBox_showOptions()
	{
		if(editableSelect_activeArrow && editableSelect_activeArrow!=this){
			editableSelect_activeArrow.src = arrowImage;
			
		}
		editableSelect_activeArrow = this;
		
		var numId = this.id.replace(/[^\d]/g,'');
		var optionDiv = document.getElementById('selectBoxOptions' + numId);
		if(optionDiv.style.display=='block'){
			optionDiv.style.display='none';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='none';
			this.src = arrowImageOver;	
		}else{			
			optionDiv.style.display='block';
			if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + numId).style.display='block';
			this.src = arrowImageDown;	
			if(currentlyOpenedOptionBox && currentlyOpenedOptionBox!=optionDiv)currentlyOpenedOptionBox.style.display='none';	
			currentlyOpenedOptionBox= optionDiv;			
		}
	}
	
	function selectOptionValue()
	{
		var parentNode = this.parentNode.parentNode;
		var textInput = parentNode.getElementsByTagName('INPUT')[0];
		textInput.value = this.innerHTML.replace('&nbsp;',' ');	
		this.parentNode.style.display='none';	
		document.getElementById('arrowSelectBox' + parentNode.id.replace(/[^\d]/g,'')).src = arrowImageOver;
		
		if(navigator.userAgent.indexOf('MSIE')>=0)document.getElementById('selectBoxIframe' + parentNode.id.replace(/[^\d]/g,'')).style.display='none';
		
	}
	var activeOption;
	function highlightSelectBoxOption()
	{
		if(this.style.backgroundColor=='#316AC5'){
			this.style.backgroundColor='';
			this.style.color='';
		}else{
			this.style.backgroundColor='#316AC5';
			this.style.color='#FFF';			
		}	
		
		if(activeOption){
			activeOption.style.backgroundColor='';
			activeOption.style.color='';			
		}
		activeOption = this;
		
	}
	
	function createEditableSelect(dest)
	{

		dest.className='selectBoxInput';		
		var div = document.createElement('DIV');
		div.style.styleFloat = 'left';
		div.style.width = dest.offsetWidth + 16 + 'px';
		div.style.position = 'relative';
		div.id = 'selectBox' + selectBoxIds;
		var parent = dest.parentNode;
		parent.insertBefore(div,dest);
		div.appendChild(dest);	
		div.className='selectBox';
		div.style.zIndex = 1 - selectBoxIds;

		var img = document.createElement('IMG');
		img.src = arrowImage;
		img.className = 'selectBoxArrow';
		
		img.onmouseover = selectBox_switchImageUrl;
		img.onmouseout = selectBox_switchImageUrl;
		img.onclick = selectBox_showOptions;
		img.id = 'arrowSelectBox' + selectBoxIds;

		div.appendChild(img);
		
		var optionDiv = document.createElement('DIV');
		optionDiv.id = 'selectBoxOptions' + selectBoxIds;
		optionDiv.className='selectBoxOptionContainer';
		optionDiv.style.width = div.offsetWidth-2 + 'px';
		div.appendChild(optionDiv);
		
		if(navigator.userAgent.indexOf('MSIE')>=0){
			var iframe = document.createElement('<IFRAME src="about:blank" frameborder=0>');
			iframe.style.width = optionDiv.style.width;
			iframe.style.height = optionDiv.offsetHeight + 'px';
			iframe.style.display='none';
			iframe.id = 'selectBoxIframe' + selectBoxIds;
			div.appendChild(iframe);
		}
		
		if(dest.getAttribute('selectBoxOptions')){
			var options = dest.getAttribute('selectBoxOptions').split(';');
			var optionsTotalHeight = 0;
			var optionArray = new Array();
			for(var no=0;no<options.length;no++){
				var anOption = document.createElement('DIV');
				anOption.innerHTML = options[no];
				anOption.className='selectBoxAnOption';
				anOption.onclick = selectOptionValue;
				anOption.style.width = optionDiv.style.width.replace('px','') - 2 + 'px'; 
				anOption.onmouseover = highlightSelectBoxOption;
				optionDiv.appendChild(anOption);	
				optionsTotalHeight = optionsTotalHeight + anOption.offsetHeight;
				optionArray.push(anOption);
			}
			if(optionsTotalHeight > optionDiv.offsetHeight){				
				for(var no=0;no<optionArray.length;no++){
					optionArray[no].style.width = optionDiv.style.width.replace('px','') - 22 + 'px'; 	
				}	
			}		
			optionDiv.style.display='none';
			optionDiv.style.visibility='visible';
		}
		
		selectBoxIds = selectBoxIds + 1;
	}	
	
	</script>
<style>
.set_locale_reg
{
	min-height:340px    !important;
}
.custom-upload {
    position: relative;
    height: 40px;
    width: 350px;
    margin:0 0 10px 0;
}

.custom-upload input[type=file]
{
    outline:none;
    position: relative;
    text-align: right;    
    -moz-opacity:0 ;
    filter:alpha(opacity: 0);
    opacity: 0;
    z-index: 2;
    width:100%;
    height:100%;
    
}

.custom-upload .fake-file
{
    position: absolute;
    top: 0px;
	<?php if($locale == 'ar') { ?> 
	right:0px;
	<?php } else { ?>
    left: 0px;
	<?php } ?>
    width: 290px;
    padding: 0;
    margin: 0;
    z-index: 1;
    line-height: 100%;
}

.custom-upload .fake-file input.no_sele
{
	background:#FFFFFF;
    border: medium none;
    font-size: 16px;
    height: 40px;
    width: 190px;
}
.lsn_vd_btn
{
	margin-top:0px !important;
}
.regfld_txtcont a.video {
    color: #2877CC;
    float: left;
    font-size: 15px;
    font-weight: normal;
    margin-top: 8px;
    text-decoration: none;
    width: 100%;
	text-align:left;
}
.remove_video_out
{
	display:none;
}
</style>
<?php echo $this->Html->script('front/jasny-bootstrap.js');?>
<?php echo $this->Html->css('front/jasny-bootstrap.css');?>
<script type="text/javascript">

	$(document).ready(function(){

if($('#video_url').val() !==''){
$('#video').hide();
$('.lesson_photo').show();
$('.video_link').show();
$('.s_z_link').show();
$('.video_link').attr('href',$('#video_url').val());						
}
		$('#file_hints_a').hide();
		//hide and show of file hints
$('#file_hints_q').click(function(){
$('#file_hints_a').toggle();
});
		
		$("#remove_video").on('click',function(){	
				if($('#file_exe').val()=='zip'){
					alert('<?php echo __('you_can_remove_vedio_once_upload_is_done');?>');
					return false;
					}
			var lessonID = $("#new_id").val();
			$.ajax({
				url:ajax_url+'Members/remove_lesson_video/'+lessonID,
				success:function(resp){
					if($.trim(resp)== 'done'){
						$("#render_video").remove();
						$("#render_video_wrapper").remove();
						$("#video").prepend('<span class="lsn_vd" id="render_video"></span>');
						$(".remove_video_out").hide();
						$('.video_div_dez').css('border','none');
						$('.jamm').removeAttr('disabled');
						$('.sc_btns').css('color','#2D66B1');
					    $('.lesson_photo input').val(''); 
						$('.lesson_photo input[type=button]').val('<?php echo __("browse"); ?>');
						$('.lesson_photo').hide();
						$('.s_z_link').hide();
						$('.video_link').attr('href','javascript:void(0)');
						
					}
				}
			});	
		});	

		
		
	});
	<?php $timestamp = time();?>

$(function() {
			$('#file_upload_test').uploadify({
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
				},
				'swf'      : ajax_url+'uploadify/uploadify.swf',
				'uploader' : ajax_url+'Common/upload_video_test',
				'debug':true,
				'onUploadError' : function(file, errorCode, errorMsg) {
					alert("<?php echo __('error_file');?>"+file.name+"<?php echo __(' is_not_uploaded');?>");
				},
				'onUploadSuccess' : function(file, data, response) {
					alert('success');
				}
			});
		});

	$(function() {
setTimeout(function () {
		$('#file_upload').uploadify({				
			'formData'     : {	
				'id' : '<?php echo $this->Session->read('LocTrain.id');?>',
				'action' : '1',			
				'timestamp' : '<?php echo $timestamp;?>',
				'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
			},
			
			'onUploadStart' : function(file) {
				$("#file_upload").uploadify("settings", 'formData', {
					'newid' : $("#new_id").val(),'memid':$('#mem_id').val(),'title':$('#l_title').val(),'description':$('#l_description').val(),'course_id':$('#title').val()
					}
					
				);
				$('.jamm').attr('disabled',true);
				$('.sc_btns').css('color','#a5a5a5');
			},
			
			'swf'      : ajax_url+'uploadify/uploadify.swf',
			'uploader' : ajax_url+'Common/upload_video',
			'debug':false,
			'onUploadError' : function(file, errorCode, errorMsg) {
				alert("<?php echo __('error_file');?>"+file.name+"<?php echo __(' is_not_uploaded');?>");
			},
			'onUploadSuccess' : function(file, data, response) {
				//alert(data);//alert(data.indexOf('{'));
				str=data.indexOf('{');
				data=data.substr(str); 
				//alert(data.duration); return false;
				data = $.parseJSON(data);					
				if($.trim(data.status) == 'false'){
					alert('<?php echo __('allowable_video_file_formats_are_flash_video_flv_mp4_mp4_shockwave_flash_swf_and_archived_html5_zip') ;?>');
				}else if($.trim(data.status) == 'true'){
					$('.lesson_photo').hide();
					$(".remove_video_out").show();					
					$("#hiddenN").val(data.file);
					$("#new_id").val(data.id);
					$('#file_exe').val(data.extension);
					$('.jamm').removeAttr('disabled');
					$('.video_div_dez').css('border','2px solid #A9CBF8');
					$('.sc_btns').css('color','#2D66B1');
					if(data.extension=="zip")
					{
                        $('#video').hide(); 
						$('.lesson_photo').show();
						$('.lesson_photo').show();
						
						
						$.ajax({
							 url:ajax_url+'Home/extzip/'+data.file+'.zip',
							 beforeSend: function() {
								$(".video-loading").css("display","block");						
							},
							 success:function(msg)
							 {
                                if($.trim(msg)!='')
							    {
								  /* $('.s_z_link').show();
									$m_id = '<?php echo base64_encode(convert_uuencode($this->Session->read('LocTrain.id'))); ?>';
									$('.video_link').attr('href',ajax_url+'Home/play_video_swf/'+data.file+'/'+data.extension+'/'+$m_id);
							*/
									$(".video-loading").css("display","none");
								   $('.add_video_out').css("display","none");$('.remove_video_out').css("display","none");
								   $('.file_hints').css("display","none");								  
								   $('#span_msg').css("display","block");		
							   }
							}
						});
                    }
					
					else if(data.extension=="swf")
					{  
						$('#video').hide();
						$('.lesson_photo').show();
						$('.video_link').show();
						$('.s_z_link').show();
						$m_id = '<?php echo base64_encode(convert_uuencode($this->Session->read('LocTrain.id'))); ?>';
						//$('.video_link').attr('href','https://s3.amazonaws.com/<?php echo LESSON_VIDEOS_BUCKET; ?>/<?php echo $this->Session->read('LocTrain.id'); ?>/'+data.file+'.'+data.extension);
						$('.video_link').attr('href',ajax_url+'Home/play_video_swf/'+data.file+'/'+data.extension+'/'+$m_id);						$('#video_url').val(ajax_url+'Home/play_video_swf/'+data.file+'/'+data.extension+'/'+$m_id);
						var strURLFull = window.document.location.toString();
						var intTemp = strURLFull.indexOf("?");
						var	strURLParams = "";
						if(intTemp != -1)
						{
							strURLParams = strURLFull.substring(intTemp + 1, strURLFull.length);
						}
						var so = new SWFObject('https://s3.amazonaws.com/<?php echo LESSON_VIDEOS_BUCKET; ?>/<?php echo $this->Session->read('LocTrain.id'); ?>/'+data.file+'.'+data.extension, "Captivate", "425px", "338px", "10", "#CCCCCC");
						so.addParam("quality", "high");
						so.addParam("name", "Captivate");
						so.addParam("id", "Captivate");
						so.addParam("wmode", "window");
						so.addParam("bgcolor","#f5f4f1");
						so.addParam("seamlesstabbing","true");
						so.addParam("menu", "false");
						so.addParam("AllowScriptAccess","always");
						so.addVariable("variable1", "value1");
						if(strURLParams != "")
						{
							so.addVariable("flashvars",strURLParams);
						}
						so.setAttribute("redirectUrl", "http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash");
						so.write("render_video");
						
						document.getElementById('Captivate').focus();
						document.Captivate.focus();
						
						
					} else {
						$('.video_link').hide();
						$('#video').show();	
						jwplayer("render_video").setup({
							//file: ajax_url+"/files/members/<?php echo $this->Session->read('LocTrain.id');?>/"+data.file+'.'+data.extension,
							file:'https://s3.amazonaws.com/<?php echo LESSON_VIDEOS_BUCKET; ?>/<?php echo $this->Session->read('LocTrain.id'); ?>/'+data.file+'.'+data.extension,
							height:'270',					
							//image: ajax_url+"/files/members/<?php echo $this->Session->read('LocTrain.id');?>/"+data.file+'_thumb.png',
							image :'https://s3.amazonaws.com/<?php echo LESSON_VIDEOS_BUCKET; ?>/<?php echo $this->Session->read('LocTrain.id'); ?>/'+data.file+'_thumb.png',
							width:'100%'
						}); 
						
						// Prevent form submission in IE AND CHROME
						jQuery(document).on('click', '.jwplayer button', function(event) { event.preventDefault(); });
						
						}
					
				}
			}				
		});},0);
		$('#file_upload-button').css('width','111px');
 //$('.uploadify-button-text').attr('style','color:#0070c0 !important');
		$('.uploadify-button-text').text('<?php echo __('upload_video');  ?>');
		
	});	
	
</script>

<!-- main container section -->
<section>
  <div class="container InnerContent">
    <div class="row">
    <?php if($this->Session->read("LocTrain.flashMsg") != ''){ ?>
    	<div class="account-activated">	<?php echo __($this->Session->read("LocTrain.flashMsg")); ?></div>
	<?php $this->Session->delete("LocTrain.flashMsg");  } ?>
        <?php $course_id = $course_deatils['Course']['id']; ?>		
        <?php echo $this->Form->create('CourseLesson',array('id'=>'addLesson','enctype'=>"multipart/form-data",'url'=>array('controller'=>'Members','action'=>'lesson_add'))); ?>        	
        <?php echo $this->Form->input('course_id',array('type'=>'hidden','id'=>'title','value'=>$course_id)); ?> 
        <?php echo $this->Form->input('language_id',array('type'=>'hidden','id'=>'language','value'=>$course_deatils['Course']['primary_lang'])); ?> 	
        <?php  echo $this->Form->input('newid',array('type'=>'hidden','id'=>'new_id','value'=>''));?>
        <?php  echo $this->Form->input('file_exe',array('type'=>'hidden','id'=>'file_exe','value'=>''));?>
<?php echo $this->Form->input('memid',array('type'=>'hidden','id'=>'mem_id','value'=>$this->Session->read('LocTrain.id'))); ?>  
    <div class="col-lg-7 col-md-7 col-sm-7">
	
    <div class="SmallLinkBoxWrapper SmallLinkBoxWrapper1 SmallLinksWrapper">   
            <div class="SmallLinkBox">
                 <?php echo $this->Form->button($this->Html->image('front/'.$img_icon.'/icon_upload_cloud.svg',array('class'=>'','width'=>57,'height'=>57)),array('type'=>'submit','escape'=>false,'class'=>'sc_btns btn_sbmt jamm disableAddLessonBut','div'=>false,'onclick'=>"return ajax_form('addLesson','Members/validate_lesson_ajax','load_top','disableAddLessonBut')"));  ?>
                <span class="Link"><?php echo $this->Form->button(__('save_changes_'),array('type'=>'submit','escape'=>false,'class'=>'sc_btns btn_sbmt jamm disableAddLessonBut','div'=>false,'onclick'=>"return ajax_form('addLesson','Members/validate_lesson_ajax','load_top','disableAddLessonBut')"));  ?></span>
            </div>
               <div class="SmallLinkBox">
                <?php 
                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_revert.svg',array('class'=>'','width'=>57,'height'=>57)),array('action'=>'course_edit',$c_id),array('escape'=>false,'class'=>'scn_link scn_margn disableAddLessonBut','div'=>'scn_link scn_margn')); ?>

                <span class="Link"><?php echo $this->Html->link(__('cancel_changes_'),array('action'=>'course_edit',$c_id),array('escape'=>false,'class'=>'scn_link scn_margn disableAddLessonBut','div'=>'scn_link scn_margn')); ?></span>
            </div>
        <div class="clearfix"></div>
    </div>
        
  <form role="form">
 <?php echo $this->Form->input('video_url',array('type'=>'hidden','maxlength'=>250,'id'=>'video_url',"placeholder"=>__(''),'class'=>'form-control','label'=>false,'autofocus'=>'','div'=>false)); ?> 
  <div class="form-group">
    <label for="l_title">*<?php echo __('lesson_title')?></label>
        <?php echo $this->Form->input('title',array('type'=>'text','maxlength'=>250,'id'=>'l_title',"placeholder"=>__(''),'class'=>'form-control','label'=>false,'autofocus'=>'','div'=>false)); ?> 
        <div id="err_title" class="register_error alert-danger"> <?php if(isset($error['title'][0])) echo $error['title'][0];?> </div> 
   
  </div>
      	<!--start uploading link video-->
    <div class="form-group">
    <label for="exampleInputPassword1">Video</label>
    <p id="span_msg" style="display:none;" class="cover_text"><?php echo __("lesson_Video_upload_is_in_process_we_will_let_you_know_once_the_video_has_been_successfully_uploaded");?></p>
      <div class="video_div_dez" id="video">					
                <span class="lsn_vd" id="render_video">

                </span>
        </div>
        <div align="center" class="video-loading" style="display:none">
			
			<?php echo $this->Html->image('front/ajax-loader.gif'); ?> 
		
		</div>
     <div class="reg_flds s_z_link" style="display:none">             
              <span class="regfld_txtcont">
				<a href="javascript:void(0)" class="video video_link"><?php echo $this->Html->image('front/cover_video.png',array('class'=>'img-responsive')); ?> </a>
              </span> 
    </div>
     
    
    <span class="Link add_video_out"><?php  echo $this->Html->link( __("upload_video"),'javascript:void(0)',array('id'=>'file_upload','escape'=>false)); ?></span>
    <span class="Link remove_video_out"> <?php echo $this->Html->link(__('remove_video'),'javascript:void(0)',array('id'=>'remove_video','escape'=>false)); ?> </span>                   
      <div class="clearfix"></div>
    <div class="file_hints">
<div id="file_hints_q"><?php echo __("which_vedio_file_formats_are_supported");?></div>
<div id="file_hints_a"> <?php echo __('you_can_upload_the_following_video_file_formats_Non-interactive_MP4_mp4_Non-interactive_Flash Vedio_Interactive_Shockwave_Flash_.swf_br_Interactive_HTML5_Archived_zip');?></div>  </div>                 
   <!--start uploading link video-->
   <div class="process"> </div>
   
    <!--end uploading link video-->
    <div id="err_video" class="register_error"> <?php if(isset($error['video'][0])) echo $error['video'][0];?> </div>
   </div>
			<!--end uploading link video-->
     
     	<!--end uploading link video-->
     
   <div class="form-group">
    <label for="l_description">*<?php echo __('description')?></label>
    <?php echo $this->Form->input('description',array('type'=>'textarea','id'=>'l_description',"row"=>3,'class'=>'form-control','label'=>false,'div'=>false)); ?> 
    <div id="err_description" class="register_error alert-danger"> <?php if(isset($error['description'][0])) echo $error['description'][0];?> </div> 
     
  </div>
  <div class="form-group">  	  
    <label for="exampleInputPassword1"><?php echo __('price'); ?></label>
      <?php $priceStr='';   
      foreach($lesson_prices as $key=>$val) { 
      $priceStr=$priceStr.$this->Utility->externlizePrice($language,$locale,$val).';';
		  } 
		  
		  ?> 
    <?php echo $this->Form->input('price',array('type'=>'text','id'=>'price','selectBoxOptions'=>$priceStr,'class'=>'form-control','value'=>$this->Utility->externlizePrice($language,$locale,'0.00'),'label'=>false,'div'=>false)); ?>                    
         <div id="err_price" class="register_error alert-danger"> <?php if(isset($error['price'][0])) echo $error['price'][0];?> </div> 
   
     
  </div>
  
  
  <div class="form-group">
     <label><?php echo __('cover_image');?></label>
     <p class="cover_text"><?php echo __('we_will_try_to_create_a_cover_image_for_your_lesson_please_upload_a_new_image_if_you_prefer_your_own_cover');?></p>
     
    <div class="fileinput fileinput-new coverImgWrapper" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 652px; height: 329px;" style="display:none;">
            <?php echo $this->Html->image('front/cover_photo.png',array('alt'=>'','class'=>'img-responsive')); ?> 
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 652px; max-height: 329px;">
             <?php echo $this->Html->image('front/cover_photo.png',array('alt'=>'','class'=>'img-responsive')); ?> 
            </div>
            <div>
              <span class="btn btn-default btn-file"><span class="fileinput-new"><?php echo __("upload_photo");?></span><span class="fileinput-exists"><?php echo __("upload_photo");?></span>
                  <?php echo $this->Form->input('image',array('name'=>'image','type'=>'file','id'=>'image','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('picture',array('type'=>'hidden','id'=>'file_hidden','label'=>false,'div'=>false)); ?>
              </span>
              <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo __('remove_photo');?></a>
            </div>
          </div> 
  
  </div>
 
</form>
    
    
    
    </div>
    
    <div class="col-lg-5 col-md-5 col-sm-5 text-center ProfileSection"> 
    	<div class="SmallLinkBoxWrapper SmallLinkBoxWrapper2 smalllinkboxbg">   
            <div class="SmallLinkBox">
                 <?php echo $this->Form->button($this->Html->image('front/'.$img_icon.'/icon_upload_cloud.svg',array('class'=>'','width'=>57,'height'=>57)),array('type'=>'submit','escape'=>false,'class'=>'sc_btns btn_sbmt jamm disableAddLessonBut','div'=>false,'onclick'=>"return ajax_form('addLesson','Members/validate_lesson_ajax','load_top','disableAddLessonBut')"));  ?>
                <span class="Link"><?php echo $this->Form->button(__('save_changes_'),array('type'=>'submit','escape'=>false,'class'=>'sc_btns btn_sbmt jamm disableAddLessonBut','div'=>false,'onclick'=>"return ajax_form('addLesson','Members/validate_lesson_ajax','load_top','disableAddLessonBut')"));  ?></span>
            </div>
              <div class="SmallLinkBox">
                <?php 
                echo $this->Html->link($this->Html->image('front/'.$img_icon.'/icon_revert.svg',array('class'=>'','width'=>57,'height'=>57)),array('action'=>'course_edit',$c_id),array('escape'=>false,'class'=>'scn_link scn_margn disableAddLessonBut','div'=>'scn_link scn_margn')); ?>

                <span class="Link"><?php echo $this->Html->link(__('cancel_changes_'),array('action'=>'course_edit',$c_id),array('escape'=>false,'class'=>'scn_link scn_margn disableAddLessonBut','div'=>'scn_link scn_margn')); ?></span>
            </div>
                <?php echo $this->Form->end(); ?>
            <div class="clearfix"></div>
        </div>
    
    

    
        
    </div>
    
    
    
    </div>
  </div>
  
</section>
<script type="text/javascript">
createEditableSelect(document.forms['addLesson'].price);
function str_replace(replace, by, str) {
    for (var i=0; i<replace.length; i++) {
        str = str.replace(new RegExp(replace[i], "g"), by[i]);
    }
  
    return str;
}
function to_numbers (str,limit)
			{var is_number = /[0-9]/;
				var formatted = '';
				for (var i=0;i<(str.length);i++)
				{
					char_ = str.charAt(i);
					if (formatted.length==0 && char_==0) char_ = false;

					if (char_ && char_.match(is_number))
					{
						/*if (limit)
						{
							if (formatted.length < limit) formatted = formatted+char_;
						}
						else
						{
							formatted = formatted+char_;
						}*/
						formatted = formatted+char_;
					}
				}

				return formatted;
			}
			function fill_with_zeroes (str)
			{var centsLimit=2;
			 if(str.length >1){
				while (str.length<(centsLimit+2)) str = str+'0';
				}else{
					while (str.length<(centsLimit+1)) str = str+'0';
					}
								return str;
			}
			function fill_with_decimal(str){
				var centsLimit=2;
				while (str.length<(centsLimit)) str = str+'0';
				return str;
				
				}
var format = function(num,locale,pre,suf,cenSep,thouSep){	var centsLimit=2;	
	if(pre == ''){
		var str = num.toString().replace(suf, ""), parts = false, output = [], i = 1, formatted = null;
		}else{
			var str = num.toString().replace(pre, ""), parts = false, output = [], i = 1, formatted = null;
		}
		if(str.indexOf(cenSep) > 0){
			partsw = str.split(cenSep);
			str=((to_numbers(partsw[0]))?to_numbers(partsw[0]):0)+cenSep+fill_with_decimal(to_numbers(partsw[1]));
		}else{
		/*str=fill_with_zeroes(to_numbers(str,6));		
		var centsVal = str.substr(str.length-centsLimit,centsLimit);
		var integerVal = str.substr(0,str.length-centsLimit);
		str =integerVal+cenSep+centsVal;*/
		partsw=(to_numbers(str,6))?to_numbers(str,6):0;
		str=partsw+cenSep+'00';
		}	
	if(str.indexOf(cenSep) > 0) {
		parts = str.split(cenSep);
		str = parts[0];
	}
	str = str.split("").reverse();
	for(var j = 0, len = str.length; j < len; j++) {
		if(str[j] != thouSep) {
			output.push(str[j]);
			if(i%3 == 0 && j < (len - 1)) {
				output.push(thouSep);
			}
			i++;
		}
	}
	formatted = output.reverse().join("");
	if(pre == ''){
		return(formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : "")+' '+suf);
	}else {
	return(pre + formatted + ((parts) ? cenSep + parts[1].substr(0, 2) : ""));
	}
};
var formatCheck=function(price){
		var replace = new Array("[\$]", "[\€]", "[¤]",'\,');
		var by = new Array("", "", "","");
		var new_price=$.trim(str_replace(replace, by, price));
		var msg = '';				
		if(new_price.length<3 || new_price.length>7){
			 msg = '<?php echo __("enter_a_number_between_000_and_999999");?>';
		  	  $("#err_price").text(msg);
			}else {
			$("#err_price").text('');
		  }
};

$('document').ready(function(){

	  var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
	specialKeys.push(37); //Home
	specialKeys.push(39); //Home
$("#price").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode;
                var ret = ((keyCode >= 48 && keyCode <= 57) || keyCode==190 || keyCode == 44 ||( specialKeys.indexOf(keyCode) != -1 && e.charCode != 37 && e.charCode != 39));                
                return ret;
		
            });
	$('#price').blur(function(){
		
		var locale='<?php echo $this->Session->read('LocTrain.locale');?>';
	
		if(locale=='en'){
			 $(this).val(format($(this).val(),locale,'$','','.',','));
					
		}else if (locale=='de'){
			$(this).val(format($(this).val(),locale,'',' $',',','.'));	
			
		}else if (locale=="es"){
			$(this).val(format($(this).val(),locale,'$','',',','.'));
			
		}else if (locale=='zh'){	
			 $(this).val(format($(this).val(),locale,'$','','.',','));	
			
		}else {
			 $(this).val(format($(this).val(),locale,'$','','.',','));	
			
		}
		 formatCheck($(this).val());	

		});
		<?php
	     $fmt = new NumberFormatter( $current_loc, NumberFormatter::DECIMAL_ALWAYS_SHOWN );

	?>
	var price_val='<?php echo $fmt->format(0.00); ?>';
	$( ".selectBoxAnOption" ).each(function() {		
		if(price_val==$(this).html()){
		$(this).css('background','#316AC5');
	}
	});
	});
	
</script>
