<?php echo '';?>
<div class="VideoLessonBox_<?php echo $lesson_highlight['CourseLesson']['id']?>">
<div class="embed-responsive embed-responsive-16by9 VideoLessonBox">
<?php $videoCdnURL = $this->Utility->bucketsCloudFront(LESSON_VIDEOS_BUCKET); ?>
<?php 
$locale = $this->Session->read('LocTrain.locale');
if(isset($css) && $css=='style'){
$img_icon='icons';
}else{
$img_icon='icons_contrast';
}
?>
<style>
    .vwcrsply_btn {
    top: 29%;
}
#CaptivateContent { 
	text-align:center;
	height:100%;
}
</style>
    <span class="lsn_para"  id="render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>">
        <?php
		  if($lesson_highlight['CourseLesson']['video'] != ''){
			  $video = $lesson_highlight['CourseLesson']['video'];
		  }else{
			  $video='';
		  }
        
		if(!empty($lesson_highlight['CourseLesson']['image']) &&  $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$lesson_highlight['CourseLesson']['image']))
		{
				echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'medium/'.$lesson_highlight['CourseLesson']['image']),array( 'class'=>'img-responsive lesson_vedio_img','width'=>'','height'=>''));

		}else{
				echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('width'=>'','height'=>'','class'=>'img-responsive lesson_vedio_img'));
		}
  
		?>
		<?php 
			 $extension = $lesson_highlight['CourseLesson']['extension'] ; 	
			 $owner_id = base64_encode(convert_uuencode($lesson_highlight['Course']['m_id']));
			 $video = $lesson_highlight['CourseLesson']['video'] ; 	
			 $m_id = $this->Session->read('LocTrain.id');
			$ulogin=0;
					if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}
			if($m_id == $lesson_highlight['Course']['m_id']) 
			{
			 
				if($extension=="swf")
			    { 
				?>		
					<div class="vwcrsply_btn" >
						<a class="ply_btn_anchr " href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a>
					</div>
			<?php	}elseif($extension=="zip")
					{ 
			?>		
					<div class="vwcrsply_btn " >					
					<a class="ply_btn_anchr " href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a>
					</div>
		<?php	}
		else {  if($lesson_highlight['CourseLesson']['video'] != ''){?>	
					
					<div class="vwcrsply_btn ">
						<a class="ply_btn_anchr ply_btn_anchr2" href="javascript:void(0);"><i class="fa fa-play"></i></a>
					</div>	

		<?php }
				}
			}else if(trim($lesson_highlight['CourseLesson']['price'])!=0.00 && $ulogin==0 )
			{		
				if($ulogin==0 ){?>
					<div class="vwcrsply_btn ">
						<a class="ply_btn_anchr ply_btn_anchr2 free_course_play_less" href="javascript:void(0);"><i class="fa fa-play"></i></a>
				</div>
				<?php }else{	}
			}else if(trim($lesson_highlight['CourseLesson']['price'])==0.00)
			{  	$ulogin=0;

					if($this->Session->read('LocTrain.id'))
						{
						$ulogin=1;
						}if($ulogin==0){ ?>
						<div class="vwcrsply_btn ">
						<a class="ply_btn_anchr ply_btn_anchr2 free_course_play_less" href="javascript:void(0);"><i class="fa fa-play"></i></a>
				</div>
				
			<?php	}else{
				if($extension!="swf" && $extension!="zip"){
					  if($lesson_highlight['CourseLesson']['video'] != ''){
		?>
				<div class="vwcrsply_btn ">
						<a class="ply_btn_anchr ply_btn_anchr2" href="javascript:void(0);"><i class="fa fa-play"></i></a>
				</div>
				
		<?php	} }else { 
			 if($extension=="swf"){ ?>
		
				<div class="vwcrsply_btn ">
						<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a>
				</div>
		
		<?php    }
		
		else if($extension=="zip"){ ?>
		
				<div class="vwcrsply_btn ">								
						<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a>
				</div>
		
		<?php    }
		 }
			} }else {   
				if(!$subscribed)
				{
					if($video != '' && $extension!="swf" && $extension!="zip")
					{  if($lesson_highlight['CourseLesson']['video'] != ''){
												if(!empty($subscription))
						{
		?>
						<div class="vwcrsply_btn ">
								<a class="ply_btn_anchr ply_btn_anchr2" href="javascript:void(0);"><i class="fa fa-play"></i></a>
						</div>
				
		<?php		}else{ ?>
			<div class="vwcrsply_btn ">
								<a class="ply_btn_anchr" href="javascript:void(0);"><i class="fa fa-play"></i></a>
						</div>
			
			<?php } } }else
					{
						
						if($extension=="swf")
						{
						
						
						if(!empty($subscription))
						{
		?>	
						<div class="vwcrsply_btn ">
							<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a>
						</div>
						
		<?php			}else
						{ if($lesson_highlight['CourseLesson']['video'] != ''){
		?>
						<div class="vwcrsply_btn ">
								<a class="ply_btn_anchr ply_btn_anchr2" href="javascript:void(0);"><i class="fa fa-play"></i></a>
						</div>	
		<?php			} }
				}
				elseif($extension=="zip")
						{
						
						
						if(!empty($subscription))
						{
		?>	
						<div class="vwcrsply_btn ">
										<!--<a class="ply_btn_anchr " href='https://s3.amazonaws.com/<?php echo LESSON_VIDEOS_BUCKET; ?>/<?php echo convert_uudecode(base64_decode($owner_id)); ?>/<?php echo $video; ?>/index.html'><i class="fa fa-play"></i></a>--><a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a></div>
						
		<?php			}else
						{ if($lesson_highlight['CourseLesson']['video'] != ''){
		?>
						<div class="vwcrsply_btn ">
								<a class="ply_btn_anchr ply_btn_anchr2" href="javascript:void(0);"><i class="fa fa-play"></i></a>
						</div>	
		<?php			} }
	}
		
	
	
	
					}
				}else {
					
					if($extension!="swf" && $extension!="zip"){ 
						
						
						if($lesson_highlight['CourseLesson']['video'] != ''){
					
		?>
					<div class="vwcrsply_btn ">
						<a class="ply_btn_anchr ply_btn_anchr2" href="javascript:void(0);"><i class="fa fa-play"></i></a>
					</div>	
				
		<?php 	 }  }else {
			
			if($extension=="swf"){ 
		?>
					<div class="vwcrsply_btn ">
							<a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a>
					</div>
					
		<?php		}
		elseif($extension=="zip"){ 
		?>
					<div class="vwcrsply_btn ">
									<!--	<a class="ply_btn_anchr " href='https://s3.amazonaws.com/<?php echo LESSON_VIDEOS_BUCKET; ?>/<?php echo convert_uudecode(base64_decode($owner_id)); ?>/<?php echo $video; ?>/index.html'><i class="fa fa-play"></i></a>--><a class="ply_btn_anchr" href="<?php echo HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ?>"><i class="fa fa-play"></i></a></div>
					
		<?php		}
		
			      }
			   }
			}		
		?>
		
		
    </span>	
    
    </div>
    <p><?php echo $lesson_highlight['CourseLesson']['description'];?></p>
<div class="row LessonPurchaseBox">
    <div class="col-sm-6">
        <div class="Price">
			<?php 
				if((!empty($subscription) || in_array($lesson_highlight['CourseLesson']['id'],$cookie_lessons)||  in_array($course_details['Course']['id'],$cookie_courses) )&&  $this->Session->read('LocTrain.id')){
					}else{
						if($lesson_highlight['CourseLesson']['price']=='0.00'){
							$class = !empty($course_details['CoursePurchaseList'])?'c_added_learning_dsbl':'vw_scn_link bg_ad add_learning_sub';
							if($class != 'c_added_learning_dsbl'){
							echo __('free');
							}else{
								//echo __('purchased');
							}
						}else{
						echo $this->Utility->externlizePrice($language,$locale,$lesson_highlight['CourseLesson']['price']);
						}
					}?>
		</div>
		<?php if((!empty($subscription) ||$my_course|| in_array($lesson_highlight['CourseLesson']['id'],$cookie_lessons)||  in_array($course_details['Course']['id'],$cookie_courses))&&  $this->Session->read('LocTrain.id')){
			}else{
				if($lesson_highlight['CourseLesson']['price']=='0.00'){}else{?>
				<p><?php echo __('you_may_purchase_this_as_a_separate_item_instead_of_the_whole_course'); ?></p>
				<?php } 
			} ?>
		</div>
		<div class="col-sm-6">
			<div class="SmallLinkBoxWrapper SmallLinkBoxWrapper2 text-center">   
				<div class="SmallLinkBox" style="  width: 25%;">
					<!-- Conditions start for add to cart/pay lesson -->
					<!-- check user is logged in or not-->
						<?php 
						$class='';$url ='';
						$uID=base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']));
						$uclass='';$title='';
						if($this->Session->read('LocTrain.id'))
								{	//user logged in 
									$uclass='course_play_less_log';
									$class = (in_array($lesson_highlight['CourseLesson']['id'],$cookie_lessons) || $course_in_cart==1)?'vw_scn_link bg_ad_basket added_learning_sub '.$uclass:'vw_scn_link bg_ad add_to_cart '.$uclass;
									$url = (in_array($lesson_highlight['CourseLesson']['id'],$cookie_lessons)|| $course_in_cart==1)?'javascript:void(0);':array('controller'=>'Home','action'=>'add_lesson_to_cart',base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id'])));
											//1.if lesson's author id & login member id same then he can play the lesson
											if($my_course) {//the login member and created memebr are smae so he can play the lesson
														if($video != '')
														{
															if($extension=="swf" || $extension=="zip" ){
																$rel=__($extension);
																$url=HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'];
																$class="ply_btn_anchr2";
															}else{
																$rel=__((!empty($subscription))?'watch_now':'preview');
																$url="javascript:void(0);";
																$class="ply_btn_anchr2";
															}	
														}else{
															$rel=__((!empty($subscription))?'watch_now':'preview');
															$url="javascript:void(0);";
															$title=__('preview_is_not_available_for_interactive_videos');
															$class="vw_scn_link bg_wch bg_wch2 vw_scn_link_disable bg_wch_preview";
														}															
													echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class,'title'=>$title,'rel'=>$rel,'escape'=>false));
													echo $this->Html->link(__('play_lesson_'),$url,array('class'=>$class,'rel'=>$rel,'title'=>$title,'escape'=>false));
											}else if($lesson_highlight['CourseLesson']['price']=='0.00'){//2.if lesson is free he can also play the lesson
												if($video != '')
														{
															if($extension=="swf" || $extension=="zip" ){$rel=__($extension);
																$url=HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'];
																$class="ply_btn_anchr2";
															}else{$rel=__((!empty($subscription))?'watch_now':'preview');
																$url="javascript:void(0);";
																$class="ply_btn_anchr2";
															}
														}else{
															$rel=__((!empty($subscription))?'watch_now':'preview');
															$url="javascript:void(0);";
															$title=__('preview_is_not_available_for_interactive_videos');
															$class="vw_scn_link bg_wch bg_wch2 vw_scn_link_disable bg_wch_preview";
														}																	
													echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class,'title'=>$title,'rel'=>$rel,'escape'=>false));
													echo $this->Html->link(__('play_lesson_'),$url,array('class'=>$class,'rel'=>$rel,'title'=>$title,'escape'=>false));
											}else if(!empty($subscription) && $lesson_highlight['CourseLesson']['price']!='0.00'){//3.if lesson is not free and sold he can also play the lesson
												if($video != '')
														{
															if($extension=="swf" || $extension=="zip" ){$rel=__($extension);
																$url=HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'];
																$class="ply_btn_anchr2";
															}else{$rel=__((!empty($subscription))?'watch_now':'preview');
																$url="javascript:void(0);";
																$class="ply_btn_anchr2";
															}	
														}else{
															$rel=__((!empty($subscription))?'watch_now':'preview');
															$url="javascript:void(0);";
															$title=__('preview_is_not_available_for_interactive_videos');
															$class="vw_scn_link bg_wch bg_wch2 vw_scn_link_disable bg_wch_preview";
														}																
													echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class,'title'=>$title,'rel'=>$rel,'escape'=>false));
													echo $this->Html->link(__('play_lesson_'),$url,array('class'=>$class,'rel'=>$rel,'title'=>$title,'escape'=>false));	
											}else{
												
												if(!empty($subscription))
												{
													$class='vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
													$url='javascript:void(0);';
												}
												if(count($course_details['CourseLesson'])==1){// if lesson is one in course then add full course
													$url=array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($lesson_highlight['Course']['id'])));
												}
												echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_add_to_cart.png',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class.' prevent_click','id'=>$uID,'escape'=>false));
												echo $this->Html->link(__('add_to_shopping_basket'),$url,array('class'=>$class.' prevent_click','id'=>$uID));
												
											}
									
								}else{// user not logged in
									$uclass='free_course_play_less_log';
									//check first lesson is free then we ask user to login for play the lesson
										if($lesson_highlight['CourseLesson']['price']=='0.00'){//lesson free
											echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_play_course.png',array('width'=>'57','height'=>'57','alt'=>'')),'javascript:void(0);',array('class'=>'vw_scn_link bg_ad_basket added_learning_sub free_course_play_less','escape'=>false));
											echo $this->Html->link(__('play_lesson_'),'javascript:void(0);',array('class'=>'vw_scn_link bg_ad_basket added_learning_sub free_course_play_less','escape'=>false));
										}else{//lesson is not free the we ask for add to cart
											//checkin cookies for lesson is added or not
											$class = (in_array($lesson_highlight['CourseLesson']['id'],$cookie_lessons) || $course_in_cart==1)?'vw_scn_link bg_ad_basket added_learning_sub '.$uclass:'vw_scn_link bg_ad add_to_cart '.$uclass;
											$url = (in_array($lesson_highlight['CourseLesson']['id'],$cookie_lessons)|| $course_in_cart==1)?'javascript:void(0);':array('controller'=>'Home','action'=>'add_lesson_to_cart',base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id'])));
											
											if(!empty($subscription))
											{
												$class='vw_scn_link bg_ad_basket added_learning_sub '.$uclass;
												$url='javascript:void(0);';
											}
											if(count($course_details['CourseLesson'])==1){// if lesson is one in course then add full course
												$url=array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($lesson_highlight['Course']['id'])));
											}
											echo $this->Html->link( $this->Html->image('front/'.$img_icon.'/icon_add_to_cart.png',array('width'=>'57','height'=>'57','alt'=>'')),$url,array('class'=>$class.' prevent_click','id'=>$uID,'escape'=>false));
											echo $this->Html->link(__('add_to_shopping_basket'),$url,array('class'=>$class.' prevent_click','id'=>$uID));
										}
									
									
								}
						?>
					
					<!-- Conditions end for add to cart/pay lesson -->
					
				</div>
				<div class="clearfix"></div>
			</div>
		</div>



<?php 
$extension1=$lesson_highlight['CourseLesson']['extension'];	
$owner_id = base64_encode(convert_uuencode($lesson_highlight['Course']['m_id']));
$video = $lesson_highlight['CourseLesson']['video'];	
if($my_course) 
{
        if($extension1!="swf" && $extension1!="zip"){ 

                echo $this->Html->link(__('watch_now'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch2',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
        }elseif($extension1=="swf")
        { 
        echo $this->Html->link(__('watch_now'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension1.'/'.$owner_id.'/'.$lesson_highlight['CourseLesson']['title']),array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
		}
		 elseif($extension1=="zip")
		{	
		 /*echo $this->Html->link(__('watch_now'),'https://s3.amazonaws.com/'.LESSON_VIDEOS_BUCKET.'/'.convert_uudecode(base64_decode($owner_id)).'/'.$video.'/index.html',array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));*/
 echo $this->Html->link(__('watch_now'),HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ,array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
		}									
}else if(trim($lesson_highlight['CourseLesson']['price'])==0.00)
{
        if($extension1!="swf" && $extension1!="zip"){

                echo $this->Html->link(__('watch_now'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch2',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
        } elseif($extension1=="swf")
        { 
        echo $this->Html->link(__('watch_now'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension1.'/'.$owner_id.'/'.$lesson_highlight['CourseLesson']['title']),array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
		}
		 elseif($extension1=="zip")
		{
		/* echo $this->Html->link(__('watch_now'),'https://s3.amazonaws.com/'.LESSON_VIDEOS_BUCKET.'/'.convert_uudecode(base64_decode($owner_id)).'/'.$video.'/index.html',array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));*/
echo $this->Html->link(__('watch_now'),HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ,array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
		}				

}
else
{
                if(!$subscribed)
                {    

                        if($video != '' && $extension1!="swf" && $extension1!="zip")
                        {
                                echo $this->Html->link(__((!empty($subscription))?'watch_now':'preview'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch2',"style"=>'display:none;','rel'=>(!empty($subscription))?'watch':'preview','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
                        }
                        else
                        { 
                                if(!empty($subscription))
                                {
                                      if($extension1=="swf")
								{ 
										echo $this->Html->link(__('watch_now'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension1.'/'.$owner_id.'/'.$lesson_highlight['CourseLesson']['title']),array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
								}
								elseif($extension1=="zip")
								{
										/*echo $this->Html->link(__('watch_now'),'https://s3.amazonaws.com/'.LESSON_VIDEOS_BUCKET.'/'.convert_uudecode(base64_decode($owner_id)).'/'.$video.'/index.html',array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));*/
echo $this->Html->link(__('watch_now'),HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ,array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
								}		
                                }else
                                { 
                                        echo $this->Html->link(__('preview'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch2 vw_scn_link_disable bg_wch_preview',"style"=>'display:none;','rel'=>'swf','title'=>__('preview_is_not_available_for_interactive_videos'),'id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));

                                }
                        }
                }
                else
                {
                        if($extension1!="swf" && $extension1!="zip"){

                                echo $this->Html->link(__('watch_now'),'javascript:void(0);',array('class'=>'vw_scn_link bg_wch bg_wch2',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
                        } elseif($extension1=="swf")
						{ 
						echo $this->Html->link(__('watch_now'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension1.'/'.$owner_id.'/'.$lesson_highlight['CourseLesson']['title']),array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
						}
						 elseif($extension1=="zip")
						{
						 /*echo $this->Html->link(__('watch_now'),'https://s3.amazonaws.com/'.LESSON_VIDEOS_BUCKET.'/'.convert_uudecode(base64_decode($owner_id)).'/'.$video.'/index.html',array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));*/
echo $this->Html->link(__('watch_now'),HTTP_ROOT.'home/play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$lesson_highlight['Course']['title'] ,array('class'=>'vw_scn_link bg_wch',"style"=>'display:none;','rel'=>'watch','id'=>base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id']))));
						}

                }
}
?>
<?php //echo $this->Html->css('captivate.css');?>
<?php echo $this->Html->script('standard.js');
echo $this->Html->script('/jwplayer/jwplayer');
 ?>
<?php echo $this->element('sql_dump');?>
<script type="text/javascript">




$('document').ready(function(){
   $('.show_link').hide();
 $('.openForgotForm').hide();
var modalId='';
$("div[id^='myModal_lesson_']").each(function(){
	
modalId=$(this).attr('id');
});
 $('.course_play_less_log').click(function(){
var lesson_len="<?php echo count($course_details['CourseLesson']);?>";
var checkBoxLength=$("input[type='checkbox'][name='add_lessons']:checked").length;
if(checkBoxLength==(lesson_len-1)){
	$('.course_play_less_log').attr('href',"<?php echo HTTP_ROOT ;?>Home/add_to_cart/<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>");

	}
});

$('.free_course_play_less_log').click(function(){
	var url=$(this).attr('href');
	$(this).attr('href','');
	 var myid=[];
	 var id=$(this).attr('id');
	  myid.push($(this).attr('id'));
if($(this).hasClass('added_learning_sub')){
	return false;
	}
			var lesson_len="<?php echo count($course_details['CourseLesson']);?>";
var checkBoxLength=$("input[type='checkbox'][name='add_lessons']:checked").length;
if(checkBoxLength==(lesson_len-1)){
	$('.course_play_less_log').attr('href',"<?php echo HTTP_ROOT ;?>Home/add_to_cart/<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>");


$.ajax({
					url:ajax_url+"Home/add_to_cart/<?php echo base64_encode(convert_uuencode($course_details['Course']['id']));?>",
					type: 'post',
					success:function(){
						$.ajax({
							url:ajax_url+"Home/findcookiepurchase/",
							
							success:function(index){
								var array=$.parseJSON(index);

$("input[type='checkbox'][name='add_lessons']").each(function(index){
											if($(this).val()==id)
												{
												$(this).prop("checked",true);
												$(this).prop("disabled",true);
												}
										});
									$('.free_course_play_less_log').addClass("added_learning_sub");
										$('.free_course_play_less_log').removeClass("add_to_cart");
										$(this).removeClass("free_course_play_less_log");
								$("#title-cart").html(array['courseslist'][0]['Course']['title']+" was added to your shopping basket. Go to the shopping basket to complete your order.");
								$(".counter_txt").html(array.countcart);
								$("#register-cart").modal('show');
									$("#check_lesson_basket").addClass("added_learning_sub");
									//$("#check_lesson_basket").unbind(event);
								
									$(".SmallLinkBoxWrapper2 .SmallLinkBox .add_to_cart").attr("id","class-check");
								$(".SmallLinkBoxWrapper2 .Link .add_to_cart").attr("id","class-check1");
								$("#class-check").removeClass("add_to_cart");
								$("#class-check").addClass("added_learning_sub");
								$("#class-check1").removeClass("add_to_cart");
								$("#class-check1").addClass("added_learning_sub");
								$("#class-check").unbind("click");
								$("#class-check1").unbind("click");
							$("input[type='checkbox'][name='add_lessons']:checked").not(":disabled").attr("disabled",true);
								
								}
							});
					
						}
					});



	}	else{				
								
								
								req = $.ajax({
				url:ajax_url+'Home/add_lesson_to_cart_ajax/',
				type: 'post',
				data:{"array":myid},
				success: function(resp){
				       
				         if(resp=='index'){
						
				       window.location.href="<?php echo HTTP_ROOT ;?>Home/register";   
				      }else{
						  var login="<?php echo $this->Session->read('LocTrain.login')?>";
				 if(login==1)
						{	 
							      window.location.href="<?php echo HTTP_ROOT ;?>Home/purchase";   
						}else
						{
				  $.ajax({
							url:ajax_url+"Home/findcookiepurchase/",
							
							success:function(index){
									var array=$.parseJSON(index);
									var values=[];
										console.log(array);
										var val=id;
										values.push(val);
										$('.free_course_play_less_log').addClass("added_learning_sub");
										$('.free_course_play_less_log').removeClass("add_to_cart");
										$(this).removeClass("free_course_play_less_log");
										//alert($("input[type='checkbox'][name='add_lessons']").length);
										
										$("input[type='checkbox'][name='add_lessons']").each(function(index){
											if($(this).val()==id)
												{
												$(this).prop("checked",true);
												$(this).prop("disabled",true);
												}
										});
										
										$("#title-cart").html("");
										$.each(values,function(index1){
											if(values.length==1)
											{
											$.ajax({
										url:ajax_url+"Home/coursetitle/"+values[index1],
										success:function(response){
										$("#title-cart").html("<p>"+response+" was added to your shopping basket. Go to the shopping basket to complete your order.</p>");
										
										}
										});
											}
											else
											{
										$("#title-cart").html("<p>Lessons were added to your shopping basket. Go to the shopping basket to complete your order.</p>");
											}
										
									});	
									
								$(".counter_txt").html(array.countcart);
								$("#register-cart").modal('show');
								
									if($("input[type='checkbox'][name='add_lessons']:checked").length==$("input[type='checkbox'][name='add_lessons']").length)
								{
									$("#check_lesson_basket").addClass("added_learning_sub");
									//$("#check_lesson_basket").unbind( event );
									//$("#check_lesson_basket").removeAttr("id");
										//$(".Link.addShopingViewBasket").html("<a href='javascript:void(0);' class='added_learning_sub'><?php echo __('add_selected_to_basket');?></a>");
										$(".SmallLinkBoxWrapper2 .SmallLinkBox .add_to_cart").attr("id","class-check");
								$(".SmallLinkBoxWrapper2 .Link .add_to_cart").attr("id","class-check1");
								$("#class-check").removeClass("add_to_cart");
								$("#class-check").addClass("added_learning_sub");
								$("#class-check1").removeClass("add_to_cart");
								$("#class-check1").addClass("added_learning_sub");
								$("#class-check").unbind("click");
								$("#class-check1").unbind("click");
								
								
								
									}								
								
								
								}
							});
					
						}	
				  }
				}
				});
				}			
			return false;
	
	});
		$('.free_course_play_less').click(function(){
if($('#foo').length >0){
	$('#foo').remove();
}
<?php 



if($lesson_highlight['CourseLesson']['price']!= 0.00){

	if($lesson_highlight['CourseLesson']['video'] != ''){
		$value=HTTP_ROOT.'home/view_course_details/'.base64_encode(convert_uuencode($lesson_highlight['Course']['id'])).'/'.base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id'])).'/yes'; 
		}else{
		$value=HTTP_ROOT.'home/view_course_details/'.base64_encode(convert_uuencode($lesson_highlight['Course']['id'])).'/no';
		}



}else{
			if($lesson_highlight['CourseLesson']['video'] != '' && ($lesson_highlight['CourseLesson']['extension'] =='zip' || $lesson_highlight['CourseLesson']['extension'] =='swf')){
		$value=HTTP_ROOT.'home/play_video_swf/'.$lesson_highlight['CourseLesson']['video'].'/'.$lesson_highlight['CourseLesson']['extension'].'/'.base64_encode(convert_uuencode($lesson_highlight['Course']['m_id'])).'/'.$lesson_highlight['Course']['title']; 
		}else if($lesson_highlight['CourseLesson']['video'] != ''){
		$value=HTTP_ROOT.'home/view_course_details/'.base64_encode(convert_uuencode($lesson_highlight['Course']['id'])).'/'.base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id'])).'/1'; 
		}else{
		$value=HTTP_ROOT.'home/view_course_details/'.base64_encode(convert_uuencode($lesson_highlight['Course']['id'])).'/'.base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id'])).'/0';
		}
}
?> 
			$('#myModal4').modal({show:true});
				$('.memberLogin_a4').attr('href','<?php echo HTTP_ROOT;?>Home/register/<?php echo base64_encode(convert_uuencode($lesson_highlight["Course"]["id"]));?>');
								$('<input>').attr({
								    type: 'hidden',
								    id: 'foo',
								    value:"<?php echo $value;?>",
								    name: 'redirectLesson'
								}).appendTo('form#memberLogin4');
							
			return false;
		});



});


var id = "";
id = "<?php echo $mem_acc_no; ?>";
 $(document).ready(function(){



	<?php if($lesson_highlight['CourseLesson']['video'] == ''){?>
	$('.ply_btn_anchr2').hide();
	<?php } ?>
	 
	 $(".bg_wch2 , .ply_btn_anchr2").on('click',function(){
		if($(this).hasClass('free_course_play_less')){
			
			return false;
		}
		//var getId = $('.bg_wch2').attr('id');
		var getId="<?php echo base64_encode(convert_uuencode($lesson_highlight['CourseLesson']['id'])); ?>";
		var a =$('.bg_wch2');
		if(a.attr('rel')=='swf')
		{
			return false;
		}
		$(".load_top").show();	
		
		var courseID=$('.course_id').attr('id');
		//alert(courseID); return false;
		if(a.attr('rel')=='watch')
		{
			
			var rt = $.ajax({
						url: ajax_url+"Home/is_course_subscribed/",
						type: "POST",
						data: {cors_id : courseID, less_id : getId},
						dataType: "html"
					  });
			rt.done(function(msg) {
					if(msg)
					{
						//alert(msg); return false;
						l_video ='watermark';
						//alert(l_video); return false;
					}
					else
					{
						l_video='preview';
					}
					
			req = $.ajax({
			url:ajax_url+'Home/play_lesson_video/'+getId,
			dataType: 'json',
			success: function(resp){
				console.log(resp);
				if(resp.status == 'error'){
					window.location.href = resp.url;
				}
				else if(resp.status == 'true')
				{	
				
					if(l_video=='watermark')
					{
							
						var request = $.ajax({
							url: ajax_url+"Home/course_watched/",
							type: "POST",
							data: {course_id : courseID},
							dataType: "html"
						  });
					}
						var id3 = "";
						
						id3 = "<?php  echo $mem_acc_no ? $mem_acc_no :$this->Session->read('LocTrain.visitor_id');  ?>"; 
						if(resp.extension == 'zip')
						{
						    $.ajax({
								url:ajax_url+'Home/ext_file/'+resp.video+'/'+resp.container,
								success:function(msg)
								{
									if($.trim(msg)!='')
									{
										//window.open(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/index.html','HTML5video','width=625,height=436');
									     window.open('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'/index.html','HTML5video','status=1');
									    return false; 
										/*$('#render_video_l').html('<iframe style="border:none;" width=425  height=322 src='+ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/index.html />');*/
									}
								}
						    });
							
						}
						
						else if(resp.extension != "swf" && resp.extension != "zip")
						{     
							console.log(resp.container);

//*****   For water mark text *****************										
						jwplayer("render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>").setup({
						file:"https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'.'+resp.extension,
						//file: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'.'+resp.extension,
								image: "https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'_thumb.png',
					 	//	image: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'_thumb.png',
							width:'100%',height:'360',
							logo:
							{
							hide: true
							},
							plugins:{
							"<?php echo ROOT_FOLDER;?>jwplayer/view_course.js":
							{				
							texto: id3 
							}
							}
							});
							jwplayer("render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>").play();
							//*****   For water mark text *****************	
							// Prevent form submission in IE AND CHROME
						    //jQuery(document).on('click', '.jwplayer button', function(event) { event.preventDefault(); });
						    setTimeout(function(){$('#render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>').css('border','1px solid #BEDAFF')},2000);
						}else if(resp.extension =="swf") {

							//alert(resp.extension);
							var strURLFull = window.document.location.toString();
							var intTemp = strURLFull.indexOf("?");
							var	strURLParams = "";
							if(intTemp != -1)
							{
								strURLParams = strURLFull.substring(intTemp + 1, strURLFull.length);
							}
							$("#render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>").css({'width':'62.5%'});
							//var so = new SWFObject(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'.'+resp.extension, "Captivate", "425px", "335px", "10", "#CCCCCC");
							var so = new SWFObject('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'.'+resp.extension, "Captivate", "425px", "338px", "10", "#CCCCCC");
							so.write("render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>");
						
							
						}						
				}
				else if(resp.status == 'nolesson'){
					alert(resp.error);
				}
				$(".load_top").hide();	
			}
		});
			
			
			
			
	});
		}
		else if(a.attr('rel')=='preview')
		{
			
			l_video='preview';
			
			req = $.ajax({
			url:ajax_url+'Home/play_lesson_video/'+getId,
			dataType: 'json',
			success: function(resp){
				if(resp.status == 'error'){
					window.location.href = resp.url;
				}
				else if(resp.status == 'true'){	
									

				//*****   For water mark text *****************						
						
                    var id2 = "";
					id2 = "<?php  echo $mem_acc_no ? $mem_acc_no :$this->Session->read('LocTrain.visitor_id');  ?>"; 
					   
					    if(resp.extension == 'zip')
						{
						    $.ajax({
								url:ajax_url+'Home/ext_file/'+resp.video,
								success:function(msg)
								{
									if($.trim(msg)!='')
									{
										//window.open(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/index.html','HTML5video','width=625,height=436');
										 window.open('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'/index.html','HTML5video','status=1');
									    return false; 
										/*$('#render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>').html('<iframe style="border:none;" width=425  height=322 src='+ajax_url+"/files/members/"+resp.container+"/"+resp.video+'/'+msg+'/index.htm />');*/
									}
								}
						    });
							
						}
					    else if(resp.extension != 'swf' && resp.extension != 'zip')
						{

//*****   For water mark text *****************										
						jwplayer("render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>").setup({

							file: "https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'_'+l_video+'.'+resp.extension,
                            //file: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'_'+l_video+'.'+resp.extension,	width:'100%',height:'360',					
							//image: ajax_url+"/files/members/"+resp.container+"/"+resp.video+'_thumb.png',
							image: "https://<?php echo $videoCdnURL; ?>/"+resp.container+"/"+resp.video+'_thumb.png',
							logo:
							{
							hide: true
							},
							
							plugins:{
									"<?php echo ROOT_FOLDER;?>jwplayer/view_course.js":
										{				
											texto: id2 
										}
									}
							});
							jwplayer("render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>").play();
							//*****   For water mark text *****************	
							// Prevent form submission in IE AND CHROME
						    //jQuery(document).on('click', '.jwplayer button', function(event) { event.preventDefault(); });
						    setTimeout(function(){$('#render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>').css('border','1px solid #BEDAFF')},2000);
						}else if(resp.extension =="swf") {
							var strURLFull = window.document.location.toString();
							var intTemp = strURLFull.indexOf("?");
							var	strURLParams = "";
							if(intTemp != -1)
							{
								strURLParams = strURLFull.substring(intTemp + 1, strURLFull.length);
							}
							$("#render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>").css({'width':'62.5%'});
							//var so = new SWFObject(ajax_url+"/files/members/"+resp.container+"/"+resp.video+'.'+resp.extension, "Captivate", "425px", "335px", "10", "#CCCCCC");
							
							var so = new SWFObject('https://<?php echo $videoCdnURL; ?>/'+resp.container+'/'+resp.video+'.'+resp.extension, "Captivate", "425px", "338px", "10", "#CCCCCC");
							so.addParam("quality", "high");
							so.addParam("name", "Captivate");
							so.addParam("id","render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>");
							so.addParam("wmode", "window");
							so.addParam("bgcolor","#f5f4f1");
							so.addParam("seamlesstabbing","true");
							so.addParam("menu", "false");
							so.addParam("AllowScriptAccess","always");
							so.addVariable("variable1", "value1");
							if(strURLParams != "")
							{
								so.addVariable("flashvars",strURLParams);
							}
							so.setAttribute("redirectUrl", "http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash");
							so.write("render_video_l_<?php echo $lesson_highlight['CourseLesson']['id']?>");
						
							document.getElementById('Captivate').focus();
							document.Captivate.focus();
						}
				}
				else if(resp.status == 'nolesson'){
					alert(resp.error);
				}
				$(".load_top").hide();	
			}
		});
		}
		
	});
	
	$(".add_learning_sub").on('click',function(){
			var getId = $(this).attr('rel');
			$(".load_top").show();	
			var a =$(this);	
			
			req = $.ajax({
				url:ajax_url+'Home/add_to_learning/'+getId,
				dataType: 'json',
				success: function(resp){
					if(resp.status == 'ok'){
						$(".load_top").hide();	
						a.css({'color':'#A5A5A5'});	
						a.addClass('added_learning');	
						a.removeClass('add_learning');						
						$(".ajax-success").html(resp.msg).show();
						$('.ajax-success').fadeOut(10000);
					}
					else if(resp.status == 'error'){
						window.location.href = resp.url;
					}
				}
			});
		});
	
	
	
	<?php if($lesson_play==1 || $lesson_play=='yes' ){ ?>
if($('#myModal_lesson_<?php echo $lesson_highlight['CourseLesson']['id'];?>').find('.add_to_cart').length==0){
if($('#myModal_lesson_<?php echo $lesson_highlight['CourseLesson']['id'];?>').find('.ply_btn_anchr').attr('href')!='javascript:void(0);'){
window.open($('#myModal_lesson_<?php echo $lesson_highlight['CourseLesson']['id'];?>').find('.ply_btn_anchr').attr('href'),"_self");
}else{
$(".bg_wch2 , .ply_btn_anchr2").trigger('click');

}
}
<?php }?>
	 $('.lsn_thmbnal_lft').click(function(){
		
			var lesson_id=$(this).attr('id');
			var course_id=$('.course_id').attr('id');
			window.location.href = ajax_url+'Members/lesson_detail/'+course_id+'/'+lesson_id;
	 
	});
	<?php if($this->Session->read('LocTrain.id') != $lesson_highlight['Course']['m_id']) {?>	
		$('#do_like').on("click",function(){
			
			var rel = $(this).attr('rel');
			$this=$(this);
			var lessonId = $('.bg_wch').attr('id');
			var getId = $('.like_button').attr('id');
			
			var request = $.ajax({
						url: ajax_url+"members/like_lesson/",
						type: "POST",
						data: {'course_id': getId,'lessonId': lessonId },
						dataType: "html"
					  });
					request.done(function(msg){
						$this.attr('rel',msg);
						$this.html(msg);
						if(msg=="Like" || msg=="Gefällt mir")
						{
							var no=$this.parent().attr('id');
							no--;
							$this.parent().attr('id',no);
							$this.next().attr('title',"( "+no+" ) <?php echo __('people_like_this');?>.");
							$this.attr('title','<?php echo __('Like this Lesson');?>');
							$this.next().html("( "+no+" )");
						}
						else
						{
							var no=$this.parent().attr('id');
							no++;
							$this.parent().attr('id',no);
							$this.attr('title','<?php echo __('Unlike this Lesson');?>');
							$this.next().attr('title',"( "+no+" ) <?php echo __('people_like_this');?>.");
							$this.next().html("( "+no+" )");
						}
					});
		});
	<?php } ?>
	 var clicked = false;
	 $('.prevent_click').on("click", function (e) {
		if(clicked===false){
		   clicked=true;
		}else{
		   e.preventDefault();
		}
	  });
	
  });
</script>
</div>

</div>
