<?php  $locale = $this->Session->read('LocTrain.locale'); ?>
<?php echo $this->Html->script('Scripts/jquery.msgBox.js');?>
<?php echo $this->Html->css('front/msgBoxLight.css');?>
<?php 
   /* Rating Script [Start] */
        echo $this->Html->script('front/star-rating.js');
?>
<style>
 .optDisble{
color:#f2f2f2;
}
.optEnble{
color:#0070c0;
}  
.vwcrsply_btn_thmb-large { 
    left: 31%;
    top: 38%;
}
.video_sec3 {
  
    min-height: 302px;
}
.tc_links_inner
{
	width:auto !important;
	/*margin-right:2px !important;*/
	margin-top:2px !important;
}
.preview_disable
{
	color: #333 !important;
    text-decoration: none !important;
}
.bg_wch_preview
{
	background-position:2px !important;
}
.bg_wch_preview:hover
{
	background-position:2px !important;
}
</style>
<script type="text/javascript">
var req = '';

$(document).ready(function(){
		
          $('body').on('click','.video_sec',function(e){
if( $(e.target).hasClass("tc_four")){
//console.log('yes');
return false;
}else if( $(e.target).hasClass("remv_frm_plan")){
//console.log('yes');
return false;
}else if( $(e.target).hasClass("vw_scn_link")){
//console.log('yes');
//return false;
}else if( $(e.target).hasClass("rating-container")){
//console.log('yes');
return false;
}else if( $(e.target).hasClass("rating-stars")){
//console.log('yes');
return false;
}

else if( $(e.target).hasClass("wishlist-link")){
//console.log('yes');
return false;
}

else if( $(e.target).hasClass("targerlink")){
//console.log('yes');
return false;
}
else{
//console.log('no');
window.location=ajax_url+'Home/view_course_details/'+$(this).attr('rel');
}
});
    $(".dropdown-menu-learning li a").click(function(){
            $("#dropdown6:first-child").text($(this).text()).append('<span class="caret"></span>' );
            
          });
       <?php 
    if(isset($this->request['pass']) && !empty($this->request['pass'])){
        if($this->request['pass'][1]=='tab3'){ ?>
            showContent('tab3');
            $('.nav-tabs a[href="#tab3"]').tab('show');
		var attr_text=$('.tab3').text();
 		$("#dropdown6:first-child").text(attr_text).append('<span class="caret"></span>' );
        <?php }
    }else{ ?>
      showContent('tab1');
        $('.nav-tabs a[href="#tab1"]').tab('show');
		var attr_text=$('.tab1').text();
 		$("#dropdown6:first-child").text(attr_text).append('<span class="caret"></span>' );
  <?php  }  ?>
	var num_records=$('.tchng_content').length ;
	$("body").on('click','.read_more_review',function(){
		var tabText = $(this).text();
		var tabNewText = $(this).attr('rel');
		$(this).parent().parent().prev().prev().show();
		$(this).parent().parent().prev().prev().prev().hide();
		$(this).attr('class','read_less_review');
		$(this).attr('rel',tabText);
		$(this).text(tabNewText);
		$(this).css({'background-position':'0 -71px'});
		return false;		
	});
	$("body").on('click','.read_less_review',function(){	
		var tabText = $(this).text();
		var tabNewText = $(this).attr('rel');
		$(this).parent().parent().prev().prev().hide();
		$(this).parent().parent().prev().prev().prev().show();
		$(this).attr('class','read_more_review');	
		$(this).attr('rel',tabText);
		$(this).text(tabNewText);
		$(this).css({'background-position':'0 0'});
		return false;		
	});	
	$("body").on('click','.open_video',function(){
		var getUrl = $(this).attr('id');
		var watch_or_preview = $(this).attr('rel');
		
		
		var w = 700;
		var h = 400;
		var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/1.5);	
		window.open(ajax_url+'Home/catalogue_video/'+getUrl,"","height="+h+",width="+w+",top="+top+",left="+left+"toolbar=no,scrollbars=no,location=no,resizable=yes,menubar=0,directories=no");		
	});	
$('#yes_remove').click(function(){
            $(".loading_inner").show();	
            var Id= $('#remove_id').val();
            var remove_id=$('#remove_id_val').val();
            req = $.ajax({
                    url:ajax_url+'Home/remove_from_learning/',
                    type: 'get',
                    data: {'id':Id},
                    success: function(resp){					
                            $(".loading_inner").hide();
                            if($.trim(resp)=='deleted')
                            {
                                $('#lesson_remove').modal('hide');
                                $('#remove_div_'+remove_id).remove();                                    
                                   var num_records=$('#tab3').find('div.video_sec').length;
                                   if(num_records==0)
                                    {
                                      $('#tab3').html('No courses or lessons found.');  
                                    } 
                                    //location.reload(true);
                            }
                            else
                            {
                                    alert(resp); 
                            }
                    }
            });
            });
         $('#no_remove').click(function(){
            $('#lesson_remove').modal('hide');
        });
	$("body").on('click','.remv_frm_plan',function(){
		var Id = $(this).attr('rel');
		$this=$(this);	
               $('#remove_id').val(Id);
               $('#remove_id_val').val($(this).attr('id'));
		//alert(Id);
		/*$(document).keydown(function(event) {
        // left = 37, right = 39
		if(event.keyCode ==27)
		{
			$(".msgBox").hide();
			//window.close();
			//$('.msgBox').remove();
			// return false;
			//vallll=event.keyCode;
			
		}*/
		
		$(document).keydown(function(event) {
			// left = 37, right = 39
			if(event.keyCode ==27)
			{
				$('.msgBox').fadeOut().remove();
				//$('.msgBox').animate({ opacity: 0, "top": top - 50, "left": 50}, 200);
				$('.msgBoxBackGround').fadeOut(300).remove();
			}
		});
		 $('#lesson_remove').modal('show');
		
		
	});	
	
// SORT RECORDS	
	$(".filterBy").click(function(){	
		var setVal = $(this).attr('rel');
		var getId = $("#"+setVal).val();
		if(getId == '1'){
			$(this).removeClass('chk_chkd');
			$("#"+setVal).val('0');
		}
		else if(getId == '0'){
			$(this).addClass('chk_chkd');			
			$("#"+setVal).val('1');
		}
		$(".loading_inner").show();	
			 var sort_order=$('#sortInput').val();
		req = $.ajax({
			url:ajax_url+'Members/my_learning/',
			type: 'get',
			data: {'type':'2','limit':0,'completed':$("#completed").val(),'current':$("#current").val(),'planed':$("#planed").val(),'sort_order':sort_order},
			success: function(resp){				
				$(".tchng_cont_content").html(resp);	
				$(".loading_inner").hide();
			}
		});
		
	});
// RESET SEARCH FILTER	
	$(".scn_link").click(function(){	
		$(".filterBy").removeClass('chk_chkd');	
		$(".filterBy").children(':input').val('0');		
		if(req  && req.readystate  != '4'){
			req.abort();
		}	
		$(".loading_inner").show();	
		req = $.ajax({
			url:ajax_url+'Members/my_learning/',
			type: 'get',
			data: {'type':'3','limit':0},
			success: function(resp){
				$("#searchType").val('0');	
				$("#sort").val('');
				$(".filter_options_rdo").removeClass("rdo_chkd");
				$(".as-selection-item").remove();
				$(".as-values").val('');
				$(".tchng_cont_content").html(resp);	
				$(".loading_inner").hide();
			}
		});
	});	

$('#cl_a').click(function(){

if($(this).find('.fa:first').hasClass('optEnble')){
	$('#sortInput').val('1');
	$(this).find('.fa:first').removeClass('optEnble');
	$(this).find('.fa:first').addClass('optDisble');
	$(this).find('.fa:last').addClass('optEnble');
	$(this).find('.fa:last').removeClass('optDisble');
	}else{$('#sortInput').val('0');
	$(this).find('.fa:last').removeClass('optEnble');
	$(this).find('.fa:last').addClass('optDisble');
	$(this).find('.fa:first').removeClass('optDisble');
	$(this).find('.fa:first').addClass('optEnble');
	
	}

$('#clt_a').find('.fa:first').removeClass('optDisble');
$('#clt_a').find('.fa:first').removeClass('optEnble');
$('#clt_a').find('.fa:first').addClass('optDisble');

$('#clt_a').find('.fa:last').removeClass('optEnble');
$('#clt_a').find('.fa:last').removeClass('optDisble');
$('#clt_a').find('.fa:last').addClass('optEnble');

$('#w_l').find('.fa:first').removeClass('optDisble');
$('#w_l').find('.fa:first').removeClass('optEnble');
$('#w_l').find('.fa:first').addClass('optDisble');

$('#w_l').find('.fa:last').removeClass('optEnble');
$('#w_l').find('.fa:last').removeClass('optDisble');
$('#w_l').find('.fa:last').addClass('optEnble');

});
$('#clt_a').click(function(){

if($(this).find('.fa:first').hasClass('optEnble')){
	$('#sortInput').val('1');
	$(this).find('.fa:first').removeClass('optEnble');
	$(this).find('.fa:first').addClass('optDisble');
	$(this).find('.fa:last').addClass('optEnble');
	$(this).find('.fa:last').removeClass('optDisble');
	}else{$('#sortInput').val('0');
	$(this).find('.fa:last').removeClass('optEnble');
	$(this).find('.fa:last').addClass('optDisble');
	$(this).find('.fa:first').removeClass('optDisble');
	$(this).find('.fa:first').addClass('optEnble');
	
	}



$('#cl_a').find('.fa:first').removeClass('optDisble');
$('#cl_a').find('.fa:first').removeClass('optEnble');
$('#cl_a').find('.fa:first').addClass('optDisble');

$('#cl_a').find('.fa:last').removeClass('optEnble');
$('#cl_a').find('.fa:last').removeClass('optDisble');
$('#cl_a').find('.fa:last').addClass('optEnble');

$('#w_l').find('.fa:first').removeClass('optDisble');
$('#w_l').find('.fa:first').removeClass('optEnble');
$('#w_l').find('.fa:first').addClass('optDisble');

$('#w_l').find('.fa:last').removeClass('optEnble');
$('#w_l').find('.fa:last').removeClass('optDisble');
$('#w_l').find('.fa:last').addClass('optEnble');
});
$('#w_l').click(function(){

if($(this).find('.fa:first').hasClass('optEnble')){
	$('#sortInput').val('1');
	$(this).find('.fa:first').removeClass('optEnble');
	$(this).find('.fa:first').addClass('optDisble');
	$(this).find('.fa:last').addClass('optEnble');
	$(this).find('.fa:last').removeClass('optDisble');
	}else{$('#sortInput').val('0');
	$(this).find('.fa:last').removeClass('optEnble');
	$(this).find('.fa:last').addClass('optDisble');
	$(this).find('.fa:first').removeClass('optDisble');
	$(this).find('.fa:first').addClass('optEnble');
	
	}

$('#cl_a').find('.fa:first').removeClass('optDisble');
$('#cl_a').find('.fa:first').removeClass('optEnble');
$('#cl_a').find('.fa:first').addClass('optDisble');

$('#cl_a').find('.fa:last').removeClass('optEnble');
$('#cl_a').find('.fa:last').removeClass('optDisble');
$('#cl_a').find('.fa:last').addClass('optEnble');

$('#clt_a').find('.fa:first').removeClass('optDisble');
$('#clt_a').find('.fa:first').removeClass('optEnble');
$('#clt_a').find('.fa:first').addClass('optDisble');

$('#clt_a').find('.fa:last').removeClass('optEnble');
$('#clt_a').find('.fa:last').removeClass('optDisble');
$('#clt_a').find('.fa:last').addClass('optEnble');
});
});
function showContent(opt){


var setVal = $(this).attr('rel');
		var completed=0;
                var current=0;
                var planed=0;
			if(opt=='tab2'){
                            completed=0;
                            current=2;
                            planed=0;
                        }
                        else if (opt=='tab3'){
                            completed=0;
                            current=0;
                            planed=1;
                        }else{
                            completed=0;
                            current=1;
                            planed=0;
                        }
 $("#completed").val(completed);
                        $("#current").val(current);
                        $("#planed").val(planed);
                        $(".loading_inner").show();
  var sort_order=$('#sortInput').val();
		req = $.ajax({
			url:ajax_url+'Members/my_learning/',
			type: 'get',
			data: {'type':'2','limit':0,'completed':completed,'current':current,'planed':planed,'sort_order':sort_order},
			success: function(resp){
					
				$("#"+opt).html(resp);	
				$(".loading_inner").hide();
			}
		});

		
}
<?php /*if(!empty($course_list)) { */?>
	$(document).on('scroll',function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			
			var getId = $("#tab1").attr('class');

			//var abc=getId.split(" ");
			 var abc=to_numbers (getId);
			var totalRecord = $(".tab-content").attr('id');	
			var searchType = $("#searchType").val();                       
                        var sort_order=$('#sortInput').val();
			if(req  && req.readystate  != '4'){
				req.abort();
			}			
			if(abc != 0){						
				$(".loading_inner").show();					
				req = $.ajax({					
					url:ajax_url+'Members/my_learning/',
					type: 'get',
					data: {'type':'1','limit':abc,'completed':$("#completed").val(),'current':$("#current").val(),'planed':$("#planed").val(),'sort_order':sort_order},
					success: function(resp){
					console.log(resp);
						if($.trim(resp) == 'error'){
							window.location.href = ajax_url+'Home/index';
						}
						$("#tab1").append(resp);	
						$(".loading_inner").hide();
					}
				});				
			}	
		}
	});	
<?php /*}*/ ?>
function to_numbers (str)
			{var is_number = /[0-9]/;
				var formatted = '';
				for (var i=0;i<(str.length);i++)
				{
					char_ = str.charAt(i);
					if (formatted.length==0 && char_==0) char_ = false;

					if (char_ && char_.match(is_number))
					{
						
						formatted = formatted+char_;
					}
				}

				return formatted;
			}
</script>
<section class="my_learning_section">
 <?php echo $this->Form->input('sortby',array('type'=>'hidden','id'=>'sortInput','value'=>'1')); ?>
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="tabbable Tabbable1 margintop20">                                    	
                         <input type="hidden" id="completed"><input type="hidden" id="current">
                                <input type="hidden" id="planed">
                    	<ul class="nav nav-tabs TabDropdownBox">
                        	<li class="dropdown pull-right tabdrop">
                                <a id="dropdown6" class="dropdown-toggle" data-toggle="dropdown" href="#">Select <b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-menu-learning TabDropdown" aria-labelledby="dropdown6">
                                    <li class=""><a href="#tab1" class="tab1" data-toggle="tab" onclick="showContent('tab1');" ><?php echo __('current_learning');?></a></li>
                                    <li class=""><a href="#tab2"  class="tab2" data-toggle="tab" onclick="showContent('tab2');"><?php echo __('completed_learning');?></a></li>
                                    <li class=""><a href="#tab3" class="tab3"  data-toggle="tab" onclick="showContent('tab3');"><?php echo __('wishlist');?></a></li>
                                    
                                </ul>
                            </li>
                        </ul>
                        
                            <ul class="nav nav-tabs">                                                                                   	                                            
                                                        
                                <li class="active"><a href="#tab1" data-toggle="tab" onclick="showContent('tab1');" id="cl_a"><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i> <?php echo __('current_learning');?></a></li>
                                <li class=""><a href="#tab2" data-toggle="tab" onclick="showContent('tab2');" id="clt_a"><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i><?php echo __('completed_learning');?></a></li>
                                <li class=""><a href="#tab3" data-toggle="tab" onclick="showContent('tab3');" id="w_l"><i class="fa fa-caret-up optEnble"></i><i class="fa fa-caret-down optDisble"></i><?php echo __('wishlist');?></a></li>
                               
                            </ul>
                        
                            <div class="tab-content">
                            
                                <div class="tab-pane active" id="tab1">
                                      <?php  $i=0;

                                        foreach($course_list as $list){ 



  $m_id = $this->Session->read('LocTrain.id');
       $decoded_id = $list['Course']['id'];
if($this->Session->read('LocTrain.id'))
		{
		
		
		if($tabtype=='wishlist')
		{
		 $is_rated = $this->Utility->getCourseRating($m_id,$decoded_id);
		$rate=''; 
			   for($i=0;$i<count($is_rated);$i++)
			   {
			   
			   $rate+=$is_rated[$i]['CourseRating']['rating'];
			   }
			   $is_rated1=$rate/count($is_rated);
		
			   if(!empty($is_rated))
			   {
			   if(count($is_rated)>1)
			   {
			   $rating=(fmod($is_rated1,0.5)==0) ? $is_rated1 : round($is_rated1);
			   }
			   else
			   {
			   $rating=$is_rated1;
			   }
			    
			   }
			else
			{
				$rating= 0;
			}
			
		
		
		}
		else{
		
		 $is_rated = $this->Utility->getCourselearningRating($m_id,$decoded_id);

 if(!empty($is_rated))
			{
				  $rating= $is_rated['CourseRating']['rating'];
			}
			else
			{
				$rating= 0;
			}
		
		}

			$login=1;
 }
 
 
										//echo '<pre>';print_r($list);echo '</pre>';?>
					<?php 	$video = '';	 $less_image='';	 $image='';		
                                                if(!empty($list['CourseLesson'])){
                                                    $if_only_one_lesson=count($list['CourseLesson']);
                                                    foreach($list['CourseLesson'] as $lesson){
                                                        if($lesson['video'] != ''){
                                                            $video = $lesson['video'];
                                                            $extension = $lesson['extension'];
                                                            $image = $lesson['image'];
                                                            break;
                                                            }
                                                        }
  foreach($list['CourseLesson'] as $lesson){
                    if($lesson['image'] != ''){                       
                        $less_image=$lesson['image'];
                        break;
                    }
                }
                                            }	$learning_type='';
										
											
						if(!empty($list['CoursePurchaseList'])){
						     foreach($list['CoursePurchaseList'] as $row){
                                                        if($row['c_id'] != $list['course']['id'] && $row['purchase_id']!=NULL && $tabtype!='wishlist'){
														
															
														
                                                            $learning_type=$row['type'];
                                                            break;
                                                            }
															elseif($row['c_id'] != $list['course']['id'] && $row['purchase_id']==NULL && $tabtype=='wishlist')
															{
															
															 $learning_type=$row['type'];
                                                            break;
															}
														
							}
						}							
					?>
                                    <div class="video_sec video_sec2 video_sec3" rel="<?php echo base64_encode(convert_uuencode($list['Course']['id']));?>" id="remove_div_<?php echo $list['CoursePurchaseList'][0]['id'];?>">
                                    	<div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 VideoBoxLeft">
                                               <?php 
                             $course_image = $list['Course']['image'];
              

$path=$this->Utility->getAWSDisplayUrl(COURSES_BUCKET,$course_image);
                    if(!empty($course_image) && $path){
                         echo $this->Html->link($this->Html->image($this->Utility->getAWSImgUrl(COURSES_BUCKET,'thumb/'.$course_image),array('width'=>'','height'=>'','class'=>'img-responsive')),array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'course_link','escape'=>false));        
                    }else if(!empty($less_image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$less_image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$less_image),array( 'class'=>'img-responsive'));
                    }else if(!empty($image) && $this->Utility->getAWSDisplayUrl(LESSON_BUCKET,$image)){
                         echo $this->Html->image($this->Utility->getAWSImgUrl(LESSON_BUCKET,'thumb/'.$image),array( 'class'=>'img-responsive'));     
                    }else
                    { 
                       echo $this->Html->image('front/no_thumbnail_'.$locale.'.png',array('url'=>array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id'])))),array('class'=>'img-responsive'));   
                    }   
                                                    ?>

                                                    
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 RatingBox LastViewedBox">
                                            	                                           	
                                                    <?php if(!empty($list['CourseWatch'])) {
                                                    $u_date=$this->Time->convert($list['CourseWatch'][0]['date_viewed'],$this->Session->read('Timezone'));

                                                        ?>
                                             <div class="OtherLink LastWatch"> <?php echo String::insert(__("last_viewed_br_date"),array('date' => $this->Timezone->dateFormatAccoringLocaleLang($this->Session->read('Config.language'),$list['CourseWatch'][0]['date_viewed']))); ?></div>
                                                <?php } else{ ?>
                                              <div class="OtherLink"></div>
                                                <?php } ?>
                                              <?php 
						//	print_r( $list['Subscription_course']);
							//print_r( $list['Subscription_lesson']);
							
						 if(!empty($list['Subscription_course']))
						{
							$option="view";
							$full_course=1;
						}
						else if(!empty($list['Subscription_lesson']))
						{
							$full_course=0;
							$option="view";
						}
						else
						{
							$option="view";
							$full_course=0;
							$if_only_one_lesson=2;
						}
				?>
                                              <div class="OtherLink">
				<?php  
                    if(trim(@$set[$i])=="unlink") {
							
				      $class = 'vw_scn_link bg_ad_basket added_learning_sub';
				      $url = 'javascript:void(0);';						
								if(isset($learning_type) && $learning_type== 1){	
					echo $this->Html->link(__('add_to_shopping_basket'),$url,array('class'=>$class));
}
				}
					
					else if(!$subscribed)
							{
                                $class ='';	
                               if(in_array($list['Course']['id'],$cookie_courses) ||  trim($list['Course']['price'])==0.00)
								/*{  
									$class = 'vw_scn_link_disable bg_ad_basket preview_disableadded_learning_sub';
								}else{
									$class ="vw_scn_link_disable bg_wch_preview preview_disable add_to_cart ad_bskt_bg";
								}*/
								
								{  
									$class = 'vw_scn_link bg_ad_basket added_learning_sub';
								}else{
									$class ="vw_scn_link add_to_cart ad_bskt_bg"; 
								}
                                $url = in_array($list['Course']['id'],$cookie_courses)? 'javascript:void(0);' : array('controller'=>'Home','action'=>'add_to_cart',base64_encode(convert_uuencode($list['Course']['id'])),'tab3');						
                              
							    if(!$full_course)
								{
									if($if_only_one_lesson > 1)
									{
									if(isset($learning_type) && $learning_type== 1){
										echo $this->Html->link(__('add_to_shopping_basket'),$url,array('class'=>$class));}
											
									}
								}
								
								if( $list['Subscription_course'][0]['type']==1 && $learning_type==1)   
							{
								
								$lessons=$this->Utility->getpurchaselistexistence($list['Course']['id'],$this->Session->read('LocTrain.id'));
								if(!empty($lessons))
								{
								if($lessons[0]['CoursePurchaseList']['lesson_id'])
								{
								echo $this->Html->link(__('add_to_shopping_basket'),'javascript:void(0);',array('class'=>'vw_scn_link_disable wishlist-link'));
								}
								}
								else
								{
								echo $this->Html->link(__('add_to_shopping_basket'),$url,array('class'=>$class));
								}
							
							}
							}
                            ?>
                                                  </div>
                                              <div class="OtherLink viewlink1">
                <?php 
					
					$owner_id = base64_encode(convert_uuencode($list['Course']['m_id']));
					if($list['Course']['price']==0.00)
					{
						if($extension!='swf' && $extension!='zip')
						{
							echo $this->Html->link(__('view'),'javascript:void(0);',array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>'tc_four open_video','rel'=>'view'));	
						}else {
							
							echo $this->Html->link(__('view'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$list['Course']['title']),array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>'tc_four'));
						
						}						
					}
					
					else if(!$subscribed)
					{
						if(isset($extension) && $extension!='swf' && $extension!='zip')
						{
							echo $this->Html->link(__($option),'javascript:void(0);',array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>'tc_four open_video','rel'=>$option)); 
						}else {
							/*if($option=="Watch"){
							
								echo $this->Html->link(__('view'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$list['Course']['title']),array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>'tc_four'));
							}else{
								echo $this->Html->link(__('view'),'javascript:void(0);',array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>'tc_four vw_scn_link_disable bg_wch_preview preview_disable','title'=>__('preview_is_not_available_for_interactive_videos')));
							} */ 
							echo $this->Html->link(__('view'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$list['Course']['title']),array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>''));
							
                        }						
						
						
					}else
					{
						if($extension!='swf' && $extension!='zip')
						{
							echo $this->Html->link(__('view'),'javascript:void(0);',array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>'tc_four open_video','rel'=>'view'));
						}else {
							echo $this->Html->link(__('view'),array('controller'=>'home','action'=>'play_video_swf/'.$video.'/'.$extension.'/'.$owner_id.'/'.$list['Course']['title']),array('id'=>base64_encode(convert_uuencode($list['Course']['id'])),'class'=>'tc_four'));
						}						
					}
				?>
                                                  </div>
												  
												  
												  	  <div class="RatingBox1">
            <div class="col-xs-6  no-padding">
            <form>
               <?php if($tabtype=='wishlist')
			  
			   {
			   ?>
			   <input id="input-21a-<?php echo $list['Course']['id']?>" data-disabled="true" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
			   
			   <?php }
			   else{
			   ?>
			    <input id="input-21a-<?php echo $list['Course']['id']?>" data-disabled="false" value="<?php echo $rating;?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xl" data-show-caption="false">
			   <?php
			   
			   }
			   ?>
          
               
            </form>
        </div>

    </div> 
	
	<div class="clear"></div>
                <?php 
			
				if($list['CoursePurchaseList'][0]['purchase_id']==NULL && empty($lessons) && in_array($list['Course']['id'],$cookie_courses)!=true && $learning_type==1)
				{
				echo '<div class="OtherLink">'.$this->Html->link(__('remove'),'javascript:void(0);',array('id'=>$list['CoursePurchaseList'][0]['id'],'class'=>'remv_frm_plan remv_frm_plan_left','rel'=>base64_encode(convert_uuencode($list['CoursePurchaseList'][0]['id'])))).'</div>'; 
				}
				?>
				
				
			       
                                            </div>        
                                        </div>
                                        <div class="row">
										
										
                                        	<div class="col-lg-12">
                                            	<h2><?php echo $this->Html->link($list['Course']['title'],array('controller'=>'Home','action'=>'view_course_details',base64_encode(convert_uuencode($list['Course']['id']))),array('class'=>'course_link')); ?></h2>
                                            	<p id="section_desc">	<?php echo $this->Text->truncate($list['Course']['description'],'200',array('exact'=>false)); ?>
                                                    	
                                                    <?php if($list['Course']['description'] > 200){ ?>
                                                        
                                                        <?php echo $this->Html->link(__('more'),'javascript:void(0);',array('class'=>'ViewMore read_more_review ','rel'=>__('less'))); ?>
                                                    <?php } ?>
                                                </p>
                                                <p class="long_desc sliding" style="display:none"> <?php echo $list['Course']['description']; ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                			
        
<?php  $i++;  } ?>
                      </div>          	
                                 
                                
                                <div class="tab-pane" id="tab2"> 
                                    
                                </div>
                                <div class="tab-pane" id="tab3">
                                    
                                </div>                                                               
                            </div>
                        
                        </div>                                               
                </div>
            </div>
        </div>
 <div class="modal fade" id="lesson_remove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog model-lesson">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><?php echo __('are_you_sure');?></h4>
                                    </div>
                                    <div class="modal-body">
                                             <div class="form-group">
                                                 <input type="hidden" id="remove_id" value="">
                                                 <input type="hidden" id="remove_id_val" value="">
                                               <p><?php echo __('are_you_sure_you_want_to_remove_this_course_from_your_learning_plan');?></p>
                                            </div>
                                         <span class="Link"><a id="yes_remove" ><?php echo __('yes');?></a>&nbsp;&nbsp;<a id="no_remove"><?php echo __('no');?></a></span>
                                          
                                    </div>                            
                                </div>
                            </div>
                        </div>
 <div class="loading" style="margin-left: -12px">
                    <div class="loading_inner">
                        <?php echo $this->Html->image('front/ajax-loader.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                    </div>
              </div>
    </section>     
    <script>
    jQuery(document).ready(function () {
$('body').addClass('homeBody');
        $(".rating").rating({
            starCaptions: function(val) {
                if (val < 3) {
                    return val;
                } else {
                    return 'high';
                }
            },
            starCaptionClasses: function(val) {
                if (val < 3) {
                    return 'label label-danger';
                } else {
                    return 'label label-success';
                }
            }
        });
  $('.rating').on('rating.change', function(event, value, caption) {
                var attr_id=$(this).attr('id');
                var arr = attr_id.split('input-21a-');
                var id =arr[1];
                var rating=value;
              $.ajax({
                           url: ajax_url+"Home/update_rating",
                           type: "POST",
                           data: {course_id : id, rating: rating},
                           dataType: "html",
						   success:function(){
						   
						   $('#to_rating_'+id).text(rating);
						   }
                     });    
              /*  console.log(id);
                console.log(value);
                console.log(caption);*/
                });
    });
</script>

