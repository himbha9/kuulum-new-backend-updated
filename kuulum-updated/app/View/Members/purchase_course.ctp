<?php 
        
        echo $this->Html->css('front/BeatPicker.min.css');
        echo $this->Html->script('front/BeatPicker.min.js');
?>
<script type="text/javascript">
    $(document).ready(function(){
	$('.FilterBox').hide();
        $(".loading_inner").hide();
        $("#dropdown7:first-child").text('<?php echo __('show_all_purchases');?>').append('<span class="caret"></span>' );
        $("#dropdown7:first-child").val('all');
        $('#date_div').hide();
        $(".dropdown-menu li a").click(function(){    
        $("#dropdown7:first-child").text($(this).text()).append('<span class="caret"></span>' );
        $("#dropdown7:first-child").val($(this).attr('value'));
        if($(this).attr('value') =='range'){
            $('#date_div').show();
                from_date.on("select", function (data) {
                $("#from_date").parent().parent('.date_selected').attr('id',data.string);
                    	 dateFrom = $('.df').attr('id');
                        	$('.dt').attr('id','');$("#to_date").val('');
			 dateTo ='';
                        $('#radioInput').val('range');
                        Type = $('#radioInput').val();
			$('.date-range').hide();
                        $(".loading_inner").show();	
                        req = $.ajax({
				url:ajax_url+'Members/purchase_course/',
				data: {'option':Type, 'date_from':dateFrom ,'date_to':dateTo ,'sort_option':1 },
				type: 'post',
				success: function(resp){
				
						$(".tchng_cont").html(resp);
						
					
					$(".loading_inner").hide();
				}
			}); 
                  
                });
		to_date.on("select", function (data) {
		  $("#to_date").parent().parent('.date_selected').attr('id',data.string);
                        dateFrom = $('.df').attr('id');
                        dateTo = $('.dt').attr('id');
                        $('#radioInput').val('range');
                        Type = $('#radioInput').val();
			$('.date-range').hide();
                        $(".loading_inner").show();	
                        req = $.ajax({
				url:ajax_url+'Members/purchase_course/',
				data: {'option':Type, 'date_from':dateFrom ,'date_to':dateTo ,'sort_option':1 },
				type: 'post',
				success: function(resp){
				
						$(".tchng_cont").html(resp);
						
					
					$(".loading_inner").hide();
				}
			}); 
               	
                    });
        }else{
           $('#date_div').hide(); 
           $('#radioInput').val($(this).attr('value'));
            Type = $('#radioInput').val();
            var dateFrom = '';
            var dateTo = '';
            $('.date_div').hide();
            $(".loading_inner").show();	
            req = $.ajax({
				url:ajax_url+'Members/purchase_course/',
				data: {'option':Type, 'date_from':dateFrom ,'date_to':dateTo ,'sort_option':1 },
				type: 'post',
				success: function(resp){
				
						$(".tchng_cont").html(resp);
						
					
					$(".loading_inner").hide();
				}
			}); 

        }
    }); 
$(document).on('click','.common_setting a',function(){
			url = $(this).attr('href');
			
			$.ajax({
				url:url,
				type:'post',
				success:function(html)
				{
					$('#purchaseId').html(html)
				}
			})
			return false;
		});
		
  $('#turn_on_off_chk').click(function(){
    var current_filter=$(this).val();  
    var   on_off_div= $('#on_off_div').val();    
    if(current_filter=='turn_filter_on' || current_filter=='')
    {
	var dateFrom = $('.df').attr('id');
        var dateTo = $('.dt').attr('id');
	  if($('#radioInput').val() ==''){
		var Type="all";
	}else{
                   var Type = $('#radioInput').val();

	}
        $(this).val('turn_filter_off');
        $(this).html('<?php echo __('turn_filter_off');?>');
		$('.FilterBox').slideDown(500);
        //slides the respective content to show
      //  $('.tchng_cont_content').slideUp(500);
    }else{

	  	 var dateFrom = '';
            var dateTo = '';
      var Type='all';
        $(this).val('turn_filter_on');
        $(this).html('<?php echo __('turn_filter_on');?>');
        //slides the respective content to show
$('.FilterBox').hide();
      //  $('.tchng_cont_content').slideDown(500);       
    }

//alert(Type);
            //$('.date_div').hide();
            $(".loading_inner").show();	
            req = $.ajax({
				url:ajax_url+'Members/purchase_course/',
				data: {'option':Type, 'date_from':dateFrom ,'date_to':dateTo ,'sort_option':1 },
				type: 'post',
				success: function(resp){
				
						$(".tchng_cont").html(resp);
						
					
					$(".loading_inner").hide();
				}
			}); 

});
    $('#reset_filter').click(function(){
        $('#from_date').val('');
        $('#to_date').val('');
       $('.tchng_cont').html('');

     });

  
    });
  
</script>
<section class="mainContent">
    	<div class="container">  
        	
            <div class="row tabbable margintop20">
            	<div class="col-lg-12">
                    <ul class="nav nav-tabs">                                                                                   	
                        <li class="active"><a href="#tab1" data-toggle="tab" id="turn_on_off_chk" value="turn_filter_on"><?php echo __('turn_filter_on');?></a></li>
                    </ul>            
                </div>
            </div>
     
            <?php echo $this->Form->create('PurchaseFilter',array('id'=>'purchase_filter'));
            
              echo $this->Form->input('radio',array('type'=>'hidden','id'=>'radioInput','value'=>'')); ?>
         
            <div class="FilterBox">
                                
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                        <div class="form-group">
                            <label for="exampleInputPassword1"><?php echo __('filter_results');?></label>
                            <div id="" class="dropdown DropdownField">
                                <button class="btn btn-default dropdown-toggle Textname" type="button" id="dropdown7" data-toggle="dropdown"><?php echo __('dropdown'); ?><span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdown7">
                                   <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="all"><?php echo __('show_all_purchases'); ?></a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="month"><?php echo __('purchases_this_month'); ?></a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="year"><?php echo __('purchases_this_year'); ?></a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;" value="range"><?php echo __('purchases_between_two_dates');?></a></li>
                                </ul>
                            </div>   
                        </div>
                    </div>
                </div>
            <div id="date_div">
                <div class="row">
					 <?php
								$str_date=date('Y,n,j', strtotime("+1 days")); 
								$from_disable_date="{from:[".$str_date."],to:'>'}";
								?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                        <div class="form-group  date_selected df" id="">
                            <label for="from_date"><?php echo __('filter_by_date_purchased_from');?></label>
                            <input  type="text" name="from_date" data-beatpicker-id="from_date" data-beatpicker="true" placeholder="" id="from_date" data-beatpicker-disable="<?php echo $from_disable_date;?>" data-beatpicker-format="<?php echo $this->Timezone->dateFormatAccoringLocaleSearch($this->Session->read('LocTrain.locale'))?>"  class="form-control DatePicker">
                        </div>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                        <div class="form-group date_selected dt" id="">
                            <label for="to_date"><?php echo __('filter_by_date_purchased_to');?></label>
                              <input  type="text" data-beatpicker-id="to_date" name="to_date" data-beatpicker="true" placeholder="" id="to_date"  data-beatpicker-disable="<?php echo $from_disable_date;?>" data-beatpicker-format="<?php echo $this->Timezone->dateFormatAccoringLocaleSearch($this->Session->read('LocTrain.locale'))?>"  class="form-control DatePicker">
                        </div>                                            
                    </div>
                </div>
                
                <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                    <span class="Link"><a id="reset_filter"><?php echo __('reset_filter');?></a></span>
                </div>
            </div>
            </div>
            </div>            
            	<?php echo $this->Form->end(); ?>
            <div class="row">
            	<div class="col-lg-12">
            		<div class="modal-title IncomeTitle shoppingContentTitle"><?php echo __('purchase_history'); ?></div>
                </div>
            </div>
            
                  	                                
            <div class="tchng_cont" id="purchaseId">
				<?php echo $this->element('frontElements/course/purchase_history_list'); ?>
            </div>
            
        </div>
            <div class="loading" style="margin-left: -12px">
                    <div class="loading_inner">
                        <?php echo $this->Html->image('front/ajax-loader.gif',array('width'=>'197','height'=>'22','class'=>'loading_image')); ?>
                    </div>
              </div>
    </section>
        
    <!--End Tab Drop section --> 
