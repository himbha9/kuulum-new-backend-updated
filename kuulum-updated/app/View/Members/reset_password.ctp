<script type="text/javascript">
$(document).ready(function(){

	var array = {'old_pass':'<?php echo __("Please enter your old password here");?>','new_password':'<div class="password_instructions"><div class="password_instruction_inner"><?php echo __("Please choose a password consisting of:");?><br/><br/></div><ul class="instruction_ul"><li style="color:#A5A5A5;"><span class="length invalid"><?php echo __("a minimum of 8 and maximum of 20 characters");?></span></li><li style="color:#A5A5A5;"><span class="upperCase invalid"><?php echo __("at least one upper case character");?></span></li><li style="color:#A5A5A5;"><span class="lowerCase invalid"><?php echo __("at least one lower case character");?></span></li><li style="color:#A5A5A5;"><span class="number invalid"><?php echo __("at least one number");?></span></li><li style="color:#A5A5A5;"><span class="alpha invalid"><?php echo __("at least one non-alphanumeric character");?></sapn></li></ul></div>','confirmPassword':'<?php echo __("Please type your password again to confirm.");?>'};
	
	
	$('#new_password').on('keyup',function(){ checkVal();	});	
	
	
	$('#new_password').on('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#new_password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		if(inputValPass == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_new_password").text(msg);
		}
		else {
			$("#err_new_password").text('');
		}
		if((inputValPass != '' && inputValConPass != '') && (inputValPass != inputValConPass)){
			msg = '<?php echo __(PASSWORD_MISTACHMATCH);?>';
			$("#err_confirm_password").text(msg);
		}
		else {
			$("#err_confirm_password").text('');
		}
		  
		  
	});
	$('#confirmPassword').on('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#new_password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		  if(inputValConPass == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_confirm_password").text(msg);
		  }
		 else if((inputValPass != '') && (inputValPass != inputValConPass)){
			  msg = '<?php echo __(PASSWORD_MISTACHMATCH);?>';
			  $("#err_confirm_password").text(msg);
		  }
		  else {
			$("#err_confirm_password").text('');
		  }
	});

function checkVal()
{
	if($('#new_password').val().length < 8 || $('#new_password').val().length > 20){
	$(".length").removeClass("valid").addClass('invalid');
	}else{
		$(".length").removeClass("invalid").addClass('valid');
	}
	
	if($('#new_password').val().match(/[A-Z]/)){
		$(".upperCase").removeClass("invalid").addClass('valid');
	}else{
		$(".upperCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[a-z]/)){
		$(".lowerCase").removeClass("invalid").addClass('valid');
	}else{
		$(".lowerCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[0-9]/)){
		$(".number").removeClass("invalid").addClass('valid');
	}else{
		$(".number").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[!@#$%^&*()\-_=+{};:,<.>~]/)){
		$(".alpha").removeClass("invalid").addClass('valid');
	}else{
		$(".alpha").removeClass("valid").addClass('invalid');
	}
}
});
</script>

<section>
  <div class="container InnerContent">
    <div class="row">
    
    <div class="col-lg-6 col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 register formWrapper">    
    
  
      <?php echo $this->Form->create('change_password',array('id'=>'resetpassword','div'=>false,'method'=>'post','url'=>array('controller'=>'Members','action'=>'reset_password')));?>
  
      
        <div class="form-group">
            <label for="new_password"><?php echo __('new_password');?></label>
            <?php echo $this->Form->input('new_password',array('type'=>'password','id'=>'new_password','class'=>'form-control TextPwd',"placeholder"=>__('new_password:'),'label'=>false,'div'=>false)); ?> 
            <div id="err_new_password" class="register_error alert-danger"> <?php if(isset($error['new_password'][0])) echo $error['new_password'][0];?> </div>
           
        </div>
         <div class="form-group">
            <label for="confirmPassword"><?php echo __('confirm_password');?></label>
            <?php echo $this->Form->input('confirm_password',array('type'=>'password','id'=>'confirmPassword','class'=>'form-control TextPwd',"placeholder"=>__('confirm_password'),'label'=>false,'div'=>false)); ?> 
            <div id="err_confirm_password" class="register_error alert-danger"> <?php if(isset($error['confirm_password'][0])) echo $error['confirm_password'][0];?> </div>
        </div>
        <?php echo $this->Form->submit(__('submit'),array('class'=>'btn btn-default link','div'=>false,'onclick'=>"return ajax_form('resetpassword','Members/validate_reset_password_ajax','load_top')")); ?>
       
  <?php echo $this->Form->end(); ?>
    
    </div>
    
    
    
    </div>
  </div>
</section>