<script>
$(document).ready(function(){
	$(document).keydown(function(e){
		 $('.guide_bot_inner').html('');
	});
	$('#submit').click(function(){
		var login_id = $('.formfld_rgt').attr('id');
		
		if(login_id ==null || login_id =='')
		{
				window.location=ajax_url+'Home/login/'+'login_first';
		}
		else
		{
			var type = $("#subscription").val();
			if(type==null || type=='')
			{
				return false;
			}
			$(".load_top").show();
			$.ajax({
					url:ajax_url+'Members/subscription/'+type,		
					success: function(resp){
						$(".load_top").hide();
						if(resp == 'error'){
							window.location.href = ajax_url;
						}else{						
							 //console.log(window.console);
							  //if(window.console || window.console.firebug) {
							  // console.clear();
							  //}
							  $('.guide_bot_inner').html(resp);
								$('form').submit();
						}
					}
			});
		}
		
		
	});
});
</script>
<div id="wrapper"> 
  <div id="middle">
  	<?php if($this->Session->read("LocTrain.flashMsg")!=""){ ?>
    	<div class="account-activated">	<?php echo $this->Session->read("LocTrain.flashMsg"); ?></div>
	<?php $this->Session->delete("LocTrain.flashMsg");  }?>
    <div class="breadcrumb_botcont">      
          <div class="formfld_cont">
            <div class="course_loccont">
            	
                <div class="container_repeatcourse">
                
                	
                	<div class="renew_sub">
						<div class="renew_sub_main">
							<p><?php echo __("subscribe_to_the_learning_portal_and_learn_what_you_want_when_you_want_and_however_often_you_want_it_really_is_unrestricted_training_-_the_way_you_want_it_simply_buy_one_of_the_subscription_plans_below_and_start_learning"); ?>
							</p>
							<span><?php echo __("i_would_like_the_freedom_to_learn_for"); ?></span>
						</div>
						
						<div class="renew_sub_package">
							<?php if(!empty($subscriptions))
							{
								foreach($subscriptions as $subscription)
								{			//pr($subscription);
									static $i=0;
									static $y=1;
									if($y%2!=0)
									{
										$cll='main_thnx renew_sub_package_block';
									}
									else
									{
										$cll='main_thnx renew_sub_package_block renew_sub_package_right';
									}
									
									if(!$i)
									{
										$classes='filter_options_rdo';
										echo $this->Form->input('subscription',array('type'=>'hidden','class'=>'subscription','id'=>'subscription','value'=>''));
									}
									else
										$classes='filter_options_rdo';
					
					$loc="name_".$locale;
					//pr($subscription);die;
					//pr($loc);die;
					?>
                    			<div class="<?php echo $cll; ?>">
                        			<div class="<?php echo $classes;?>" id="<?php echo base64_encode(convert_uuencode($subscription['Price']['id']));?>" rel="subscription">  </div>
                        			<p> <?php echo __($subscription['Price'][$loc]);?></p><span><?php echo '$ '.$subscription['Price']['initial_value'];?> <span>
                     			</div>      
                    
                <?php   $i++; $y++;	}  
							}
				?>
						</div>
					</div>
					
					<div class="guide_bot_inner"></div> 
                </div>
            </div>
          </div>
          <div class="formfld_rgt" id="<?php echo $login_member_id; ?>">
            <div class="section_guide"> <span class="guide_upper"> <?php echo __('in_this_section');?> </span>
              <h2 class="guide_head"> <?php echo __('checkout'); ?> </h2>             
              <div class="guide_bot"> <span class="option"> <?php echo __('related_links');?> </span>            
                <!--<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" id='submit' alt="PayPal - The safer, easier way to pay online!">  -->
				 <?php if($locale =='ko' || $locale =='zh' || $locale =='ar'  )
				 {?>
				 <?php $image='https://www.sandbox.paypal.com/en_US/i/btn/btn_subscribeCC_LG.gif';    ?>
					 <a id='submit' href="javascript:void(0)" > <?php echo $this->Html->image($image,array('class'=>'sc_btns_img', 'src'=>$image,'alt'=>'PayPal - The safer, easier way to pay online!')); ?> </a> 
					<?php } else {?>
					 
					<?php $image='https://www.sandbox.paypal.com/'.$language_folder.'/i/btn/btn_subscribeCC_LG.gif';    ?>
					 <a id='submit' href="javascript:void(0)" > <?php echo $this->Html->image($image,array('class'=>'sc_btns_img', 'src'=>$image,'alt'=>'PayPal - The safer, easier way to pay online!')); ?> </a> 
					<?php } ?>
					 
              </div>
			</div>
         </div>
	</div> 
   </div>
  </div>
</div>
