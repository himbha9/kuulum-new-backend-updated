

<div id="wrapper"> 
  <div id="middle">    
    <div class="breadcrumb_botcont">
      <?php echo $this->Form->create('Member',array('id'=>'memberRegister','div'=>false,'method'=>'post','url'=>array('controller'=>'Home','action'=>'register')));?>
          <div class="cms_cont">
          	<div class="cms_head">
            	
            		
            		<div class="summary">
						<table style="border:1px solid #000; font-size:14px; line-height: 16px;" cellspacing="0">
							<tr style="margin:0;padding:0; background-color: #B2B2B2;">
								<th style="width:330px; text-align:left; border-right:1px solid #000; padding:5px;">Title</th>
								<th style="width:50px; text-align:right; border-right:1px solid #000; padding:5px;">Views</th>
								<th style="width:150px; text-align: right; border-right:1px solid #000; padding:5px;">Unit Value (€)</th>
								<th style="width:130px; text-align: right; padding:5px;">Total Income (&#8364;)</th>
							</tr>
							<tr style="background-color: #E5E5E5;">
								<td style="width:330px; text-align:left; border-right:1px solid #000; padding:5px;">Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor</td>
								<td style="width:50px; text-align:right; border-right:1px solid #000; padding:5px;">36</td>
								<td style="width:150px; text-align: right; border-right:1px solid #000; padding:5px;">18.50</td>
								<td style="width:130px; text-align: right; padding:5px;">666.00</td>
							</tr>
							<tr>
								<td style="width:330px; text-align:left; border-right:1px solid #000; padding:5px;">Incididunt ut labore et dolore magna oliqua</td>
								<td style="width:50px; text-align:right; border-right:1px solid #000; padding:5px;">34</td>
								<td style="width:150px; text-align: right; border-right:1px solid #000; padding:5px;">18.00</td>
								<td style="width:130px; text-align: right; padding-right:5px;">612.00</td>
							</tr>
							<tr style="background-color: #E5E5E5;">
								<td style="width:330px; text-align:left; border-right:1px solid #000; padding:5px;">Ut enim od minim veniam</td>
								<td style="width:50px; text-align:right; border-right:1px solid #000; padding:5px;">5</td>
								<td style="width:150px; text-align: right; border-right:1px solid #000; padding:5px;">36.99</td>
								<td style="width:130px; text-align: right; padding-right:5px;">184.95</td>
							</tr>
							<tr>
								<td style="width:330px; text-align:left; border-right:1px solid #000; padding:5px;">Quis nostrud exercitation ullamco laboris nisi ut oliquip ex ea commodo consequat</td>
								<td style="width:50px; text-align:right; border-right:1px solid #000; padding:5px;">145</td>
								<td style="width:150px; text-align: right; border-right:1px solid #000; padding:5px;">5.99</td>
								<td style="width:130px; text-align: right; padding-right:5px;">868.55</td>
							</tr>
							<tr style="background-color: #E5E5E5;">
								<td style="width:330px; text-align:left; border-right:1px solid #000; padding:5px;">Duis aute irure dolor in reprehenderit</td>
								<td style="width:50px; text-align:right; border-right:1px solid #000; padding:5px;">6</td>
								<td style="width:150px; text-align: right; border-right:1px solid #000; padding:5px;">35.00</td>
								<td style="width:130px; text-align: right; padding-right:5px;">210.00</td>
							</tr>

						</table> 
		
					</div>
            
            	
            	
            	
            </div>
			
          </div>
          
		  
		  <div class="formfld_rgt">
            <div class="section_guide"> <span class="guide_upper"> <?php echo __('my_teaching');?>:</span>
              <h2 class="guide_head"><?php echo $this->Html->link(__('view_income'),array('controller'=>'Home','action'=>'about_us'),array('class'=>'course_link'));?> </h2>
				
              <div class="guide_bot"> <span class="option"> <?php echo __('options');?>: </span>
              	<div class="section_links">
					<?php echo $this->Html->link(__('create_a_new_course'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                </div>
              </div>
              
              <div class="guide_bot"> <span class="option"> <?php echo __('more_information');?>: </span>
              	<div class="section_links">
					<?php echo $this->Html->link(__('read_reviews'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                </div>
                <div class="section_links">
					<?php echo $this->Html->link(__('view_history'),'javascript:void(0)',array('class'=>'uc_link')); ?>
                </div>              
              </div>
              <h2 class="guide_head"> <?php echo $this->Html->link(__('terms_of_use'),array('controller'=>'Home','action'=>'terms_of_use'),array('class'=>'course_link'));?> </h2>
              <h2 class="guide_head border-top"> <?php echo $this->Html->link(__('cookie_policy'),array('controller'=>'Home','action'=>'cookie_policy'),array('class'=>'course_link'));?> </h2>
              
			  
            </div>
          </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<?php echo $this->element('frontElements/home/contact_us');?>
