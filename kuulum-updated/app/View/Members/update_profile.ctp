<?php $s_lang = array('en'=> __('english_united_states'),'de'=> __('german_germany'),'ar'=> __('arabic'),'fr'=> __('french_france'),'it'=> __('italian_italy'),'ja'=> __('japanese'),'ko'=> __('korean'),'pt'=> __('portuguese_portugal'),'ru'=> __('russian_russian_federation'),'zh'=> __('chinese_china'),'es'=> __('spanish_spain')); 
$t_lang = array('Mr'=>__('mr'),'Miss'=>__('miss'),'Mrs'=>__('mrs'));
if(isset($css) && $css=='style'){
$img_icon='icons';
}else{
$img_icon='icons_contrast';
}
?>
<?php $locale=$this->Session->read('LocTrain.locale');
		if($locale == 'ar') 
		{
			echo $this->Html->css('autoSuggest_arabic');
		}
		else{
			echo $this->Html->css('autoSuggest'); 
		}
                
   //length of current languages
                if(!empty($language)){
                    $lang_length= count($language);
                }else{
                    $lang_length=1;
                }
		?>
<?php echo $this->Html->script('jquery.autoSuggest');?>
<?php echo $this->Html->script('front/jasny-bootstrap.js');?>
<?php echo $this->Html->css('front/jasny-bootstrap.css');?>
<script type="text/javascript">
    $(document).ready(function(){

$('#notification').on('click',function(){
$('#notification_chk').val('0');
 	if($(this).is(':checked'))
	{
		$(this).val('1');
	}else{
		$(this).val('0');
	}
});


$('#notification_1').on('click',function(){
 	if($(this).is(':checked'))
	{
		$(this).val('1');
	}else{
		$(this).val('0');
	}
});
    /*   if(form=='memberUpdate'){
            $('#check_form').val();
        }*/
        
        /////
             $(".fileinput").on("change.bs.fileinput", function(e){
                var cls=$(this).parent('div').attr('class');
                if(cls == "profile_pics profile_pics2"){
                  $('#check_form').val(1);  
                   var image_val=$('#image').val();
                }else{
                  $('#check_form').val(0);
                   var image_val=$('#image_1').val();
                }
                 var get_extn=image_val.split('.');
                    if(get_extn[1]=='png' || get_extn[1] =='jpeg' || get_extn[1]=='jpg' || get_extn[1]=='gif'){
                        
                    }else{
                        $('.fileinput').fileinput('clear');
                        alert('Please select valid image file'); 
                        return false;
                    }
                });
  $('.remove_cover_image').click(function(){

  $('#check_form').val(1);  
                    $(this).hide();
                     $('.thumbnail img').attr('src','<?php echo ROOT_FOLDER; ?>img/front/cover_photo.png');
                });
      /*  $('.fileinput').fileinput({
                change.bs.fileinput:function(){alert('hello');}
            });*/
        
	var data = {items: [<?php echo $interest_list;?>]};
$("#interest").autoSuggest(data.items, {selectedItemProp: "name", searchObjProps: "value",preFill: [<?php echo $selectinterest_list;?>],fieldName: "data[Member][keyword]"});
$('ul.as-selections').addClass('FilterKeyword');

////////////////////// dropdown selection for dropdown4

$(function(){
    var locale_set=$('#locale_set').attr('value');
    if(locale_set !==''){
        $(".dropdown_menu_locale li a").each(function(){
            if( $(this).attr('value') == locale_set ) {
                 $("#dropdown4:first-child").text($(this).text()).append('<span class="caret"></span>' );;
            }
        });       
        $("#dropdown4:first-child").val(locale_set);        
    }
  
  $(".dropdown_menu_locale li a").click(function(){
    $('#locale_set').val($(this).attr('value'));
    $("#dropdown4:first-child").text($(this).text()).append('<span class="caret"></span>' );
    $("#dropdown4:first-child").val($(this).attr('value'));
  });
  $('body').on('click', '.dropdown-menu li a' ,function(){
    var cls=$(this).closest('div').attr('id');
    var id=$('#'+cls).find('.btn').attr('id'); 
    var prev_val=$('#'+id).val();
    var prev_text=$('#'+id).text();
    var input_div_id=$('#'+cls).parent('div').attr('id');
    var input_id=$('#'+input_div_id).find('.c_language').attr('id');
    $("#"+input_id).val($(this).attr('value'));
    var drop_val=$(this).attr('value');
    var drop_text=$(this).text();
    $('.LanguageRegion_chk').each(function(){
            if($(this).attr('id') != id && id !='dropdown4'){
              var drop_id=$(this).attr('id');
              if($('#'+drop_id).val()==drop_val){
                  $("#"+id+":first-child").text(prev_text).append('<span class="caret"></span>' );
                  $("#"+id+":first-child").val(prev_val);
                  alert('<?php echo __("can_not_add_multiple_biographies_with_same_language");?>');
                  return false;
              }else{
                $("#"+id+":first-child").text(drop_text).append('<span class="caret"></span>' );
                $("#"+id+":first-child").val(drop_val);
               
                }
            }else{
            $("#"+id+":first-child").text(drop_text).append('<span class="caret"></span>' );
                $("#"+id+":first-child").val(drop_val); 
               
            }
            
            
     });
  });

});
var lang_length='<?php echo $lang_length;?>';
///adding bio and locale dynamically
$("#addButton").click(function () {
                if( ($('.addFormFields .control-group').length+1)> lang_length) {
                   $('#addButton').hide();
                   return false;
                }
                var id = ($('.addFormFields .control-group').length + 1).toString();              
                var str='<div class="control-group" id="control-group' + id + '"><div class="form-group"><label><?php echo __("biography");echo '&nbsp;&nbsp;';?>'+id+' <a onclick="removeBio(\'control-group'+id+'\');"> <?php echo __('remove');?></a></label><textarea class="form-control text_area c-description" onkeyup="textkeyup(this);" rows="3" name="data[Member][about_me-'+id+']" id="c_description_'+id+'"></textarea> <div id="err_about_me-'+id+'" class="register_error alert-danger"></div> </div><div class="form-group" id="dropdown_input_'+id+'"><label for="exampleInputPassword'+id+'"><?php echo __("biography");echo '&nbsp;&nbsp;';?>'+id+'<?php echo '&nbsp;&nbsp;';echo __('language'); ?></label><input id="lang_locale-'+id+'" class="c_language" type="hidden" value="" name="data[Member][lang_locale-'+id+']"><div class="dropdown DropdownField" id="dropdown_lang_drop_'+id+'"> <button data-toggle="dropdown" id="dropdown_lang_'+id+'" type="button" onclick="checkvalidate(this);" class="btn btn-default dropdown-toggle LanguageRegion LanguageRegion_chk">Dropdown<span class="caret"></span></button><ul aria-labelledby="dropdown_lang_'+id+'" role="menu" class="dropdown-menu"> <?php foreach($language as $lang) { ?> <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $lang['Language']['locale'] ?>"><?php echo $s_lang[$lang['Language']['locale']];?> </a></li> <?php } ?></ul>  </div> <div id="err_lang_locale-'+id+'" class="register_error alert-danger">  </div>   </div></div> ';
                 $('.addFormFields').append(str);
            });
            

});
function removeBio(id){
$('#'+id).remove();
}
</script>
<?php ?>
<!-- main container section -->
<?php if($this->Session->read("LocTrain.flashMsg") != ''){ ?>
    	<div class="account-activated">	<?php echo $this->Session->read("LocTrain.flashMsg"); ?></div>
<?php $this->Session->delete("LocTrain.flashMsg"); }  ?>
 <?php echo $this->Form->create('Member',array('id'=>'memberUpdate','div'=>false,'method'=>'post','url'=>array('controller'=>'Members','action'=>'update_profile'),'enctype'=>'multipart/form-data'));?>
<section>
  <div class="container InnerContent">
    <div class="row">
    
    <div class="col-lg-6 col-md-6 col-sm-6">
	
    <div class="SmallLinkBoxWrapper SmallLinkBoxWrapper1 SmallLinksWrapper">   
        <div class="SmallLinkBox">
            <?php $type=base64_encode(convert_uuencode(2))?>
            <?php echo $this->Html->image('front/'.$img_icon.'/icon_calander.svg',array('url'=>array('controller'=>'Members','action'=>'my_learning',$type)),array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
            <span class="Link"><?php echo $this->Html->link(__('learning_history_'),'/Members/my_learning/'.$type,array('class'=>'','escape'=>false)); ?></span>
        </div>
        <div class="SmallLinkBox">
            <?php echo $this->Html->image('front/'.$img_icon.'/icon_shopping_bag.svg',array('url'=>array('controller'=>'Members','action'=>'purchase_course')),array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
            <span class="Link"><?php echo $this->Html->link(__('purchase_history_'),'/Members/purchase_course',array('class'=>'','escape'=>false)); ?> </span>
        </div>
        <?php if($this->Session->read('LocTrain.member_type')==3) { ?>
        <div class="SmallLinkBox">
            <?php echo $this->Html->image('front/'.$img_icon.'/icon_price.svg',array('url'=>array('controller'=>'Members','action'=>'view_trainer_income')),array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
            <span class="Link"><?php echo $this->Html->link(__('my_income_'),array('controller'=>'Members','action'=>'view_trainer_income'),array('class'=>'','escape'=>false)); ?> </span>
        </div>
        <?php } ?>
        <?php if($subscribed && $subs_function==1){?>
        <div class="SmallLinkBox">
            <?php echo $this->Html->image('front/'.$img_icon.'/icon_price.svg',array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
            <?php echo $this->Html->link(__('my_subscription_plan_'),'/Members/subscription',array('class'=>'','escape'=>false)); ?> 
        </div>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
        
  
  <div class="form-group">
    <label for="exampleInputEmail1"><?php echo __('preferred_name');?></label>
    <?php echo $this->Form->input('given_name',array('type'=>'text','id'=>'givenName','class'=>'form-control Textname',"placeholder"=>__(""),'label'=>false,'div'=>false,'autofocus'=>'','value'=>$memberDetail['Member']['given_name'])); ?>
    <div id="err_given_name" class="register_error alert-danger"> <?php if(isset($error['given_name'][0])) echo $error['given_name'][0];?> </div>
   </div>
  <div class="form-group">
    <label for="email"><?php echo __('E_mail_address');?></label>
        <?php echo $this->Form->input('email',array('type'=>'text','id'=>'email','class'=>'form-control TextEmail',"placeholder"=>__(""),'label'=>false,'div'=>false,'value'=>$memberDetail['Member']['email'])); ?>
        <div id="err_email" class="register_error alert-danger"> <?php if(isset($error['email'][0])) echo $error['email'][0];?> </div>
    
  </div>
        <?php echo $this->Form->input('check_form',array('type'=>'hidden','id'=>'check_form','label'=>false,'div'=>false,'value'=>0)); ?>
  <div class="form-group">
    <label for="exampleInputPassword1"><?php echo __('language');?></label>
    <?php echo $this->Form->input('locale',array('type'=>'hidden','id'=>'locale_set','class'=>'c_language','label'=>false,'div'=>false,'value'=>$memberDetail['Member']['locale'])); ?> 
    <div class="dropdown DropdownField" id="dropdown4_dd">
        <button data-toggle="dropdown" id="dropdown4" type="button" class="btn btn-default dropdown-toggle LanguageRegion"><?php echo __('dropdown');?><span class="caret"></span></button>
        <ul aria-labelledby="dropdown4" role="menu" class="dropdown-menu dropdown_menu_locale ">
            <?php foreach($language as $lang) { ?>
            <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $lang['Language']['locale'] ?>"><?php echo $s_lang[$lang['Language']['locale']];?> </a></li>
            <?php } ?>
        </ul>
    </div> 
    <div id="err_locale" class="register_error alert-danger"> <?php if(isset($error['locale'][0])) echo $error['locale'][0];?> </div>   
  </div>
  <div class="form-group">
     <label><?php echo __('type_some_keywords_or_phrases_that_describe_your_learning_interests');?></label>
   <?php echo $this->Form->input('interests',array('type'=>'text','id'=>'interest','class'=>'form-control','label'=>false,'div'=>false)); ?>
    <div id="err_interests" class="register_error alert-danger"> <?php if(isset($error['interests'][0])) echo $error['interests'][0];?> </div>
 
  </div>
  <div class="addFormFields">
      <?php $about_loc=$memberDetail['Member']['about_me'];
      if(trim($about_loc) !=''){
          $about_loc_new=json_decode($about_loc);
          //print_r($about_loc_new);
          $i=0;if(empty($about_loc_new)){ ?>
 <div class="control-group" id="control-group0">
    <div class="form-group">
      <label><?php echo __("biography");echo'&nbsp;&nbsp;1';?> </label>
          <?php echo $this->Form->input('about_me-0',array('type'=>'textarea',"rows"=>"3",'id'=>'c_description_0','onkeyup'=>'textkeyup(this);','class'=>'form-control text_area c-description','label'=>false,'div'=>false,'value'=>'')); ?> 
          <div id="err_about_me-0" class="register_error alert-danger"> <?php if(isset($error['about_me-0'][0])) echo $error['about_me-0'][0];?> </div> 
  
      </div>
    <div class="form-group" id="dropdown_input">
      <label for="exampleInputPassword1"><?php echo __("biography");echo '&nbsp;&nbsp;1&nbsp;&nbsp;';?><?php echo __('language');?></label>
      <?php echo $this->Form->input('lang_locale-0',array('type'=>'hidden','id'=>'lang_locale','class'=>'c_language','label'=>false,'div'=>false,'value'=>'')); ?> 
      <div class="dropdown DropdownField" id="dropdown_lang_drop">
          <button data-toggle="dropdown" id="dropdown_lang" type="button" class="btn btn-default dropdown-toggle LanguageRegion LanguageRegion_chk"><?php echo __('dropdown');?><span class="caret"></span></button>
          <ul aria-labelledby="dropdown_lang" role="menu" class="dropdown-menu">
              <?php foreach($language as $lang) { ?>
              <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $lang['Language']['locale'] ?>"><?php echo $s_lang[$lang['Language']['locale']];?> </a></li>
              <?php } ?>
          </ul>
      </div> 
      <div id="err_lang_locale-0" class="register_error alert-danger"> <?php if(isset($error['lang_locale-0'][0])) echo $error['lang_locale-0'][0];?> </div> 
      

    </div> 
          </div>


         <?php }else{

          foreach($about_loc_new as $row){
       ?>
           <div class="control-group" id="control-group<?php echo $i;?>">
    <div class="form-group">
      <label><?php echo __("biography");echo '&nbsp;&nbsp;'.($i+1);?> <?php if($i>0){?><a onclick="removeBio('control-group<?php echo $i;?>');"> <?php echo __('remove');?></a><?php } ?></label>
          <?php echo $this->Form->input('about_me-'.$i,array('type'=>'textarea',"rows"=>"3",'id'=>'c_description_'.$i,'class'=>'form-control text_area c-description','onkeyup'=>'textkeyup(this);','label'=>false,'div'=>false,'value'=>$row->about_me)); ?> 
          <div id="err_about_me-<?php echo $i;?>" class="register_error alert-danger"> <?php if(isset($error['about_me-'.$i][0])) echo $error['about_me-'.$i][0];?> </div> 
  
      </div>
    <div class="form-group" id="dropdown_input">
      <label for="exampleInputPassword1"><?php echo __("biography"); echo '&nbsp;&nbsp;'.($i+1).'&nbsp;&nbsp;';?><?php echo __('language');?> </label>
      <?php echo $this->Form->input('lang_locale-'.$i,array('type'=>'hidden','id'=>'lang_locale','class'=>'c_language','label'=>false,'div'=>false,'value'=>$row->lang_locale)); ?> 
      <div class="dropdown DropdownField" id="dropdown_lang_drop_<?php echo $i;?>">
          <button data-toggle="dropdown" id="dropdown_lang_<?php echo $i;?>" type="button" class="btn btn-default dropdown-toggle LanguageRegion LanguageRegion_chk " onclick="checkvalidate(this);" value="<?php echo $row->lang_locale;?>">
              <?php foreach($language as $lang) { 
              if($lang['Language']['locale'] == $row->lang_locale ){
                   echo $s_lang[$lang['Language']['locale']];
              }
              }
              ?>
              <span class="caret"></span></button>
          <ul aria-labelledby="dropdown_lang" role="menu" class="dropdown-menu">
              <?php foreach($language as $lang) { ?>
              <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $lang['Language']['locale'] ?>"><?php echo $s_lang[$lang['Language']['locale']];?> </a></li>
              <?php } ?>
          </ul>
      </div> 
      <div id="err_lang_locale-<?php echo $i;?>" class="register_error alert-danger"> <?php if(isset($error['lang_locale-'.$i][0])) echo $error['lang_locale-'.$i][0];?> </div> 
      

    </div> 
          </div>
           
           
         <?php $i++; }} }else{
      ?>
      
      <div class="control-group" id="control-group0">
    <div class="form-group">
      <label><?php echo __("biography");echo '  1';?></label>
          <?php echo $this->Form->input('about_me-0',array('type'=>'textarea',"rows"=>"3",'id'=>'c_description','class'=>'form-control text_area','label'=>false,'div'=>false,'value'=>'')); ?> 
          <div id="err_about_me-0" class="register_error alert-danger"> <?php if(isset($error['about_me-0'][0])) echo $error['about_me-0'][0];?> </div> 
  
      </div>
    <div class="form-group" id="dropdown_input">
      <label for="exampleInputPassword1"><?php echo __("biography");echo'  1';?> <?php echo __('language');?> </label>
      <?php echo $this->Form->input('lang_locale-0',array('type'=>'hidden','id'=>'lang_locale','class'=>'c_language','label'=>false,'div'=>false,'value'=>'')); ?> 
      <div class="dropdown DropdownField" id="dropdown_lang_drop">
          <button data-toggle="dropdown" id="dropdown_lang" type="button" class="btn btn-default dropdown-toggle LanguageRegion LanguageRegion_chk" onclick="checkvalidate(this);"><?php echo __('dropdown');?><span class="caret"></span></button>
          <ul aria-labelledby="dropdown_lang" role="menu" class="dropdown-menu">
              <?php foreach($language as $lang) { ?>
              <li role="presentation"><a href="javascript:;" tabindex="-1" role="menuitem" value="<?php echo $lang['Language']['locale'] ?>"><?php echo $s_lang[$lang['Language']['locale']];?> </a></li>
              <?php } ?>
          </ul>
      </div> 
      <div id="err_lang_locale-0" class="register_error alert-danger"> <?php if(isset($error['lang_locale-0'][0])) echo $error['lang_locale-0'][0];?> </div> 
      

    </div> 
          </div>
        <?php } ?>
   </div>
       <span class="Link add_bio"><a id="addButton"><?php echo __('add_biography_in_a_different_language');?></a></span>
 
 	<div class="profile_pics profile_pics1 ProfileSection">    
        <div class="Title"><?php echo __('profile_photo');?></div>   
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" style="display:none;">
            <?php



                          if($memberDetail['Member']['image'] && $this->Utility->getAWSDisplayUrl(MEMBERS_BUCKET,$memberDetail['Member']['image']))
                        {
                             echo $this->Html->image($this->Utility->getAWSImgUrl(MEMBERS_BUCKET,'thumb/'.$memberDetail['Member']['image'])); 
                        }else{

				echo $this->Html->image('front/img_placeholder.png',array('alt'=>'')); 
}
 ?>
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                <?php
                          if($memberDetail['Member']['image'] && $this->Utility->getAWSDisplayUrl(MEMBERS_BUCKET,$memberDetail['Member']['image']))
                        {
                             echo $this->Html->image($this->Utility->getAWSImgUrl(MEMBERS_BUCKET,'thumb/'.$memberDetail['Member']['image'])); 
                        }else{

				echo $this->Html->image('front/img_placeholder.png',array('alt'=>'')); 
}
 ?>
            </div>
            <div class="profileButtons">
              <span class="btn btn-default btn-file"><span class="fileinput-new"><?php echo __("upload_photo");?></span><span class="fileinput-exists"><?php echo __("upload_photo");?></span>
                  <?php echo $this->Form->input('image_1',array('name'=>'image_1','type'=>'file','id'=>'image_1','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('picture_1',array('type'=>'hidden','id'=>'file_hidden','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('old_image_1',array('type'=>'hidden','id'=>'old_image','label'=>false,'div'=>false,'value'=>$memberDetail['Member']['image'])); ?>
              </span>
  <?php
                if($memberDetail['Member']['image'] && $this->Utility->getAWSDisplayUrl(MEMBERS_BUCKET,$memberDetail['Member']['image']))
                {
                    $image = $memberDetail['Member']['image'];
                  
                    ?>
                    <a class="fileinput-new remove_cover_image" id=""><?php echo __("remove_photo");?></a>
                    <?php } ?>
              <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo __('remove_photo');?></a>
            </div>
<div class="clearfix"></div>
          </div>
            <?php /* echo $this->Html->image('front/img_placeholder.png',array('alt'=>'Photo', "width"=>"165", "height"=>"162" )); */?> 
        

        <div class="profile_upload">
<!--        	<span class="Link"><a href="#">Upload photo</a></span>
        	<span class="Link"><a href="#">Remove photo</a></span>-->
        </div> 
        
        <div class="checkbox ProfileSectionCheck text-left">
            <label>
                <p><?php echo __("tick_this_box_if_you_are_happy_for_us_to_tell_you_about_relevant_news_and_new_courses_that_may_interest_you");?> 
                       <?php
            if($memberDetail['Member']['notification']){$check= True;$check_n=1;}else{$check=  false;$check_n=0;}
            ?> <?php echo $this->Form->input('notification_chk',array('type'=>'hidden','id'=>'notification_chk','label'=>false,'div'=>false,'value'=>$check_n)); ?>
                    <?php echo $this->Form->input('notification_1',array('type'=>'checkbox','checked'=>$check,"class"=>"Checkbox2 pull-right ntct_chk",'id'=>'notification_1','value'=>$check_n,'label'=>false,'div'=>false)); ?> 
                </p>
            </label>
        </div>
            
    </div>
 
 <span class="Link"> 
     
     <?php echo $this->Form->submit(__('save_changes'),array('class'=>'btn btn-default link','div'=>false,'onclick'=>"return ajax_form('memberUpdate','Members/validate_updateprofile_ajax','load_top','disRegisBut')")); ?> </span>
 <span class="Link"><a data-toggle="modal" data-target="#myModal1"><?php echo __('reset_password');?></a></span>
 <span class="Link"><?php echo $this->Html->link(__('cancel_my_account'),array('controller'=>'Home','action'=>'cancel_account'),array('class'=>'')); ?></span>
  <?php if($this->Session->read('Loctrain.member_type') == '1') { ?>
    <span class="Link">
         <?php echo $this->Html->link(__('create_a_new_course'),array('controller'=>'Members','action'=>'course_create'),array('class'=>'')); ?>        
     </span>
  <?php } ?>
  

    
    
    
    </div>
    
    <div class="col-lg-5 col-md-6 col-sm-6 col-lg-offset-1 text-center ProfileSection"> 
    	<div class="SmallLinkBoxWrapper SmallLinkBoxWrapper2">   
            <div class="SmallLinkBox firstSmallLinkBox">
                <?php $type=base64_encode(convert_uuencode(2));?>
                <?php echo $this->Html->image('front/'.$img_icon.'/icon_calander.svg',array('url'=>array('controller'=>'Members','action'=>'my_learning',$type)),array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
                <span class="Link"><?php echo $this->Html->link(__('learning_history_'),'/Members/my_learning/'.$type,array('class'=>'','escape'=>false)); ?></span>
            </div>
            <div class="SmallLinkBox">
                <?php echo $this->Html->image('front/'.$img_icon.'/icon_shopping_bag.svg',array('url'=>array('controller'=>'Members','action'=>'purchase_course')),array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
                <span class="Link"><?php echo $this->Html->link(__('purchase_history_'),'/Members/purchase_course',array('class'=>'','escape'=>false)); ?> </span>
            </div>
            <?php if($this->Session->read('LocTrain.member_type')==3) { ?>
            <div class="SmallLinkBox">
                <?php echo $this->Html->image('front/'.$img_icon.'/icon_price.svg',array('url'=>array('controller'=>'Members','action'=>'view_trainer_income')),array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
                <span class="Link"><?php echo $this->Html->link(__('my_income_'),array('controller'=>'Members','action'=>'view_trainer_income'),array('class'=>'small','escape'=>false)); ?> </span>
            </div>
            <?php } ?>
            <?php if($subscribed && $subs_function==1){?>
            <div class="SmallLinkBox">
                <?php echo $this->Html->image('front/'.$img_icon.'/icon_price.svg',array('alt'=>'', "width"=>"57", "height"=>"57" )); ?> 
                <?php echo $this->Html->link(__('my_subscription_plan_'),'/Members/subscription',array('class'=>'','escape'=>false)); ?> 
            </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    
    
    <div class="profile_pics profile_pics2">    
        <div class="Title"><?php echo __('profile_photo');?></div>  
        
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" style="display:none;">
            <?php
                           if($memberDetail['Member']['image'] && $this->Utility->getAWSDisplayUrl(MEMBERS_BUCKET,$memberDetail['Member']['image']))
                        {
                             echo $this->Html->image($this->Utility->getAWSImgUrl(MEMBERS_BUCKET,'thumb/'.$memberDetail['Member']['image'])); 
                        }else{

				echo $this->Html->image('front/img_placeholder.png',array('alt'=>'')); 
}
 ?>
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                <?php
                           if($memberDetail['Member']['image'] && $this->Utility->getAWSDisplayUrl(MEMBERS_BUCKET,$memberDetail['Member']['image']))
                        {
                             echo $this->Html->image($this->Utility->getAWSImgUrl(MEMBERS_BUCKET,'thumb/'.$memberDetail['Member']['image'])); 
                        }else{

				echo $this->Html->image('front/img_placeholder.png',array('alt'=>'')); 
}
 ?>
            </div>
            <div class="profileButtons">
              <span class="btn btn-default btn-file"><span class="fileinput-new"><?php echo __('upload_photo');?></span><span class="fileinput-exists"><?php echo __('upload_photo');?></span>
                  <?php echo $this->Form->input('image',array('name'=>'image','type'=>'file','id'=>'image','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('picture',array('type'=>'hidden','id'=>'file_hidden','label'=>false,'div'=>false)); ?>
                  <?php echo $this->Form->input('old_image',array('type'=>'hidden','id'=>'old_image','label'=>false,'div'=>false,'value'=>$memberDetail['Member']['image'])); ?>
              </span>
         <?php
                if($memberDetail['Member']['image'] && $this->Utility->getAWSDisplayUrl(MEMBERS_BUCKET,$memberDetail['Member']['image']))
                {
                    $image = $memberDetail['Member']['image'];
                  
                    ?>
                    <a class="fileinput-new remove_cover_image" id=""><?php echo __("remove_photo");?></a>
                    <?php } ?>
              <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?php echo __('remove_photo');?></a>
            </div>
<div class="clearfix"></div>
          </div>

        <?php /*echo $this->Html->image('front/img_placeholder.png',array('alt'=>'', "width"=>"165", "height"=>"162" ));*/ ?> 
        <div class="profile_upload">
<!--        	<span class="Link"><a href="#">Upload photo</a></span>
        	<span class="Link"><a href="#">Remove photo</a></span>-->
        </div> 
        <div class="checkbox text-left">
            <?php
            if($memberDetail['Member']['notification']){$check= True;$check_n1=1;}else{$check=  false;$check_n1=0;}
            ?>
            <label> <p><?php echo __("tick_this_box_if_you_are_happy_for_us_to_tell_you_about_relevant_news_and_new_courses_that_may_interest_you");?> <?php echo $this->Form->input('notification',array('type'=>'checkbox','checked'=>$check,"class"=>"Checkbox2 pull-right",'id'=>'notification','value'=>$check_n1,'label'=>false,'div'=>false)); ?></p></label>
        </div>
       <?php echo $this->Form->end(); ?>
    </div>
    
        
    </div>
    
    
    
    </div>
  </div>
    
    
     <!-- Modal -->
                <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            	<h4 class="modal-title" id="myModalLabel"><?php echo __('choose_a_new_password');?></h4>
                            </div>
                            <div class="modal-body"> 
                                <?php echo $this->Form->create('change_password',array('id'=>'resetpassword','div'=>false,'method'=>'post','url'=>array('controller'=>'Members','action'=>'reset_password')));?>
                                <div class="form-group">
                                <label for="new_password"><?php echo __('new_password:');?></label>                                
                                <?php echo $this->Form->input('new_password',array('type'=>'password','id'=>'new_password','class'=>'form-control TextPwd',"placeholder"=>__('new_password:'),'label'=>false,'div'=>false)); ?> 
                                <div id="err_new_password" class="register_error alert-danger"> <?php if(isset($error['new_password'][0])) echo $error['new_password'][0];?> </div>
                               </div>
                                 <div class="form-group">
                                    <label for="confirmPassword"><?php echo __('confirm_password');?></label>
                                    <?php echo $this->Form->input('confirm_password',array('type'=>'password','id'=>'confirmPassword','class'=>'form-control TextPwd',"placeholder"=>__('confirm_password'),'label'=>false,'div'=>false)); ?> 
                                 <div id="err_confirm_password" class="register_error alert-danger"> <?php if(isset($error['confirm_password'][0])) echo $error['confirm_password'][0];?> </div>
                               </div>
                                <?php echo $this->Form->submit(__('submit'),array('class'=>'btn btn-primary btn-lg ButtonBlue','onclick'=>"return ajax_form('resetpassword','Members/validate_reset_password_ajax','load_top')")); ?>
                            </div>                            
                        </div>
                    </div>
                </div>
</section>
<script type="text/javascript">
$(document).ready(function(){


$("#givenName").on('keyup',function(){
	var msg = '';
		var inputgivenname = $('#givenName').val();		
		if(inputgivenname == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_given_name").text(msg);
		}
		else {
			$("#err_given_name").text('');
		}

});

$("#email").on('keyup',function(){



	var msg = '';
		var inputValEmail = $('#email').val();		
		
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,40})?$/;
		if(inputValEmail == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_email").text(msg);
		}
		else {
			$("#err_email").text('');
		}
		if( !emailReg.test( inputValEmail ) ) {
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_email").text("<?php echo __('please_enter_valid_email'); ?>");
		} 
		


});
/*
$(".c-description").each(function(index){
$("#c_description"+index).on('keyup',function(){
	var msg = '';
		var inputdescription = $("#c_description"+index).val();		
		if(inputdescription == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_about_me-"+index).text(msg);
		}
		else {
			$("#err_about_me-"+index).text('');
		}

});   

});  
*/




	$('#new_password').on('keyup',function(){ checkVal();	});	
	
	
	$('#new_password').on('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#new_password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		if(inputValPass == ''){
			msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_new_password").text(msg);
		}
		else {
			$("#err_new_password").text('');
		}
		if((inputValPass != '' && inputValConPass != '') && (inputValPass != inputValConPass)){
			msg = '<?php echo __(PASSWORD_MISTACHMATCH);?>';
			$("#err_confirm_password").text(msg);
		}
		else {
			$("#err_confirm_password").text('');
		}
		  
		  
	});
	$('#confirmPassword').on('focusout',function(){ 	
		var msg = '';
		var inputValPass = $('#new_password').val();	
		var inputValConPass = $('#confirmPassword').val();		
		  if(inputValConPass == ''){
			  msg = '<?php echo __(FIELD_REQUIRED);?>';
		  	  $("#err_confirm_password").text(msg);
		  }
		 else if((inputValPass != '') && (inputValPass != inputValConPass)){
			  msg = '<?php echo __(PASSWORD_MISTACHMATCH);?>';
			  $("#err_confirm_password").text(msg);
		  }
		  else {
			$("#err_confirm_password").text('');
		  }
	});

function checkVal()
{
	if($('#new_password').val().length < 8 || $('#new_password').val().length > 20){
	$(".length").removeClass("valid").addClass('invalid');
	}else{
		$(".length").removeClass("invalid").addClass('valid');
	}
	
	if($('#new_password').val().match(/[A-Z]/)){
		$(".upperCase").removeClass("invalid").addClass('valid');
	}else{
		$(".upperCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[a-z]/)){
		$(".lowerCase").removeClass("invalid").addClass('valid');
	}else{
		$(".lowerCase").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[0-9]/)){
		$(".number").removeClass("invalid").addClass('valid');
	}else{
		$(".number").removeClass("valid").addClass('invalid');
	}
	
	if($('#new_password').val().match(/[!@#$%^&*()\-_=+{};:,<.>~]/)){
		$(".alpha").removeClass("invalid").addClass('valid');
	}else{
		$(".alpha").removeClass("valid").addClass('invalid');
	}
}




$('.ntct_chk').click(function(){
    $('#notification_chk').val(1);
});
});

function checkvalidate(abc)
{
var id=abc.getAttribute("id");
if($("#"+id).text()!='Dropdpwn'){
var ids=id.split("_");
	
$("#err_lang_locale-"+ids[ids.length-1]).text('');


}


}

function textkeyup(abc){
var id1=abc.getAttribute("id");
var ids1=id1.split("_");
if(abc.value!='')
{
$("#err_about_me-"+ids1[ids1.length-1]).text('');
}
else{
msg = '<?php echo __(FIELD_REQUIRED);?>';
			$("#err_about_me-"+ids1[ids1.length-1]).text(msg);
}
}
</script>
<?php /*echo $this->element('frontElements/home/reset_password');*/ ?> 
<!--End  main container section -->
