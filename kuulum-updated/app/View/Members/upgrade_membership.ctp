<?php echo '';;?>
<script type="text/javascript">
$('document').ready(function(){
	$('#author_term_condition').on('click',function(){
	 if($(this).is(':checked')){
		$(this).val('1');
		}else{
	$(this).val('0');
		}
	});
});

</script>
<!-- main container section -->
<?php if($this->Session->read("LocTrain.flashMsg") != ''){ ?>
        <div class="account-activated">	<?php echo __($this->Session->read("LocTrain.flashMsg")); ?></div>
    <?php $this->Session->delete("LocTrain.flashMsg");  } ?>
          <?php echo $this->Form->create('Member',array('id'=>'upgrade','div'=>false,'method'=>'post','url'=>array('controller'=>'Members','action'=>'upgrade_membership')));?>
<section id="bcoming-autor"  class="bcoming-author first">
<div class="container" >
<div class="row">
<div class="col-lg-12">
<h4 class="modal-title"><?php echo $title ?></h4>
</div>
</div>
</div>
</section>
  <?php echo $description; ?>

<section class="bcoming-author last">
<div class="container" >
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="Title"><?php echo __('are_you_ready_to_become_an_author');?></div>
<p><?php echo __('i_have_read_and_agree_with_the');?> 
   <?php echo $this->Html->link(__('author_terms_and_conditions'),array('controller'=>'Home','action'=>'author_terms'),array());?>.
<?php echo $this->Form->input('author_term_condition',array('type'=>'checkbox','label'=>false,'div'=>false,'id'=>'author_term_condition','checked'=>($member_details['Member']['author_term_condition']==1)?'checked':'','class'=>'delte_conted added_learning_dsbl','value'=>'0')); ?>
   </p><br>
  <div id="err_author_term_condition" class="register_error alert-danger"> <?php if(isset($error['author_term_condition'][0])) echo $error['author_term_condition'][0];?> </div>
<p><?php echo __('i_would_like_to_be_kept_informed_about_relevant_author_news_best_practices_and_promotions');?> 
   <?php echo $this->Form->input('author_notification',array('type'=>'checkbox','label'=>false,'div'=>false,'checked'=>($member_details['Member']['author_notification']==1)?'checked':'','class'=>'delte_conted added_learning_dsbl ','id'=>'','value'=>0)); ?>
</p>
<div id="err_author_notification" class="register_error alert-danger"> <?php if(isset($error['author_notification'][0])) echo $error['author_notification'][0];?> </div>

<span class="Link">
    <?php
if($check_author_member > 0){ 
        echo $this->Form->submit(__('make_me_an_author'),array('class'=>'sc_btns disRegisBut', 'style'=>'color:#7f7f7f !important; text-decoration: none;','disabled'=>"disabled",'div'=>'section_links','onclick'=>"return ajax_form('upgrade','Members/validate_upgrade_ajax','load_top')"));
    }else if(!empty($member_details)){
        echo $this->Form->submit(__('make_me_an_author'),array('class'=>'sc_btns disRegisBut', 'style'=>'color:#7f7f7f !important; text-decoration: none;', 'disabled'=>"disabled",'div'=>'section_links','onclick'=>"return ajax_form('upgrade','Members/validate_upgrade_ajax','load_top')"));
    }else{
    echo $this->Form->submit(__('make_me_an_author'),array('class'=>'sc_btns disRegisBut', 'div'=>'section_links','onclick'=>"return ajax_form('upgrade','Members/validate_upgrade_ajax','load_top')"));
    
    } 
 ?>
</span>
<span class="Link">
    <?php echo $this->Form->submit(__('cancel'),array('class'=>'sc_btns disRegisBut', 'div'=>'section_links', 'onclick'=>'upgrade.action="'. HTTP_ROOT.'Home/about_company"')); ?>
   </span>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
 <div class="Title"><?php echo __('what_happens_next'); ?></div>
<p> <?php echo __('as_an_author_you_will_have_access_to_a_section_of_the_training_portal_called_my_teaching_this_is_where_you_can_manage_your_courses_and_lessons'); ?><br /><br />
 <?php echo __('you_will_also_find_author_related_information_that_will_help_you_create_the_perfect_course');?></p>
</div>
</div>
</div>
</section>
           <?php echo $this->Form->end(); ?>

<!--End  main container section --> 
<style>
    
.inner_catalogue {
    border: 1px solid #ddd;
    border-radius: 10px;
    box-shadow: 1px 1px 1px #ddd;
    float: left;
    width: 100%;
}

.main_catalogue {
    display: table-cell;
    padding: 10px;
    width: 98%;
}
.formfld_cont p {
    float: left;
    font-size: 13pt;
    font-weight: bold;
    line-height: 17px;
    width: 100%;
}
.reg_flds_decp p {
    font-weight: normal !important;
}

.ipsumpara span {
    float: left;
    padding: 2px;
}
.aliqua {
    font-size: small !important;
    width: auto;
}
.ipsum_size {
    font-size: 26px !important;
    width: auto;
}
.minim {
    font-size: medium !important;
    width: auto;
}
.ipsum_sm {
    font-size: 19px !important;
    width: auto;
}
.ipsumpara a{color: #0070c0;}
.ipsumpara a span {
    color: #0070c0 !important;
}
    </style>
