<?php //echo $this->Html->script('jquery.validate.js'); ?>
<?php echo $this->Html->script('admin/js/ckfinder/ckfinder.js');?>
<script type="text/javascript">

	$(document).ready(function() {
		CKFinder.setupCKEditor( null, ajax_url+'js/admin/js/ckfinder/' );
		var editor = CKEDITOR.replaceAll('editor1',{
			
			filebrowserBrowseUrl : ajax_url+'js/admin/js/ckfinder/ckfinder.html',
	        filebrowserImageBrowseUrl : ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Flash',
			filebrowserUploadUrl : ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserImageUploadUrl : ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
			filebrowserFlashUploadUrl : ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
		
		});
		
		$('#tabs, #tabs2, #tabs5').tabs();

		/*$('#add_category').validate();*/
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			
			$('#lang_locale').val($lang);
		});
	
	<?php if(!empty($contents)){ ?>
		var data = $(".content").html();
	 	CKEDITOR.instances.editor1.setData(data);
	<?php } ?>
		
	/*$("#addnews").submit(function (){
	 $("#err_description_"+$lang).text('');
	 $("#err_title"+$lang).text('');
		var a = CKEDITOR.instances.editor1.getData();
		var title = $("#pageid").val();
		
		var text = $(a).text();
		if(text == '' && title==''){
			$("#err_description").text('This field is required');
			$("#err_title").text('This field is required');
			return false;
		}
		if(text == '' && title!='')
		{
			$("#err_description").text('This field is required');
			return false;
		}
		if(text != '' && title=='')
		{
			$("#err_title").text('This field is required');
			return false;
		}
	});	*/
	});
	
</script>

<div id="sub-nav">
  <div class="page-title">
    <h1> ADD BANNERS</h1>
  </div>
</div>
<div id="page-layout">
<div id="page-content">
<div class="content" style="display:none;"><?php echo $contents;?></div>
  <div id="page-content-wrapper" class="no-bg-image wrapper-full">
    <div class="inner-page-title">
      <h2>ADD BANNERS</h2>
	  <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
      <span></span> </div>
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
    <div class="content-box content-box-header" style="border:none;">
	  <div class="column-content-box">
       <div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span> Banner Information </div>
		
		<div id="tabs"> 
			<ul>
				<?php $i=1; foreach($language as $lang){  ?>
					<li><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>	
			</ul>	
   
           <div class="content-box-wrapper">
			
				<?php echo $this->Form->create('Banners',array('class'=>'editTemplateForm','id'=>'addnews','url'=>array('controller'=>'users','action'=>'add_banners'),'enctype'=>'multipart/form-data')); ?>
				<input type="hidden" id="lang_locale" value="en" name="data[Banners][locale]"/>	
				<fieldset>
				<?php //pr($language);die; ?>
				  <?php 
					$i=1; 
					foreach($language as $lang)
					{
						$locale = $lang['Language']['locale'];								
					?>
			
				<div class="" id="tabs-<?php echo $i; ?>" title="<?php echo $lang['Language']['locale']; ?>">
					<ul>
					  <li>
						<span class="mandatory">*</span> <label class="desc" ><?php echo __('Title in ').$lang['Language']['language']; ?></label>
						 
						<div> <?php echo $this->Form->input('title_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>'field text full required')); ?> </div>
					   <p class="university-add-Error" id="err_title_<?php echo $locale; ?>">
						  <?php if(isset($error['title'.$locale][0])) echo $error['title'.$locale][0]; ?>
						</p>
					  </li>
					 
					
					  <li>
						<label class="desc" >Image</label>
						 
						<div > <?php echo $this->Form->input('image_'.$locale,array('id'=>'image','type'=>'file','div'=>false,'label'=>false,'class'=>'field text full required')); ?> </div>
						<div class="note">Please provide only jpg,png,gif file <div>
						 <p class="university-add-Error" id="err_image">
						  <?php if(isset($error['image'][0])) echo $error['image'][0]; ?>
						</p>
					  </li>
				
					  <li>
					 <label class="desc" ><?php echo __('Description in ').$lang['Language']['language']; ?></label>
						<div style="float:left; width:100%;"> <textarea class="editor1" style="width:100%;" name="data[Banners][description_<?php echo $locale; ?>]"> </textarea></div>
						<p class="university-add-Error" id="err_description_<?php echo $locale; ?>">
						  <?php if(isset($error['description_'.$locale][0])) echo $error['description_'.$locale][0]; ?>
						</p>
					  </li>
					  <li>
						  <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("addnews","Users/validate_add_news_ajax","newloading") '/>
						<!--<input class="sub-bttn" type="submit" value="Submit" />-->
					  </li>
					 
					</ul>
				</div>
			 <?php  $i++; }  ?>   
            </fieldset>
			<?php echo $this->form->end(); ?>  
          </div>
		</div>
     </div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul>
					<?php */?>
    
      <div class="clearfix"></div>
     <!-- <div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
  
    <div class="clear"></div>
  </div>
  </div>
</div>
<div class="clear"></div>
            
