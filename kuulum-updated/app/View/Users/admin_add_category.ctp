<script type="text/javascript">
	
	$(document).ready(function() {
	
	//	$('#tabs, #tabs2, #tabs5').tabs();

		$('#add_category').validate();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('rel');
			
			$('#lang_locale').val($lang);
		});
	});
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Add course category <br><small>Add a course category</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
				<?php $i++;
                                }
                                ?>	
			</ul>
                   
<!--<div id="sub-nav">
  <div class="page-title">
    <h1>Add Category</h1>
  </div>
</div>
<div id="page-layout">
  <div id="page-content">
    <div id="page-content-wrapper" class="no-bg-image wrapper-full">
      <div class="inner-page-title"> 
        <h2>Add Category</h2>
        <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> </div> -->
      <?php if($this->Session->check('success')){ ?>
      <div class="success ui-corner-all successdeveloperClass" id="success"> <span class='successMessageText'> <?php echo $this->Session->read('success');?> </span> </div>
      <?php $this->Session->delete('success'); ?>
      <?php } ?>
      <!--<div class="content-box content-box-header" style="border:none;">
		
		<div id="tabs"> 
			<ul>
				<?php $i=1; foreach($language as $lang){  ?>
					<li><a class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>	
			</ul>  
	  
        <div class="column-content-box">
          
          <div class="content-box-wrapper">-->
			<?php echo $this->Form->create('CourseCategory',array('url'=>array('controller'=>'users','action'=>'add_category'),'id'=>'add_category',"class"=>"js-validation-bootstrap form-horizontal editTemplateForm")); ?>
			<input type="hidden" id="lang_locale" value="en" name="data[CourseCategory][locale]"/>	
        <div class="block-content tab-content">  
			<?php 
				$i=1; 
				foreach($language as $lang)
				{
					$locale = $lang['Language']['locale'];								
			?>
					
			 <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
                <div class="form-group">
                <label for="val-username" class="col-md-1 text-right"> Category:</label>
                <div class="col-md-6">
					<!-- <span class="mandatory">*</span> <label class="desc" >Category</label>-->
					 <?php echo $this->Form->input($locale,array('type'=>'text','id'=>$locale,'div'=>false,'label'=>false,'class'=>'form-control field text full')); ?>
<span class="university-add-Error" id="err_<?php echo $locale;?>">
						  <?php if(isset($error['title'][0])) echo $error['title'][0]; ?>
						</span>
</div></div>
						
					 
					  <div class="form-group">
                <div class="col-md-8 col-md-offset-1">
						 <input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("add_category","Users/validate_add_category_ajax","newloading") '/>
					<!--<input class="sub-bttn" type="submit" value="Submit" />-->
					   </div> </div>
					
			</div>	
			<?php  $i++;  } ?>
           <?php echo $this->form->end(); ?>
          </div>
        </div>
		</div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
      </div>
      <div class="clearfix"></div>
    <!--  <div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
    </div>


<div class="clear"></div>