<?php //echo $this->Html->script('jquery.validate.js'); ?>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Add commission <br><small>Add commission details</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
  <div class="block">

    <div class="block-content">

    <div id="page-content-wrapper" class="no-bg-image wrapper-full">        
      <div class="inner-page-title">
     <!-- <h2><?php echo __('Add Commission');?></h2> 
    <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>-->
      <span></span> </div>    
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
  <!--<div class="content-box content-box-header" style="border:none;">
  
      <div class="column-content-box">
        <div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span>Add Commission</div>
        <div id="tabs" class="add-commissions"> -->

 <?php echo $this->Form->create('Commission',array('class'=>'js-validation-bootstrap form-horizontal editTemplateForm',"div"=>false ,'id'=>'addCommission01','url'=>array('controller'=>'users','action'=>'add_commission'))); ?>
           <!--      <div id="tabs1">
            <div class="content-box-wrapper"> -->
            
              <div class="errorcheck"></div>
              
     <!--    <ul>                 
                  <li>-->
            <div class="form-group">
                                  <label for="val-username" class="col-md-2 ">Commission Value:</label>                                            <div class="col-md-7">
                                  
            <?php echo $this->Form->input('Commission.commission',array('type'=>'text','id'=>'commission01','required'=>'required','value'=>'','div'=>false,'label'=>false,"placeholder"=>"Enter a percentage value...",'class'=>'form-control field text full required')); ?> 
                                      
                        <div class="error-commission"></div>
                                  </div></div>
            
                       <div class="form-group">
                                  <label for="val-username" class="col-md-2 ">Select Author:</label>                                            <div class="col-md-7">
                       
            <?php 
echo $this->Form->input(
    'Commission.author_name',
    array("id"=>"CommissionAuthorName01",'required'=>'required','onchange'=>'selectcomm_courses();',"type"=>'select',"class"=>"form-control","label"=>false,"div"=>false,'options' => $members,'empty'=>'Please select an author from the list...',) 
    
);
?>
<div class="error-author_name"></div>
                                  </div></div>
 <div class="form-group">
                                  <label for="val-username" class="col-md-2 ">Select duration:</label>                                            <div class="col-md-7">
<?php
echo $this->Form->input('Commission.duration',array('options'=>array('Limited','Unlimited'),'required'=>'required','empty'=>'Please select a duration','id'=>'duration01','onchange'=>"dateduration();",'label'=>false,"div"=>false,"class"=>"form-control"));


?>
<div class="error-duration"></div>
</div></div>

<script>
 $(function() {
     

    $("#datepicker11" ).datepicker({format:'m/d/yyyy'}).on('changeDate', function(selected){        
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#datepicker12').datepicker('setStartDate', startDate);
       
    }); 
      $( "#datepicker12" ).datepicker({format:'m/d/yyyy'}).on('changeDate', function(selected){        
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#datepicker11').datepicker('setEndDate', FromEndDate);
       
    });
  });
  </script>
  <div class="form-group" id="durationdateselection01" style="display: none;margin-bottom: 30px;">
                                            <label for="example-daterange1" class="col-md-2 ">Date Range</label>
                                            <div class="col-md-7">
                                                <div style="display:table;">
                                                    <?php echo $this->Form->input('Commission.date_from',array("value"=>"","placeholder"=>"From","class"=>"form-control","readonly"=>true,'type'=>'text','id'=>'datepicker11',"label"=>false,"style"=>"text-align:center;"))?>
                                                  
                                                 <div class="error-date_from" style="position:absolute;margin-top:2px;"></div> 
                                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                                    <?php echo $this->Form->input('Commission.date_to',array("value"=>"","placeholder"=>"To","class"=>"form-control","readonly"=>true,'type'=>'text','id'=>'datepicker12',"label"=>false,"style"=>"text-align:center;"))?>
                                                 <div class="error-date_to" style="position:absolute;margin-top:2px;"></div>
                                                <!--    <input type="text" placeholder="To" name="example-daterange2" id="datepicker12" class="form-control"> -->
                                                </div>
                                            </div>
                                        </div>
 
 <div class="form-group">
<label for="val-username" class="col-md-2 ">Select courses:</label>
<div class="col-md-7">
<?php
    echo $this->Form->input('Commission.courses',array('options'=>array(),'required'=>'required','multiple'=>true,'empty'=>'Please select one or more courses from the list...','id'=>'select-course01','onchange'=>"coursesselection();",'label'=>false,"class"=>"form-control"));?>
<div class="error-courses"></div>
</div></div>
<div class="form-group">
<div class="col-md-7 col-md-offset-2">
<?php
//echo $this->Form->select('Commission.particular-course','',
//    array("id"=>'CommissionParticular-course01','multiple' => true,"label"=>false,'style'=>'display:none',"class"=>"form-control")
//  
//);


?>
<div class="error-particular-course"></div>
 </div>    
 </div>    

<?php echo $this->Form->input('Commission.date',array('type'=>'hidden','value'=>date('Y-m-d H:i:s'),'div'=>false,'label'=>false)); ?>
            
        
          
          <p class="university-add-Error" id="err_commission">
                    </p>
                 <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                   <!-- <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("addCommission","Users/validate_add_commission_ajax","newloading") '/>-->
            <input class="btn btn-sm btn-primary" type="button" value="Submit" onclick="checknumber('');"/>
                  
             
           
          <?php echo $this->Form->end(); ?> </div>
       
      </div>
</div>
     </div>
         
      <div class="clearfix"></div>
      <!--<div id="sidebar">
        
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            
