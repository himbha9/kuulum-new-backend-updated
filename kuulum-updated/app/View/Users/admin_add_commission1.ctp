<?php //echo $this->Html->script('jquery.validate.js'); ?>
<div id="sub-nav">
  <div class="page-title">
    <h1><?php echo __('Add Commission'); ?></h1>
  </div>
</div>
<div id="page-layout">
<div id="page-content">
  <div id="page-content-wrapper" class="no-bg-image wrapper-full">
    <div class="inner-page-title">
      <h2><?php echo __('Add Commission');?></h2>
	  <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
      <span></span> </div>	  
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
    <div class="content-box content-box-header" style="border:none;">
	
      <div class="column-content-box">
        <div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span>Add Commission</div>
        <div id="tabs"> <?php echo $this->Form->create('Commission',array('class'=>'editTemplateForm', 'id'=>'addCommission','url'=>array('controller'=>'users','action'=>'add_commission'),'onsubmit'=>'return checknumber()')); ?>
          <div id="tabs1">
            <div class="content-box-wrapper">
              <fieldset>
                <ul>                 
                  <li>
                   <label class="desc" ><?php echo __('Commission Value');?></label>
                   
						<?php echo $this->Form->input('Commission.commission',array('type'=>'text','id'=>'commission','value'=>'','div'=>false,'label'=>false,'class'=>'field text full required',)); ?>
						
						<?php 


echo $this->Form->select(
    'Commission.author_name',
    $members,
    array('multiple' => true)
	  
);

echo $this->Form->input('Commission.duration',array('options'=>array('Limited','Unlimited'),'empty'=>'Select Duration','id'=>'duration','onchange'=>"dateduration();",'label'=>false));


?>

<script>
 $(function() {
    $( "#datepicker" ).datepicker({
      showOn: "button",
      buttonImage: "<?php echo $this->webroot; ?>/img/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
	    $( "#datepicker1" ).datepicker({
      showOn: "button",
      buttonImage: "<?php echo $this->webroot; ?>/img/calendar.gif", 
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  });
  </script>
  <div id="durationdateselection" style='display:none'>
<?php echo $this->Form->input('Commission.date_from',array('type'=>'text','id'=>'datepicker'))?>
<?php echo $this->Form->input('Commission.date_to',array('type'=>'text','id'=>'datepicker1'))?>

</div>
<?php
echo $this->Form->input('Commission.courses',array('options'=>array('All Courses','Particular Courses'),'empty'=>'Select Courses','id'=>'select-course','onchange'=>"coursesselection();",'label'=>false));
echo $this->Form->select('Commission.particular-course',
    $courses,
    array('multiple' => true,'style'=>'display:none;')
	
);


?>

<?php echo $this->Form->input('Commission.date',array('type'=>'hidden','value'=>date('Y-m-d H:i:s'),'div'=>false,'label'=>false)); ?>
						</div>
				
					
					<p class="university-add-Error" id="err_commission">
                    </p>
                  </li>
                  <li>
                   <!-- <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("addCommission","Users/validate_add_commission_ajax","newloading") '/>-->
					  <input class="sub-bttn" type="submit" value="Submit"/>
                  </li>
                </ul>
              </fieldset>
            </div>
          </div>
          <?php echo $this->Form->end(); ?> </div>
       
      </div>
      <div class="clearfix"></div>
      <!--<div id="sidebar">
        
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            
