

<script type="text/javascript">
	
	$(document).ready(function(){
		
		$('#category_id').change(function(){
				
				$cat_id = $.trim($(this).val());
				
				if($cat_id != '' && $cat_id != 0)
				{
					$.get(ajax_url+'Users/find_subcategories',{'category_id':$cat_id}, function(resp){
							
							if($.trim(resp)!='no')
							{
								$('#subcatDiv').show();
								$('#subcategory_id').html(resp);
								
							} else {
								
								$('#subcatDiv').hide();
								$('#subcategory_id').val('NULL');
								
								$('#slsubcatDiv').hide();
								$('#sl_subcategory_id').val('NULL');
							}
							
					});
					
				} else {
				
					$('#subcatDiv').hide();
					$('#subcategory_id').val('NULL');
								
					$('#slsubcatDiv').hide();
					$('#sl_subcategory_id').val('NULL');
				}
		});
		
		$('#subcategory_id').change(function(){
				
				$subcat_id = $.trim($(this).val());
				
				if($subcat_id != '' && $subcat_id != 0)
				{
					$.post(ajax_url+'Users/find_sl_subcategories',{'subcategory_id':$subcat_id}, function(resp){
							
							if($.trim(resp)!='no')
							{
								$('#slsubcatDiv').show();
								$('#sl_subcategory_id').html(resp);
								
							} else {
								
								$('#slsubcatDiv').hide();
								$('#sl_subcategory_id').val('NULL');
							}
							
					});
				} else {
					$('#slsubcatDiv').hide();
					$('#sl_subcategory_id').val('NULL');
				}
		});
		
	});
	
</script>

<div id="sub-nav">
	<div class="page-title">
		<h1>Add Course</h1>
	</div>
</div>

<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2>Add Course</h2>
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> </div>
					
				<div class="content-box content-box-header" style="border:none;">
					<div class="column-content-box">
						<div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span> Add Course </div>
						<div class="content-box-wrapper">
							<?php echo $this->Form->create('Course',array('url'=>array('controller'=>'users','action'=>'add_courses'),'id'=>'add_degree')); ?>
								<fieldset>
									<ul>
										<li>
											<label class="desc" >Select Author</label>
											<div>
												<?php echo $this->Form->input('m_id',array('type'=>'select','id'=>'title','div'=>false,'label'=>false,'class'=>'field select full required','options'=>$authors)); ?>
												<p class="university-add-Error" id="err_m_id">
													<?php if(isset($error['m_id'][0])) echo $error['m_id'][0]; ?>
												</p>
											</div>
										</li>
										<li>
											<span class="mandatory">*</span> <label class="desc" >Title</label>
											<div>
												<?php echo $this->Form->input('title',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
												<p class="university-add-Error" id="err_title">
													<?php if(isset($error['title'][0])) echo $error['title'][0]; ?>
												</p>
											</div>
										</li>
										<li>
											<span class="mandatory">*</span><label class="desc" >Description</label>
											<div>
												<?php echo $this->Form->input('description',array('type'=>'textarea','id'=>'discription','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
												<p class="university-add-Error" id="err_description">
													<?php if(isset($error['description'][0])) echo $error['description'][0]; ?>
												</p>
											</div>
										</li>
										<li>
											<label class="desc" >Primary Language</label>
											<div>
												<?php echo $this->Form->input('primary_lang',array('type'=>'select','id'=>'student_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>$language)); ?>
												<p class="university-add-Error" id="err_primary_lang">
													<?php if(isset($error['primary_lang'][0])) echo $error['primary_lang'][0]; ?>
												</p>
											</div>
										</li>
										<li>
											<span class="mandatory">*</span><label class="desc" >Category</label>
											<div>
												<?php $categories[0] = '***others***'; ?>
												<?php echo $this->Form->input('category_id',array('type'=>'select','id'=>'category_id','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array(''=>'Select Category',$categories))); ?>
												<p class="university-add-Error" id="err_category_id">
													<?php if(isset($error['category_id'][0])) echo $error['category_id'][0]; ?>
												</p>
											</div>
										</li>
										<li style="display:none;" id="subcatDiv">
											<span class="mandatory">*</span><label class="desc" >Sub-Category</label>
											<div>
												<?php echo $this->Form->input('subcategory_id',array('type'=>'select','id'=>'subcategory_id','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array(''=>'Select Sub Category','NULL'=>'NULL'))); ?>
												<p class="university-add-Error" id="err_subcategory_id">
													<?php if(isset($error['subcategory_id'][0])) echo $error['subcategory_id'][0]; ?>
												</p>
											</div>
										</li>
										<li style="display:none;" id="slsubcatDiv">
											<span class="mandatory">*</span><label class="desc" >SL Sub-Category</label>
											<div>
												<?php echo $this->Form->input('sl_subcategory_id',array('type'=>'select','id'=>'sl_subcategory_id','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array(''=>'Select SL Sub Category','NULL'=>'NULL'))); ?>
												<p class="university-add-Error" id="err_sl_subcategory_id">
													<?php if(isset($error['sl_subcategory_id'][0])) echo $error['sl_subcategory_id'][0]; ?>
												</p>
											</div>
										</li>
										<li>
											<label class="desc" >Select Keywords</label>
											<div>
												<select name="data[Course][keyword][]" multiple="multiple"   id="subjectDomain">
													<?php foreach($interests as $key=>$value):?>
													<option value="<?php echo $value;?>"><?php echo $value;?></option>
													<?php endforeach;?>
												</select>
												<label >Hold ctrl key to select multiple Keywords </label>
											</div>
										</li>
										<li>
											<label class="desc" >Current Status</label>
											<div>
												<?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>'1')); ?>
												<p class="university-add-Error" id="err_status">
													<?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
												</p>
											</div>
										</li>
										<li>
											<input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("add_degree","Users/validate_cours_ajax","loading") '/>
											<div class="loading" style="margin-left: -37% !important;"> <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px;'));?> </div>
										</li>
									</ul>
								</fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div id="sidebar">
					<?php //echo $this->element('adminElements/left_right_bar');?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<div class="clear"></div>