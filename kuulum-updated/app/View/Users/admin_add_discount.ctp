<?php ?>
<script type="text/javascript">

    $(document).ready(function() {
        
        
        $('.lang_tab').click(function(){ 
            
            $lang = $(this).attr('title');
            
            $('#lang_locale').val($lang);
        });
    });
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Add discount code <br><small>Add the details of discount code</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
    <div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
                <?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
                <?php $i++;
                                }
                                ?>  
            </ul>
                    <?php
                    if($this->Session->check('success'))
                    {
                    ?>
                    <div class="success ui-corner-all successdeveloperClass" id="success">
                    <span>
                    <?php echo $this->Session->read('success');?>
                    </span>
                    </div>
                    <?php $this->Session->delete('success'); ?>
                    <?php 
                    }
                    ?>
    
                   <?php echo $this->Form->create('Discount',array('class'=>'js-validation-bootstrap form-horizontal editTemplateForm', 'id'=>'adddiscount','url'=>array('controller'=>'users','action'=>'add_discount'))); ?>
                <input type="hidden" id="lang_locale" value="en" name="data[Discount][locale]"/>
                <div class="block-content tab-content">  
                <?php
                $i=1; 
                foreach($language as $lang)
                {
                $locale = $lang['Language']['locale'];      
                ?>
                <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
                <div class="form-group">
                <label for="val-username" class="col-md-2 "> Title:</label>
                <div class="col-md-7">
                <?php echo $this->Form->input('name_'.$locale,array('id'=>'pageid','type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required', "placeholder"=>"Enter title.")); ?>
                <span class="university-add-Error" id="err_name_<?php echo $locale;?>"></span>
                </div>
                </div>
                <div class="form-group">
                <label for="val-username" class="col-md-2"> Description:</label>
                <div class="col-md-7">
                <textarea class="form-control" placeholder="Enter description" name="data[Discount][description_<?php echo $locale; ?>]" ></textarea>
                <span class="university-add-Error" id="err_description_<?php echo $locale; ?>">
                </span>
                </div>
                </div>
                <?php 
                if($i==1)
                {
                ?>
                <div class="form-group">
                <label for="val-username" class="col-md-2 ">Discount Value:</label>
                <div class="col-md-7">
                <div>
                <?php echo $this->Form->input('initial_value',array('id'=>'pageid','type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required',"placeholder"=>"Enter discount value in percentage.")); ?> </div>
        <span class="university-add-Error" id="err_initial_value">
                </span>
                </div>
                </div>
                <div class="form-group">
                <label for="val-username" class="col-md-2 "> Discount code:</label>
                <div class="col-md-7">
                <?php echo $this->Form->input('discount_code',array('type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required', "placeholder"=>"Enter discount code.")); ?>
                <span class="university-add-Error" id="err_discount_code"></span>
                </div>
                </div>
<script>
             $(function() {
                $("#datepicker11" ).datepicker({format:'m/d/yyyy'}).on('changeDate', function(selected){        
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#datepicker12').datepicker('setStartDate', startDate);

                }); 
                        $( "#datepicker12" ).datepicker({format:'m/d/yyyy'}).on('changeDate', function(selected){        
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#datepicker11').datepicker('setEndDate', FromEndDate);

                });
              });
              </script>
                <div class="form-group" style="margin-bottom:30px;">
                <label for="example-daterange1" class="col-md-2 ">Date Range:</label>
                <div class="col-md-1">
                <?php echo $this->Form->input('Discount.unlimited_time',array('type'=>'checkbox','value'=>1,'id'=>'unlimited_time',"label"=>"Unlimited"))?>
                <span class="university-add-Error" id="err_unlimited_time"></span>
                </div>
            
                <div class="col-md-6">
                <div style="display:table;" class="input_daterange">
                <?php echo $this->Form->input('Discount.date_from',array("placeholder"=>"From","class"=>"form-control","readonly"=>true,'type'=>'text','id'=>'datepicker11',"label"=>false,"style"=>"text-align:center;"))?>
                 <span class="university-add-Error" id="err_date_from" style="position:absolute;margin-top:2px"></span>
                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                <?php echo $this->Form->input('Discount.date_to',array("placeholder"=>"To","class"=>"form-control","readonly"=>true,'type'=>'text','id'=>'datepicker12',"label"=>false,"style"=>"text-align:center;"))?>
                <span class="university-add-Error" id="err_date_to" style="position:absolute;margin-top:2px"></span>
                </div>
               
                
                </div>
                </div>
                <div class="form-group">
                <label for="val-username" class="col-md-2 ">Select Author:</label>
                <div class="col-md-7">
                <?php 
                echo $this->Form->select(
                    'Discount.author_name',
                    $published_authors    ,
                    array("id"=>"DiscountAuthorName01",'multiple' => true,'required'=>'required','onchange'=>'selectcourses();selectmembers();',"class"=>"form-control","div"=>false) 

                );
                ?>
                <span class="university-add-Error" id="err_author_name"></span>
                </div>
                </div>
                <div class="form-group">
                <label for="val-username" class="col-md-2 ">Select courses:</label>  
                <div class="col-md-7">
                <?php
                echo $this->Form->select('Discount.particular-course','',
                    array("id"=>'DiscountParticular-course01','multiple' => true,"label"=>false,"class"=>"form-control")
                        );
                ?>
                <span class="university-add-Error" id="err_particular-course"></span>
                </div>    
                </div>  
                <div class="form-group">
                <label for="val-username" class="col-md-2 ">Select Members:</label>
                <div class="col-md-7">
                <?php
                echo $this->Form->select('Discount.members','',
                    array("id"=>'DiscountMember01','multiple' => true,"label"=>false,"class"=>"form-control")
                        );
                ?>
                <span class="university-add-Error" id="err_members"></span>
                </div>
                </div>
                <?php
                }
                ?>
                <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                <input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("adddiscount","Users/validate_add_discount_ajax","newloading") '/>
                </div>
                </div>
                </div>
                <?php 
                ++$i;
                }
                ?>
                </div>
                <?php 
                echo $this->Form->end();
                ?>                 
    <div class="clear"></div>
  </div>
</div>
    </div>
<div class="clear"></div>


<!-- Disable Date Range fields if unlimited checkbox is true -->
<script>
$(document).ready(function(){
    $("#unlimited_time").click(function(){
        if($("#unlimited_time").is(":checked")==true)
        {
            $("#datepicker11").val("");
            $("#datepicker12").val("");
            $(".input_daterange").parent().css("display","none");
        }
        else
        {
            $(".input_daterange").parent().css("display","block");
        }
    })
})
</script>
            
