<!--<?php echo $this->Html->css('uploadify');?>
<?php echo $this->Html->script('jquery.uploadify');?>
<?php echo $this->Html->script('/jwplayer/jwplayer');?> -->


<script type="text/javascript">
		
	$(document).ready(function()
	{
		//$('#tabs, #tabs2, #tabs5').tabs();

		$('#add_category').validate();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			
			$('#lang_locale').val($lang);
		}); 
		
		
	
});
		
	
</script> 

<?php $type=array('free_video'=>'Add a YouTube video','own_video'=>'Add your own video'); ?>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                           Add third party video <br><small>Add a new third party video.</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php $i=1; foreach($language as $lang){  ?>
                                        <li <?php if($i==1)echo "class='active'";?>><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>	
                                          <li class="pull-right">
<button class="btn btn-minw btn-primary" type="button" onclick="history.go(-1);">Back</button>
</li>
			</ul>

      <?php if($this->Session->check('error')){ ?>
      <div class="response-msg error ui-corner-all"> <span>Success message</span> <?php echo $this->Session->read('error');?> </div>
      <?php $this->Session->delete('error'); ?>
      <?php } ?>
 
	  <?php echo $this->Form->create('FreeVideo',array('url'=>array('controller'=>'users','action'=>'add_free_video'),'enctype'=>'multipart/form-data','id'=>'add_free_video',"class"=>"js-validation-bootstrap form-horizontal editTemplateForm"));
			?>
		  <input type="hidden" id="lang_locale" value="en" name="data[FreeVideo][locale]"/>
	  <div class="block-content tab-content">  
		<?php 
					$i=1; 
					foreach($language as $lang)
					{
						$locale = $lang['Language']['locale'];								
					?>
			 <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
	 <div class="form-group">
                                  <label for="val-username" class="col-md-1 "> Title:</label>                                            <div class="col-md-6">
                 
                  <?php echo $this->Form->input('title_'.$locale,array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'form-control field text full required',"placeholder"=>"Enter title..")); ?>
                    <span class="university-add-Error" id="err_title_<?php echo $locale; ?>">
                      <?php if(isset($error['title_'.$locale][0])) echo $error['title_'.$locale][0]; ?>
                    </span>
                                  </div></div>
                 <div class="form-group">
                                  <label for="val-username" class="col-md-1"> Description:</label>                                            <div class="col-md-6">
					
						<?php echo $this->Form->input('description_'.$locale,array('type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>'',"placeholder"=>"Enter description..")); ?>
						<span class="university-add-Error" id="err_description_<?php echo $locale;?>">
							<?php if(isset($error['description_'.$locale][0])) echo $error['description_'.$locale][0]; ?>
						</span>
                                  </div></div>
               
                <?php if($i==1) { ?>
				 <div class="form-group">
                                  <label for="val-username" class="col-md-1"> Web URL:</label>                                            <div class="col-md-6">
				
						 <?php echo $this->Form->input('free_video_url',array('type'=>'text','id'=>'url','div'=>false,'label'=>false,'placeholder'=>'Enter the public Youtube URL','class'=>'form-control field text full required')); ?>
						
						<span class="university-add-Error" id="err_free_video_url">
							<?php if(isset($error['free_video_url'][0])) echo $error['free_video_url'][0]; ?>
						</span>
                                                </div></div>
              
			<?php } ?>				
                   <div class="form-group">
                                            <div class="col-md-8 col-md-offset-1">
                  <input class="btn btn-sm btn-primary jamm sc_btns" id="submitId" type="submit" value="Submit" onclick='return ajax_form("add_free_video","Users/validate_add_free_video_ajax","newloading") '/>
                  
                 </div>   </div>
               
			  </div>
				<?php  $i++; }  ?>  
         </div>
			 <?php echo $this->Form->end(); ?> 
          <div class="clear"></div>
          </div>
        <div class="clear"></div>
	  </div>
    <div class="clear"></div>
</div>
