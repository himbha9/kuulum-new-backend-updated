<div id="sub-nav">
  <div class="page-title">
    <h1>Add Lesson</h1>
  </div>
</div>
<div id="page-layout">
  <div id="page-content">
    <div id="page-content-wrapper" class="no-bg-image wrapper-full">
      <div class="inner-page-title">
        <h2>Add Lesson</h2>
        <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> </div>
      <?php if($this->Session->check('error')){ ?>
      <div class="response-msg error ui-corner-all"> <span>Success message</span> <?php echo $this->Session->read('error');?> </div>
      <?php $this->Session->delete('error'); ?>
      <?php } ?>
      <div class="content-box content-box-header" style="border:none;">
        <div class="column-content-box">
          <div class="content-box-wrapper"> <?php echo $this->Form->create('CourseLesson',array('url'=>array('controller'=>'users','action'=>'admin_add_lesson'),'enctype'=>'multipart/form-data','id'=>'add_lesson'));
		  	echo $this->Form->input('mem_id',array('type'=>'hidden', 'value'=>$mem_id['Course']['m_id'])); ?>
            <fieldset>
              <ul>
                <li>
                 <span class="mandatory">*</span> <label class="desc" >Title</label>
                  <div> <?php echo $this->Form->input('title',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
                    <p class="university-add-Error" id="err_title">
                      <?php if(isset($error['title'][0])) echo $error['title'][0]; ?>
                    </p>
                  </div>
                </li>
               				 

				<?php echo $this->Form->input('course_id',array('type'=>'hidden','value'=>$mem_id['Course']['id'])); ?>
				  
                <li>
                 <span class="mandatory">*</span> <label class="desc" >Description</label>
                  <div>  <?php echo $this->Form->input('description',array('type'=>'textarea','id'=>'discription','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
                    <p class="university-add-Error" id="err_description">
                      <?php if(isset($error['description'][0])) echo $error['description'][0]; ?>
                    </p>
                  </div>
                </li>
				<li>
                    <label class="desc" >Video</label>
					 
                    <div> <?php echo $this->Form->input('CourseLesson.video',array('id'=>'image','type'=>'file','div'=>false,'label'=>false,'class'=>'field text full required')); ?> </div>
					<div class="note">Please provide only mp4,flv file <div>
					<p class="university-add-Error" id="err_image">
                      <?php if(isset($error['image'][0])) echo $error['image'][0]; ?>
                    </p>
                  </li>                
                <li>
                  <label class="desc" >status</label>
                  <div> <?php echo $this->Form->input('status',array('type'=>'select','id'=>'student_status','div'=>false,'label'=>false,'class'=>'field text full required','style'=>'height:25px','options'=>array('0'=>'In-Active','1'=>'Active'))); ?>
                    <p class="university-add-Error" id="err_lat">
                    </p>
                  </div>
                </li>
                <li>
                  <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("add_lesson","Users/validate_add_lesson_ajax","newloading") '/>
                  <div class="newloading"> <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px'));?> </div>
                </li>
              </ul>
            </fieldset>
            </form>
          </div>
        </div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
      </div>
      <div class="clearfix"></div>
      <!--<div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
 <div class="clear"></div>