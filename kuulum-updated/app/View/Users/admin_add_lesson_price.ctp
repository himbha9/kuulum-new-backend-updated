<?php //echo $this->Html->script('jquery.validate.js'); ?>
<div id="sub-nav">
  <div class="page-title">
    <h1> Add Lesson Price</h1>
  </div>
</div>
<div id="page-layout">
<div id="page-content">
  <div id="page-content-wrapper" class="no-bg-image wrapper-full">
    <div class="inner-page-title">
      <h2>Lesson Price</h2>
	  <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
      <span></span> </div>
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
    <div class="content-box content-box-header" style="border:none;">
      <div class="column-content-box">
        <div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span>Lesson Price</div>
        <div id="tabs"> <?php echo $this->Form->create('LessonPrice',array('class'=>'editTemplateForm', 'id'=>'editprice','url'=>array('controller'=>'users','action'=>'add_lesson_price'))); ?>
          <div id="tabs1">
            <div class="content-box-wrapper">
              <fieldset>
                <ul>                 
                  <li>
                    <label class="desc" >Price Amount</label>
					  
                    <div> <?php echo $this->Form->input('LessonPrice.price',array('id'=>'pageid','type'=>'text','div'=>false,'label'=>false,'class'=>'field text full required')); ?> </div>
					<p class="university-add-Error" id="err_price">
                    </p>
                  </li>
                  
                  <li>
                    <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("editprice","Users/validate_edit_lesson_price_ajax","newloading") '/>
                  </li>
                </ul>
              </fieldset>
            </div>
          </div>
          <?php echo $this->Form->end(); ?> </div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul>
					<?php */?>
      </div>
      <div class="clearfix"></div>
     <!-- <div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            
