
<script type="text/javascript">
		
		$(function(){
			
			$('#image').bind('change',function(){
		
				var imagePath = $('#image').val();//document.FormTwo.picFile.value;
				var pathLength = imagePath.length;
				var lastDot = imagePath.lastIndexOf(".");
				var fileType = imagePath.substring(lastDot,pathLength);	
				var fileType = fileType.toLowerCase();
				
				$('#file_hidden').val(imagePath); 

				
			});
			
			$('#mem_type').change(function(){
					
				if($.trim($(this).val())=='3') // if author
				{
					$('.author_li').slideDown();
				}else{
					$('.author_li').slideUp();
				}	
					
			});
			
		});
		
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Add member <br><small>Add the information to add a new member or author</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">


		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				<!--<h2>Add Member</h2> -->
		<!--		<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span>
			</div>
			<?php if($this->Session->check('success')){ ?>
				<div class="success ui-corner-all successdeveloperClass" id="success"> <span class='successMessageText'> <?php echo $this->Session->read('success');?> </span> </div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
				
			 <div class="content-box content-box-header" style="border:none;">
				<div class="column-content-box">
					<div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span> Add Member </div>
					<div class="content-box-wrapper"> -->
						<?php echo $this->Form->create('Member',array('url'=>array('controller'=>'users','action'=>'add_newMember'),'id'=>'add_member','enctype'=>'multipart/form-data',"class"=>"js-validation-bootstrap form-horizontal")); ?>
			<div class="form-group">
                                       <label for="val-username" class="col-md-2 " ><span class="">*  </span>Preferred name:</label>
                                       <div class="col-md-7">	
                        <?php echo $this->Form->input('final_name',array('type'=>'text','id'=>'title','placeholder'=>'Enter preferred name','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?>
			<span class="university-add-Error" id="err_final_name">
		<?php if(isset($error['final_name'][0])) echo $error['final_name'][0]; ?>
		</span>
</div>		</div>	
								
										
            <div class="form-group">
                                  <label for="val-username" class="col-md-2"><span class="">*  </span>Email:</label>                                            <div class="col-md-7">
                                            <?php echo $this->Form->input('email',array('type'=>'text','id'=>'title','placeholder'=>'Enter email address','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?>									<span class="university-add-Error" id="err_email">
<?php if(isset($error['email'][0])) echo $error['email'][0]; ?> </span>
</div>
</div>
		<div class="form-group">
                                            <label for="val-username" class="col-md-2 " ><span class="">*  </span>Language:</label>
                                            <div class="col-md-7">							
										<?php  $locale = array(''=>'Select language','en'=>'English','de'=>'German','ar'=>'Arabic','fr'=>'French','it'=>'Italian','ja'=>'Japanese','ko'=>'Korean','pt'=>'Portuguese','ru'=>'Russian','zh'=>'Chinese','es'=>'Spanish') ?>
										
										<?php echo $this->Form->input('locale',array('type'=>'select','id'=>'mem_type','div'=>false,'label'=>false,'class'=>'form-control field select full required','options'=>$locale,'selected'=>'2')); ?>

<span class="university-add-Error" id="err_locale">
<?php if(isset($error['locale'][0])) echo $error['locale'][0]; ?> </span></div>
</div>
<div class="form-group">
                                            <label for="val-username" class="col-md-2 " ><span class="">*  </span>Status:</label>
                                            <div class="col-md-7">	
			<?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'class'=>'form-control field select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>'1')); ?>
							<span class="university-add-Error" id="err_status">
							<?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
											</span>
										</div>
</div>
	<div class="form-group">
                                            <label for="val-username" class="col-md-2 " ><span class="">*  </span>Email Preferences:</label>
                                            <div class="col-md-7">	

										 <?php echo $this->Form->input('notification',array('type'=>'select','id'=>'','div'=>false,'label'=>false,'class'=>'form-control field select full required','options'=>array('0'=>'Opt Out','1'=>'Opt In'),'selected'=>'1')); ?>
							<span class="university-add-Error" id="err_status">
												<?php if(isset($error['notification'][0])) echo $error['notification'][0]; ?>
											</span>
										</div></div>
									<!--
							<span class="mandatory">*</span><label class="desc" >Password:</label>				 
						<?php echo $this-> Form->input('password',array('type'=>'password','id'=>'title','div'=>false,'label'=>false,'class'=>' field text full required')); ?>
											<label>Note:Password must have  minimum of 8 and maximum of 20 characters at least one upper case character at least one lower case character at least one number and at least one non-alphanumeric character.</label>
											<p class="university-add-Error" id="err_password">
												<?php if(isset($error['password'][0])) echo $error['password'][0]; ?>
											</p>
										</div></div>
									</li>
									<li>
										<span class="mandatory">*</span> <label class="desc" >Given name:</label>
										<div> <?php echo $this->Form->input('given_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
											<p class="university-add-Error" id="err_given_name">
												<?php if(isset($error['given_name'][0])) echo $error['given_name'][0]; ?>
											</p>
										</div>
									</li>
									<li>
										<label class="desc" >Family name:</label>
										<div> <?php echo $this->Form->input('family_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
											<p class="university-add-Error" id="err_family_name">
												<?php if(isset($error['family_name'][0])) echo $error['family_name'][0]; ?>
											</p>
										</div>
									</li>
									<li>
										<label class="desc" >Other names:</label>
										<div> <?php echo $this->Form->input('other_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
											<p class="university-add-Error" id="err_other_name">
												<?php if(isset($error['other_name'][0])) echo $error['other_name'][0]; ?>
											</p>
										</div>
									</li>
									<li>
										<span class="mandatory">*</span> <label class="desc" >What should we call you?</label>
										<div> <?php echo $this->Form->input('final_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
											<p class="university-add-Error" id="err_final_name">
												<?php if(isset($error['final_name'][0])) echo $error['final_name'][0]; ?>
											</p>
										</div>
									</li>
									<li>
										<label class="desc" >Keep me informed:</label>
										<div> <?php echo $this->Form->input('notification',array('type'=>'select','id'=>'','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'No','1'=>'Yes'),'selected'=>'1')); ?>
											<p class="university-add-Error" id="err_status">
												<?php if(isset($error['notification'][0])) echo $error['notification'][0]; ?>
											</p>
										</div></div>
									<div class="form-group">
                                            <label for="val-username" class="col-md-2 " ><span class="">*  </span>Status:</label>
                                            <div class="col-md-7">	
										 <?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>'1')); ?>
											<p class="university-add-Error" id="err_status">
												<?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
											</p>
										</div>
</div> -->
							<div class="form-group">
                                            <label for="val-username" class="col-md-2 " ><span class="">*  </span>Role Type:</label>
                                            <div class="col-md-7">			
										<?php echo $this->Form->input('role_id',array('type'=>'select','id'=>'mem_type','div'=>false,'label'=>false,'class'=>'form-control field select full required','options'=>$role,'selected'=>'2')); ?>
											<span class="university-add-Error" id="err_status">
												<?php if(isset($error['role_id'][0])) echo $error['role_id'][0]; ?>
											</span>
										</div></div>

<div class="form-group">
                                            <label for="val-username" class="col-md-2 " >Keywords:</label>
                                            <div class="col-md-7">			
										<?php echo $this->Form->input('keywords',array('class'=>'text full form-control','type'=>'text','placeholder'=>'Enter keywords','label'=>false,'div'=>false)); ?>
											
											
										</div></div>

<div class="form-group">
                                            <label for="val-username" class="col-md-2" >Biography:</label>
                                            <div class="col-md-7">

											<?php echo $this->Form->input('about_me',array('class'=>'text full form-control','type'=>'textarea','placeholder'=>'Enter memer bio','label'=>false,'div'=>false)); ?>											
											<span class="university-add-Error" id="err_about_me">
											</span>
										</div></div>
									
										<!--<label class="desc" >Title</label>				   
										<div> <?php echo $this->Form->input('gender',array('type'=>'select','id'=>'student_status','div'=>false,'label'=>false,'class'=>'field select full required','style'=>'height:25px','options'=>array(''=>'Select','Mr'=>'Mr','Miss'=>'Miss','Mrs'=>'Mrs'),'selected'=>'Male')); ?>
											<span class="university-add-Error" id="err_lat">
											</span>
										</div>
									</li>
									<li class="author_li" style="display:none;"> -->
													   
										<div class="form-group">
                                            <label for="val-username" class="col-md-2 " >Picture:</label>

                                            <div class="col-md-6"> 
<div class="col-md-2" style="padding-left:0px !important">
                                       
                                        <img class=" img-avatar-new" src="<?php echo HTTP_ROOT; ?>img/admin_new_theme/avatars/avatar4.jpg" alt="">
</div>
<label>File Input</label><br>
                                            
                                    
											<?php echo $this->Form->input('image',array('name'=>'image',"class"=>"",'type'=>'file','id'=>'image','label'=>false,'div'=>false)); ?>
											<?php echo $this->Form->input('picture',array('type'=>'hidden','id'=>'file_hidden','label'=>false,'div'=>false)); ?>
										

	
											<span class="university-add-Error" id="err_picture">
											</span>
										</div>
</div>
								
									
									<div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
										<input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("add_member","Users/validate_add_newMember_ajax","loading") '/>
</div>
                                        </div>
										<!--<div class="loading" style="margin-left: -37% !important;"> <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px;'));?> </div> -->
									
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		<!--	<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
