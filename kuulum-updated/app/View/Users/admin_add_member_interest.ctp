
<script type="text/javascript">
		
	
		$(document).ready(function(){
				
				$('#add_member_interest').validate();
				
		});
		
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Add member interest<br><small>Add a member interest
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				<!--<h2>Add Member</h2> -->

						<?php echo $this->Form->create('MemberKeywords',array('url'=>array('controller'=>'users','action'=>'add_member_interest'),'id'=>'add_member_interest','enctype'=>'multipart/form-data',"class"=>"js-validation-bootstrap form-horizontal")); ?>
<input type="hidden" id="m_id" name="data[MemberKeywords][role_id]" value="<?php echo $role_id; ?>"/>
<input type="hidden" id="m_id" name="data[MemberKeywords][m_id]" value="<?php echo $m_id; ?>"/>
			<div class="form-group">
                                       <label for="val-username" class="col-md-1 text-right " >Interest:</label>
                                       <div class="col-md-6">	
                        <?php echo $this->Form->input('keyword',array('type'=>'text','id'=>'keyword','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?>
			<span class="university-add-Error" id="err_final_name">
		<?php if(isset($error['final_name'][0])) echo $error['final_name'][0]; ?>
		</span>
</div>		</div>	
								
										
            	
									<div class="form-group">
                                            <div class="col-md-8 col-md-offset-1">
										<input class="btn btn-sm btn-primary" type="submit" value="Submit" />
<!-- onclick='return ajax_form("add_member_interest","Users/validate_keywords","loading") ' -->
</div>
                                        </div>
										<!--<div class="loading" style="margin-left: -37% !important;"> <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px;'));?> </div> -->
									
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		<!--	<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
