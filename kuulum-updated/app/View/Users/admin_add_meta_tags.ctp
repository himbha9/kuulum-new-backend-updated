<?php //echo $this->Html->script('jquery.validate.js'); ?>
<style type="text/css">
form li {
	clear: none;
}
</style>
<script type="text/javascript">

	$(document).ready(function() {		
		
		//$('#tabs, #tabs2, #tabs5').tabs();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			
			$('#lang_locale').val($lang);
		});
	
	});
	
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                          Add Meta Tag <br><small>Add meta tags for portal pages</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
				<?php $i++;
                                }
                                ?>
 <li class="pull-right">
<button class="btn btn-minw btn-primary" type="button" onclick="history.go(-1);">Back</button>
</li>	
			</ul>
                   
              
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
    <?php echo $this->Form->create('MetaTag',array('class'=>'','id'=>'addmetatags','url'=>array('controller'=>'users','action'=>'add_meta_tags'),'enctype'=>'multipart/form-data',"class"=>'js-validation-bootstrap form-horizontal editTemplateForm')); ?>
    <div class="block-content tab-content"> 
                   
              
				
				<input type="hidden" id="lang_locale" value="en" name="data[MetaTag][locale]"/>	
				
				
				  <?php 
					$i=1; 
					foreach($language as $lang)
					{
						$locale = $lang['Language']['locale'];								
					?>
			
				    <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
					<?php if($i==1)
                                        {
                                            ?>
                                 
                <div class="form-group">
                <label for="val-username" class="col-md-2 "> Page Title:</label>
                <div class="col-md-7">
                                             
						 
                 <?php echo $this->Form->input('page_title',array('id'=>'page_title','type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required',"placeholder"=>"Enter title..")); ?> <span class="university-add-Error" id="err_page_title">
                          <?php if(isset($error['page_title'][0])) echo $error['page_title'][0]; ?>
                        </span> </div></div>
                       
                    <div class="form-group">
                <label for="val-username" class="col-md-2 "> Page URL:</label>
                <div class="col-md-7">                    
                   
              <?php echo $this->Form->input('page_url',array('id'=>'page_url','type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required',"placeholder"=>"Enter the page URL..")); ?> 
                    <span class="university-add-Error" id="err_page_url">
                      <?php if(isset($error['page_url'][0])) echo $error['page_url'][0]; ?>
                    
                    </span>
                            </div>   </div>           <?php  } ?>
                    
                    <div class="form-group">
                <label for="val-username" class="col-md-2 "> Meta Title :</label>
                <div class="col-md-7"> 
			 <?php echo $this->Form->input('meta_title_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>' form-control field text full required',"placeholder"=>"Enter the meta title for the page..")); ?> 
                                                <span class="university-add-Error" id="err_meta_title_<?php echo $locale; ?>">
						  <?php if(isset($error['meta_title_'.$locale][0])) echo $error['meta_title_'.$locale][0]; ?>
                                                </span>
                    </div>
                    </div>
					  <div class="form-group">
                <label for="val-username" class="col-md-2 "> Meta Keywords :</label>
                <div class="col-md-7"> 
                      <?php echo $this->Form->input('meta_keywords_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>' form-control field text full required',"placeholder"=>"Enter Meta keywords seperated by a comma..")); ?> 
		
					   <span class="university-add-Error" id="err_meta_keywords_<?php echo $locale;?>">
						  <?php if(isset($error['meta_keywords_'.$locale][0])) echo $error['meta_keywords_'.$locale][0]; ?>
				</span>
                    </div>
                    </div>
					 <div class="form-group">
                <label for="val-username" class="col-md-2 "> Meta Description:</label>
                <div class="col-md-7"> 
                     <?php echo $this->Form->input('meta_description_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>' form-control field text full required',"placeholder"=>"Enter a meta description..")); ?> 
                
						<span class="university-add-Error" id="err_meta_description_<?php echo $locale; ?>">
						  <?php if(isset($error['meta_description_'.$locale][0])) echo $error['meta_description_'.$locale][0]; ?>
                                                </span>
                    </div>
					</div>
					 <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
		
						  <input class="btn btn-primary" type="submit" value="Submit" onclick='return ajax_form("addmetatags","Users/validate_metatags_ajax","newloading") '/>
						
                                    </div></div>
				</div>
			 <?php  $i++; }  ?>   
          
			</div>
    <?php echo $this->form->end(); ?>    
          </div>
		</div>
       
     </div>
       
    
      <div class="clearfix"></div>
     <!-- <div id="sidebar">
        
      </div> -->
      <div class="clear"></div>
  
    <div class="clear"></div>
  
  </div>
</div>
<div class="clear"></div>
            
