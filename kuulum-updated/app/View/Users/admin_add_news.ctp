<?php //echo $this->Html->script('jquery.validate.js'); ?>
<?php echo $this->Html->script('admin/js/ckfinder/ckfinder.js');?>
<script type="text/javascript">

	$(document).ready(function() {
		CKFinder.setupCKEditor( null, ajax_url+'js/admin/js/ckfinder/' );
		
		var editor = CKEDITOR.replaceAll('editor1',{
			
			filebrowserBrowseUrl : ajax_url+'js/admin/js/ckfinder/ckfinder.html',
	        filebrowserImageBrowseUrl : ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Flash',
			filebrowserUploadUrl : ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserImageUploadUrl : ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
			filebrowserFlashUploadUrl : ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
		
		});
		
	//	$('#tabs, #tabs2, #tabs5').tabs();

		$('#add_category').validate();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			
			$('#lang_locale').val($lang);
		}); 

	<?php if(!empty($contents)){ ?>
		var data = $(".content").html();
	 	CKEDITOR.instances.editor1.setData(data);
	<?php } ?>
		
	/*$("#addnews").submit(function (){
	 $("#err_description_"+$lang).text('');
	 $("#err_title"+$lang).text('');
		var a = CKEDITOR.instances.editor1.getData();
		var title = $("#pageid").val();
		
		var text = $(a).text();
		if(text == '' && title==''){
			$("#err_description").text('This field is required');
			$("#err_title").text('This field is required');
			return false;
		}
		if(text == '' && title!='')
		{
			$("#err_description").text('This field is required');
			return false;
		}
		if(text != '' && title=='')
		{
			$("#err_title").text('This field is required');
			return false;
		}
	});	*/
	});
	
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Add news story <br><small>Add a news story to the frontend</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>
<div id="content" class="content">
	<div class="block">
             <ul data-toggle="tabs" class="nav nav-tabs">
				<?php $i=1; foreach($language as $lang){  ?>
                                        <li <?php if($i==1)echo "class='active'";?>><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>
                                          <li class="pull-right">
<button class="btn btn-minw btn-primary" type="button" onclick="history.go(-1);">Back</button>
</li>
			</ul>
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>

			
				<?php echo $this->Form->create('News',array('class'=>'js-validation-bootstrap form-horizontal editTemplateForm','id'=>'addnews','url'=>array('controller'=>'users','action'=>'add_news'),'enctype'=>'multipart/form-data')); ?>
				<input type="hidden" id="lang_locale" value="en" name="data[News][locale]"/>	
				 <div class="block-content tab-content">  
				<?php //pr($language);die; ?>
				  <?php 
					$i=1; 
					foreach($language as $lang)
					{
						$locale = $lang['Language']['locale'];								
					?>
                                      <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
				 
                                         <div class="form-group">
                                  <label for="val-username" class="col-md-1 "> Title:</label>                                            <div class="col-md-7">
						
						 
					<?php echo $this->Form->input('title_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?>
                                       <span class="university-add-Error" id="err_title_<?php echo $locale; ?>">
					  
						  <?php if(isset($error['title'.$locale][0])) echo $error['title'.$locale][0]; ?>
						</span>
                                     </div>
                                         </div>
                                     
					   <div class="form-group">
                                  <label for="val-username" class="col-md-1 "> Subtitle:</label>                                            <div class="col-md-7">
					 <?php echo $this->Form->input('sub_title_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?> 
                                  <span class="university-add-Error" id="err_sub_title_<?php echo $locale;?>">
						  <?php if(isset($error['sub_title_'.$locale][0])) echo $error['sub_title_'.$locale][0]; ?>
						</span>
                                  </div> </div>
					   
					   <!--      <div class="form-group">
                                  <label for="val-username" class="col-md-2 ">Meta Title:</label>                                            <div class="col-md-7">
						
						 
					<?php echo $this->Form->input('title_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?>
                                     </div>
                                         </div>
                                                
                                     
					   <div class="form-group">
                                  <label for="val-username" class="col-md-2 "> Meta Keywords:</label>                                            <div class="col-md-7">
					 <?php echo $this->Form->input('sub_title_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?> </div> </div>
                                      <span class="university-add-Error" id="err_title_<?php echo $locale; ?>">
					  
						  <?php if(isset($error['title'.$locale][0])) echo $error['title'.$locale][0]; ?>
						</span>
					   <div class="form-group">
                                  <label for="val-username" class="col-md-2 "> Meta Description:</label>                                            <div class="col-md-7">
					 <?php echo $this->Form->input('sub_title_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?> </div> </div>-->
                                         <?php if($i==1){ ?>
					 <div class="form-group">
                                            <label for="val-username" class="col-md-1 " >Image:</label>

                                            <div class="col-md-6"> 
<div class="col-md-2" style="padding-left:0px !important">
                                       
                                        <img class=" img-avatar-new" src="<?php echo HTTP_ROOT; ?>img/admin_new_theme/avatars/avatar4.jpg" alt="">
</div>
<label>File Input</label><br>
					  <?php echo $this->Form->input('image',array('id'=>'image','type'=>'file','div'=>false,'label'=>false,'class'=>'field text full required')); ?> 
						
						 <span class="university-add-Error" id="err_image">
						  <?php if(isset($error['image'][0])) echo $error['image'][0]; ?>
						</span>
                                         </div></div> <?php } ?>
					 <div class="form-group">
                                                                        <div class="col-md-12">
						 <textarea class="editor1" style="width:100%;" name="data[News][description_<?php echo $locale; ?>]"> </textarea>
						<span class="university-add-Error" id="err_description_<?php echo $locale; ?>">
						  <?php if(isset($error['description_'.$locale][0])) echo $error['description_'.$locale][0]; ?>
						</span>
                                                </div>
                                                                        </div>
					  <div class="form-group">
                                            <div class="col-md-8 ">
						  <input class="btn  btn-primary" type="submit" value="Submit" onclick='return ajax_form("addnews","Users/validate_add_news_ajax","newloading") '/>
						<!--<input class="sub-bttn" type="submit" value="Submit" />-->
                                                  </div></div>
				</div>
			 <?php  $i++; }  ?>   
                                     
                                 </div>
			<?php echo $this->form->end(); ?>  
          </div>
		</div>
     </div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul>
					<?php */?>
    
      <div class="clearfix"></div>
     <!-- <div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
  
    <div class="clear"></div>

<div class="clear"></div>
            
