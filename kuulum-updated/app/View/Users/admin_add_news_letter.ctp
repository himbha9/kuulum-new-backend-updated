<?php echo $this->Html->script('jquery.validate.js'); ?>
<?php  echo $this->Html->script('newadmin/sidebar_position.js');?>
<?php  echo $this->Html->script('newadmin/ui/ui.tabs.js');?>
<?php echo $this->Html->script('ui/ui.tabs.js');?>


<script type="text/javascript">
	
	$(document).ready(function() {
		
		$('#tabs, #tabs2, #tabs5').tabs();
		CKEDITOR.replaceAll('editor1');
		
	});
	
</script>

<div id="sub-nav">
  <div class="page-title">
   <h1>Add News</h1>
  </div>
</div>
<div id="page-layout">
<div id="page-content">
  <div id="page-content-wrapper" class="no-bg-image wrapper-full">
    <div class="inner-page-title">
      <h2>Add News Letter</h2>
      <span></span> 
	</div>
	<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a>
				<span></span>
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
	
	<div class="content-box content-box-header" style="border:none;">
				
		<div class="ui-state-default ui-corner-top ui-box-header">

			<span class="ui-icon float-left ui-icon-notice"></span>

			Add News Letter Detail

		</div>
				
		<div id="tabs"> 
			<ul>
				<?php $i=1; foreach($language as $lang){  ?>
					<li><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>
			</ul>
			
			
			
			
			<form class="packageForm" method="post" enctype="multipart/form-data" action="<?php echo HTTP_ROOT.'admin/users/add_news_letter'; ?>">
			
			<!--<form class="editCMSForm" method="post" action="<?php echo HTTP_ROOT;?>admin/users/edit_cms"> -->
			
				<?php 
				$i=1; 
				//pr($language);die;
				foreach($language as $lang)
				{
					$locale = $lang['Language']['locale'];								
			?>
			
					<div id="tabs-<?php echo $i; ?>">
						
						<div class="content-box-wrapper">
							<input  name="data[<?php echo $i; ?>][Newsletter][locale]" type="hidden" value="<?php echo $locale; ?>" readonly="readonly" />
						
							
							<fieldset>
								<ul>
									<li>
										<label class="desc" >Title</label>
										<div><input  class="field text full required" name="data[<?php echo $i; ?>][Newsletter][title]" type="text"/></div>
									</li>

									<li>
										<label class="desc" >Description</label>
										<div>
										
											<textarea class="tinymce required editor1" style="width:100%;" name="data[<?php echo $i; ?>][Newsletter][description]" id=""></textarea>
										</div>
									</li>                                       
									<li>
										<input class="sub-bttn" type="submit" value="Submit"/>
									</li>
								</ul>
							</fieldset>
							
						</div>
					</div>
			<?php  $i++;  } ?>
			</form>
		</div>
			
		</div>
			<div class="clearfix"></div>
		</div>
	
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            
