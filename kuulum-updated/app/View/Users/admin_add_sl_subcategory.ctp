
<script type="text/javascript">
	
	$(document).ready(function() {
	
		$('#tabs, #tabs2, #tabs5').tabs();
	
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('rel');
			$('#lang_locale').val($lang);
		});
		
		
	});
</script>

<div id="sub-nav">
	<div class="page-title">
		<h1>Add Second Level Sub-Category</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2>Add Second Level Sub-Category</h2>
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a>
				<span></span>
			</div>
			<div class="content-box content-box-header" style="border:none;">
				<div class="column-content-box">
					<div id="tabs"> 
						<ul>
							<?php $i=1; foreach($language as $lang){ ?>
								<li><a  class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>" href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
							<?php $i++; } ?>	
						</ul>
						
						<div class="content-box-wrapper">
							<?php echo $this->Form->create('CourseSlSubcategory',array('url'=>array('controller'=>'users','action'=>'add_sl_subcategory'),'id'=>'add_sl_subcategory')); ?>
				<input type="hidden" id="lang_locale" value="en" name="data[CourseSlSubcategory][locale]"/>
								<?php echo $this->Form->input('category_id',array('type'=>'hidden','value'=>$cat_id, 'div'=>false , 'label'=>false ));  ?>
								<?php echo $this->Form->input('sub_category_id',array('type'=>'hidden','value'=>$sub_cat_id, 'div'=>false , 'label'=>false ));  ?>
							
								<?php 
									$i=1; 
									foreach($language as $lang)
									{
										$locale = $lang['Language']['locale'];								
								?>
							  
										<div id="tabs-<?php echo $i; ?>">	
			  
											<ul>
												<li>
													<span class="mandatory">*</span> <label class="desc" >SL Sub-Category</label>
													<div><?php echo $this->Form->input($locale,array('type'=>'text','id'=>'name','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
														<p class="university-add-Error" id="err_<?php echo $locale; ?>">
															<?php if(isset($error[$locale][0])) echo $error[$locale][0]; ?>
														</p>

													</div>
												</li>
												
												<?php /*if($i==1) { ?>
												
													<li>
														<label class="desc" >Current Status</label>
														<div> <?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'style'=>'height:25px','class'=>'select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>'1')); ?>
															<p class="university-add-Error" id="err_status">
																<?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
															</p>
														</div>
													</li>
													
												<?php } */ ?>	
												
												<li>
													<input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("add_sl_subcategory","Users/validate_sl_subcategory_ajax","newloading") '/>
													<div class="loading" style="margin-left: -37% !important;"> <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px;'));?> </div>
												</li>
											</ul>
										
										</div>								
								<?php 	
										$i++;
									} 
								?>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>	
				</div>        
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>