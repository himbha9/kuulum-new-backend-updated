
<script type="text/javascript">
	
	$(document).ready(function() {
	
		//$('#tabs, #tabs2, #tabs5').tabs();

	//	$('#add_subcategory').validate();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('rel');
			//alert($lang);
			$('#lang_locale').val($lang);
		});
		
	});
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Add course subategory <br><small>Add a course subcategory</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" rel="<?php echo $lang['Language']['locale']; ?>" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
				<?php $i++;
                                }
                                ?>	
			</ul>
                   
<!--<div id="sub-nav">
  <div class="page-title">
    <h1>Add Sub-Category</h1>
  </div>
</div>
<div id="page-layout">
  <div id="page-content">
    <div id="page-content-wrapper" class="no-bg-image wrapper-full">
      <div class="inner-page-title">
        <h2>Add Sub-Category</h2>
        <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> </div> -->
      <?php if($this->Session->check('success')){ ?>
      <div class="success ui-corner-all successdeveloperClass" id="success"> <span class='successMessageText'> <?php echo $this->Session->read('success');?> </span> </div>
      <?php $this->Session->delete('success'); ?>
      <?php } ?>
     <!-- <div class="content-box content-box-header" style="border:none;">
        <div class="column-content-box">
          
		  <div id="tabs"> 
			<ul>
				<?php $i=1; foreach($language as $lang){ ?>
					<li><a class="lang_tab" rel="<?php echo $lang['Language']['locale']; ?>" href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>	
			</ul>
		  
          <div class="content-box-wrapper"> -->
			<?php echo $this->Form->create('CourseSubcategory',array('url'=>array('controller'=>'users','action'=>'add_subcategory'),'id'=>'add_subcategory',"class"=>"js-validation-bootstrap form-horizontal editTemplateForm")); ?>
			<?php echo $this->Form->input('category_id',array('type'=>'hidden','value'=>$c_id, 'div'=>false , 'label'=>false ));  ?>
			<input type="hidden" id="lang_locale" value="en" name="data[CourseSubcategory][locale]"/>	
           <div class="block-content tab-content">  
			<?php 
				$i=1; 
				foreach($language as $lang)
				{
					$locale = $lang['Language']['locale'];								
			?>
		  
         <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
                <div class="form-group">
                <label for="val-username" class="col-md-2 control-label"> Subcategory:</label>
                <div class="col-md-7">
                <!-- <span class="mandatory">*</span> <label class="desc" >Sub-Category</label>-->
                   <?php echo $this->Form->input($locale,array('type'=>'text','id'=>$locale,'div'=>false,'label'=>false,'class'=>'field text full required form-control')); ?>

                    <span class="university-add-Error" id="err_<?php echo $locale; ?>">
                      <?php if(isset($error[$locale][0])) echo $error[$locale][0]; ?>
                    </span> </div> </div>
                 
               
				<?php /* if($i==1) { ?>
                <li>
                  <label class="desc" >Current Status</label>
                  <div> <?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>'1')); ?>
                    <p class="university-add-Error" id="err_status">
                      <?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
                    </p>
                  </div>
                </li>
				<?php } */ ?>
                <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                   <input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("add_subcategory","Users/validate_add_subcategory_ajax","newloading") '/>
                  
			  </div>  </div>  
</div>
			  
			  <?php $i++;  } ?>
              </div>
            </form>
          </div>
		  </div>
    
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
      </div>
      <div class="clearfix"></div>
     <!-- <div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div>-->
      <div class="clear"></div>
   
<div class="clear"></div>