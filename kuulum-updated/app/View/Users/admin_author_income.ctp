<script type='text/javascript'>
	$(document).ready(function() {
		gran_total = $('#t_grant').val();
		comm_total = $('#t_commission').val();
		reve_total = $('#t_income').val();
		
		$('#G_total_id').html('Gross Revenue : '+gran_total);
		$('#C_total_id').html('Commission: '+comm_total);
		$('#R_total_id').html('Net Revenue: '+reve_total);
		//gran_total='0';
		
		$('#loading_overlay').on('click',function()
		{
			$(this).hide();
			$('.date-range').hide();
		});
		
		
	});
	
	$(document).on('click','.common_setting a',function(){
	
			Type = $('#filter_option').val();
			$m_id= $('#m_id').val();
		
			
			DateFrom = $('.df').attr('id');
			DateTo = $('.dt').attr('id');
			url = $(this).attr('href');
			
			$.ajax({
				url:url,
				data: {'Type':Type,'DateFrom':DateFrom,'DateTo':DateTo,'m_id':$m_id},
				type:'post',
				success:function(html)
				{ 
					$('.loadPaginationContent').html(html)
				}
			})
			return false;
		});
		
	
	
		$(document).ready(function() {	
			$('#search1').change(function(){ 
				var $value = $(this).attr('value');
				$m_id= $('#m_id').val();
		
				if($value=="range")
				{ 
					$("#c_language").val('');	
					$(".selected").css({'color':'#888'});
					$('.date-range').show();
					$('#loading_overlay').show();
					$("#datepicker").datepicker({	
						dateFormat : 'yy-mm-dd',
						changeMonth : true,
						changeYear : true,
						yearRange : '2013:<?php echo date('Y');?>',
						inline: true,
						maxDate: new Date(),
						
						onSelect: function(dateSelected){
									
							$(this).parent('.date_selected').attr('id',dateSelected);
							$("#datepicker1").parent('.date_selected').attr('id','');
							$("#datepicker1").val('');
							$( "#datepicker1" ).datepicker( "destroy");
					
							$("#datepicker1").datepicker({	
								dateFormat : 'yy-mm-dd',
								changeMonth : true,
								changeYear : true,		
								yearRange : '1930:2030',
								inline: true,
								minDate: new Date(dateSelected),
								maxDate: new Date(),
								onSelect: function(dateSelected1){
									$("#datepicker1").parent('.date_selected').attr('id',dateSelected1);
									
									DateFrom = $('.df').attr('id');
									
									DateTo = $('.dt').attr('id');
									
									$('#filter_option').val($value);
									Type = $('#filter_option').val();
				
									$('.date-range').hide();
									req = $.ajax({
										url:location.href,
										data: {'Type':Type,'DateFrom':DateFrom,'DateTo':DateTo,'m_id':$m_id},
										type:'post',
										success: function(resp){
												
											$(".loadPaginationContent").html(resp);	
											
										}
										
									});
								}
							});
						}
					});
				}else{ 
					$('#filter_option').val($value);
					Type = $('#filter_option').val();
					$('.date-range').hide();
					$('#loading_overlay').hide();
					$(".adminTutorSearchWait").show();
					req = $.ajax({
						url:location.href,
						data: {'Type':Type,'m_id':$m_id},
						type:'post',
						success: function(resp){
							$(".adminTutorSearchWait").hide();		
							$(".loadPaginationContent").html(resp);	
						}
					});
				}
				gran_total = $('#t_grant').val();
				
				comm_total = $('#t_commission').val();
				reve_total = $('#t_income').val();
				$('#G_total_id').html('Gross Revenue : '+gran_total);
				$('#C_total_id').html('Commission: '+comm_total);
				$('#R_total_id').html('Net Revenue: '+reve_total);
				
				
				/*$(this).parent().css('display','none');
				$(this).closest('div.selectBox').attr('value',$(this).attr('value'));
				$(this).parent().siblings('span.selected').html($(this).html());*/
			});
					
	});
	
	
</script>

<!--<div id="sub-nav">
	<div class="page-title">
		<h1>Authors Income</h1>
	</div>
</div>

<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				<h2>Author Income List</h2>      -->  
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Income for <?php echo $member_name["Member"]["given_name"]; ?><br><small>Income details for the <?php echo  $member_name["Member"]["given_name"]; ?>.</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>
<div id="content" class="content">

	<div class="block">

		<div class="">
<div class="inner-page-title">         
                 <?php /*?> <a href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" id="hideTutorSearch" style="margin-top:-10px; margin-right:10px;">Click Here to Hide/Show Search option</a><?php */?>

				<span></span>
			</div>
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
      <!--      <div class="search">
                	    <?php echo $this->Form->create('Filter',array('id'=>'trainer_filter')); ?>
                        <div class="search_contents">
							<?php echo $this->Form->input('filtervalue',array('type'=>'hidden','value'=>'','div'=>false,'label'=>false,'id'=>'filter_option'));
							?>

                            <label class="desc" ></label>
                            <?php echo $this->Form->input('type',array('type'=>'select','id'=>'search1','div'=>false, 'style'=>'width:200px; padding:4px; border:1px solid #abadb3;' ,'label'=>false,'class'=>'field text full required','options'=>array(''=>'Filter By','all'=>'Show All','week'=>'This Week','month'=>'This Month','quarter'=>'This Quarter','year'=>'This Year','range'=>'Date Range'))); ?>
							
                        </div>
                <div class="date-range" style="display:none;">
					<div class="desc-upper">
						<label>From:</label>
						<span class="desc date_selected df" id="">
							<?php echo $this->Form->input('from_date',array('type'=>'text','id'=>'datepicker','div'=>false, 'style="width:145px; height:25px; padding:0px 0px 0px 3px; border:1px solid #abadb3;','label'=>false)); ?>
                        </span>
					</div>
					<div class="desc-upper ">
						<label>To:</label>
						<span class="desc date_selected dt" id="">
                         	<?php echo $this->Form->input('to_date',array('type'=>'text','id'=>'datepicker1','div'=>false,'style'=>'width:145px; height:25px; padding:0px 0px 0px 3px; border:1px solid #abadb3;','label'=>false)); ?>
                        </span>
					</div>
				</div>
				 
               
						<div class="total">
						<h2 id="G_total_id">Gross Revenue:</h2>
						<h2 id="C_total_id">Commission:</h2>     						
						<h2 id="R_total_id">Net Revenue:</h2>     						
						</div>
						
                <div class="adminTutorSearchWait">
					<?php echo $this->Html->image('front/wait.gif',array('height'=>'32px'));?>
                </div>  
					<input type="hidden" value="<?php echo $id ?>" name="data[Member][id]" id="m_id"/>				
                       <?php echo $this->Form->end(); ?>                
            </div> -->
			<div class="content-box content-box-header" id='check'>
            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/author_income_list');?>
                </div>   
                <?php /*?><ul class="sidebar-position">
                    <li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
                    <li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
                </ul><?php */?>
				
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div>	-->		
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>

<style>
.showHideCourseTier
{
	display: none;
}
.hideStartEndDate
{
	display: none;
}
.total{float:right;font-size:8px;padding:8px 16px;}
.date-range{float:left; margin-top:5px;}
.desc-upper, .desc-upper span{float:left;}
.desc-upper label{margin:7px 5px 0 0; float:left;}
.desc-upper span input{float: left; width: 100px; margin-right:10px;}
</style>