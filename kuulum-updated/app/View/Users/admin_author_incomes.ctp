<script type='text/javascript'>
	$(document).ready(function() {
		gran_total = $('#t_grant').val();
		comm_total = $('#t_commission').val();
		reve_total = $('#t_income').val();
		
		$('#G_total_id').html('Gross Revenue : '+gran_total);
		$('#C_total_id').html('Commission: '+comm_total);
		$('#R_total_id').html('Net Revenue: '+reve_total);
		//gran_total='0';
		
		$('#loading_overlay').on('click',function()
		{
			$(this).hide();
			$('.date-range').hide();
		});
		
		
	});
	
	$(document).on('click','.common_setting a',function(){
	
			Type = $('#filter_option').val();
			
		
			
			DateFrom = $('.df').attr('id');
			DateTo = $('.dt').attr('id');
			url = $(this).attr('href');
			
			$.ajax({
				url:url,
				data: {'Type':Type,'DateFrom':DateFrom,'DateTo':DateTo},
				type:'post',
				success:function(html)
				{ 
					$('.loadPaginationContent').html(html)
				}
			})
			return false;
		});
		
	
	
		$(document).ready(function() {	
		$("#search1").selectbox({
                onChange: function ($value, inst) {
                if($value=="range")
				{ 
                                        $("#c_language").val('');	
					$(".selected").css({'color':'#888'});
					$('.date-range').show();
					$('#loading_overlay').show();
                                        var DateFrom="";
                                        var DateTo=""
                                        
						$("#datepicker").datepicker({
                                                    format: 'yy-mm-dd',
                                                    autoclose: true
                                                    })
                                                .on('changeDate', function(e) {
                                                    DateFrom = $("#datepicker").datepicker('getFormattedDate');
                                                    $("#datepicker1").datepicker('setStartDate', DateFrom);
                                                    $("#datepicker1").datepicker('clearDates');
                                                    $(this).datepicker('hide');
                                                    
                                                    // `e` here contains the extra attributes
                                                })
                                                .on('show', function(e) {
                                                  $("#datepicker1").datepicker('hide');
                                                });
                                                
                                                $("#datepicker1").datepicker({
                                                    format: 'yy-mm-dd',
                                                    autoclose: true
                                                    })
                                                .on('changeDate', function(e) {
                                                    DateTo =$("#datepicker1").datepicker('getFormattedDate') ;
                                                    $(this).datepicker('hide');
                                                    
                                                    
                                                    
                                                    var Type = $value
                                                         if(DateFrom && DateTo){ req = $.ajax({
										url:location.href,
										data: {'Type':Type,'DateFrom':DateFrom,'DateTo':DateTo},
										type:'post',
										success: function(resp){
												
											$(".loadPaginationContent").html(resp);	
											$('.dataTable').each(function() {
                                                                                            dt = $(this).dataTable();
                                                                                            dt.fnDraw();
                                                                                        })
										}
										
									});
                                                          }            
                                                                        
                                                })
                                                .on('show', function(e) {
                                                  $("#datepicker").datepicker('hide');
                                                });
                                                
				}else{ 
					$('#filter_option').val($value);
					Type = $('#filter_option').val();
					$('.date-range').hide();
					$('#loading_overlay').hide();
					$(".adminTutorSearchWait").show();
					req = $.ajax({
						url:location.href,
						data: {'Type':Type},
						type:'post',
						success: function(resp){
							$(".adminTutorSearchWait").hide();
                                                        $(".loadPaginationContent").html(resp);	

                                                        $('.dataTable').each(function() {
                                                            dt = $(this).dataTable();
                                                            dt.fnDraw();
                                                        })
                                                }
					});
				}
				gran_total = $('#t_grant').val();
				comm_total = $('#t_commission').val();
				reve_total = $('#t_income').val();
				$('#G_total_id').html('Gross Revenue : '+gran_total);
				$('#C_total_id').html('Commission: '+comm_total);
				$('#R_total_id').html('Net Revenue: '+reve_total);
			

         
         
            
                }});
					
	});
	
	
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Income all authors<br><small>Income details for all authors</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">
<div class="block">

    <div>
        <div id="page-content-wrapper" class="no-bg-image wrapper-full">        
            <div class="content-box content-box-header" id='check'>

<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>

<div>
    <div class="row" style="margin-bottom:5px !important;"></div>
<?php echo $this->Form->create('Filter',array('id'=>'trainer_filter',"class"=>"form-inline col-sm-12","style"=>"padding-left:10px")); ?>
                <div class="form-group">
                  <div class="search_contents form-group">
                    <div class="btn-group">
                    <button type="button" class="btn btn-default">Filters</button>
                    </div>
                    <?php echo $this->Form->input('filtervalue',array('type'=>'hidden','value'=>'','div'=>false,'label'=>false,'id'=>'filter_option'));?>
                    <?php echo $this->Form->input('type',array('type'=>'select','tabindex'=>'1','id'=>'search1','div'=>false,'label'=>false,'class'=>'field text full required','options'=>array('all'=>'Show all','week'=>'This week','month'=>'This month','quarter'=>'This quarter','year'=>'This year','range'=>'Date range'))); ?>

                    </div>  
                </div>               
                <div class="form-group">
                        <div class="date-range" style="display:none;">
                            <div class="form-group">
                                <?php echo $this->Form->input('',array('type'=>'text','readonly'=>'readonly','id'=>'datepicker','div'=>false,'class'=>"  datepicker form-control field text full required",'label'=>false)); ?>
                            </div>
                            <div class="form-group">
                                        <?php echo $this->Form->input('',array('type'=>'text','readonly'=>'readonly','id'=>'datepicker1','div'=>false,'class'=>"datepicker   form-control field text full required",'label'=>false)); ?>
                            </div>   
                        </div>
                </div>
	    <?php echo $this->Form->end(); ?>                
</div>           


            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/author_income_lists');?>
                </div> 
              </div>  
        </div>
    </div>
</div>
</div>
<div class="clear"></div>

<style>
.showHideCourseTier
{
	display: none;
}
.hideStartEndDate
{
	display: none;
}
.total{float:right;font-size:8px;padding:8px 16px;}
.date-range{float:left; margin-top:5px;}
.desc-upper, .desc-upper span{float:left;}
.desc-upper label{margin:7px 5px 0 0; float:left;}
.desc-upper span input{float: left; width: 100px; margin-right:10px;}
</style>




