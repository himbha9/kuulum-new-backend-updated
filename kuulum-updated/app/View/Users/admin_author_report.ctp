<?php
	header("Content-type:application/vnd.ms-excel");
	header("Content-disposition:attachment; filename=Authors-Report.xls");

	echo "\xEF\xBB\xBF";
?>
<style type="text/css">
	.tableTd {
	   	border-width: 0.5pt; 
		border: solid;
		height: 30px;
		background-color: #E2F1FF; 
	}
	.tableTdContent{
		border-width: 0.5pt; 
		border: solid;
		height: 30px;
	}
	#titles{
		font-weight: bolder;
	}
   
</style>

<?php 
	echo '<table>
	<tr>
		<td colspan="4"><b>Authors Report.</b></td>
	</tr>
	<tr>
		<td><b>Date:</b></td>
		<td>';
	echo date("F j, Y");
	echo'</td>
	</tr>
	
	<tr>
		<td></td>
	</tr>
			<td class="tableTd">S.No.</td>
			<td class="tableTd">E-mail</td>
			<td class="tableTd">Given Name</td>
			<td class="tableTd">Family Name</td>
			<td class="tableTd">Other Name</td>
			<td class="tableTd">Final Name</td>
			<td class="tableTd">Interest</td>
			<td class="tableTd">Status</td>
        </tr>';
	$j = 1;
	if(!empty($info)){
		foreach($info as $info)
		{
			
			switch ($info['Member']['status'])
			{
				case 0:
					$status='In-Active';
					break;
				case 1:
					$status='Active';
					break;
				
			}
			$i=1;
			foreach ($info['MemberKeyword'] as $intrests)
			{
			$intrest.=$i.")".$intrests['keyword']." ";
			$i++;
			}
					echo 	'<tr>
								<td class="tableTdContent">'.$j.'</td>
								<td class="tableTdContent">'.$info['Member']['email'].'</td>
								<td class="tableTdContent" align="center">'.$info['Member']['given_name'].'</td>
								<td class="tableTdContent">'.$info['Member']['family_name'].'</td>
								<td class="tableTdContent">'.$info['Member']['other_name'].'</td>
								<td class="tableTdContent">'.$info['Member']['final_name'].'</td>
								<td class="tableTdContent">'.$intrest.'</td>
								<td class="tableTdContent" align="right">'.$status.'</td>
							</tr>';
					$j++;		
				}
				echo '</table>';
			}
			else
			{
				echo '<tr> <td class="tableTdContent" colspan="6" >No Records.</td></tr>';
			}
?>
