<script type="text/javascript">
$(document).ready(function(){
	/*$( ".datepicker" ).datepicker({			
				dateFormat:'dd-mm-yy',
				changeMonth: true,
				changeYear: true,		
				yearRange: '1930:2030',
				inline: true
		});
	*/                           
                                                
	$('#hideTutorSearch').on('click',function(){		
			$(".search").slideToggle(1000);
		//$('#hideTutorSearch').attr('id','showTutorSearch');
		});	
	
	$('#member_email').on('change',function(){
	
			var value=$('#member_email').val();
			if(value=='email'){
				$('.hideStartEndDate').hide();
				$('.showHideCoursetText').show();			
			}
			else if(value=='date_requested'){
				$('.showHideCoursetText').hide();
				$('.hideStartEndDate').show();			
			}
			
		});	
});
</script>

<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Author Requests <br><small>Approve and reject a member's request to become an author</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">

    
	<div class="block">

		<div class="">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				
                <!--  <a href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" id="hideTutorSearch" style="margin-top:-10px; margin-right:10px;">Click Here to Hide/Show Search option</a> -->
				<span></span>
			</div>
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
 <div class="search">
     <div class="row">
        <div class="row" style="margin-top: 10px !important;"></div>	
        <div>
            

             <?php echo $this->Form->create('search',array('id'=>'searchauthorreq',"class"=>"form-inline col-sm-12")); ?>
                <div class="form-group">
                                    <div class="btn-group">
                                                <button type="button" class="btn btn-default">Actions</button>
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                      
                                                        <li>
                                                            <a href="" tabindex="-1" id="approve_req">Approve</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" tabindex="-1" id="reject_req">Reject</a>
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                            </div> 
                </div>
                <div class="form-group">
                        <div class="btn-group">
                        <button type="button" class="btn btn-default">Filters</button>
                        <div class="btn-group">
                        <?php echo $this->Form->input('type',array('type'=>'select','id'=>'member_email','div'=>false, 'style'=>'width:148px; padding:4px; border:1px solid #abadb3;' ,'label'=>false,'class'=>'field text full required','options'=>array('name'=>"By name",'email'=>'By email'))); //,'date_requested'=>'Date Requested' ?>
                        </div>
                        </div>
                </div>
            <div class="form-group">
                <?php echo $this->Form->input('text',array('type'=>'text','id'=>'email_search','div'=>false, 'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input')); ?> 
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit('Search',array('div'=>false,'label'=>false,'id'=>'searchauthReq','class'=>'required btn btn-sm btn-primary ')); ?>    
            </div>        
            <?php echo $this->Form->end(); ?> 
         
        </div>
                 
</div>
     
                                   
                </div>
			<div class="content-box content-box-header" id='check'>
            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/author_request_list');?>
                </div>   
                <?php /*?><ul class="sidebar-position">
                    <li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
                    <li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
                </ul><?php */?>
				
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div>		-->	
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
</div>
</div>
<style>

.showHideCourseTier
{
	display: none;
}
.hideStartEndDate
{
	display: none;
}
</style>

<script>
    $(document).ready(function(){
  	$("#member_email").selectbox();
   });
</script>        