<style>
	form li span
	{
		width:100%;
	}
	tr.mceLast
	{
		display:none;
	}
</style>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Change Password <br><small>Change your administrator password.</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				<!--<h2>Change Password</h2> -->
				<span></span>
			</div>
			
			 <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>

		
			<!--<div class="content-box content-box-header" style="border:none;">

				<div class="column-content-box">

					<div class="ui-state-default ui-corner-top ui-box-header">

						<span class="ui-icon float-left ui-icon-notice"></span>

						Change Password

					</div>
					
					<div id="tabs"> -->
                     <?php echo $this->Form->create('ChangePass',array("class"=>"js-validation-bootstrap form-horizontal",'id'=>'pass','url'=>array('controller'=>'users','action'=>'change_password'))); ?>
						
							<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$username['Admin']['id'])); ?> 	
								
									<div class="form-group">
                                            <label for="val-username" class="col-md-2 control-label"><span class="">*  </span>Old Password:</label>
                                            <div class="col-md-7">
											
                                            	<?php echo $this->Form->input('old_pass',array("placeholder"=>"Enter your current password...","class"=>"form-control text full field required",'type'=>'password','div'=>false,'label'=>false)); ?> 
                                                
                                             <p class="tutor-add-Error" id="err_old_pass"><?php if(isset($error['old_pass'][0])) echo $error['old_pass'][0]; ?>  </p>   
							</div>
					</div>  
										<div class="form-group">
                                            <label for="val-username" class="col-md-2 control-label"><span class="">*  </span>New Password:</label>
                                            <div class="col-md-7">
											
                                             <?php echo $this->Form->input('new_pass',array("placeholder"=>"Enter your new password...","class"=>"form-control",'type'=>'password','div'=>false,'label'=>false)); ?>    
                                             <p class="tutor-add-Error" id="err_new_pass"><?php if(isset($error['new_pass'][0])) echo $error['new_pass'][0]; ?>  </p>   
						</div>
                                            </div>
										<div class="form-group">
                                            <label for="val-username" class="col-md-2 control-label"><span class="">*  </span>Re-enter new password:</label>
                                            <div class="col-md-7">
											
                                            	<?php echo $this->Form->input('con_pass',array("placeholder"=>"Re-enter your new password...","class"=>"form-control",'type'=>'password','div'=>false,'label'=>false,)); ?> 
                                                
                                             <p class="tutor-add-Error" id="err_con_pass"><?php if(isset($error['con_pass'][0])) echo $error['con_pass'][0]; ?>  </p>   
                                                </div>
                                        </div>
                                      <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
  <input type="submit" class="btn btn-sm btn-primary" value="Submit" onclick='return ajax_form("pass","Users/validate_change_password_ajax","newloading") '/>
                                               
                                            </div>
                                        </div>
                                            
                                          <!--  <div class="newloading">
                                                <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px'));?>
                                            </div>  -->
                                           
										
								
								
				     <?php echo $this->Form->end(); ?>
	       	 	
					
					<?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
					
			
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
	<div class="clear"></div>
