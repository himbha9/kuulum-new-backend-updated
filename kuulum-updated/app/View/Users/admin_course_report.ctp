<?php
	header("Content-type:application/vnd.ms-excel");
	header("Content-disposition:attachment; filename=Course-Report.xls");
	echo "\xEF\xBB\xBF";
?>
<style type="text/css">
	.tableTd {
	   	border-width: 0.5pt; 
		border: solid;
		height: 30px;
		background-color: #E2F1FF; 
	}
	.tableTdContent{
		border-width: 0.5pt; 
		border: solid;
		height: 30px;
	}
	#titles{
		font-weight: bolder;
	}
   
</style>

<?php 
	echo '<table>
	<tr>
		<td colspan="4"><b>Course Report.</b></td>
	</tr>
	<tr>
		<td><b>Date:</b></td>
		<td>';
	echo date("F j, Y");
	echo'</td>
	</tr>
	
	<tr>
		<td></td>
	</tr>
			<td class="tableTd">S.No.</td>
			<td class="tableTd">Title</td>
			<td class="tableTd">Description</td>
			<td class="tableTd">Primary language</td>
			<td class="tableTd">Date Added</td>
        </tr>';
	$j = 1;
	if(!empty($info)){
		foreach($info as $info)
		{
			
			switch ($info['Course']['status'])
			{
				case 0:
					$status='In-Active';
					break;
				case 1:
					$status='Active';
					break;
				
			}
					echo 	'<tr>
								<td class="tableTdContent">'.$j.'</td>
								<td class="tableTdContent">'.$info['Course']['title'].'</td>
								<td class="tableTdContent" align="center">'.$info['Course']['description'].'</td>
								<td class="tableTdContent">'.$info['Language']['language'].'</td>
								<td class="tableTdContent">'. date('d M Y, h:i e',$info['Course']['date_added']).'</td>
								
							</tr>';
					$j++;		
				}
				echo '</table>';
			}
			else
			{
				echo '<tr> <td class="tableTdContent" colspan="6" >No Records.</td></tr>';
			}
?>
