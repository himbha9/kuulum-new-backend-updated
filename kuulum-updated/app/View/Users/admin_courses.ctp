<script type="text/javascript">
//$(window).resize(function(){
//alert($(window).height());
//});

$(document).ready(function(){
        $('.showHideCoursetText2').hide();
	$('.hideStatusType').hide();
	$('.showHideAuthor').hide(); 	
	$('.hideStartEndDate').hide();
	
	$('#hideTutorSearch').on('click',function(){		
		$(".search").slideToggle(1000);
		//$('#hideTutorSearch').attr('id','showTutorSearch');
	});
        
	$(".datepicker").datepicker({
                                                    format: 'yyyy-mm-dd',
                                                    autoclose: true
                                                    })
                                                .on('changeDate', function(e) {
                                                    $(this).datepicker('hide');
                                                });
                                                
	
	$("#Student_title").selectbox({
                onChange: function (value, inst) {
		
		if(value=='title'){
			$('.showHideCoursetText2').hide();
			$('.showHideCourseTier').show();
			$('.hideStatusType').hide();
			$('.showHideAuthor').hide();			
		}
		else if(value=='primarylanguage'){
			$('.showHideCourseTier').hide();
			$('.showHideCoursetText2').show();
			$('.hideStatusType').hide();
			$('.showHideAuthor').hide();			
		}
                else if(value=='date_added'){
			$('.showHideCourseTier').hide();
			$('.hideStartEndDate').show();
			$('.hideStatusType').hide();
			$('.showHideAuthor').hide();			
		}
                else if(value=='status'){
				$('.showHideCourseTier').hide();
				$('.showHideCoursetText2').hide();
				$('.hideStatusType').show();
				$('.showHideAuthor').hide();			
			}
		else if(value=='author'){
				$('.showHideCourseTier').hide();
				$('.showHideCoursetText2').hide();
				$('.hideStatusType').hide();
				$('.showHideAuthor').show();			
			}

	}});
});
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Course list <br><small>List of courses available on the portal</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">

    
	<div class="block">

		<div>

		<!--<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<!--<div class="inner-page-title">
		
				<?php /* <a href="<?php echo HTTP_ROOT.'admin/users/add_courses'; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add Courses</a> */?>
                  <a href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" id="hideTutorSearch" style="margin-top:-10px; margin-right:10px;">Click Here to Hide/Show Search option</a>
				<span></span>
			</div> -->
            <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>

            
                
                <div class="row">
        <div class="row" style="margin-top: 5px !important;"></div>	
        <div>
                	
    <?php echo $this->Form->create('search',array('id'=>'searchCourse',"class"=>"form-inline col-sm-12")); ?>
               
               <div class="form-group">
                <div class="btn-group">
                <button type="button" class="btn btn-default">Actions</button>
                <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                <li><a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a></li>
                <li><a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a></li>
                </ul>
                </div>
                </div>
                   
               </div>    
                <div class="form-group">
                    <div class="search_contents form-group">
                    <div class="btn-group">
                    <button type="button" class="btn btn-default">Filters</button>
                    </div>   
                 <?php echo $this->Form->input('type',array('type'=>'select','id'=>'Student_title','div'=>false, 'style'=>'width:148px; padding:4px; border:1px solid #abadb3;' ,'label'=>false,'class'=>'field text full required','options'=>array('author'=>'By author','date_added'=>'By date','status'=>'By status'))); ?>   
                    </div>    
                 </div> 
                
                 <div class="form-group showHideCourseTier">
                    <?php echo $this->Form->input('title',array('type'=>'text','id'=>'student_courses','div'=>false,'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input')); ?>
                 </div>
             
            <div class="form-group hideStartEndDate">
             <?php echo $this->Form->input('date_added',array('type'=>'text','readonly'=>'readonly', 'id'=>'date_added','div'=>false, 'label'=>false,'class'=>'datepicker form-control field text full required col-sm-3 ')); ?>
             </div>
            
                <div class="form-group showHideAuthor">
                 <?php echo $this->Form->input('author',array('type'=>'text','id'=>'author_name','div'=>false,'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input')); ?>
                </div>
                <div class="form-group showHideCoursetText2">
                    <?php echo $this->Form->input('language',array('type'=>'select','id'=>'Tutor_search','div'=>false,'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input','options'=>$lang)); ?> 
                </div>
                <div class="form-group hideStatusType">
                            <?php echo $this->Form->input('status',array('type'=>'select','id'=>'mem_status','div'=>false ,'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input','options'=>array('1'=>'Active','0'=>'In-active'))); ?> 
                
                </div>   
                <div class="form-group">
                  	<?php echo $this->Form->submit('Search',array('div'=>false,'label'=>false,'id'=>'searchauthorCourse','class'=>'required btn btn-sm btn-primary  txtts_inproperty')); ?>                    
                </div>  
                <?php echo $this->Form->end(); ?>                
                
             
        </div>
        	
    </div>        
       <div class="row" style="margin-top: 5px !important;"></div>                 
            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/cours_list');?>
                </div> 
          <!--      <?php /*?><ul class="sidebar-position">
                    <li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
                    <li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
                </ul><?php */?>
				
			</div> -->
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div>	-->		
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<style>
.showHideCourseTier
{
	display: block;
}
.showHideCoursetText2
{
	display: none;
}
</style>