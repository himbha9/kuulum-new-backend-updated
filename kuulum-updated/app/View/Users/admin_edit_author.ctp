<script type="text/javascript">
		
		$(function(){
			
			$('#image').bind('change',function(){
		
				var imagePath = $('#image').val();//document.FormTwo.picFile.value;
				var pathLength = imagePath.length;
				var lastDot = imagePath.lastIndexOf(".");
				var fileType = imagePath.substring(lastDot,pathLength);	
				var fileType = fileType.toLowerCase();
				
				$('#file_hidden').val(imagePath); 

				
			});
			
			$('#mem_type').change(function(){
					
				if($.trim($(this).val())=='3') // if author
				{
					$('.author_li').slideDown();
				}else{
					$('.author_li').slideUp();
				}	
					
			});
			
		});
		
</script>

<div id="sub-nav">
	<div class="page-title">
		<h1> Edit Author</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2>Author Detail</h2>
				<a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
				<span></span>
			</div>
			<?php if($this->Session->check('success')){ ?>
				<div class="success ui-corner-all successdeveloperClass" id="success" style="height:27px;"> <span > <?php echo $this->Session->read('success');?> </span> </div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			<div class="content-box content-box-header" style="border:none;">
				<div class="column-content-box">
					<?php echo $this->Form->create('Member',array('url'=>array('controller'=>'users','action'=>'edit_author'),'id'=>'edit_member','enctype'=>'multipart/form-data')); ?>
						<?php echo $this->Form->input('id',array('type'=>'hidden','div'=>false,'label'=>false,'class'=>'field text full','value'=>$info['Member']['id'])); ?>
						<div class="content-box-wrapper">
							<fieldset>
								<ul>
									<li>
										<span class="mandatory">*</span><label class="desc">Given Name</label>
										<div>
											<?php echo $this->Form->input('given_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required','value'=>$info['Member']['given_name'])); ?>
										</div>
									</li>									
									<li>
										<label class="desc" >Family Name</label>
										<div>
											<?php echo $this->Form->input('family_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required','value'=>$info['Member']['family_name'])); ?>
										</div>
									</li>									
									<li>
										<label class="desc" >Other Name</label>
										<div>
											<?php echo $this->Form->input('other_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required','value'=>$info['Member']['family_name'])); ?>
										</div>
									</li>
									<li>
										<span class="mandatory">*</span><label class="desc">What should we call you?</label>
										<div> <?php echo $this->Form->input('final_name',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required','value'=>$info['Member']['final_name'])); ?>
											<p class="university-add-Error" id="err_final_name">
												<?php if(isset($error['final_name'][0])) echo $error['final_name'][0]; ?>
											</p>
										</div>
									</li>
									<li>
										<label class="desc" >Keep me informed:</label>
										<div> <?php echo $this->Form->input('notification',array('type'=>'select','id'=>'student_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'No','1'=>'Yes'),'selected'=>$info['Member']['notification'])); ?>
											<p class="university-add-Error" id="err_lat">
											</p>
										</div>
									</li>
									<li>
										<label class="desc" >Current Status</label>
										<div> <?php echo $this->Form->input('status',array('type'=>'select','id'=>'student_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>$info['Member']['status'])); ?>
											<p class="university-add-Error" id="err_lat">
											</p>
										</div>
									</li>
									<li>
										<label class="desc" >Profile Type</label>
										<input name="data[Member][old_type]" type="hidden" value="<?php echo $info['Member']['role_id']?>"  />
										<div> <?php echo $this->Form->input('role_id',array('type'=>'select','id'=>'mem_type','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('2'=>'Member','3'=>'Author'),'selected'=>$info['Member']['role_id'])); ?>
											<p class="university-add-Error" id="err_lat">
											</p>
										</div>
									</li>																											
									<li class="author_li">
										<div style="float:left; width:380px;"> 
											<label class="desc" >Picture</label>				   
											<div> 
												<?php echo $this->Form->input('image',array('name'=>'image','type'=>'file','id'=>'image','label'=>false,'div'=>false)); ?>
												<?php echo $this->Form->input('picture',array('type'=>'hidden','id'=>'file_hidden','label'=>false,'div'=>false)); ?>
												<?php echo $this->Form->input('old_image',array('type'=>'hidden','id'=>'old_image','label'=>false,'div'=>false,'value'=>$info['Member']['image'])); ?>
												<label>Allowable picture file formats are (*.jpg), (*.jpeg), (*.png) and (*.gif)</label>
												<p class="university-add-Error" id="err_picture"></p>
											</div>
										</div>
										<div style="float:left;">
											<?php
												if($info['Member']['image'])
												{
													$image = $info['Member']['image'];
													$path = 'files/members/profile/'.$image;
													if(file_exists($path))
													{
														echo $this->Html->image(HTTP_ROOT.'files/members/profile/thumb/'.$image,array('width'=>'150'));
													}
												}
											?>
										</div>
									</li>
									<li class="author_li">
										<span class="mandatory">*</span><label class="desc" >About the author</label>				   
										<div> 
											<?php echo $this->Form->input('about_me',array('class'=>'text full','type'=>'textarea','label'=>false,'div'=>false,'value'=>$info['Member']['about_me'])); ?>											
											<p class="university-add-Error" id="err_about_me">
											</p>
										</div>
									</li>
									<li>
										<input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("edit_member","Users/validate_edit_member_ajax","newloading") '/>
									</li>
								</ul>
							</fieldset>
						</div>
					<?php echo $this->Form->end(); ?>	
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
