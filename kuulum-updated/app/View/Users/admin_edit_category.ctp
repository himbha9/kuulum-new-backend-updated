<script type="text/javascript">
	
	$(document).ready(function() {
	
		//$('#tabs, #tabs2, #tabs5').tabs();

		$('#edit_category').validate();
		
			
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('rel');
			//alert($lang);
			$('#lang_locale').val($lang);
		});
		
	});
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Edit course category <br><small>Edit a course category</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>" rel="<?php echo $lang['Language']['locale'] ?>"   href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
				<?php $i++;
                                }
                                ?>	
 <li class="pull-right">     <?php $id= base64_encode(convert_uuencode($info['CourseCategory']['id']));?>
				<button class="btn  btn-primary" type="button" onclick="document.location.href='<?php echo HTTP_ROOT."admin/users/course_subcategory/".$id; ?>'">View Subcategories</button>  
</li>
			</ul>
<!--<div id="sub-nav">
  <div class="page-title">
    <h1>Edit Category</h1>
  </div>
</div>
<div id="page-layout">
  <div id="page-content">
    <div id="page-content-wrapper" class="no-bg-image wrapper-full">
      <div class="inner-page-title">
        <h2>Edit Category</h2>
        <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> </div> -->
      <?php if($this->Session->check('success')){ ?>
      <div class="success ui-corner-all successdeveloperClass" id="success"> <span class='successMessageText'> <?php echo $this->Session->read('success');?> </span> </div>
      <?php $this->Session->delete('success'); ?>
      <?php } ?>
      <!--<div class="content-box content-box-header" style="border:none;">
		
		<div id="tabs"> 
			<ul>
				<?php $i=1; foreach($language as $lang){ ?>
					<li><a title="<?php echo $lang['Language']['locale'] ?>" class="lang_tab" href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>
			</ul>
        <div class="column-content-box">
          
          <div class="content-box-wrapper"> -->

 <?php echo $this->Form->create('CourseCategory',array('url'=>array('controller'=>'users','action'=>'edit_category'),'id'=>'edit_category',"class"=>"js-validation-bootstrap form-horizontal editTemplateForm")); ?>
		  
		  <?php echo $this->Form->input('id',array('type'=>'hidden', 'value'=>$info['CourseCategory']['id'],'div'=>false,'label'=>false,'class'=>'form-control field text full')); ?>
		  <input type="hidden" id="lang_locale" value="en" name="data[CourseCategory][locale]"/>	
						
			  <div class="block-content tab-content">  
			<?php 
				$i=1; 
				foreach($language as $lang)
				{
					$locale = $lang['Language']['locale'];								
			?>
			 <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
                <div class="form-group">
                <label for="val-username" class="col-md-1 text-right"> Category:</label>
                <div class="col-md-6">
					 <?php if(!empty($info)){
						 $id=base64_encode(convert_uuencode($info['CourseCategory']['id']));
						
						 $status=$info['CourseCategory']['status'];
					 }
					 
					 echo $this->Form->input('id',array('type'=>'hidden','div'=>false,'label'=>false,'value'=>$id));
					 ?>
					 

 <?php echo $this->Form->input($locale,array('type'=>'text','id'=>$locale,'div'=>false,'label'=>false,'value'=>$info['CourseCategory'][$locale],'class'=>'form-control field text full required')); ?>

						<span class="university-add-Error" id="err_<?php echo $locale; ?>">
						  <?php if(isset($error['title'][0])) echo $error['title'][0]; ?>
						</span>
					 </div></div>
					<?php /* if($i==1) { ?>
					<li>
					  <label class="desc" >Current Status</label>
					  <div> <?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>$status)); ?>
						<p class="university-add-Error" id="err_status">
						  <?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
						</p>
					  </div>
					</li>
					<?php  } */?>
					<div class="form-group">
                <div class="col-md-8 col-md-offset-1">
					   <input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("edit_category","Users/validate_edit_category_ajax","newloading") '/>
					
				
			</div>	</div></div>
			<?php $i++; } ?>
            <?php echo $this->Form->end(); ?>
          </div>
        </div>
		</div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
      </div>
      <div class="clearfix"></div>
      <!--<div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
