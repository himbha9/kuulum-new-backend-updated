<!--<?php echo $this->Html->script('ui/ui.tabs.js');?>-->

<script type="text/javascript">
	
	$(document).ready(function() {
		CKEDITOR.replaceAll('editor1');
               	CKEDITOR.config.filebrowserBrowseUrl = ajax_url+'js/admin/js/ckfinder/ckfinder.html';
		CKEDITOR.config.filebrowserImageBrowseUrl = ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Images';
		CKEDITOR.config.filebrowserFlashBrowseUrl = ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Flash';
		CKEDITOR.config.filebrowserUploadUrl = ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
		CKEDITOR.config.filebrowserImageUploadUrl = ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
		CKEDITOR.config.filebrowserFlashUploadUrl = ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
		
	});
	
</script>

<!--<script type="text/javascript">
	
	$(document).ready(function() {
		
		$('#tabs, #tabs2, #tabs5').tabs();
	});
	
</script>-->

<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            <?php echo $info[0]['CmsPage']['page_title']; ?>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div  class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php $i=1; foreach($language as $lang){  ?>
                                        <li <?php if($i==1)echo "class='active'";?>><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>" rel="<?php echo $lang['Language']['locale'] ?>" href="#tabs-<?php echo $i; ?>" ><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>
                                       <li class="pull-right">
<button class="btn btn-minw btn-primary" type="button" onclick="history.go(-1);">Back</button>
</li>
			</ul>

    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>

			
			<form class="js-validation-bootstrap form-horizontal editTemplateForm" method="post" action="<?php echo HTTP_ROOT;?>admin/users/edit_cms">
			 <div class="block-content tab-content">  
				<?php $i = 1; foreach($info as $info) { ?>
			
					<div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
						
						<!--<div class="content-box-wrapper"> -->
							<input type="hidden" name="data[<?php echo $i; ?>][CmsPage][id]" value="<?php echo $info['CmsPage']['id']; ?>" readonly="readonly" />
							
								  <div class="form-group">
                                  <label for="val-username" class="col-md-2  text-left"> Title:</label>                                            <div class="col-md-7">
                                      <input  class="form-control field text full required" name="data[<?php echo $i; ?>][CmsPage][page_title]" type="text" placeholder="Enter title." value="<?php echo $info['CmsPage']['page_title']?>" /></div>
                                                                  </div>
                                                         <div class="form-group">
                                  <label for="val-username" class="col-md-2  text-left"> Meta Title:</label>                                            <div class="col-md-7">
										<input  class="form-control field text full required" name="data[<?php echo $i; ?>][CmsPage][meta_title]" type="text" placeholder="Enter the meta title for the page." value="<?php echo $info['CmsPage']['meta_title']; ?>" /></div>
                                                                  </div>
                                                         <div class="form-group">
                                  <label for="val-username" class="col-md-2  text-left"> Meta Keywords:</label>                                            <div class="col-md-7">
										<input  class="form-control field text full required" name="data[<?php echo $i; ?>][CmsPage][meta_keywords]" placeholder="Enter meta keywords seperated by a comma." type="text" value="<?php echo $info['CmsPage']['meta_keywords']; ?>" /></div>
                                                                  </div>
                                                         <div class="form-group">
                                  <label for="val-username" class="col-md-2  text-left"> Meta Description:</label>                                            <div class="col-md-7">
										<input  class="form-control field text full required" name="data[<?php echo $i; ?>][CmsPage][meta_description]" placeholder="Enter meta description." type="text" value="<?php echo $info['CmsPage']['meta_description']; ?>" /></div>
                                                                  </div>

									 <div class="form-group">
                                                                        <div class="col-md-12">
									
										
											<textarea class=" form-control tinymce required editor1" style="width:100%;" name="data[<?php echo $i; ?>][CmsPage][description]" id=""><?php echo $info['CmsPage']['description']; ?></textarea>
										</div>
                                                                         </div>  
                                                        
									 <div class="form-group">
                                            <div class="col-md-8">
                   
                   
										<input class="btn  btn-primary" type="submit" value="Submit"/>
                                                                                 </div></div>
									
							
						<!--</div> -->
					</div>
				
				<?php $i++; } ?>
                         </div>
			</form>
		</div>
			
		</div>
			<div class="clearfix"></div>
		</div>
	
    <div class="clear"></div>
 
<div class="clear"></div>
            
