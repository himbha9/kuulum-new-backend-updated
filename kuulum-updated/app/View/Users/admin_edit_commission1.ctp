<?php //echo $this->Html->script('jquery.validate.js'); ?>
<div id="sub-nav">
  <div class="page-title">
    <h1><?php echo __('Edit Commission'); ?></h1>
  </div>
</div>
<div id="page-layout">
<div id="page-content">
  <div id="page-content-wrapper" class="no-bg-image wrapper-full">
    <div class="inner-page-title">
      <h2><?php echo __('Commission Detail');?></h2>
	  <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
      <span></span> </div>	  
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
    <div class="content-box content-box-header" style="border:none;">
	
      <div class="column-content-box">
        <div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span> Commission Information </div>
        <div id="tabs"> <?php echo $this->Form->create('Commission',array('class'=>'editTemplateForm', 'id'=>'editCommission','url'=>array('controller'=>'users','action'=>'edit_commission'),'onsubmit'=>'return checknumber()')); ?>
          <div id="tabs1">
            <div class="content-box-wrapper">
              <fieldset>
                <ul>                 
                  <li>
                   <label class="desc" ><?php echo __('Commission Value');?></label>
                  
						
						<?php echo $this->Form->input('Commission.commission',array('type'=>'text','id'=>'commission','value'=>$info['Commission']['commission'],'div'=>false,'label'=>false,'class'=>'field text full required',)); ?>
						
						<?php 


$memberselected=explode(",",$info['Commission']['author_name']);
foreach($memberselected as $member)
{
if(array_key_exists($member,$members))
{
$mem=$members[$member];
$members[$member]=array('name' => $mem, 'value' => $member,  'selected' => 'selected');
}
}
echo $this->Form->select(
    'Commission.author_name',
    $members,
    array('multiple' => true)
	  
);


$durations=array('Limited','Unlimited');
$duration=$info['Commission']['duration'];
if(array_key_exists($duration,$durations))
{
$dur=$durations[$duration];
$durations[$duration]=array('name' => $dur, 'value' => $duration,  'selected' => 'selected');
}

echo $this->Form->input('Commission.duration',array('options'=>$durations,'empty'=>'Select Duration','id'=>'duration','onchange'=>"dateduration();",'label'=>false));

?>
<script>
 $(function() {
    $( "#datepicker" ).datepicker({
      showOn: "button",
      buttonImage: "<?php echo $this->webroot; ?>/img/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
	    $( "#datepicker1" ).datepicker({
      showOn: "button",
      buttonImage: "<?php echo $this->webroot; ?>/img/calendar.gif", 
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  });
  </script>
<div id="durationdateselection" style="display:none;">
  <?php
$date_from=date('m/d/y', strtotime(str_replace('/', '-', $info['Commission']['date_from'])));
echo $this->Form->input('Commission.date_from',array('type'=>'text','id'=>'datepicker','value'=>$date_from));?>
<?php
$date_to=date('m/d/y', strtotime(str_replace('/', '-', $info['Commission']['date_to'])));
 echo $this->Form->input('Commission.date_to',array('type'=>'text','id'=>'datepicker1','value'=>$date_to));?>
 </div>
<?php
$memberselected=explode(",",$info['Commission']['author_name']);
foreach($memberselected as $member)
{
if(array_key_exists($member,$members))
{
$mem=$members[$member];
$members[$member]=array('name' => $mem, 'value' => $member,  'selected' => 'selected');
}
}
$selectcourses=array('All Courses','Particular Courses');
$selectcourse=$info['Commission']['courses'];
if(array_key_exists($selectcourse,$selectcourses))
{
$sel=$selectcourses[$selectcourse];
$selectcourses[$selectcourse]=array('name' => $sel, 'value' => $selectcourse,  'selected' => 'selected');
}
echo $this->Form->input('Commission.courses',array('options'=>$selectcourses,'empty'=>'Select Courses','id'=>'select-course','onchange'=>"coursesselection();",'label'=>false));


$courseelected=explode(",",$info['Commission']['particular-course']);
foreach($courseelected as $course)
{
if(array_key_exists($course,$courses))
{
$cou=$courses[$course];
$courses[$course]=array('name' => $cou, 'value' => $course,  'selected' => 'selected');
}
}
echo $this->Form->select('Commission.particular-course',
    $courses,
    array('multiple' => true,'style'=>'display:none;')
	
);


?>
							<input name="data[Commission][id]" type="hidden" value="<?php echo base64_encode(convert_uuencode(($info['Commission']['id']))); ?>" readonly="readonly" />
					<p class="university-add-Error" id="err_commission">
                    </p>
                  </li>
                  <li>
                    <input class="sub-bttn" type="submit" value="Submit" />
                  </li>
                </ul>
              </fieldset>
            </div>
          </div>
          <?php echo $this->Form->end(); ?> </div>
       
      </div>
      <div class="clearfix"></div>
    <!--  <div id="sidebar">
        
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            
