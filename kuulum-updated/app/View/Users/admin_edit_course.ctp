<script type="text/javascript">
$(document).ready(function(){
			
			/*$("#title").on('keyup',function(e){	
		  		var countVal = $('#title').val().length;
				
				
				if(countVal >10)
				{
					$(this).next('p').next('p').show();
					$('#title').attr('maxlength','10');
						
				}
				
				
					
	});*/
	
	$("#title").bind('paste',function(e){					
				setTimeout(function(e) {  
					var countVal = $("#title").val().length;
					
					var text = $("#title").val();
					
					var getval = $("#showCount").text();
					var total = 249 - countVal;
					if(total >= 0){
						$('#showCount').text(total);
						
					}else{				
						$("#showCount1").show();
						$("#title").val($("#title").val().substring(0, 250));
						
					}
				
				}, 15)
			});
			$("#title").bind('cut',function(e){					
				e.preventDefault();
			});
			$("#title").on('keyup',function(e){	
		  		var countVal = $(this).val().length;
				
				var text = $(this).val();
				
				var getval = $("#showCount").text();
				var total = 249 - countVal;
				if(total >= 0){
		  			$('#showCount').text(total);
				}else{					
					 $("#showCount1").show();
					 $(this).val($(this).val().substring(0, 250));
				}
		});
		$("#title").on('focusout',function(e){			
			 $(this).val($(this).val().substring(0, 250));
			 var countVal = $(this).val().length;			
			 var total = 250 - countVal;
			 $('#showCount').text(total);
		});
	
	
	
	
	
	});

</script>

<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                           Edit course details<br><small>Edit details of selected course</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full"> 
				<!--<h2>Edit Course</h2> 
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span>-->
			
			<!--<div class="content-box content-box-header" style="border:none;">
				<div class="column-content-box">
					<div class="content-box-wrapper"> -->
						
						<?php echo $this->Form->create('Course',array('url'=>array('controller'=>'users','action'=>'edit_course'),'id'=>'edit_course',"class"=>"js-validation-bootstrap form-horizontal")); ?>
							
							<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$coursName['Course']['id'])); ?> 
							
							<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Course Title:</label>
                                       <div class="col-md-6">	
						<?php echo $this->Form->input('title',array('type'=>'text','id'=>'title','maxlength'=>250,'div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$coursName['Course']['title'])); ?>
				
			<span class="university-add-Error" id="err_title">
					<?php if(isset($error['title'][0])) echo $error['title'][0]; ?>
						</span>
										</div></div>
									
									
									<?php $id=base64_encode(convert_uuencode($coursName['Course']['id']));?>
									
									<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Course Description:</label>
                                       <div class="col-md-6">	
										
					<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$coursName['Course']['id'])); ?> <?php echo $this->Form->input('description',array('type'=>'textarea','id'=>'discription','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$coursName['Course']['description'])); ?>
							<span class="university-add-Error" id="err_description">
					<?php if(isset($error['description'][0])) echo $error['description'][0]; ?>
											</span>
						</div></div>
					<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Course Price:</label>
                                       <div class="col-md-6">	
										
					<select class="form-control field select full required" id="price" name="data[Course][price]">
					<option value="">Select Price</option>
					<?php foreach($coursPrice as $key=>$val) { ?>
						<option selected="selected" value="<?php echo $key;?>"><?php echo 'USD '.$val; ?></option>
						<?php } ?>
						</select>
										
					<span class="university-add-Error" id="err_price">
						<?php if(isset($error['price'][0])) echo $error['price'][0]; ?>
					</span>
					</div></div>
						<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Meta Title:</label>
                                       <div class="col-md-6">				
                                   
                                   <?php echo $this->Form->input('meta_title',array('type'=>'text','id'=>'meta_title','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$coursName['Course']['meta_title'])); ?>

                                            <span class="university-add-Error" id="err_meta_title">
                                                    <?php if(isset($error['meta_title'][0])) echo $error['meta_title'][0]; ?>
                                            </span>
                                    </div></div>
                           
                            <?php $id=base64_encode(convert_uuencode($coursName['Course']['id']));?>

                           <div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Meta Description:</label>
                                       <div class="col-md-6">
                             
                                    <?php echo $this->Form->input('meta_description',array('type'=>'textarea','id'=>'meta_description','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$coursName['Course']['meta_description'])); ?>
                                            <span class="university-add-Error" id="err_meta_description">
                                                    <?php if(isset($error['meta_description'][0])) echo $error['meta_description'][0]; ?>
                                            </span>
                                    </div></div>
                           <div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Meta Keywords:</label>
                                       <div class="col-md-6">
                                  
                                    <?php echo $this->Form->input('meta_keyword',array('type'=>'text','id'=>'meta_keyword','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>trim($coursName['Course']['meta_keyword'],","))); ?>
                                            <span class="university-add-Error" id="err_meta_keyword">
                                                    <?php if(isset($error['meta_keyword'][0])) echo $error['meta_keyword'][0]; ?>
                                            </span>
                                    </div></div>
                           <div class="form-group">

                                       <div class="col-md-8 col-md-offset-2">
<button class="btn  btn-primary" type="submit" onclick='return ajax_form("edit_course","Users/validate_cours_ajax","newloading") '>Update</button>   
 <!--<input class=" btn sub-bttn btn-primary" type="submit" value="Update" onclick='return ajax_form("edit_course","Users/validate_cours_ajax","newloading") '/> -->
 <button class="btn  btn-primary" type="button" onclick="history.go(-1);">Cancel</button>   

                                    
                                    </div>  </div>
                            
						<?php echo $this->Form->end(); ?>
					</div>	
					
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
<div class="clear"></div>
</div>

	
