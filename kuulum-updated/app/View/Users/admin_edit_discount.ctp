<script type="text/javascript">

    $(document).ready(function() {
        
    
        $('.lang_tab').click(function(){ 
            
            $lang = $(this).attr('title');
            
            $('#lang_locale').val($lang);
        });
    });
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                           Edit discount code <br><small>Edit the details of the discount code</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
    <div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
<?php
    $i=1; 

    foreach($language as $lang)
    {
        ?>
        <li <?php if($i==1)echo "class='active'";?>>
        <a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
        <?php echo $lang['Language']['language']; ?>
        </a>
        </li>
        <?php $i++;
    }
?>  
            </ul>
        <?php 
        if($this->Session->check('success'))
        {
        ?>
        <div class="success ui-corner-all successdeveloperClass" id="success">
        <span>
        <?php echo $this->Session->read('success');?>
        </span>
        </div>
        <?php $this->Session->delete('success'); ?>
        <?php 
        }
        ?>
    
        <?php echo $this->Form->create('Discount',array('class'=>'js-validation-bootstrap form-horizontal editTemplateForm', 'id'=>'editdiscount','url'=>array('controller'=>'users','action'=>'edit_discount'))); ?>
        <input type="hidden" id="lang_locale" value="en" name="data[Discount][locale]"/> 
        <?php echo $this->Form->input('discount_code_old',array('id'=>'discount_code_old','type'=>'hidden','div'=>false,'label'=>false,'class'=>'form-control field text full','value'=>base64_encode(convert_uuencode($info['Discount']['discount_code'])))); ?>
        <div class="block-content tab-content">  
        <?php
        $i=1; 
        foreach($language as $lang)
        {
        $locale = $lang['Language']['locale'];      
        ?>
        <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
        <div class="form-group">
        <label for="val-username" class="col-md-2"> Title:</label>                                            
        <div class="col-md-7">
        <?php echo $this->Form->input('name_'.$locale,array('id'=>'pageid','type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required', "placeholder"=>"Enter title.",'value'=>$info['Discount']['name_'.$locale])); ?>
        <span class="university-add-Error" id="err_name_<?php echo $locale;?>"></span>
        </div>
        </div>
        <div class="form-group">
        <label for="val-username" class="col-md-2 "> Description:</label>                                            
        <div class="col-md-7">
        <textarea class="form-control" placeholder="Enter description" name="data[Discount][description_<?php echo $locale; ?>]" ><?php echo trim($info['Discount']['description_'.$locale]); ?></textarea>
        <span class="university-add-Error" id="err_description_<?php echo $locale; ?>">
        </span>
        </div>
        </div>
        <?php
        if($i==1)
        {
        ?>
        <div class="form-group">
        <label for="val-username" class="col-md-2 ">Discount Value:</label>                                            <div class="col-md-7">
        <div>
        <?php echo $this->Form->input('initial_value',array('id'=>'pageid','type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required',"placeholder"=>"Enter discount value in percentage.",'value'=>$info['Discount']['initial_value'],)); ?> </div>
        <span class="university-add-Error" id="err_initial_value"></span>
        </div>
        </div>
        <div class="form-group">
        <label for="val-username" class="col-md-2 "> Discount code:</label>
        <div class="col-md-7">
        <?php echo $this->Form->input('discount_code',array('type'=>'text','div'=>false,'label'=>false,'class'=>'form-control field text full required', "placeholder"=>"Enter discount code.","value"=>trim($info['Discount']['discount_code']))); ?>
        <span class="university-add-Error" id="err_discount_code">
        </span>
        </div>
        </div>



        <div class="form-group" style="margin-bottom:30px;">
        <label for="example-daterange1" class="col-md-2 ">Date Range:</label>
        <div class="col-md-1">
                                            <?php 
                                            if($info['Discount']['duration']==1)
                                            {
                                            $checked=true;
                                            }
                                            else
                                            {
                                            $checked=false;
                                            }
                                            
                                            echo $this->Form->input('Discount.unlimited_time',array('type'=>'checkbox','checked'=>$checked,'value'=>1,'id'=>'unlimited_time',"label"=>"Unlimited"))?>
                                            <span class="university-add-Error" id="err_unlimited_time"></span>
                                            </div>
                                            <?php
                                            
                                            if($info['Discount']['duration']==1)
                                                    {
                                                        $hidden=true;
                                                        $date_from='';
                                                        $date_to='';
                                                        $datepicker_display="style='display:none'";
                                                    }
                                                    else
                                                    {
                                                        $hidden=false;
                                                        $date_from=date('n/j/Y', strtotime($info['Discount']['date_from']));
                                                        $date_to=date('n/j/Y', strtotime($info['Discount']['date_to']));
                                                        $datepicker_display="style='display:block'";                                             
                                                    }
                                                    ?>
                                            <div class="col-md-6" <?php echo $datepicker_display;?>>
                                                <div style="display:table;" class="input_daterange">
                                                    <?php 
                                                    echo $this->Form->input('Discount.date_from',array("placeholder"=>"From","class"=>"form-control","readonly"=>true,'type'=>"text",'id'=>'datepicker11',"label"=>false,'value'=>$date_from,"style"=>"text-align:center;"))?>
                                                  <span class="university-add-Error" id="err_date_from" style="position:absolute;margin-top:2px"></span>
                                                  <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                                    <?php 
                                                    
                                                    echo $this->Form->input('Discount.date_to',array("placeholder"=>"To","class"=>"form-control","readonly"=>true,'type'=>"text",'id'=>'datepicker12',"label"=>false,'value'=>$date_to,"style"=>"text-align:center;"))?>
                                               <span class="university-add-Error" id="err_date_to" style="position:absolute;margin-top:2px"></span>
                                               
                                                </div>
                                                
                                            </div>

 <script>
 $(function() {
     $("#datepicker11" ).datepicker({
         endDate:new Date("<?php echo $date_to?>"),
         format:'m/d/yyyy',
     }).on('changeDate', function(selected){        
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#datepicker12').datepicker('setStartDate', startDate);
       
    }); 
        $( "#datepicker12" ).datepicker({
         startDate:new Date("<?php echo $date_from?>"),
         format:'m/d/yyyy',
     }).on('changeDate', function(selected){        
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#datepicker11').datepicker('setEndDate', FromEndDate);
       
    });
  });
  </script>
                                        </div>
                           <div class="form-group">
                                  <label for="val-username" class="col-md-2 ">Select Author:</label>                                            <div class="col-md-7">
                      
                        <?php 
foreach($published_authors as $a_key=>$author)
{
if(in_array($a_key,$authors_selected))
{
$published_authors[$a_key]=array('name' => $author, 'value' => $a_key,  'selected' => 'selected');
}
}

echo $this->Form->select(
    'Discount.author_name',
    $published_authors    ,
    array("id"=>"DiscountAuthorName01",'multiple' => true,'required'=>'required','onchange'=>'selectcourses();selectmembers();',"class"=>"form-control","div"=>false) 
      
);
?>
<span class="university-add-Error" id="err_author_name"></span>
                                  </div></div>
              <div class="form-group">
                        <label for="val-username" class="col-md-2 ">Select courses:</label>  
     <div class="col-md-7">
<?php
$courses_modal = ClassRegistry::init('Courses');
$member_modal = ClassRegistry::init('Member');

        $newcourses=array();
    foreach($authors_selected as $auth_key=>$author)
    {
    
    $member=$member_modal->find('all',array('conditions'=>array('Member.id'=>$author),'fields'=>array('given_name')));
    $courses=$courses_modal->find('all',array('conditions'=>array('isPublished'=>1,'status !='=>2,'m_id'=>$author),'fields'=>array('id','title')));
    
    if(!empty($courses))
    {
    
    asort($courses);
    
    foreach($courses as $course)
    {
        $key1=$course['Courses']['id'];
        $newcourses[$member[0]['Member']['given_name']][$key1]=$course['Courses']['title'];
    
        }
    
    }
    }

         ?>
         <?php echo $this->Form->input('Discount.particular-course', array("label"=>false,"class"=>"form-control","id"=>'DiscountParticular-course01','type' => 'select', 'multiple' => true, 'options' => $newcourses, 'selected' => $courses_selected)); ?>
<span class="university-add-Error" id="err_particular-course"></span>
 </div>    
 </div>    
              <div class="form-group">
                                  <label for="val-username" class="col-md-2 ">Select Members:</label>                                            <div class="col-md-7">
                       <!-- <label class="desc" ><?php echo __('Select Author');?></label> -->
                           
                                      
                                      <?php
                                      
                                      
       $published_members=$member_modal->find('all',array('conditions'=>array('NOT'=>array('Member.id'=>$authors_selected)),'fields'=>array('id','given_name')));
        $newmembers=array();
        foreach($published_members as $member)
    {
        $key1=$member['Member']['id'];
        $newmembers[$key1]=$member['Member']['given_name'];
    
        }
        
    ?>
   <?php 
   echo $this->Form->input('Discount.members', array("label"=>false,"class"=>"form-control","id"=>'DiscountMember01','type' => 'select', 'multiple' => true, 'options' => $newmembers, 'selected' => $members_selected)); ?>                                   
<span class="university-add-Error" id="err_members"></span>
                                  </div></div>
                                
                 <?php } ?>
                    <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                    <input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("editdiscount","Users/validate_edit_discount_ajax","newloading")'/>
                    </div></div>
                                        </div> <?php ++$i; } ?>
                                     
        </div>
            <?php echo $this->Form->end(); ?> 
    


      </div>
      <div class="clearfix"></div>
     
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            

<!-- Disable Date Range fields if unlimited checkbox is true -->
<script>
$(document).ready(function(){
    $("#unlimited_time").click(function(){
        if($("#unlimited_time").is(":checked")==true)
        {
            $("#datepicker11").val("");
            $("#datepicker12").val("");
            $(".input_daterange").parent().css("display","none");
        }
        else
        {
            $(".input_daterange").parent().css("display","block");
        }
    })
})
</script>
            
