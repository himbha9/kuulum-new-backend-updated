<?php echo $this->Html->script('jquery.validate.js'); ?>
<style>
form li span {
	width:100%;
}
tr.mceLast {
	display:none;
}
.cke_bottom {
    padding: 6px 0 2px 2px !important;
    width: 99.8% !important;
}
.cke_top {
    margin: 0 0 2px 1px !important;
    padding: 6px 0 2px !important;
    width: 100% !important;
}
</style>
<script type="text/javascript">
	
	$(document).ready(function() {
	
		$('.editTemplateForm').validate();
	});
	
</script>

<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                          Default email addresses for <?php echo $info['EmailTemplate']['title']; ?> <br><small>Enter the default email address for new member registrations</small>
                            </h1>
                          <div class="clear"></div>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="">

    
	<div class="block">

		<div class="block-content">
    <div class="inner-page-title">
    
      <span></span> </div>
    <?php if($this->Session->check('error')){ ?>
    <div class="response-msg error ui-corner-all"> <span>Error message</span> <?php echo $this->Session->read('error');?> </div>
    <?php $this->Session->delete('error'); ?>
    <?php } ?>
   
			
			
          <form id="admin_email" class="js-validation-bootstrap form-horizontal editTemplateForm" method="post" action="<?php echo HTTP_ROOT;?>admin/users/edit_email">
			<input type="hidden" value="<?php echo $info['EmailTemplate']['id']?>" name="data[EmailTemplate][id]" />
                        <div class="push-50-t">
                        </div>
                      <div class="form-group">
                                       <label for="val-username" class="col-md-2 text-right " >Email address:</label>
                                       <div class="col-md-7">	
                    
                        <input  class="form-control field text full" name="data[EmailTemplate][email_to]" type="text" value="<?php echo $info['EmailTemplate']['email_to']?>" />
						<span class="university-add-Error" id="err_email_to">
						  <?php if(isset($error['email_to'][0])) echo $error['email_to'][0]; ?>
						</span>
                      </div>
                      </div>
                   <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                      <input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("admin_email","Users/validate_email_ajax","newloading") '/>
                   
              </div>
            </div>
				
          </form>
        </div>
            <div class="block-content"></div>
      </div>
      <div class="clearfix"></div>
    <!--  <div id="sidebar">
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>

<div class="clear"></div>
