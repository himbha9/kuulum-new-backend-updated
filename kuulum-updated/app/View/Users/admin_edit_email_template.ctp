<style>
form li span {
	width:100%;
}
tr.mceLast {
	display:none;
}
.cke_bottom {
    padding: 6px 0 2px 2px !important;
    width: 99.8% !important;
}
.cke_top {
    margin: 0 0 2px 1px !important;
    padding: 6px 0 2px !important;
    width: 100% !important;
}
</style>
<script type="text/javascript">
	
	$(document).ready(function() {
	
		//$('#tabs, #tabs2, #tabs5').tabs();
		
		$('#edit_email_template').validate();

		CKEDITOR.replaceAll('editor1',
		{
		   contentsLangDirection :'ltr'
		});

               CKEDITOR.config.filebrowserBrowseUrl = ajax_url+'js/admin/js/ckfinder/ckfinder.html';
		CKEDITOR.config.filebrowserImageBrowseUrl = ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Images';
		CKEDITOR.config.filebrowserFlashBrowseUrl = ajax_url+'js/admin/js/ckfinder/ckfinder.html?type=Flash';
		CKEDITOR.config.filebrowserUploadUrl = ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
		CKEDITOR.config.filebrowserImageUploadUrl = ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
		CKEDITOR.config.filebrowserFlashUploadUrl = ajax_url+'js/admin/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
		/*var oFCKeditor = new FCKeditor() ;
		FCKeditor.BasePath	= ajax_url+'js/fckeditor/' ;
		//oFCKeditor.BasePath	= sBasePath ;
		//oFCKeditor.ReplaceTextarea() ;
		FCKeditor.ReplaceAllTextareas() ;*/
	});
	
</script>

<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                          <?php echo $info[0]['EmailTemplate']['title'];?>
                                <br><small>Edit the text and format of the template</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div  class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php $i=1; foreach($language as $lang){  ?>
                                        <li <?php if($i==1)echo "class='active'";?>><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>" rel="<?php echo $lang['Language']['locale'] ?>"   href="#tabs-<?php echo $i; ?>" href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>
                                       <li class="pull-right">
<button class="btn btn-minw btn-primary" type="button" onclick="history.go(-1);">Back</button>
</li>
			</ul>
     
    <?php if($this->Session->check('error')){ ?>
    <div class="response-msg error ui-corner-all"> <span>Error message</span> <?php echo $this->Session->read('error');?> </div>
    <?php $this->Session->delete('error'); ?>
    <?php } ?>
    
			
          <form id="edit_email_template" name="edit_email_template" class="js-validation-bootstrap form-horizontal editTemplateForm" method="post" action="<?php echo HTTP_ROOT;?>admin/users/edit_email_template">
              	 <div class="block-content tab-content"> 
			<?php $i = 1; 
			
			
				foreach($info as $info) { ?>
         <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
               <div class="form-group">
                                  <label for="val-username" class="col-md-2  text-left"> Title:</label>                                            <div class="col-md-7">
                                      <input  class="form-control field text full required" name="data[<?php echo $i; ?>][EmailTemplate][title]" type="text" value="<?php echo $info['EmailTemplate']['title']?>" />
                                  </div></div>
                     <!-- <label class="desc" >Title: <font> <?php echo $info['EmailTemplate']['title']?></font></label>-->
                     <div class="form-group">
                                  <label for="val-username" class="col-md-2  text-left"> Subject:</label>                                            <div class="col-md-7">
					 
					  
                        <input name="data[<?php echo $i; ?>][EmailTemplate][id]" type="hidden" value="<?php echo $info['EmailTemplate']['id']?>"  readonly="readonly"/>
                        <input  class="form-control field text full required" name="data[<?php echo $i; ?>][EmailTemplate][subject]" type="text" value="<?php echo $info['EmailTemplate']['subject']?>" />
                                  </div></div>
                    <div class="form-group">
                                  <label for="val-username" class="col-md-2  text-left"> From Email:</label>                                            <div class="col-md-7">
                     
                        <input  class="form-control field text full required email" name="data[<?php echo $i; ?>][EmailTemplate][email_from]" type="text" value="<?php echo $info['EmailTemplate']['email_from']?>" />
                      </div></div>
                   
                    <div class="form-group">
                        <div class="col-md-12">
                        <?php /*?><textarea class="tinymce" style="width:100%;" name="data[<?php echo $i; ?>][EmailTemplate][html_content]"><?php echo $info['EmailTemplate']['html_content']; ?></textarea><?php */?>
                        <textarea class="form-control editor1" style="width:100%;" name="data[<?php echo $i; ?>][EmailTemplate][description]"> <?php echo $info['EmailTemplate']['description']; ?></textarea>
						
                      </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                      <input class="btn  btn-primary" type="submit" value="Submit"/>
                   
                    </div>
                  </div>
             </div>
			<?php $i++; } ?>
                 </div>
          </form>
        </div>
      </div>
      <div class="clearfix"></div>
    <!--  <div id="sidebar">
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  