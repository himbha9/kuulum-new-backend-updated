<?php //echo $this->Html->script('jquery.validate.js'); ?>
<script type="text/javascript">
	
	$(document).ready(function() {
	$("#edit_feature").submit(function (){
	 
		var title = $("#FeatureCourseFeaturedBy").val();
		//alert(title); return false;
		if(title=='')
		{
			$("#err_feature_by").text('This field is required');
			return false;
		}
	});		
		
	});
	
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Featured courses <br><small>Define how to display featured courses</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
      <!--<h2>EDIT FEATURED COURSE</h2> 
	  <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>-->
      <span></span> </div>
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
     <?php echo $this->Form->create('FeatureCourse',array('class'=>'js-validation-bootstrap form-horizontal editTemplateForm','id'=>'edit_feature','url'=>'/admin/users/edit_featured_course','enctype'=>'multipart/form-data')); ?>
         
                 	
					 <input name="data[FeatureCourse][id]" type="hidden" value="<?php echo base64_encode(convert_uuencode($feature['FeatureCourse']['id']));?>"  readonly="readonly"/>
                                        
                    <div class="form-group">
                                       <label for="val-username" class="col-md-1 " >Featured By:</label>
                                       <div class="col-md-5"> <?php //echo $this->Form->input('FeatureCourse.feature_by',array('id'=>'pageid','type'=>'text','div'=>false,'label'=>false,'class'=>'field text full required','value'=>$feature_by)); 
					
					
					$options = array('1' => 'Most frequently watched', '2' => 'Most recently published','3'=>'Alphabetical by title','4'=>'Random');
					echo $this->Form->select('featured_by', $options, array('value'=>$feature['FeatureCourse']['featured_by'],'empty'=>'please select','escape' => false,'class'=>"form-control","div"=>false));
					
					?> 
					<span class="university-add-Error" id="err_feature_by">
					  <?php if(isset($error['feature_by'][0])) echo $error['title'][0]; ?>
                                        </span>
                                            </div></div>
                  <div class="form-group">
                                            <div class="col-md-8 col-md-offset-1">
										   <input class="btn btn-sm btn-primary" type="submit" value="Submit" />
</div>
                                        </div>
                    
           
          <?php echo $this->Form->end(); ?> </div> 
                </div>
          </div>
       </div>
    </div>
      <div class="clearfix"></div>
  <!--    <div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
    
    <div class="clear"></div>
      
