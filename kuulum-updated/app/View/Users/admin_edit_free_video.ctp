<!--<script type="text/javascript">
		
	$(document).ready(function()
	{
		
		//$('#tabs, #tabs2, #tabs5').tabs();

		/*$('#add_category').validate();*/
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			
			$('#lang_locale').val($lang);
		});
		$('#free').change(function()
		{
			free = $('#free').val();
			//alert(free);
			if(free=='own_video')
			{
				
				$('.freevideo').hide();
				$('.ownvideo').show();
				$('#url').val('');
				
			}else
			{
				$('.freevideo').show();
				$('.ownvideo').hide();
				
			}
		});
		$('#image').bind('change',function(){
		
			var imagePath = $('#image').val();//document.FormTwo.picFile.value;
			var pathLength = imagePath.length;
			var lastDot = imagePath.lastIndexOf(".");
			var fileType = imagePath.substring(lastDot,pathLength);	
			var fileType = fileType.toLowerCase();
				
			if(! (fileType=='.mp4' || fileType=='.flv' || fileType=='.wmv' || fileType=='.avi' || fileType=='.mov'))
			{
				$('#image').val('');
				
				alert('<?php echo __('Invalid formate! Please provide only mp4,wmv,avi,mov and flv formate.');?>');
				
			}
			
	
		});
		
		
	});
		
</script> -->

<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                           Edit third party video <br><small>Edit a third party video.</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php $i=1; foreach($language as $lang){  ?>
                                        <li <?php if($i==1)echo "class='active'";?>><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>" rel="<?php echo $lang['Language']['locale'] ?>" href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>
  <li class="pull-right">
<button class="btn btn-minw btn-primary" type="button" onclick="history.go(-1);">Back</button>
</li>	
			</ul>
				
						
						<?php echo $this->Form->create('FreeVideo',array('url'=>array('controller'=>'users','action'=>'edit_free_video'),'id'=>'edit_free','enctype'=>'multipart/form-data',"class"=>"js-validation-bootstrap form-horizontal ")); ?>
							
		<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$freeVideoiIfo['FreeVideo']['id'])); ?>
							
								<input type="hidden" id="lang_locale" value="en" name="data[FreeVideo][locale]"/>							
							
							<div class="block-content tab-content">  
<?php								
	$i=1; 
									foreach($language as $lang)
									{
										$locale = $lang['Language']['locale'];								
									?>
							
								<div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
	 <div class="form-group">
                                  <label for="val-username" class="col-md-1 "> Title:</label>                                            <div class="col-md-6">
			<?php echo $this->Form->input('title_'.$locale,array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$freeVideoiIfo['FreeVideo']['title_'.$locale],"placeholder"=>"Enter title..")); ?>
											<span class="university-add-Error" id="err_title_<?php echo $locale; ?>">
												<?php if(isset($error['title_'.$locale][0])) echo $error['title_'.$locale][0]; ?>
											</span>
										</div></div>
									<div class="form-group">
                                  <label for="val-username" class="col-md-1"> Description:</label>                                            <div class="col-md-6">
										
										<?php echo $this->Form->input('description_'.$locale,array('type'=>'text','id'=>'description','div'=>false,'label'=>false,"placeholder"=>"Enter description..",'class'=>'form-control field text full required','value'=>$freeVideoiIfo['FreeVideo']['description_'.$locale])); ?>
											<span class="university-add-Error" id="err_description_<?php echo $locale; ?>">
												<?php if(isset($error['description_'.$locale][0])) echo $error['description_'.$locale][0]; ?>
											</span>
										</div></div>
									
								
									
									 <?php if($i==1) { ?>
				 <div class="form-group">
                                  <label for="val-username" class="col-md-1"> Web URL:</label>                                            <div class="col-md-6">
										 <?php echo $this->Form->input('free_video_url',array('type'=>'text','id'=>'url','placeholder'=>'Youtube Embed URL','div'=>false,'label'=>false,'value'=>$freeVideoiIfo['FreeVideo']['free_video_url'],'class'=>'form-control field text full required','placeholder'=>'Enter the public Youtube URL')); ?>
											<span class="university-add-Error" id="err_free_video_url">
												<?php if(isset($error['free_video_url'][0])) echo $error['free_video_url'][0]; ?>
											</span>
										</div></div>
									<?php } ?>
									
								 <div class="form-group">
                                            <div class="col-md-8 col-md-offset-1">									<input class="btn btn-sm btn-primary" type="submit" id="submitId" value="Submit" onclick='return ajax_form("edit_free","Users/validate_free_video_ajax","newloading") '/>
										</div>
								</div>
                                                                </div>
								<?php  $i++; }  ?> 
						</div>
						<?php echo $this->Form->end(); ?>
					</div>	
				</div>
			</div>			
				
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>

<div class="clear"></div>
