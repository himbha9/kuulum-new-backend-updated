<?php //echo $this->Html->script('jquery.validate.js'); ?>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Edit keyword <br><small>Edit keyword details</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">


		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
    <div class="inner-page-title">
   <!--   <h2>Edit Interest</h2>
	  <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a> -->
      <span></span> </div>	  
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
    
        <?php echo $this->Form->create('Interest',array("class"=>"js-validation-bootstrap form-horizontal", 'id'=>'editinterest','url'=>array('controller'=>'users','action'=>'admin_edit_interest'))); ?>
        <div class="form-group">
                                       <label for="val-username" class="col-md-1 " >Keyword:</label>
                                       <div class="col-md-6">	
                                           <input name="data[Interest][id]" type="hidden"  value="<?php echo $info['Interest']['id']?>"  readonly="readonly"/>
                     <?php echo $this->Form->input('Interest.interest',array('id'=>'pageid','type'=>'text','value'=>$info['Interest']['interest'],'div'=>false,'label'=>false,'class'=>'form-control field text full required',)); ?> 
					<span class="university-add-Error" id="err_interest">
                    </span>
                                       </div></div>
                 <div class="form-group">
                                       <label for="val-username" class="col-md-1 " >Status:</label>
                                       <div class="col-md-6">	 
                                           <?php echo $this->Form->input('status',array('type'=>'select','id'=>'student_status','div'=>false,'label'=>false,'class'=>'form-control field text full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>$info['Interest']['status'])); ?>
                    <span class="university-add-Error" id="err_lat">
                    </span>
                                       </div></div>
                  <div class="form-group">
                                            <div class="col-md-8 col-md-offset-1">
                    <input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("editinterest","Users/validate_edit_interest_ajax","newloading") '/>
                
            </div>
          </div>
          <?php echo $this->Form->end(); ?> </div>
        <?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul>
					<?php */?>
      </div>
      <div class="clearfix"></div>
      <!--<div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            
