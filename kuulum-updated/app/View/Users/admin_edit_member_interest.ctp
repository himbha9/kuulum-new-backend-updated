
<script type="text/javascript">
		
		$(document).ready(function(){
				
				$('#edit_member_interest').validate();
				
		});
		
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                               Edit member interest<br><small>Edit a member interest</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				<!--<h2>Add Member</h2> -->
		<!--		<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span>
			</div>
			<?php if($this->Session->check('success')){ ?>
				<div class="success ui-corner-all successdeveloperClass" id="success"> <span class='successMessageText'> <?php echo $this->Session->read('success');?> </span> </div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
				
			 <div class="content-box content-box-header" style="border:none;">
				<div class="column-content-box">
					<div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span> Add Member </div>
					<div class="content-box-wrapper"> -->
						<?php echo $this->Form->create('MemberKeywords',array('url'=>array('controller'=>'users','action'=>'edit_member_interest'),'id'=>'edit_member_interest','enctype'=>'multipart/form-data',"class"=>"js-validation-bootstrap form-horizontal")); ?>
<input type="hidden" id="m_id" name="data[MemberKeywords][m_id]" value="<?php echo $info['MemberKeywords']['m_id']; ?>"/>
<input type="hidden" id="id" name="data[MemberKeywords][id]" value="<?php echo $info['MemberKeywords']['id'];  ?>"/>
			<div class="form-group">
                                       <label for="val-username" class="col-md-1 " >Interest:</label>
                                       <div class="col-md-6">	
                        <?php echo $this->Form->input('keyword',array('type'=>'text','id'=>'keyword','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$info['MemberKeywords']['keyword'])); ?>
			<p class="university-add-Error" id="err_keyword">
		<?php if(isset($error['keyword'][0])) echo $error['keyword'][0]; ?>
		</p>
</div>		</div>	
								

<div class="form-group">
                                            <label for="val-username" class="col-md-1" >Status:</label>
                                            <div class="col-md-6">	
			<?php echo $this->Form->input('status',array('type'=>'select','id'=>'status','div'=>false,'label'=>false,'class'=>'form-control field select full required','options'=>array('0'=>'Inactive','1'=>'Active'),'selected'=>$info['MemberKeywords']['status'])); ?>
							<p class="university-add-Error" id="err_status">
							<?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
											</p>
										</div>
</div>
	
									
									<div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
										<input class="btn btn-sm btn-primary" type="submit" value="Submit" />
</div>
                                        </div>
										<!--<div class="loading" style="margin-left: -37% !important;"> <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px;'));?> </div> -->
									
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		<!--	<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
