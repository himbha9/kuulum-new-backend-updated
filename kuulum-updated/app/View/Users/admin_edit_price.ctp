<script type="text/javascript">
	
	$(document).ready(function() {
	
		$('#tabs, #tabs2, #tabs5').tabs();

		/*$('#add_category').validate();*/
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			
			$('#lang_locale').val($lang);
		});
		
	});
</script>
<?php //echo $this->Html->script('jquery.validate.js'); ?>
<div id="sub-nav">
  <div class="page-title">
    <h1> Edit Subscription Price</h1>
  </div>
</div>
<div id="page-layout">
<div id="page-content">
  <div id="page-content-wrapper" class="no-bg-image wrapper-full">
    <div class="inner-page-title">
      <h2>Subscription Price Detail</h2>
	  <a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
      <span></span> </div>
    <?php if($this->Session->check('success')){ ?>
    <div class="success ui-corner-all successdeveloperClass" id="success"> <span > <?php echo $this->Session->read('success');?> </span> </div>
    <?php $this->Session->delete('success'); ?>
    <?php } ?>
    <div class="content-box content-box-header" style="border:none;">
	<div id="tabs"> 
			<ul>
				<?php $i=1; foreach($language as $lang){ ?>
					<li><a class="lang_tab" title="<?php echo $lang['Language']['locale'] ?>" href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
				<?php $i++; } ?>	
			</ul>
	  
        <div class="column-content-box">
          
          <div class="content-box-wrapper">
			<?php echo $this->Form->create('Price',array('class'=>'editTemplateForm', 'id'=>'editprice','url'=>array('controller'=>'users','action'=>'edit_price'))); ?>
			<input type="hidden" id="lang_locale" value="en" name="data[Price][locale]"/>	
			 <input name="data[Price][id]" type="hidden" value="<?php echo $info['Price']['id']?>"  readonly="readonly"/>
			<?php 
				$i=1; 
				foreach($language as $lang)
				{
					$locale = $lang['Language']['locale'];								
			?>
					
								
			<div id="tabs-<?php echo $i; ?>">
				<fieldset>
                <ul>                 
                  <li>
                    <label class="desc" >Name <?php echo $locale; ?></label>
					 
                    <div> <?php echo $this->Form->input('Price.name_'.$locale,array('id'=>$locale,'type'=>'text','div'=>false,'value'=>$info['Price']['name_'.$locale],'label'=>false,'class'=>'field text full required',)); ?> </div>
					<p class="university-add-Error" id="err_name_<?php echo $locale?>">
                    </p>
                  </li>
				 
                  <li>
                    <label class="desc" >Description </label>
                    <div style="float:left; width:100%;"> <textarea  style="width:100%;" name="data[Price][description_<?php echo $locale?>]"><?php echo trim($info['Price']['description_'.$locale]); ?></textarea></div>
					<p class="university-add-Error" id="err_description_<?php echo $locale?>">
                    </p>
                  </li>
				  
				  <?php if($i==1){?>
				  <li>
                    <label class="desc" >Number of months</label>
					<?php $opt=array('1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'10'=>10,'11'=>11,'12'=>12); ?>
                    <div style="float:left; width:100%;"><?php echo $this->Form->select('Price.no_of_months',$opt,	array('empty'=>'--Select Number of months--','value'=>$info['Price']['no_of_months']));?></div>
					<p class="university-add-Error" id="err_no_of_months">
                    </p>
                  </li>
				  <li>
                    <label class="desc" >Initial Value </label>
                   <div> <?php echo $this->Form->input('Price.initial_value',array('id'=>'pageid','type'=>'text','value'=>$info['Price']['initial_value'],'div'=>false,'label'=>false,'class'=>'field text full required',)); ?> </div>
				   <p class="university-add-Error" id="err_initial_value">
                    </p>
                  </li>
				  <?php }?>
                  <li>
                    <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("editprice","Users/validate_edit_price_ajax","newloading") '/>
                  </li>
                </ul>
              </fieldset>
				
			</div>	
			<?php  $i++;  } ?>
           <?php echo $this->form->end(); ?>
          </div>
        </div>
		</div>
	
      <div class="clearfix"></div>
   <!--   <div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
            
