
<script type="text/javascript">
	
	$(document).ready(function() {
	
		//$('#tabs, #tabs2, #tabs5').tabs();

		$('#editReson').validate();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('rel');
			
			$('#lang_locale').val($lang);
		});
		
	});
	
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Edit cancellation reasons <br><small>Edit a cancellation reasons</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
				<?php $i++;
                                }
                                ?>
 <li class="pull-right">
<button class="btn btn-minw btn-primary" type="button" onclick="history.go(-1);">Back</button>
</li>	
			</ul>
                   
              
    
						<?php echo $this->Form->create('Reason',array('url'=>array('controller'=>'users','action'=>'edit_reason'),'id'=>'editReson',"class"=>"js-validation-bootstrap form-horizontal editTemplateForm")); ?>
						
						<?php echo $this->Form->input('id',array('type'=>'hidden', 'value'=>$info['Reason']['id'],'div'=>false,'label'=>false,'class'=>'field text full required')); ?>
						<input type="hidden" id="lang_locale" value="en" name="data[Reason][locale]"/>
						 <div class="block-content tab-content">  
						<?php 
							$i=1; 
							foreach($language as $lang)
							{
								$locale = $lang['Language']['locale'];								
						?>
				 <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
                <div class="form-group">
                <label for="val-username" class="col-md-1 control-label"> Reason:</label>
                <div class="col-md-7">
												<?php echo $this->Form->input($locale,array('type'=>'text','value'=>$info['Reason'][$locale],'id'=>$locale,'div'=>false,'label'=>false,'class'=>'form-control field text full required')); ?>

												<span class="university-add-Error" id="err_<?php echo $locale;?>">
													<?php if(isset($error[$locale][0])) echo $error[$locale][0]; ?>
												</span>
			</div></div>									
											
																		
		 <div class="form-group">
                <div class="col-md-8 col-md-offset-1">
											<input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("editReson","Users/validate_edit_reason_ajax","newloading") '/>
											
																			
								</div>	</div>	</div>
						<?php	$i++;
							}
						?>
						</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>      
		
			<div class="clearfix"></div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>