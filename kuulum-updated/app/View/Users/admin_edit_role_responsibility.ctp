<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Edit role <br><small>Edit a member role</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<!--<div class="inner-page-title">
     <h2>Edit Roles and Responsibilities</h2>
        <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> </div> -->
      <?php if($this->Session->check('error')){ ?>
      <div class="response-msg error ui-corner-all"> <span>Success message</span> <?php echo $this->Session->read('error');?> </div>
      <?php $this->Session->delete('error'); ?>
      <?php } ?>
      <div class="content-box content-box-header" style="border:none;">
        <div class="column-content-box">
          <div class="content-box-wrapper"> <?php echo $this->Form->create('Role',array('url'=>array('controller'=>'users','action'=>'edit_role_responsibility'),'id'=>'edit_course',"class"=>"js-validation-bootstrap form-horizontal")); ?>
           	<?php $id=base64_encode(convert_uuencode($info['Role']['id']));?>
           
                                        <div class="form-group">
                                       <label for="val-username" class="col-md-1 " >Role:</label>
                                       <div class="col-md-7">	
                <!--  <label class="desc" >Role:<font> <?php echo $info['Role']['role'];?></font> </label>-->
                 
                  <?php echo $this->Form->input('role',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$info['Role']['role'])); ?>
    <span class="university-add-Error" id="err_role"> <?php if(isset($error["role"][0]))echo $error["role"][0]; ?></span>
</div>
</div>

                                        <div class="form-group">
                                       <label for="val-username" class="col-md-1 " >Permission:</label>
                                       <div class="col-md-7">	
           
				  	<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$info['Role']['id'])); ?> 
               <?php  $options = array('BC'=>'Browse Site','EAP'=>'Enter authenticated Partition','EOM'=>'Edit own membership','PC'=>'Purchase course','PS'=>'Purchase subscription','CC'=>'Add courses','PN'=>'Publish news','MC'=>' Manage courses','MP'=>'Manage programs','MA'=>'Manage administrators','VCFR'=>'View financial reports'); ?>

<?php echo $this->Form->input('permission',array('type'=>'select','multiple'=>'checkbox','id'=>'selects','div'=>false,'label'=>false,'options'=>$options,'class'=>'','selected'=>$arr)); ?>

                    <span class="university-add-Error" id="err_permission">
                      <?php if(isset($error['description'][0])) echo $error['description'][0]; ?>
                    </span>
 </div> </div>
           <div class="form-group">
                                            <div class="col-md-8 col-md-offset-1">

  <input class="btn btn-sm btn-primary" type="submit" value="Update" onclick='return ajax_form("edit_course","Users/validate_role_per_ajax","newloading") '/>

										
</div>
                                        </div>
              <!--    <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("edit_course","Users/validate_role_per_ajax","newloading") '/>
                  <div class="newloading"> <?php echo $this->Html->image('wait.gif',array('height'=>'32px'));?> </div>-->
              
            </form>
          </div>
        </div>      
      </div>
      <div class="clearfix"></div>
     <!-- <div id="sidebar">       
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
 <div class="clear"></div>
 <style>
 .edit_div_cont
 {
	 float:left;
	 width:100%;
 }
 .edit_div_cont
 {
	 float:left;
	 width:100%;
 }
 .box_container
 {
	 float:left;
	 width:100%;
	 padding: 4px;
 }
  .box_container input
 {
	 float:left;
	 width:2%;
 }
 .box_container label
 {
	 float:left;
	 width:80%;
	 clear: none;
	 padding-top:2px;
 }
 </style>