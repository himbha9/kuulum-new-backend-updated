
<script type="text/javascript">
	
	$(document).ready(function() {
	
		$('#tabs, #tabs2, #tabs5').tabs();

		/*$('#edit_subcategory').validate();*/
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('rel');
			$('#lang_locale').val($lang);
		});
		
	});
</script>

<div id="sub-nav">
	<div class="page-title">
		<h1>Edit SL Sub-Category</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2>Edit SL Sub-Category</h2>
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a>
				<span></span>
			</div>

			<div class="content-box content-box-header" style="border:none;">
				
				<div id="tabs"> 
					<ul>
						<?php $i=1; foreach($language as $lang){ ?>
							<li><a  class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>" href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
						<?php $i++; } ?>	
					</ul>
				
					<div class="column-content-box">
						<div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-notice"></span>Edit SL Subcategory</div>
						<div class="content-box-wrapper">
						<?php echo $this->Form->create('CourseSlSubcategory',array('url'=>array('controller'=>'users','action'=>'edit_sl_subcategory'),'id'=>'edit_subcategory')); ?>
							<input type="hidden" id="lang_locale" value="en" name="data[CourseSlSubcategory][locale]"/>	
							<?php
								$id=base64_encode(convert_uuencode($info['CourseSlSubcategory']['id']));
								$sub_cat_id = base64_encode(convert_uuencode($info['CourseSlSubcategory']['sub_category_id']));
								$status = $info['CourseSlSubcategory']['status'];
							?>
							
							<?php echo $this->Form->input('id',array('type'=>'hidden','div'=>false,'label'=>false,'value'=>$id)); ?>
							<?php echo $this->Form->input('sub_category_id',array('type'=>'hidden','div'=>false,'label'=>false,'value'=>$sub_cat_id)); ?>
							
							<?php 
								$i=1; 
								foreach($language as $lang)
								{
									$locale = $lang['Language']['locale'];								
									$name = $info['CourseSlSubcategory'][$locale];
							?>
									<div id="tabs-<?php echo $i; ?>">
							
										<ul>
											<li>
												<span class="mandatory">*</span> <label class="desc" >Sub-Category</label>
												
												<div> <?php echo $this->Form->input($locale,array('type'=>'text','id'=>'name','div'=>false,'label'=>false,'value'=>$name,'class'=>'field text full required')); ?>
													<p class="university-add-Error" id="err_<?php echo $locale; ?>"><?php if(isset($error['name'][0])) echo $error[$locale][0]; ?></p>
												</div>
											</li>
											
											<?php /* if($i == 1){ ?>

												<li>
													<label class="desc" >Current Status</label>
													<div> <?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'style'=>'height:25px','class'=>'select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>$status)); ?>
														<p class="university-add-Error" id="err_status"><?php if(isset($error['status'][0])) echo $error['status'][0]; ?></p>
													</div>
												</li>

											<?php } */?>		
											
											<li>
												<input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("edit_subcategory","Users/validate_sl_subcategory_ajax","loading") '/>
												<div class="loading" style="margin-left: -37% !important;"> <?php echo $this->Html->image('front/wait.gif',array('height'=>'32px;'));?> </div>
											</li>
										</ul>
										
									</div>								
							<?php 
									$i++; 
								} 
							?>
								
						<?php echo $this->Form->end(); ?>
					</div>
        </div>        
      </div>
      <div class="clearfix"></div>
      <!--<div id="sidebar">
        <?php //echo $this->element('adminElements/left_right_bar');?>
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>