<?php /* if(isset($info)&& !empty($info)){
		 $id=base64_encode(convert_uuencode($info['CourseSubcategory']['id']));
		 $category_id=base64_encode(convert_uuencode($info['CourseSubcategory']['category_id']));
		 $status=$info['CourseSubcategory']['status'];
	 } */
?>

<script type="text/javascript">
	
	$(document).ready(function() {
	
		//$('#tabs, #tabs2, #tabs5').tabs();

		//$('#edit_subcategory').validate();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('rel');
			//alert($lang);
			$('#lang_locale').val($lang);
		});
		
	});
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                            Edit course subcategory <br><small>Edit a course subcategory</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">
	<div class="block">
          <ul data-toggle="tabs" class="nav nav-tabs">
				<?php
                                $i=1; 
                                foreach($language as $lang)
                                {
                                ?>
                                <li <?php if($i==1)echo "class='active'";?>>
                                <a class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>" title="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>">
                                <?php echo $lang['Language']['language']; ?>
                                </a>
                                </li>
				<?php $i++;
                                }
                                ?>	
			</ul>

<!--<div id="sub-nav">
	<div class="page-title">
		<h1>Edit Sub-Category</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2>Edit Sub-Category</h2>
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a>
				<span></span>
			</div> 

			<div class="content-box content-box-header" style="border:none;">
		
				<div id="tabs"> 
					<ul>

						<?php $i=1; foreach($language as $lang){ ?>
							<li><a class="lang_tab" rel="<?php echo $lang['Language']['locale'] ?>"  href="#tabs-<?php echo $i; ?>"><?php echo $lang['Language']['language']; ?></a></li>
						<?php $i++; } ?>	
					</ul>
					
					<div class="column-content-box">
						<div class="content-box-wrapper"> -->
							<?php echo $this->Form->create('CourseSubcategory',array('url'=>array('controller'=>'users','action'=>'edit_subcategory'),'id'=>'edit_subcategory',"class"=>"js-validation-bootstrap form-horizontal editTemplateForm")); ?>

 <div class="block-content tab-content">  		  	               
 <input type="hidden" id="lang_locale" value="en" name="data[CourseSubcategory][locale]"/>	 
							<?php 
								echo $this->Form->input('category_id',array('type'=>'hidden','div'=>false,'label'=>false,'value'=>base64_encode(convert_uuencode($info['CourseSubcategory']['category_id']))));
								echo $this->Form->input('id',array('type'=>'hidden','div'=>false,'label'=>false,'value'=>base64_encode(convert_uuencode($info['CourseSubcategory']['id']))));
								$i=1; 
								foreach($language as $lang)
								{
									$locale = $lang['Language']['locale'];
							?>

								  <div class="tab-pane <?php if($i==1)echo 'active'; ?>" id="tabs-<?php echo $i; ?>">
                <div class="form-group">
                <label for="val-username" class="col-md-2 control-label"> Subcategory:</label>
                <div class="col-md-7">
											
				<?php echo $this->Form->input($locale,array('type'=>'text','id'=>$locale,'div'=>false,'label'=>false,'value'=>$info['CourseSubcategory'][$locale],'class'=>'form-control field text full')); ?>
            
					<span class="university-add-Error" id="err_<?php echo $locale; ?>">
						<?php if(isset($error[ $locale][0])) echo $error['title'][0]; ?>
					</span>    </div></div>
												
											
											<?php /* if($i == 1){ ?>
											
												<li>
													<label class="desc" >Current Status</label>
													<div> <?php echo $this->Form->input('status',array('type'=>'select','id'=>'course_status','div'=>false,'label'=>false,'class'=>'field select full required','options'=>array('0'=>'In-Active','1'=>'Active'),'selected'=>$status)); ?>
														<p class="university-add-Error" id="err_status">
															<?php if(isset($error['status'][0])) echo $error['status'][0]; ?>
														</p>
													</div>
												</li>

											<?php } */ ?>
											
			<div class="form-group">
                <div class="col-md-8 col-md-offset-2">								
<input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("edit_subcategory","Users/validate_edit_subcategory_ajax","newloading")' />
								
									</div>			
									</div>							
												
									</div>
						<?php 
									$i++; 
								} 
						?>
						</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
		
		
		<div class="clearfix"></div>
         