<script type="text/javascript">
//$(window).resize(function(){
//alert($(window).height());
//});

$(document).ready(function(){
	$('.hideStatusType').hide();
	$('.hideStartEndDate').hide();
	$('.hideModifiedDate').hide();
	$('.showHideCourseTier2').hide();
	
	
        
        $(".datepicker").datepicker({
                                                    format: 'dd-mm-yy',
                                                    autoclose: true
                                                    })
                                                .on('changeDate', function(e) {
                                                    DateFrom = $(".datepicker").datepicker('getFormattedDate');
                                                    $(".datepicker").datepicker('setStartDate', DateFrom);
                                                    $(this).datepicker('hide');
                                                    // `e` here contains the extra attributes
                                                });
   
	
	$('#hideTutorSearch').on('click',function(){		
		$(".search").slideToggle(1000);
		
	});
	
	
	$("#video_title").selectbox({
                onChange: function (value, inst) {
		
                if(value=='title'){
			$('.showHideCourseTier').show();
			$('.hideStatusType').hide();
			$('.hideStartEndDate').hide();
			$('.hideModifiedDate').hide();
			$('.showHideCourseTier2').hide();			
		}
		else if(value=='status'){
				$('.showHideCourseTier').hide();
				$('.hideStartEndDate').hide();
				$('.hideStatusType').show();
				$('.hideModifiedDate').hide();
				$('.showHideCourseTier2').hide();
					
			}else if(value=='published_date'){
			
				$('.hideStatusType').hide();
				$('.hideStartEndDate').show();
				$('.showHideCourseTier').hide();
				$('.hideModifiedDate').hide();
				$('.showHideCourseTier2').hide();
			}else if(value=='date_modified'){
			
				$('.hideStatusType').hide();
				$('.hideStartEndDate').hide();
				$('.hideModifiedDate').show();
				$('.showHideCourseTier').hide();
				$('.showHideCourseTier2').hide();
			}else if(value=='viewer'){
			
				$('.hideStatusType').hide();
				$('.hideStartEndDate').hide();
				$('.hideModifiedDate').hide();
				$('.showHideCourseTier').hide();
				$('.showHideCourseTier2').show();
			}
			
			
		
	}});
	$("#viewerId").on('keyup',function(e){	
		  		var countVal = $(this).val().length;
				
				var text = $(this).val();
				
				if(text.trim()=='')
				{
					
					$('#err_viewer').html(' ');
					
				}
				if(text.trim()!='')
				{
					if($.isNumeric(text)==false)
					{	
						
						$('#err_viewer').html('Please enter numeric value.');
					}
				}
				
		});
});

/*
$("#MemberFirstName").keyup(function (){
					var isTyping = $('#MemberFirstName').val();
					
							if(isTyping.trim()!=''){
								$('#First').html('');
								 
							}else{
								$('#First').html('Required');
								$('.sbm_btn_cl').prop('disabled','disabled');
							}
				}); 

*/
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Third party videos<br><small>List of videos from third party sources</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">

    
	<div class="block">

		<div class="">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
			<!--	<h2>Free Video List</h2> 
				  <a href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" id="hideTutorSearch" style="margin-top:-10px; margin-right:10px;">Click Here to Hide/Show Search option</a>
				  <a href="<?php echo HTTP_ROOT.'admin/users/add_free_video'; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add Free Video</a>-->
				<span></span>
			</div>
            <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>

                    
                 
        <div>
        <div class="row" style="margin-bottm: 5px !important;"></div>           	
       <?php echo $this->Form->create('search',array('id'=>'searchfree',"class"=>"form-inline col-sm-12")); ?> 
               
               <div class="btn-group">
               <button type="button" class="btn btn-default">Actions</button>
               <div class="btn-group">
               <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
               <span class="caret"></span>
               </button>
               <ul class="dropdown-menu">
               <li><a href="<?php echo HTTP_ROOT.'admin/users/add_free_video'; ?>" tabindex="-1">Add</a></li>
               <li><a href="javascript:void(0)" tabindex="-1" id="remove_row">Remove</a></li>
               <li><a href="javascript:void(0)" tabindex="-1" id="toggle_row">Toggle Active/Inactive</a></li>
               </ul>
               </div>
               </div>    
               
                <div class="form-group">
                    <div class="search_contents form-group">
                    <div class="btn-group">
                    <button type="button" class="btn btn-default">Filters</button>
                    </div>   
                 <?php echo $this->Form->input('type',array('type'=>'select','id'=>'video_title','div'=>false, 'style'=>'width:148px; padding:4px; border:1px solid #abadb3;' ,'label'=>false,'class'=>'field text full required','options'=>array('title'=>'Title','published_date'=>'Published Date')));  //,'date_modified'=>'Modified Date','viewer'=>'Viewers','status'=>'Current Status' ?>
                    </div>    
                 </div> 
                
               <div class="form-group showHideCourseTier">
                          <label class="desc" >&nbsp;</label>
                                <?php echo $this->Form->input('title',array('type'=>'text','id'=>'student_courses','div'=>false,'label'=>false,'class'=>'form-control field text full required col-sm-3 search_input')); ?>
                         </div>
					
                         <div class="form-group hideStartEndDate"> 
                            <!--<label class="desc" >Published Date</label>-->
                            <?php echo $this->Form->input('published_date',array('type'=>'text','id'=>'published_date','div'=>false,'label'=>false,'class'=>'datepicker form-control field text full required col-sm-3 search_input')); ?> 
                          </div>
                  
                <div class="form-group">
                  	<?php echo $this->Form->submit('Search',array('div'=>false,'label'=>false,'id'=>'searchFreeVideo','class'=>'required btn btn-sm btn-primary  txtts_inproperty')); ?>                    
                </div>  
                <?php echo $this->Form->end(); ?>                
         </div>            
        
			<div class="content-box content-box-header" id='check'>
            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/free_video_list');?>
                </div>   
           </div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div>		-->	
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<style>
.showHideCourseTier
{
	display: block;
}
.showHideCoursetText2
{
	display: none;
}
</style>