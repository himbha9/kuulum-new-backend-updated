
<script type="text/javascript">
$(document).ready(function(){
//	var germ=$('#ger').val();
//	var ara=$('#arab').val();
//	var frn=$('#frenc').val();
//	var ita=$('#ital').val();
//	var japn=$('#japan').val();
//	var kore=$('#korea').val();
//	var prt=$('#port').val();
//	var rsia=$('#rusian').val();
//	var china=$('#chin').val();
//	var spnish=$('#spain').val();
	
        $("#german").click(function(){
            
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_german'); 
             }else
             {
                 $.post('lang_status_change/off_german');
             }
                  
        });
   
        $("#arabic").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_arabic'); 
             }else
             {
                 $.post('lang_status_change/off_arabic');
             }
                  
        });
        
         $("#french").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_french'); 
             }else
             {
                 $.post('lang_status_change/off_french');
             }
                  
        });
        
        
         $("#italian").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_italian'); 
             }else
             {
                 $.post('lang_status_change/off_italian');
             }
                  
        });
        
           $("#japanese").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_japanese'); 
             }else
             {
                 $.post('lang_status_change/off_japanese');
             }
                  
        });
        
           $("#korean").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_korean'); 
             }else
             {
                 $.post('lang_status_change/off_korean');
             }
       });
        
           $("#portuguese").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_portuguese'); 
             }else
             {
                 $.post('lang_status_change/off_portuguese');
             }
       });
       
        $("#russian").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_russian'); 
             }else
             {
                 $.post('lang_status_change/off_russian');
             }
       });
       
         $("#chinese").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_chinese'); 
             }else
             {
                 $.post('lang_status_change/off_chinese');
             }
       });
       
          $("#spanish").click(function(){
             if($(this).is(":checked"))
             {
                $.post('lang_status_change/on_spanish'); 
             }else
             {
                 $.post('lang_status_change/off_spanish');
             }
       });
       
}); 
</script>

<style type="text/css">
.left {
    float: left;
    width: auto;
}
#ajax {
    float: left;
    font-weight: 700;
    padding-top: 5px;
    width: 30px;
}
.clear {
    clear: both;
}

.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border-top: 0 solid #f0f0f0;
    padding: 12px 10px;
}
.switch-primary2 input:checked + span {
    background-color: green;
}
</style> 


<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                               <?php echo __("Portal languages") ?> <br><small>Turn portal languages on and off</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>
    
<div class="loadPaginationContent">	    
<div id="content" class="content">
    <div class="block">
        <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			
			
        <div class="table-responsive text-left" style="padding:20px;" >
	
            
            
<div class="col-lg-4">
            						<div class="col-lg-8">German (Germany)</div>
							<div class="col-lg-2 text-right">
                                                           <div class="content-box content-box-header" style="border:none;"> 
                                                            <label class="css-input switch switch-sm switch-primary2">
                                                            <span></span>
                                                            <input id="german" type="checkbox"  name="login-remember-me">
                                                            <span></span>
                                                            </label>
                                                           </div>   
                                                        </div>
</div>
<div class="col-lg-4">
            						<div class="col-lg-8">Arabic</div>
							<div class="col-lg-2 text-right">
							<div class="content-box content-box-header" style="border:none;">                                                                           <label class="css-input switch switch-sm switch-primary2">
                                                                <span></span>
                                                                <input id="arabic" type="checkbox"  name="login-remember-me">
                                                                <span></span>
                                                                </label>    
								</div>
							</div>   
                                                        </div>
<div class="col-lg-4">
            						<div class="col-lg-8">French</div>
							<div class="col-lg-2 text-right">
							<div class="content-box content-box-header ui-switchbutton-ios5" style="border:none;">				
                                                                     <label class="css-input switch switch-sm switch-primary2">
                                                                    <span></span>
                                                                    <input id="french" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
								</div>
						</div>   
                                           </div>
						<div class="col-lg-4">
            						<div class="col-lg-8">Italian (Italy)</div>
							<div class="col-lg-2 text-right">
								<div class="content-box content-box-header" style="border:none;">                                                                               <label class="css-input switch switch-sm switch-primary2">
                                                                    <span></span>
                                                                    <input id="italian" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
								</div>
							</div>
						</div>
						<div class="col-lg-4">
            						<div class="col-lg-8">Japanese</div>
							<div class="col-lg-2 text-right">
								<div class="content-box content-box-header" style="border:none;">				                                     <div class="content-box content-box-header" style="border:none;">                                                                               <label class="css-input switch switch-sm switch-primary2">
                                                                    <span></span>
                                                                    <input id="japanese" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
                                                                    </div>   
								</div>
							</div>
						</div>
						<div class="col-lg-4">
            						<div class="col-lg-8">Korean</div>
							<div class="col-lg-2 text-right">
                                                     <div class="content-box content-box-header" style="border:none;">
                                                         <div class="content-box content-box-header" style="border:none;">                                                                               <label class="css-input switch switch-sm switch-primary2">
                                                                            <span></span>
                                                                    <input id="korean" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
                                                                    </div>   
								</div>
							</div>
                                                  </div>          
						<div class="col-lg-4">
            						<div class="col-lg-8">Portuguese (Portugal)</div>
							<div class="col-lg-2 text-right">
								<div class="content-box content-box-header" style="border:none;">                                                                       
                                                                    <div class="content-box content-box-header" style="border:none;">                                                                               <label class="css-input switch switch-sm switch-primary2">
                                                                            <span></span>
                                                                    <input id="portuguese" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
                                                                    </div>   
								
								</div>
							</div>
                                                     </div>       
						
							<div class="col-lg-4">
            						<div class="col-lg-8">Russian (Russia)</div>
							<div class="col-lg-2 text-right">
								<div class="content-box content-box-header" style="border:none;">
                                                                    <div class="content-box content-box-header" style="border:none;">                                                                               <label class="css-input switch switch-sm switch-primary2">
                                                                            <span></span>
                                                                    <input id="russian" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
                                                                    </div>  
                                                                </div>
							</div>
						</div>
						<div class="col-lg-4">
            						<div class="col-lg-8">Simplified Chinese</div>
							<div class="col-lg-2 text-right">
								<div class="content-box content-box-header" style="border:none;">
                                                                    <div class="content-box content-box-header" style="border:none;">                                                                               <label class="css-input switch switch-sm switch-primary2">
                                                                            <span></span>
                                                                    <input id="chinese" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
                                                                    </div>  
								</div>
							</div>
                                                       </div>     
							<div class="col-lg-4">
            						<div class="col-lg-8">Spanish</div>
							<div class="col-lg-2 text-right">
								<div class="content-box content-box-header" style="border:none;">
                                                                    <div class="content-box content-box-header" style="border:none;">                                                                               <label class="css-input switch switch-sm switch-primary2">
                                                                            <span></span>
                                                                    <input id="spanish" type="checkbox"  name="login-remember-me">
                                                                    <span></span>
                                                                    </label> 
                                                                    </div> 
								</div>
							</div>
						</div>
				
			</div>
    </div>
</div>
</div>
</div>

<script>
$(document).ready(function(){
<?php //pr($info[1]['Language']['status']);die;
	if($info[1]['Language']['status'])
	{
		$ger='on';
            echo '$("#german").prop("checked", true);' ;
	}
	else
	{
            echo '$("#german").prop("checked", false);' ;
		$ger='off';
	}
	if($info[2]['Language']['status'])
	{
		$arab='on';
                echo '$("#arabic").prop("checked", true);' ;
	}
	else
	{
		$arab='off';
                echo '$("#arabic").prop("checked", false);' ;
	}
	if($info[3]['Language']['status'])
	{
            echo '$("#french").prop("checked", true);' ;
		$frenc='on';
	}
	else
	{
                echo '$("#french").prop("checked", false);' ;
		$frenc='off';
	}
	if($info[4]['Language']['status'])
	{
                echo '$("#italian").prop("checked", true);' ;
		$ital='on';
	}
	else
	{
                
                echo '$("#italian").prop("checked", false);' ;
		$ital='off';
	}
	if($info[5]['Language']['status'])
	{
            echo '$("#japanese").prop("checked", true);' ;
		$japan='on';
	}
	else
	{
                echo '$("#japanese").prop("checked", false);' ;
		$japan='off';
	}
	if($info[6]['Language']['status'])
	{
                echo '$("#korean").prop("checked", true);' ;
		$korea='on';
	}
	else
	{
                echo '$("#korean").prop("checked", false);' ;
		$korea='off';
	}
	if($info[7]['Language']['status'])
	{
                echo '$("#portuguese").prop("checked", true);' ;
		$port='on';
	}
	else
	{
                echo '$("#portuguese").prop("checked", false);' ;
		$port='off';
	}
	if($info[8]['Language']['status'])
	{
                echo '$("#russian").prop("checked", true);' ;
		$rusian='on';
	}
	else
	{
                echo '$("#russian").prop("checked", false);' ;
		$rusian='off';
	}
	if($info[9]['Language']['status'])
	{
                echo '$("#chinese").prop("checked", true);' ;
		$chin='on';
	}
	else
	{
                echo '$("#chinese").prop("checked", false);' ;
		$chin='off';
	}
	if($info[10]['Language']['status'])
	{
                echo '$("#spanish").prop("checked", true);' ;
		$spain='on';
	}
	else
	{
                echo '$("#spanish").prop("checked", false);' ;
		$spain='off';
	}
	 ?>
             });
</script> 
<input type="hidden" id="ger" name="value_of_toggle" value="<?php echo $ger ?>" />
<input type="hidden" id="arab" name="value_of_toggle" value="<?php echo $arab ?>" />
<input type="hidden" id="frenc" name="value_of_toggle" value="<?php echo $frenc ?>" />
<input type="hidden" id="ital" name="value_of_toggle" value="<?php echo $ital ?>" />
<input type="hidden" id="japan" name="value_of_toggle" value="<?php echo $japan ?>" />
<input type="hidden" id="korea" name="value_of_toggle" value="<?php echo $korea ?>" />
<input type="hidden" id="port" name="value_of_toggle" value="<?php echo $port ?>" />
<input type="hidden" id="rusian" name="value_of_toggle" value="<?php echo $rusian ?>" />
<input type="hidden" id="chin" name="value_of_toggle" value="<?php echo $chin ?>" />
<input type="hidden" id="spain" name="value_of_toggle" value="<?php echo $spain ?>" />
<style>
.hastable tbody tr:hover td, .hastable tbody tr:hover th {

	background-color:#FFFFFF;

}
.content-box .content-box-wrapper {
    padding: 0;
	margin-left:200px;
}

</style>
