<script type="text/javascript">
$(document).ready(function(){	
	$( ".datepicker" ).datepicker({			
			dateFormat:'dd-mm-yy',
			changeMonth: true,
			changeYear: true,		
			yearRange: '1930:2030',
			inline: true
		});
	
	$('#hideTutorSearch').on('click',function(){		
		$(".search").slideToggle(1000);
		//$('#hideTutorSearch').attr('id','showTutorSearch');
	});
	
	
	$('#Student_title').on('change',function(){
		var value=$('#Student_title').val();
		if(value=='course'){
			$('.showHideCoursetText').hide();
			$('.hideStartEndDate').hide();
			$('.showHideCourseTier').show();			
		}
		else if(value=='date'){
			$('.showHideCoursetText').hide();
			$('.showHideCourseTier').hide();
			$('.hideStartEndDate').show();			
		}
		else{
			$('.hideStartEndDate').hide();
			$('.showHideCourseTier').hide();
			$('.showHideCoursetText').show();
		}
	});
});
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Lesson list <br><small>List of lessons of course</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">

    
	<div class="block">

		<div class="">
        <div class="inner-page-title">
      <?php /*?>  <a href="<?php echo HTTP_ROOT.'admin/users/add_lesson/'.$this->params->pass[0]; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add Lesson</a>
        <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> 		<span></span> </div>       
			<div class="inner-page-title"><?php */?>
				<!--<h2>Lesson List</h2> -->
             <!--    <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> -->
				<span></span>
			</div>
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			
			<div class="content-box content-box-header" id='check'>
            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/lesson_list');?>
                </div>   
                <?php /*?><ul class="sidebar-position">
                    <li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
                    <li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
                </ul><?php */?>
				
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div>	-->		
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<style>
.showHideCourseTier
{
	display: none;
}
.hideStartEndDate
{
	display: none;
}
</style>
