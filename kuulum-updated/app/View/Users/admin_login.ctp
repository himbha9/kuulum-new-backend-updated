<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>OneUI - Admin Dashboard Template & UI Framework</title>

        <meta name="description" content="OneUI - Admin Dashboard Template & UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

   
            
         <?php  echo $this->Html->script('admin_new_theme/core/jquery.min.js');?>
 
        <?php  echo $this->Html->script('admin/common.js');?>
        <?php  echo $this->Html->script('ui/ui.widget.js');?>
<?php  echo  $this->Html->script('ui/ui.tabs.js'); ?>
              <?php  echo $this->Html->script('admin_new_theme/core/bootstrap.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/core/jquery.slimscroll.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/core/jquery.scrollLock.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/core/jquery.appear.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/core/jquery.countTo.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/core/jquery.placeholder.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/core/js.cookie.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/app.js');?>
        <?php  echo $this->Html->script('admin_new_theme/plugins/jquery-validation/jquery.validate.min.js');?>
        <?php  echo $this->Html->script('admin_new_theme/pages/base_pages_login.js');?>
        
        <?php  echo $this->Html->css('admin/admin.css');?>
<?php  echo $this->Html->css('ui/ui.base.css');?>
<?php  echo $this->Html->css('ui/ui.login.css');?>
<?php  echo $this->Html->css('themes/black_rose/ui.css');?>
<?php  echo $this->Html->css('themes/black_rose/ui.css','stylesheet',array('title'=>'style'));?>

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Bootstrap and OneUI CSS framework -->
        <?php  echo $this->Html->css('admin_new_theme/bootstrap.min.css');?>
          <?php  echo $this->Html->css('admin_new_theme/oneui.css');?>

       <!--  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="assets/css/oneui.css">

        You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
         <div class="content bg-image overflow-hidden" style="background-image: url('<?php echo HTTP_ROOT; ?>img/admin_new_theme/photos/photo3@2x.jpg'); ?>');">
                    <div class="push-15">
                        <h1 class="h2 text-white animated zoomIn">Dashboard</h1>
                        <h2 class="h5 text-white-op animated zoomIn">Welcome Administrator</h2>
                    </div>
                </div>
        <!-- Login Content -->
        <div class="push-100-t">
            <div class="row block">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                    <!-- Login Block -->
                    
                    <div class="block block-themed animated fadeIn">
                      <!--  <div class="block-header bg-primary">
                            <ul class="block-options">
                                <li>
                                    
                                    <a href="base_pages_reminder.html">Forgot Password?</a>
                                </li>
                                
                            </ul>
                            <h3 class="block-title">Login</h3>
                        </div> -->
                        <div class="block-content block-content-full block-content-narrow">
                            <!-- Login Title -->
                            <div class="text-center push-30-t">
                            <?php echo $this->HTMl->image('powered_by_kuulum_black.svg'); ?> 
                            </div>
                            <h1 class="h2 font-w600 push-30-t push-5 "></h1>
                            <div class="text-center"><p>Powered by Kuulum</p></div>
                            <!-- END Login Title -->

                            <!-- Login Form -->
                            <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
                            <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                         		
					
                              <p class="adminLoginError" id="err_password"><?php if(isset($error['password'][0]))echo $error['password'][0]; ?>  </p> 	                                  
                            <?php echo $this->Form->create('Admin',array("class"=>"js-validation-login form-horizontal push-30-t push-50","id"=>"AdminLogin",'url' => array('controller'=>'Users','action'=>'login'))); ?>
                            
                         
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                           <?php echo $this->Form->input('email',array('type'=>'text','id'=>'email','class'=>'form-control','label'=>false,'div'=>false));?>
                                            <label for="email">Username</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <?php echo $this->Form->input('password',array('type'=>'password','id'=>'password','class'=>'form-control','label'=>false,'div'=>false));?>
                                            <label for="password">Password</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
					<div class="col-xs-7">
                                        <label class="css-input switch switch-sm switch-primary">
                                            <input type="checkbox" id="login-remember-me" name="login-remember-me"><span></span> Remember Me?
                                        </label>
					</div>
					<div class="col-xs-5 text-right">
                                         <a href="<?php echo HTTP_ROOT;?>admin/forgot/forgot_password">Forgot Password?</a>
					</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 text-center">
                                        
                                        <div class="col-sm-12 text-center">
                               <?php echo $this->Form->button('<i class="si si-login pull-right"></i> Log In',array('onclick'=>"return ajax_form('AdminLogin','Users/validate_admin_login','adminLoginWait');",'class'=>'btn btn-primary','label'=>false,'div'=>false))?>     <div class="adminLoginWait">
                                        	<?php echo $this->Html->image('wait.gif',array('height'=>'32px'));?>
                               </div>
                                        </div>    				
                                    </div>
                                </div>
                            <?php echo $this->Form->end();?>
                            
                       
                        <!--    </form>
                         END Login Form -->
                        </div>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
        <!-- END Login Content -->

        <!-- Login Footer -->

        <div class="push-20-t text-center animated fadeInUp">
            <small class="text-muted font-w600">&copy; 2014-2015 Localization Training LLC. All rights reserved.</small>
        </div>
        <!-- END Login Footer -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
       
  


<!--


        <script src="assets/js/core/jquery.min.js"></script>
        <script src="assets/js/core/bootstrap.min.js"></script>
        <script src="assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="assets/js/core/jquery.appear.min.js"></script>
        <script src="assets/js/core/jquery.countTo.min.js"></script>
        <script src="assets/js/core/jquery.placeholder.min.js"></script>
        <script src="assets/js/core/js.cookie.min.js"></script>
        <script src="assets/js/app.js"></script>

         Page JS Plugins 
        <script src="assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>

         Page JS Code 
        <script src="assets/js/pages/base_pages_login.js"></script>-->
    </body>
</html>
<style type="text/css">
.adminLoginError{
	color:#F00;
}
</style>
<script>
   $(document).ready(function(){
        //alert($("#password").val());
        
        $('#password').focus();
   }) 
</script>
