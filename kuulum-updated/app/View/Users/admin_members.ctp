<script type="text/javascript">
$(document).ready(function(){
	$('.hideStatusType').hide();
	/*$( ".datepicker" ).datepicker({			
				dateFormat:'dd-mm-yy',
				changeMonth: true,
				changeYear: true,		
				yearRange: '1930:2030',
				inline: true
		});
                
                */
	$(".datepicker").datepicker({
                                                    format: 'dd-mm-yy',
                                                    autoclose: true
                                                    })
                                                .on('changeDate', function(e) {
                                                    DateFrom = $("#datepicker").datepicker('getFormattedDate');
                                                    $("#datepicker1").datepicker('setStartDate', DateFrom);
                                                    $("#datepicker1").datepicker('clearDates');
                                                    $(this).datepicker('hide');
                                                    
                                                });
                                                
	$('#hideTutorSearch').on('click',function(){		
			$(".search").slideToggle(1000);
		//$('#hideTutorSearch').attr('id','showTutorSearch');
		});	
	
	$("#member_email").selectbox({
                onChange: function (value, inst) {
	
			//var value=$('#member_email').val();
			if(value=='email'){
				$('.hideStartEndDate').hide();
				$('.hideStatusType').hide();
				$('.showHideCoursetText').show();			
			}
			else if(value=='date_register'){
				$('.showHideCoursetText').hide();
				$('.hideStartEndDate').show();
				$('.hideStatusType').hide();			
			}
			else if(value=='status'){
				$('.showHideCoursetText').hide();
				$('.hideStartEndDate').hide();
				$('.hideStatusType').show();			
			}
			
    }});	
});
</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                               Registered members <br><small>List of registered members and authors</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">

    
	<div class="block">

		<div class="">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<!--<div class="inner-page-title">
				
                  <a href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" id="hideTutorSearch" style="margin-top:-10px; margin-right:10px;">Click Here to Hide/Show Search option</a> 

				<span></span>
			</div> -->
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
          
			
			
			<div class="content-box content-box-header" id='check'>
            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/member_list');?>
                </div>   
                <?php /*?><ul class="sidebar-position">
                    <li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
                    <li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
                </ul><?php */?>
				
			</div>
			<div class="clearfix"></div>
					
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<style>
.showHideCourseTier
{
	display: none;
}
.hideStartEndDate
{
	display: none;
}
</style>



