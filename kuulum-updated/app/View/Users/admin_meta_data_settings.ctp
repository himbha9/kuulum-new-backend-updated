<?php  //echo $this->Html->script('newadmin/sidebar_position.js');?>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Meta data settings <br><small>Edit the meta data settings for the portal</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">

    
	<div class="block">

		<div class="">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				
                               
				<span></span>
			</div>
			 <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			<div class="content-box content-box-header" style="border:none;">	
            	<div class="loadPaginationContent">	
					<?php echo $this->element('adminElements/admin/metaTags/meta_data_setting_list'); ?>
                </div>		
					
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
    </div>
<div class="clear"></div>
</div>
