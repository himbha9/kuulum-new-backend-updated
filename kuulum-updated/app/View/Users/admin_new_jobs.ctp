<script>
$(document).ready(function(){
	$('#Student_title').live('change',function(){
		var title=$(this).val();
		if(title=='requested_course'){
			$('#S_course').show();
			$('#S_title').hide();
		}else{
			$('#S_course').hide();
			$('#S_title').show();
		}
	});
	
	$('#hideTutorSearch').live('click',function(){		
		$(".search").slideToggle(1000);
		//$('#hideTutorSearch').attr('id','showTutorSearch');
	});
	
});
</script>
<style>
#S_course
{
	display:none;	
}
</style>
<div id="sub-nav">
	<div class="page-title">
		<h1>Open Jobs</h1>
	</div>
</div>

<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">       
			<div class="inner-page-title">
				<h2>Open Jobs Listing</h2>
                <a href="<?php echo HTTP_ROOT.'admin/users/add_job'; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add Job</a>
                <a href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" id="hideTutorSearch" style="margin-top:-10px; margin-right:10px;">Click Here to Hide/Show Search option</a>
				<span></span>
			</div>
            <div class="search">
                	<h2>Search</h2>
                    <?php echo $this->Form->create('search',array('id'=>'searchOpenJob')); ?>
                        <div class="search_contents">
                             <label class="desc" >Select Option</label>
                                <?php echo $this->Form->input('type',array('type'=>'select','id'=>'Student_title','div'=>false, 'style'=>'width:148px; padding:4px; border:1px solid #abadb3;' ,'label'=>false,'class'=>'field text full required','options'=>array('job_id'=>'Job Id','requested_course'=>'Course Name','s_id'=>'Student Id'))); ?>
                         </div> 
                         <div class="search_contents" id="S_title"> 
                            <label class="desc" >&nbsp;</label>
                            <?php echo $this->Form->input('text',array('type'=>'text','id'=>'Student_search','div'=>false, 'style'=>'width:145px; height:25px; padding:0px 0px 0px 3px; border:1px solid #abadb3;','label'=>false,'class'=>'field text full required')); ?> 
                          </div>  
                           <div class="search_contents" id="S_course"> 
                            <label class="desc" >&nbsp;</label>
                            <?php echo $this->Form->input('course',array('type'=>'select','id'=>'Student_course','div'=>false, 'style'=>'width:145px; height:27px; padding:0px 0px 0px 3px; border:1px solid #abadb3;','label'=>false,'class'=>'field text full required','options'=>array($course_list))); ?> 
                          </div>  
                        <div class="search_contents" style="margin-top:19px;">                        
                        	<?php echo $this->Form->submit('Search',array('div'=>false,'label'=>false,'id'=>'searchJob','class'=>'field text full required txtts_inproperty')); ?>                        
                      </div> 
                       <div class="adminTutorSearchWait">
							<?php echo $this->Html->image('front/wait.gif',array('height'=>'32px'));?>
                    	</div>      
                       <?php echo $this->Form->end(); ?>                
                </div>
            <?php if($this->Session->check('success')){ ?>
				<div class="success ui-corner-all successdeveloperClass" id="success">
					<span class='successMessageText'>
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
	
			<div class="content-box content-box-header" style="border:none; display:block !important;">
				<div class="loadPaginationContent">	
					<?php echo $this->element('adminElements/admin/job/job_list');?>	
                </div>			
               <?php /*?> <ul class="sidebar-position">
                    <li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
                    <li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
                </ul><?php */?>
				
			</div>
			<div class="clearfix"></div>

			<!--<div id="sidebar">

				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
