<script type="text/javascript">
	$(document).ready(function(){
		$('#roletype').val('News Manager');
		$('.role').click(function(){
		 var role_type  =$(this).val();
		 if(role_type=='4')
		 {
		 	role_type='News Manager';
		 }
		 if(role_type=='5')
		 {
		 	role_type='Membership Manager';
		 }
		 if(role_type=='6')
		 {
		 	role_type='Course Manager';
		 }
		 if(role_type=='7')
		 {
		 	role_type='Administrator';
		 }
		 if(role_type=='8')
		 {
		 	role_type='Partner';
		 }
		 $('#roletype').val(role_type);
		});
	});
</script>
<div id="sub-nav">
  <div class="page-title">
    <h1>Add Roles and Responsibilities</h1>
  </div>
</div>
<div id="page-layout">
  <div id="page-content">
    <div id="page-content-wrapper" class="no-bg-image wrapper-full">
      <div class="inner-page-title">
        <h2>Add Roles and Responsibilities</h2>
        <a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> </div>
      <?php if($this->Session->check('error')){ ?>
      <div class="response-msg error ui-corner-all"> <span>Success message</span> <?php echo $this->Session->read('error');?> </div>
      <?php $this->Session->delete('error'); ?>
      <?php } ?>
      <div class="content-box content-box-header" style="border:none;">
        <div class="column-content-box">
          <div class="content-box-wrapper"> <?php echo $this->Form->create('Admin',array('url'=>array('controller'=>'users','action'=>'assign_role'),'id'=>'assin_role')); ?>
           <fieldset>
              <ul>
                <li>
                  <label class="desc" >Permissions:</label>
                  <div class="edit_div_cont"> 
				   <?php echo $this->Form->input('role',array('type'=>'hidden','id'=>'roletype','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
				  <div class="box_container">
				  	<input type="radio"  value="4" name="data[Admin][role_id]" checked="checked" class="role"/>
					<label class="desc" >News Manager</label>
				  </div>
				  <div class="box_container">
				  	<input type="radio"  value="5" name="data[Admin][role_id]" class="role"/>
					<label class="desc" >Membership Manager</label>
				  </div>
				  <div class="box_container">
				  	<input type="radio"  value="6" name="data[Admin][role_id]" class="role"/>
					<label class="desc" >Course Manager</label>
				  </div>
				  <div class="box_container">
				  	<input type="radio"  value="7" name="data[Admin][role_id]" class="role"/>
					<label class="desc" >Administrator</label>
				  </div>
				  <div class="box_container">
				  	<input type="radio"  value="8" name="data[Admin][role_id]" class="role"/>
					<label class="desc" >Partner</label>
				  </div>
                  </div>
                </li>
				<li>
                  <span class="mandatory">*</span> <label class="desc" >E-mail</label>
                  <div> <?php echo $this->Form->input('email',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
                    <p class="university-add-Error" id="err_email">
                      <?php if(isset($error['email'][0])) echo $error['title'][0]; ?>
                    </p>
                  </div>
                </li>
				<li>
                <span class="mandatory">*</span><label class="desc" >User Name</label>
                  <div> <?php echo $this->Form->input('username',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
                    <p class="university-add-Error" id="err_username">
                      <?php if(isset($error['username'][0])) echo $error['title'][0]; ?>
                    </p>
                  </div>
                </li>
				<li>
                 <span class="mandatory">*</span><label class="desc" >Password</label>
                  <div> <?php echo $this->Form->input('password',array('type'=>'password','id'=>'title','div'=>false,'label'=>false,'class'=>'field text full required')); ?>
                    <p class="university-add-Error" id="err_password">
                      <?php if(isset($error['password'][0])) echo $error['title'][0]; ?>
                    </p>
                  </div>
                </li>
                <li>
                  <input class="sub-bttn" type="submit" value="Submit" onclick='return ajax_form("assin_role","Users/validate_assign_role_ajax","loading") '/>
                  <div class="newloading"> <?php echo $this->Html->image('wait.gif',array('height'=>'32px'));?> </div>
                </li>
              </ul>
            </fieldset>
            </form>
          </div>
        </div>      
      </div>
      <div class="clearfix"></div>
      <!--<div id="sidebar">       
      </div> -->
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
 <div class="clear"></div>
 <style>
 .edit_div_cont
 {
	 float:left;
	 width:100%;
 }
 .edit_div_cont
 {
	 float:left;
	 width:100%;
 }
 .box_container
 {
	 float:left;
	 width:100%;
	 padding: 4px;
 }
  .box_container input
 {
	 float:left;
	 width:2%;
 }
 .box_container label
 {
	 float:left;
	 width:80%;
	 clear: none;
	 padding-top:2px;
 }
 </style>