<?php  echo $this->Html->script('newadmin/sidebar_position.js');?>

<script>
	
	$(document).ready(function(){
		
			
			
			$('.search_bttn').click(function(){
					
					$('#searchDiv').toggle('slow');
			});
			
			var d = new Date();
	
			$('.datepicker').datepicker({
				
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true
				
			});
			
			$('#searchFrom').submit(function(){
				
				if($('#end').val() < $('#start').val())
				{
					alert('End Date should be grater than Start Date.');
					return false;
				}
				
			});
			
			$('#ui-datepicker-div').css('display','none');
			
	});
	
</script>

<div id="sub-nav">
	<div class="page-title">
		<h1>Newsletter List</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper">
			<div class="inner-page-title">
				<h2>Newsletter List</h2>
				
				 <a onclick="" href="<?php echo HTTP_ROOT.'admin/users/add_news_letter' ;?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add Newsletter</a>
                
				<span></span>
			</div>
			
           	 	<?php if($this->Session->check('success')){ ?>
					<div class="response-msg success ui-corner-all" id="success">
						<span>Success message</span>
						<?php echo $this->Session->read('success');?>
					</div>
					<?php $this->Session->delete('success'); ?>
				<?php } ?>
				
				<div class="response-msg success ui-corner-all" id="resp_msg" style="display:none;">
					<span>Success message</span>
					 has been deleted successfully 
				</div>
            
			<div class="content-box content-box-header" style="border:none;">			
				
				<?php echo $this->element('adminElements/admin/cmsPages/news_letter_list'); ?>
					
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/admins/cms/left_right_bar');?>
			</div> -->
			
		</div>
		
	</div>
</div>
<div class="clear"></div>