<script type="text/javascript">
$(document).ready(function(){
	$( ".datepicker" ).datepicker({			
				dateFormat:'dd-mm-yy',
				changeMonth: true,
				changeYear: true,		
				yearRange: '1930:2030',
				inline: true
		});
		
	$('#hideTutorSearch').live('click',function(){		
			$(".search").slideToggle(1000);
		//$('#hideTutorSearch').attr('id','showTutorSearch');
		});	
	
	$('#member_email').live('change',function(){
	
			var value=$('#member_email').val();
			if(value=='email'){
				$('.hideStartEndDate').hide();
				$('.showHideCoursetText').show();			
			}
			else if(value=='date_register'){
				$('.showHideCoursetText').hide();
				$('.hideStartEndDate').show();			
			}
			
		});	
});
</script>
<div id="sub-nav">
	<div class="page-title">
		<h1>User Roles</h1>
	</div>
</div>

<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
				<h2>User Roles</h2>    
				 <a href="<?php echo HTTP_ROOT.'admin/users/new_role'; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add New</a>             	<span></span>
			</div>
                 <?php /*?><a href="<?php echo HTTP_ROOT.'admin/users/add_member'; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add Member</a>
				<span></span>
			</div>
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
            <?php /*?><div class="search">
                	<h2>Search</h2>
                    <?php echo $this->Form->create('search',array('id'=>'searchAdminUser')); ?>
                         <div class="search_contents">
                             <label class="desc" >Select Option</label>
                                <?php echo $this->Form->input('type',array('type'=>'select','id'=>'member_email','div'=>false, 'style'=>'width:148px; padding:4px; border:1px solid #abadb3;' ,'label'=>false,'class'=>'field text full required','options'=>array('email'=>'E-mail Id','date_register'=>'Date Registered'))); ?>
                         </div>
                         <div class="search_contents showHideCoursetText"> 
                            <label class="desc" >&nbsp;</label>
                            <?php echo $this->Form->input('text',array('type'=>'text','id'=>'email_search','div'=>false, 'style'=>'width:145px; height:25px; padding:0px 0px 0px 3px; border:1px solid #abadb3;','label'=>false,'class'=>'field text full required')); ?> 
                          </div>  
						  <div class="search_contents hideStartEndDate"> 
                            <label class="desc" >Register Date</label>
                            <?php echo $this->Form->input('register_date',array('type'=>'text','id'=>'register_date','div'=>false, 'style'=>'width:145px; height:25px; padding:0px 0px 0px 3px; border:1px solid #abadb3;','label'=>false,'class'=>'datepicker field text full required')); ?> 
                          </div>
                        <div class="search_contents" style="margin-top:19px;">                        
                        	<?php echo $this->Form->submit('Search',array('div'=>false,'label'=>false,'id'=>'searchTutorRecord','class'=>'field text full required txtts_inproperty')); ?>                        
                      </div>  
                      <div class="adminTutorSearchWait">
						<?php echo $this->Html->image('wait.gif',array('height'=>'32px'));?>
                    </div>    
                       <?php echo $this->Form->end(); ?>                
                </div><?php */?>
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			
			<div class="content-box content-box-header" id='check'>
            	<div class="loadPaginationContent">				
					<?php echo $this->element('adminElements/admin/member/subadmin_list');?>
                </div>   
                <?php /*?><ul class="sidebar-position">
                    <li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
                    <li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
                </ul><?php */?>
				
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div>		-->	
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<style>
.showHideCourseTier
{
	display: none;
}
.hideStartEndDate
{
	display: none;
}
</style>