<style type="text/css">
.switch-primary2 input:checked + span {
    background-color: green;
}
</style> 

<?php


?>
<div id="main-container" >
    <div class="content bg-gray-lighter">
        <div class="row items-push">
           <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                               PayPal account selection<br><small>Configure and activate Paypal sandbox and live accounts.</small>
                            </h1>
                        </div>

            </div>
        </div>
    </div>

<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
			<!--	<h2>Edit Logout Time</h2> 
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> -->
			</div>
			
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			
		<!--	<div class="content-box content-box-header" style="border:none;">				
				<div class="column-content-box">
					<div class="content-box-wrapper"> -->
						<?php echo $this->Form->create('PaypalInfo',array('url'=>array('controller'=>'users','action'=>'paypal_info'),'id'=>'paypal',"class"=>"js-validation-bootstrap form-horizontal")); ?>
                <input type="hidden" name="data[PaypalInfo][id]" value="<?php echo $info['PaypalInfo']['id']; ?>">
<div class="col-md-5">
    <div class="form-group ">
        <div class="content-box content-box-header col-md-1" style="border:none;"> 
                                                            <label class="css-input switch switch-sm switch-primary2">
                                                            <span></span>
                                                            <input id="payPal_live" type="radio" value="testMode" name="data[PaypalInfo][paypal_live]"  <?php echo $info['PaypalInfo']['paypal_live']=="testMode" ? "checked" : "" ; ?> >
                                                            <span></span>
                                                            </label>
                                                           </div>  
        <h3 class="col-lg-10">PayPal sandbox account</h3> 
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3" ><span class=""></span>URL:</label>
        <div class="col-md-8">
        <?php echo $this->Form->input('sandbox_url',array('type'=>'text','value'=>$info["PaypalInfo"]["sandbox_url"],'id'=>'sandbox_url','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
            <span class="university-add-Error" id="err_sandbox_url">
		<?php if(isset($error['sandbox_url'][0])) echo $error['sandbox_url'][0]; ?>
		</span>
        </div>
    </div>
                    
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >API User name:</label>
        <div class="col-md-8">
         <?php echo $this->Form->input('sandbox_api_user',array('type'=>'text','value'=>$info["PaypalInfo"]["sandbox_api_user"],'id'=>'sandbox_api_user','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
            <span class="university-add-Error" id="err_sandbox_api_user">
		<?php if(isset($error['sandbox_api_user'][0])) echo $error['sandbox_api_user'][0]; ?>
		</span>
        </div>
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >API password:</label>
        <div class="col-md-8">
         <?php echo $this->Form->input('sandbox_api_pwd',array('type'=>'text','value'=>$info["PaypalInfo"]["sandbox_api_pwd"],'id'=>'sandbox_api_pwd','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
             <span class="university-add-Error" id="err_sandbox_api_pwd">
		<?php if(isset($error['sandbox_api_pwd'][0])) echo $error['sandbox_api_pwd'][0]; ?>
		</span>
        </div>
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >API signature:</label>
        <div class="col-md-8">
         <?php echo $this->Form->input('sandbox_api_sign',array('type'=>'text','value'=>$info["PaypalInfo"]["sandbox_api_sign"],'id'=>'sandbox_api_sign','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
            <span class="university-add-Error" id="err_sandbox_api_sign">
		<?php if(isset($error['sandbox_api_sign'][0])) echo $error['sandbox_api_sign'][0]; ?>
		</span>
        </div>
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >Test user name:</label>
         <div class="col-md-8">
         <?php echo $this->Form->input('sandbox_user',array('type'=>'text','value'=>$info["PaypalInfo"]["sandbox_user"],'id'=>'sandbox_api_sign','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
             <span class="university-add-Error" id="err_sandbox_user">
		<?php if(isset($error['sandbox_user'][0])) echo $error['sandbox_user'][0]; ?>
		</span>
        </div>
    </div>
    								
</div>

<div class="col-md-5">
    <div class="form-group ">
        <div class="content-box content-box-header col-md-1" style="border:none;"> 
                                                            <label class="css-input switch switch-sm switch-primary2">
                                                            <span></span>
                                                            <input id="payPal_live" type="radio" value="liveMode"  name="data[PaypalInfo][paypal_live]" <?php echo $info['PaypalInfo']['paypal_live']=="liveMode" ? "checked" : "" ; ?>>
                                                            <span></span>
                                                            </label>
                                                           </div>  
        <h3 class="col-lg-10">PayPal live account</h3> 
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3 " ><span class=""></span>URL:</label>
        <div class="col-md-8">
         <?php echo $this->Form->input('live_url',array('type'=>'text','value'=>$info['PaypalInfo']['live_url'],'id'=>'live_url','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
            <span class="university-add-Error" id="err_live_url">
		<?php if(isset($error['live_url'][0])) echo $error['live_url'][0]; ?>
		</span>
        </div>
    </div>
                    
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >API User name:</label>
       <div class="col-md-8">
         <?php echo $this->Form->input('live_api_user',array('type'=>'text','value'=>$info['PaypalInfo']['live_api_user'],'id'=>'live_api_user','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
            <span class="university-add-Error" id="err_live_api_user">
		<?php if(isset($error['live_api_user'][0])) echo $error['live_api_user'][0]; ?>
		</span>
        </div>
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >API password:</label>
        <div class="col-md-8">
         <?php echo $this->Form->input('live_api_pwd',array('type'=>'text','value'=>$info['PaypalInfo']['live_api_pwd'],'id'=>'live_api_pwd','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
            <span class="university-add-Error" id="err_live_api_pwd">
		<?php if(isset($error['live_api_pwd'][0])) echo $error['live_api_pwd'][0]; ?>
		</span>
        </div>
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >API signature:</label>
        <div class="col-md-8">
         <?php echo $this->Form->input('live_api_sign',array('type'=>'text','value'=>$info['PaypalInfo']['live_api_sign'],'id'=>'live_api_sign','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
              <span class="university-add-Error" id="err_live_api_sign">
		<?php if(isset($error['live_api_sign'][0])) echo $error['live_api_sign'][0]; ?>
		</span>  
        </div>
    </div>
    <div class="form-group">
        <label for="val-username" class="col-md-3 " >Test user name:</label>
        <div class="col-md-8">
         <?php echo $this->Form->input('live_user',array('type'=>'text','value'=>$info['PaypalInfo']['live_user'],'id'=>'live_user','div'=>false,'label'=>false,'class'=>'form-control field text full required','maxlength'=>'100')); ?>
             <span class="university-add-Error" id="err_live_user">
		<?php if(isset($error['live_user'][0])) echo $error['live_user'][0]; ?>
		</span>  
        </div>
    </div>
    								
</div>
                <div class="col-md-10 text-left">
                              
  <div class="form-group">
        <div class="col-md-6">
        <input class="btn btn-primary col-md-2" type="submit" value="Submit" onclick='return ajax_form("paypal","Users/validate_paypal_ajax","loading")'>
        </div>
    </div> 
                    </div>             
                
						<?php echo $this->Form->end(); ?>
					</div>
				</div>      
			</div>
<div>
			<div class="clearfix"></div>
			<div class="clear"></div>
		<div class="clear"></div>

<div class="clear"></div>