<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                              Cancellation reasons <br><small>List of reasons displayed on the cancel account screen</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>

<div id="content" class="content">

    
	<div class="block">

		<div class="">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<!--<div class="inner-page-title">
				<h2>Reasons</h2>
				<a href="<?php echo HTTP_ROOT.'admin/users/add_reason'; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add Reason</a>
				<span></span>
			</div>-->
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
						<?php echo $this->Session->read('success');?>
					</span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			<div class="content-box content-box-header" style="border:none;">	
            	<div class="loadPaginationContent">	
					<?php echo $this->element('adminElements/admin/manage/reasons_list'); ?>
                </div>		
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>