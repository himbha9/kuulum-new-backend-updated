<!--<?php  echo $this->Html->script('newadmin/sidebar_position.js');?> -->
<script>
$(document).ready(function(){
	
	$('.delete_review').click(function(){
		var review_id = $(this).attr('rel');
			
		if(confirm('Are you sure you want to delete this review?'))
		{
		
			req = $.ajax({
			url:ajax_url+'users/delete_review/',
			type: 'get',
			data: {'review_id': review_id},
			success: function(resp){
				if(resp == 'deleted')
				{
					window.location=ajax_url+'admin/users/reports_on_reviews';	
				}
				else
				{
					alert('Something went wrong! Please try later.');
				}
			}
		});
		}
		
	});

});

</script>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Conflict resolution <br><small>Approve or reject review removal request by author</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>
<div id="content" class="content">

	<div class="block">

		<div class="">

			<div class="inner-page-title">
				<!--<h2>Report list</h2> -->
                
				<span></span>
			</div>
			 <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			<div class="content-box content-box-header" style="border:none;">	
            	<div class="loadPaginationContent">	
					<?php echo $this->element('adminElements/admin/member/report_list'); ?>
                </div>		
					<?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul>
					<?php */?>
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
