
<div id="sub-nav">
	<div class="page-title">
		<h1>Second Level Sub-Categories of <?php echo $sub_category['CourseSubcategory']['en'].' Sub Category' ?></h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2>Sub-Category List</h2>
					<a href="<?php echo HTTP_ROOT.'admin/users/course_subcategory/'.base64_encode(convert_uuencode($sub_category['CourseSubcategory']['category_id'])); ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a>
					<a href="<?php echo HTTP_ROOT.'admin/users/add_sl_subcategory/'.base64_encode(convert_uuencode($sub_category['CourseSubcategory']['category_id'])).'/'.$sub_cat_id; ?>" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Add SL Sub-Category</a>
				<span></span>
			</div>
			 <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			<div class="content-box content-box-header" style="border:none;">	
            	<div class="loadPaginationContent">	
					<?php echo $this->element('adminElements/admin/manage/sl_sub_category_list'); ?>
                </div>							
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div>-->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>