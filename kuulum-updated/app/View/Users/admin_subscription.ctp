<?php echo $this->Html->script('admin/jquery.iphone-switch.js');?>

<!--<script language="javascript" type="text/javascript" src="jquery.iphone-switch.js"></script>-->
<script type="text/javascript">
$(document).ready(function(){
	var my_value=$('#value_of_toggle').val();
    $('#1').iphoneSwitch(my_value, 
		function() {
			$('#ajax').load('subs_status/on');
		},
		function() {
			$('#ajax').load('subs_status/off');
		},
		{
			switch_on_container_path:ajax_url+'img/admin/iphone_switch_container_off.png'
		},
		{
			switch_path:ajax_url+'img/admin/iphone_switch.png'
		}
	);
}); 
</script>

<style type="text/css">
.left {
    float: left;
    width: auto;
}
#ajax {
    float: left;
    font-weight: 700;
    padding-top: 5px;
    width: 30px;
}
.clear {
    clear: both;
}
</style> 
	
<div id="sub-nav">
	<div class="page-title">
		<h1>Subscription Functionality on Portal</h1>
	</div>
</div>

<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2>Subscription Functionality on Portal</h2>
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span>
			</div>
			
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			
			<div class="content-box content-box-header" style="border:none;">				
				<div class="column-content-box">
					<div class="content-box-wrapper">
					
					
					
						<div class="left" id="1"></div>
						<div id="ajax"></div>
						<div class="clear"></div>
					
					
					
					</div>
				</div>      
			</div>
			<div class="clearfix"></div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>

<input type="hidden" id="value_of_toggle" name="value_of_toggle" value="<?php echo $value ?>" />