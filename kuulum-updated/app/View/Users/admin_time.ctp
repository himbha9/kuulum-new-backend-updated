
<script type="text/javascript">
		
		$(document).ready(function(){
				
				$('#logoutForm').validate();
				
		});
		
</script>
	
<div id="main-container" >
    <div class="content bg-gray-lighter">
        <div class="row items-push">
           <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                               Portal logout time<br><small>Enter the member logout time in minutes</small>
                            </h1>
                        </div>

            </div>
        </div>
    </div>

<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<div class="inner-page-title">
			<!--	<h2>Edit Logout Time</h2> 
				<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> -->
			</div>
			
			<?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all" id="success">
					<span >
					   <?php echo $this->Session->read('success');?>
                    </span>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
			
		<!--	<div class="content-box content-box-header" style="border:none;">				
				<div class="column-content-box">
					<div class="content-box-wrapper"> -->
						<?php echo $this->Form->create('LogoutTime',array('url'=>array('controller'=>'users','action'=>'time'),'id'=>'logoutForm',"class"=>"js-validation-bootstrap form-horizontal")); ?>
		
							<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Logout Time in Minutes:</label>
                                       <div class="col-md-2">	
									
										<?php echo $this->Form->input('minutes',array('type'=>'text','value'=>$info['LogoutTime']['minutes'],'id'=>'minutes','div'=>false,'label'=>false,'class'=>'form-control field text full required digits','maxlength'=>'3')); ?>													
									</div>
								</div>

									<div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
								
									<input class="btn  btn-primary" type="submit" value="Submit"/>
									</div></div>								
					
						<?php echo $this->Form->end(); ?>
					</div>
				</div>      
			</div>
<div>
			<div class="clearfix"></div>
			<div class="clear"></div>
		<div class="clear"></div>

<div class="clear"></div>