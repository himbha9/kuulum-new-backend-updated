<script type="text/javascript">
$(document).ready(function(){
			
			/*$("#title").on('keyup',function(e){	
		  		var countVal = $('#title').val().length;
				
				
				if(countVal >10)
				{
					$(this).next('p').next('p').show();
					$('#title').attr('maxlength','10');
						
				}
				
				
					
	});*/
	
	$("#title").bind('paste',function(e){					
				setTimeout(function(e) {  
					var countVal = $("#title").val().length;
					
					var text = $("#title").val();
					
					var getval = $("#showCount").text();
					var total = 249 - countVal;
					if(total >= 0){
						$('#showCount').text(total);
						
					}else{				
						$("#showCount1").show();
						$("#title").val($("#title").val().substring(0, 250));
						
					}
				
				}, 15)
			});
			$("#title").bind('cut',function(e){					
				e.preventDefault();
			});
			$("#title").on('keyup',function(e){	
		  		var countVal = $(this).val().length;
				
				var text = $(this).val();
				
				var getval = $("#showCount").text();
				var total = 249 - countVal;
				if(total >= 0){
		  			$('#showCount').text(total);
				}else{					
					 $("#showCount1").show();
					 $(this).val($(this).val().substring(0, 250));
				}
		});
		$("#title").on('focusout',function(e){			
			 $(this).val($(this).val().substring(0, 250));
			 var countVal = $(this).val().length;			
			 var total = 250 - countVal;
			 $('#showCount').text(total);
		});
	
	
	
	
	
	});

</script>

<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                           View Ccourse details<br><small>Display details of selected course</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full"> 
<div class="row text-right">       
				<!--<h2>Edit Course</h2> -->  <button class="btn  btn-primary" type="button" onclick="document.location.href='<?php echo HTTP_ROOT.'admin/users/lessons/'.$course_id; ?>'">View Lessons</button>  
</div>
				<!--<a onclick="history.go(-1);" href="javascript:void(0);" class="ui-state-default ui-corner-all float-right ui-button" style="margin-top:-10px;">Back</a> <span></span> -->
			
					
						<?php echo $this->Form->create('Course',array('url'=>array('controller'=>'users','action'=>'edit_course'),'id'=>'edit_course',"class"=>"js-validation-bootstrap form-horizontal")); ?>
							
							<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$info['Course']['id'])); ?> 
							<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Course title:</label>
                                       <div class="col-md-6">	
										 <?php echo $this->Form->input('title',array('type'=>'text','id'=>'title','maxlength'=>250,'div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$info['Course']['title'],"readonly"=>true)); ?>
	<!--<p style="float:left;margin-right: 5px;"> <?php echo __('Characters remaining '); ?> </p><p id="showCount">250</p>
	<p style="display:none" id="showCount1" ><?php echo __('A maximum of 250 characters are allowed');?></p>
        <p class="university-add-Error" id="err_title">
												<?php if(isset($error['title'][0])) echo $error['title'][0]; ?>
											</p> -->
										</div></div>
									
					<?php $id=base64_encode(convert_uuencode($info['Course']['id']));?>
									
									<div class="form-group">
                                       <label for="val-username" class="col-md-2" >Course Description:</label>
                                       <div class="col-md-6">
				 <?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$info['Course']['id'])); ?> <?php echo $this->Form->input('description',array('type'=>'textarea','id'=>'discription','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$info['Course']['description'],"readonly"=>true)); ?>
	<!--<p class="university-add-Error" id="err_description">
							<?php if(isset($error['description'][0])) echo $error['description'][0]; ?>
											</p> -->
										</div></div>
									
		<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Author:</label>
                                       <div class="col-md-6">	
										 <?php echo $this->Form->input('author',array('type'=>'text','id'=>'title','maxlength'=>250,'div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$memberInfo['Member']['final_name'],"readonly"=>true)); ?>
	
										</div></div>
<div class="form-group">
                                       <label for="val-username" class="col-md-2 " >Date Added:</label>
                                       <div class="col-md-6">	
<?php echo $this->Form->input('date_added',array('type'=>'text','id'=>'title','maxlength'=>250,'div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>date('d/m/Y',$info['Course']['date_added']),"readonly"=>true)); ?>
	
										</div></div>								
                                                                       
						
									<div class="form-group">
                                       <div class="col-md-8 col-md-offset-2">
 <button class="btn  btn-primary" type="button" onclick="history.go(-1);">Back</button>   
<button class="btn  btn-primary" type="button" onclick="document.location.href='<?php echo HTTP_ROOT.'admin/users/edit_course/'.$id; ?>'">Edit</button>
<!-- 
<input class="btn btn-sm btn-primary" type="submit" value="Submit" onclick='return ajax_form("edit_course","Users/validate_cours_ajax","newloading") '/> -->
										
</div>
                                        </div>			
									
																			
						<?php echo $this->Form->end(); ?>
					</div>	
				</div>
			</div>			
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>

	