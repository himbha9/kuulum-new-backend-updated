<?php echo $this->Html->script('/jwplayer/jwplayer');?>
<style>
	.hastable tbody th
	{
		padding:10px;
	}
	.hastable tr td
	{
		text-align:left;
	}
	#imageedit
	{
		float:left;
		margin-top:0px !important ; 
	}	
</style>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                           View Course Details<br><small>Display details of selected course.</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			<!--<div class="inner-page-title">
				<!--<h2 style="width:90%; ">Course Information</h2> -->
				<a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
				<span></span>
			</div> -->
			<?php echo $this->Form->create('Course',array('url'=>array('controller'=>'users','action'=>'view_course'),'id'=>'add_member','enctype'=>'multipart/form-data',"class"=>"js-validation-bootstrap form-horizontal")); ?>
			<div class="form-group">
                                       <label for="val-username" class="col-md-2 " ><span class="">*  </span>Course title:</label>
                                       <div class="col-md-7">	
                        <?php echo $this->Form->input('title',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'form-control field text full required',"value"=> $info['Course']['title'],"readonly"=>true)); ?>
			<p class="university-add-Error" id="err_final_name">
		<?php if(isset($error['final_name'][0])) echo $error['final_name'][0]; ?>
		</p>
</div>		</div>	
								
										
            <div class="form-group">
                                  <label for="val-username" class="col-md-2"><span class="">*  </span>Course Description:</label>                                            <div class="col-md-7">
                                            <?php echo $this->Form->input('Description',array('type'=>'textarea','id'=>'description','div'=>false,'label'=>false,'class'=>'form-control field text full required',"value"=> $info['Course']['description'],"readonly"=>true)); ?>									<p class="university-add-Error" id="err_email">
<?php if(isset($error['email'][0])) echo $error['email'][0]; ?>
</div>
</div>
                    
                    
			<div class="content-box content-box-header" style="border:none;">
			<div class="hastable">			
                   			<table id="sort-table"> 
                            <tbody>
								<tr>							
                                    <th width="10%;">Author</th> 
                                    <td width="30%;"><?php echo $memberInfo['Member']['final_name'] ?></td> 
                                </tr>
                            	<tr>							
                                    <th width="10%;">Title</th> 
                                    <td width="30%;"><?php echo $info['Course']['title'] ?></td> 
                                </tr>
								<tr>							
                                    <th width="10%;">Description</th> 
                                    <td width="30%;"><?php echo $info['Course']['description'] ?></td> 
                                </tr>
                                 <tr>
                                    <th>Primary language</th>
                                    <td>
									<?php echo $info['Language']['language'] ?>
									</td>                                
                                  </tr> 
								  
                            	<tr>							
                                    <th width="10%;">Date added</th> 

                                    <td width="30%;"><?php echo  date('d M Y, h:i e',$info['Course']['date_added']); ?> </td> 

                                </tr> 
								<tr>							
                                    <th width="10%;">Status</th> 
                                    <td width="30%;"><?php if($info['Course']['status']==0){echo "Inactive";}else{echo "Active";} ;?></td> 
                                </tr>
								
                            </tbody>
						</table>
					<div class="clear"></div>
				</div>
					
					<?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
					
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div> 