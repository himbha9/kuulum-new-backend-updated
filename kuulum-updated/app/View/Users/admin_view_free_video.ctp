<?php echo $this->Html->script('/jwplayer/jwplayer');?>
<style>
	.hastable tbody th
	{
		padding:10px;
	}
	.hastable tr td
	{
		text-align:left;
	}
	#imageedit
	{
		float:left;
		margin-top:0px !important ; 
	}	
</style>
<div id="sub-nav">
	<div class="page-title">
		<h1>View Free Video Details</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2 style="width:90%; ">Free Video Information</h2>
				<a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
				<span></span>
			</div>
			
			<div class="content-box content-box-header" style="border:none;">
			<div class="hastable">			
                   			<table id="sort-table"> 
                            <tbody>
								<tr>							
                                    <th width="10%;">Title</th> 
                                    <td width="30%;"><?php echo $info['FreeVideo']['title_en'] ?></td> 
                                </tr>
								<?php if(!empty($info['FreeVideo']['free_video_url']))
								{ ?>
									<tr>							
										<th width="10%;">Free Video URL</th> 
										<td width="30%;"><?php echo $info['FreeVideo']['free_video_url'] ?></td> 
									</tr>
								<?php }else
								{ ?>
									<tr>							
										<th width="10%;">Video File</th> 
										<td width="30%;"><?php echo $info['FreeVideo']['own_video_url'] ?></td> 
									</tr>
								<?php } ?>
								<tr>							
                                    <th width="10%;">Total Viewer</th> 
                                    <td width="30%;"><?php echo $info['FreeVideo']['viewer'] ?></td> 
                                </tr>
								<?php if(!empty($info['FreeVideo']['mins']) || !empty($info['FreeVideo']['hours']) || !empty($info['FreeVideo']['secs'])) { ?>
								<tr>							
                                    <th width="10%;">Duration</th> 
                                    <td width="30%;">
									<?php 
									if($info['FreeVideo']['mins'] > 0 || $info['FreeVideo']['secs'] > 0) { ?>                                           
                                                    <?php echo  ($info['FreeVideo']['mins'] != NULL && $info['FreeVideo']['mins'] > 0)?$info['FreeVideo']['mins']." ".__('minutes'):' ';?>
                                                    <?php echo  ($info['FreeVideo']['secs'] != NULL && $info['FreeVideo']['secs'] > 0)?$info['FreeVideo']['secs']." ".__('seconds'):'00 '.__('seconds');?>
                                                <?php } ?>
									
									</td> 
                                </tr>
							<?php } ?>
								<tr>							
                                    <th width="10%;">Published Date</th> 
                                     <td width="30%;"><?php echo  date('d M Y, h:i e',$info['FreeVideo']['date_added']); ?> </td> 
                                </tr>
                                 <tr>							
                                    <th width="10%;">Modified Date</th> 

                                    <td width="30%;"><?php echo  date('d M Y, h:i e',$info['FreeVideo']['date_modified']); ?> </td> 

                                </tr> 
								<tr>							
                                    <th width="10%;">Status</th> 
                                    <td width="30%;"><?php if($info['FreeVideo']['status']==0){echo "Inactive";}else{echo "Active";} ;?></td> 
                                </tr>
								<tr>							
                                    <th width="10%;">Description</th> 
                                    <td width="30%;"><?php echo $info['FreeVideo']['description_en'] ?></td> 
                                </tr>
								
                            </tbody>
						</table>
					<div class="clear"></div>
				</div>
					
					<?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
					
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div> 