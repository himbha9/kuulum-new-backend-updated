
<style>
	.hastable tbody th
	{
		padding:10px;
	}
	.hastable tr td
	{
		text-align:left;
	}	
</style>


<div id="sub-nav">
	<div class="page-title">
		<h1>View Job Details</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2 style="width:90%; ">Job Information</h2>
				<a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
				<span></span>
			</div>
			
			<div class="content-box content-box-header" style="border:none;">
			<div class="hastable">
            						
                   			<table id="sort-table"> 
                            <tbody>
                            	<tr>							
                                    <th width="10%;">Job Id</th> 
                                    <td width="30%;"><?php echo $job_request['JobRequest']['job_id'];?></td> 
                                </tr>
                            	<tr>							
                                    <th width="10%;">Student Id</th> 
                                    <td width="30%;"><?php echo $job_request['JobRequest']['s_id']; ?></td> 
                                </tr>  
                                <tr>							
                                    <th width="10%;">Name</th> 
                                    <td width="30%;"><?php echo $job_request['Student']['title']." ".$job_request['Student']['fore_name']." ".$job_request['Student']['sur_name'];?></td> 
                                </tr>   
                                <tr> 
                                    <th>E-mail (Primary)</th> 
                                    <td><?php echo $job_request['Student']['email']; ?></td> 
                                </tr>            
                                <tr>                
                                    <th>Other E-mail(s)</th> 
                                    <td> 
										<?php  
                                            if(count($student_email)>0){
                                            	foreach($student_email as $student_email) { 
                                                         $email[]=$student_email['StudentEmail']['email']; 
                                            	}
												echo $email=implode(',',$email);
                                             }else{ 
											 		echo "No Record Found";
											}
                                        ?>
                                    </td> 
                                 </tr> 
                                 <tr>                
                                    <th>Mobile(Primary)</th> 
                                    <td> <?php 
									
									echo $job_request['Student']['mobile'];?></td> 
                                 </tr> 
                                 <tr>                
                                    <th>Other Mobile(s)</th> 
                                    <td> 
										<?php  
                                            if(count($student_mobile)>0){
                                            	foreach($student_mobile as $student_mobile) { 
                                                         $mobiles[]=$student_mobile['StudentMobile']['mobile']; 
                                            	}
												echo $mobiles=implode(',',$mobiles);
                                             }else{ 
											 		echo "No Record Found";
											 }
                                        ?>
                                    </td> 
                                 </tr>     
								 <tr>
                                    <th>Current Job Status</th>
                                    <td>
									<?php 
                                          if($job_request['JobRequest']['status']==0)
                                          {
                                              echo "Disable";
                                          }
                                          elseif($job_request['JobRequest']['status']==1)
                                          {
                                              echo "Active";
                                          }
                                        ?></td>                                
                                  </tr>
                                  <tr>
                                    <th>Student University</th>
                                    <td><?php  echo $job_request['University']['name']; ?></td>                                 
                                 </tr>
                                 <tr>
                                    <th>Title of Degree</th>
                                    <td><?php echo $degreeName['Degree']['title']; ?></td>                                 
                                 </tr>
                                  <tr>
                                    <th>Requested Year</th>
                                    <td>
										<?php 
											if($job_request['JobRequest']['requested_year']==1){
												echo 'First Year';
											}
											elseif($job_request['JobRequest']['requested_year']==2){
												echo 'Second Year';
											}elseif($job_request['JobRequest']['requested_year']==3){
												echo 'Third Year';
											}
									
										?>
                                      </td>                                 
                                 </tr>
								 <tr>
                                    <th>Requested Course(s) </th>
                                    <td><?php  echo $job_request['UniversityCourse']['course_name'];?></td>                                 
                                 </tr>
                                  <tr>
                                    <th>Required Level of Expertise </th>
                                     <td>
										<?php 
											if($job_request['JobRequest']['expertise_level']==1){
												echo 'Gold';
											}
											elseif($job_request['JobRequest']['expertise_level']==2){
												echo 'Silver';
											}elseif($job_request['JobRequest']['expertise_level']==3){
												echo 'Premium';
											}elseif($job_request['JobRequest']['expertise_level']==4){
												echo 'Peer';
											}
									
										?>
                                      </td>                                        
                                 </tr>
                                  <tr>
                                    <th>Job Hour Rate </th>
                                    <td><?php 
									echo '(£'.$job_request['JobRequest']['job_hour_rate'].'/hr)';
									?></td>                                 
                                 </tr>
                                 <tr>
                                    <th>Start Date</th>
                                     <td><?php echo $job_request['JobRequest']['start_date']; ?></td>                              
                                 </tr>
                                 <tr>
                                    <th>End Date</th>
                                     <td><?php echo $job_request['JobRequest']['end_date']; ?></td>                               
                                 </tr>
								 <tr>
                                    <th>Frequency</th>
                                    <td>
										<?php 
											if($job_request['JobRequest']['frequency']==1){
												echo 'Once a week';
											}
											elseif($job_request['JobRequest']['frequency']==2){
												echo 'Twice a week';
											}elseif($job_request['JobRequest']['frequency']==3){
												echo '3 times a week';
											}elseif($job_request['JobRequest']['frequency']==5){
												echo '5 times a week';
											}
									
										?>
                                      </td>                                 
                                 </tr>
                                  <tr>
                                    <th> Duration of Session</th>
                                     <td><?php echo $job_request['JobRequest']['duration'].' hours'; ?></td>                              
                                 </tr>
								  <tr>
                                    <th>Available Time</th>
                                    <td><?php echo $job_request['JobRequest']['available_time'].' '.$job_request['JobRequest']['time_format']; ?></td>                                 
                                 </tr>
								 
                                 
                                 <?php if($job_request['JobRequest']['location']!=1) { ?>
                                 <tr>
                                    <th>Available Location Postcode</th>
                                    <td><?php echo $job_request['JobRequest']['postcode']; ?></td>                                 
                                 </tr>
                                 <?php } else{ ?>
								 <tr>
                                    <th>Available Location</th>
                                    <td><?php echo $job_request['University']['name'].' ( '.$job_request['University']['postcode'].' )'; ?></td>                                 
                                 </tr>
                                 <?php } ?>
                                 <tr>
                                    <th>Interested in Tuition by correspondence?</th>
                                    <td>
										<?php 
											if($job_request['JobRequest']['interested_in_tuition']==1){
												echo 'No, face-to-face only';
											}
											elseif($job_request['JobRequest']['interested_in_tuition']==2){
												echo 'Yes, i am interested in correspondence only';
											}elseif($job_request['JobRequest']['interested_in_tuition']==3){
												echo 'Yes, correspondence preferred to face-to-face';
											}elseif($job_request['JobRequest']['interested_in_tuition']==4){
												echo 'Yes, but face-to-face preferred to correspondence';
											}
									
										?>
                                      </td>                                 
                                 </tr>
                                  <tr>
                                    <th>Other Requirements</th>
                                    <td><?php  echo $job_request['JobRequest']['condition']; ?></td>                                 
                                 </tr>
                                 <tr>
                                    <th>Estimated Total Hours</th>
                                    <td><?php  echo $job_request['JobRequest']['total_hour'];?></td>                                 
                                 </tr>
							</tbody>
						</table>                        	                        
					
                    <div class="inserted_toursmain">
						<?php echo $this->Html->link('Interested Tutors for this Job',HTTP_ROOT."admin/Users/interested_tutors/".$job_request['JobRequest']['id']);?>
                          <?php if($job_request['JobRequest']['payment_status']==1){ ?>
                             <?php echo $this->Html->link('Assign job to tutor',HTTP_ROOT."admin/Users/assign_job_to_tutor_list/". base64_encode(convert_uuencode($job_request['JobRequest']['id'])));?>
                         <?php } ?>
                    </div>
                    
                    
                    
					<div class="clear"></div>
				</div>
					<?php /*?>
					<ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul>
					<?php */?>
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div> 