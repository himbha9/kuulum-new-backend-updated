<?php echo $this->Html->script('/jwplayer/jwplayer');?>
<style>
	.hastable tbody th
	{
		padding:10px;
	}
	.hastable tr td
	{
		text-align:left;
	}
	#imageedit
	{
		float:left;
		margin-top:0px !important ; 
	}	
</style>
<div id="sub-nav">
	<div class="page-title">
		<h1>View Lesson Details</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2 style="width:90%; ">Lesson Information</h2>
				<a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
				<span></span>
			</div>
			<div class="content-box content-box-header" style="border:none;">
			<div class="hastable">			
                   			<table id="sort-table"> 
                            <tbody>
                            	<tr>							
                                    <th width="10%;">Title</th> 
                                    <td width="30%;"><?php echo $info['CourseLesson']['title'] ?></td> 
                                </tr>
								<tr>							
                                    <th width="10%;">Description</th> 
                                    <td width="30%;"><?php echo $info['CourseLesson']['description'] ?></td> 
                                </tr>
                                 <tr>
                                    <th>Video</th>
                                    <td>
                                    <?php
									$vediofile='files/members/'.convert_uudecode(base64_decode($this->params['pass'][1])).'/'.$info['CourseLesson']['video'];
									if(file_exists($vediofile)){ ?>
									 <div id="mediaplayer" style="margin-bottom:10px;">Loading the player..</div>
										 <script type="text/javascript">
											jwplayer("mediaplayer").setup({
												file: "<?php echo HTTP_ROOT;?>files/members/<?php echo convert_uudecode(base64_decode($this->params['pass'][1]));?>/<?php echo $info['CourseLesson']['video'];?>",
												height: "200",
												width: "200",
												image: ""
											});
										</script> 
										</div>
                                     <?php  }
									 else { ?>
                                     No Vedio File
                                     <?php } ?>
									</td>                                
                                  </tr> 
                            	<tr>							
                                    <th width="10%;">Date added</th> 
                                    <td width="30%;"><?php echo date('d M Y, h:i e',$info['CourseLesson']['date_added']); ?> </td> 
                                </tr> 
								<tr>							
                                    <th width="10%;">Status</th> 
                                    <td width="30%;"><?php if($info['CourseLesson']['status']==0){echo "Inactive";}else{echo "Active";} ;?></td> 
                                </tr>
                            </tbody>
						</table>
					<div class="clear"></div>
				</div>
					<?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
			</div>
			<div class="clearfix"></div>
		<!--	<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div> 