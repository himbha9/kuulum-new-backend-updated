
<style>
	.hastable tbody th
	{
		padding:10px;
	}
	.hastable tr td
	{
		text-align:left;
	}
	#imageedit
	{
		float:left;
		margin-top:0px !important ; 
	}	
</style>


<div id="sub-nav">
	<div class="page-title">
		<h1>View News Details</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2 style="width:90%; ">News Information</h2>
				<a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
				<span></span>
			</div>
			
			<div class="content-box content-box-header" style="border:none;">
			<div class="hastable">			
                   			<table id="sort-table"> 
                            <tbody>
                            	<tr>							
                                    <th width="10%;">News Title</th> 
                                    <td width="30%;"><?php echo $info['News']['title_en'] ?></td> 
                                </tr>
								<tr>							
                                    <th width="10%;">Sub Title</th> 
                                    <td width="30%;"><?php echo $info['News']['sub_title_en'] ?></td> 
                                </tr>
                                 <tr>
                                    <th>Image</th>
                                    <td>
									<?php 
									
									 if($info['News']['image'] && $this->Utility->getAWSDisplayUrl(NEWS_BUCKET,'thumbnails/'.$info['News']['image'])) 
                        {
                             echo $this->Html->image($this->Utility->getAWSImgUrl(NEWS_BUCKET,'thumbnails/'.$info['News']['image']),array('width'=>160,'height'=>75,'id'=>'imageedit')); 
                        }
									
									
									 else
									 {
									 	echo $this->Html->image('front/no_img_small.png',array('width'=>160,'height'=>75,'id'=>'imageedit'));
									 }
									 ?></td>                                
                                  </tr> 
								  <tr>
                                    <th>Description</th>
                                    <td>
									<?php echo $info['News']['description_en'];?></td>                                
                                  </tr>
                            	<tr>							
                                    <th width="10%;">Date modified</th> 
                                    <td width="30%;"><?php echo date('d M Y, h:i e',$info['News']['date_modified']); ?> </td> 
                                </tr> 
								<tr>							
                                    <th width="10%;">Status</th> 
                                    <td width="30%;"><?php if($info['News']['status']==0){echo "Inactive";}else{echo "Active";} ;?></td> 
                                </tr>
								
                            </tbody>
						</table>
					<div class="clear"></div>
				</div>
					
					<?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
					
			</div>
			<div class="clearfix"></div>
		<!--	<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div> 
