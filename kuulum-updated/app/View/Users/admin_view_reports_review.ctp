<!--<?php echo $this->Html->css('uploadify');?>
<?php echo $this->Html->script('jquery.uploadify');?>
<?php echo $this->Html->script('/jwplayer/jwplayer');?> -->


<!--<script type="text/javascript">
		
	$(document).ready(function()
	{
		/*$('#tabs, #tabs2, #tabs5').tabs();

		$('#add_category').validate();
		
		$('.lang_tab').click(function(){ 
			
			$lang = $(this).attr('title');
			
			$('#lang_locale').val($lang);
		}); */
		$('.ownvideo').hide();
		
		$('#free').change(function()
		{
			free = $('#free').val();
			
			if(free=='own_video')
			{
				
				$('.freevideo').hide();
				$('.ownvideo').show();
				
			}else
			{
				$('.freevideo').show();
				$('.ownvideo').hide();
				
			}
		});
		$('#image').bind('change',function(){
		
			var imagePath = $('#image').val();//document.FormTwo.picFile.value;
			var pathLength = imagePath.length;
			var lastDot = imagePath.lastIndexOf(".");
			var fileType = imagePath.substring(lastDot,pathLength);	
			var fileType = fileType.toLowerCase();
			
			if(! (fileType=='.mp4' || fileType=='.flv' || fileType=='.wmv' || fileType=='.avi' || fileType=='.mov'))
			{
				$('#image').val('');
				$('#own_video_urlId').val('1');
				alert('<?php echo __('Invalid formate! Please provide only mp4,wmv,avi,mov and flv formate.');?>');	
			}else
			{
				$('#own_video_urlId').val('0');
			}
			
	
		});
});
		
	
</script> -->

<?php $type=array('free_video'=>'Add a YouTube video','own_video'=>'Add your own video'); ?>
<div id="main-container" >
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div id="sub-nav">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                           View review details <br><small>Review details supporting author's request</small>
                            </h1>
                        </div>

        </div>
    </div>
</div>


<div id="content" class="content">

    
	<div class="block">

		<div class="block-content">

		<div id="page-content-wrapper" class="no-bg-image wrapper-full">        
			

      
	  <?php echo $this->Form->create('view_report',array('url'=>array('controller'=>'users','action'=>'add_free_video'),'enctype'=>'multipart/form-data','id'=>'add_free_video',"class"=>"js-validation-bootstrap form-horizontal editTemplateForm"));
			?>
		
	 <div class="form-group">
                                  <label for="val-username" class="col-md-2 text-left"> Course Title:</label>                                            <div class="col-md-7">
                 
                  <?php echo $this->Form->input('title_',array('type'=>'text','id'=>'title','div'=>false,'label'=>false,'class'=>'form-control field text full required',"placeholder"=>"Enter title..","value"=>$info['CourseRemark']['Course']['title'],"readonly"=>true)); ?>
                    <span class="university-add-Error" >
                      <?php if(isset($error['title_'][0])) echo $error['title_'][0]; ?>
                    </span>
                                  </div></div>
                 <div class="form-group">
                                  <label for="val-username" class="col-md-2 text-left">Member Review:</label>                                            <div class="col-md-7">
					
						<?php echo $this->Form->input('description_',array('type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$info["RemarkReport"]["reason"],"placeholder"=>"Enter discription..","readonly"=>true)); ?>
						<span class="university-add-Error" >
							<?php if(isset($error['description_'][0])) echo $error['description_'][0]; ?>
						</span>
                                  </div></div>
                    <div class="form-group">
                                  <label for="val-username" class="col-md-2 text-left">Author Response:</label>                                            <div class="col-md-7">
					
						<?php echo $this->Form->input('description_',array('type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control field text full required','value'=>$info['CourseRemark']['comment'],"placeholder"=>"Enter discription..","readonly"=>true)); ?>
						<span class="university-add-Error">
							<?php if(isset($error['description_'][0])) echo $error['description_'][0]; ?>
						</span>
                                  </div></div>
               
              	
                   <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                  <button class="btn  btn-primary" type="button" onclick="history.go(-1);">Cancel</button>    
                  <!--<button class="btn  btn-primary" type="button" onclick="history.go(-1);">Keep Review</button>    <button class="btn  btn-primary" type="button" onclick="history.go(-1);">Remove Review</button>    -->
                  
                 </div>   </div>
               
				 <?php echo $this->Form->end(); ?> 
				
         </div>
		
            </form>
          </div>
        
	  </div>
