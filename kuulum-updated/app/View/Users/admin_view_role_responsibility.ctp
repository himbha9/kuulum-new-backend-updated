
<style>
	.hastable tbody th
	{
		padding:10px;
	}
	.hastable tr td
	{
		text-align:left;
	}	
</style>


<div id="sub-nav">
	<div class="page-title">
		<h1>View Roles and Responsibilities</h1>
	</div>
</div>
<div id="page-layout">
	<div id="page-content">
		<div id="page-content-wrapper" class="no-bg-image wrapper-full">
			<div class="inner-page-title">
				<h2 style="width:90%; ">Roles and Responsibilities Information</h2>
				<a style="margin-top:-10px;" class="ui-state-default ui-corner-all float-right ui-button" href="javascript:void(0);" onclick="history.go(-1);">Back</a>
				<span></span>
			</div>
			
			<div class="content-box content-box-header" style="border:none;">
			<div class="hastable">			
                   			<table id="sort-table"> 
                                <tbody>
                                    <tr>							
                                        <th width="10%;">Role</th> 
                                        <td width="30%;"><?php echo $info['Role']['role']; ?></td> 
                                    </tr>
                                    <?php $t_id=base64_encode(convert_uuencode($info['Role']['id'])); $i =1; ?>
                                    
                                    <tr>
                                        <th>Permissions</th>
                                        <td>      
                                        	<div class="hastable_td_inner"> 
                                            	
												<?php if($info['Role']['BC'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Browse Catalogue </div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['EAP'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Enter authenticated Partition </div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['EOM'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Edit own membership </div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['PC'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Purchase course</div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['PS'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Purchase subscription</div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['CC'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Contribute courses </div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['PN'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Publish news</div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['MM'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Manage mambers</div>
                                                <?php $i++;} ?>
                                                 <?php if($info['Role']['MC'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Manage courses</div>
                                                <?php $i++;} ?>
                                                 <?php if($info['Role']['MP'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Manage programmes</div>
                                                <?php $i++;} ?> 
                                                <?php if($info['Role']['MA'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> Manage administrators </div>
                                                <?php $i++;} ?>
                                                <?php if($info['Role']['VCFR'] == '1') {  ?>
                                                   <div class="hastable_td_inner_content">  <?php echo $i.")" ;?> View company financial reports </div>
                                                <?php $i++;} ?>
                                            </div>                                         
                                        </td>                                
                                    </tr>
                                     
                                    
                                </tbody>
							</table>
					<div class="clear"></div>
				</div>
					
					<?php /*?><ul class="sidebar-position">
						<li class="float-left" style="margin-top:20px;"> <a title="Left Sidebar" id="sidebar-left" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-w"></span> Left Sidebar </a> </li>
						<li class="float-right"  style="margin-top:20px;"> <a title="Right Sidebar" id="sidebar-right" href="javascript:void(0);" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon ui-icon-arrowthick-1-e"></span> Right Sidebar </a> </li>
					</ul><?php */?>
					
			</div>
			<div class="clearfix"></div>
			<!--<div id="sidebar">
				<?php //echo $this->element('adminElements/left_right_bar');?>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div> 
<style>
.hastable_td_inner
{
	float:left;
	width:100%;
}
.hastable_td_inner_content
{
	float:left;
	width:100%;
}
.hastable_td_inner_content
{
	float:left;
	padding:5px;
}
</style>