var host = window.location.host;
var proto = window.location.protocol;
var ajax_url = proto+"//"+host+"/kuulum/";

$(document).ready(function(){


$("#check-all").click(function (){
           if ( $(this).is(":checked") )
           {
                $(".checkboxs").each(function (){
                    $(this).prop('checked', true);
                });
            }else
            {
                $(".checkboxs").each(function (){
                    $(this).prop('checked', false);
                });
            }
        });


//Function For Ajax pagination		
		$(".pagination a").on('click',function(){	
		
		var acturl= $(this).attr('href');		
		$(".loading").show();		
		var randNumber=randomFunc();	
			 
		$.ajax({		
			type:'post',
			url:acturl+randNumber,
			success:function(html){				 
				$(".loadPaginationContent").html(html);
				$(".loading").hide();
			}	
		});
	
		return false;	
	});
    //Function END
	$('#showEmailLink').on('click',function(){			
		
		var relVal=$('#addMoreEmail').attr('rel');	
		$('#addMoreEmail').attr('rel',parseInt(relVal)+1);
		if(relVal>=2)
		{
			$('#addMoreEmail').hide();
		}
		else
		{
			$('#addMoreEmail').show();
		}
		$('#divMoreEmail').show();
		$('#showEmailLink').hide();	
	});
	$('#showMobileLink').on('click',function(){
		var relVal=$('#addMoreMobile').attr('rel');	
		$('#addMoreMobile').attr('rel',parseInt(relVal)+1);
		if(relVal>=2)
		{
			$('#addMoreMobile').hide();
		}
		else
		{
			$('#addMoreMobile').show();
		}
		$('#divMoreMobile').show();
		$('#showMobileLink').hide();		
	});
	$('.deleteMobileEmail').on('click',function(){
		
			var scrollTop=$(window).scrollTop();
			$('.loading').css('margin-top',scrollTop-10);
			$('.loading').show();
			var getId=$(this).attr('id');
			var getRel=$(this).attr('rel');
			var getTableName=getRel.split('%');
			var getRemoveDivId=$(this).parent().parent().attr('id');
			$.ajax({				
				url:ajax_url+'admin/Users/deleteStudentMobileEmail/'+getId+'/'+getTableName[0],
				success:function(html){
					if(html=='y')
					{
						//alert(getRemoveDivId);
						$('.loading').hide();
						$('#'+getRemoveDivId).remove();
						var count=$('#'+getTableName[1]).attr('rel');			
						$('#'+getTableName[1]).attr('rel',parseInt(count)-1);
						if(count<=3)
						{					
							$('#'+getTableName[1]).show();
						}
						
						//window.location.reload();
					}
					else
					{
						$('.loading').hide();
					}
				}
			});
			return false;
	});
	$('#confirmaStudentAddress').on('click',function(){
		
		var getCheckVal=$("#confirmaStudentAddress").is(':checked')
		if(getCheckVal==true)
		{
			var address1=$('#corresAddress1').val();
			var address2=$('#corresAddress2').val();
			var corrCity=$('#cCity').val();
			var corrPostCode=$('#cPostCode').val();
			
			$('#permaAddress1').val(address1);
			$('#permaAddress2').val(address2);
			$('#pCity').val(corrCity);	
			$('#pPostCode').val(corrPostCode);
		}
		if(getCheckVal==false)
		{
			$('#permaAddress1').val('');
			$('#permaAddress2').val('');
			$('#pCity').val('');	
			$('#pPostCode').val('');
		}

	});
	//FUNCTION FOR ADD AND REMOVE MOBILEs AND EMAILs
		
		$('.addMoreButtonInput').on('click',function(){			
			var count=$(this).attr('rel');	
			var getId=$(this).attr('id');
			var getTableField=getId.split('-');
			var html='<div class="keywordleftdetail"><input type="text" class="text_fore" style="margin-bottom:3px;" name="data['+getTableField[0]+']['+getTableField[1]+'][]"><a href="javascript:void(0)" class="remove" id="'+getId+'" rel=""><img src="'+ajax_url+'/img/front/button_cancel.png"></a></div>';
			$("#"+getTableField[2]).append(html);
			$('#'+getId).attr('rel',parseInt(count)+1);	
			if(count>=2)
			{
				$('#'+getId).hide();
			}
		});	
				
		$('.remove').on('click',function(){
			$(this).parent().remove();	
			var getId=$(this).attr('id');
		
			var count=$('#'+getId).attr('rel');			
			$('#'+getId).attr('rel',parseInt(count)-1);
			if(count<=3)
			{					
				$('#'+getId).show();
			}
		})
	//FUNCTION END
	
	//SEARCH STUDENT  FRUNCTION
	$('#searchRecord').on('click',function(){
			$('.adminTutorSearchWait').show();
			
		 
			$.ajax({
					url:ajax_url+"admin/users/adminStudentSearch",
					data:$('#searchAdminUser').serialize(),
					type:'post',
					success:function(html){
						$('.adminTutorSearchWait').hide();
						if(html=='error'){											
							$(".loadPaginationContent").html('No Record Found');
						} 
						else{
							$(".loadPaginationContent").html(html);
						}
					}
				});		
			return false;									
												 
	});
	
	//-END OF FUNCTION
	
	$('#searchTutorRecord').on('click',function(){
			$('.adminTutorSearchWait').show();
			$.ajax({
					url:ajax_url+"admin/users/adminEmailSearch",
					data:$('#searchAdminUser').serialize(),
					type:'post',
					success:function(html){
						$('.adminTutorSearchWait').hide();
						if(html=='error'){											
							$(".loadPaginationContent").html('No Record Found');
						} 
						else{
							$(".loadPaginationContent").html(html);
						}
					}
				});		
			return false;									
												 
	});
	
	
	$('#searchautor').on('click',function(){		
			$('.adminTutorSearchWait').show();
			$.ajax({
					url:ajax_url+"admin/users/authors",
					data:$('#searchauthor').serialize(),
					type:'post',
					success:function(html){
						$('.adminTutorSearchWait').hide();
						if(html=='error'){											
							$(".loadPaginationContent").html('No Record Found');
						} 
						else{
							$(".loadPaginationContent").html(html);
						}
					}
				});		
			return false;									
												 
	});
	
	$('#searchauthReq').on('click',function(){		
			$('.adminTutorSearchWait').show();
			$.ajax({
					url:ajax_url+"admin/users/author_requests",
					data:$('#searchauthorreq').serialize(),
					type:'post',
					success:function(html){
						$('.adminTutorSearchWait').hide();
						if(html=='error'){											
							$(".loadPaginationContent").html('No Record Found');
                                                         $('.dataTable').each(function() {
                                                            dt = $(this).dataTable();
                                                            dt.fnDraw();
                                                        })
						} 
						else{
							$(".loadPaginationContent").html(html);
                                                         $('.dataTable').each(function() {
                                                            dt = $(this).dataTable();
                                                            dt.fnDraw();
                                                        })
						}
					}
				});		
			return false;									
												 
	});
	
	
	$('#searchauthorCourse').on('click',function(){	
            //alert("hello ");
			$('.adminTutorSearchWait').show();
			$.ajax({
					url:ajax_url+"admin/users/courses",
					data:$('#searchCourse').serialize(),
					type:'post',
					success:function(html){
						$('.adminTutorSearchWait').hide();
						if(html=='error'){											
							//$(".loadPaginationContent").html('No Record Found');
                                                         $('.dataTable').each(function() {
                                                            dt = $(this).dataTable();
                                                            dt.fnDraw();
                                                        })
						} 
						else{
							$(".loadPaginationContent").html(html);
					 $('.dataTable').each(function() {
                                                            dt = $(this).dataTable();
                                                            dt.fnDraw();
                                                        })	
                                            }
					}
				});		
			return false;									
	});
	
	
	
	$('#searchFreeVideo').on('click',function(){		
			$('.adminTutorSearchWait').show();
			$.ajax({
					url:ajax_url+"admin/users/adminSearchFreeVideo",
					data:$('#searchfree').serialize(),
					type:'post',
					success:function(html){
						$('.adminTutorSearchWait').hide();
						if(html=='error'){											
							//$(".loadPaginationContent").html('No Record Found');
                                                         $('.dataTable').each(function() {
                                                            dt = $(this).dataTable();
                                                            dt.fnDraw();
                                                        })
						} 
						else{
							$(".loadPaginationContent").html(html);
                                                         $('.dataTable').each(function() {
                                                            dt = $(this).dataTable();
                                                            dt.fnDraw();
                                                        })
						}
					}
				});		
			return false;									
												 
	});
	//END OF FUNCTION
	
	//-SEARCH TUTOR  FRUNCTION
	

	//END OF FUNCTION
	
	
	//SEARCH FOR THE OPEN JOB COURSE-
	 $('#searchJob').on('click',function(){
		 $('.adminTutorSearchWait').show();
		 $.ajax({
		 	url:ajax_url+'admin/Users/StudentOpenJobSearch',
			data:$('#searchOpenJob').serialize(),
			type:'post',
			success:function(html){
				$('.adminTutorSearchWait').hide();
				if(html=='error'){
					$(".loadPaginationContent").html('No Record Found');	
				}else{
					$(".loadPaginationContent").html(html);	
				}
			}
		 });
		return false;	 
	 });
	
//SEARCH FOR THE ASSIGNED  JOB COURSE-
	 $('#Assignedjob').on('click',function(){
		 $('.adminTutorSearchWait').show();
		 $.ajax({
		 	url:ajax_url+'admin/Users/StudentAssignedJobSearch',
			data:$('#searchAssignedJob').serialize(),
			type:'post',
			success:function(html){
				$('.adminTutorSearchWait').hide();
				if(html=='error'){
					$(".loadPaginationContent").html('No Record Found');	
				}else{
					$(".loadPaginationContent").html(html);	
				}
			}
		 });
		return false;	 
	 });
	
//SEARCH FOR THE TUTOR"S PAYMENT FROM-TO Date
		
	 $('#payment').on('click',function(){
		 $('.adminTutorSearchWait').show();
		  var startdate=$('#startDate').val();
		  var enddate=$('#endDate').val();
		  var search_text=$("#search_text").val();
		  var search_type=$('#search_type').val();
		  var explodeStartDate=startdate.split("-");
		  var explodeEndDate=enddate.split("-");
			
			var dt1  = parseInt(startdate.substring(0,2),10); 
			var mon1 = parseInt(startdate.substring(3,5),10);
			var yr1  = parseInt(startdate.substring(6,10),10);
			var dt2  = parseInt(enddate.substring(0,2),10);
			var mon2 = parseInt(enddate.substring(3,5),10);
			var yr2  = parseInt(enddate.substring(6,10),10);
			var date1 = new Date(yr1, mon1, dt1);
			var date2 = new Date(yr2, mon2, dt2);
			if(search_type=='date'){
				 if(startdate==""){
					alert('Select From Date');
					$('#startDate').focus();
					 $('.adminTutorSearchWait').hide();
					return false; 
				 }else if(enddate==""){
					alert('Select To Date');
					$('#endDate').focus();
					 $('.adminTutorSearchWait').hide();
					return false; 
				 }else if(date2<=date1)
				  {
					  alert("To date must be greater then From date");
					  $('.adminTutorSearchWait').hide();
					  $('#endDate').focus();
					  return false;
				  }
			}
		 $.ajax({
		 	url:ajax_url+'admin/Users/TutorPaymentJobSearch',
			data:$('#searchpayment').serialize(),
			type:'post',
			success:function(html){
				$('.adminTutorSearchWait').hide();
				if(html=='error'){
					$(".loadPaginationContent").html('No Record Found');	
				}else{
					$(".loadPaginationContent").html(html);	
				}
			}
		 });
		return false;	 
	 });
	 
	  $('#searchLessonRecord').on('click',function(){
		 $('.adminTutorSearchWait').show();
		 $.ajax({
		 	url:ajax_url+'admin/Users/completedLessonSearch',
			data:$('#searchAdminLesson').serialize(),
			type:'post',
			success:function(html){
				$('.adminTutorSearchWait').hide();
				if(html=='error'){
					$(".loadPaginationContent").html('No Record Found');	
				}else{
					$(".loadPaginationContent").html(html);	
				}
			}
		 });
		return false;	 
	 });
//SEARCH FOR THE PAYMENT from TO Date
		
	 $('#studentpayment').on('click',function(){
		 $('.adminTutorSearchWait').show();
		  var startdate=$('#startDate').val();
		  var enddate=$('#endDate').val();
		  var search_text=$("#search_text").val();
		  var search_type=$('#search_type').val();
		  var explodeStartDate=startdate.split("-");
		  var explodeEndDate=enddate.split("-");
			
			var dt1  = parseInt(startdate.substring(0,2),10); 
			var mon1 = parseInt(startdate.substring(3,5),10);
			var yr1  = parseInt(startdate.substring(6,10),10);
			var dt2  = parseInt(enddate.substring(0,2),10);
			var mon2 = parseInt(enddate.substring(3,5),10);
			var yr2  = parseInt(enddate.substring(6,10),10);
			var date1 = new Date(yr1, mon1, dt1);
			var date2 = new Date(yr2, mon2, dt2);
			if(search_type=='date'){
				 if(startdate==""){
					alert('Select From Date');
					$('#startDate').focus();
					 $('.adminTutorSearchWait').hide();
					return false; 
				 }else if(enddate==""){
					alert('Select To Date');
					$('#endDate').focus();
					 $('.adminTutorSearchWait').hide();
					return false; 
				 }else if(date2<=date1)
				  {
					  alert("To date must be greater then From date");
					  $('.adminTutorSearchWait').hide();
					  $('#endDate').focus();
					  return false;
				  }
			}
		 $.ajax({
		 	url:ajax_url+'admin/Users/StudentPaymentSearch',
			data:$('#searchpayment').serialize(),
			type:'post',
			success:function(html){
				$('.adminTutorSearchWait').hide();
				if(html=='error'){
					$(".loadPaginationContent").html('No Record Found');	
				}else{
					$(".loadPaginationContent").html(html);	
				}
			}
		 });
		return false;	 
	 }); 
         
         
       

        var val1=  $("#duration").val();
        if(val1=='0')
        {
        $("#durationdateselection").show(); 
       
           $("#datepicker").attr("required","required");  
            $("#datepicker1").attr("required","required");  
          
        }
        else
        {
           $("#durationdateselection").hide(); 
             $("#datepicker").removeAttr("required","required");
              $("#datepicker1").removeAttr("required","required");
        }
          
        
        
            
      
        var val2=  $("#select-course").val();
        if(val2==0)
        {
        $("#CommissionParticular-course").hide();
       
              $("#CommissionParticular-course").removeAttr("required","required");
          
        }
        else
        {
           $("#CommissionParticular-course").show(); 
              $("#CommissionParticular-course").attr("required","required");
        }
    
       $('#commission').keyup(function(){
		   
		var commission=$(this).val();
		var regex = /^([0-9.]+)$/;
		var value=commission.match(regex);
		if(value==null)
		{
		
		$(".error-commission").html("<p style='color:red;'>* Enter a number from 0 to 100. The number may have a maximum of two decimal places. *</p>");
		}
		else
		{
		
			$(".error-commission").html("");
			}
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 2){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(2);
            }  
         }        
	 
         return this; //for chaining
		 
		 
		 
    });
    
    
      
       $('#defaultcommission').keyup(function(){
		   
		var commission=$(this).val();
		var regex = /^([0-9.]+)$/;
		var value=commission.match(regex);
		if(value==null)
		{
		
		$(".error-defcommission").html("<p style='color:red;'>* Enter a number from 0 to 100. The number may have a maximum of two decimal places. *</p>");
		}
		else
		{
		
			$(".error-defcommission").html(""); 
			}
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 2){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(2);
            }  
         }        
	 
         return this; //for chaining
		 
		 
		 
    });
	
	//commission form 1 //
	
	       $('#commission1').keyup(function(){
		   
		var commission=$(this).val();
		var regex = /^([0-9.]+)$/;
		var value=commission.match(regex);
		if(value==null)
		{
		
		$(".error-commission1").html("<p style='color:red;'>* Enter a number from 0 to 100. The number may have a maximum of two decimal places. *</p>");
		}
		else
		{
		
			$(".error-commission1").html("");
			}
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 2){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(2);
            }  
         }        
	 
         return this; //for chaining
		 
		 
		 
    });
	
	
	  $('#CommissionAuthorName').change(function(){
		   
		var commission=$(this).val();	
		console.log(commission);
		if(commission.length>0)
		{
			$(".error-author_name").html(""); 
			}
		     
	 
     
		 
		 
		 
    });
         
       $('#duration').change(function(){
		   
		var commission=$(this).val();	
		
		if(commission!='')
		{
			$(".error-duration").html("");
			}
		     
	 
		 
    });   
         // duration 1

   $('#duration1').change(function(){
		   
		var commission=$(this).val();	
		
		if(commission!='')
		{
			$(".error-duration1").html("");
			}
		     
	 
		 
    });   
         		 
		   $('#select-course').change(function(){
		   
		var commission=$(this).val();	
		
		if(commission!='')
		{
			$(".error-courses").html("");
			}
		     
	 
		 
    }); 
	
	  $('#CommissionParticular-course').change(function(){
		   
		var commission=$(this).val();	
		console.log(commission);
		if(commission.length>0)
		{
			$(".error-particular-course").html("");
			}
		     
	 
     
		 
		 
		 
    });
	 
	 
});

$(document).keypress(function(e){

	if(e.keyCode==27)//Disable popup on pressing `ESC`
	{
		//$('#documentViewDiv').hide();
		$('#documentViewDiv').fadeOut();
		hidePopUp('showpopup'); 	
	}
	
});

function checkcommission(){
var num=$("#defaultcommission1").val();
//console.log(num);
//alert($(".error-defcommission").html());
if(parseFloat(num)>parseFloat(100))
{
alert("Enter a number from 0 to 100. The number may have a maximum of two decimal places.");
return false;
}

else
{
//$("#Defaultcommission").submit();	
return true;
}
} 

function checknumber(package_id)
{ 
var num=$("#commission01").val();
var a2=$("#CommissionAuthorName01").val();
var a3=$("#duration01").val();

var a5=$("#select-course01").val();
var a6=$("#datepicker11").val();
var a7=$("#datepicker12").val();

$(".errorcheck").html("");		
if(num=='')
{
	$(".error-commission").html("<p style='color:red;'>* Commission field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-commission').position().top}, 'slow');	
}
else if(a2=='')
{
	$(".error-author_name").html("<p style='color:red;'>* Author Name field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-author_name').position().top}, 'slow');	
}
else if(a3=='')
{
	$(".error-duration").html("<p style='color:red;'>* Duration field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-duration').position().top}, 'slow');	
}
else if(a6=='' && a3==0)
{
	$(".error-date_from").html("<p style='color:red;'>* Date_From field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-date_from').position().top}, 'slow');	
}
else if(a7=='' && a3==0)
{
	$(".error-date_to").html("<p style='color:red;'>* Date_To field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-date_to').position().top}, 'slow');	
} 
else if(a5==null || (($.inArray("all",a5)==1 && $.inArray("",a5)==0 && a5.length>2) || ($.inArray("all",a5)==0 && $.inArray("",a5)==-1 && a5.length!=1)) || ($.inArray("",a5)==0 && $.inArray("all",a5)==-1) || ($.inArray("",a5)==0 && $.inArray("all",a5)==1))
{
        $(".error-courses").html("<p style='color:red;'>* Please select atleast one course or select all courses *</p>");
	$('html, body').animate({scrollTop:$('.error-courses').position().top}, 'slow');
        
}

else if(parseFloat(num)>parseFloat(100))
{
alert("Enter a number from 0 to 100. The number may have a maximum of two decimal places.");
}
//else if($(".error-commission").html()=='' && $(".error-author_name").html()=='' && $(".error-duration").html()=='' && $(".error-courses").html()=='' && $(".error-date_from").html()=='' && $(".error-date_to").html()=='' && $(".error-particular-course").html()=='')
else
{   var datapass='';
    var urlcall='';
    if(package_id!='')
    {
    datapass='package_id='+package_id+'&commission='+num+'&author='+a2+'&duration='+a3+'&course='+a5+'&particular='+a5+'&date_from='+a6+'&date_to='+a7;   
    urlcall=ajax_url+'admin/Users/commissioncheck_edit';
    }
    else
    {
    datapass='commission='+num+'&author='+a2+'&duration='+a3+'&course='+a5+'&particular='+a5+'&date_from='+a6+'&date_to='+a7;
    urlcall=ajax_url+'admin/Users/commissioncheck';
    }
       $.ajax({
	url:urlcall,
	data:datapass,
	type:'POST',
	success:function(result)
	{		
	
	if($.trim(result)=='')
	{
	$("#addCommission01").submit();
	}
	else
	{
	$(".errorcheck").append(result);		
	
	
	
	}
	}
	});

}


}



function checknumber1()
{    
var num=$("#commission").val();
var a2=$("#CommissionAuthorName").val();
var a3=$("#duration").val();
var a4=$("#select-course").val();
var a5=$("#CommissionParticular-course").val();
var a6=$("#datepicker").val();
var a7=$("#datepicker1").val();

if(parseFloat(num)>parseFloat(100))
{
alert("Please Select Value less than 100 and more than 1");
return false;
}
var ajaxdata='';
$.ajax({
	url:ajax_url+'admin/Users/commissioncheck',
	data:'commission='+num+'&author='+a2+'&duration='+a3+'&course='+a4+'&particular='+a5+'&date_from='+a6+'&date_to='+a7,
	type:'POST',
	success:function(result)
	{		
	
	$(".errorcheck").html(result);	
	
	var value = $(".errorcheck").html();
	
	
	if($.trim(value) == "")
	{
		
	$("#editCommission").submit();
	}
	else
	{		
	return false;
	}
	
	}
	
	});
	

}

function overlay()
{
	if($("#overlay").length!=0)
	{
		
		position=$("#overlay").offset();
		$(".overlay").css("width",$("#overlay").width());
		$(".overlay").css("height",$("#overlay").height());
		$(".overlay").css(position);
		$("#new_loading").css("top",($("#overlay").height())/2);
		$("#new_loading").css("left",($("#overlay").width())/2);
	}
	$(".overlay").show();
}

/* pop up js by inder */
function loadPiece(obj) {
	show_overlay();
	if($("#pagingStatus").length != '0'){
		$("#pagingStatus").val(obj.href);
	}
    $(obj.divName).load(unescape(obj.href), {}, function(){
		hidePopUp("showpopup2");
		if(obj.callback){
			obj.callback();
		}
    });
} 


function showPopUp(trgt)
{
	//$("#"+trgt).css('top',($(window).height()/2)-$("#"+trgt).height()/2);
	$("#"+trgt).css('left',$(window).width()/2-$("#"+trgt).width()/2);
	$("#"+trgt).show();
}

function hidePopUp(trgt)
{
	$("#"+trgt).fadeOut("slow");
	$(".black_overlay").hide();
}


function show_overlay()
{
	
	if($(".black_overlay").length != '0')
	{
		if (window.innerHeight) 
		{// Firefox
			if(window.scrollMaxY)
			{
				yWithScroll = window.innerHeight + window.scrollMaxY;
				xWithScroll = window.innerWidth + window.scrollMaxX;
			}
			else
			{
				yWithScroll = window.innerHeight;
				xWithScroll = window.innerWidth ;
			}
		} 
		else if (document.body.scrollHeight > document.body.offsetHeight)
		{ // all but Explorer Mac
			yWithScroll = document.body.scrollHeight;
			xWithScroll = document.body.scrollWidth;
		}
		else 
		{ // works in Explorer 6 Strict, Mozilla (not FF) and Safari
			yWithScroll = document.body.offsetHeight;
			xWithScroll = document.body.offsetWidth;
		}
		$(".black_overlay").css('height',$(document).height());
		$(".black_overlay").show();
		
	}
}



function showDivAtCenter(divid) 
{
	var scrolledX, scrolledY;
	if( self.pageYoffset )
	{
		scrolledX = self.pageXoffset;
		scrolledY = self.pageYoffset;
	} 
	else if( document.documentElement && document.documentElement.scrollTop ) 
	{
		scrolledX = document.documentElement.scrollLeft;
		scrolledY = document.documentElement.scrollTop;
	} 
	else if( document.body ) 
	{
		scrolledX = document.body.scrollLeft;
		scrolledY = document.body.scrollTop;
	}
	var centerX, centerY;
	if( self.innerHeight ) 
	{
		centerX = self.innerWidth;
		centerY = self.innerHeight;
	} 
	else if( document.documentElement && document.documentElement.clientHeight ) 
	{
		centerX = document.documentElement.clientWidth;
		centerY = document.documentElement.clientHeight;
	}
	else if( document.body ) 
	{
		centerX = document.body.clientWidth;
		centerY = document.body.clientHeight;
	}
	Xwidth=$('#'+divid).width();
	Yheight=$('#'+divid).height();
	
	var leftoffset = scrolledX + (centerX - Xwidth) / 2;
	var topoffset = scrolledY + (centerY - Yheight) / 2;
	
	leftoffset=(leftoffset<0)?0:leftoffset;
	topoffset=(topoffset<0)?0:topoffset;	
	var o=document.getElementById(divid);
	var r=o.style;
	r.top = topoffset + 'px';
	r.left = leftoffset + 'px';
	r.display = "block";  
}

function ajax_form(form,site_url,classWait,ckeditor)
{   

	var form = form;
	$('.'+classWait).show();	
	var req = $.post
	(	 	
		ajax_url+site_url, 
		$('#' + form).serialize(), 
		function(html)
		{	
			var explode = html.split("\n");
			var shown = false;
			$('.'+classWait).hide();	
			for ( var i in explode )
			{
				if(parseInt(i)!=i)
				break;
				var explode_again = explode[i].split("|");
				if ($.trim(explode_again[0])=='error') {
					shown = true;
					$('#err_' + explode_again[1]).show();
					if($('#err_' + explode_again[1]).length>0) {
						$('#err_' + explode_again[1]).html(explode_again[2]);
					}					
				}
				else if ($.trim(explode_again[0])=='ok') {
					$('#err_' + explode_again[1]).hide();										
				}
			}			
			if ( ! shown ){				
				$('#'+form).submit();							
				$(".overlay").hide();
			}
			else {				
			}			
			req = null;
		}		
	);
	return false;
}

//FUNCTION FOR DELETE OF STUDENT

	function deleteUser(url,page,id,tableName,renderPath,renderElement)
	{
		if(confirm('Do you want to delete this record?')){
			$(".loading").show();
			var randNumber=randomFunc();
			$.ajax({
				url:ajax_url+url+'/'+page+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
				success:function(html){
                                    console.log(this.url);
					$(".loadPaginationContent").html(html);
					$(".loading").hide();
					
				}
			});
			return false;
		}
		else
		{
			return false;
		}
		
	}
        
        
        function deleteCommission(url,page,id,tableName,renderPath,renderElement)
	{
		if(confirm('Do you want to delete this record?')){
			$(".loading").show();
			var randNumber=randomFunc();
			$.ajax({
				url:ajax_url+url+'/'+page+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
				success:function(html){
                                   
					$(".loadPaginationContent").html(html);
					$(".loading").hide();
					
				}
			});
			return false;
		}
		else
		{
			return false;
		}
		
	}
	
        
        function dateduration()
        {
			
        var val=  $("#duration01").val();
		
        if(val=='0')
        {
        $("#durationdateselection01").show();
       // $("#datepicker").attr("required","required");
      //  $("#datepicker1").attr("required","required");         
        }
        else
        {
        $("#durationdateselection01").hide(); 
       // $("#datepicker").removeAttr("required","required");
      //  $("#datepicker1").removeAttr("required","required");
       // $("#datepicker").val("");
       // $("#datepicker1").val("");
        }
          
        }
		
		
		 function dateduration1()
        {
			
        var val=  $("#duration1").val();
		
        if(val=='0')
        {
        $("#durationdateselection1").show();
        $("#datepicker2").attr("required","required");
        $("#datepicker3").attr("required","required");         
        }
        else
        {
        $("#durationdateselection1").hide(); 
        $("#datepicke2").removeAttr("required","required");
        $("#datepicker3").removeAttr("required","required");
        $("#datepicker2").val("");
        $("#datepicker3").val("");
        }
          
        }
        
        
            
        function coursesselection()
        {
        var val=  $("#select-course01").val();
        if(val==0)
        {
        $("#CommissionParticular-course01").hide();
       
            $("#CommissionParticular-course01").removeAttr("required","required");
             $("#CommissionParticular-course01").val("");
          
        }
        else
        {
           $("#CommissionParticular-course01").show(); 
            $("#CommissionParticular-course01").attr("required","required");
        }
    }
        
//DELETE ACTIVE CHANGE PROFILE REQUEST-

function deleteChangeRequest(url,change_id,t_id,id,tableName,renderPath,renderElement)
	{
		if(confirm('Do you want to delete this record.')){
			$(".loading").show();
			var randNumber=randomFunc();
			$.ajax({
							
				url:ajax_url+url+'/'+change_id+'/'+t_id+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
				success:function(html){
					$(".loadPaginationContent").html(html);
					$(".loading").hide();
					
				}
			});
			return false;
		}
		else
		{
			return false;
		}
		
	}


//FUNCTION FOR UPDATE STATUS


	function updateStatus(url,page,id,tableName,renderPath,renderElement)
	{
		
		$(".loading").show();
		var randNumber=randomFunc();
		$.ajax({			
			url:ajax_url+url+'/'+page+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
			success:function(html){
				$(".loadPaginationContent").html(html);
				$(".loading").hide();
				
			}
		});
		return false;
	}
	
//FUNCTION TO ASSIGN OR REJECT JOB-

	function assignJob(url,tutorName,page,t_id,job_id,tableName,table_id,renderPath,renderElement)
	{	
		if(confirm("Are you sure you would like to assign this job to  "+tutorName))
			{
			$(".loading").show();
			var randNumber=randomFunc();
				$.ajax({
					url:ajax_url+url+'/'+t_id+'/'+job_id+'/'+tableName+'/'+table_id+'/'+randNumber,
					dataType:'json',
					success:function(html){
						if(html.suc=='y'){
							window.location.href=ajax_url+'admin/Users/interested_tutors/'+html.id;
						}	
						
					}
				});
				return false;
		}else{
			return false;
		}
	}
			
//EOF ASSIGN OR REJECT JOB
	

//FUNCTION FOR TUTOR ACTIVATION-

	function makeAuthor(url,page,id,tableName,renderPath,renderElement)
	{
		
		$(".loading").show();
		var randNumber=randomFunc();
		$.ajax({			
			url:ajax_url+url+'/'+page+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
			success:function(html){
				$(".loadPaginationContent").html(html);
				$(".loading").hide();
				
			}
		});
		return false;
	}
	
	
	
//END OF TUTOR ACTIVATION FUNCTION


// FUNCTION FOR  MEMBER AUTHOR REEQUEST 
function activate(url,page,id,tableName,renderPath,renderElement)
	{
		
		$(".loading").show();
		var randNumber=randomFunc();
		$.ajax({			
			url:ajax_url+url+'/'+page+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
			success:function(html){
				$(".loadPaginationContent").html(html);
				$(".loading").hide();
				
			}
		});
		return false;
	}
// END OF MEMBER AUTHOR REEQUEST FUNCTION

//ASSIGN JOB TO PARTICULAR TUTOR

function assignJobtoTutor(url,tutorName,t_id,job_id)
	{	
		if(confirm("Are you sure you would like to assign this job to  "+tutorName))
			{
			$(".loading").show();
			var randNumber=randomFunc();
				$.ajax({
					url:ajax_url+url+'/'+t_id+'/'+job_id,
					success:function(html){
						window.location.href=ajax_url+'admin/Users/assigned_job';
					}
				});
				return false;
		}else{
			return false;
		}
	}


function checkvalidmodify()
{
	var selected=[];
var formhtml='';
		$(".checkboxes").each(function(index){
						if($(this).is(":checked")==true)
						{							
						formhtml+="<input type='hidden' name='modifyid[]' value='"+$(this).val()+"'/>";
						selected.push($(this).val());
						}
							
						
						});
						$(".errorcheck").html("");
var num=$("#commission").val();
var a3=$("#duration").val();
var a6=$("#datepicker").val();
var a7=$("#datepicker1").val();
if(num=='')
{
$(".error-commission").html("<p style='color:red;'>* Commission field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-commission').position().top}, 'slow');
return false;
}
else if(a3=='-1')
{
$(".error-duration").html("<p style='color:red;'>* Duration field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-duration').position().top}, 'slow');
return false;
}
else if(a6=='' && a3==0)
{
$(".error-date_from").html("<p style='color:red;'>* Date_From field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-date_from').position().top}, 'slow');
return false;	
}
else if(a7=='' && a3==0)
{
$(".error-date_to").html("<p style='color:red;'>* Date_To field is required *</p>");
	$('html, body').animate({scrollTop:$('.error-date_to').position().top}, 'slow');
return false;
} 

else if(parseFloat(num)>parseFloat(100))
{
alert("Enter a number from 0 to 100. The number may have a maximum of two decimal places.");
return false;
}
else if($(".error-commission").html()=='' && $(".error-duration").html()=='' && $(".error-date_from").html()=='' && $(".error-date_to").html()=='' )
{	
$.ajax({
	url:ajax_url+'admin/Users/modifycheck',
	data:'commission='+num+'&duration='+a3+'&date_from='+a6+'&date_to='+a7+"&selectedids="+JSON.stringify(selected),
	type:'POST',
	success:function(result)
	{		
	
	if($.trim(result)=='')
	{
	$("#modifyform").submit();
	}
	else
	{
	$(".errorcheck").append(result);
	$('html, body').animate({scrollTop:$('.errorcheck').position().top}, 'slow');			
	
	}
	}
	});

}
}

function checkvalidmodify1()
{
	var selected=[];
var formhtml='';
		$(".checkboxes").each(function(index){
						if($(this).is(":checked")==true)
						{							
						formhtml+="<input type='hidden' name='modifyid[]' value='"+$(this).val()+"'/>";
						selected.push($(this).val());
						}
							
						
						});
						$(".errorcheck").html("");
var num=$("#commission1").val();

if(num=='')
{
$(".error-commission1").html("<p style='color:red;'>* Commission field is required *</p>");
$('html, body').animate({scrollTop:$('.errorcheck').position().top}, 'slow');			
return false;
}


else if(parseFloat(num)>parseFloat(100))
{
alert("Enter a number from 0 to 100. The number may have a maximum of two decimal places.");
return false;
}
else if($(".error-commission1").html()=='' )
{



	$("#modifyform1").submit();
	
	
	

}
}

function checkvalidmodify2()
{
	var selected=[];
var formhtml='';
		$(".checkboxes").each(function(index){
						if($(this).is(":checked")==true)
						{							
						formhtml+="<input type='hidden' name='modifyid[]' value='"+$(this).val()+"'/>";
						selected.push($(this).val());
						}
							
						
						});
						$(".errorcheck").html("");

var a3=$("#duration1").val();
var a6=$("#datepicker2").val();
var a7=$("#datepicker3").val();
if(a3=='-1')
{
$(".error-duration1").html("<p style='color:red;'>* Duration field is required *</p>");
$('html, body').animate({scrollTop:$('.error-duration1').position().top}, 'slow');			
return false;
}
else if(a6=='' && a3==0)
{
$(".error-date_from1").html("<p style='color:red;'>* Date_From field is required *</p>");
$('html, body').animate({scrollTop:$('.error-date_from1').position().top}, 'slow');			
return false;	
}
else if(a7=='' && a3==0)
{
$(".error-date_to1").html("<p style='color:red;'>* Date_To field is required *</p>");
$('html, body').animate({scrollTop:$('.error-date_to1').position().top}, 'slow');			
return false;
} 
else if($(".error-duration1").html()=='' && $(".error-date_from1").html()=='' && $(".error-date_to1").html()=='' )
{

$.ajax({
	url:ajax_url+'admin/Users/modifycheck',
	data:'duration='+a3+'&date_from='+a6+'&date_to='+a7+"&selectedids="+JSON.stringify(selected),
	type:'POST',
	success:function(result)
	{		
	
	if($.trim(result)=='')
	{
	$("#modifyform2").submit();
	}
	else
	{
	$(".errorcheck").append(result);			
	
	}
	}
	});

}
}
//COMPLETE LESSON

function assignComplteLesson(url,id,lesson,job_id)
	{	
		if(confirm("Are you sure you would like complete lesson:"+lesson))
			{
			$(".loading").show();
			var randNumber=randomFunc();
				$.ajax({
					url:ajax_url+url+'/'+id,
					success:function(html){
						window.location.href=ajax_url+'admin/Users/current_lesson_detail/'+job_id;	
					}
				});
				return false;
		}else{
			return false;
		}
	}

//DELETE NEW APPLICATION DATA


function deleteApplicationData(url,t_id,id,tableName,renderPath,renderElement)
	{
		if(confirm('Do you want to delete this record.')){
			$(".loading").show();
			var randNumber=randomFunc();
			$.ajax({
							
				url:ajax_url+url+'/'+t_id+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
				success:function(html){
					$(".loadPaginationContent").html(html);
					$(".loading").hide();
					
				}
			});
			return false;
		}
		else
		{
			return false;
		}
		
	}

//END

//FUNCTION FOR EDIT TUTOR'S DETAILS

	function editTutor(url,id,tableName,renderPath)
	{
		
		$(".loading").show();
		var randNumber=randomFunc();
		$.ajax({			
			url:ajax_url+url+'/'+id,
			success:function(html){
				$(".loadPaginationContent").html(html);
				$(".loading").hide();
				
			}
		});
		return false;
	}
	
	
	
//END OF EDIT TUTOR'S DETAILS FUNCTION

	
	function updateStatus(url,page,id,tableName,renderPath,renderElement,courseId)
	{
		
		$(".loading").show();
		var randNumber=randomFunc();
		$.ajax({			
			url:ajax_url+url+'/'+page+'/'+id+'/'+tableName+'/'+renderPath+'/'+renderElement+'/'+courseId+randNumber,
			success:function(html){
				$(".loadPaginationContent").html(html);
				$(".loading").hide();
				
			}
		});
		return false;
	}
	
	
	//FUNCTION FOR DELETE OF UUNIVERSITY COURSE(S)

	function deleteUniversityCourse(url,page,id,u_id,tableName,renderPath,renderElement)
	{
		if(confirm('Do you want to delete this record.')){
			$(".loading").show();
			var randNumber=randomFunc();
			$.ajax({			
				url:ajax_url+url+'/'+page+'/'+id+'/'+u_id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
				success:function(html){
					$(".loadPaginationContent").html(html);
					$(".loading").hide();
					
				}
			});
			return false;
		}
		else
		{
			return false;
		}
		
	}
	
//FUNCTION FOR DELETE OF UUNIVERSITY COURSE(S)

//FUNCTION FOR UPDATE STATUS OF UNIVERSITY COURSE


	function updateUniversityCourseStatus(url,page,id,u_id,tableName,renderPath,renderElement)
	{
		
		$(".loading").show();
		var randNumber=randomFunc();
		$.ajax({			
			url:ajax_url+url+'/'+page+'/'+id+'/'+u_id+'/'+tableName+'/'+renderPath+'/'+renderElement+randNumber,
			success:function(html){
				$(".loadPaginationContent").html(html);
				$(".loading").hide();
				
			}
		});
		return false;
	}
	
//FUNCTION FOR FEEDBACK FORM-
function ajax_form_feedback(form,site_url,classWait,ckeditor)
{   
	
	var form = form;
	$('.'+classWait).show();	
	var req = $.post
	(	 	
		ajax_url+site_url, 
		$('#' + form).serialize(), 
		function(html)
		{	
			var explode = html.split("\n");
			var shown = false;
			$('.'+classWait).hide();
			for ( var i in explode )
			{
				if(parseInt(i)!=i)
				break;
				var explode_again = explode[i].split("|");
				if ($.trim(explode_again[0])=='error') {
					shown = true;
					$('#err_' + explode_again[1]).show();
					if($('#err_' + explode_again[1]).length>0) {
						$('#err_' + explode_again[1]).html(explode_again[2]);
					}
					$('.'+classWait).hide();
				}
				else if ($.trim(explode_again[0])=='ok') {
					$('#err_' + explode_again[1]).hide();
					$('.'+classWait).hide();					
				}
			}			
			if ( ! shown ){	
			 if(form=='tutorQualification' || form=='tutorEditQualification'){		
				
				$.ajax({
						url:ajax_url+'admin/Users/add_qualification',
						type:'post',
						data:$("#"+form).serialize(),
						success:function(resp){ 
							window.location.reload();
							 $("#degree").val('');
							 $("#universityName").val('');
							 $("#gradeGot").val('');
							 $("#yearAwarded").val('');
							 $('#mask , .login-popup').fadeOut(200 , function() {
							$('#mask').remove(); 
							 })
						}
					
				});
			}
			
			
				//$('#'+form).submit();							
				$('.'+classWait).hide();
				return false;
			}
			else {				
			}			
			req = null;
		}		
	);
	return false;
  }
	//DELETE QUALIFICATION/LANGUAGE/EXPERIENCE/COURSE(S)
	
	
	//EOF
	
	function randomFunc()
	{
		var d = new Date();
		var randomNumber = "/" + d.getHours() +""+ d.getMinutes() +""+ d.getSeconds() +""+ d.getMilliseconds();
		return randomNumber;
	}
	
	
	function checksearchvalidation(){
		
	var option=	$("#CommissionSearchSearchBy").val();
	
	
	if(option=='0')
	{
		var value=$("#CommissionSearchSearch").val();
	  var numbers = /^([A-z0-9\s]+)$/;  
                         
                          if(value.match(numbers)==null)
                          {
                         $("#search-error").html("<p>* Please fill valid author name *</p>");  
                          return false;
                          }
	
		}
		else if(option=='1')
	{
		var value=$("#CommissionSearchSearch").val();
	  var numbers =  /^[A-z\._0-9-]+[@]+[A-z\._0-9-]+([.]+[A-z]+)+$/; 
                         
                          if(value.match(numbers)==null)
                          {
                           $("#search-error").html("<p>* Please fill valid author email *</p>");  
                          return false;
                          }
	
		}
		else if(option=='2'){
			
			var value=$("#CommissionSearchSearch").val();
	  var numbers =   /^[0-9.]+$/; 
                         
                          if(value.match(numbers)==null)
                          {
                           $("#search-error").html("<p>* Please fill integer value *</p>");  
                          return false;
                          }
			}
		}

 function selectcourses()
        {
	var authors=$("#DiscountAuthorName01").val();
	$.ajax({
		url:ajax_url+'admin/Users/getallauthors',
		data:{authors:authors},
		type:'POST',
		success:function(result){
			
			$("#DiscountParticular-course01").html(result);
			
			}
		
		});
	}
        
         // Select course for selected authors on commission page
        function selectcomm_courses()
        {
	var authors=$("#CommissionAuthorName01").val();
	$.ajax({
		url:ajax_url+'admin/Users/getallcomm_authors',
		data:{authors:authors},
		type:'POST',
		success:function(result){
			
			$("#select-course01").html(result);
			
			}
		});
	}
        
        
        function selectmembers(){
	
	var authorid=$("#DiscountAuthorName01").val();
	$.ajax({
		url:ajax_url+'admin/Users/getallmembers',
		data:{authorid:authorid},
		type:'POST',
		success:function(result){
			console.log(result);
			$("#DiscountMember01").html(result);
			
			}
		
		});
	
	
	}
	
        
        
        
        
        
        
	function updateimageincorder(previd,id)
	{
	
		var splittedid=id.split("_");
		var currentid=splittedid[2];
		var dataid=splittedid[3];
		var splittedid1=previd.split("_");
		var changeid=splittedid1[2];
		var dataid1=splittedid1[3];
		var locale=splittedid[1];
		var newid=splittedid[0]+"_"+splittedid[1]+"_"+splittedid[2]+"_"+splittedid1[3];
		var newid1=splittedid1[0]+"_"+splittedid1[1]+"_"+splittedid1[2]+"_"+splittedid[3];
		$.ajax({
			url:ajax_url+'admin/Users/updateorder',
			data:{previd:changeid,id:currentid,locale:locale,dataid:dataid,dataid1:dataid1},
			type:'POST',
			success:function(result)
			{
						$(".loadingcontent").html(result);
				 
			}
			})
		
		return false;
	}
	
	function updateimagedecorder(nextid,id)
	{
		var splittedid=id.split("_");
		var currentid=splittedid[2];
		var dataid=splittedid[3];
		var splittedid1=nextid.split("_");
		var changeid=splittedid1[2];
		var dataid1=splittedid1[3];
		var locale=splittedid[1];
		var newid=splittedid[0]+"_"+splittedid[1]+"_"+splittedid[2]+"_"+splittedid1[3];
		var newid1=splittedid1[0]+"_"+splittedid1[1]+"_"+splittedid1[2]+"_"+splittedid[3];
		$.ajax({
			url:ajax_url+'admin/Users/updateorder',
			data:{nextid:changeid,id:currentid,locale:locale,dataid:dataid,dataid1:dataid1},
			type:'POST',
			success:function(result)
			{
			
			$(".loadingcontent").html(result);
				 
			}
			});
		
		return false;
	}
	
	
	function deleteimage(id){
		var splittedid=id.split("_");
		var order=splittedid[2];
		var locale=splittedid[1];
		$.ajax({
			url:ajax_url+'admin/Users/deletebanner',
			data:{locale:locale,order:order},
			type:'POST',
			success:function(result)
			{
				console.log(result);
				
			if(result)
			{
				
						$(".loadingcontent").html(result);
			}	 
			}
			})
		
		return false;
		}
        
        
        
        
	
	function selectcourses1(){
	
	//alert("hello");
	
	var authors=$("#CommissionAuthorName01").val();
	if(authors.length>1)
	{
	$.ajax({
		url:ajax_url+'admin/Users/getallauthors',
		data:{authors:authors},
		type:'POST',
		success:function(result){
			console.log(result);
			$("#CommissionParticular-course01").append(result);
			
			}
		
		});
		
	}
	else
	{
	$.ajax({
		url:ajax_url+'admin/Users/getallauthors',
		data:{authors:authors},
		type:'POST',
		success:function(result){
			//console.log(result);
			$("#CommissionParticular-course01").html(result);
			
			}
		
		});
	}
	
	
	
	}
