/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraPlugins = 'widget,lineutils,dialog,clipboard,tableresize';
	config.removePlugins = 'image, format';
	config.stylesSet = 'my_styles';
config.allowedContent = true;
};

CKEDITOR.stylesSet.add( 'my_styles', [
    // Block-level styles
    { name: 'Normal', element: 'p', styles: { 'color': '#3F3F3F', 'font-size': '130%', 'margin-bottom': '14px' } },
    { name: 'Title', element: 'p', styles: { 'color': '#3F3F3F', 'font-size': 'x-large', 'font-weight': 'bold', 'line-height': '200%' } },
    { name: 'Subtitle', element: 'p', styles: { 'color': '#A5A5A5', 'font-size': 'large', 'font-weight': 'bold', 'line-height': '200%' } },
    { name: 'Heading', element: 'h1', styles: { 'color': '#3F3F3F', 'line-height': '200%' } },
    { name: 'Subheading', element: 'h2', styles: { 'color': '#A5A5A5', 'line-height': '200%' } },

    // Inline styles
    { name: 'Highlight', element: 'span', styles: { 'background-color': 'Yellow' } },
    { name: 'Computer Code', element: 'code' },
    { name: 'Variable', element: 'var' },
    { name: 'Language: RTL', element: 'span', attributes: { 'dir': 'rtl' } },
    { name: 'Language: LTR', element: 'span', attributes: { 'dir': 'ltr' } },

// Object styles
    { name: 'Table', element: 'table', attributes: { 'border': '1', 'cellpadding': '10', 'cellspacing': '0', 'width': '100%'}, styles: { 'color': '#3F3F3F', 'font-size': '130%', 'border': '1px solid black', 'padding': '10px', 'border-collapse': 'collapse' } },
    { name: 'Table Header', element: 'th', styles: { 'border': '1px solid black', 'padding': '10px' } },
    { name: 'Table Cell', element: 'td', styles: { 'border': '1px solid black', 'padding': '10px' } },
    { name: 'Ordered List', element: 'ol', styles: { 'color': '#3F3F3F', 'font-size': '130%', 'margin-bottom': '4px' } },
    { name: 'Unordered List', element: 'ul', styles: { 'color': '#3F3F3F', 'font-size': '130%', 'margin-bottom': '4px' } }
]);

// Set default values in the Table dialogue
CKEDITOR.on('dialogDefinition', function( ev ) {
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;

  if(dialogName === 'table') {
    var infoTab = dialogDefinition.getContents('info');
    var cellSpacing = infoTab.get('txtCellSpace');
    cellSpacing['default'] = "0";
    var cellPadding = infoTab.get('txtCellPad');
    cellPadding['default'] = "5";
    var border = infoTab.get('txtBorder');
    border['default'] = "1";
  }
});
